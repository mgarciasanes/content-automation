echo
echo
echo
echo "Connecting to Container's Console"
echo
echo
echo 
echo
echo  "These are the current active PODS"
echo
echo
kubectl --kubeconfig /etc/kubernetes/admin.conf get pods --all-namespaces
echo
echo


read -p "Please type the name of the POD and press Enter.  "
echo
echo

POD=$REPLY

read -p "Please type the namespace and press Enter.  "
echo
echo

NAMESPACE=$REPLY


echo "These are the available containers"
echo
echo
kubectl --kubeconfig /etc/kubernetes/admin.conf get pods "$POD"  -n "$NAMESPACE"  -o jsonpath={.spec.containers[*].name}
echo
echo


read -p "Please type the name of the container  and press Enter.  "
echo
echo

CONTAINER=$REPLY

echo "You have selected Container $CONTAINER in POD $POD in namespace $NAMESPACE .  Trying to connect..."
echo
echo

kubectl --kubeconfig /etc/kubernetes/admin.conf exec -it "$POD"   -c "$CONTAINER"  /bin/bash -n "$NAMESPACE"
