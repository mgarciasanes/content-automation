#!/usr/bin/env python2.7
#####################################
# Import Modules
#####################################
import sys
import logging
import threading
import os
import os.path
import time
import datetime
import socket
import psutil


modulesPath=r'/root/scripts/modules/'
sys.path.append(modulesPath)
corePath=r'/root/scripts/core/'
sys.path.append(corePath)


import influx as influx
import genFunctions as gen
import logger as logger
import worker as worker
import signalMonitor
import rabbitQueue
import inMemoryStorage


#####################################
# Global Variables
#####################################
#Hostname
host=socket.gethostname()
INFLUXDB='influxKube'
RABBITSERVER='content-automation-rabbitmq'


#####################################
# General Functions
#####################################


def refreshModules():
    logger.autolog(level=1, message='Refreshing Modules, reloading...')
    reload(influx)
    reload(gen)
    reload(logger)
    reload(worker)
    #reload(signalMonitor)


#####################################
#  FUNCTIONS IN LOOP
#####################################

def checkActiveRequests():

    #global host

    #activeSessions=influx.readFromInfluxDb('select "datacenter","host","recipePath","recipeName","type","demo","id","vpod","anycpwd","owner",demoStatus from (select "datacenter","host","recipePath","recipeName","type","demo","id","vpod","anycpwd","owner",last(demoStatus) as demoStatus  from inDemoAutomation_sessions  group by id )  where demoStatus='+"'active'" +' and host='+"'"+host+"'" +'  group by id',influxDb=INFLUXDB)
    #activeSessions=influx.readFromInfluxDb('select *,id from (select *,last(demoStatus) from '+ str(influx.measurementSessions)+'  group by id )  where demoStatus='+"'active'" +' and location='+"'"+LOCATION+"'" + ' group by id',influxDb=INFLUXDB)

    activeSessions=influx.readFromInfluxDb('select *,id from (select *,last(demoStatus) from '+ str(influx.measurementSessions)+'  group by id )  where demoStatus='+"'active'" + ' group by id',influxDb=INFLUXDB)


    logger.autolog(level=1,message="Active Sessions:")
    for activeSession in activeSessions:
        logger.autolog(level=1,message=str(activeSession["recipeName"] + " " +str(activeSession["id"])))

    if activeSessions == False:
        logger.autolog(message='Unable to get active  requests')
        return False

    if activeSessions == []:
        logger.autolog(message='No active requests found')
        return True

    for activeSession in activeSessions:
        
        try:
            
            logger.autolog(level=1,message="Will get data from REDIS for key: " + str(activeSession["key"]))
            tmp=inMemoryStorage.cacheReadRequest(activeSession)
            logger.autolog(level=1,message="Got: " + str(tmp["request"]))
            activeSession=tmp
            
            tasks,errorMessage=gen.getRecipeEndTasks(activeSession["request"]['recipeName'],specificFolder=activeSession["request"]['recipePath'])
            logger.autolog(message='These are the END tasks:   ' + str(tasks))
            logger.autolog(level=1, message="CHECKING ACTIVE SESSION with recipe " + str(activeSession["request"]['recipeName']) + " with ID " + str(activeSession["request"]['id']) + " for user " + str(activeSession["request"]['owner']))

            try:
                if len(tasks)>1:
                    logger.autolog(level=1,message='WARNING: More than one END task detected. Taking only the first one')
                    tasks=[tasks[0]]
            except Exception as e:
                logger.autolog(level=1, message="Error while checking tasks size, maybe not a list")
                logger.autolog(level=1, message=str(e))

            if tasks == False:
                logger.autolog(message=errorMessage +' '+ str(activeSession["request"]['recipeName'])+ ' with ID ' + activeSession["request"]['id'])
                influx.markSessionError(activeSession["request"],comments=errorMessage,influxDb=INFLUXDB)
            else:   
                failureFree,allTasksExecuted,executedTasks=worker.execTasks(activeSession,tasks,action='End')   
                influx.recordExecutedTasks(executedTasks,influxDb=INFLUXDB)
                if (failureFree and allTasksExecuted):

                    try:  
                        if executedTasks[0]['taskOutput']['readyToDelete']:
                            logger.autolog(level=1,message='All tasks executed without errors while checking Active Session. Marking for deletion')
                            #Mark Session for deletion in DB
                            influx.markSessionForDeletion(activeSession["request"],influxDb=INFLUXDB)
                            #Queue the request in RABBIT
                            activeSession["action"]='Stop'
                            rabbitQueue.rabbitSendToWorkersQueue(RABBITSERVER,activeSession)
                            return True
                        else:
                            logger.autolog(message='Session still active...')

                    except Exception as e:
                        logger.autolog(level=1, message="Error while reading END task output")
                        logger.autolog(level=1, message=str(e))
                        #return False

                if (not failureFree):
                    logger.autolog(message='Some tasks got errors while checking Active Sessions. Marking error')
                    #influx.markSessionError(activeSession,comments='Error while cleanup',influxDb=INFLUXDB)
                    #return False

        except Exception as e:

            tasks,errorMessage=gen.getRecipeEndTasks(activeSession['recipeName'],specificFolder=activeSession['recipePath'])
            logger.autolog(message='These are the END tasks:   ' + str(tasks))
            logger.autolog(level=1, message="CHECKING ACTIVE SESSION with recipe " + str(activeSession['recipeName']) + " with ID " + str(activeSession['id']) + " for user " + str(activeSession['owner']))

            try:
                if len(tasks)>1:
                    logger.autolog(level=1,message='WARNING: More than one END task detected. Taking only the first one')
                    tasks=[tasks[0]]
            except Exception as e:
                logger.autolog(level=1, message="Error while checking tasks size, maybe not a list")
                logger.autolog(level=1, message=str(e))

            if tasks == False:
                logger.autolog(message=errorMessage +' '+ str(activeSession['recipeName'])+ ' with ID ' + activeSession['id'])
                influx.markSessionError(activeSession,comments=errorMessage,influxDb=INFLUXDB)
            else:   
                failureFree,allTasksExecuted,executedTasks=worker.execTasks(activeSession,tasks,action='End')   
                influx.recordExecutedTasks(executedTasks,influxDb=INFLUXDB)
                if (failureFree and allTasksExecuted):

                    try:  
                        if executedTasks[0]['taskOutput']['readyToDelete']:
                            logger.autolog(level=1,message='All tasks executed without errors while checking Active Session. Marking for deletion')
                            #Mark Session for deletion in DB
                            influx.markSessionForDeletion(activeSession,influxDb=INFLUXDB)
                            #Queue the request in RABBIT
                            activeSession["action"]='stop'
                            rabbitQueue.rabbitSendToWorkersQueue(RABBITSERVER,activeSession)
                            return True
                        else:
                            logger.autolog(message='Session still active...')

                    except Exception as e:
                        logger.autolog(level=1, message="Error while reading END task output")
                        logger.autolog(level=1, message=str(e))
                        #return False

                if (not failureFree):
                    logger.autolog(message='Some tasks got errors while checking Active Sessions. Marking error')
                    #influx.markSessionError(activeSession,comments='Error while cleanup',influxDb=INFLUXDB)
                    #return False




def getRequestFromQueue():

    global host

    query='select *,id from (select *,last(demoStatus) from '+ str(influx.measurementSessions)+'  group by id )  where demoStatus='+"'active'" +' group by id'
    activeSession=influx.readFromInfluxDb(query,influxDb=INFLUXDB)

    logger.autolog(level=1,message=str(activeSession))

    if activeSession == False:
        logger.autolog(message='Unable to get active  requests')
        return False

    if activeSession == []:
        logger.autolog(message='No active requests found')
        return True

    logger.autolog(level=1,message='Found Active request for recipe ' +  activeSession['recipeName']+ ' with id ' + activeSession['id'] + '. It will be checked now')
    return activeSession 


def executeRecipe(activeSession):
    tasks,errorMessage=gen.getRecipeEndTasks(activeSession['recipeName'],specificFolder=activeSession['recipePath'])
    if tasks == False:
        logger.autolog(message=errorMessage +' '+ str(activeSession['recipeName'])+ ' with ID ' + activeSession['id'])
        influx.markSessionError(activeSession,comments=errorMessage,influxDb=INFLUXDB)
    else:   
        failureFree,allTasksExecuted,executedTasks=worker.execTasks(activeSession,tasks,action='End')   
        influx.recordExecutedTasks(executedTasks,influxDb=INFLUXDB)
        if (failureFree and allTasksExecuted):
            logger.autolog(message='All tasks executed without errors while checking Active Session. Marking deleted')
            influx.markSessionDeleted(activeSession,influxDb=INFLUXDB)
        if (not failureFree):
            logger.autolog(message='Some tasks got errors while checking Active Sessions. Marking error')
            influx.markSessionError(activeSession,comments='Error while cleanup',influxDb=INFLUXDB)
        if (failureFree and not allTasksExecuted):
            logger.autolog(message='No Errors, leaving Session as Active')



#####################################
#  LOOP
#####################################

def loop(loopMode,multithread,timer):

    refreshModules()
    
    if signalCheck.kill_now:
        logger.autolog(message="End of loop. I was killed gracefully.")
        quit()

    checkActiveRequests()
    #activeSession=getRequestFromQueue()
    #if activeSession:
    #    executeRecipe(activeSession)
    #else:
    #    logger.autolog(message='No session details for Recipe execution...')

     
    if loopMode:
        logger.autolog(message='checkQueuedRequests running loop mode. Will excute in ' + str(timer)+' seconds...')

        #Loop Option 1
        #logger.autolog(level=1, message='Looping with threading.Timer')
        #A thread will call the loop function in time described in "timer"
        #threading.Timer(timer, loop,[loopMode, multithread, timer]).start() 


        #Loop Option 2
        #The code below is not actually a loop, but rather it will finish the script and restart itself automatially 
        #
        logger.autolog(level=1, message='Looping with execl. Restarting Script.')
        time.sleep(timer)
        try:
            p = psutil.Process(os.getpid())
            for handler in p.open_files() + p.connections():
                print("Restarting Script: Closing handler  " + str(handler))
                os.close(handler.fd)        
        except Exception as e:
            logger.autolog(level=1, message="Error Restarting the Script")
            logger.autolog(level=1, message=str(e))

        python = sys.executable
        os.execl(python, python, *sys.argv) 


if __name__ == '__main__':

    ##################################
    ## Check Arguments
    ##################################

    from argparse import ArgumentParser
    parser = ArgumentParser("dCloud DCV Automation Script: checkActiveRequests")
    parser.add_argument('-l', '--loop', help='Run Once or Loop. Values are  loop or once', default=True)
    parser.add_argument('-t', '--timer', help='Run Once or Loop. Values are  loop or once', default=30)
    parser.add_argument('-d', '--database', help='Specify database to use. Default is influxKube ', default='influxKube')
    parser.add_argument('-dp', '--databaseport', help='Specify database port to use. Default is False  ', default="noPort")
    parser.add_argument('-loc', '--location', help='Specify Location where this script is running. Default is False ', default="noLocation")
    parser.add_argument('-x', '--multithread', help='Run tasks for all sessions in concurrent or sequential. Values are true or false. default is false.', default=False)


    parser.add_argument('-r', '--rabbitserver', help='Specify rabbitmq server to use. Default is rabbitKube ', default='rabbitKube')
    parser.add_argument('-rp', '--rabbitport', help='Specify rabbitmq port to use. Default is noPort  ', default="noPort")
    parser.add_argument('-c', '--credfile', help='Specify Credentials File  ', default=r'/root/repo/dcv/creds.cfg')

    args = parser.parse_args()

    ################################
    #Start SSH Service
    ################################
    
    #os.system(r'/usr/sbin/sshd -D &')
    
    #####################################
    ## Configure Logger
    #####################################


    hostname = socket.gethostname()
    logger.setLogFiles(newLogFile=str(hostname)+'.log',newLogDebugFile=str(hostname )+'_DEBUG.log')
    #logger.setLogFiles(newLogFile='checkActiveRequests.log',newLogDebugFile='checkActiveRequests_DEBUG.log')
    logger.setLogger()

    #####################################
    ## Configure Signal Check 
    #####################################

    signalCheck = signalMonitor.SignalMonitor()


    #####################################
    ## Start Loop
    #####################################
    #global INFLUXDB

    #DB Port to be used. If not specified as argument, use the one specified in Creds file, section "database"
    if args.databaseport=="noPort":
        logger.autolog(level=1,message="No database port specified, using port from Cred file")
        INFLUXDB=args.database
    else:
        logger.autolog(level=1,message="Database port specified, using it. " +str(args.databaseport))
        INFLUXDB={'database':args.database,'port':args.databaseport}


    #Get Cluster Location. If not specified as argument, use the valued obtained directly from Kubernetes' label
    if args.location=="noLocation":
        cmd=r'kubectl get services kubernetes --namespace=default -o jsonpath={.metadata.labels.location} > /root/location'
        logger.autolog(level=1,message=str(cmd))
        os.system(cmd)
        locationFile=open(r'/root/location','r')
        location=locationFile.read().strip('\n')
        locationFile.close()
        logger.autolog(level=1,message="We are working with location  " + str(location))
        LOCATION=location
    else:
        LOCATION=str(args.location)


    #Get Rabbitmq Server
    creds = gen.getDemoCredentialsByName(args.credfile,args.rabbitserver)
    RABBITSERVER=creds['host']



    loop(args.loop,args.multithread,int(args.timer))





