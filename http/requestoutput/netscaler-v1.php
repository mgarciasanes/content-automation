<!DOCTYPE html>
<html>
<head>
<title>DCV Automation Controller: CWOM</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script>
<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script src="scripts/generic.js"></script>
<style>
#copy-to-clipboard {
    background-image: url( 'https://www.iconexperience.com/_img/o_collection_png/green_dark_grey/24x24/plain/clipboard_check_edit.png' );
    background-size: 24px 24px;
    background-color: white;
    background-repeat: no-repeat;
    border: 1px;
    height: 24px;  
    width: 24px;}
</style>

</head>
<body>
<br><br><br>
<table width="600" border="0" cellspacing="0" cellpadding="0" align="center">
  <tbody>
    <tr>
    </tr>
    <tr>
      <th scope="col">&nbsp;</th>
      <th scope="col">&nbsp;</th>
      <th scope="col" colspan="3"><h2> Thanks for using dCloud <br> We have created the following credentials for you : </h2> </th>
      <th scope="col">&nbsp;</th>
      <th scope="col">&nbsp;</th>
      <th scope="col">&nbsp;</th>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td align="right">Userid:</td>
      <td align="center"><h3><p id="username"></p></h3></p></td>
      <td td align="left"><button id="copy-to-clipboard" onclick="copyToClipboard('#username')"></button></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td align="right">Password:</td>
      <td align="center"><h3><p id="password"></p></h3></p></td>
      <td align="left"><button id="copy-to-clipboard" onclick="copyToClipboard('#password')"></button></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td align="center" colspan="3">
        <p> The provided credentials are valid for 2 hours. <br> <h1><p id="timer"> </p></h1> <br> To proceed to the requested site, please click  <div id="hyperlink"> </div>  </p> </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align="center" colspan="8" ></td>
    </tr>

  </tbody>
</table>

<script type="text/javascript">

    function checkRequestStatus(demoStatus) {
        var self = this;
        self.tasksURI = '<?php echo  $requestStatusApi; ?>';
        self.ajax = function(uri, method, data) {
            var request = {
                url: uri,
                type: method,
                contentType: "application/json",
                accepts: "application/json",
                cache: false,
                dataType: 'json',
                data: JSON.stringify(data),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization","Basic " + btoa(self.username + ":" + self.password));
                },
                error: function(jqXHR) {
                    console.log("ajax error " + jqXHR.status);
                }
            };
            //alert(JSON.stringify(request))
            return $.ajax(request);
        }

        self.ajax(self.tasksURI , 'GET','').done(function(data) {
            console.log(data)
            if (data.status=='active' ||  data.status=='complete' ||  data.status=='executed'){

                // Update HYPERLINK to UCSPM
                document.getElementById("hyperlink").innerHTML='<a href="'+data.tasks.Start[1].output.url+'" title="Citrix Netscaler Cloud" target="_blank"> HERE </a>';
                document.getElementById("username").innerHTML='DCV\\'+data.tasks.Start[0].output.username;
                document.getElementById("password").innerHTML=data.tasks.Start[0].output.password;

                // Prepare Countdown
                startTime="0"
                for (var stage in data.history){
                    if (data.history[stage].demoStatus=='starting'){
                        //console.log(data.history[stage])
                        startTime=data.history[stage].time;
                        //console.log(startTime);
                    }
                }
                if (startTime!="0"){
                    // Get time when the request started
                    startTime= new Date(startTime);
                    console.log(startTime);
                    phpNow='<?php echo date("Y-m-d H:i:s"); ?>';
                    phpNow=phpNow.replace(' ', 'T');
                    console.log(phpNow);
                    serverNow=new Date(phpNow);
                    console.log(serverNow);
                    startTime.setSeconds(startTime.getSeconds() + parseInt(data.tasks.End[0].input.sessionMaxDurationMins)*60);
                    //startTime.setSeconds(startTime.getSeconds() + 120*60 );
                    console.log(startTime);
                    distance=  startTime - serverNow;
                    console.log(distance);
                    // Update the count down every 1 second
                    var x = setInterval(function() {
                                            distance = distance -1000;
                                            //console.log(distance);
                                            // Time calculations for days, hours, minutes and seconds
                                            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                                            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                                            // Display the result in the element with id="demo"
                                            document.getElementById("timer").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
                                            // If the count down is finished, write some text 
                                            if (distance < 0) {
                                                clearInterval(x);
                                                document.getElementById("timer").innerHTML = "EXPIRED";
                                            }
                                        }, 1000);

                } 
            }
            if (data.status=='error' ||  data.status=='cancelled' ||  data.status=='toBeDeleted'){
                console.log(data);
                window.open('../errorPage.php',"_self");
             }
        });
    }

    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
    }




    console.log("Start");
    checkRequestStatus();

</script>
</body>
</html>
