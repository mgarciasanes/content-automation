<!DOCTYPE html>
<html>
<head>
<title>DCV Automation Controller: Intersight</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script>
<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script src="scripts/generic.js"></script>
</head>
<body>

<table width="600" border="0" cellspacing="0" cellpadding="0" align="center">
  <tbody>
    <tr>
      <td align="center" colspan="8" ><br><br><img  src="https://dcloud.cisco.com/favicon.ico" width="100" height="100" alt=""/></td>
    </tr>
    <tr>
      <th scope="col">&nbsp;</th>
      <th scope="col">&nbsp;</th>
      <th scope="col" colspan="3"><h2> Welcome to dCloud <br> We have created an Intersight account for you </h2> </th>
      <th scope="col">&nbsp;</th>
      <th scope="col">&nbsp;</th>
      <th scope="col">&nbsp;</th>
    </tr>
    <tr>
      <th scope="col">&nbsp;</th>
      <th scope="col">&nbsp;</th>
      <th scope="col" colspan="3"><p>You will be requested to authenticate with your Cisco CCO account  </p> </th>
      <th scope="col">&nbsp;</th>
      <th scope="col">&nbsp;</th>
      <th scope="col">&nbsp;</th>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td align="center" colspan="3">
        <p> Your Intersight access will be valid for 2 hours. <h1><p id="timer"> </p></h1> In order to proceed to the Intersight site, please click  <div id="hyperlink"> </div> </p> </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align="center" colspan="8" ></td>
    </tr>

  </tbody>
</table>

<script type="text/javascript">

    function checkRequestStatus(demoStatus) {
        var self = this;
        self.tasksURI = '<?php echo  $requestStatusApi; ?>';
        self.ajax = function(uri, method, data) {
            var request = {
                url: uri,
                type: method,
                contentType: "application/json",
                accepts: "application/json",
                cache: false,
                dataType: 'json',
                data: JSON.stringify(data),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization","Basic " + btoa(self.username + ":" + self.password));
                },
                error: function(jqXHR) {
                    console.log("ajax error " + jqXHR.status);
                }
            };
            //alert(JSON.stringify(request))
            return $.ajax(request);
        }

        self.ajax(self.tasksURI , 'GET','').done(function(data) {
            console.log(data)
            if (data.status=='active' ||  data.status=='complete' ||  data.status=='executed'){

                // Update HYPERLINK to INTERSIGHT
                document.getElementById("hyperlink").innerHTML='<a href="'+data.tasks.Output.destUrl+'" title="dCloud" target="_blank"> HERE </a>';

                // Prepare Countdown
                startTime="0"
                for (var stage in data.history){
                    if (data.history[stage].demoStatus=='starting'){
                        //console.log(data.history[stage])
                        startTime=data.history[stage].time;
                        //console.log(startTime);
                    }
                }
                if (startTime!="0"){
                    // Get time when the request started
                    startTime= new Date(startTime);
                    console.log(startTime);
                    serverNow=new Date('<?php echo date("Y-m-d H:i:s"); ?>');
                    phpNow='<?php echo date("Y-m-d H:i:s"); ?>';
                    phpNow=phpNow.replace(' ', 'T');
                    console.log(phpNow);
                    serverNow=new Date(phpNow)
                    startTime.setSeconds(startTime.getSeconds() + parseInt(data.tasks.End[0].input.sessionMaxDurationMins)*60);
                    //startTime.setSeconds(startTime.getSeconds() + 120*60 );
                    console.log(startTime);
                    distance=  startTime - serverNow;
                    console.log(distance);
                    // Update the count down every 1 second
                    var x = setInterval(function() {
                                            distance = distance -1000;
                                            //console.log(distance);
                                            // Time calculations for days, hours, minutes and seconds
                                            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                                            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                                            // Display the result in the element with id="demo"
                                            document.getElementById("timer").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
                                            // If the count down is finished, write some text 
                                            if (distance < 0) {
                                                clearInterval(x);
                                                document.getElementById("timer").innerHTML = "EXPIRED";
                                            }
                                        }, 1000);

                } 
            }
            if (data.status=='error' ||  data.status=='cancelled' ||  data.status=='toBeDeleted'){
                console.log(data);
                window.open('../errorPage.php',"_self");
             }
        });
    }

    console.log("Start");
    checkRequestStatus();

</script>
</body>
</html>
