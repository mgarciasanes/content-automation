<!DOCTYPE html>
<html>
<head>
<title>DCV Automation Controller</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script>
<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script src="scripts/generic.js"></script>

</head>



<body style="background-color: #3399FF;">

<div>

<!-- HERE GOES THE HTML CODE -->

</div>

<script type="text/javascript">

    function checkRequestStatus(demoStatus) {
        var self = this;
        self.tasksURI = '<?php echo  $requestStatusApi; ?>';
        self.ajax = function(uri, method, data) {
            var request = {
                url: uri,
                type: method,
                contentType: "application/json",
                accepts: "application/json",
                cache: false,
                dataType: 'json',
                data: JSON.stringify(data),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization","Basic " + btoa(self.username + ":" + self.password));
                },
                error: function(jqXHR) {
                    console.log("ajax error " + jqXHR.status);
                }
            };
            //alert(JSON.stringify(request))
            return $.ajax(request);
        }

        self.ajax(self.tasksURI , 'GET','').done(function(data) {

            if (data.status=='active' ||  data.status=='complete' ||  data.status=='executed'){
                // --- HERE GOES THE DATA PARSING --- 
                
            }
            if (data.status=='error' ||  data.status=='cancelled' ||  data.status=='toBeDeleted'){
                window.open('../errorPage.php',"_self");
             }
        });
    }




    // --- HERE MORE SCRIPTS IF NEEDED ---


    </script>
</body>
</html>
