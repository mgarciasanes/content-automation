<!DOCTYPE html>
<html>
<head>
<title>DCV Automation Controller</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="loader.css">

<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script>
<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
</head>
<body>

<div class="progress"></div>
<div class="loading"><p align="center">Loading...</p></div>


    <div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="#">DCV Automation Controller: Session Details</a>
        </div>
    </div>
    <div id="main" class="container">
	
    <br>	


    <button data-bind="click: showStartupLog" class="btn">Show Startup Logs </button> <button data-bind="click: showCleanupLog" class="btn">Show Cleanup Logs</button>  <button data-bind="click: showDeletedLog" class="btn">Show Delete Tasks Logs</button>  <button data-bind="click: showStartupLogDebug" class="btn">Show Startup Logs (Debug)</button> <button data-bind="click: showCleanupLogDebug" class="btn">Show Cleanup Logs (Debug)</button>    <button data-bind="click: showDeletedLogDebug" class="btn">Show Delete Tasks Logs (Debug)</button><br><br>


<table class="table table-striped">
    <tr><td style="width: 1px;"><b>Demo</b></td><td><b>DC</b></td><td><b>ID</b></td><td><b>Type</b></td><td><b>Priority</b></td><td><b>Host</b></td><td><b>User</b></td></tr>
    <!-- ko foreach: mainDetails -->
    <tr>
        <td><p data-bind="text: demo"></p></td>
        <td><p data-bind="text: datacenter">  </p></td>
        <td><p data-bind="text: id">  </p></td>
        <td><p data-bind="text: location"></p></td>
        <td><p data-bind="text: owner"></p></td>
    </tr>
    <!-- /ko -->
</table><br> 


<div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="#">Session: History </a>
        </div>
    </div>


<table class="table table-striped">
    <tr><td style="width: 1px;">Status</td><td><b>Demo</b></td><td><b>Time</b></td><td><b>Comments</b></td></tr>
    <!-- ko foreach: history -->
    <tr data-bind="visible: visibleFlag"  >
        <td >
            <span data-bind="visible: activeFlag" class="label label-success">Active</span>
            <span data-bind="visible: errorFlag" class="label label-important">Error</span>
            <span data-bind="visible: queuedFlag" class="label label-info">Queued</span>
            <span data-bind="visible: completeFlag" class="label label-warning">Complete</span>
            <span data-bind="visible: toBeDeletedFlag" class="label label-inverse">To be deleted</span>
	    <span data-bind="visible: startingFlag" class="label label-inverse">Starting</span>
            <span data-bind="visible: executingFlag" class="label label-inverse">Executing</span>
	    <span data-bind="visible: cleaningFlag" class="label label-inverse">Cleaning</span>
	    <span data-bind="visible: cancelledFlag" class="label label-inverse">Cancelled</span>
        </td>
        <td><p><b data-bind="text: demo"></b></p></td>
        <td><p data-bind="text: time"></p></td>
        <td><p data-bind="text: comments"></p></td>
    </tr>
    <!-- /ko -->
</table><br> 


<div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="#">Session: StartUp Tasks </a>
        </div>
    </div>


<table class="table table-striped">
    <tr><td style="width: 1px;"><b>Status</b></td><td><b>Task Name</b></td><td><b>Time</b></td><td><b>Comments</b></td></tr>
    <!-- ko foreach: startupTasks -->
    <tr>
        <td >
            <span data-bind="visible: statusFlag" class="label label-success">Success</span>
            <span data-bind="visible: !statusFlag()" class="label label-important">Failed</span>
        </td>
        <td><p data-bind="text: taskName"></p></td>
        <td><p data-bind="text: comments"></p></td>
    </tr>
    <!-- /ko -->
</table><br> 

<div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="#">Session: CleanUp Tasks </a>
        </div>
    </div>
<table class="table table-striped">
    <tr><td style="width: 1px;"><b>Status</b></td><td><b>Task Name</b></td><td><b>Time</b></td><td><b>Comments</b></td></tr>
    <!-- ko foreach: cleanupTasks -->
    <tr>
        <td >
            <span data-bind="visible: statusFlag" class="label label-success">Success</span>
            <span data-bind="visible: !statusFlag()" class="label label-important">Failed</span>
        </td>
        <td><p data-bind="text: taskName"></p></td>
        <td><p data-bind="text: comments"></p></td>
    </tr>
    <!-- /ko -->
</table>


    </div>

    <script type="text/javascript">

    function TasksViewModel(demoStatus) {

        var self = this;
        var visibleFlag=true;
        var startingFlag='';
        var deletingFlag='';
        var cancelingFlag='';

        self.tasksURI = 'http://198.19.254.51:31088';
        self.ajax = function(uri, method, data) {
            var request = {
                url: uri,
                type: method,
                contentType: "application/json",
                accepts: "application/json",
                cache: false,
                dataType: 'json',
                data: JSON.stringify(data),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization","Basic " + btoa(self.username + ":" + self.password));
                },
                error: function(jqXHR) {
                    console.log("ajax error " + jqXHR.status);
                    alert('scheise')
                }
            };
            alert(JSON.stringify(request))
            return $.ajax(request);
        }

        alert('here')	

        //locationString=(String(window.location));
        //sessionId=locationString.split('sessionshow')[1];
        self.ajax(self.tasksURI , 'GET').done(function(data) {

            alert('now here');

        });
    }

    TasksViewModel();
//    ko.applyBindings(new TasksViewModel(), $('#main')[0]);

    </script>
</body>
</html>
