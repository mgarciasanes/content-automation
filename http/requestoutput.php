<?php
$id=$_GET['id'];
$htmlTemplate=$_GET['htmlTemplate'];

$apiServer=$_ENV['APISERVER'];
$webServer=$_ENV['WEBSERVER'];
$apiPort=$_ENV['APIPORT'];
$webPort=$_ENV['WEBPORT'];
$apiProtocol=$_ENV['APIPROTOCOL'];
$webProtocol=$_ENV['WEBPROTOCOL'];
$requestStatusUrl=$apiProtocol.'://'.$apiServer.':'.$apiPort.'/api/v2.0/request/'.$id;
$requestLogsUrl=$webProtocol.'://'.$webServer.':'.$webPort.'/showlogs.php?filter='.$id;
$errorUrl=$webProtocol.'://'.$webServer.':'.$webPort.'/errorPage.php?id='.$id;

$webURL=$_SERVER[HTTP_HOST];
$apiURL=str_replace("web","api",$webURL);
$requestStatusApi='http://'.$apiURL.'/api/v2.0/request/'.$id;


include 'requestoutput/'.$htmlTemplate;
?>
