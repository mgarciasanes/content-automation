
<!DOCTYPE html>
<html>
<head>
<title>dCloud in-demo Automation Controller</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script>
<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment.min.js"></script>
<script src="scripts/pikaday.js"></script>
<script src="scripts/generic.js"></script>
<link rel="stylesheet" href="css/pikaday.css">
<link rel="stylesheet" href="css/site.css">


</head>
<body>
    <div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="#"> dCloud in-demo Automation Controller: Dashboard [DEVELOPMENT BRANCH] </a>
        </div>
    </div>
    <div id="main" class="container">



<br>

<input type="image" id="showhidearrow_dashboardFilterForm" style="width:15px;" src="images/icons/down_arrow.png" onClick="div_toggle_with_image('dashboardFilterForm',this.id);">Dashboard Options <br>
<div  id="dashboardFilterForm" hidden="true" style="overflow-x: auto;background-color:#FFF;border: 1px solid #A9A9A9;border-radius: 7px;-moz-border-radius: 7px;">

<div style="width:95%;margin:auto;">
<br>

<div class="navbar"><div class="navbar-inner" ><a class="brand" > Search Form </a></div></div>

<table class="table table-striped">
       <tr>
           <td style="width: 1px;"><b>Status</b></td><td><b>Demo</b></td><td><b>ID</b></td><td><b>Location</b></td><td><b>DC</b></td><td><b>User</b></td><td><b>Time Range [from/to]</b></td><td><b>Search</b></td>

       </tr>
       <tr>
           <td>
               <select  id="queryStatus"  name="status" style="height:125px;width:100px;" multiple size="6">
                 <option value="active" >active</option>
                 <option value="complete">complete</option>
                 <option value="error">error</option>
                 <option value="scheduled">scheduled</option>
                 <option value="toBeDeleted">toBeDeleted</option>
                 <option value="starting">starting</option>
               </select>
           </td>
           <td><input type="text" id="queryDemo"  style="height:8px;width:75px"></td>
           <td><input type="text" id="queryId" style="height:8px;width:75px"></td>
           <td><input type="text" id="queryLocation" style="height:8px;width:75px"></td>
           <td>
               <select  id="queryDc" style="height:125px;width:100px" name="dc" multiple size="5">
                 <option value="rtp">rtp</option>
                 <option value="lon">lon</option>
                 <option value="sng">sng</option>
                 <option value="chi">chi</option>
                 <option value="sjc">sjc</option>
               </select>
           </td>
           <td><input type="text" id="queryUser" style="height:8px;width:75px"></td>
           <td><input type="text" id="timeRangeStart" style="height:8px;width:75px;">-<input type="text" id="timeRangeEnd" style="height:8px;width:75px" ></td>
           <td><button data-bind="click: refreshDashboard"  class="btn" style="height:125px;width:100px;">Search</button></td>
       </tr>
    </table>

<div class="navbar"><div class="navbar-inner"><a class="brand" > Filter Options </a></div></div>

<table width="100%">
<tr align="center">
    <td><button data-bind="click: showAll" class="btn">Show All Requests</button></td>
    <td><button data-bind="click: showActive" class="btn">Show Active Requests</button></td> 
    <td><button data-bind="click: showComplete" class="btn">Show Complete Requests</button></td> 
    <td><button data-bind="click: showError" class="btn">Show Error Requests</button></td>
    <td><button data-bind="click: showQueued" class="btn">Show Queued Requests</button></td>
    <td><button data-bind="click: showScheduled" class="btn">Show Scheduled Requests</button></td>
</tr>
</table>
<br><br>
</div>
</div>

<br><br>

<table class="table table-striped">
    <tr><td style="width: 1px;"><b>Status</b></td><td><b>Demo</b></td><td><b>ID</b></td><td><b>DC</b></td><td><b>Location</b></td><td><b>User</b></td><td><b>Time</b></td><td><b>Comments</b></td><td><b>Options</b></td></tr>
    <!-- ko foreach: requests -->
    <tr data-bind="visible: visibleFlag"  >
        <td>
            <span data-bind="visible: activeFlag" class="label label-warning">Active</span>
            <span data-bind="visible: errorFlag" class="label label-important">Error</span>
            <span data-bind="visible: queuedFlag" class="label label-info">Queued</span>
            <span data-bind="visible: completeFlag" class="label label-success">Complete</span>
            <span data-bind="visible: toBeDeletedFlag" class="label label-info">Marked for cleanup</span>
            <span data-bind="visible: startingFlag" class="label label-warning">Starting</span>
            <span data-bind="visible: executedFlag" class="label label-success">Executed</span>
            <span data-bind="visible: cleaningFlag" class="label label-warning">Cleaning</span>
            <span data-bind="visible: cancelledFlag" class="label label-inverse">Cancelled</span>
            <span data-bind="visible: scheduledFlag" class="label label-info">Scheduled</span>
            <span data-bind="visible: deletedFlag" class="label label-success">Deleted</span>
        </td>
        <td><p><b data-bind="text: demo"></b></p></td>
        <td><p data-bind="text: id"></p></td>
        <td><p data-bind="text: dc"></p></td>
        <td><p data-bind="text: locationTag" style="font-size:75%;"></p></td>
        <td><p data-bind="text: owner"></p></td>
        <td width="70"><p data-bind="text: time" style="font-size:75%;"></p></td>
        <td><p data-bind="text: comments" style="font-size:75%;"></p></td>
        <td width="120">
            <table class="newclass">
              <tr>
                <td>
                  <span> <button data-bind="click: $parent.restart" class="btn"  style="background-image: url('images/icons/startup_icon.png');background-repeat: no-repeat; background-poistion: center;background-size:30px 30px;height:31px;width:31px"></button> </span>
                </td>
                <td>
                  <span> <button data-bind="click: $parent.delete"  class="btn"  style="background-image: url('images/icons/cleanup_icon.png');background-repeat: no-repeat; background-poistion: center;background-size:30px 30px;height:31px;width:31px"></button> </span>
                </td>
                <td>
                  <span> <button data-bind="click: $parent.show"  class="btn"  style="background-image: url('images/icons/show_icon.png');background-repeat: no-repeat; background-poistion: center;background-size:30px 30px;height:31px;width:31px"></button> </span>
                </td>
              </tr>
            </table>
        </td>
    </tr>
    <!-- /ko -->
</table>







    </div>



    <script type="text/javascript">



    function TasksViewModel(demoStatus) {
        var self = this;
        var visibleFlag=true;
        var startingFlag='';
        var deletingFlag='';
        var cancelingFlag='';


        self.controllersAPI ={"rtp":"dcv-automation-dev-api.svpod.dc-01.com","lon":"dcv-automation-dev-api.svpod.dc-03.com","sng":"dcv-automation-dev-api.svpod.dc-02.com","sjc":"dcv-automation-dev-api.svpod.dc-05.com",};
        self.controllersURL ={"rtp":"dcv-automation-dev-web.svpod.dc-01.com","lon":"dcv-automation-dev-web.svpod.dc-03.com","sng":"dcv-automation-dev-web.svpod.dc-02.com:","sjc":"dcv-automation-dev-web.svpod.dc-05.com",};
        self.requestStatusURI = '/requeststatus.php?id=';
        self.requestsListAPI = '/api/v2.0/request/list';
        self.requestRestartAPI = '/api/v2.0/request/restart/';
        self.requestCleanupAPI = '/api/v2.0/request/cleanup/';

     
        self.username = "mgarcias";
        self.password = "C1sco12345";
        self.requests = ko.observableArray();

        self.ajax = function(uri, method, data) {
            var request = {
                url: uri,
                type: method,
                contentType: "application/json",
                accepts: "application/json",
                cache: false,
                dataType: 'json',
                data: JSON.stringify(data),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", 
                        "Basic " + btoa(self.username + ":" + self.password));

                    moreHeaders=getQueryFilter();
                    for (var headerKey in moreHeaders) { 
                        xhr.setRequestHeader(headerKey,moreHeaders[headerKey]);
                    }
                    console.log("Reaching out to " + String(uri) + " with query headers ");
                    console.log(moreHeaders) ;


                },
                error: function(jqXHR) {
                    console.log("ajax error " + jqXHR.status);
                }
            };
            return $.ajax(request);
        }


        setInterval(function(){self.refreshDashboard()}, 30000);

        self.refreshDashboard = function(task) {

            self.requests([]);
            for (var controller in self.controllersAPI){ 
                console.log(controller);
                url='http://'+String(self.controllersAPI[controller])+String(self.requestsListAPI);
                console.log(url);
                self.ajax(url, 'GET').done(function(data) {
                    //alert(data.requests);
                    //console.log(data);
                    //console.log(data.requests.length);
                    //self.requests([]);
                    visibleFlag=true;
                    for (var i = 0; i < data.requests.length; i++) {
                        //console.log(i);
                        activeFlag=false;
                        errorFlag=false;
                        completeFlag=false;
                        queuedFlag=false;
                        toBeDeletedFlag=false;
                        startingFlag=false;
                        executedFlag=false;
                        cleaningFlag=false;
                        cancelledFlag=false;
                        scheduledFlag=false;
                        deletedFlag=false;

                        if (data.requests[i].demoStatus=='active'){activeFlag=true;}
                        if (data.requests[i].demoStatus=='error'){errorFlag=true;}
                        if (data.requests[i].demoStatus=='complete'){completeFlag=true;}
                        if (data.requests[i].demoStatus=='queued'){queuedFlag=true;}
                        if (data.requests[i].demoStatus=='toBeDeleted'){toBeDeletedFlag=true;}
                        if (data.requests[i].demoStatus=='starting'){startingFlag=true;}
                        if (data.requests[i].demoStatus=='executed'){executedFlag=true;}
                        if (data.requests[i].demoStatus=='cleaning'){cleaningFlag=true;}
                        if (data.requests[i].demoStatus=='cancelled'){cancelledFlag=true;}
                        if (data.requests[i].demoStatus=='scheduled'){scheduledFlag=true;}
                        if (data.requests[i].demoStatus=='deleted'){deletedFlag=true;}
               
                        fullTime=String(data.requests[i].time);
                        shortTime=String(fullTime.split(".",1));
                        shortTime=shortTime.replace(/T/g," ");

                        self.requests.push({
                            //Data
                            locationTag: ko.observable(data.requests[i].location),
                            dc: ko.observable(data.requests[i].dc),
                            demo: ko.observable(data.requests[i].demo),
                            comments: ko.observable(data.requests[i].comments),
                            owner: ko.observable(data.requests[i].owner),
                            id: ko.observable(data.requests[i].id),
                            time: ko.observable(shortTime),
                            api: 'http://'+String(self.controllersAPI[data.requests[i].dc]),
                            web: 'http://'+String(self.controllersURL[data.requests[i].dc]),

                            //Flags
                            activeFlag: ko.observable(activeFlag),
                            errorFlag: ko.observable(errorFlag),
                            queuedFlag: ko.observable(queuedFlag),
                            toBeDeletedFlag: ko.observable(toBeDeletedFlag),
                            startingFlag: ko.observable(startingFlag),
                            deletingFlag: ko.observable(deletingFlag),
                            cancelledFlag: ko.observable(cancelledFlag),
                            completeFlag: ko.observable(completeFlag),
                            executedFlag: ko.observable(executedFlag),
                            scheduledFlag: ko.observable(scheduledFlag),
                            deletedFlag: ko.observable(deletedFlag),
                            cleaningFlag: ko.observable(cleaningFlag),
                            visibleFlag: ko.observable(visibleFlag)
                        });
                    }
                self.requests.sort(function(left, right) {return left.time() == right.time()  ? 0 : (left.time()  < right.time()  ? 1 : -1) });
                });
            //self.requests.sort(function(left, right) {return left.time() == right.time()  ? 0 : (left.time()  < right.time()  ? 1 : -1) });
        }
    }


        self.restart = function(task) {
            self.ajax(task.api + String(self.requestRestartAPI)+ task.id(), 'POST',{}).done(function(data) {

                activeFlag=false;
                errorFlag=false;
                completeFlag=false;
                queuedFlag=false;
                toBeDeletedFlag=false;
                startingFlag=false;
                executedFlag=false;
                cleaningFlag=false;
                cancelledFlag=false;
                scheduledFlag=false

                if (data.requests[0].demoStatus=='active'){activeFlag=true;}
                if (data.requests[0].demoStatus=='error'){errorFlag=true;}
                if (data.requests[0].demoStatus=='complete'){completeFlag=true;}
                if (data.requests[0].demoStatus=='queued'){queuedFlag=true;}
                if (data.requests[0].demoStatus=='toBeDeleted'){toBeDeletedFlag=true;}
                if (data.requests[0].demoStatus=='starting'){startingFlag=true;}
                if (data.requests[0].demoStatus=='executed'){executedFlag=true;}
                if (data.requests[0].demoStatus=='cleaning'){cleaningFlag=true;}
                if (data.requests[0].demoStatus=='cancelled'){cancelledFlag=true;}
                if (data.requests[0].demoStatus=='scheduledFlag'){scheduledFlag=true;}
                if (data.requests[0].demoStatus=='deleted'){deletedFlag=true;}


                //console.log(data.requests);

                //data
                task.locationTag(data.requests[0].location);
                task.dc(data.requests[0].dc);
                task.demo(data.requests[0].demo);
                task.comments(data.requests[0].comments);
                task.owner(data.requests[0].owner);
                task.id(data.requests[0].id);
                fullTime=String(data.requests[0].time);
                shortTime=String(fullTime.split(".",1));
                shortTime=shortTime.replace(/T/g," ");
                task.time(shortTime);

                //flags
                task.activeFlag(activeFlag);
                task.errorFlag(errorFlag);
                task.queuedFlag(queuedFlag);
                task.toBeDeletedFlag(toBeDeletedFlag);
		  task.startingFlag(startingFlag);
		  task.executedFlag(executedFlag);
		  task.deletingFlag(deletingFlag);
		  task.cancelledFlag(cancelledFlag);
                task.completeFlag(completeFlag);
                task.scheduledFlag(scheduledFlag);
                task.deletedFlag(deletedFlag);
                task.cleaningFlag(cleaningFlag);
                task.visibleFlag(true);

                self.requests.sort(function(left, right) {return left.time() == right.time()  ? 0 : (left.time()  < right.time()  ? 1 : -1) });

            });
        }


        self.delete = function(task) {
            self.ajax(task.api + String(self.requestCleanupAPI)+ task.id(), 'POST', {}).done(function(data) {
                activeFlag=false;
                errorFlag=false;
                completeFlag=false;
                queuedFlag=false;
                toBeDeletedFlag=false;
                startingFlag=false;
                executedFlag=false;
                cleaningFlag=false;
                cancelledFlag=false;
                scheduledFlag=false;
                deletedFlag=false;

                if (data.requests[0].demoStatus=='active'){activeFlag=true;}
                if (data.requests[0].demoStatus=='error'){errorFlag=true;}
                if (data.requests[0].demoStatus=='complete'){completeFlag=true;}
                if (data.requests[0].demoStatus=='queued'){queuedFlag=true;}
                if (data.requests[0].demoStatus=='toBeDeleted'){toBeDeletedFlag=true;}
                if (data.requests[0].demoStatus=='starting'){startingFlag=true;}
                if (data.requests[0].demoStatus=='executed'){executedFlag=true;}
                if (data.requests[0].demoStatus=='cleaning'){cleaningFlag=true;}
                if (data.requests[0].demoStatus=='cancelled'){cancelledFlag=true;}
                if (data.requests[0].demoStatus=='scheduledFlag'){scheduledFlag=true;}
                if (data.requests[0].demoStatus=='deletedFlag'){deletedFlag=true;}
                console.log(data.requests);


                //data
                task.locationTag(data.requests[0].location);
                task.dc(data.requests[0].dc);
                task.demo(data.requests[0].demo);
                task.comments(data.requests[0].comments);
                task.owner(data.requests[0].owner);
                task.id(data.requests[0].id);
                fullTime=String(data.requests[0].time);
                shortTime=String(fullTime.split(".",1));
                shortTime=shortTime.replace(/T/g," ");
                task.time(shortTime);

                //flags
                task.activeFlag(activeFlag);
                task.errorFlag(errorFlag);
                task.queuedFlag(queuedFlag);
                task.toBeDeletedFlag(toBeDeletedFlag);
		  task.startingFlag(startingFlag);
		  task.executedFlag(executedFlag);
		  task.deletingFlag(deletingFlag);
		  task.cancelledFlag(cancelledFlag);
                task.completeFlag(completeFlag);
                task.scheduledFlag(scheduledFlag);
                task.deletedFlag(deletedFlag);
                task.cleaningFlag(cleaningFlag);
                task.visibleFlag(true);

                self.requests.sort(function(left, right) {return left.time() == right.time()  ? 0 : (left.time()  < right.time()  ? 1 : -1) });

            });
        }

        self.cancel = function(task) {
            //self.ajax(self.requestCancelURI + task.id(), 'POST', {}).done(function(data) {
            //});
            alert("Not supported yet");
        }

        self.show = function(task) {
           //var id = task.id()
           var win = window.open(task.web + self.requestStatusURI + task.id() , '_blank');
           win.focus();
        }


        self.showAll = function(task) {
            for (var i = 0; i < self.requests().length; i++) {
                self.requests()[i].visibleFlag(true);
            }
        }

        self.showQueued = function(task) {
            for (var i = 0; i < self.requests().length; i++) {
                self.requests()[i].visibleFlag(self.requests()[i].queuedFlag());
            }
        }

        self.showError = function(task) {
            for (var i = 0; i < self.requests().length; i++) {
                self.requests()[i].visibleFlag(self.requests()[i].errorFlag());
            }
        }

        self.showActive = function(task) {
            for (var i = 0; i < self.requests().length; i++) {
                self.requests()[i].visibleFlag(self.requests()[i].activeFlag());
            }
        }


        self.showComplete = function(task) {
            for (var i = 0; i < self.requests().length; i++) {
                self.requests()[i].visibleFlag(self.requests()[i].completeFlag()||self.requests()[i].executedFlag());
            }
        }

        self.showScheduled = function(task) {
            for (var i = 0; i < self.requests().length; i++) {
                self.requests()[i].visibleFlag(self.requests()[i].scheduledFlag());
            }
        }

        for (var controller in self.controllersAPI){ 
            console.log(controller);
            url='http://'+String(self.controllersAPI[controller])+String(self.requestsListAPI);
            self.ajax(url, 'GET').done(function(data) {
                //alert(data.requests);
                //console.log(data);
                //console.log(data.requests.length);
                for (var i = 0; i < data.requests.length; i++) {
                    activeFlag=false;
                    errorFlag=false;
                    completeFlag=false;
                    queuedFlag=false;
                    toBeDeletedFlag=false;
                    startingFlag=false;
                    executedFlag=false;
                    cleaningFlag=false;
                    cancelledFlag=false;
                    scheduledFlag=false;
                    deletedFlag=false;

                    if (data.requests[i].demoStatus=='active'){activeFlag=true;}
                    if (data.requests[i].demoStatus=='error'){errorFlag=true;}
                    if (data.requests[i].demoStatus=='complete'){completeFlag=true;}
                    if (data.requests[i].demoStatus=='queued'){queuedFlag=true;}
                    if (data.requests[i].demoStatus=='toBeDeleted'){toBeDeletedFlag=true;}
                    if (data.requests[i].demoStatus=='starting'){startingFlag=true;}
                    if (data.requests[i].demoStatus=='executed'){executedFlag=true;}
                    if (data.requests[i].demoStatus=='cleaning'){cleaningFlag=true;}
                    if (data.requests[i].demoStatus=='cancelled'){cancelledFlag=true;}
                    if (data.requests[i].demoStatus=='scheduledFlag'){scheduledFlag=true;}
                    if (data.requests[i].demoStatus=='deleted'){deletedFlag=true;}

                    fullTime=String(data.requests[i].time);
                    shortTime=String(fullTime.split(".",1));
                    shortTime=shortTime.replace(/T/g," ");

                    self.requests.push({
                        //Data
                        locationTag: ko.observable(data.requests[i].location),
                        dc: ko.observable(data.requests[i].dc),
                        demo: ko.observable(data.requests[i].demo),
                        comments: ko.observable(data.requests[i].comments),
                        owner: ko.observable(data.requests[i].owner),
                        id: ko.observable(data.requests[i].id),
                        time: ko.observable(shortTime),
                        api: 'http://'+String(self.controllersAPI[data.requests[i].dc]),
                        web: 'http://'+String(self.controllersURL[data.requests[i].dc]),

                        //Flags
                        activeFlag: ko.observable(activeFlag),
                        completeFlag: ko.observable(completeFlag),
                        errorFlag: ko.observable(errorFlag),
                        queuedFlag: ko.observable(queuedFlag), 
                        toBeDeletedFlag: ko.observable(toBeDeletedFlag),
		          startingFlag: ko.observable(startingFlag),
		          deletingFlag: ko.observable(deletingFlag),
		          cancelledFlag: ko.observable(cancelledFlag),
		          executedFlag: ko.observable(executedFlag),
                        scheduledFlag: ko.observable(scheduledFlag),
                        deletedFlag: ko.observable(deletedFlag),
                        cleaningFlag: ko.observable(cleaningFlag),
                        visibleFlag: ko.observable(visibleFlag)
                    });
                }
            self.requests.sort(function(left, right) {return left.time() == right.time()  ? 0 : (left.time()  < right.time()  ? 1 : -1) });
            });
        }
    }


    ko.applyBindings(new TasksViewModel(), $('#main')[0]);




    var queryTimerStart = new Pikaday(
    {
        field: document.getElementById('timeRangeStart'),
        firstDay: 1,
        minDate: new Date(2000, 0, 1),
        maxDate: new Date(2020, 12, 31),
        yearRange: [2000,2020],
        onSelect: function() {
            var date = document.createTextNode(this.getMoment().format('Do MMMM YYYY') + ' ');
            console.log(this.getMoment().format('YYYY-MM-DD'));//document.getElementById('selected').appendChild(date);
        }
    });

    var queryTimeEnd = new Pikaday(
    {
        field: document.getElementById('timeRangeEnd'),
        firstDay: 1,
        minDate: new Date(2018,0,1),
        maxDate: new Date(2020, 12, 31),
        yearRange: [2000,2020],
        onSelect: function() {
            var date = document.createTextNode(this.getMoment().format('Do MMMM YYYY') + ' ');
            console.log(this.getMoment().format('YYYY-MM-DD'));//document.getElementById('selected').appendChild(date);
        }
    });



    function getQueryFilter(){
      var headers={};
      if (document.getElementById('queryDemo').value != ""){headers['queryDemo']=document.getElementById('queryDemo').value;}
      if (document.getElementById('queryId').value != ""){headers['queryId']=document.getElementById('queryId').value;}
      if (document.getElementById('queryStatus').value != ""){headers['queryStatus']=getSelectValues('queryStatus');}
      if (document.getElementById('queryDc').value != ""){headers['queryDc']=getSelectValues('queryDc');}
      if (document.getElementById('queryUser').value != ""){headers['queryUser']=document.getElementById('queryUser').value;}
      if (document.getElementById('timeRangeStart').value != ""){headers['timeRangeStart']=document.getElementById('timeRangeStart').value;}//else{headers['timeRangeStart']=' now() - 20d ';}
      if (document.getElementById('timeRangeEnd').value != ""){headers['timeRangeEnd']=document.getElementById('timeRangeEnd').value;}//else{headers['timeRangeEnd']=' now() ';}
      if (document.getElementById('queryLocation').value != ""){headers['queryLocation']=document.getElementById('queryLocation').value;}

      //console.log("Headers are:");
      //console.log(headers);
      return headers;
    }

    function getSelectValues(select) {
      select=document.getElementById(select);
      var result="any";
      var options = select && select.options;
      var opt;
      for (var i=0, iLen=options.length; i<iLen; i++) {
        opt = options[i];

      if (opt.selected) {
          result=result+"|"+opt.value;
        }
      }
      return result;
    }

    </script>
</body>
</html>


