<!DOCTYPE html>
<html>
<head>
<title>dCloud in-demo Automation Controller</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script>
<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script src="scripts/generic.js"></script>


</head>
<body>

<?php


$id=$_GET['id'];
$apiServer=$_ENV['APISERVER'];
$webServer=$_ENV['WEBSERVER'];
$apiPort=$_ENV['APIPORT'];
$webPort=$_ENV['WEBPORT'];
$apiProtocol=$_ENV['APIPROTOCOL'];
$webProtocol=$_ENV['WEBPROTOCOL'];

$requestStatusUrl=$apiProtocol.'://'.$apiServer.':'.$apiPort.'/api/v2.0/request/'.$id;
$requestLogsUrl=$webProtocol.'://'.$webServer.':'.$webPort.'/showlogs.php?filter='.$id;



$webURL=$_SERVER[HTTP_HOST];
$apiURL=str_replace("web","api",$webURL);
$requestStatusUrl='http://'.$apiURL.'/api/v2.0/request/'.$id;

$requestLogsUrl='http://'.$webURL.'/showlogs.php?filter='.$id;

$requestRestartApi='http://'.$apiURL.'/api/v2.0/request/restart/'.$id;
$requestCleanupApi='http://'.$apiURL.'/api/v2.0/request/cleanup/'.$id;



?>


<!--<div class="progress"></div>
<div class="loading"><p align="center">Loading...</p></div>
-->

    <div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="#">dCloud in-demo Automation Controller: Request Details</a>
        </div>
    </div>
    <div id="main" class="container">
	
    <br>	


<div class="navbar"><div class="navbar-inner"><a class="brand" href="#" onClick="div_toggle('requestMainDetailsTable');">Request Main Details </a></div></div>
<div  id="requestMainDetailsTable"  style="overflow-x: auto;display:block;" >
<table class="table table-striped">
    <tr><td style="width: 1px;"><b>Status</b></td><td><b>Demo</b></td><td><b>DC</b></td><td><b>ID</b></td><td><b>Location</b></td><td><b>User</b></td><td><b>Recipe</b></td></tr>
    <!-- ko foreach: mainDetails -->
    <tr>
        <td>
            <span data-bind="visible: activeFlag" class="label label-success">Active</span>
            <span data-bind="visible: errorFlag" class="label label-important">Error</span>
            <span data-bind="visible: queuedFlag" class="label label-info">Queued</span>
            <span data-bind="visible: completeFlag" class="label label-success">Complete</span>
            <span data-bind="visible: toBeDeletedFlag" class="label label-info">Marked for cleanup</span>
	    <span data-bind="visible: startingFlag" class="label label-warning">Starting</span>
            <span data-bind="visible: executingFlag" class="label label-warning">Executing</span>
	    <span data-bind="visible: cleaningFlag" class="label label-warning">Cleaning</span>
	    <span data-bind="visible: cancelledFlag" class="label label-inverse">Cancelled</span>
        </td>
 
        <td><p data-bind="text: demo"></p></td>
        <td><p data-bind="text: datacenter">  </p></td>
        <td><p data-bind="text: id">  </p></td>
        <td><p data-bind="text: location"></p></td>
        <td><p data-bind="text: owner"></p></td>
        <td><p data-bind="text: recipeName"></p></td>
    </tr>
    <!-- /ko -->
        <tr>
        <td colspan="7">
            <input type="image" id="showhidearrow_requestDetails" style="width:15px;" src="images/icons/down_arrow.png" onClick="div_toggle_with_image('requestDetailsTable',this.id);">
        </td>  
    </tr> 
</table>
<br> 
<div  id="requestDetailsTable" hidden="true" style="overflow-x: auto;"></div>
</div>


<div class="navbar"><div class="navbar-inner"><a class="brand" href="#" onClick="div_toggle('requestHistoryTable');">Request History </a></div></div>
<div  id="requestHistoryTable"  style="overflow-x: auto;display:block;" >
<table class="table table-striped">
    <tr><td style="width: 1px;">Status</td><td><b>Demo</b></td><td><b>Time</b></td><td><b>Comments</b></td></tr>
    <!-- ko foreach: history -->
    <tr data-bind="visible: visibleFlag"  >
        <td >
            <span data-bind="visible: activeFlag" class="label label-success">Active</span>
            <span data-bind="visible: errorFlag" class="label label-important">Error</span>
            <span data-bind="visible: queuedFlag" class="label label-info">Queued</span>
            <span data-bind="visible: completeFlag" class="label label-success">Complete</span>
            <span data-bind="visible: toBeDeletedFlag" class="label label-info">Marked for cleanup</span>
	    <span data-bind="visible: startingFlag" class="label label-warning">Starting</span>
            <span data-bind="visible: executingFlag" class="label label-warning">Executing</span>
	    <span data-bind="visible: cleaningFlag" class="label label-warning">Cleaning</span>
	    <span data-bind="visible: cancelledFlag" class="label label-inverse">Cancelled</span>
        </td>
        <td><p><b data-bind="text: demo"></b></p></td>
        <td><p data-bind="text: time"></p></td>
        <td><p data-bind="text: comments"></p></td>
    </tr>
    <!-- /ko -->
</table><br> 
</div>


<div class="navbar"><div class="navbar-inner"><a class="brand" href="#" onClick="div_toggle('requestStartupTable');">Recipe StartUp Tasks </a></div></div>
<div  id="requestStartupTable"  style="overflow-x: auto;display:none;" >
<table class="table table-striped">
    <tr><td style="width: 1px;"><b>Status</b></td><td><b>Task Name</b></td><td><b>Comments</b></td></tr>
    <!-- ko foreach: startupTasks -->
    <tr>
        <td>
            <span data-bind="visible: successFlag" class="label label-success">Success</span>
            <span data-bind="visible: failedFlag" class="label label-important">Failed</span>
            <span data-bind="visible: unavailableFlag" class="label label-warning">Unavailable</span>
        </td>
        <td><p data-bind="text: taskName"></p></td>
        <td><p data-bind="text: comments"></p></td>
    </tr>
    <!-- /ko -->
    <tr>
        <td colspan="3">
            <input type="image" id="showhidearrow_requestStartupTasks" style="width:15px;" src="images/icons/down_arrow.png" onClick="div_toggle_with_image('requestStartupTasksTable',this.id);">
        </td>
    </tr>
</table><br> 
<div id="requestStartupTasksTable" hidden="true" style="overflow-x: auto;"></div>
</div>


<div class="navbar"><div class="navbar-inner"><a class="brand" href="#" onClick="div_toggle('requestCleanupTable');">Recipe CleanUp Tasks </a></div></div>
<div  id="requestCleanupTable"  style="overflow-x: auto;display:none;" >
<table class="table table-striped">
    <tr><td style="width: 1px;"><b>Status</b></td><td><b>Task Name</b></td><td><b>Comments</b></td></tr>
    <!-- ko foreach: cleanupTasks -->
    <tr>
        <td>
            <span data-bind="visible: successFlag" class="label label-success">Success</span>
            <span data-bind="visible: failedFlag" class="label label-important">Failed</span>
            <span data-bind="visible: unavailableFlag" class="label label-warning">Unavailable</span>
        </td>
        <td><p data-bind="text: taskName"></p></td>
        <td><p data-bind="text: comments"></p></td>
    </tr>
    <!-- /ko -->
    <tr>
        <td colspan="3">
            <input type="image" id="showhidearrow_requestCleanupTasks" style="width:15px;" src="images/icons/down_arrow.png" onClick="div_toggle_with_image('requestCleanupTasksTable',this.id);">
        </td>
    </tr>
    
</table>
<div id="requestCleanupTasksTable" hidden="true" style="overflow-x: auto;"></div>
</div>



<input type="image" id="showhidearrow_requestDetailsAll" style="width:15px;" src="images/icons/down_arrow.png" onClick="div_toggle_with_image('requestDetailsAll',this.id);">All Details
<br> 
<div  id="requestDetailsAll" hidden="true" style="overflow-x: auto;"></div>
</div>

<br>

</div>

    <script type="text/javascript">

    function TasksViewModel(demoStatus) {

        var self = this;
        var visibleFlag=true;
        var startingFlag='';
        var deletingFlag='';
        var cancelingFlag='';

        self.tasksURI = '<?php echo $requestStatusUrl;?>';
        self.history = ko.observableArray();
        self.startupTasks = ko.observableArray();
        self.cleanupTasks = ko.observableArray();
        self.mainDetails = ko.observableArray();
        self.tasks = ko.observableArray();

        self.showLogs = function(data){
        window.open('<?php echo $requestLogsUrl;?>',"_blank");
        }

        self.requestRestart= function(data){
        console.log('About to send restart request');
        self.ajax('<?php echo $requestRestartApi;?>', 'POST',{}).done(function(data){console.log('Restart Request received!');})
        }

        self.requestCleanup= function(data){
        console.log('About to send cleanup request');
        self.ajax('<?php echo $requestCleanupApi;?>', 'POST',{}).done(function(data){console.log('Cleanup Request received!');})

        }

        self.showOutput = function(data){
        //console.log(self.mainDetails()[0].outputUrl());
        window.open(self.mainDetails()[0].outputUrl(),"_blank");
        }

        self.ajax = function(uri, method, data) {
            var request = {
                url: uri,
                type: method,
                contentType: "application/json",
                accepts: "application/json",
                cache: false,
                dataType: 'json',
                data: JSON.stringify(data),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization","Basic " + btoa(self.username + ":" + self.password));
                },
                error: function(jqXHR) {
                    console.log("ajax error " + jqXHR.status);
                    //alert('scheise')
                }
            };
            //alert(JSON.stringify(request))
            return $.ajax(request);
        }

        //alert('here')	
        self.ajax(self.tasksURI , 'GET').done(function(data) {

            console.log(data);
            activeFlag=false;
            errorFlag=false;
            completeFlag=false;
            queuedFlag=false;
            toBeDeletedFlag=false;
            startingFlag=false;
            executingFlag=false;
            cleaningFlag=false;
            cancelledFlag=false;

            if (data.status=='active'){activeFlag=true;}
            if (data.status=='error'){errorFlag=true;}
            if (data.status=='complete'){completeFlag=true;}
            if (data.status=='queued'){queuedFlag=true;}
            if (data.status=='toBeDeleted'){toBeDeletedFlag=true;}
            if (data.status=='starting'){startingFlag=true;}
            if (data.status=='executed'){executedFlag=true;}
            if (data.status=='cleaning'){cleaningFlag=true;}                
            if (data.status=='cancelled'){cancelledFlag=true;}

            self.mainDetails.push({
                activeFlag:      ko.observable(activeFlag),
                errorFlag:       ko.observable(errorFlag),
                completeFlag:    ko.observable(completeFlag),
                queuedFlag:      ko.observable(queuedFlag), 
                toBeDeletedFlag: ko.observable(toBeDeletedFlag),
	        startingFlag:    ko.observable(startingFlag),
                executingFlag:   ko.observable(executingFlag),
	        cleaningFlag:    ko.observable(cleaningFlag),
	        cancelledFlag:   ko.observable(cancelledFlag), 
                datacenter:      ko.observable(data.request.datacenter),
                demo:            ko.observable(data.request.demo),
                owner:           ko.observable(data.request.owner),
                id:              ko.observable(data.request.id),
                location:        ko.observable(data.request.location),
                recipeName:      ko.observable(data.request.recipeName),
                outputUrl:      ko.observable(data.outputUrl)

            });

             
            document.getElementById('requestDetailsTable').innerHTML = '<pre>'+JSON.stringify(data.request,undefined,2)+'</pre>';
            document.getElementById('requestDetailsAll').innerHTML = '<pre>'+JSON.stringify(data,undefined,2)+'</pre>';


            for (var i = 0; i < data.history.length; i++) {
                //alert(data.session[i].errorFlag);
                activeFlag=false;
                errorFlag=false;
                completeFlag=false;
                queuedFlag=false;
                toBeDeletedFlag=false;
                startingFlag=false;
                executingFlag=false;
                cleaningFlag=false;
                cancelledFlag=false;
                if (data.history[i].demoStatus=='active'){activeFlag=true;}
                if (data.history[i].demoStatus=='error'){errorFlag=true;}
                if (data.history[i].demoStatus=='complete'){completeFlag=true;}
                if (data.history[i].demoStatus=='queued'){queuedFlag=true;}
                if (data.history[i].demoStatus=='toBeDeleted'){toBeDeletedFlag=true;}
                if (data.history[i].demoStatus=='starting'){startingFlag=true;}
                if (data.history[i].demoStatus=='executed'){executedFlag=true;}
                if (data.history[i].demoStatus=='cleaning'){cleaningFlag=true;}                
                if (data.history[i].demoStatus=='cancelled'){cancelledFlag=true;}

                self.history.push({
                    demo:            ko.observable(data.history[i].demo),
                    comments:        ko.observable(data.history[i].comments),
                    time:            ko.observable(data.history[i].time),
                    activeFlag:      ko.observable(activeFlag),
                    errorFlag:       ko.observable(errorFlag),
                    completeFlag:    ko.observable(completeFlag),
                    queuedFlag:      ko.observable(queuedFlag), 
                    toBeDeletedFlag: ko.observable(toBeDeletedFlag),
		    startingFlag:    ko.observable(startingFlag),
                    executingFlag:   ko.observable(executingFlag),
		    cleaningFlag:    ko.observable(cleaningFlag),
		    cancelledFlag:   ko.observable(cancelledFlag),
                    visibleFlag:     ko.observable(visibleFlag)
 
                });
            }


            document.getElementById('requestStartupTasksTable').innerHTML =  '<pre>'+JSON.stringify(data.tasks.Start,undefined,2)+'</pre>';
            for (var i = 0; i < data.tasks.Start.length; i++) {
                successFlag=false;
                failedFlag=false;
                unavailableFlag=false;
                if(data.tasks.Start[i].status=='Success'){successFlag=true}
                if(data.tasks.Start[i].status.includes('Failed')){failedFlag=true}
                if(data.tasks.Start[i].status=='unavailable'){unavailableFlag=true}

                self.startupTasks.push({
                    comments:        ko.observable(data.tasks.Start[i].status),
                    successFlag:     ko.observable(successFlag),
                    failedFlag:      ko.observable(failedFlag),
                    unavailableFlag: ko.observable(unavailableFlag),
                    taskName:        ko.observable(data.tasks.Start[i].name)
                });
            }



            document.getElementById('requestCleanupTasksTable').innerHTML = '<pre>'+JSON.stringify(data.tasks.Stop,undefined,2)+'</pre>';
            for (var i = 0; i < data.tasks.Stop.length; i++) {
                successFlag=false;
                failedFlag=false;
                unavailableFlag=false;
                if(data.tasks.Stop[i].status=='Success'){successFlag=true}
                if(data.tasks.Stop[i].status.includes('Failed')){failedFlag=true}
                if(data.tasks.Stop[i].status=='unavailable'){unavailableFlag=true}

                self.cleanupTasks.push({
                    comments:        ko.observable(data.tasks.Stop[i].status),
                    successFlag:     ko.observable(successFlag),
                    failedFlag:      ko.observable(failedFlag),
                    unavailableFlag: ko.observable(unavailableFlag),
                    taskName:        ko.observable(data.tasks.Stop[i].name)
                });
            }


        });
    }

    ko.applyBindings(new TasksViewModel(), $('#main')[0]);


    function showLogsInNewTab(){
        window.open('<?php echo $requestLogsUrl;?>',"_blank");
    }

    </script>
</body>
</html>
