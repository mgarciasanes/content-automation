<!DOCTYPE html>
<html>
<head>
<title>DCV Automation Controller</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">



<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script>
<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
<script src="scripts/generic.js"></script>
<link rel="stylesheet" href="css/flying.css">

</head>
<body >

<?php


$id=$_GET['id'];
$apiServer=$_ENV['APISERVER'];
$webServer=$_ENV['WEBSERVER'];
$apiPort=$_ENV['APIPORT'];
$webPort=$_ENV['WEBPORT'];
$apiProtocol=$_ENV['APIPROTOCOL'];
$webProtocol=$_ENV['WEBPROTOCOL'];

$requestStatusUrl=$apiProtocol.'://'.$apiServer.':'.$apiPort.'/api/v2.0/request/'.$id;
$requestLogsUrl=$webProtocol.'://'.$webServer.':'.$webPort.'/showlogs.php?filter='.$id;
$errorUrl=$webProtocol.'://'.$webServer.':'.$webPort.'/errorPage.php?id='.$id;


$webURL=$_SERVER[HTTP_HOST];
$apiURL=str_replace("web","api",$webURL);
$requestStatusUrl='http://'.$apiURL.'/api/v2.0/request/'.$id;

$requestLogsUrl='http://'.$webURL.'/showlogs.php?filter='.$id;

$requestRestartApi='http://'.$apiURL.'/api/v2.0/request/restart/'.$id;
$requestCleanupApi='http://'.$apiURL.'/api/v2.0/request/cleanup/'.$id;






?>

<div class='body'>
  <span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </span>
  <div class='base'>
    <span></span>
    <div class='face'></div>
  </div>
</div>
<div class='longfazers'>
  <span></span>
  <span></span>
  <span></span>
  <span></span>
</div>
<br><br>
<h4>Still on the way...  Give us a minute!. </h4>





<script type="text/javascript">

    function checkRequestStatus(demoStatus) {
        var self = this;
        self.tasksURI = '<?php echo $requestStatusUrl;?>';
       self.ajax = function(uri, method, data) {
            var request = {
                url: uri,
                type: method,
                contentType: "application/json",
               accepts: "application/json",
                cache: false,
                dataType: 'json',
                data: JSON.stringify(data),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization","Basic " + btoa(self.username + ":" + self.password));
                },
                error: function(jqXHR) {
                    console.log("ajax error " + jqXHR.status);
                    //alert('scheise')
                }
            };
            //alert(JSON.stringify(request))
            return $.ajax(request);
        }

        //alert('here')	
        self.ajax(self.tasksURI , 'GET','').done(function(data) {
            //alert('now here');
            console.log(data)
            if (data.status=='active' || data.status=='executed') {window.open(data.outputUrl, "_self");}
            if (data.status=='error' ||  data.status=='cancelled' ||  data.status=='toBeDeleted' ||  data.status=='complete'){window.open(data.errorUrl, "_self");}

        });
    }

    //ko.applyBindings(new TasksViewModel(), $('#main')[0]);

    setInterval(function(){
    checkRequestStatus()}, 3000)


    </script>
</body>
</html>
