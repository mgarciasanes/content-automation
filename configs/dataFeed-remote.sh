
echo "DEST-DC $DESTDC"
echo "SOURCE-DC $SOURCEDC"

######################################################################################
#Influx Settings
######################################################################################
#dbIp="198.19.254.59"
dbIp="telemetry-central.ciscodcloud.com"
dbPort="8086"
dbPort="443"
dbProt="https"
dbName="proxystats_dcv"
sName="dcvSessions"
httpAuth='enabled'  # enabled/disabled
dbUser='proxystats_dcv'
dbPass='bRustaTruca!8c6t'


######################################################################################
#RabbitMQ Settings
######################################################################################
#Outside Kube
#rabbitClientUrl="/cli/rabbitmqadmin"
#rabbitAdminPort="30001"
#rabbitIp="198.19.254.51"
#rabbitPort="30001"

#Inside Kube
#rabbitClientUrl="/cli/rabbitmqadmin"
#rabbitAdminPort="15672"
#rabbitIp="content-automation-rabbitmq.master"
#rabbitPort="15672"


#Remote Rabbitmq
rabbitClientUrl="cli/rabbitmqadmin"
rabbitAdminPort="24002"
rabbitIp="dcv-automation-amqp.svpod.dc-$DESTDC.com"
rabbitPort="24002"



#Queue
exchangeName="amq.default"
routeKey="HAPROXY-01.dcloud.$SOURCEDC.sharedservices"

#Creds
rabbitUser='guest'
rabbitPass='guest'


######################################################################################
#Global Tags
######################################################################################
DC="SNG"
HOST=$HOSTNAME
globalTags="dc=$DC,host=$HOST,"


######################################################################################
#Script Settings
######################################################################################
sourceFile=/root/logs/rsyslog_messages
workDir=/root/metricsFeed
pipe=$workDir/cookiePipe
dumpFile=$workDir/dumpFile
tmpFile=$workDir/tmpFile
#logFile=$workDir/metricsFeed.log
logFile=/root/logs/metricsFeed-$SOURCEDC-$DESTDC.log
tailPidFile=$workDir/tail.pid
sleepPidFile=$workDir/sleep.pid
mainPidFile=$workDir/main.pid


######################################################################################
#Create Work Directory, Pipe and register main PID to file
######################################################################################
if [[ ! -d $workDir ]];
then
    echo "Work directory  $workDir not found. Creating it..." >>  $logFile
    mkdir -p  $workDir
fi
echo $$ > $mainPidFile



######################################################################################
# Download RABBITMQ client
######################################################################################
if [[ ! -f $workDir/rabbitmqadmin ]];
then
    echo "Rabbit client not found, will try to download it now..." >>  $logFile
    cd $workDir; wget http://$rabbitIp:$rabbitAdminPort/$rabbitClientUrl; chmod 777 $workDir/rabbitmqadmin
fi

######################################################################################
#Global Variables
######################################################################################
#INFLUXDB
if [[ $httpAuth == "enabled" ]];
then
    curlCommandWithGlobalTags="curl -XPOST -k -s '$dbProt://$dbIp:$dbPort/write?db=$dbName' -u $dbUser:$dbPass --data-binary '$sName,$globalTags"
else
    curlCommandWithGlobalTags="curl -XPOST -k -s '$dbProt://$dbIp:$dbPort/write?db=$dbName' --data-binary '$sName,$globalTags"
fi

#RABBITMQ
rabbitCommand="$workDir/rabbitmqadmin  --host=$rabbitIp   --port=$rabbitPort --username=$rabbitUser --password=$rabbitPass publish exchange=$exchangeName  routing_key=$routeKey  payload="


######################################################################################
# Starting to tail the source file  file on a subshell
# The tail stream is filtered in order to get certain cookies only dumped to $dumpFile
# We also register the PID of the tail process into a file
######################################################################################
(
  set -e
  echo "Tracking $sourceFile  for cookie info and streaming info to file $dumpFile" >>  $logFile
  pid=$BASHPID
  echo $pid  > $tailPidFile
  echo "PID of tail process is:" >> $logFile
  cat $tailPidFile >>  $logFile
  #tail  -F $sourceFile  | stdbuf -o0 awk -F"]:" '{print $2 }' | stdbuf -o0 awk -F";" '{for(i=1;i<=NF;i++){if($i~/^( dcloud=| SRVNAME=| origin=| referer=| username=| demo=)/){a=a","$i}} print a; a=""; }' | stdbuf -o0 awk '{ gsub (" ", "", $0); print}'  >> $dumpFile
  tail -n 0 -F $sourceFile   >> $dumpFile


) &


######################################################################################
# Timer loop in a subshell. It will send a USR1 signal to the parten pid every 20s.
# We also register the PID of the sleep process into a file
######################################################################################
echo "Starting timer loop..." >>  $logFile
(
   set -e
   pid=$BASHPID
   echo $pid  > $sleepPidFile
   echo "PID of sleep process is:" >> $logFile
   cat $sleepPidFile >>  $logFile

  while true
  do
    sleep 20
    # Since this is run as a subshell (instead of an external command),the parent pid is $$, not $PPID.
    kill -USR1 $$
  done
) &
#pid=$!;echo $pid  > $sleepPidFile;echo "PID of sleep process is:" >> $logFile ;cat $sleepPidFile >>  $logFile


######################################################################################
# Functions that read and process content from variable cookieDump
# After being processed,content is sent to influxDB using curl
######################################################################################
send2influx(){
    echo "$(date) Sending Data..."  >>  $logFile

    echo "$(date) Fetching data from $dumpFile, and emptying it..." >> $logFile
    
    rm -f $tmpFile ; cp $dumpFile $tmpFile ; echo "" > $dumpFile;

    listOfSessions=""
    dataBucket=""
    while read cookie
    do
        #echo $cookie >> $logFile
        dcloud=$(echo $cookie | sed  -rn 's/.*(dcloud=)([a-z0-9A-Z]*);.*/\2 /p')
        #session=$(echo $cookie |awk -F"," '{for(i=0;i<=NF;i++){if($i~/^(dcloud=)/){split($i,a,"=");}} print a[2]}')
        #demo=$(echo $cookie |awk -F"," '{for(i=0;i<=NF;i++){if($i~/^(demo=)/){split($i,a,"=");}} print a[2]}')
        #source=$(echo $cookie |awk -F"," '{for(i=0;i<=NF;i++){if($i~/^(source=)/){split($i,a,"=");}} print a[2]"_"a[3]}')
        #origin=$(echo $cookie |awk -F"," '{for(i=0;i<=NF;i++){if($i~/^(origin=)/){split($i,a,"=");}} print a[2]}')
        #username=$(echo $cookie |awk -F"," '{for(i=0;i<=NF;i++){if($i~/^(owner=)/){split($i,a,"userid=");split(a[2],b,"HTTP")}} print b[1]}')
        #datacenter
        #recipePath
        #recipeName
        #owner
        #urlPath
        #host


        #INFLUXDB
        #if [[ $session == "" ]];
        #then
        #    echo "No session cookie found. Ignorning..."  >>  $logFile
        #else
        #    if [[ $listOfSessions == *$session* ]];
        #    then
        #        :
        #        #echo "Already sent cookie for this session $session  in this round, skipping..."
        #    else
        #        listOfSessions=$listOfSessions$session
        #        now=`date +%s`
        #        duration=$((now-session))
        #        #echo $curlCommandWithGlobalTags"sessionTag="$session",refTag="$referer",originTag="$origin",usernameTag="$username",demoTag="$demo$' demo="'$demo$'",duration="'$duration$'"'$"'" >>  $logFile
        #        echo $curlCommandWithGlobalTags"sessionTag="$session",refTag="$referer",originTag="$origin",usernameTag="$username",demoTag="$demo$' demo="'$demo$'",duration='$duration$'i,session='$session$'i'"'" >>  $logFile
        #        eval $curlCommandWithGlobalTags"sessionTag="$session",refTag="$referer",originTag="$origin",usernameTag="$username",demoTag="$demo$' demo="'$demo$'",duration='$duration$'i,session='$session$'i'"'" >>  $logFile
        #    fi
        #fi


        #RABBITMQ
        if [[ $dataBucket  == *$dcloud* ]];
            then
                if [ "$dcloud" == "" ];
                    then
                        CMD=$rabbitCommand$"'"$cookie$"'"
                        eval $CMD 1> /dev/null 2>> $logFile
                    else
                        echo "Already sent this data in this round, skipping...: $dcloud" >> $logFile
                fi
            else
                dataBucket=$dataBucket$dcloud
                #now=`date +%s`
                #duration=$((now-session))
                CMD=$rabbitCommand$"'"$cookie$"'"
                #echo "$(date): $CMD"  >> $logFile
                eval $CMD 1> /dev/null 2>> $logFile
        fi


    done < $tmpFile
}

######################################################################################
# Trap configuration
# Configure Trap for process termination and for USR1 (timer)
######################################################################################
#trap "kill -9 `cat $tailPidFile`; rm -f  $tailPidFile;kill -9 `cat $sleepPidFile`; rm -f  $sleepPidFile;" EXIT SIGHUP SIGINT SIGTERM
#trap "kill 0;" EXIT SIGHUP SIGINT SIGTERM
trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT
trap send2influx USR1


######################################################################################
# Infinite Loop that reads from the named pipe cookiePipe
# The content is stored in a variable cookieDump
######################################################################################

sleep 5s
while :
do
if [[ ! -f $tailPidFile ]];
then
    echo "PID file for tail process was not found. Aborting.." >>  $logFile
    exit
fi
sleep 5s
done

