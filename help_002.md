
## <a name="controllermaster" > </a>Master Controller Instance

Sometimes referred to as the production controller. Is is the main instance (or set of instances) of the controller that executes on tested an reliable code.  At the moment, a controller  communicates with the world on the following ways:

- **API:**  		The controller master instance listens on port `31080` for API requests
- **Dashboard:**  	The controller master instance has a dashboard accessible on `http://dcv-automation-api:31080/api/v1.0/dashboard`
- **Database:**  	The controller master instance relies on an Influx database which has an API port `31886`
            	There is also a web interface for this instance that is available ar `http://dcv-automation-api:31883`
- **Code:**			The code used by the controller master (as well as by any development instance of the controller) instance is stored on the [Local Repository](#localrepository) and is accessible via SSH/SCP. 
  
Note1: The code stored on the Local Repository is currently "manually"" synchronised with Github


## <a name="controllerdevelopment" > </a>Development Controller Instances
 
Besides the master instance of the automation controller (sometimes referred to as production), there are additional instances of the controller that are dynamically created with the purpose of development and troubleshooting. These additional instances are referred to as Development instances. 
 
Both, the master and the development instances of the controller are all hosted on the same Kubernetes Cluster in each site.  

In principle the development instances are identical to the master. Since they are hosted on the same cluster, they are configured to be accessed in a unique way (API ports) . 

As described  [before](#controllermaster) , the API port used by the master instance is `31080`.  For the development instances we have a range of ports: `31081-31099` 

When a development instance is created the API port is assigned. In fact, the API port itself will be part of the name of the branch, as described [later](#branchnaming). 

Each development instance has the same ways to communicat with the external world:

- **API:**  		The development instance listens on port `310[82-99]` for API requests
- **Dashboard:**   	The development instance has a dashboard accessible on `http://dcv-automation-api:310[82-99]/api/v1.0/dashboard`
- **Database:**  	The development instance relies on an Influx database which has an API port `310[82-99]`
            	There is also a web interface for this instance that is available ar `http://dcv-automation-api:312[82-99]`
- **Code:**			The code used by the development instance  is stored on the [Local Repository](#localrepository) and is accessible via SSH/SCP. 
  

Note1: The code stored on the Local Repository is currently "manually"" synchronised with Github


## Controller and Github Branches

Each controller instance, either master or development , has access to its own independent set of code. This code is copied directly from the master at creation time. 

When a development instance is created, a new branch (Github) is created from the master branch. This new branch is then downloaded to the Local Repository to be used by the development instance. 

 
### <a name="branchnaming" > </a>Branch Naming Convention

The branches are named by convention after the owner (or the requester) and the API port to be used as described below:

    <owner>-<API port>
    If the user is mgarcias and the assigned port is 31081, the branch name will be mgarcias-31081 
  

### <a name="portconvention" > </a>Port Convention

Besides the master instance, which ports are described [here](#controllermaster), the development instances have a range of API ports. Here a description of other ports used by the development instances:

As mentioned, the range of API ports available is `31081-31099`. But all the controller instances (the master too) use more than one single port:
  
    - Controller API Port:  310[82-99]  
    - Database WEB Interface Port:  311[82-99]
    - Database API port:  312[82-99]

    For example, the ports for branch mgarcias-31081 will be:

    - Controller API Port:  31081  
    - Database WEB Interface Port:  31181
    - Database API port:  31281
  
  


### <a name="localrepository" > </a>Local Repository (Editing your code)

Each site (LON, RTP, SJC, SNG, CHI) has its own Local Repository. 

Access is posible via SCP (for code editing) and SSH for Branch tasks as Push/Pull:

    The IP address is 198.19.254.160
    
The credentials are unique for each controller instance and are described [here](#credentials)
    
The Local Repository is used to store:

    - Code (for all the instances, including the master)  This is in synch with Github
    - Logs  All the logs are centralised stored here 
    - Database  The Database data for each  controller instance is stored here.
    - Repo  Used for credentials, licenses and other confidential data 

<img src="https://github.com/dcloud-automation/content-automation/blob/master/html/local-repository.png"> </img>

As you can see on the picture, the Local Repository hosts four folder structures, code, logs, db and repo. They are all shared via NFS and SMB. 

When a development instance is created:  

  - A branch is cloned from the master in Github
  - A folder is created in the Code three, also with the same name as the branch name. The code is actually downloaded from Github (not copied from the local master folder) and stored on a folder. 
  - A folder is created in the Logs tree, also with the same name as the branch name. This folder is empty and will be used by the crated instance to store logs. 
  - A folder is created in the DB tree, also with the same name as the brach name. This folder is empty and will be used by the created instance to stora data. 
  - A user is created on the Local Repository with access to this specific folder.  **A set of [credentials](#credentials)** is provided to the owner in order to edit code while developing and or troubleshooting recipes and modules. 

Note1: The data contained is REPO is currently being synchronised across sites manually. Due to security policies restrictions, we do not use Github for this purpose. We are currently working on an automatic system to sync.

Note2: Contrary to Code, logs and db that add folders when a controller instance is created, repo is unique and is used by all the active controller instances (master and development). Access to this repo is possible over SSH/SCP. Credentials are provided to each team. 

Note3: One folder in each tree (code, logs and db)  will be created for each development instance 


### <a name="credentials" > </a> Local Repository: Credentials

By default, the credentials are created using the brach name as username and password.

    For branch mgarcias-31081, credentials are
    username: mgarcias-31081
    password: mgarcias-31081
    

### Example:

Branch Name: `mgarcias-31081`

This branch belongs to mgarcias and uses port 31081

#### Ports: 

    - Controller API Port: 31081
    - DB Web Interface Port: 31181 
    - Db API port: 31281 

#### Access to Dashboard for this specific branch 

    http://dcv-automation-api:31081/api/v1.0/dashboard 

#### API to Schedule new Requests for this specific branch: </br>

    http://198.19.254.51:31081/api/v1.0/task 

#### CURL Command that send new requests to this specific branch (You have to edit the recipeName, recipePath and other values accordinly)

    curl -i -H "Content-Type: text/xml" -X POST -d '<?xml version="1.0" encoding="UTF-8"?><session><demo></demo><recipeName>sparkTest</recipeName><recipePath>/col</recipePath><datacenter>RTP</datacenter><id>XXXX</id><owner>testing</owner><vpod>90</vpod><anycpwd>95f76f</anycpwd><type>instant</type><devices></devices><translations></translations><dids></dids></session>' http://198.19.254.51:31081/api/v1.0/task
    



### Kubernetes Cluster (Master and Nodes)


### High Availability 


