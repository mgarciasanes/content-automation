
#####################################
# Global Variables
#####################################
modulesPath=r'/root/scripts/modules/'

#####################################
# Import Modules
#####################################
import sys
sys.path.append(modulesPath)
import os
import time
import subprocess
import logger
import logging

def isActive(pid):        
    try:
        os.kill(pid, 0)
    except OSError:
        return False
    else:
        return True

def startProc(proc):
    logger.autolog(message="Starting Process " + str(proc["cmd"]))
    try:
        proc["pid"]=subprocess.Popen(proc["cmd"]).pid
        proc["status"]="OK"
        logger.autolog(message="Command " + str(proc["cmd"]) + " Started OK. PID is " + str(proc['pid']))
    except Exception as e:
        logger.autolog(message="Command " + str(proc["cmd"]) + " failed to start")
        logger.autolog(message=str(e))            
        proc["status"]=str(e)

    return proc

if __name__ == '__main__':

    logger.setLogFiles(newLogFile='starter.log',newLogDebugFile='starter_DEBUG.log')
    logger.setLogger()

    #os.system('/usr/sbin/sshd -D &')
    #os.system('/usr/bin/python /root/scripts/checkActiveRequests.py &')
    #os.system('/usr/bin/python /root/scripts/checkQueuedRequests.py &')
    #os.system('/usr/bin/python /root/scripts/api.py &')

    processes= [ {"cmd": ['/usr/sbin/sshd','-D'],"pid": "none", "status" : "none"}, 
                 {"cmd": [r'/usr/bin/python',r'/root/scripts/api.py'] ,"pid": "none", "status" : "none"},
                 {"cmd": [r'/usr/bin/python',r'/root/scripts/checkActiveRequests.py'] ,"pid": "none", "status" : "none"},
                 {"cmd": [r'/usr/bin/python',r'/root/scripts/checkQueuedRequests.py'] ,"pid": "none", "status" : "none"}]

    for proc  in processes:
        proc=startProc(proc)

    while True:
        for proc in processes:
            if proc['status']=='OK':
                if isActive(proc['pid']):
                    logger.autolog(message="Command " + str(proc["cmd"]) + " with pid " + str(proc["pid"]) + " and status " + str(proc["status"]))
                else:
                    proc['status']="Restarting..."
                    proc['pid']="none"
                    logger.autolog(message="Command " + str(proc["cmd"]) + " with pid " + str(proc["pid"]) + " was not active. WIll retart ")
            else:
                logger.autolog(message="Command  to restart " + str(proc["cmd"]))
                startProc(proc)

        logger.autolog(message=str(processes))
        time.sleep(10)

