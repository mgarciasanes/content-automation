
[Back to Main Page](https://github.com/dcloud-automation/content-automation/blob/master/README.md)




## Step by Step Configuration Process (by example)

Here we try to describe the steps to be followed in order to configure a dcloud demo to **use the automation server for IN-DEMO purposes only**.

### <a name="step1"></a> Step 1. Define the main values to be used. We assume we start working with a DEV instance of the controller. (Not production)

The values are specified by the following arguments:

    --indemo-recipe:      esa-indemo-v1
    --indemo-recipe-path: sec/
    --branch-name:        swadvani-31091
    --api-port:           31091
    --db-api-port:        31291


### <a name="step2"></a>Step 2. Update the Bootstrap script 

The latest version of the bootstrap.sh is located in Github [here](https://github.com/dcloud-automation/content-automation/tree/master/indemo/common)
In case an update is needed, download and replace the existing one. 

Spin up a session of your content, SSH to the Centos7-tools1 VM,delete the existing bootstrap.sh file and download the latest version of the script. The following code should do it:

    mkdir /root/tmp
    rm -f /root/bootstrap.sh
    git clone -b master https://github.com/dcloud-automation/content-automation.git /root/tmp
    cp /root/tmp/indemo/common/bootstrap.sh /root/
    chmod 777 /root/bootstrap.sh
    rm -fr /root/tmp

Once the bootstrap.sh file has been downloaded,change the permission of bootstrap.sh file to 777 and proceed to save the session.

**Note1:** For more information about the bootstrap.sh script, please click [here](https://github.com/dcloud-automation/content-automation/blob/master/indemo/common/bootstrap.md)

**Note2:** Make sure the Centos7-tools1 VM is part of the content you are working on. It can be found as a template in topology builder under the name Centos7-tools1

**TO BE SOLVED: At the moment, we are unable to mount NFS shares on the Centos7-tools machine via the auto.sh script. Although we are able to push the required configuration, mounts will not be persistent and the docker container will fail to access the mounts. The current workaround is to force a reboot at the end of auto.sh, the reboot process trigers the configuration and makes the mounts persistent. Since the docker container is configured no "restart --always", the container will start properly after boot with access to the NFS mounts**

**Possible solution is to pre-configure the Centos7-tools VM to mount the NFS shares at boot, so this does not have to be done by auto.sh.  This is the code to pre-configure the Centos7 VM:**


    REPOIP="198.19.254.160"
    mkdir -p /var/nfs/code
    mkdir -p /var/nfs/logs
    mkdir -p /var/nfs/repo
    echo "$REPOIP:/var/nfs/code /var/nfs/code  nfs  defaults    0   0" >> /etc/fstab
    echo "$REPOIP:/var/nfs/logs /var/nfs/logs  nfs  defaults  0   0" >> /etc/fstab
    echo "$REPOIP:/var/nfs/repo /var/nfs/repo  nfs    defaults  0   0" >> /etc/fstab



### <a name="step3"></a>Step 3. Configure Topology Builder  to execute the boostrap script on Centos7-tools1 VM 
On the session saved on the step above, go to Topology Builder and configure "Guest Automation" to point to the DEV instance of the controller specified on [Step 1](#step1)


<img src="https://github.com/dcloud-automation/content-automation/blob/master/html/topology-builder-centos7-tools1.png"> </img> 


The command should be something like:

    /root/bootstrap.sh --indemo-recipe esa-indemo-v1 --indemo-recipe-path sec/ --branch-name swadvani-31091 --api-port 31091 --db-api-port 31291

**Note1:** We will use default auto.sh located in indemo/common/auto.sh in the selected branch (in this example swadvani-31091)  so no need to specify its location with --auto-path

**Note2:** auto.sh is a script that will be downloaded and executed by bootstrap.sh  For more information about  auto.sh click [here](https://github.com/dcloud-automation/content-automation/blob/master/indemo/common/auto.md)

**Note3:** Make sure the Automation Guest is enabled on the Centos7-tools1 VM, so a session.xml file is created at /dcloud/session.xml

**Note4:** Make sure a credential entry for the influxDb is added to the creds.cfg file on the repo/common folder. If not sure, please ask.


### <a name="step4"></a>Step 4. Spin up a session of the demo and Verify status on DEV Dashboard 

Status should be visible on http://dcv-automation-api:31091/api/v1.0/dashboard 

**Note1:** Notice that we are using the api-port specficied on [Step 1](#step1):  31091

**Note2:** The workstation where you are trying to see the dashboard should be able to resolve the dcv-automation-api name to 198.19.254.51. If not, modify your `C:\Windows\System32\drivers\etc\hosts`  file adding the following line:

    198.19.254.51		dcv-automation-api
    
You will need administrator permissions on your workstation



### <a name="step5"></a>Step 5. Customize auto.sh (create our own) 
The `auto.sh` located in `indemo/common/auto.sh` is provided as a template. In most of the cases it will need to be customised for your specific content requirements. 

Connect to the code repository with your controller's instance credentials. For more information on connecting to the code repository, click here. 

Create a copy of `indemo/common/auto.sh` and save it in `indemo/sec/esa-indemo-v1/auto.sh` 

Modify the new `auto.sh` file in order to add a new "demo" node to the XML file. By doing this, the name of the demo will be shown on the dashboard.

Open the `auto.sh` for edition and locate the "indemo" section. You will find it by looking for the section marked with:

    ##################################
    #  IN-DEMO AUTOMATION PROCESSING
    ##################################

Once in that section, find the XML Preparation section marked as:

    #########################################
    # Prepare XML for Demo Automation Request
    ########################################    

After the duplication of the session file, add the following lines:

    #Add Demo tag to the session file (if not present already)
    grep '<demo>' /dcloud/session-indemo.xml 2>&1 1>/dev/null || sed -i '/<\/session>/i <demo>ESA DEMO<\/demo>' /dcloud/session-indemo.xml
    
    
This will add a demo node into the session.xml file with the name of the demo as value. We are using "ESA DEMO" as demo name on this example.Save the changes. 

**Note1:** The path `indemo/sec/esa-indemo-v1/auto.sh` will be created by the developer. The recommended path is `indemo/<architecture name >/<recipe name>/auto.sh`
    
**Note2:**  Architecture names are: `dcv` for datacenter, `sec` for security, `col` for collaboration




### <a name="step6"></a>Step 6. Configure Topology Builder  to execute the boostrap script on Centos7-tools1 VM. (This time with our customized auto.sh)  

The command should be something like:

    /root/bootstrap.sh --indemo-recipe esa-indemo-v1 --indemo-recipe-path sec/ --branch-name swadvani-31091 --api-port 31091 --db-api-port 31291  --auto-path indemo/sec/esa-indemo-v1/auto.sh

**Note1:** With this command, we are configuring bootstrap to download and execute our customised auto.sh script. 




### <a name="step7"></a>Step 7. Spin up a session of the demo and Verify status on DEV Dashboard (This time we should see the demo name on it) 

Status should be visible on http://dcv-automation-api:31091/api/v1.0/dashboard 

**Note1:**  If you see error messages related to your Recipe or Modules, this is the **time to test and troubleshoot**. Modify your recipes and modules accordingly until you get the desired results. 




### <a name="step8"></a>Step 8. Github: Push  

Once you are happy with your recipe, modules and auto.sh script, it is time to push the changes to Github.

Connect to the code repository via SSH with your controller's instance credentials. For more information on connecting to the code repository, click here.

Once logged in, execute the gitPushThisFolder.sh script as follows:

    ./gitPushThisFolder.sh -u <email address of your github account>
 


### <a name="step9"></a>Step 9. Github: Pull Request  

Go to Github, make sure you select your branch ( `swadvani-31091` on this case), verify your changes are present and proceed to click on "Pull Request" 


### <a name="step10"></a>Step 10. Github Merge  

The administration will have to approve the request and the your changes will be merged into the master branch which is the one used by the controller's master instance.

**Note1:** At the moment, this has to be approved by the owner of the repository. In the  future, we want at least one person on each architecture to be able to merge into the master. 


### <a name="step11"></a>Step 11. Github MasterPull  

The administrator has to pull the latest changes from github's master branch into the local repository. Once this is done, the new changes take effect immediately. 

**Note1:** This is done manually by the administrator.  In the future, we want this to happen automatically. 


### <a name="step12"></a>Step 12. Configure Topology Builder  to execute the boostrap script on Centos7-tools1 VM. (This time with our customized auto.sh and on the master controller)  

The command should be something like:

    /root/bootstrap.sh --indemo-recipe esa-indemo-v1 --indemo-recipe-path sec/  --auto-path indemo/sec/esa-indemo-v1/auto.sh

**Note1:** Notice that by not specifying  `--branch-name`, `--api-port` and `--db-api-port`, bootstrap will use master branch by default on API port 31080


### <a name="step13"></a>Step 13. Spin up a new session and verify status on master dashboard 

Status should be visible on http://dcv-automation-api:31080/api/v1.0/dashboard

**Note1:** We are seeing this on the controller's master instance by using port 31080



[Back to Main Page](https://github.com/dcloud-automation/content-automation/blob/master/README.md)










