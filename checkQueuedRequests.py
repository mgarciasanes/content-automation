#!/usr/bin/env python2.7

#####################################
# Global Variables
#####################################
#demoRecipesPath=r'/root/scripts/demoRecipes/'
modulesPath=r'/root/scripts/modules/'
INFLUXDB='influxKube'
global LOCATION
global INFLUXDBPORT

#####################################
# Import Modules
#####################################
import sys
import logging
import threading
import os
import os.path
import time
import datetime
import socket
import psutil
import signal

modulesPath=r'/root/scripts/modules/'
sys.path.append(modulesPath)
corePath=r'/root/scripts/core/'
sys.path.append(corePath)


import genFunctions as gen
import logger as logger
import worker as worker
import signalMonitor
import influx as influx

host = socket.gethostname()


#####################################
# General Functions
#####################################

def refreshModules():
    logger.autolog(level=1, message='Refreshing Modules, importing...')
    reload(influx)
    reload(gen)
    reload(logger)
    reload(worker)
    #reload(signalMonitor)


#####################################
#  FUNCTIONS IN LOOP
#####################################


def executeQueuedRequests():
    logger.autolog(level=1,message='Execute queued requests...')

    #queuedSessions=influx.readFromInfluxDb('select "datacenter","host","recipePath","recipeName","type","demo","id","vpod","anycpwd","owner",demoStatus from (select "datacenter","recipePath","recipeName","type","demo","host","id","vpod","anycpwd","owner",last(demoStatus) as demoStatus  from inDemoAutomation_sessions  group by id )  where demoStatus='+"'queued'" +' and host='+"'"+host+"'" +   ' group by id',influxDb=INFLUXDB)
    queuedSessions=influx.readFromInfluxDb('select *,id from (select *,last(demoStatus) from '+ str(influx.measurementSessions)+'  group by id )  where demoStatus='+"'queued'" +' and host='+"'"+host+"'" +   ' group by id',influxDb=INFLUXDB)

    if queuedSessions == False:
        logger.autolog(message='Unable to get queued requests')
        return False

    if queuedSessions == []:
        logger.autolog(message='No queued requests found')
        return True

    for queuedSession in queuedSessions:
        logger.autolog(level=1,message='Found queued request for recipe ' +  queuedSession['recipeName']+ ' with id ' + queuedSession['id'] + '. It will be executed now')
        influx.markSessionStarting(queuedSession,influxDb=INFLUXDB)
        tasks,errorMessage=gen.getRecipeStartTasks(queuedSession['recipeName'],specificFolder=queuedSession['recipePath'])
        if tasks == False: 
            logger.autolog(message=errorMessage +' '+ str(queuedSession['recipeName']))
            influx.markSessionError(queuedSession ,comments=errorMessage,influxDb=INFLUXDB)
        else: 
            failureFree,allTasksExecuted,executedTasks=worker.execTasks(queuedSession,tasks,action='Start') 
            influx.recordExecutedTasks(executedTasks,influxDb=INFLUXDB)
            if (failureFree and allTasksExecuted):
                logger.autolog(message='queued session executed without errors')
                influx.markSessionActive(queuedSession,influxDb=INFLUXDB)
            else:
                logger.autolog(message='queued session executed with errors or some tasks were not executed')
                influx.markSessionError(queuedSession,comments='Failed to startup',influxDb=INFLUXDB)


def getRequestFromQueue():
    logger.autolog(level=1,message='WIll connect to DB and get the oldest request in queue...')

    query='select *,id from (select *,last(demoStatus) from '+ str(influx.measurementSessions)+'  group by id )  where demoStatus='+"'queued'" +' and host='+"'"+host+"'" +   ' group by id'
    queuedSession=influx.readFromInfluxDb(query,influxDb=INFLUXDB)

    if queuedSession == False:
        logger.autolog(message='WARNING: Unable to get queued requests')
        return False

    if queuedSession == []:
        logger.autolog(message='No queued requests found')
        return False

    logger.autolog(level=1,message='Found queued request for recipe ' +  queuedSession['recipeName']+ ' with id ' + queuedSession['id'] + '. It will be executed now')
    return queuedSession


def executeRecipe(queuedSession):
    influx.markSessionStarting(queuedSession["request"],influxDb=INFLUXDB)
    tasks,errorMessage=gen.getRecipeStartTasks(queuedSession["request"]['recipeName'],specificFolder=queuedSession["request"]['recipePath'])
    if tasks == False: 
        logger.autolog(message=errorMessage +' '+ str(queuedSession["request"]['recipeName']))
        influx.markSessionError(queuedSession["request"] ,comments=errorMessage,influxDb=INFLUXDB)
    else: 
        failureFree,allTasksExecuted,executedTasks,queuedSession=worker.execTasks(queuedSession,tasks,action='Start', returnDetails=True) 
        #influx.recordExecutedTasks(executedTasks,influxDb=INFLUXDB)
        if (failureFree and allTasksExecuted):
            logger.autolog(message='queued session executed without errors')
            influx.markSessionActive(queuedSession["request"],influxDb=INFLUXDB)
            return executedTasks
        else:
            logger.autolog(message='queued session executed with errors or some tasks were not executed')
            influx.markSessionError(queuedSession["request"],comments='Failed to startup',influxDb=INFLUXDB)
            return False
                
#####################################
#  LOOP
#####################################

def loop(loopMode,multithread,timer):
    
    #refreshModules()

    if signalCheck.kill_now:
        logger.autolog(message="End of loop. I was killed gracefully.")
        quit()

    executeQueuedRequests()
    #queuedSession=getRequestFromQueue()
    #if queuedSession:
    #    executeRecipe(queuedSession)
    #else:
    #    logger.autolog(message='No session details for Recipe execution...')



    if loopMode:
        logger.autolog(message='checkQueuedRequests running loop mode. Will excute in ' + str(timer)+' seconds...')
      
        #Loop Option 1
        #logger.autolog(level=1, message='Looping with threading.Timer')
        #A thread will call the loop function in time described in "timer"
        #threading.Timer(timer, loop,[loopMode, multithread, timer]).start() 


        #Loop Option 2
        #The code below is not actually a loop, but rather it will finish the script and restart itself automatially 
        #
        logger.autolog(level=1, message='Looping with execl. Restarting Script.')
        time.sleep(timer)
        try:
            p = psutil.Process(os.getpid())
            for handler in p.open_files() + p.connections():
                logger.autolog(level=1,message="Restarting Script: Closing handler  " + str(handler))
                os.close(handler.fd)        
        except Exception as e:
            logger.autolog(level=1, message="Error Restarting the Script")
            logger.autolog(level=1, message=str(e))

        logger.autolog(level=1,message="Restarting Script NOW!")
        python = sys.executable
        os.execl(python, python, *sys.argv) 


if __name__ == '__main__':

    ##################################
    ## Check Arguments
    ##################################

    from argparse import ArgumentParser
    parser = ArgumentParser("dCloud DCV Automation Script: checkQueuedRequests")
    parser.add_argument('-l', '--loop', help='Run Once or Loop. Values are  loop or once', default=True)
    parser.add_argument('-t', '--timer', help='Run Once or Loop. Values are  loop or once', default=5)
    parser.add_argument('-d', '--database', help='Specify database to use. Default is influxKube ', default='influxKube')
    parser.add_argument('-dp', '--databaseport', help='Specify database port to use. Default is False  ', default="noPort")
    parser.add_argument('-loc', '--location', help='Specify Location where this script is running. Default is False ', default="noLocation")
    parser.add_argument('-x', '--multithread', help='Run tasks for all sessions in concurrent or sequential. Values are true or false. default is false.', default=False)
    args = parser.parse_args()
    
    
    #####################################
    ## Configure Logger
    #####################################

    logger.setLogFiles(newLogFile='checkQueuedRequests.log',newLogDebugFile='checkQueuedRequests_DEBUG.log')
    logger.setLogger()


    #####################################
    ## Configure Signal Check
    #####################################

    signalCheck = signalMonitor.SignalMonitor()

    #####################################
    ## Start Loop
    #####################################
    #global INFLUXDB


    #DB Port to be used. If not specified as argument, use the one specified in Creds file, section "database"
    if args.databaseport=="noPort":
        logger.autolog(level=1,message="No database port specified, using port from Cred file")
        INFLUXDB=args.database
    else:
        logger.autolog(level=1,message="Database port specified, using it. " +str(args.databaseport))
        INFLUXDB={'database':args.database,'port':args.databaseport}


    #Get Cluster Location. If not specified as argument, use the valued obtained directly from Kubernetes' label
    if args.location=="noLocation":
        cmd=r'kubectl get services kubernetes --namespace=default -o jsonpath={.metadata.labels.location} > /root/location'
        logger.autolog(level=1,message=str(cmd))
        os.system(cmd)
        locationFile=open(r'/root/location','r')
        location=locationFile.read().strip('\n')
        locationFile.close()
        logger.autolog(level=1,message="We are working with location  " + str(location))
        LOCATION=location
    else:
        LOCATION=str(args.location)


    loop(args.loop,args.multithread,int(args.timer))
