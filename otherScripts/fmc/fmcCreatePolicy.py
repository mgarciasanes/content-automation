import json
import requests.packages.urllib3
from pprint import pprint
import os, sys
from tqdm import tqdm
from fmc_rest_client import FMCRestClient
from fmc_rest_client import ResourceException
from fmc_rest_client.resources import *
#from tetpyclient import RestClient
#from TetPolicy2 import Environment, InventoryFilter, Cluster

requests.packages.urllib3.disable_warnings()
appIDs = []
appNames = []


import sys
#tenant_name = sys.argv[1]
#csr_ip = sys.argv[2]


#Connect to FMC
fmc_server_url = "https://10.150.0.10:443"
#fmc_server_url = "https://"+str(csr_ip)+r":10443"
fmc_username = "aciadmin"
fmc_password = "cisco"

print('Connecting to FMC {} ...'.format(fmc_server_url))
fmc = FMCRestClient(fmc_server_url, fmc_username, fmc_password)
print('Connected Successfully')

#Create blank Access Control Policy

fmc_acp = AccessPolicy('ftd-ngips-policy','BLOCK')
fmc_acp = fmc.create(fmc_acp)
fmc_rules=[]

rule = AccessRule('ftd-rule1',fmc_acp)
rule.action = 'ALLOW'
fmc_rules.append(rule)
for fmc_rule in tqdm(fmc_rules):
    fmc_acp_rules = fmc.create(fmc_rule)

print('Access Policy Created')