(Get-Module -Name virtualmachinemanager -ErrorAction SilentlyContinue) 
 {
. �C:\Program Files\Microsoft System Center 2012 R2\virtualmachinemanager.psd1�} 

param([string]$vmName = "mgarcias-02",[string]$portProfile = "vivek56|Opencart|web|ms-hybridcloud",[string]$output = "C:\scripts\apic_v2\output.txt", [string]$sessionId = "sessionid")

$LogFile="C:\scripts\scvmm\cloneVm.log"
$templateName='web1'

function LogWrite
{
   Param ([string]$logstring)
   $LogTime = Get-Date -Format "MM-dd-yyyy_hh-mm-ss"
   $LogSource= $sessionId
   $LogTask = 'cloneVM'
   Add-content $Logfile -value $($LogTime +" | " + $LogSource +" | " + $LogTask + " | " + $logstring)
}

LogWrite('starting execution...')






#Checking if VM already exists
LogWrite('Checking if VM already exists...')
$VM = Get-SCVirtualMachine -Name $vmName -ErrorAction SilentlyContinue  
If ($VM){  
     LogWrite('VM already exists. Not creating...')

}  
Else {  
     LogWrite('VM doesnt exists. Creating...')
     
    #Get Template
    LogWrite('Getting template')
    $Template = Get-SCVMTemplate -VMMServer scvmm1.dcv.svpod  | where {$_.Name -eq $templateName}

    #Create VM Configuration
    LogWrite('Creating VM Configuraiton')
    $virtualMachineConfiguration = New-SCVMConfiguration -VMTemplate $Template -Name $vmName
    LogWrite($virtualMachineConfiguration) 
    Write-Output $virtualMachineConfiguration

    #Create VM
    LogWrite('Creating Vm')
    $cloud = Get-SCCloud -Name "MS-HybridCloud"
    New-SCVirtualMachine -Name $vmName -VMConfiguration $virtualMachineConfiguration -Cloud $cloud -Description "" -StartVM 
    $VM = Get-SCVirtualMachine -Name $vmName
    LogWrite($VM) 
    Write-Output $VM       
}  



#Checking if VMNetwork already exists
LogWrite('Checking if VMNetwork already exists in 30 secs')
Start-Sleep -s 120
LogWrite('Checking if VMNetwork already exists now')

$VMNetwork = Get-SCVMNetwork -VMMServer scvmm1.dcv.svpod -Name $portProfile
If ($VMNetwork){  
     LogWrite('VMNetwork  exists. Continuing...')


    #Change VM's adapter VMNetwork
    LogWrite('Get Adapter from VM')
    $Adapter = Get-SCVirtualNetworkAdapter -VM $VM
    LogWrite($Adapter) 
    Write-Output $Adapter
    LogWrite('Changing VM Network')
    #$VMNetwork = Get-SCVMNetwork -VMMServer scvmm1.dcv.svpod -Name $portProfile
    #LogWrite($VMNetwork)
    #Write-Output $VMNetwork
    Set-SCVirtualNetworkAdapter -VirtualNetworkAdapter $Adapter -VMNetwork $VMNetwork



}  
Else {  
     LogWrite('VMNetwork nof found')
     Write-Error('VMNetwork nof found')
     exit 1
}









LogWrite('end of execution...')
