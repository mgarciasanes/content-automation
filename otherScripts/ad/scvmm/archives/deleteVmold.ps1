param([string]$vmName = "xxxxxx", [string]$sessionId = "sessionid")

$LogFile="C:\scripts\scvmm\deleteVm.log"


function LogWrite
{
   Param ([string]$logstring)
   $LogTime = Get-Date -Format "MM-dd-yyyy_hh-mm-ss"
   $LogSource= $sessionId
   $LogTask = 'deleteVM'
   Add-content $Logfile -value $($LogTime +" | " + $LogSource +" | " + $LogTask + " | " + $logstring)
}

LogWrite('starting execution...')




#Checking if VM already exists
LogWrite('Checking if VM  exists...')
$VM = Get-SCVirtualMachine -Name $vmName -ErrorAction SilentlyContinue  
If ($VM){  
     LogWrite('VM  exists. Trying to delete...')

    Stop-VM -VM $VM
    Remove-VM -VM $VM -Force -Confirm:$False

}  
Else {  
     LogWrite('VM doesnt exists. Exiting...')
     
}  



LogWrite('end of execution...')
