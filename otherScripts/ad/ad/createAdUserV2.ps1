param([string]$sessionId = "sessionId", [string]$userId = "sec-rlabatia-28644" , [string]$password = "sec-rlabatia-286448-10" , [string]$groupName="Secure Datacenter")

$LogFile="C:\scripts\ad\createAdUserV2.log"
function LogWrite
{
   Param ([string]$logstring)
   $LogTime = Get-Date -Format "MM-dd-yyyy_hh-mm-ss"
   $LogSource= $sessionId
   $LogTask = 'createAdUser'
   Add-content $Logfile -value $($LogTime +" | " + $LogSource +" | " + $LogTask + " | " + $logstring)
}



#if ($groupName -eq "apic-v2" ){
#    LogWrite("Group Name is Microsegmentation APIC Demo Users")
#    $groupName = "Microsegmentation APIC Demo Users"
#}
#
#if ($groupName -eq "hyperflex-hw" ){
#    LogWrite("Group Name is Hyperflex Hardware Demo Users")
#    $groupName = "Hyperflex Hardware Demo Users"
#}


LogWrite('starting script...')
Import-Module ActiveDirectory -ErrorAction SilentlyContinue

$truncatedUserId=$userId.subString(0, [System.Math]::Min(20, $userId.Length))


LogWrite("The truncated userId is:   $truncatedUserId")

#Check if user exists:
$User = Get-ADUser -LDAPFilter "(Name=$userId)"


If ($User -eq $Null) {

    LogWrite("User does not exist. Proceding to create user  $userId")
}
else {

    #User Exists. Remove it!
    try {
        LogWrite("User exists. Trying to remove user  $userId")
        Remove-ADUser -identity $truncatedUserId -Confirm:$false
         }
    catch [System.Object]
         {
         LogWrite("Could not remove user $userId ")
         LogWrite([string]$_.Exception.GetType().FullName + [string]$_.Exception.Message) 
         exit
         }
}


LogWrite("About to create new user in active directory with userID " + $userId + " password " + $password + " in group " + $groupName)
# Create User
try {
    LogWrite("Executing New-ADUser for user $userId")
    New-ADUser -SamAccountName $truncatedUserId `
               -Name $userId `
               -DisplayName $userId `
               -GivenName $userId `
               -Surname $userId `
               -EmailAddress ($userId + '@dcv.svpod') `
               -UserPrincipalName ($userId + '@dcv.svpod') `
               -Enabled $true -ChangePasswordAtLogon $false `
               -PasswordNeverExpires  $true `
               -AccountPassword (ConvertTo-SecureString $password -AsPlainText -force) -PassThru `
     }
catch [System.Object]
     {
     LogWrite("Could not create user $userId ")
     LogWrite([string]$_.Exception.GetType().FullName +  [string]$_.Exception.Message)
     Write-Error([string]$_.Exception.GetType().FullName +  [string]$_.Exception.Message)
     exit 1
     }
     


#Add USer to Group
try {
    LogWrite("Executing Add-ADGroupMember for user $userId")
    Add-ADGroupMember $groupName $truncatedUserId
    }
catch [System.Object]
     {
     LogWrite("Could not add user $userId to group $groupName")
     LogWrite([string]$_.Exception.GetType().FullName + [string]$_.Exception.Message)
     Write-Error([string]$_.Exception.GetType().FullName + [string]$_.Exception.Message)
     exit 1
     }
 
LogWrite("Script Completed Ok")                    


