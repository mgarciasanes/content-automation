﻿param([string]$sessionId = "sessionId", [string]$userId = "dcloud1" , [string]$groupName="MSHC_Plan_WAP_Tenants")

$LogFile="C:\scripts\ad\addUserToGroup.log"
function LogWrite
{
   Param ([string]$logstring)
   $LogTime = Get-Date -Format "MM-dd-yyyy_hh-mm-ss"
   $LogSource= $sessionId
   $LogTask = 'addUserToGroup'
   Add-content $Logfile -value $($LogTime +" | " + $LogSource +" | " + $LogTask + " | " + $logstring)
}


LogWrite('starting script...')
Import-Module ActiveDirectory -ErrorAction SilentlyContinue

#Check if user exists:
$User = Get-ADUser -LDAPFilter "(sAMAccountName=$userId)"
If ($User -eq $Null) {
    LogWrite("User does not exist. Aborting...")
    exit 1
}
else {
     #User Exists!
     LogWrite("User exists. Trying to add user  $userId to group $groupName")

     #Add USer to Group
     try {
         LogWrite("Executing Add-ADGroupMember for user $userId")
         Add-ADGroupMember $groupName $userId
         LogWrite("User $userId added to $groupName")
         }
      catch [System.Object]
         {
         LogWrite("Could not add user $userId to group $groupName")
         LogWrite([string]$_.Exception.GetType().FullName + [string]$_.Exception.Message)
         Write-Error([string]$_.Exception.GetType().FullName + [string]$_.Exception.Message)
         exit 1
         }
}

LogWrite("Script Completed Ok")