# Delete VMs, User and Subscription in WAP Script

param([string]$username = "xxxxxx")

# Import Required Modules
import-module mgmtsvcadmin
import-module virtualmachinemanager
import-module virtualmachinemanagercore

# Set SCVMM Credentials
$scvmmadmin = "dcv\administrator"
$scvmmpassword = "C^rr0t3965" | convertto-securestring -asplaintext -force
$scvmmcred = new-object system.management.automation.pscredential($scvmmadmin,$scvmmpassword)
Get-SCVMMServer -ComputerName scvmm1 -credential $scvmmcred

# Set WAP Credentials
$wapadmin = "dcv\administrator"
$wappassword = "C^rr0t3965" | convertto-securestring -asplaintext -force
$wapcred = new-object system.management.automation.pscredential($wapadmin,$wappassword)

# Set Environment Specific Variables
$WAPServer = "wap1.dcv.svpod"

# Establish Connection to WAP
$AdminURI = "https://" + $WAPServer + ":30004"
$AuthSite = "https://" + $WAPServer + ":30072"
$ClientRealm = "http://azureservices/AdminSite"
$Token = Get-MgmtSvcToken -Type Windows -AuthenticationSite $AuthSite -ClientRealm $ClientRealm -User $wapcred -DisableCertificateValidation

# Define User
$WAPUser = $username+"@dcv.svpod"

# Delete VMs, User and Subscription
$vm_roles = Get-CloudResource | Where-Object Owner -match $username

foreach ($vm_role in $vm_roles)
{ Remove-CloudResource -CloudResource $vm_role }

$owned_vms = Get-SCVirtualMachine | Where-Object Owner -match $username

foreach ($owned_vm in $owned_vms)
{ Stop-SCVirtualMachine -VM $owned_vm -Force }

foreach ($owned_vm in $owned_vms)
{ Remove-SCVirtualMachine -VM $owned_vm }

$matching_vms = Get-SCVirtualMachine | Where-Object Name -match $username

foreach ($matching_vm in $matching_vms)
{ Stop-SCVirtualMachine -VM $matching_vm -Force }

foreach ($matching_vm in $matching_vms)
{ Remove-SCVirtualMachine -VM $matching_vm }

Remove-MgmtSvcUser -Name $WAPUser -AdminUri $AdminURI -Token $Token -DeleteSubscriptions -DisableCertificateValidation -Confirm:$false
