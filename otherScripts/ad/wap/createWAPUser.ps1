# Create User in WAP and Subscribe to Plan

param([string]$username = "xxxxxx")

# Import Required Modules
import-module mgmtsvcadmin

# Set WAP Credentials
$wapadmin = "dcv\administrator"
$wappassword = "C^rr0t3965" | convertto-securestring -asplaintext -force
$wapcred = new-object system.management.automation.pscredential($wapadmin,$wappassword)

# Set Environment Specific Variables
$WAPServer = "wap1.dcv.svpod"

# Establish Connection to WAP
$AdminURI = "https://" + $WAPServer + ":30004"
$AuthSite = "https://" + $WAPServer + ":30072"
$ClientRealm = "http://azureservices/AdminSite"
$Token = Get-MgmtSvcToken -Type Windows -AuthenticationSite $AuthSite -ClientRealm $ClientRealm -User $wapcred -DisableCertificateValidation

# Set WAP Plan Variables
$PlanID = "MSHybiu08fqkc"
$SubFN = "MS-HybridCloud_Hosting_Plan"

# Define User
$WAPUser = $username+"@dcv.svpod"

# Create User in WAP
Add-MgmtSvcUser  -AdminUri $AdminURI -Token $Token -Name $WAPUser -Email $WAPUser -State 'Active'

# Subscribe User to Plan
Add-MgmtSvcSubscription -AdminUri $AdminURI -Token $Token -AccountAdminLiveEmailId $WAPUser -AccountAdminLivePuid $WAPUser -PlanId $PlanID -FriendlyName $SubFN -DisableCertificateValidation
