@ECHO OFF
SET LOCAL

REM Argument 1 is the output file name
REM Argument 2 is the session file name
REM All the other arguments are passed to the PS script

SET otherArguments=%3
CALL :dequote otherArguments
rem start /wait "Executing PS Script %2"  C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -psc "C:\Program Files (x86)\VMware\Infrastructure\vSphere PowerCLI\vim.psc1" -file "%2" -output %1 %otherArguments%
start /wait "Executing PS Script %2"  C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -file "%2" -output %1 %otherArguments%
Goto :eof

:DeQuote
for /f "delims=" %%A in ('echo %%%1%%') do set %1=%%~A
Goto :eof