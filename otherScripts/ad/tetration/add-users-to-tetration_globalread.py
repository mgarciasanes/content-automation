"""
add-users-to-tetration.py takes as its input a JSON file and creates an
account for every person in that JSON file (if they don't already exist).
It then assigns that new user the Global Read Only role.
The JSON file must be in the following format:
[
    {
        "email": "dchosnek@cisco.com",
        "firstName": "Doron",
        "lastName": "Chosnek"
    },
    {
        "email": "username@domain.com",
        "firstName": "FirstName",
        "lastName": "Lastname"
    }
]

Author: Doron Chosnek, Cisco Systems, June 2017

CAUTION: I don't know if GLOBAL_READ_ONLY uses the same ID in every
instance of Tetration. I have hard-coded it here in this script but it
may need to be changed.
"""

# pylint: disable=invalid-name

import json
import requests.packages.urllib3
from tetpyclient import RestClient

API_ENDPOINT="https://198.19.193.228"
API_CREDS = "api_credentials_lon.json"
GLOBAL_READ_ONLY = "59d81533497d4f3964b6f228"
ROLE_PAYLOAD = {"role_id": GLOBAL_READ_ONLY}

# ============================================================================
# Get current TA users
# ----------------------------------------------------------------------------

restclient = RestClient(
    API_ENDPOINT,
    credentials_file=API_CREDS,
    verify=False
)

requests.packages.urllib3.disable_warnings()
resp = restclient.get('/openapi/v1/users')

existing = []
for person in resp.json():
    existing.append(person["email"])
    # display users who have no role associated with their accounts
    if len(person["role_ids"]) < 1:
        print 'No role: {} {}'.format(person["email"], person["id"])


# ============================================================================
# Compare people from JSON file to existing users and add those that don't
# already exist
# ----------------------------------------------------------------------------

counter = 0

# get team members from JSON file
with open('sevtusers.json') as data_file:
    entire_team = json.load(data_file)

for member in entire_team:
    if member["email"] not in existing:
        print 'Adding {} {} ({})'.format(member['firstName'], member['lastName'], member["email"])
        req_payload = {
            "first_name": member["firstName"],
            "last_name": member["lastName"],
            "email": member["email"]
        }
        # this command creates the user with no role
        resp = restclient.post('/users', json_body=json.dumps(req_payload))

        # if that command completed successfully, add the Global Read Only
        # role to that account
        errorFlag = False
        if resp.status_code == 200:
            # give the new user global read only role
            new_user_id = resp.json()["id"]
            add_role_path = '/users/' + str(new_user_id) + '/add_role'
            resp = restclient.put(add_role_path, json_body=json.dumps(ROLE_PAYLOAD))
            if resp.status_code != 200:
                errorFlag = True
        else:
            errorFlag = True

        if errorFlag:
            print "  Error adding {} to the system".format(member["email"])
        else:
            counter += 1

print "\nAdded {} new users to Tetration".format(str(counter))
print
