"""
add-one-user-to-tetration.py will add the specified user to Tetration
(if they don't already exist) and give them the MSLAB Read Only role.
The JSON file must be in the following format:

Author: Doron Chosnek, Cisco Systems, September 2017

CAUTION: the MSLAB_READ_ONLY role does not use the same ID in every
instance of Tetration. I have hard-coded a value here but you can change
it by passing a different value through a command line argument.
"""

# pylint: disable=invalid-name

import json
import argparse
import requests.packages.urllib3
from tetpyclient import RestClient

API_ENDPOINT="https://198.19.193.228"
API_CREDS = "api_credentials_lon.json"
MSLAB_READ_ONLY = "5a9f1f4a755f02793400c575"
DOMAIN = 'dcloud.cisco.com'

# =============================================================================
# ARGPARSE
# -----------------------------------------------------------------------------

parser = argparse.ArgumentParser(description='Add a scope for the specified user.')
parser.add_argument('-user', '-u', required=True, help="username")
parser.add_argument('-domain', '-d', default=DOMAIN, help="Email domain. Default is dcloud.cisco.com")
parser.add_argument('-endpoint', default=API_ENDPOINT, help="URL of Tetration instance")
parser.add_argument('-credentials', default=API_CREDS, help="path to credentials file")
parser.add_argument('-givenName', required=True, help="user given (first) name")
parser.add_argument('-surname', required=True, help="user surname (last name)")
parser.add_argument('-roleId', default=MSLAB_READ_ONLY, help="GUID for MSLAB Read Only role")
args = parser.parse_args() 
print(args.user)


# ============================================================================
# Get current TA users
# ----------------------------------------------------------------------------

restclient = RestClient(
    args.endpoint,
    credentials_file=args.credentials,
    verify=False
)

requests.packages.urllib3.disable_warnings()
resp = restclient.get('/users')
#print json.dumps(resp.json(), indent=4, sort_keys=True)
existing = []
for person in resp.json():
    existing.append(person["email"])


# ============================================================================
# Add the specified user if he/she doesn't already exist
# ----------------------------------------------------------------------------

member = '{}@{}'.format(args.user, args.domain)

if member in existing:
    print "User {} already exists.".format(member)
else:
    req_payload = {
        "first_name": args.givenName,
        "last_name": args.surname,
        "email": member
    }
    # this command creates the user with no role
    resp = restclient.post('/users', json_body=json.dumps(req_payload))

    # if that command completed successfully, add the MSLAB Read Only
    # role to that account
    if resp.status_code == 200:
        # give the new user MSLAB read only role
        new_user_id = resp.json()["id"]
        add_role_path = '/users/' + str(new_user_id) + '/add_role'
        role_payload = {"role_id": args.roleId}
        resp = restclient.put(add_role_path, json_body=json.dumps(role_payload))
        if resp.status_code != 200:
            print "Error adding MSLAB Read Only role to {}".format(member)
    else:
        print "Error adding {} to the system".format(member)
        print resp.text
