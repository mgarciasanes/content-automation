"""
get-roles.py for Tetration displays the following role information:
1. The role ID and role name for every role returned by the API
2. The role ID and number of occurences of that role for every user
   role that was not returned by the API
3. The result of API calls specifying each role discovered in 2 above.

Author: Doron Chosnek, Cisco Systems, June 2017
"""


# pylint: disable=invalid-name

import json
import requests.packages.urllib3
from tetpyclient import RestClient

API_ENDPOINT = "https://perseus-aus.cisco.com"
API_CREDS = "api_credentials.json"

restclient = RestClient(
    API_ENDPOINT,
    credentials_file=API_CREDS,
    verify=False
)

requests.packages.urllib3.disable_warnings()

# ============================================================================
# Functions
# ----------------------------------------------------------------------------

def table_print(d, head1, head2, col2Left=True):
    '''
    Display a dict (d) as a table with column headers col1 and col2. The key
    will be used in column 1 and the value in column 2.
    '''
    # first determine the proper width for each column by determining the
    # longest value that exists in each column (including headers)
    col1_len = 0
    col2_len = 0
    for k, v in d.iteritems():
        if len(str(k)) > col1_len:
            col1_len = len(str(k))
        if len(str(v)) > col2_len:
            col2_len = len(str(v))
    if len(head1) > col1_len:
        col1_len = len(head1)
    if len(head2) > col2_len:
        col2_len = len(head2)

    # print the heading row
    print "{:<{w1}} {:<{w2}}".format(head1, head2, w1=col1_len, w2=col2_len)
    print '-' * col1_len + ' ' + '-' * col2_len
    # print the data
    if col2Left:
        for k, v in d.iteritems():
            print "{:<{w1}} {:<{w2}}".format(k, v, w1=col1_len, w2=col2_len)
    else:
        for k, v in d.iteritems():
            print "{:<{w1}} {:>{w2}}".format(k, v, w1=col1_len, w2=col2_len)


# ============================================================================
# Display list of roles returned by API
# ----------------------------------------------------------------------------

resp = restclient.get('/openapi/v1/roles')

# save all role IDs retrieved
advertised = {}
for role in resp.json():
    advertised[role["id"]] = role["name"]

print "\nThese are roles returned by the API:\n"
table_print(advertised, 'Role ID', 'Name')


# ============================================================================
# Display list of roles **NOT** returned by API but assigned to users
# ----------------------------------------------------------------------------

# step through all users and collect data about any roles that haven't
# already been discovered

resp = restclient.get('/openapi/v1/users')

discovered = {}
for person in resp.json():
    for role in person["role_ids"]:
        if role not in advertised:
            # save the number of occurences of each role ID
            if role not in discovered:
                discovered[role] = 1
            else:
                discovered[role] += 1

print "\nThese are roles assigned to users and NOT returned by the API:\n"
table_print(discovered, 'Role ID', 'Occurences', col2Left=False)


# ============================================================================
# Display details of GET calls to these discovered roles
# ----------------------------------------------------------------------------

print "\nHere is what the API returns for each discovered role:\n"

for k in discovered.iterkeys():
    resp = restclient.get('/openapi/v1/roles/' + str(k))
    print "Role " + k + " : status code " + str(resp.status_code)
    print json.dumps(resp.json(), indent=2)

# print an extra newline at the end for readability
print
