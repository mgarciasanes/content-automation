﻿Param(
    [String]$dcName, [String]$adUser, [string]$newPwd, [String]$bindUser, [String]$bindPwd
)

#--- Create remote powershell session to set new lab admin password ---#
$session = New-PSSession $dcName -Authentication Kerberos -Credential (New-Object System.Management.Automation.PsCredential($bindUser, ($bindPwd | ConvertTo-SecureString -AsPlainText -Force)))
Invoke-Command -Session $session -ScriptBlock { Set-ADAccountPassword $args[0] -NewPassword ($args[1] | ConvertTo-SecureString -AsPlainText -Force) -Reset} -ArgumentList $adUser,$newPwd
