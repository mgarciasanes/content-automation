﻿<#
.SYNOPSIS
Add a single user to AUSLAB Active Directory and notify that user of their new credentials.

.DESCRIPTION
Adds a single user to AUSLAB domain and puts them into a specified Security Group. It also notifies the user (and the lab admins) of their new credentials.

.NOTES
    Author: Doron Chosnek

.EXAMPLE
Add-AdUser.ps1 -bindUser dchosnek -newUser jotest -givenName Joseph -surname Test

#>


# =========================================================================
# INPUTS AND LOGGING
# -------------------------------------------------------------------------

# required to enable verbose output
[cmdletbinding()]
Param(
    #[string]$bindUser, $bindPwd=(Read-Host "Admin Password:" -AsSecureString), [string]$newUser, [string]$givenName, [string]$surname, [string]$SecurityGroup="Perseus Users", [string]$ADServerName="vdi-dc.auslab.cisco.com"
    [string]$newUser, [string]$givenName, [string]$surname, [string]$SecurityGroup="Perseus Users"
)

# Import-Module ActiveDirectory

# log all of the parameters given to this function
$log = @{}
# $log."bindUser" = $bindUser
$log."newUser" = $newUser
$log."givenName" = $givenName
$log."surname" = $surname
$log."SecurityGroup" = $SecurityGroup
$log."ADServerName" = $ADServerName
$log | ConvertTo-Json | Out-File "$($PSScriptRoot)\logs\$($newUser).txt"

# don't need any of this anymore since we aren't invoking this code on the AD controller anymore... we
# are running the ActiveDirectory module on this machine
<#
if($bindPwd -is [SecureString])
{
    $cred = New-Object System.Management.Automation.PSCredential($bindUser, $bindPwd)
}
else
{
    $cred = New-Object System.Management.Automation.PSCredential($bindUser, ($bindPwd | ConvertTo-SecureString -AsPlainText -Force))
}

$session = New-PSSession $ADServerName -Authentication Kerberos -Credential $cred
Invoke-Command -Session $session {Get-ADUser -Filter * -SearchBase "CN=Users,DC=auslab,DC=cisco,DC=com"} | select -ExpandProperty SamAccountName
#>

# =========================================================================
# FUNCTION - SEND EMAIL TO USER
# -------------------------------------------------------------------------

function NotifyNewUser
{
    param([Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)][object[]]$NewUser)

    # the begin block runs only once
    begin
    {
        $From = 'WWDC-LabAdmins@cisco.com'
        $SmtpServer = "smtp-ext.cisco.com"
        $Subject = "your new AUSLAB credentials"
        $Bcc = 'wwdc-labadmins@cisco.com'
    }

    # the process block runs once for each user sent to this function (could be one
    # user or could be a list of users)
    process
    {
        $body = "$($NewUser.GivenName),

Welcome to the AUSLAB domain. Your new login ID and password are shown below. These credentials will be used to access team lab gear.

login: $($NewUser.SamAccountName)
password: $($NewUser.Password)

You can change your password with a UCSD workflow at: https://auslab-infra-ucsd-1.auslab.cisco.com
Go to https://perseus-toolbox.cisco.com for instructions on lab access.

Thank you,
WWDC-LabAdmins@cisco.com"

        Send-MailMessage -Body $body -To "$($NewUser.SamAccountName)@cisco.com" -SmtpServer $SmtpServer -From $From -Bcc $Bcc -Subject $Subject
        #Send-MailMessage -Body $body -To "dchosnek@cisco.com" -SmtpServer $SmtpServer -From $From -Subject $Subject
    }
}


# =========================================================================
# ADD USER TO ACTIVE DIRECTORY
# -------------------------------------------------------------------------

# create structure for new user account

$u = "" | select GivenName, Surname, SamAccountName, Description, UserPrincipalName, Name, EmailAddress
$u.GivenName = $givenName
$u.Surname = $surname
$u.Name = ($u.GivenName, $u.Surname -join " ")
$u.SamAccountName = $newUser
$u.UserPrincipalName = $u.SamAccountName + '@auslab.cisco.com'
$u.Description = "created by UCSD workflow"
$u.EmailAddress = $newUser + '@cisco.com'

# create random password

$upper = "ABCDEFGHJKLMNPQRSTUVWXYZ".ToCharArray()
$lower = "abcdefghijklmnopqrstuvwxyz".ToCharArray()
$digits = "0123456789".ToCharArray()
$special = '~!@#$%^&*_-+=`|\(){}[]:;<>,.?/'.ToCharArray()

($upper, $lower, $digits, $special) | Get-Random -Count 22 | % -Begin {[string]$random=$null} -Process {$random+=$_}


# create the user if it doesn't already exist

if((Get-ADUser -Filter * | select -ExpandProperty SamAccountName) -contains $u.SamAccountName)
{
    Write-Verbose "User already exists."
    $notify = $false
}
else
{
    $u | New-ADUser -AccountPassword (ConvertTo-SecureString -AsPlainText $random -Force) -Enabled $true -PasswordNeverExpires $true
    Get-ADGroup $SecurityGroup | Add-ADGroupMember -Members $u.SamAccountName

    $notify = $true
    Write-Verbose "User $($u.UserPrincipalName) created with password $($random)."
}

# =========================================================================
# SET THE USER'S ACI PERMISSIONS
# -------------------------------------------------------------------------

# set the user's ACI access rights if not already set
$newbie = Get-ADUser -Filter {SAMAccountName -like $newUser} -SearchBase "CN=Users,DC=auslab,DC=cisco,DC=com" -Properties aciCiscoAVPair
if(($newbie.aciCiscoAVPair).length -eq 0)
{
    $newbie.aciCiscoAVPair = 'shell:domains = all//'
    Set-ADUser -Instance $newbie
    Write-Verbose "Set the user aciCiscoAVPair parameter"

    # only log the password if the user was created; otherwise this is not useful
    $log.Password = $random
}
else
{
    Write-Verbose "aciCiscoAVPair parameter left unchanged."
}


# =========================================================================
# SEND EMAIL TO NOTIFY USER
# -------------------------------------------------------------------------

# send an email
if($notify)
{
    $u | Add-Member -MemberType NoteProperty -Name Password -Value $random
    $u | NotifyNewUser
    Write-Verbose "Email sent to $($u.EmailAddress)"
}


# =========================================================================
# REWRITE THE LOG FILE
# -------------------------------------------------------------------------

$log.aciCiscoAVPair = $newbie.aciCiscoAVPair
$log.EmailSent = $notify
$log | ConvertTo-Json | Out-File "$($PSScriptRoot)\logs\$($newUser).txt"
