﻿

Param(
    [string]$bindUser, $bindPwd=(Read-Host "Admin Password:" -AsSecureString), [string]$removeUser, [string]$ADServerName="vdi-dc.auslab.cisco.com"
)



if($bindPwd -is [SecureString])
{
    $cred = New-Object System.Management.Automation.PSCredential($bindUser, $bindPwd)
}
else
{
    $cred = New-Object System.Management.Automation.PSCredential($bindUser, ($bindPwd | ConvertTo-SecureString -AsPlainText -Force))
}

$session = New-PSSession $ADServerName -Authentication Kerberos -Credential $cred


# remove the user
Invoke-Command -Session $session -ArgumentList $removeUser { 

    param($user)

    Remove-ADUser -Identity $user -Confirm:$false

} 
