﻿

Param(
    [string]$bindUser, $bindPwd=(Read-Host "Admin Password:" -AsSecureString), [string]$domainUser, [string]$newPwd, [string]$ADServerName="vdi-dc.auslab.cisco.com"
)

# the domainUser will probably come in the format username@auslab.cisco.com, so we need to prune it
$shortName = $domainUser.Split('@')[0]

$output = "" | select bindUser, bindPwd, domainUser, shortName, newPwd, AdServerName
$output.bindUser = $bindUser
$output.bindPwd = $bindPwd
$output.domainUser = $domainUser
$output.shortName = $shortName
$output.newPwd = $newPwd
$output.AdServerName = $ADServerName

#$output | Set-Content "$($PSScriptRoot)\\change_pwd.txt" -Force

# While testing this script, I was running it directly from the PSA host so I had it prompt me for
# the bindPwd (secure string). UCSD passes it as a plain string. So this if statement creates
# the appropriate credential object depending on whether the bindPwd is plain or secure.

if($bindPwd -is [SecureString])
{
    $cred = New-Object System.Management.Automation.PSCredential($bindUser, $bindPwd)
}
else
{
    $cred = New-Object System.Management.Automation.PSCredential($bindUser, ($bindPwd | ConvertTo-SecureString -AsPlainText -Force))
}

$session = New-PSSession $ADServerName -Authentication Kerberos -Credential $cred


Invoke-Command -Session $session -ArgumentList $shortName, $newPwd { 

    param($user, $newPwd)

    Set-ADAccountPassword -Identity $user -NewPassword ($newPwd | ConvertTo-SecureString -AsPlainText -Force) -Reset

}