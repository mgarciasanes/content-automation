"""
create-lab-space.py for Tetration will create a scope for the specified user
and make that user the owner of that scope. It will also create n number of
child scopes for that user. The steps for doing this are:
1. Create a new scope named after the user
2. Create a new role named after the user
3. Add 'SCOPE_OWNER' ability to the new role and apply it to the new scope
4. Apply the new role to the specified local user in TA

Author: Doron Chosnek, Cisco Systems, June 2017
"""

# pylint: disable=invalid-name

import json
import argparse
import requests.packages.urllib3
from tetpyclient import RestClient

API_ENDPOINT = "https://perseus-aus.cisco.com"
API_CREDS = "api_credentials.json"
FIELD_LABS_SCOPE = "596d57a0497d4f3eaef1fd07"
DOMAIN = 'cisco.com'
ROLE_PREFIX = 'Field Scope Owner '

# =============================================================================
# ARGPARSE
# -----------------------------------------------------------------------------

parser = argparse.ArgumentParser(description='Add a scope for the specified user.')
parser.add_argument('-user', '-u', required=True, help="username (CEC ID)")
parser.add_argument('-domain', '-d', default='cisco.com', help="Email domain. Default is cisco.com")
parser.add_argument('-number', '-n', type=int, default=4, help="number of child scopes to create")
parser.add_argument('-endpoint', default=API_ENDPOINT, help="URL of Tetration instance")
parser.add_argument('-credentials', default=API_CREDS, help="path to credentials file")
args = parser.parse_args()


# Disable warnings
requests.packages.urllib3.disable_warnings()
restclient = RestClient(
    args.endpoint,
    credentials_file=args.credentials,
    verify=False
)

# ============================================================================
# Functions
# ----------------------------------------------------------------------------

def get_scopeid(scope_name, restclient):
    '''
    Returns the ID of the scope whose short_name matches scope_name
    scope_name: string
    '''
    resp = restclient.get('/openapi/v1/app_scopes')

    # walk through all of the scopes in Tetration & compare to scope_name
    for scope in resp.json():
        if scope_name == scope["short_name"]:
            return scope["id"]

    return None

def get_userid(email, restclient):
    '''
    Get the ID of a user in Tetration while searching by email address
    '''
    resp = restclient.get('/openapi/v1/users')

    for person in resp.json():
        if email == person["email"]:
            return person["id"]

    return None

def create_scope(scope, parent, qtype, qfield, qvalue, restclient):
    '''
    Create a scope with the given name under the given parent. If it
    completes successfully, return the scope ID.
    '''
    req_payload = {
        "short_name": scope,
        "short_query": {
            "type": qtype,
            "field": qfield,
            "value": qvalue
        },
        "parent_app_scope_id": parent
    }
    resp = restclient.post('/openapi/v1/app_scopes', json_body=json.dumps(req_payload))
    if resp.status_code == 200:
        return (resp.json())["id"]
    else:
        return resp.text

def get_roleid(role_name, restclient):
    '''
    Find the ID for the specified role_name
    '''
    resp = restclient.get('/openapi/v1/roles')
    for role in resp.json():
        if role_name == role["name"]:
            return role["id"]
    # print json.dumps(resp.json(), indent=2)

    return None


def create_role(role_name, restclient):
    '''
    Create a role with the given name. If it completes successfully,
    return the role ID.
    '''
    req_payload = {
        "name": role_name,
        "description": "created by script"
    }
    resp = restclient.post('/openapi/v1/roles', json_body=json.dumps(req_payload))
    if resp.status_code == 200:
        return (resp.json())["id"]
    else:
        return resp.text

def add_ability_to_role(ability, role, scope):
    '''
    Add one ability for one scope to one role. Possible values for ability are
    SCOPE_READ, SCOPE_WRITE, EXECUTE, ENFORCE, SCOPE_OWNER.
    '''
    req_payload = {
        "app_scope_id": scope,
        "ability": ability
    }
    url = '/openapi/v1/roles/{}/capabilities'.format(role)
    # print url
    # print req_payload
    resp = restclient.post(url, json_body=json.dumps(req_payload))
    return resp.status_code

# ============================================================================
# Create new scope
# ----------------------------------------------------------------------------

username = '{}@{}'.format(args.user, args.domain)
print "\n"

# if the user ID exists and the scope does not already exist, then create it
user_id = get_userid(username, restclient)
scope_id = get_scopeid(args.user, restclient)
if scope_id is not None:
    print "Scope {} already exists. ID = {}".format(args.user, scope_id)
elif user_id is None:
    print "Error: user does not exist."
    quit()
else:
    # the parent scope we create must use 'user_Owner' as a filter; the 'user_' prefix
    # is required for all filters that use user annotations
    scope_id = create_scope(args.user, FIELD_LABS_SCOPE, 'eq', 'user_Owner', args.user, restclient)
    print "Scope {} created. ID = {}".format(args.user, scope_id)
    for i in range(args.number):
        my_scope = 'App' + str(i+1)
        # the child scope we create must use 'user_AppScope' as a filter; the 'user_' prefix
        #  is required for all filters that use user annotations
        create_scope(my_scope, scope_id, 'eq', 'user_AppScope', my_scope, restclient)
        print "  Child scope {} created.".format(i+1)


# ============================================================================
# Create new role with owner capabilities
# ----------------------------------------------------------------------------

# if the role doesn't already exist, then create it
role_name = ROLE_PREFIX + args.user
role_id = get_roleid(role_name, restclient)
if role_id is None:
    role_id = create_role(role_name, restclient)
    print "Role '{}' created. ID = {}".format(role_name, role_id)
else:
    print "Role '{}' already exists. ID = {}".format(role_name, role_id)

# add the Owner ability to the role
if add_ability_to_role('SCOPE_OWNER', role_id, scope_id) == 200:
    print "Added scope owner ability to role {}".format(role_name)
else:
    print "Problem adding scope owner ability to that role."


# ============================================================================
# Assign new role to the appropriate user
# ----------------------------------------------------------------------------

add_role_path = '/openapi/v1/users/' + str(user_id) + '/add_role'
role_payload = {"role_id": role_id}
resp = restclient.put(add_role_path, json_body=json.dumps(role_payload))
if resp.status_code == 200:
    print "Role {} added to user {}.".format(role_name, username)
else:
    print "Problem adding role {} to user {}".format(role_name, username)

print "\n"
