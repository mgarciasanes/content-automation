"""
remove-lab-space.py for Tetration will delete the scopes and roles
created by the create-lab-space.py script. It will not delete
the user.

Author: Doron Chosnek, Cisco Systems, June 2017
"""

# pylint: disable=invalid-name

import json
import argparse
import requests.packages.urllib3
from tetpyclient import RestClient

API_ENDPOINT = "https://perseus-aus.cisco.com"
API_CREDS = "api_credentials.json"
FIELD_LABS_SCOPE = "596d57a0497d4f3eaef1fd07"
DOMAIN = 'cisco.com'
ROLE_PREFIX = 'Field Scope Owner '

# =============================================================================
# ARGPARSE
# -----------------------------------------------------------------------------

parser = argparse.ArgumentParser(description='Add a scope for the specified user.')
parser.add_argument('-user', '-u', required=True, help="username (CEC ID)")
parser.add_argument('-endpoint', default=API_ENDPOINT, help="URL of Tetration instance")
parser.add_argument('-credentials', default=API_CREDS, help="path to credentials file")
args = parser.parse_args()


# Disable warnings
requests.packages.urllib3.disable_warnings()
restclient = RestClient(
    args.endpoint,
    credentials_file=args.credentials,
    verify=False
)
# ============================================================================
# Find and remove scope and child scopes (child scopes removed first)
# ----------------------------------------------------------------------------

resp = restclient.get('/openapi/v1/app_scopes')

for scope in resp.json():
    if scope["short_name"] == args.user and scope["parent_app_scope_id"] == FIELD_LABS_SCOPE:
        for child in scope["child_app_scope_ids"]:
            resp = restclient.delete('/app_scopes/' + str(child))
            print resp
        resp = restclient.delete('/app_scopes/' + str(scope["id"]))
        print resp

# ============================================================================
# Find and remove role for the user
# ----------------------------------------------------------------------------

resp = restclient.get('/openapi/v1/roles')

role_name = ROLE_PREFIX + args.user

for role in resp.json():
    if role["name"] == role_name:
        resp = restclient.delete('/roles/' + str(role["id"]))
        print resp
