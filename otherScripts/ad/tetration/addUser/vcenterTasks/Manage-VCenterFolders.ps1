﻿

# ====================================================
# GLOBALS
# ----------------------------------------------------

$vcenter_address = 'tetration-lab-vcenter.auslab.cisco.com'
$field_labs_foldername = 'Field Labs'
$datacenter_name = 'Tetration-DC-1'
$domain = 'auslab'



# ====================================================
# Load modules & log in
# ----------------------------------------------------

<#
    Must first install VMWare PowerCLI module. First confirm you can get to PowerCLI Gallery:
    PS> Find-Module -Name VMware.PowerCLI

    If that works, you can run the install (note that the -AllowClobber option is required
    because some of the cmdlet names like Get-VM collide with the hyper-v cmdlet names):
    PS> Install-Module -Name VMware.PowerCLI –Scope CurrentUser -AllowClobber

    And then import it:
    PS> Import-Module VMware.PowerCLI

#>

Remove-Module hyper-v -ErrorAction SilentlyContinue
Import-Module VMware.PowerCLI


$cred = Get-Credential -Message 'Enter your Tetration vCenter credentials'

Write-Progress -Activity 'Logging in' -PercentComplete 0
$viserver = Connect-VIServer $vcenter_address -credential $cred

Write-Progress -Activity 'Discovering heirarchy' -PercentComplete 10
$dc = Get-Datacenter -Name $datacenter_name
$root = Get-Folder -name 'vm' -Location $dc -Type VM

# if the Field Labs folder doesn't already exist, create it
$folders = Get-Folder -Location $root -Type VM | select -ExpandProperty Name
if( $folders.Contains($field_labs_foldername) )
{
    $parent_folder = Get-Folder -Name $field_labs_foldername -Location $root
}
else
{
    $parent_folder = New-Folder -Name $field_labs_foldername -Location $root
}



# ====================================================
# Load team members
# ----------------------------------------------------

# get list of existing Field Labs folders and a list of existing users in the domain
Write-Progress -Activity 'Collecting data about existing objects' -PercentComplete 20
$folder_name = Get-Folder -Location $parent_folder -Type VM | select -ExpandProperty Name
$domain_user = Get-VIAccount -Domain $domain -User | select -ExpandProperty Name

# Walk through every member of the team...
# 1) if the given user's folder doesn't exist, create it
# 2) assign admin priv for each user to their own personal folder

$team = Get-Content -Raw team.json | Convertfrom-Json
$count = 0

$team | % {
    $u = $_.email -replace '@cisco.com', ''
    $count += 1

    Write-Progress -Activity 'Building folders and permissions' -Status "Working on $($u)" -PercentComplete ($count / $team.Count * 100)

    if( $folder_name -contains $u )
    {
        $child_folder = Get-Folder -Location $parent_folder -Name $u
    }
    else
    {
        $child_folder = New-Folder -Location $parent_folder -Name $u
    }

    # assign Admin priv to the child folder only if that user exists in the domain
    $principal = "auslab\$($u)"

    if( $domain_user -contains $principal )
    {
        New-VIPermission -Entity $child_folder -Role Admin -Principal $principal | Out-Null
    }

}


Disconnect-VIServer -Server $viserver -Confirm:$false
