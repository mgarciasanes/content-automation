"""
This script is meant to be called by the add user automation workflow (or script) to do aci related tasks

"""
import sys
import os
import re
import json
import signal
import re
import acitoolkit.acitoolkit as aci

"""
=========================================================================================================
GLOBAL VARS
---------------------------------------------------------------------------------------------------------
"""

JSON_PATH = os.path.dirname(__file__)
ADD_USER_MICRO_EPG_TEMPLATE = JSON_PATH + '/addUserMicroEpg.json'
ADD_USER_L3_CONTRACT_TEMPLATE = JSON_PATH + '/addUserL3Contract.json'
ATTACH_CONSUMED_CONTRACT_TEMPLATE = JSON_PATH + '/attachConsumedContract.json'
ATTACH_PROVIDED_CONTRACT_TEMPLATE = JSON_PATH + '/attachProvidedContract.json'

def prettyPrint(target):
    print json.dumps(target,sort_keys=True,indent=4)

def addToReplaceList(key,value):
    return {
                'key':key,
                'value':value
            }

def getTemplate(template,replaceList):
    with open(template) as json_file:
        json_data = json_file.read()
        for replaceTerm in replaceList:
            json_data = re.sub('{{' + replaceTerm["key"] + '}}',replaceTerm["value"],json_data)
    return json.loads(json_data)

def addUserMicroEpg(session,replaceList):
    postUrl = '/api/node/mo/uni/tn-Field-Labs/ap-User-Apps/epg-{{cec}}.json'
    postUrl = re.sub('{{' + replaceList[0]["key"] + '}}',replaceList[0]["value"],postUrl)
    print postUrl
    resp = session.push_to_apic(postUrl,getTemplate(ADD_USER_MICRO_EPG_TEMPLATE,replaceList))
    return resp

def addUserL3Contract(session,replaceList):
    postUrl = '/api/node/mo/uni/tn-common/brc-{{cec}}-l3-out.json'
    postUrl = re.sub('{{' + replaceList[0]["key"] + '}}',replaceList[0]["value"],postUrl)
    print postUrl
    resp = session.push_to_apic(postUrl,getTemplate(ADD_USER_L3_CONTRACT_TEMPLATE,replaceList))
    return resp

def attachConsumedContractToL3Out(session,replaceList):
    postUrl = '/api/node/mo/uni/tn-common/out-prod-l3-out/instP-External-Network.json'
    print postUrl
    resp = session.push_to_apic(postUrl,getTemplate(ATTACH_CONSUMED_CONTRACT_TEMPLATE,replaceList))
    return resp

def attachProvidedContractToL3Out(session,replaceList):
    postUrl = '/api/node/mo/uni/tn-common/out-prod-l3-out/instP-External-Network.json'
    print postUrl
    resp = session.push_to_apic(postUrl,getTemplate(ATTACH_PROVIDED_CONTRACT_TEMPLATE,replaceList))
    return resp

def attachConsumedContractToUserMicroEpg(session,replaceList):
    postUrl = '/api/node/mo/uni/tn-Field-Labs/ap-User-Apps/epg-{{cec}}.json'
    postUrl = re.sub('{{' + replaceList[0]["key"] + '}}',replaceList[0]["value"],postUrl)
    print postUrl
    resp = session.push_to_apic(postUrl,getTemplate(ATTACH_CONSUMED_CONTRACT_TEMPLATE,replaceList))
    return resp

def attachProvidedContractToUserMicroEpg(session,replaceList):
    postUrl = '/api/node/mo/uni/tn-Field-Labs/ap-User-Apps/epg-{{cec}}.json'
    postUrl = re.sub('{{' + replaceList[0]["key"] + '}}',replaceList[0]["value"],postUrl)
    print postUrl
    resp = session.push_to_apic(postUrl,getTemplate(ATTACH_PROVIDED_CONTRACT_TEMPLATE,replaceList))
    return resp

def attachConsumedContractToTetrationL3Out(session,replaceList):
    postUrl = '/api/node/mo/uni/tn-Tetration/out-Tetration-L3-Handoff/instP-Tetration-External-Nettwork.json'
    postUrl = re.sub('{{' + replaceList[0]["key"] + '}}',replaceList[0]["value"],postUrl)
    print postUrl
    resp = session.push_to_apic(postUrl,getTemplate(ATTACH_CONSUMED_CONTRACT_TEMPLATE,replaceList))
    return resp

def attachProvidedContractToTetrationL3Out(session,replaceList):
    postUrl = '/api/node/mo/uni/tn-Tetration/out-Tetration-L3-Handoff/instP-Tetration-External-Nettwork.json'
    postUrl = re.sub('{{' + replaceList[0]["key"] + '}}',replaceList[0]["value"],postUrl)
    print postUrl
    resp = session.push_to_apic(postUrl,getTemplate(ATTACH_PROVIDED_CONTRACT_TEMPLATE,replaceList))
    return resp

def main():
    description = ('Script to do ACI related user provisioning tasks')
    creds = aci.Credentials('apic', description)
    # reference for future arguments.....not used
    creds.add_argument('--cec', help='CEC (or sAMAccountname) of user to add', required=True)
    args = creds.get()
    # Login to APIC
    session = aci.Session(args.url, args.login, args.password)
    resp = session.login()

    if args.cec:
        replaceList = []
        replaceList.append(addToReplaceList('cec',args.cec))
        resp = addUserMicroEpg(session,replaceList)
        print resp
        resp = addUserL3Contract(session,replaceList)
        print resp
        resp = attachConsumedContractToL3Out(session,replaceList)
        print resp
        resp = attachProvidedContractToL3Out(session,replaceList)
        print resp
        resp = attachConsumedContractToUserMicroEpg(session,replaceList)
        print resp
        resp = attachProvidedContractToUserMicroEpg(session,replaceList)
        print resp
        resp = attachConsumedContractToTetrationL3Out(session,replaceList)
        print resp
        resp = attachProvidedContractToTetrationL3Out(session,replaceList)
        print resp
        

    # Gracefully close APIC session if interrupt is detected
    def signal_handler(signum, frame):
        if session.logged_in():
            if args.verbose:
                print "Interrupt detected closing session"
            session.close()
    signal.signal(signal.SIGINT, signal_handler)

    session.close()

if __name__ == '__main__':
    main()
