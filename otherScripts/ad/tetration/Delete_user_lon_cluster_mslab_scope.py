"""
add-one-user-to-tetration.py will add the specified user to Tetration
(if they don't already exist) and give them the MSLAB Read Only role.
The JSON file must be in the following format:

Author: Doron Chosnek, Cisco Systems, September 2017

CAUTION: the MSLAB_READ_ONLY role does not use the same ID in every
instance of Tetration. I have hard-coded a value here but you can change
it by passing a different value through a command line argument.
"""

# pylint: disable=invalid-name

import json
import argparse
import requests.packages.urllib3
from tetpyclient import RestClient

API_ENDPOINT="https://198.19.193.228"
API_CREDS = "api_credentials_lon.json"
MSLAB_READ_ONLY = "5a9f1f4a755f02793400c575"
DOMAIN = 'dcloud.cisco.com'

# =============================================================================
# ARGPARSE
# -----------------------------------------------------------------------------

parser = argparse.ArgumentParser(description='Add a scope for the specified user.')
parser.add_argument('-user', '-u', required=True, help="username")
parser.add_argument('-domain', '-d', default=DOMAIN, help="Email domain. Default is dcloud.cisco.com")
parser.add_argument('-endpoint', default=API_ENDPOINT, help="URL of Tetration instance")
parser.add_argument('-credentials', default=API_CREDS, help="path to credentials file")
parser.add_argument('-givenName', required=True, help="user given (first) name")
parser.add_argument('-surname', required=True, help="user surname (last name)")
parser.add_argument('-roleId', default=MSLAB_READ_ONLY, help="GUID for MSLAB Read Only role")
args = parser.parse_args() 
print("user passed is",args.user)
email=args.user+"@"+args.domain
print(email)


# ============================================================================
# Get current TA users and Delete required one
# ----------------------------------------------------------------------------

restclient = RestClient(
    args.endpoint,
    credentials_file=args.credentials,
    verify=False
)

requests.packages.urllib3.disable_warnings()
resp = restclient.get('/users')
#print json.dumps(resp.json(), indent=4, sort_keys=True)
existing = []
for person in resp.json():
   if(person["email"]==email):
     print(person["id"])
     user_id=person["id"]
     print('id is',user_id)
     #existing.append(person["email"])
     location= '/users/' + str(user_id)
     resp = restclient.delete(location)

      
   

        
    
    
