from tetpyclient import RestClient
import urllib3
urllib3.disable_warnings()
import json

API_ENDPOINT="https://198.19.193.228"

# ``verify`` is an optional param to disable SSL server authentication.
# By default, Tetration appliance dashboard IP uses self signed cert after
# deployment. Hence, ``verify=False`` might be used to disable server
# authentication in SSL for API clients. If users upload their own
# certificate to Tetration appliance (from ``Settings > Company`` Tab)
# which is signed by their enterprise CA, then server side authentication
# should be enabled.
# credentials.json looks like:
#{
  # "api_key": "5ec2c031dfc44e4a838b7d53090e9b4d",
   #"api_secret": "d533fdf34dda3c680368dd8a7ce8d946fa16084c"
 #}

restclient = RestClient(API_ENDPOINT,credentials_file='api_credentials_lon.json',verify=False)
# followed by API calls, for example API to retrieve list of agents.
# API can be passed /openapi/v1/sensors or just /sensors.
resp = restclient.get('/openapi/v1/roles')
#resp = restclient.get('/openapi/v1/users/5a9f1d4b497d4f67fa3d6392')
print json.dumps(resp.json(), indent=4, sort_keys=True)
print ("======================")
resp = restclient.delete('/openapi/v1/users/5a9f1d4b497d4f67fa3d6392')
resp = restclient.get('/openapi/v1/users/5a9f1d4b497d4f67fa3d6392')
print json.dumps(resp.json(), indent=4, sort_keys=True)

