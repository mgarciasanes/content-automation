param([string]$sessionFile = "none",[string]$output = "C:\Scripts\generic\output.txt", [string]$userId = "none" , [string]$password = "C1sco12345" , [string]$groupName="hyperflex-hw")

$LogFile="C:\scripts\hyperflex\createAdUser.log"
function LogWrite
{
   Param ([string]$logstring)
   $LogTime = Get-Date -Format "MM-dd-yyyy_hh-mm-ss"
   $LogSource= $sessionFile
   $LogTask = 'createAdUser'
   Add-content $Logfile -value $($LogTime +" | " + $LogSource +" | " + $LogTask + " | " + $logstring)
}


if ($groupName -eq "apic-v2" ){
    LogWrite("Group Name is Microsegmentation APIC Demo Users")
    $groupName = "Microsegmentation APIC Demo Users"
}

if ($groupName -eq "hyperflex-hw" ){
    LogWrite("Group Name is Hyperflex Hardware Demo Users")
    $groupName = "Hyperflex Hardware Demo Users"
}





LogWrite('starting script...')
Import-Module ActiveDirectory -ErrorAction SilentlyContinue

LogWrite("About to create new user in active directory with userID " + $userId + " password " + $password + " in group " + $groupName)

# Create User
try {
    LogWrite("Executing New-ADUser for user $userId")
    New-ADUser -SamAccountName $user.SamAccountName `
               -Name $userId `
               -DisplayName $userId `
               -GivenName $userId `
               -Surname $userId `
               -EmailAddress ($userId + '@dcv.svpod') `
               -UserPrincipalName ($userId + '@dcv.svpod') `
               -Enabled $true -ChangePasswordAtLogon $false `
               -PasswordNeverExpires  $true `
               -AccountPassword (ConvertTo-SecureString $password -AsPlainText -force) -PassThru `
     }
catch [System.Object]
     {
     LogWrite("Could not create user $userId ")
     LogWrite($_.Exception.GetType().FullName, $_.Exception.Message)
     Add-Content  $output ($_.Exception.GetType().FullName, $_.Exception.Message) 
     exit
     }
     


#Add USer to Group






try {
    LogWrite("Executing Add-ADGroupMember for user $userId")
    Add-ADGroupMember $groupName $userId
    }
catch [System.Object]
     {
     LogWrite("Could not add user $userId to group $groupName")
     LogWritet($_.Exception.GetType().FullName, $_.Exception.Message)
     Add-Content  $output ($_.Exception.GetType().FullName, $_.Exception.Message)
     exit 
     }
 
LogWrite("Script Completed Ok")                    
Add-Content  $output True

