param([string]$sessionFile = "none",[string]$output = "C:\Scripts\generic\output.txt", [string]$userId = "none")

$LogFile="C:\scripts\hyperflex\removeAdUser.log"
function LogWrite
{
   Param ([string]$logstring)
   $LogTime = Get-Date -Format "MM-dd-yyyy_hh-mm-ss"
   $LogSource= $sessionFile
   $LogTask = 'removeAdUser'
   Add-content $Logfile -value $($LogTime +" | " + $LogSource +" | " + $LogTask + " | " + $logstring)
}


LogWrite('starting script...')
Import-Module ActiveDirectory -ErrorAction SilentlyContinue

LogWrite("About to remove user in active directory with userID " + $userId)

# Create User
try {
    LogWrite("Executing Remove-ADUser for user $userId")
    Remove-ADUser -identity $userId -Confirm:$false
     }
catch [System.Object]
     {
     LogWrite("Could not remove user $userId ")
     LogWrite($_.Exception.GetType().FullName, $_.Exception.Message)
     Add-Content  $output ($_.Exception.GetType().FullName, $_.Exception.Message)
     exit
     }
     
 
LogWrite("Script Completed Ok")                    
Add-Content  $output True

