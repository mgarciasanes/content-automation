#!/bin/bash

__create_user() {
# Create a user to SSH into as.
useradd demouser
SSH_USERPASS=C1sco12345
echo -e "$SSH_USERPASS\n$SSH_USERPASS" | (passwd --stdin demouser)
echo ssh user password: $SSH_USERPASS
}

__change_root_password() {
# Create a user to SSH into as.
SSH_USERPASS=C^rr0t3965
echo -e "$SSH_USERPASS\n$SSH_USERPASS" | (passwd --stdin root)
echo ssh user password: $SSH_USERPASS
}



# Call all functions
__create_user
__change_root_password

