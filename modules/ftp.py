
import os
import sys
import time
import ftplib
from ftplib import FTP

modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger

####################################
#  Global Variables
####################################
credsFile=gen.credsFile
globalLogFormat=''


###################################
#  Front-end functions
###################################

def uploadFile(sessionId,target,otherInputs=[], logFormat=''):

    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            fileName=otherInputs["fileName"]
            filePath=otherInputs["filePath"]
            ftpFolder=otherInputs["ftpFolder"]

        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'


    result, message= ftpUploadFile(filePath=filePath,fileName=fileName,ftpDirectory=ftpFolder,ftpServer=creds['host'],username=creds['username'],password=creds['password'])

    if result:
        return {}, 'Success'
    else:
        logger.autolog(level=1,message='Failed: '+ message ,format=logFormat)
        return {}, 'Failed: ' + message


###################################





def ftpDownloadAll(path,destination,ftpServer="198.19.255.136",username="",password="", recursive=False):

    ftp=FTP(ftpServer)
    ftp.login(username,password)

    path_list=path.split("/")
    #print path_list
    dest_list=destination.split("\\")
    #print dest_list

    #clone path to destination
    try:
        ftp.cwd(path)
        dest=destination
        for folder in path_list[1:len(path_list)]:
            dest=os.path.join(dest,folder)
        logger.autolog(level=1, message=dest +" built")
        os.makedirs(dest)
    except OSError:
        #folder already exists at destination
        pass
    except ftplib.error_perm:
        #invalid entry (ensure input form: "/dir/folder/something/")
        logger.autolog(level=1, message="error: could not change to "+path)
        ftp.quit()
        return False

    #Copy files in current folder
    #list children:
    filelist=ftp.nlst()

    for file in filelist:
        try:
            #this will check if file is folder:
            logger.autolog(level=1, message="Testing  if directory in FTP : " + path+file+"/")
            ftp.cwd(path+file+"/")
            #if so, explore it:
            if recursive:
                ftpDownloadAll(path+file+"/",destination,ftpServer=ftpServer, username=username, password=password, recursive=recursive)
        except ftplib.error_perm:
            #not a folder with accessible content
            #download & return
            ftp.cwd(path)
            os.chdir(dest)
            #possibly need a permission exception catch:
            logger.autolog(level=1, message="about to download file " + str(file))
            ftp.retrbinary("RETR "+file, open(file,"wb").write)
            logger.autolog(level=1, message=file + " downloaded")

    ftp.quit()
    return True




    
def ftpUploadFile(filePath="",fileName="",ftpDirectory='',ftpServer='',username='',password=''):

    if filePath=='':
        logger.autolog(level=1, message='ftpUpload: ftpPath missing')
        return False, 'ftpUpload: ftpPath missing'
    if fileName=='':
        logger.autolog(level=1, message='ftpUpload: ftpName missing')
        return False, 'ftpUpload: ftpName missing'
    try:
        logger.autolog(level=1, message='ftpUpload: Connecting...')
        ftp =FTP(ftpServer)  # Connect to dCloud internal FTP server
        ftp.login(username,password)   # login with user/pwd

        logger.autolog(level=1, message='ftpUpload: Changing directory on FTP...')
        ftp.cwd(ftpDirectory)    # Go to desired folder where upload to (on FTP server)
        file=open(filePath+fileName,'rb')

        logger.autolog(level=1, message='ftpUpload: Uploading...')
        ftp.storbinary('STOR '+ fileName , file)
        file.close()
        ftp.quit()

    except Exception as e:
        logger.autolog(level=1, message='Error while uploading FTP ')
        logger.autolog(level=1, message=str(e))
        return False, 'Error while uploading FTP ' + str(e)
    return True,''

