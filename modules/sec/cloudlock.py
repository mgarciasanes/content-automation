
####################################
#  Module Details
#
# 	Name:
#	Description:
#	Author:
#	Version:
#	Date:
#
#
####################################



####################################
#  Imports
####################################
import sys
modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger

import logging
import os
import os.path
import requests
import time
import xml.dom.minidom
import yaml
import platform
from xml.dom import minidom
import datetime



####################################
#  Global Variables
####################################
credsFile=gen.credsFile


####################################
#  Front-End  Functions
####################################
def cleanupCloudlock(sessionId, target, otherInputs=[], logFormat=''):
    global scriptPath
    if (type(otherInputs) is dict):
        try:
            id=otherInputs["id"]
            device=otherInputs["device"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'


    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    
    result,eMsg= dcloudCleanup(device,creds['token_info'],logFormat)

    if result:
        return {},'Success'
    else:
        return {},'Failed: ' + str(eMsg) 



####################################
#  Generic  Functions
####################################


def removeSomething(id,dc,target,logFormat):
    return True,'OK'

def dcloudCleanup(device,token_info,logFormat= ''):
    device = device.split("dcloud@")[1].split(".com")[0]
    fqdnhostname = device + ".cloudlockng.com"
    logger.autolog(level=1,message='Fqdn host name - '+fqdnhostname ,format=logFormat)


    hostname = fqdnhostname.split(".com")[0]
    logger.autolog(level=1,message='Hostname - '+hostname ,format=logFormat)

    
    ami_name="ami-" + hostname
  
    cmd = 'curl https://dcloud:0fd04e86e0e39f57f3d2b5b77c706bc0@jenkins-dcloud.cloudlockng.com/job/terminate_aio/buildWithParameters/?name\='+fqdnhostname+'\&token\='+token_info
    logger.autolog(level=1,message='Curl Command - '+cmd ,format=logFormat)

    result = os.system(cmd)
    if result != 0 :
	return False, 'Error while executing termiante aio curl command' 

    time.sleep(10*60)

    cmd1 = 'curl https://dcloud:0fd04e86e0e39f57f3d2b5b77c706bc0@jenkins-dcloud.cloudlockng.com/job/build_aio_from_ami/buildWithParameters\?name\='+fqdnhostname+'\&ami_id\='+ami_name+'\&token\='+token_info
    logger.autolog(level=1,message='Curl Command-1 - '+cmd1 ,format=logFormat)

    result1 = os.system(cmd1)
    if result1 != 0 :
	return False, 'Error while executing restore aio curl command' 

    return True,'OK'
