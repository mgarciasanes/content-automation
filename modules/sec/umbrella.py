
####################################
#  Module Details
#
# 	Name:
#	Description:
#	Author:
#	Version:
#	Date:
#
#
####################################



####################################
#  Imports
####################################
import sys
modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger

import logging
import os
import os.path
import requests
import time
import xml.dom.minidom
import yaml
import platform
from xml.dom import minidom
import datetime



####################################
#  Global Variables
####################################
credsFile=gen.credsFile


####################################
#  Front-End  Functions
####################################
def umbrellaCleanup(sessionId, target, otherInputs=[], logFormat=''):
    global scriptPath
    if (type(otherInputs) is dict):
        try:
            id=otherInputs["id"]
            device=otherInputs["device"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'


    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    
    result,eMsg = umbrellaLabCleanup(device,creds['postman-token'],creds['x-admin-id'],creds['x-api-key'],creds['x-api-token'],logFormat)

    if result:
        return {},'Success'
    else:
        return {},'Failed: ' + str(eMsg) 



####################################
#  Generic  Functions
####################################


def removeSomething(id,dc,target,logFormat):
    return True,'OK'

def umbrellaLabCleanup(device,postman_token,x_admin_id,x_api_key,x_api_token,logFormat=''):
   count=0
   logger.autolog(level=1,message='device - '+str(device) ,format=logFormat)
   logger.autolog(level=1,message='postman_token - '+str(postman_token) ,format=logFormat)
   logger.autolog(level=1,message='x_admin_id - '+str(x_admin_id) ,format=logFormat)
   logger.autolog(level=1,message='x_api_key - '+str(x_api_key) ,format=logFormat)
   logger.autolog(level=1,message='x_api_token '+str(x_api_token) ,format=logFormat)
   
   
   while(1):
        
        cmd = "curl -X POST http://dcloud-reset.ateam.qq.opendns.com/reset/"+str(device)+" -H 'cache-control: no-cache' -H 'postman-token: "+str(postman_token)+"' -H 'x-admin-id: "+str(x_admin_id)+"' -H 'x-api-key: "+str(x_api_key)+"' -H 'x-api-token: "+str(x_api_token)+"'"
	print(cmd)
	logger.autolog(level=1,message='Curl Command - '+cmd ,format=logFormat)
		
	json_result=os.popen(cmd).read()
        temp=type(json_result)
	#logger.autolog(level=1,message='The json response - '+temp ,format=logFormat)
	logger.autolog(level=1,message='The json response - '+json_result ,format=logFormat)
		
        output=json_result.split(": ")
	logger.autolog(level=1,message='The json response after splitting collon - '+str(output) ,format=logFormat)
        final=output[0].split("\"")
	logger.autolog(level=1,message='The json response after splitting double quotes - '+str(final) ,format=logFormat)
        result=final[1]
	logger.autolog(level=1,message='The json response keyword - '+str(result) ,format=logFormat)
        print(result)
        if str(result) == "success":
           break
	else:
           count=count+1
           print("Error while executing umbrella curl command with attempt",count)
           logger.autolog(level=1,message='Error while executing curl command with attempt - '+str(count) ,format=logFormat)
           time.sleep(15)
           if count == 5:
              return False,'Failed after 5 attempts'
	     
    
  
  
   return True,'OK'
   
  
  
  
  
   