tenantName=$1
adminFile=$2

#kubectl --kubeconfig $adminFile get deployments -l io.contiv.tenant=$tenantName  | grep $tenantName | awk '{print $1}'

deploymentsToDelete=$(kubectl --kubeconfig $adminFile get deployments -l io.contiv.tenant=$tenantName -n default | grep $tenantName | awk '{print $1}' )
for deployment in "${deploymentsToDelete[@]}"
do
echo deleting deployment $deployment
kubectl --kubeconfig $adminFile delete deployment $deployment -n default 
done
exit 0





