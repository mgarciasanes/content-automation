import urllib2
import json
import os
import requests
import sys
import time
import platform

import ssl
import logging
requests.packages.urllib3.disable_warnings()

modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger






def sendHttpInLineRequest(protocol="http",host='', url='',data='', method='post', headers={}, port=80):

    if (port!=80):
        full = '%s://%s:%s%s%s' % (protocol,host,port,url,data)
    else:
        full = '%s://%s%s%s' % (protocol,host,url,data)
  
    if (method == 'post'):
        try:
            r = requests.post( full,data=data, verify=False, headers=headers)
            result=r.text
            logger.autolog(level=1, message="Sent HTTP request to " + str(full)) 
            logger.autolog(level=1, message=str(result))
            return r
        except Exception as e:
            logger.autolog(level=1, message="Error while sending request")
            logger.autolog(level=1, message=e) 
            return False



def sendRequestGetDict(protocol="http",host='', url='',data='', method='post', headers={},cookies={}, port=80,logFormat=''):
    r=sendHttpRequest(protocol=protocol,host=host, url=url,data=data, method=method, headers=headers, cookies=cookies, port=port,logFormat=logFormat)
    if r != False:
        logger.autolog(level=1, message='result='+str(r.text),format=logFormat)
        try:
            rDict=eval(str(r.text))
            return rDict
        except Exception as e:
            return {"noJson":str(r.text)}
    else:
        return False


def sendHttpRequest(protocol="http",host='', url='',data='', method='post', headers={},  cookies={} , basicAuth={}, port=80,logFormat=''):


    kwargs={}

    if (port!=80):
        full = '%s://%s:%s%s' % (protocol,host,port,url)
    else:
        full = '%s://%s%s' % (protocol,host,url)

    kwargs['data']=data
    kwargs['headers']=headers
    kwargs['verify']=False
    kwargs['cookies']=cookies


    try:
        if basicAuth!={}:
            kwargs['auth']=requests.auth.HTTPBasicAuth(basicAuth['user'],basicAuth['password'])
            logger.autolog(level=1, message="Will use basic authentication...",format=logFormat)
        #else:
            #basicAuth=requests.auth.HTTPBasicAuth('','')
    except Exception as e:
        logger.autolog(level=1, message="Failed to read basicAuth. Trying with no authentication...",format=logFormat)
        #basicAuth=requests.auth.HTTPBasicAuth('','')


    if (method == 'post'):
        try:
            logger.autolog(level=1, message="Will send HTTP POST request to " + str(full),format=logFormat)
            r = requests.post(full, **kwargs)
        except Exception as e:
            logger.autolog(level=1, message="Error while sending POST request..." + str(e) ,format=logFormat)
            return False
    if (method == 'put'):
        try:
            logger.autolog(level=1, message="Will send HTTP PUT request to " + str(full),format=logFormat)
            r = requests.post(full, **kwargs)
        except Exception as e:
            logger.autolog(level=1, message="Error while sending PUT  request..." + str(e) ,format=logFormat)
            return False
    if (method == 'get'):
        try:
            logger.autolog(level=1, message="Will send HTTP GET  request to " + str(full),format=logFormat)
            r = requests.get(full, **kwargs)
        except Exception as e:
            logger.autolog(level=1, message="Error while sending GET request..." + str(e) ,format=logFormat)
            return False
    if (method == 'delete'):
        try:
            logger.autolog(level=1, message="Will send HTTP DELETE  request to " + str(full),format=logFormat)
            r = requests.delete(full, **kwargs)
        except Exception as e:
            logger.autolog(level=1, message="Error while sending DELETE request..." + str(e) ,format=logFormat)
            return False

    result=r.text
    logger.autolog(level=1, message="Sent HTTP POST request to " + str(full),format=logFormat) 
    logger.autolog(level=1, message="Sent Data Payload is : " + str(data),format=logFormat)
    logger.autolog(level=1, message="Received Text: " + str(result),format=logFormat)
    logger.autolog(level=1, message="Received Status Code: " + str(r.status_code),format=logFormat)
    logger.autolog(level=1, message="Received Url: " + str(r.url),format=logFormat)
    logger.autolog(level=1, message="Received History: " + str(r.history),format=logFormat)
    logger.autolog(level=1, message="Received Cookies: " + str(r.cookies),format=logFormat)
    logger.autolog(level=1, message="Received Headers: " + str(r.headers),format=logFormat)
    return r