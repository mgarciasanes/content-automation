import logging
import threading
import urllib2
import glob
import json
import os
import os.path
import requests
import sys
import time
import xml.dom.minidom
import yaml
import platform
from xml.dom import minidom
import datetime

modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import logger as logger
import genFunctions as gen

# Import access classes
from cobra.mit.access import MoDirectory
from cobra.mit.session import LoginSession
from cobra.mit.request import ConfigRequest

# Import model classes
from cobra.model.fvns import VlanInstP, EncapBlk
from cobra.model.infra import RsVlanNs
from cobra.model.fv import Tenant, Ctx, BD, RsCtx, Ap, AEPg, RsBd, RsDomAtt
from cobra.model.vmm import DomP, UsrAccP, CtrlrP, RsAcc

import cobra.mit.access
import cobra.mit.request
import cobra.mit.session
import cobra.model.aaa
import cobra.model.fv
import cobra.model.pol
import cobra.model.vns
import cobra.model.vz
import cobra.model.draw
import cobra.model.vmm

import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()

###############################
# Global Variables
###############################
credsFile=gen.credsFile
tenantTemplate1 = {'name': 'myName',
                'pvn': 'myPVN',
                'bd': 'myBD',
                'ap': [{'name': 'myAPP',
                        'epgs': [{'name': 'app'},
                                 {'name': 'web'},
                                 {'name': 'db'},
                                ]
                       },
                      ]
                }

#####################################################
# Public Functions for APIC HW V2 (Microsegmentation)
#####################################################
def createUserApicV2(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC User ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=gen.sanitizeString(otherInputs["tenantName"])
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(credsFile,target)
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        createUser(moDir, userName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant' , format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC User ' + str(e)

    return {},'Success'


def createSecDomainApicV2(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Security Domain ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(credsFile,target)
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        createSecDo(moDir, secDoName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC Security Domain  ' , format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant'  + str(e)

    return {},'Success'


def createTenantApicV2(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Tenant  ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(credsFile,target)
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        createTenantApicMicroseg(moDir, tenantName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant '  + str(e)

    return {},'Success'


def deleteUserApicV2(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Delete  APIC User ' , format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=gen.sanitizeString(otherInputs["tenantName"])
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(credsFile,target)
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteUser(moDir, userName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while deleting APIC User ' , format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant '  + str(e)

    return {},'Success'


def deleteSecDomainApicV2(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Delete APIC Security Domain  ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(credsFile,target)
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteSecDo(moDir, secDoName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while deleting  APIC Security Domain ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant '  + str(e)

    return {},'Success'


def deleteTenantApicV2(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Delete  APIC Tenant '  , format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(credsFile,target)
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteTenant(moDir, tenantName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while deleting  APIC tenant  ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant '  + str(e)

    return {},'Success'



######################################## END APIC V2 ###########################################




#####################################################
# Public Functions for Contiv Demo V1
#####################################################

def createUserContivV1(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC User ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(credsFile,target)
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        createUser(moDir, userName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant' , format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC User '

    return {},'Success'


def createSecDomainContivV1(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Security Domain ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(credsFile,target)
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        createSecDo(moDir, secDoName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC Security Domain  ' , format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant'

    return {},'Success'


def createTenantContivV1(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Tenant  ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(credsFile,target)
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        createTenantContiv(moDir, tenantName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant'

    return {},'Success'


def deleteUserContivV1(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Delete  APIC User ' , format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(credsFile,target)
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteUser(moDir, userName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while deleting APIC User ' , format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant'

    return {},'Success'


def deleteSecDomainContivV1(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Delete APIC Security Domain  ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(credsFile,target)
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteSecDo(moDir, secDoName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while deleting  APIC Security Domain ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant'

    return {},'Success'


def deleteTenantContivV1(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Delete  APIC Tenant '  , format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(credsFile,target)
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteTenant(moDir, tenantName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while deleting  APIC tenant  ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant'

    return {},'Success'

############################################ END CONTIV V1 ###########################################



def createAsapdcConfig(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Tenant', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(credsFile,target)
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        createSecDoAsapdc(moDir, secDoName=tenantName)
        createUserAsapdc(moDir, userName=tenantName)
        createTenantAsapdc(moDir, tenantName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant'

    return {},'Success'




def removeAsapdcConfig(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Remove ASAPD  APIC Config', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(credsFile,target)
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteSecDo(moDir, secDoName=tenantName)
        deleteUser(moDir, userName=tenantName)
        deleteTenant(moDir, tenantName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while removing ASAPDC APIC configuration', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while removing ASAPDC  APIC Config '

    return {},'Success'


def deleteTenant(md, tenantName='tenantName'):
    # the top level object on which operations will be made
    topMo = cobra.model.pol.Uni('')
    # build the request using cobra syntax
    fvTenant = cobra.model.fv.Tenant(topMo,tenantName)
    fvTenant.delete()
    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)

def deleteSecDo(md, secDoName='secDoName'):
    # the top level object on which operations will be made
    topMo = cobra.model.pol.Uni('')
    # build the request using cobra syntax
    aaaUserEp = cobra.model.aaa.UserEp(topMo)
    aaaDomain = cobra.model.aaa.Domain(aaaUserEp,'SecDo-'+str(secDoName))
    aaaDomain.delete()
    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(aaaUserEp)
    md.commit(c)

def deleteUser(md, userName='userName'):
    # the top level object on which operations will be made
    topMo = cobra.model.pol.Uni('')
    # build the request using cobra syntax
    aaaUserEp = cobra.model.aaa.UserEp(topMo)
    aaaUser = cobra.model.aaa.User(aaaUserEp,userName)
    aaaUser.delete()
    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(aaaUser)
    md.commit(c)



def login(apicUrl,apicUsername,apicPassword):
    try:
        session = LoginSession(apicUrl,apicUsername,apicPassword)
        moDir = MoDirectory(session)
        moDir.login()
        logger.autolog(level=1,message='Logged to APIC ok')
        return moDir
    except Exception as e:
        logger.autolog(level=1,message='Error while connecting to APIC.')
        logger.autolog(level=1,message=str(e))
        return False

def getUniInfra(moDir):
    # Get the top level Policy Universe Directory
    uniMo = moDir.lookupByDn('uni')
    uniInfraMo = moDir.lookupByDn('uni/infra')
    return uniInfraMo

def getUni(moDir):
    # Get the top level Policy Universe Directory
    uniMo = moDir.lookupByDn('uni')
    return uniMo


def createVmmDomain(moDir,VMM_DOMAIN_INFO):
#VMM_DOMAIN_INFO = {'name': "mininet",
#                   'ctrlrs': [{'name': 'vcenter1', 'ip': '192.0.20.3', 
#                               'scope': 'vm'}],
#                   'usrs': [{'name': 'admin', 'usr': 'administrator',
#                             'pwd': 'pa$$word1'}],
#                   'namespace': {'name': 'VlanRange', 'from': 'vlan-100',
#                                 'to': 'vlan-200'}
#                  }
    vmmpVMwareProvPMo = moDir.lookupByDn('uni/vmmp-VMware')
    vmmDomPMo = DomP(vmmpVMwareProvPMo, VMM_DOMAIN_INFO['name'])
    
    vmmUsrMo = []
    for usrp in VMM_DOMAIN_INFO['usrs']:
        usrMo = UsrAccP(vmmDomPMo, usrp['name'], usr=usrp['usr'],pwd=usrp['pwd'])
        vmmUsrMo.append(usrMo)

    # Create Controllers under domain
    for ctrlr in VMM_DOMAIN_INFO['ctrlrs']:
        vmmCtrlrMo = CtrlrP(vmmDomPMo, ctrlr['name'], scope=ctrlr['scope'],
                            hostOrIp=ctrlr['ip'])
        # Associate Ctrlr to UserP
        RsAcc(vmmCtrlrMo, tDn=vmmUsrMo[0].dn)
    
    # Associate Domain to Namespace
    RsVlanNs(vmmDomPMo, tDn=fvnsVlanInstPMo.dn)
   
    vmmCfg = ConfigRequest()
    vmmCfg.addMo(vmmDomPMo)
    moDir.commit(vmmCfg)




def createTenantApicMicroseg(md, tenantName='tenantName'):
    # the top level object on which operations will be made
    topMo = cobra.model.pol.Uni('')
    # build the request using cobra syntax
    fvTenant = cobra.model.fv.Tenant(topMo, ownerKey=u'', name=tenantName, descr=u'', ownerTag=u'')
    aaaDomainRef = cobra.model.aaa.DomainRef(fvTenant, ownerKey=u'', name=u'SecDo-'+tenantName, descr=u'', ownerTag=u'')
    vzBrCP = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'web-load', prio=u'unspecified', targetDscp=u'unspecified', ownerTag=u'', descr=u'')
    vzSubj = cobra.model.vz.Subj(vzBrCP, revFltPorts=u'yes', name=u'Subject', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt = cobra.model.vz.RsSubjFiltAtt(vzSubj, directives=u'', tnVzFilterName=u'default')
    vzBrCP2 = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'allow_all', prio=u'unspecified', targetDscp=u'unspecified', ownerTag=u'', descr=u'')
    vzSubj2 = cobra.model.vz.Subj(vzBrCP2, revFltPorts=u'yes', name=u'Subject', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt2 = cobra.model.vz.RsSubjFiltAtt(vzSubj2, directives=u'', tnVzFilterName=u'icmp')
    vzRsSubjFiltAtt3 = cobra.model.vz.RsSubjFiltAtt(vzSubj2, directives=u'', tnVzFilterName=u'port_3306')
    drawCont = cobra.model.draw.Cont(fvTenant)
    drawInst = cobra.model.draw.Inst(drawCont, info=u"{'{fvAp/epg}-{db}':{'x':65,'y':329.5},'{fvAp/epg}-{web}':{'x':185,'y':329.5},'{fvAp/epg}-{Loadbalancer}':{'x':305,'y':329.5},'{fvAp/contract}-{allow_all}':{'x':120,'y':29.50000000000003},'{fvAp/contract}-{web-load}':{'x':240,'y':29.50000000000003}}", oDn=u'uni/tn-'+tenantName+u'/ap-Opencart')
    fvBD = cobra.model.fv.BD(fvTenant, ownerKey=u'', vmac=u'not-applicable', unkMcastAct=u'flood', name=u'opencart_bd', descr=u'', unkMacUcastAct=u'proxy', arpFlood=u'no', limitIpLearnToSubnets=u'no', llAddr=u'::', mcastAllow=u'no', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', unicastRoute=u'yes', ownerTag=u'', multiDstPktAct=u'bd-flood', type=u'regular', ipLearning=u'yes')
    fvRsBDToNdP = cobra.model.fv.RsBDToNdP(fvBD, tnNdIfPolName=u'')
    fvRsCtx = cobra.model.fv.RsCtx(fvBD, tnFvCtxName=u'opencart_vrf')
    fvRsIgmpsn = cobra.model.fv.RsIgmpsn(fvBD, tnIgmpSnoopPolName=u'')
    fvSubnet = cobra.model.fv.Subnet(fvBD, name=u'', descr=u'', ctrl=u'', ip=u'10.105.0.1/24', preferred=u'no', virtual=u'no')
    fvRsBdToEpRet = cobra.model.fv.RsBdToEpRet(fvBD, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvCtx = cobra.model.fv.Ctx(fvTenant, ownerKey=u'', name=u'opencart_vrf', descr=u'', knwMcastAct=u'permit', pcEnfDir=u'ingress', ownerTag=u'', pcEnfPref=u'enforced')
    fvRsBgpCtxPol = cobra.model.fv.RsBgpCtxPol(fvCtx, tnBgpCtxPolName=u'')
    fvRsCtxToExtRouteTagPol = cobra.model.fv.RsCtxToExtRouteTagPol(fvCtx, tnL3extRouteTagPolName=u'')
    fvRsOspfCtxPol = cobra.model.fv.RsOspfCtxPol(fvCtx, tnOspfCtxPolName=u'')
    vzAny = cobra.model.vz.Any(fvCtx, matchT=u'AtleastOne', name=u'', descr=u'')
    fvRsCtxToEpRet = cobra.model.fv.RsCtxToEpRet(fvCtx, tnFvEpRetPolName=u'')
    vnsSvcCont = cobra.model.vns.SvcCont(fvTenant)
    fvAp = cobra.model.fv.Ap(fvTenant, ownerKey=u'', prio=u'unspecified', name=u'Opencart', descr=u'', ownerTag=u'')
    fvAEPg = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Loadbalancer', prio=u'unspecified', descr=u'', pcEnfPref=u'unenforced')
    fvRsCons = cobra.model.fv.RsCons(fvAEPg, tnVzBrCPName=u'web-load', prio=u'unspecified')
    fvRsDomAtt = cobra.model.fv.RsDomAtt(fvAEPg, tDn=u'uni/vmmp-Microsoft/dom-ms-hybridcloud', primaryEncap=u'unknown', classPref=u'encap', delimiter=u'', instrImedcy=u'lazy', encap=u'unknown', resImedcy=u'immediate')
    vmmSecP = cobra.model.vmm.SecP(fvRsDomAtt, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'reject', ownerTag=u'', allowPromiscuous=u'reject', macChanges=u'reject')
    fvRsDomAtt2 = cobra.model.fv.RsDomAtt(fvAEPg, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', primaryEncap=u'unknown', classPref=u'useg', delimiter=u'', instrImedcy=u'lazy', encap=u'unknown', resImedcy=u'immediate')
    fvRsBd = cobra.model.fv.RsBd(fvAEPg, tnFvBDName=u'useg-inbound')
    fvRsCustQosPol = cobra.model.fv.RsCustQosPol(fvAEPg, tnQosCustomPolName=u'')
    fvAEPg2 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'db', prio=u'unspecified', descr=u'', pcEnfPref=u'unenforced')
    fvRsDomAtt3 = cobra.model.fv.RsDomAtt(fvAEPg2, tDn=u'uni/vmmp-Microsoft/dom-ms-hybridcloud', primaryEncap=u'unknown', classPref=u'encap', delimiter=u'', instrImedcy=u'lazy', encap=u'unknown', resImedcy=u'immediate')
    vmmSecP2 = cobra.model.vmm.SecP(fvRsDomAtt3, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'reject', ownerTag=u'', allowPromiscuous=u'reject', macChanges=u'reject')
    fvRsDomAtt4 = cobra.model.fv.RsDomAtt(fvAEPg2, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', primaryEncap=u'unknown', classPref=u'useg', delimiter=u'', instrImedcy=u'lazy', encap=u'unknown', resImedcy=u'immediate')
    fvRsBd2 = cobra.model.fv.RsBd(fvAEPg2, tnFvBDName=u'opencart_bd')
    fvRsCustQosPol2 = cobra.model.fv.RsCustQosPol(fvAEPg2, tnQosCustomPolName=u'')
    fvRsProv = cobra.model.fv.RsProv(fvAEPg2, tnVzBrCPName=u'allow_all', matchT=u'AtleastOne', prio=u'unspecified')
    fvAEPg3 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'web', prio=u'unspecified', descr=u'', pcEnfPref=u'unenforced')
    fvRsCons2 = cobra.model.fv.RsCons(fvAEPg3, tnVzBrCPName=u'allow_all', prio=u'unspecified')
    fvRsDomAtt5 = cobra.model.fv.RsDomAtt(fvAEPg3, tDn=u'uni/vmmp-Microsoft/dom-ms-hybridcloud', primaryEncap=u'unknown', classPref=u'encap', delimiter=u'', instrImedcy=u'lazy', encap=u'unknown', resImedcy=u'immediate')
    vmmSecP3 = cobra.model.vmm.SecP(fvRsDomAtt5, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'reject', ownerTag=u'', allowPromiscuous=u'reject', macChanges=u'reject')
    fvRsDomAtt6 = cobra.model.fv.RsDomAtt(fvAEPg3, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', primaryEncap=u'unknown', classPref=u'useg', delimiter=u'', instrImedcy=u'lazy', encap=u'unknown', resImedcy=u'immediate')
    fvRsBd3 = cobra.model.fv.RsBd(fvAEPg3, tnFvBDName=u'opencart_bd')
    fvRsCustQosPol3 = cobra.model.fv.RsCustQosPol(fvAEPg3, tnQosCustomPolName=u'')
    fvRsProv2 = cobra.model.fv.RsProv(fvAEPg3, tnVzBrCPName=u'web-load', matchT=u'AtleastOne', prio=u'unspecified')
    fvRsTenantMonPol = cobra.model.fv.RsTenantMonPol(fvTenant, tnMonEPGPolName=u'')
    vzFilter = cobra.model.vz.Filter(fvTenant, ownerKey=u'', name=u'web-load_Filter', descr=u'', ownerTag=u'')
    vzFilter2 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', name=u'port_80', descr=u'', ownerTag=u'')
    vzFilter3 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', name=u'icmp', descr=u'', ownerTag=u'')
    vzEntry = cobra.model.vz.Entry(vzFilter3, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'unspecified', descr=u'', matchDscp=u'unspecified', prot=u'icmp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'unspecified', name=u'icmp')
    vzFilter4 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', name=u'allow_all_Filter', descr=u'', ownerTag=u'')
    vzFilter5 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', name=u'port_3306', descr=u'', ownerTag=u'')
    vzEntry2 = cobra.model.vz.Entry(vzFilter5, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'3306', descr=u'', matchDscp=u'unspecified', prot=u'tcp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'3306', name=u'web_db')

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)


def createTenantContiv(md, tenantName='tenantName'):

    # the top level object on which operations will be made
    topMo = cobra.model.pol.Uni('')

    fvTenant = cobra.model.fv.Tenant(topMo, ownerKey=u'', name=tenantName, descr=u'', ownerTag=u'')
    aaaDomainRef = cobra.model.aaa.DomainRef(fvTenant, ownerKey=u'', name=u'SecDo-'+tenantName, descr=u'', ownerTag=u'')
    #vnsSvcCont = cobra.model.vns.SvcCont(fvTenant)
    #fvRsTenantMonPol = cobra.model.fv.RsTenantMonPol(fvTenant, tRn=u'monepg-default', tDn=u'uni/tn-common/monepg-default', rType=u'mo', tCl=u'monEPGPol', tContextDn=u'', forceResolve=u'yes', tType=u'name', tnMonEPGPolName=u'')

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)




def createTenantAsapdc(md, tenantName='tenantName'):
    # the top level object on which operations will be made
    topMo = cobra.model.pol.Uni('')


    # build the request using cobra syntax
    fvTenant = Tenant(topMo,tenantName)
    aaaDomainRef = cobra.model.aaa.DomainRef(fvTenant, ownerKey=u'', name=u'SecDo-'+tenantName, descr=u'', ownerTag=u'')
    fvBD = cobra.model.fv.BD(fvTenant, ownerKey=u'', vmac=u'not-applicable', unkMcastAct=u'flood', name=u'myBD', descr=u'', unkMacUcastAct=u'proxy', arpFlood=u'no', limitIpLearnToSubnets=u'no', llAddr=u'::', mcastAllow=u'no', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', unicastRoute=u'yes', ownerTag=u'', multiDstPktAct=u'bd-flood', type=u'regular', ipLearning=u'yes')
    fvRsBDToNdP = cobra.model.fv.RsBDToNdP(fvBD, tnNdIfPolName=u'')
    fvRsCtx = cobra.model.fv.RsCtx(fvBD, tnFvCtxName=u'myVRF')
    fvRsIgmpsn = cobra.model.fv.RsIgmpsn(fvBD, tnIgmpSnoopPolName=u'')
    fvRsBdToEpRet = cobra.model.fv.RsBdToEpRet(fvBD, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvCtx = cobra.model.fv.Ctx(fvTenant, ownerKey=u'', name=u'myVRF', descr=u'', knwMcastAct=u'permit', pcEnfDir=u'ingress', ownerTag=u'', pcEnfPref=u'enforced')
    fvRsBgpCtxPol = cobra.model.fv.RsBgpCtxPol(fvCtx, tnBgpCtxPolName=u'')
    fvRsCtxToExtRouteTagPol = cobra.model.fv.RsCtxToExtRouteTagPol(fvCtx, tnL3extRouteTagPolName=u'')
    fvRsOspfCtxPol = cobra.model.fv.RsOspfCtxPol(fvCtx, tnOspfCtxPolName=u'')
    vzAny = cobra.model.vz.Any(fvCtx, matchT=u'AtleastOne', name=u'', descr=u'')
    fvRsCtxToEpRet = cobra.model.fv.RsCtxToEpRet(fvCtx, tnFvEpRetPolName=u'')
    vnsSvcCont = cobra.model.vns.SvcCont(fvTenant)
    fvAp = cobra.model.fv.Ap(fvTenant, ownerKey=u'', prio=u'unspecified', name=u'myApp', descr=u'', ownerTag=u'')
    fvAEPg = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Web', prio=u'unspecified', descr=u'', pcEnfPref=u'unenforced')
    fvRsBd = cobra.model.fv.RsBd(fvAEPg, tnFvBDName=u'myBD')
    fvRsCustQosPol = cobra.model.fv.RsCustQosPol(fvAEPg, tnQosCustomPolName=u'')
    fvAEPg2 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Db', prio=u'unspecified', descr=u'', pcEnfPref=u'unenforced')
    fvRsBd2 = cobra.model.fv.RsBd(fvAEPg2, tnFvBDName=u'myBD')
    fvRsCustQosPol2 = cobra.model.fv.RsCustQosPol(fvAEPg2, tnQosCustomPolName=u'')
    fvAEPg3 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'App', prio=u'unspecified', descr=u'', pcEnfPref=u'unenforced')
    fvRsBd3 = cobra.model.fv.RsBd(fvAEPg3, tnFvBDName=u'myBD')
    fvRsCustQosPol3 = cobra.model.fv.RsCustQosPol(fvAEPg3, tnQosCustomPolName=u'')
    fvRsTenantMonPol = cobra.model.fv.RsTenantMonPol(fvTenant, tnMonEPGPolName=u'')

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)


def createUser(md,userName='userName'):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    aaaUserEp = cobra.model.aaa.UserEp(polUni)

    # build the request using cobra syntax
    aaaUser = cobra.model.aaa.User(aaaUserEp, ownerKey=u'', expires=u'no', firstName=userName, descr=u'', lastName=u'', clearPwdHistory=u'no', accountStatus=u'active', phone=u'', pwdLifeTime=u'no-password-expire', expiration=u'never', ownerTag=u'', email=u'', name=userName, pwd=userName)

    aaaUserDomain = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'SecDo-'+userName, descr=u'', ownerTag=u'')
    aaaUserRole = cobra.model.aaa.UserRole(aaaUserDomain, ownerKey=u'', privType=u'writePriv', name=u'admin', descr=u'', ownerTag=u'')
    aaaUserDomain2 = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'common', descr=u'', ownerTag=u'')
    aaaUserRole2 = cobra.model.aaa.UserRole(aaaUserDomain2, ownerKey=u'', privType=u'readPriv', name=u'read-all', descr=u'', ownerTag=u'')
    aaaUserDomain3 = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'all', descr=u'', ownerTag=u'')
    aaaUserRole3 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'aaa', descr=u'', ownerTag=u'')
    aaaUserRole4 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'fabric-admin', descr=u'', ownerTag=u'')
    aaaUserRole5 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'vmm-admin', descr=u'', ownerTag=u'')
    aaaUserRole6 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'tenant-ext-admin', descr=u'', ownerTag=u'')
    aaaUserRole7 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'ops', descr=u'', ownerTag=u'')
    aaaUserRole8 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'read-all', descr=u'', ownerTag=u'')
    aaaUserRole9 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'nw-svc-admin', descr=u'', ownerTag=u'')
    aaaUserRole10 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'admin', descr=u'', ownerTag=u'')
    aaaUserRole11 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'access-admin', descr=u'', ownerTag=u'')
    aaaUserRole12 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'tenant-admin', descr=u'', ownerTag=u'')

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(aaaUserEp)
    md.commit(c)


def createUserAsapdc(md,userName='userName'):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    aaaUserEp = cobra.model.aaa.UserEp(polUni)

    # build the request using cobra syntax
    aaaUser = cobra.model.aaa.User(aaaUserEp, ownerKey=u'', expires=u'no', firstName=userName, descr=u'', lastName=u'', clearPwdHistory=u'no', accountStatus=u'active', phone=u'', pwdLifeTime=u'no-password-expire', expiration=u'never', ownerTag=u'', email=u'', name=userName, pwd=userName)

    aaaUserDomain = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'SecDo-'+userName, descr=u'', ownerTag=u'')
    aaaUserRole = cobra.model.aaa.UserRole(aaaUserDomain, ownerKey=u'', privType=u'writePriv', name=u'admin', descr=u'', ownerTag=u'')
    aaaUserDomain2 = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'common', descr=u'', ownerTag=u'')
    aaaUserRole2 = cobra.model.aaa.UserRole(aaaUserDomain2, ownerKey=u'', privType=u'readPriv', name=u'read-all', descr=u'', ownerTag=u'')
    aaaUserDomain3 = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'all', descr=u'', ownerTag=u'')
    aaaUserRole3 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'aaa', descr=u'', ownerTag=u'')
    aaaUserRole4 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'fabric-admin', descr=u'', ownerTag=u'')
    aaaUserRole5 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'vmm-admin', descr=u'', ownerTag=u'')
    aaaUserRole6 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'tenant-ext-admin', descr=u'', ownerTag=u'')
    aaaUserRole7 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'ops', descr=u'', ownerTag=u'')
    aaaUserRole8 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'read-all', descr=u'', ownerTag=u'')
    aaaUserRole9 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'nw-svc-admin', descr=u'', ownerTag=u'')
    aaaUserRole10 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'admin', descr=u'', ownerTag=u'')
    aaaUserRole11 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'access-admin', descr=u'', ownerTag=u'')
    aaaUserRole12 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'tenant-admin', descr=u'', ownerTag=u'')

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(aaaUserEp)
    md.commit(c)


def createSecDo(md, secDoName='securityDomainName'):
    polUni = cobra.model.pol.Uni('')
    aaaUserEp = cobra.model.aaa.UserEp(polUni)

    # build the request using cobra syntax
    aaaDomain = cobra.model.aaa.Domain(aaaUserEp, name=u'SecDo-'+secDoName)

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(aaaUserEp)
    md.commit(c)



def createSecDoAsapdc(md, secDoName='securityDomainName'):
    polUni = cobra.model.pol.Uni('')
    aaaUserEp = cobra.model.aaa.UserEp(polUni)

    # build the request using cobra syntax
    aaaDomain = cobra.model.aaa.Domain(aaaUserEp, name=u'SecDo-'+secDoName)

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(aaaUserEp)
    md.commit(c)




def createTenantFromTemplate(moDir,TENANT_INFO):
#TENANT_INFO = [{'name': 'ExampleCorp',
#                'pvn': 'pvn1',
#                'bd': 'bd1',
#                'ap': [{'name': 'OnlineStore',
#                        'epgs': [{'name': 'app'},
#                                 {'name': 'web'},
#                                 {'name': 'db'},
#                                ]
#                       },
#                      ]
#                }
#              ]



    uniMo = moDir.lookupByDn('uni')

    for tenant in TENANT_INFO:
        logger.autolog(level=1,message="Creating tenant " + tenant['name'])
        fvTenantMo = Tenant(uniMo, tenant['name'])
        
        # Create Private Network
        Ctx(fvTenantMo, tenant['pvn'])
        
        # Create Bridge Domain
        fvBDMo = BD(fvTenantMo, name=tenant['bd'])
        
        # Create association to private network
        RsCtx(fvBDMo, tnFvCtxName=tenant['pvn'])
        
        # Create Application Profile
        for app in tenant['ap']:
            logger.autolog(level=1,message='Creating Application Profile: ' + app['name'])
            fvApMo = Ap(fvTenantMo, app['name'])
            
            # Create EPGs 
            for epg in app['epgs']:
                logger.autolog(level=1,message="Creating EPG: " + epg['name']) 
                fvAEPgMo = AEPg(fvApMo, epg['name'])
                
                # Associate EPG to Bridge Domain 
                RsBd(fvAEPgMo, tnFvBDName=tenant['bd'])
                # Associate EPG to VMM Domain
                # RsDomAtt(fvAEPgMo, vmmDomPMo.dn)

        # Commit each tenant seperately
        tenantCfg = ConfigRequest()
        tenantCfg.addMo(fvTenantMo)
        logger.autolog(message=str(fvTenantMo))
        moDir.commit(tenantCfg)



