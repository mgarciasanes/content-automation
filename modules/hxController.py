#import ssl
import logging
#from pyVmomi import vim
import time
import pexpectCommands as p
import sys 
import datetime
from dateutil import parser

modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
#import mainAutomation as main
import logger
import inMemoryStorage


####################################
#  Global Variables
####################################
#credsFile=r'/root/indemo_automation/creds.cfg'
credsFile=gen.credsFile

######################################
# Front-end Functions (Called externally)
######################################
def createDatastore(sessionFile, target,  otherInputs=[], logFormat=''):
    global credsFile
    
    logger.autolog(level=1,message=' Starting creation of DS in Hyperflex Cluster',format=logFormat)

    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            dsName=otherInputs["dsName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    # Get Credentials and connect to HX Controller
    #creds = gen.getDemoCredentialsByName(credsFile,target)
    #if not creds:
    #    return {},'Failed: Unable to get credentials for target'

    # Get Credentials and connect to HX Controller
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'


    logger.autolog(level=1,message='Credentials for ' + str(target) + ' are ok. DS Name is ' + str(dsName) ,format=logFormat)

    #if not hxCreateDatastore(creds, dsName=dsName):

    myDs=hxCreateDatastore(creds, dsName=dsName)
    logger.autolog(level=1,message='hxCreateDatastore function has been called. Result is ' + str(myDs),format=logFormat)

    if not myDs: 
        logger.autolog(level=1,message='Unable to create DS',format=logFormat)
        return {},'Failed: Error while creating DS'
    else:
        logger.autolog(level=1,message='Created DS in HX Cluster',format=logFormat)
        return {},'Success'


def removeDatastore(sessionFile, target,  otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            dsName=otherInputs["dsName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    # Get Credentials and connect to HX Controller
    #creds = gen.getDemoCredentialsByName(credsFile,target)
    #if not creds:
    #    return {},'Failed: Unable to get credentials for target'

    # Get Credentials and connect to HX Controller
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    if not deleteUserDatastore(creds, dsName):
        return {}, 'Failed: Unable to delete DS'

    return {},'Success'


def datastoreCleanup(sessionFile, target,  otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            dsWhiteList=otherInputs["dsWhiteList"]
            dsTtlHours=otherInputs["dsTtlHours"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    # Get Credentials and connect to HX Controller
    #creds = gen.getDemoCredentialsByName(credsFile,target)
    #if not creds:
    #    return {},'Failed: Unable to get credentials for target'

    # Get Credentials and connect to HX Controller
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    dsToDelete=getDatastoresToBeDeletedRedis(creds,dsTtlHours=dsTtlHours,dsWhiteList=dsWhiteList,logFormat=logFormat)

    if dsToDelete==False:
        return {}, 'Failed: Unable to get list of datastores ready for deletion'

    if not deleteDatastores(creds, datastores=dsToDelete, logFormat=logFormat):
        return {}, 'Failed: List of datastores obtained but deletion failed'

    return {},'Success'


######################################
# Generic Functions (Called locally)
######################################


def getDatastoresList(creds):
    logger.autolog(level=1,message='About to get list of datastores from HX Controller')

    #creds = gen.getDemoCredentialsByName(credsFile,'hx-controller')

    prompt = 'root@SpringpathController'
    promptStar = prompt + r'.*'
    command = r"stcli datastore list |grep 'name:\|id:' | sed -e 's/^[ \t]*//' |  awk '{print $2}' | awk '!x[$0]++'"

    logonArray = {'nodeIP':creds['host'] , 'nodeUsername' : creds['username'] , 'nodePassword' : creds['password'], 'nodeLoginPrompt': prompt }
    expectArray=[]
    expectArray.append({'order': 0, 'cmd': command , 'expect': promptStar , 'timeout' : 10 , 'next': 'end'})
    capturedOutput = p.pexpectExecuteArray(expectArray, logonArray)

    if not capturedOutput:
        logger.autolog(level=1,message='Error while getting pexpect output on Hx Controller' )
        return False

    datastores=[]
    try:
        #print(str(capturedOutput))
        lines = str(capturedOutput[0]).split('\r\n')
        lines.pop(0) #remove the command from output
        for i in range(len(lines)/3):
            datastores.append({'hx-id': lines[3*i],'name': lines[3*i+1], 'vmware-id': lines[3*i+2]})
        logger.autolog(level=1,message=str(datastores))
        return datastores
       
        #return gen.formatOutput(lines), True
    except Exception as e:
        logger.autolog(level=1,message='Error while obtainign list of datastores')
        logger.autolog(level=1,message=str(e))
        #return gen.formatOutput(False), False
        return False

def getDatastoreDetails(creds, dsName):
    datastores=getDatastoresList(creds)
    if not datastores:
        logger.autolog(level=1,message='Unable to get list of datastores')
        return False

    for ds in datastores:
        if ds['name']==dsName:
            return ds
    logger.autolog(level=1,message='No datastore found with provided name ' + dsName)
    return {}



def deleteUserDatastore(creds, dsName):

    return deleteDatastore(creds,dsName)

    myDsDetails= getDatastoreDetails(creds, dsName)

    if myDsDetails:
        if myDsDetails != {}:
            return  deleteDatastores(creds, [myDsDetails])
    else:
        logger.autolog(level=1, message='No DS to delete.')
        return True


def hxCreateDatastore(creds, dsName='', dsSize='1', sizeUnit='tb'):
    import pexpectCommands as p
    logger.autolog(level=1,message='  About to Create datastore with name ' + str(dsName))
    
    prompt = 'root@SpringpathController'
    promptStar = prompt + r'.*'

    command = r" stcli datastore create --name " + dsName + " --size " + dsSize + " --unit " + sizeUnit
    logonArray = {'nodeIP':creds['host'] , 'nodeUsername' : creds['username'] , 'nodePassword' : creds['password'], 'nodeLoginPrompt': prompt }
    expectArray=[]
    expectArray.append({'order': 0, 'cmd': command , 'expect': promptStar , 'timeout' : 30 , 'next': 'end'})

    tries=0
    while (tries < 3):

        capturedOutput = p.pexpectExecuteArray(expectArray, logonArray)

        logger.autolog(level=1,message='Result of DS Creation on try ' +str(tries)  + ' is '  + str(capturedOutput ))
        #logger.autolog(level=1,message='About to get DS details with creds ' + str(creds) + ' and dsName ' + str(dsName))

        if "mounted: False" in str(capturedOutput):
            logger.autolog(level=1,message=' Datastore with name ' + str(dsName) + ' has been created but not mounted.')
            deleteDatastore(creds, dsName)
            time.sleep(5)
            tries=tries+1
            continue

        if "Datastore already exists" in str(capturedOutput):
            logger.autolog(level=1,message=' Datastore with name ' + str(dsName) + ' already exists. Not need to create.')
            return True

        if "Created Springpath datastore with name" in str(capturedOutput):
            logger.autolog(level=1,message=' Datastore with name ' + str(dsName) + ' has been created.')
            return True

        logger.autolog(level=1,message=' Datastore with name ' + str(dsName) + ' has NOT been created.')
        tries=tries+1
        time.sleep(5)


    return False



    myDsDetails=getDatastoreDetails(creds, dsName)
    if myDsDetails:
        if myDsDetails != {}:
            return myDsDetails
    else:
        return False


def deleteDatastores(creds, datastores=[], logFormat=''):
    import pexpectCommands as p
    logger.autolog(level=1,message='About to deltete datastores', format=logFormat)
    logger.autolog(level=1,message=str(datastores), format=logFormat)

    #creds = gen.getDemoCredentialsByName(credsFile,'hx-controller')

    prompt = 'root@SpringpathController'
    promptStar = prompt + r'.*'

    result=True
    for ds in datastores:
        logger.autolog(level=1,message='About to deltete datastore ' + ds['name'], format=logFormat)
        command = r" stcli datastore delete --name " + ds['name']
        logonArray = {'nodeIP':creds['host'] , 'nodeUsername' : creds['username'] , 'nodePassword' : creds['password'], 'nodeLoginPrompt': prompt }
        expectArray=[]
        expectArray.append({'order': 0, 'cmd': command , 'expect': promptStar , 'timeout' : 30 , 'next': 'end'})
        capturedOutput = p.pexpectExecuteArray(expectArray, logonArray)

        if "Datastore not found" in str(capturedOutput):
            logger.autolog(level=1,message=' Datastore with name ' + str(ds['name']) + ' has not been found. Not need to delete.', format=logFormat)
        if "ResourceInUse" in str(capturedOutput):
            logger.autolog(level=1,message=' Datastore with name ' + str(ds['name']) + ' can not be deleted. It is in use.', format=logFormat)
            result=False
        if "Datastore delete failed" in str(capturedOutput):
            logger.autolog(level=1,message=' Datastore with name ' + str(ds['name']) + ' failed to delete.', format=logFormat)
            result=False
        logger.autolog(level=1,message=' deletion of datastore with name ' + str(ds['name']) + ' returned nothing. Assuming it deleted.', format=logFormat)
        
    return result



def deleteDatastore(creds, dsName):
    import pexpectCommands as p
    logger.autolog(level=1,message='About to delete datastore ' + str(dsName))

    prompt = 'root@SpringpathController'
    promptStar = prompt + r'.*'

    command = r" stcli datastore delete --name " + str(dsName)
    logonArray = {'nodeIP':creds['host'] , 'nodeUsername' : creds['username'] , 'nodePassword' : creds['password'], 'nodeLoginPrompt': prompt }
    expectArray=[]
    expectArray.append({'order': 0, 'cmd': command , 'expect': promptStar , 'timeout' : 30 , 'next': 'end'})
    

    tries=0
    while (tries < 3):

        capturedOutput = p.pexpectExecuteArray(expectArray, logonArray)

        if "Datastore not found" in str(capturedOutput):
            logger.autolog(level=1,message=' Datastore with name ' + str(dsName) + ' has not been found. Not need to delete.')
            return True
        if "ResourceInUse" in str(capturedOutput):
            logger.autolog(level=1,message=' Datastore with name ' + str(dsName) + ' can not be deleted. It is in use.')
            tries=tries+1
            continue
            #return False
        if "Datastore delete failed" in str(capturedOutput):
            logger.autolog(level=1,message=' Datastore with name ' + str(dsName) + ' failed to delete.')
            tries=tries+1
            continue
            #return False
        logger.autolog(level=1,message=' deletion of datastore with name ' + str(dsName) + ' returned nothing. Assuming it deleted.')
        return True






def getDatastoresToBeDeleted(creds):
    import datetime
    import pickle

    datastores=getDatastoresList(creds)
    now = datetime.datetime.now()
    fiveDaysAgo = now - datetime.timedelta(days=5)
    fiveHoursAgo = now - datetime.timedelta(hours=5)
    fiveMinutesAgo = now - datetime.timedelta(minutes=5)
    oneDayAgo = now - datetime.timedelta(days=1)
    oneHourAgo = now - datetime.timedelta(hours=1)
    oneMinuteAgo = now - datetime.timedelta(minutes=1)
    thirtyMinutesAgo = now - datetime.timedelta(minutes=30)

    expiryTime = thirtyMinutesAgo

    datastoresInLog=[]
    try:
        with open('/root/hyperflex/dslog','rb') as f:
            datastoresInLog= pickle.load(f)
    except Exception as e:
        logger.autolog(level=1,message="Error while opening dslog file. Will create a new one  : " + str(e))
        
    dsToBeDeleted = []

    for ds in datastores:
        found = False
        logger.autolog(level=1,message="Checking DS " + ds['name'])
        if ds['name'] not in ['dCloud_desktops', 'dCloud_infra', 'VM-Sandbox','Boot-Storm']:
            for dsInLog in datastoresInLog:        
                if dsInLog['ds']['hx-id']== ds['hx-id']:
                    found = True
                    if (dsInLog['creationDate'] < expiryTime ):
                        logger.autolog(level=1,message='DS with name ' + ds['name'] + ' was created more than 5 days ago, marking for deletion...')
                        #logger.autolog(level=1,message=str(dsInLog['creationDate']))
                        #logger.autolog(level=1,message=str(expiryTime))
                        dsToBeDeleted .append(ds)
                        datastoresInLog.remove(dsInLog)                            
                    else:
                        logger.autolog(level=1,message='DS with name ' + ds['name'] + ' was created less than 5 days ago, leaving DS be...')
                        #logger.autolog(level=1,message=str(dsInLog['creationDate']))
                        #logger.autolog(level=1,message=str(expiryTime))
            if not found:
                logger.autolog(level=1,message='DS with name ' +  ds['name'] + ' is new. Adding to datastoresInLog')
                datastoresInLog.append({'ds': ds , 'creationDate': now})

    #logger.autolog(level=1,message=str(datastoresInLog))

    with open('/root/hyperflex/dslog','wb') as f:
        pickle.dump(datastoresInLog,f)

    logger.autolog(level=1,message='Datastores to be deleted:')
    logger.autolog(level=1,message=str(dsToBeDeleted))
    return dsToBeDeleted



def getDatastoresToBeDeletedRedis(creds,dsTtlHours=48, dsWhiteList=[], logFormat=''):


    hardCodedDsWhiteList=['dCloud_desktops', 'dCloud_infra', 'dCloud_Instant',  'dCloud-Instant', 'VM-Sandbox','Boot-Storm','CitrixCloud']
    whiteList=hardCodedDsWhiteList + dsWhiteList

    now = datetime.datetime.now()
    fiveDaysAgo = now - datetime.timedelta(days=5)
    fiveHoursAgo = now - datetime.timedelta(hours=5)
    fiveMinutesAgo = now - datetime.timedelta(minutes=5)
    oneDayAgo = now - datetime.timedelta(days=1)
    oneHourAgo = now - datetime.timedelta(hours=1)
    oneMinuteAgo = now - datetime.timedelta(minutes=1)
    thirtyMinutesAgo = now - datetime.timedelta(minutes=30)

    expiryTime = now - datetime.timedelta(hours=int(dsTtlHours))
    #expiryTime = now - datetime.timedelta(minutes=1)


    #Get list of Datastore in HX
    datastores=getDatastoresList(creds)
    if not datastores:
        logger.autolog(level=1,message="Error while getting list of  datastores from HX controller. Aborting datastore cleanup...", format=logFormat)
        return False

    #Get list of Datastores logged in REDIS
    datastoresInLog=inMemoryStorage.redisGetJson('hxDatastores',logFormat=logFormat)
    if not datastoresInLog:
        logger.autolog(level=1,message="Error while reading REDIS for hxDatastores. Assuming empty list...", format=logFormat)
        datastoresInLog=[]

    #Initialize list        
    dsToBeDeleted = []


    logger.autolog(level=1,message="Going through  datastores list...", format=logFormat)
    for ds in datastores:
        found = False
        logger.autolog(level=1,message="Checking DS " + ds['name'], format=logFormat)
        #Make sure we will not remove any datastores in our whitelist...
        if ds['name'] not in whiteList:
            for dsInLog in datastoresInLog:        
                if dsInLog['hx-id']== ds['hx-id']:
                    found = True
                    if (parser.parse(dsInLog['creationDate']) < expiryTime ):
                        logger.autolog(level=1,message='DS with name ' + ds['name'] + ' was created more than '+ str(dsTtlHours) +' hours ago, marking for deletion...', format=logFormat)
                        #logger.autolog(level=1,message=str(dsInLog['creationDate']))
                        #logger.autolog(level=1,message=str(expiryTime))
                        dsToBeDeleted.append(ds)
                        #Remove Datastore from the array to be written in REDIS
                        datastoresInLog.remove(dsInLog)                            
                    else:
                        logger.autolog(level=1,message='DS with name ' + ds['name'] + ' was created less than '+ str(dsTtlHours) +' hours ago, leaving DS be...', format=logFormat)
                        #logger.autolog(level=1,message=str(dsInLog['creationDate']))
                        #logger.autolog(level=1,message=str(expiryTime))
            if not found:
                logger.autolog(level=1,message='DS with name ' +  ds['name'] + ' is new. Adding to datastoresInLog', format=logFormat)
                #Adding Datastore in array to be writen in REDIS
                ds['creationDate']=now.strftime('%H:%M %d/%m/%Y')
                datastoresInLog.append(ds)

    #logger.autolog(level=1,message=str(datastoresInLog))

    #Write list of active datastores to REDIS
    if not inMemoryStorage.redisSetJson('hxDatastores', datastoresInLog,logFormat=logFormat):
        logger.autolog(level=1,message='Unable to write datastoresInLog to REDIS. Aborting deletion...',format=logFormat)
        return False 

    logger.autolog(level=1,message='Datastores to be deleted:',format=logFormat)
    logger.autolog(level=1,message=str(dsToBeDeleted),format=logFormat)
    return dsToBeDeleted