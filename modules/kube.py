import urllib2
import json
import os
import requests
import sys
import time
import platform
import yaml
import ssl
import xmltodict
import logging
requests.packages.urllib3.disable_warnings()
import pexpectCommands as p
import subprocess

modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger
import http as http
import worker as worker

from kubernetes import config, client

#configFile='/root/scripts/otherFiles/kubernetes/admin.conf'
os.system('mkdir /root/localFiles 2> /dev/null ')
os.system('mkdir /root/localFiles/kubernetes 2> /dev/null')
configFile='/root/localFiles/kubernetes/admin.conf'



deployment1='''
apiVersion: v1
kind: DEPLOYMENTKIND
metadata:
  name: PODNAME
  labels:
    app: LABELAPP
    io.contiv.tenant: LABELTENANT
    io.contiv.network: LABELNETWORK
    io.contiv.net-group: LABELGROUP
spec:
  containers:
  - name: CONTAINERNAME
    image: CONTAINERIMAGE
'''

deployment2='''
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: DEPLOYMENTNAME
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: LABELAPP
        io.contiv.tenant: LABELTENANT
        io.contiv.network: LABELNETWORK
        io.contiv.net-group: LABELGROUP
    spec:
      containers:
      - name: CONTAINERNAME
        image: CONTAINERIMAGE
'''


def deploymentKube(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            networkName=otherInputs["networkName"]
            groupName=otherInputs["groupName"]
            containerName=otherInputs["containerName"]
            containerImage=otherInputs["containerImage"]
            deploymentName=otherInputs['deploymentName']

        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    #podConfig={'DEPLOYMENTKIND':'Pod','PODNAME':'example1.com','LABELAPP':'APP', 'LABELTENANT': tenantName , 'LABELNETWORK':networkName,'LABELGROUP':groupName, 'CONTAINERNAME': containerName , 'CONTAINERIMAGE':containerImage} 
    podConfig={'DEPLOYMENTNAME':deploymentName,'LABELAPP':'APP', 'LABELTENANT': tenantName , 'LABELNETWORK':networkName,'LABELGROUP':groupName, 'CONTAINERNAME': containerName , 'CONTAINERIMAGE':containerImage} 

    deploymentConf=getKubeDeploymentConfig(podConfig)
    #deploymentConf=deployment2
    if kubeDeployment(deploymentConf):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while deploying in Kubernetes'


def undeploymentKube(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'
    #if kubeUndeployment(tenantName=tenantName):
    #    return {'result':True},'Success'
    if kubectlDeleteAllDeployments(tenantName):
        return {'result':True},'Success'
    
    else:
        return {'result':False},'Failed: Error while undeploying in Kubernetes'




def testRecurrentString(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            folderName=otherInputs["folderName"]
            dcName=otherInputs["dcName"]
            groupName=otherInputs["groupName"]

        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    print folderName
    print dcName
    print groupName


    return {'result':True},'Success'



def getKubeDeploymentConfig(podConfig):
    global deployment2
    output=deployment2
    for key in podConfig.keys():
        output=output.replace(key, podConfig[key])
    #print output
    return output


def kubeDeployment(podConfig,namespace='default', ):
    global configFile
    try:
        getKubeAdminFile()
        logger.autolog(level=1, message='About to load Kube config file')
        config.load_kube_config(config_file=configFile)
        logger.autolog(level=1, message='About to load Kube Deployment YAML configuration ')
        dep = yaml.load(podConfig)
        k8s_beta = client.ExtensionsV1beta1Api()
        #k8s_beta = client.AppsV1beta1Api()
        logger.autolog(level=1, message='Starting Kube Deployment')
        resp = k8s_beta.create_namespaced_deployment(body=dep, namespace=namespace)
        logger.autolog(level=1, message="Deployment created. status='%s'" % str(resp.status))
    except Exception as e:
        logger.autolog(level=1, message="Error while deploying Kubernetes")
        #logger.autolog(level=1, message=str(resp))
        logger.autolog(level=1, message=str(e))
    return True


def kubeUndeployment(tenantName='',namespace='default'):
    global configFile
    config.load_kube_config(config_file=configFile)

    deleteOptions=client.V1DeleteOptions()
    deleteOptions.api_version = 'extensions/v1beta1'
    deleteOptions.grace_period_seconds = 0
    #deleteOptions.kind = kind
    deleteOptions.orphan_dependents = True
    #deleteOptions.preconditions = preconditions

    k8s_beta = client.ExtensionsV1beta1Api()
    #k8s_beta = client.AppsV1beta1Api()
    #resources=k8s_beta.get_api_resources()
    #print(str(resources))
 

    try:
        deps=k8s_beta.list_namespaced_deployment(namespace=namespace)
        depsToDelete=[]
        for item in deps.items:
            labels = item.metadata.labels
            for key in labels.keys():
                if labels[key] == tenantName:
                    depsToDelete.append(item)
                    break
        logger.autolog(level=1,message="Deleting Deployments for Tenant " + tenantName)
        for deps in depsToDelete:
            logger.autolog(level=1,message="Deleting Deployment " + str(deps.metadata.name))
            delDeps = k8s_beta.delete_namespaced_deployment(name=deps.metadata.name,namespace=namespace, body=deleteOptions)
    except Exception as e:
        logger.autolog(level=1,message='Error while cleaning up deployments')
        logger.autolog(level=1,message=str(e))
        return False

    try:
        rs=k8s_beta.list_replica_set_for_all_namespaces()
        rsToDelete=[]
        for item in rs.items:
            labels = item.metadata.labels
            for key in labels.keys():
                if labels[key] == tenantName:
                    rsToDelete.append(item)
                    break
        logger.autolog(level=1,message="Deleting Replica Sets for Tenant " + tenantName)
        for rs in rsToDelete:
            logger.autolog(level=1,message="Deleting Replica Set " + str(rs.metadata.name))
            delRs=k8s_beta.delete_namespaced_replica_set(name=rs.metadata.name, namespace=namespace, body=deleteOptions)
    except Exception as e:
        logger.autolog(level=1,message='Error while cleaning up replica sets')
        logger.autolog(level=1,message=str(e))
        return False

    try:
        k8s_v1=client.CoreV1Api()
        pods=k8s_v1.list_namespaced_pod(namespace=namespace)
        podsToDelete=[]
        for item in pods.items:
            labels = item.metadata.labels
            for key in labels.keys():
                if labels[key] == tenantName:
                    podsToDelete.append(item)
                    break
        logger.autolog(level=1,message="Deleting Pods  for Tenant " + tenantName)
        for pod in podsToDelete:
            logger.autolog(level=1,message="Deleting Pod " + str(pod.metadata.name)) 
            delRs=k8s_v1.delete_namespaced_pod(name=pod.metadata.name, namespace=namespace, body=deleteOptions)
    except Exception as e:
        logger.autolog(level=1,message='Error while cleaning up pods')
        logger.autolog(level=1,message=str(e))
        return False


    logger.autolog(level=1, message="Resources Cleanup finished!")
    return True



def getKubeAdminFile():
    global configFile

    import pexpectCommands as p
    reload(p)
    #kubeIp='198.19.254.63'
    kubeIp='kubernetes'
    username='root'
    password='C^rr0t3965'
    source='/etc/kubernetes/admin.conf'
    dest=configFile
    #prompt='root@198.19.254.63'
    prompt='root@kubernetes'
    timeout=60
    p.pexpectScp(ipAddress=kubeIp, username=username, password=password, prompt=prompt, source=source, dest=dest, timeout=timeout)
    return 


def kubectlDeleteAllDeployments(tenantName):
    global configFile
    try:
        getKubeAdminFile()
        logger.autolog(level=1, message='Making kube.sh executable...')
        os.system('chmod 777 /root/scripts/modules/kube.sh')
        logger.autolog(level=1, message='Excuting kube.sh with tenant name ' + str(tenantName) + '  and config file ' + str(configFile))
        output=subprocess.check_output('/root/scripts/modules/kube.sh ' + str(tenantName) + ' ' + str(configFile), shell=True)  
        logger.autolog(level=1, message=str(output))
        return True
    except Exception as e:
        logger.autolog(level=1,message='Error while cleaning up pods with kubectl')
        logger.autolog(level=1,message=str(e))
        return False




def execFromRecipe(sessionDetails, action='startup'):

    logFormat=sessionDetails['demo']+'|'+ sessionDetails['id'] +'|' + sessionDetails['owner']
    logger.autolog(level=1,message=str(sessionDetails), format=logFormat)
    logger.autolog(message='Got Request. Executing...', format=logFormat)


    if action == 'startup':
        tasks=gen.getDemoTasks(sessionDetails['demo'])
    if action == 'cleanup':
        tasks=gen.getDemoCleanup(sessionDetails['demo'])

    if not tasks:
        logger.autolog(message='Unable to get tasks for demo ' + sessionDetails['demo'], format=logFormat)
        return False
    else:
        failureFree,allTasksExecuted=worker.execTasks(sessionDetails,tasks)
        if (failureFree and allTasksExecuted):
            logger.autolog(message='Test  request executed without errors', format=logFormat)
        else:
            logger.autolog(message='Test request executed with errors or some tasks were not executed', format=logFormat)




if __name__ == '__main__':


    logger.setLogFiles(newLogFile='kubernetes.log',newLogDebugFile='kubernetes_DEBUG.log')
    logger.setLogger()
    logging.getLogger('werkzeug').setLevel(logging.ERROR)

    xml='<?xml version="1.0" encoding="UTF-8"?><session><demo>kubernetes-test</demo><datacenter>RTP</datacenter><id>182630</id><owner>mgarcias</owner><vpod>010</vpod><anycpwd>95f76f</anycpwd><type>session</type><devices></devices><translations></translations><dids></dids></session>'



 
    sessionDetails=xmltodict.parse(xml)['session']
    #execFromRecipe(sessionDetails)
    #execFromRecipe(sessionDetails, action='cleanup')


    #deploymentKube('10000', 'kube', otherInputs={'tenantName':'CONTIV-95','networkName':'contiv-net','groupName':'APP','containerName':'app','containerImage':'johnkday/contiv-test-container','deploymentName':'deploy1'})

    #deploymentKube('10000', 'kube', otherInputs={'tenantName':'CONTIV-95','networkName':'contiv-net','groupName':'DB','containerName':'db','containerImage':'johnkday/contiv-test-container','deploymentName':'deploy3'})

    undeploymentKube('10000', 'kube', otherInputs={'tenantName':'mgarcias-013'})
    undeploymentKube('10000', 'kube', otherInputs={'tenantName':'mgarcias-017'})
    undeploymentKube('10000', 'kube', otherInputs={'tenantName':'mgarcias-104'})
    undeploymentKube('10000', 'kube', otherInputs={'tenantName':'mgarcias018'})
    undeploymentKube('10000', 'kube', otherInputs={'tenantName':'mgarcias232'})
    undeploymentKube('10000', 'kube', otherInputs={'tenantName':'mgarcias482'})
    undeploymentKube('10000', 'kube', otherInputs={'tenantName':'mgarcias666'})


