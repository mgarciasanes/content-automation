import signalMonitor
import time

if __name__ == '__main__':
  killer = signalMonitor.SignalMonitor()
  while True:
    time.sleep(1)
    print("doing something in a loop ...")
    if killer.kill_now:
      break

  print "End of the program. I was killed gracefully :)"
