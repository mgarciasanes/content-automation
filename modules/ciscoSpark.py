
####################################
#  Imports
####################################
import sys
modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
modulesPath=r'/root/scripts/modules/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger
import os
import sys
import urllib2
import time
import requests
import json
import http as http

####################################
#  Global Variables
####################################


####################################
#  FRONT-END FUNCTIONS
####################################

def frontEndFunction(sessionId, target, otherInputs=[], logFormat=''):

    #INPUT
    if (type(otherInputs) is dict):
        try:
            # Get the inputs defined in the recipe
            # inputName=otherInputs=['inputName']
            pass
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'


    #CREDENTIALS
    #If credentials are needed for this task, get coordinates from the recipe 
    #creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    #if not creds:
    #    return {},'Failed: Unable to get credentials for target'


    #CODE HERE: 
    #Execute the task here
    result=[]


    #OUTPUT
    #Based on the results of the code above, return success or failed. 
    #In case of "failed", provide some error message to show on the dashboard.
    if result=='[]':
        return {},'Success'
    else:
        return {}, 'Failed: ' + str(result)



def spartTestFunction(sessionId, target, otherInputs=[], logFormat=''):
    msg=otherInputs['message']

    result=sendSparkPost(msg,target)

    if result:
        return {},'Success'
    else:
        return {},'Failed: ' + 'Failed...'  



####################################
#  GENERIC FUNCTIONS
####################################


def messagePost(msg,target):
    """
    This method is used for:
        -posting a message to the Spark room 
    """

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return False,'Error: Unable to get spark room details'

    data={"roomId": creds['spaceId'], "markdown": str(msg)}
    bot_bearer = creds['bot_bearer']
    url=creds['url']

    request = urllib2.Request(url, json.dumps(data),headers={"Accept" : "application/json","Content-Type":"application/json"})
    request.add_header("Authorization", "Bearer "+bot_bearer)
    contents = urllib2.urlopen(request).read()
    return contents

