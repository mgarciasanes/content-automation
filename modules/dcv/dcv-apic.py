import logging
import threading
import urllib2
import glob
import json
import os
import os.path
import requests
import sys
import time
import xml.dom.minidom
import yaml
import platform
from xml.dom import minidom
import datetime

modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import logger as logger
import genFunctions as gen

# Import access classes
from cobra.mit.access import MoDirectory
from cobra.mit.session import LoginSession
from cobra.mit.request import ConfigRequest

# Import model classes
from cobra.model.fvns import VlanInstP, EncapBlk
from cobra.model.infra import RsVlanNs
from cobra.model.fv import Tenant, Ctx, BD, RsCtx, Ap, AEPg, RsBd, RsDomAtt
from cobra.model.vmm import DomP, UsrAccP, CtrlrP, RsAcc

import cobra.mit.access
import cobra.mit.request
import cobra.mit.session
import cobra.model.aaa
import cobra.model.fv
import cobra.model.fvns
import cobra.model.pol
import cobra.model.vns
import cobra.model.vz
import cobra.model.draw
import cobra.model.vmm
import cobra.model.l3ext
import cobra.model.igmp




import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()

###############################
# Global Variables
###############################
credsFile=gen.credsFile
tenantTemplate1 = {'name': 'myName',
                'pvn': 'myPVN',
                'bd': 'myBD',
                'ap': [{'name': 'myAPP',
                        'epgs': [{'name': 'app'},
                                 {'name': 'web'},
                                 {'name': 'db'},
                                ]
                       },
                      ]
                }

###########################################################################################################
# Public Functions for APIC HW V2 (Microsegmentation)
##########################################################################################################
def createUserApicV2(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC User ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=gen.sanitizeString(otherInputs["tenantName"])
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        createUser(moDir, userName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant' , format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC User ' + str(e)

    return {},'Success'


def createSecDomainApicV2(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Security Domain ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        createSecDo(moDir, secDoName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC Security Domain  ' , format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant'  + str(e)

    return {},'Success'


def createTenantApicV2(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Tenant  ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        createContract(moDir, tenantName="common", contractName='l3out_lb_'+tenantName)
        createContractL3Out(moDir, tenantName='common', l3outName='L3_Out', networkName='Outside',contractName='l3out_lb_'+tenantName)
        createTenantApicMicroseg(moDir, tenantName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant '  + str(e)

    return {},'Success'


def deleteUserApicV2(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Delete  APIC User ' , format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=gen.sanitizeString(otherInputs["tenantName"])
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteUser(moDir, userName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while deleting APIC User ' , format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant '  + str(e)

    return {},'Success'


def deleteSecDomainApicV2(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Delete APIC Security Domain  ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteSecDo(moDir, secDoName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while deleting  APIC Security Domain ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant '  + str(e)

    return {},'Success'


def deleteTenantApicV2(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Delete  APIC Tenant '  , format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteTenant(moDir, tenantName=tenantName)
        deleteContractL3Out(moDir, tenantName='common', l3outName='L3_Out', networkName='Outside',contractName='l3out_lb_'+tenantName)
        deleteContract(moDir, tenantName="common", contractName='l3out_lb_'+tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while deleting  APIC tenant  ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant '  + str(e)

    return {},'Success'



######################################## END APIC V2 ###########################################



################################################################################################
# Public Functions for FUNCTIONS SECURE DC
################################################################################################


def createUserSecureDc(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC User ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteUser(moDir, userName=tenantName)
        createUserWithoutDomainAllWithVmware(moDir, userName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant' , format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC User ' + str(e)

    return {},'Success'



def createTenantApicSecureDc(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Tenant  ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        sdcCreateTenant(moDir,tenantName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant '  + str(e)

    return {},'Success'


def createTenantApicSecureDcV2(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Tenant for SecureDC  ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        sdcCreateTenantV2(moDir,tenantName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant '  + str(e)

    return {},'Success'



def createFmcDeviceManager(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create FMC Manager ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            csrIp=otherInputs["csrIp"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        sdcFmcDeviceManager(moDir ,tenantName,csrIp)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating FMC Manager ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating FMC Manager'  + str(e)
    return {},'Success'



def createFtdv1Device(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create FTDv1  Device ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            csrIp=otherInputs["csrIp"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        sdcFtdv1Device(moDir , tenantName, csrIp)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating FTDv1 Device ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating FTDv1 Device '  + str(e)
    return {},'Success'



def createFtdv2Device(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create FTDv2  Device ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            csrIp=otherInputs["csrIp"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        sdcFtdv2Device(moDir , tenantName, csrIp)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating FTDv2 Device ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating FTDv2 Device '  + str(e)
    return {},'Success'


def createFtdv1FunctionProfile(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create FTDv1  Function Profile ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        sdcFtdv1FunctionProfile(moDir , tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating FTDv1 Function Profile ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating FTDv1 Function Profile '  + str(e)
    return {},'Success'



def createFtdv2FunctionProfile(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create FTDv2  Function Profile ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        sdcFtdv2FunctionProfile(moDir , tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating FTDv2 Function Profile ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating FTDv2 Function Profile '  + str(e)
    return {},'Success'



def createFtdv1ServiceGraphTemplate(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create FTDv1  Service Graph Template ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        sdcFtdv1ServiceGraphTemplate(moDir , tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating FTDv1 Service Graph Template ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating FTDv1 Service Graph Template '  + str(e)
    return {},'Success'


def createAsavDevice(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create ASAv Device ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            csrIp=otherInputs["csrIp"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        sdcAsavDevice(moDir , tenantName,csrIp)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating ASAv Device ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating ASAv Device '  + str(e)
    return {},'Success'




def createAsavFunctionProfile(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create Asav  Function Profile ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        sdcAsavFunctionProfile(moDir , tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating Asav Function Profile ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating Asav Function Profile '  + str(e)
    return {},'Success'



def createFtdv2AsavServiceGraphTemplate(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create FTDv2 + ASAv  Service Graph Template ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        sdcFtdv2AsavServiceGraphTemplate(moDir , tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating FTDv2 + ASAv Service Graph Template ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating FTDv2 + ASAv Service Graph Template '  + str(e)
    return {},'Success'




def applyFtdv1ServiceGraphTemplate(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Apply FTDv1  Service Graph Template ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        sdcFtdv1ServiceGraphApply(moDir , tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while applying FTDv1 Service Graph Template ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while applying FTDv1 Service Graph Template '  + str(e)
    return {},'Success'



def applyFtdv2AsavServiceGraphTemplate(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Apply FTDv2 + ASAv  Service Graph Template ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            ipPoolIndex=otherInputs["ipPoolIndex"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        sdcFtdv2AsavServiceGraphApply(moDir , tenantName, ipPoolIndex)

    except Exception as e:
        logger.autolog(level=1,message='Error while applying FTDv2 + ASAv Service Graph Template ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while applying FTDv2 + ASAv Service Graph Template '  + str(e)
    return {},'Success'



def applyFtdv2AsavServiceGraphTemplateV2(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Apply FTDv2 + ASAv  Service Graph Template ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            ipPoolIndex=otherInputs["ipPoolIndex"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        sdcFtdv2AsavServiceGraphApplyV2(moDir , tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while applying FTDv2 + ASAv Service Graph Template ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while applying FTDv2 + ASAv Service Graph Template '  + str(e)
    return {},'Success'



#def addDevicesToTenantSecureDc(sessionDetails, target, otherInputs=[], logFormat=''):
#    logger.autolog(level=1,message='About to Create APIC Tenant  ', format=logFormat)
#    #Get Arguments
#    if (type(otherInputs) is dict):
#        try:
#            tenantName=otherInputs["tenantName"]
#            asaIp=otherInputs["asaIp"]
#            ngipsIp=otherInputs["ngipsIp"]
#            
#        except Exception as e:
#            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
#            logger.autolog(level=1,message=str(e), format=logFormat)
#            return {},'Failed: Error while reading input arguments'
#    else:
#        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
#        return {},'Failed: No arguments provided'
#
#    tenantName=gen.sanitizeString(tenantName)
#
#    try:
#        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
#        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
#
#        addDeviceAsaSecureDc(moDir,tenantName=tenantName, asaIp=asaIp)
#        addDeviceNgipsSecureDc(moDir,tenantName=tenantName, ngipsIp=ngipsIp)
#
#    except Exception as e:
#        logger.autolog(level=1,message='Error while creating APIC tenant ', format=logFormat)
#        logger.autolog(level=1,message=str(e), format=logFormat)
#        return {},'Failed: Error while creating APIC Tenant '  + str(e)
#
#    return {},'Success'


def addServiceGraphToTenantSecureDc(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Tenant  ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])

        addServiceGraphsAsaSecureDc(moDir,tenantName=tenantName)
        addServiceGraphsNgipsSecureDc(moDir,tenantName=tenantName)
        addServiceGraphsAsaNgipsSecureDc(moDir,tenantName=tenantName)


    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant '  + str(e)

    return {},'Success'


def addStaticRouteEpg(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Tenant  ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            apachePublicIp=otherInputs["apachePublicIp"]     
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)


    webOutIp="10.150.2."+str(apachePublicIp.split(".")[3])

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        sdcAddStaticRoutesToWeboutEpg(moDir, tenantName, attackerIp=u'10.150.1.100', internetSite=u'222.29.197.232/32', webToDbSubnet=u'10.150.3.0/24' , webOutIp=webOutIp )

    except Exception as e:
        logger.autolog(level=1,message='Error while creating static routes in FTDv1 ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while addind static routes to EPG '  + str(e)

    return {},'Success'








#####################################################
# Public Functions for ACI SECURITY INTEGRATION Demo
#####################################################


def createUserAciSecIntegration(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC User ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=gen.sanitizeString(otherInputs["tenantName"])
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteUser(moDir, userName=tenantName)
        createUserWithoutDomainAll(moDir, userName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant' , format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC User ' + str(e)

    return {},'Success'



def createTenantAciSecIntegration(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Tenant  ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            l3outVlan=otherInputs["l3outVlan"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'

        try:
            vlanOffset=otherInputs["vlanOffset"]
        except Exception as e:
            vlanOffset=0

        l3outVlan=str(int(l3outVlan)+int(vlanOffset))

    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        createAciSecLabTenant(moDir,l3outVlan=l3outVlan, tenantName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant '  + str(e)

    return {},'Success'



def createMgmtAppProfInMainTenant(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Tenant  ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            ctxName=otherInputs["ctxName"]
            bdName=otherInputs["bdName"]
            appProfName=otherInputs["appProfName"]
            epgName=otherInputs["epgName"]
            asaContext=otherInputs["asaContext"]
            asaMgmtVlanOffset=otherInputs["asaMgmtVlanOffset"]

        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments: ' + str(e)

    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    asaContextMgmtVlanId=int(asaContext) + int(asaMgmtVlanOffset)
  
    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        addBdToTenant(moDir ,bdName=bdName,ctxName=ctxName,tenantName=tenantName)
        addCtxToTenant(moDir ,ctxName=ctxName,tenantName=tenantName)
        addAppProfToTenant(moDir,tenantName=tenantName,appProfName=appProfName,bdName=bdName,epgName=epgName,vlanId=str(asaContextMgmtVlanId))

    except Exception as e:
        logger.autolog(level=1,message='Error while creating Application Profile in APIC tenant ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating Application Profile in APIC Tenant '  + str(e)

    return {"asaContextMgmtVlanId":asaContextMgmtVlanId},'Success'



def removeMgmtAppProfFromMainTenant(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Tenant  ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            ctxName=otherInputs["ctxName"]
            bdName=otherInputs["bdName"]
            appProfName=otherInputs["appProfName"]

        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'

    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        removeBdFromTenant(moDir ,bdName=bdName,tenantName=tenantName)
        removeCtxFromTenant(moDir ,ctxName=ctxName,tenantName=tenantName)
        removeAppProfFromTenant(moDir , tenantName=tenantName, appProfName=appProfName)


    except Exception as e:
        logger.autolog(level=1,message='Error while deleting Application Profile from APIC tenant ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while deleteing  Application Profule from APIC Tenant '  + str(e)

    return {},'Success'




#####################################################
# Public Functions for Contiv Demo V1
#####################################################

def createUserContivV1(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC User ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        createUser(moDir, userName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant' , format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC User '

    return {},'Success'


def createSecDomainContivV1(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Security Domain ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        createSecDo(moDir, secDoName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC Security Domain  ' , format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant'

    return {},'Success'


def createTenantContivV1(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Tenant  ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        createTenantContiv(moDir, tenantName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant'

    return {},'Success'


def deleteUserContivV1(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Delete  APIC User ' , format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteUser(moDir, userName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while deleting APIC User ' , format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant'

    return {},'Success'


def deleteSecDomainContivV1(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Delete APIC Security Domain  ', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteSecDo(moDir, secDoName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while deleting  APIC Security Domain ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant'

    return {},'Success'


def deleteTenantContivV1(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Delete  APIC Tenant '  , format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteTenant(moDir, tenantName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while deleting  APIC tenant  ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant'

    return {},'Success'

############################################ END CONTIV V1 ###########################################



def createAsapdcConfig(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Create APIC Tenant', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        createSecDoAsapdc(moDir, secDoName=tenantName)
        createUserAsapdc(moDir, userName=tenantName)
        createTenantAsapdc(moDir, tenantName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while creating APIC tenant', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while creating APIC Tenant'

    return {},'Success'




def removeAsapdcConfig(sessionDetails, target, otherInputs=[], logFormat=''):
    logger.autolog(level=1,message='About to Remove ASAPD  APIC Config', format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)  
        return {},'Failed: No arguments provided'

    tenantName=gen.sanitizeString(tenantName)

    try:
        creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        moDir = login(creds['prot']+'://'+creds['host'],creds['username'],creds['password'])
        deleteSecDo(moDir, secDoName=tenantName)
        deleteUser(moDir, userName=tenantName)
        deleteTenant(moDir, tenantName=tenantName)

    except Exception as e:
        logger.autolog(level=1,message='Error while removing ASAPDC APIC configuration', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return {},'Failed: Error while removing ASAPDC  APIC Config '

    return {},'Success'


def deleteTenant(md, tenantName='tenantName'):
    # the top level object on which operations will be made
    topMo = cobra.model.pol.Uni('')
    # build the request using cobra syntax
    fvTenant = cobra.model.fv.Tenant(topMo,tenantName)
    fvTenant.delete()
    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)

def deleteSecDo(md, secDoName='secDoName'):
    # the top level object on which operations will be made
    topMo = cobra.model.pol.Uni('')
    # build the request using cobra syntax
    aaaUserEp = cobra.model.aaa.UserEp(topMo)
    aaaDomain = cobra.model.aaa.Domain(aaaUserEp,'SecDo-'+str(secDoName))
    aaaDomain.delete()
    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(aaaUserEp)
    md.commit(c)

def deleteUser(md, userName='userName'):
    # the top level object on which operations will be made
    topMo = cobra.model.pol.Uni('')
    # build the request using cobra syntax
    aaaUserEp = cobra.model.aaa.UserEp(topMo)
    aaaUser = cobra.model.aaa.User(aaaUserEp,userName)
    aaaUser.delete()
    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(aaaUser)
    md.commit(c)



def login(apicUrl,apicUsername,apicPassword):
    try:
        session = LoginSession(apicUrl,apicUsername,apicPassword)
        moDir = MoDirectory(session)
        moDir.login()
        logger.autolog(level=1,message='Logged to APIC ok')
        return moDir
    except Exception as e:
        logger.autolog(level=1,message='Error while connecting to APIC.')
        logger.autolog(level=1,message=str(e))
        return False

def getUniInfra(moDir):
    # Get the top level Policy Universe Directory
    uniMo = moDir.lookupByDn('uni')
    uniInfraMo = moDir.lookupByDn('uni/infra')
    return uniInfraMo

def getUni(moDir):
    # Get the top level Policy Universe Directory
    uniMo = moDir.lookupByDn('uni')
    return uniMo


def createVmmDomain(moDir,VMM_DOMAIN_INFO):
#VMM_DOMAIN_INFO = {'name': "mininet",
#                   'ctrlrs': [{'name': 'vcenter1', 'ip': '192.0.20.3', 
#                               'scope': 'vm'}],
#                   'usrs': [{'name': 'admin', 'usr': 'administrator',
#                             'pwd': 'pa$$word1'}],
#                   'namespace': {'name': 'VlanRange', 'from': 'vlan-100',
#                                 'to': 'vlan-200'}
#                  }
    vmmpVMwareProvPMo = moDir.lookupByDn('uni/vmmp-VMware')
    vmmDomPMo = DomP(vmmpVMwareProvPMo, VMM_DOMAIN_INFO['name'])
    
    vmmUsrMo = []
    for usrp in VMM_DOMAIN_INFO['usrs']:
        usrMo = UsrAccP(vmmDomPMo, usrp['name'], usr=usrp['usr'],pwd=usrp['pwd'])
        vmmUsrMo.append(usrMo)

    # Create Controllers under domain
    for ctrlr in VMM_DOMAIN_INFO['ctrlrs']:
        vmmCtrlrMo = CtrlrP(vmmDomPMo, ctrlr['name'], scope=ctrlr['scope'],
                            hostOrIp=ctrlr['ip'])
        # Associate Ctrlr to UserP
        RsAcc(vmmCtrlrMo, tDn=vmmUsrMo[0].dn)
    
    # Associate Domain to Namespace
    RsVlanNs(vmmDomPMo, tDn=fvnsVlanInstPMo.dn)
   
    vmmCfg = ConfigRequest()
    vmmCfg.addMo(vmmDomPMo)
    moDir.commit(vmmCfg)




def createTenantApicMicroseg(md, tenantName='tenantName'):
    # the top level object on which operations will be made
    topMo = cobra.model.pol.Uni('')
    # build the request using cobra syntax
    fvTenant = cobra.model.fv.Tenant(topMo, ownerKey=u'', name=tenantName, descr=u'', ownerTag=u'')
    aaaDomainRef = cobra.model.aaa.DomainRef(fvTenant, ownerKey=u'', name=u'SecDo-'+tenantName, descr=u'', ownerTag=u'')





    vzBrCP = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'web_db', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj = cobra.model.vz.Subj(vzBrCP, revFltPorts=u'yes', name=u'Subject', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt = cobra.model.vz.RsSubjFiltAtt(vzSubj, directives=u'', tnVzFilterName=u'icmp')
    vzRsSubjFiltAtt2 = cobra.model.vz.RsSubjFiltAtt(vzSubj, directives=u'', tnVzFilterName=u'port_3306')
    vzBrCP2 = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'lb_web', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj2 = cobra.model.vz.Subj(vzBrCP2, revFltPorts=u'yes', name=u'Subject', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt3 = cobra.model.vz.RsSubjFiltAtt(vzSubj2, directives=u'', tnVzFilterName=u'icmp')
    vzRsSubjFiltAtt4 = cobra.model.vz.RsSubjFiltAtt(vzSubj2, directives=u'', tnVzFilterName=u'port_80')

    vzFilter = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'icmp', descr=u'', nameAlias=u'')
    vzEntry = cobra.model.vz.Entry(vzFilter, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'unspecified', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'icmp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'unspecified', name=u'icmp')
    vzFilter2 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'port_3306', descr=u'', nameAlias=u'')
    vzEntry2 = cobra.model.vz.Entry(vzFilter2, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'3306', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'tcp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'3306', name=u'port_3306')
    vzFilter3 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'port_80', descr=u'', nameAlias=u'')
    vzEntry3 = cobra.model.vz.Entry(vzFilter3, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'http', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'tcp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'http', name=u'port_80')



    #vzBrCP = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'lb_web', prio=u'unspecified', targetDscp=u'unspecified', ownerTag=u'', descr=u'')
    #vzSubj = cobra.model.vz.Subj(vzBrCP, revFltPorts=u'yes', name=u'Subject', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    #vzRsSubjFiltAtt = cobra.model.vz.RsSubjFiltAtt(vzSubj, directives=u'', tnVzFilterName=u'default')

    #vzBrCP2 = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'web_db', prio=u'unspecified', targetDscp=u'unspecified', ownerTag=u'', descr=u'')
    #vzSubj2 = cobra.model.vz.Subj(vzBrCP2, revFltPorts=u'yes', name=u'Subject', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    #vzRsSubjFiltAtt2 = cobra.model.vz.RsSubjFiltAtt(vzSubj2, directives=u'', tnVzFilterName=u'icmp')
    #vzRsSubjFiltAtt3 = cobra.model.vz.RsSubjFiltAtt(vzSubj2, directives=u'', tnVzFilterName=u'port_3306')


    drawCont = cobra.model.draw.Cont(fvTenant)
    drawInst = cobra.model.draw.Inst(drawCont, info=u"{'{fvAp/epg}-{db}':{'x':65,'y':329.5},'{fvAp/epg}-{web}':{'x':185,'y':329.5},'{fvAp/epg}-{Loadbalancer}':{'x':305,'y':329.5},'{fvAp/contract}-{web_db}':{'x':120,'y':29.50000000000003},'{fvAp/contract}-{lb_web}':{'x':240,'y':29.50000000000003}}", oDn=u'uni/tn-'+tenantName+u'/ap-Opencart')

    fvBD = cobra.model.fv.BD(fvTenant, ownerKey=u'', vmac=u'not-applicable', unkMcastAct=u'flood', name=u'opencart_bd', descr=u'', unkMacUcastAct=u'proxy', arpFlood=u'no', limitIpLearnToSubnets=u'no', llAddr=u'::', mcastAllow=u'no', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', unicastRoute=u'yes', ownerTag=u'', multiDstPktAct=u'bd-flood', type=u'regular', ipLearning=u'yes')
    fvRsBDToNdP = cobra.model.fv.RsBDToNdP(fvBD, tnNdIfPolName=u'')
    fvRsCtx = cobra.model.fv.RsCtx(fvBD, tnFvCtxName=u'opencart_vrf')
    fvRsIgmpsn = cobra.model.fv.RsIgmpsn(fvBD, tnIgmpSnoopPolName=u'')
    fvSubnet = cobra.model.fv.Subnet(fvBD, name=u'', descr=u'', ctrl=u'', ip=u'10.105.0.1/24', preferred=u'no', virtual=u'no')
    fvRsBdToEpRet = cobra.model.fv.RsBdToEpRet(fvBD, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvCtx = cobra.model.fv.Ctx(fvTenant, ownerKey=u'', name=u'opencart_vrf', descr=u'', knwMcastAct=u'permit', pcEnfDir=u'ingress', ownerTag=u'', pcEnfPref=u'enforced')
    fvRsBgpCtxPol = cobra.model.fv.RsBgpCtxPol(fvCtx, tnBgpCtxPolName=u'')
    fvRsCtxToExtRouteTagPol = cobra.model.fv.RsCtxToExtRouteTagPol(fvCtx, tnL3extRouteTagPolName=u'')
    fvRsOspfCtxPol = cobra.model.fv.RsOspfCtxPol(fvCtx, tnOspfCtxPolName=u'')
    vzAny = cobra.model.vz.Any(fvCtx, matchT=u'AtleastOne', name=u'', descr=u'')
    fvRsCtxToEpRet = cobra.model.fv.RsCtxToEpRet(fvCtx, tnFvEpRetPolName=u'')
    vnsSvcCont = cobra.model.vns.SvcCont(fvTenant)

    fvAp = cobra.model.fv.Ap(fvTenant, ownerKey=u'', prio=u'unspecified', name=u'Opencart', descr=u'', ownerTag=u'')

    fvAEPg = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Loadbalancer', prio=u'unspecified', descr=u'', pcEnfPref=u'unenforced')
    fvRsCons = cobra.model.fv.RsCons(fvAEPg, tnVzBrCPName=u'lb_web', prio=u'unspecified')
    #####fvRsDomAtt = cobra.model.fv.RsDomAtt(fvAEPg, tDn=u'uni/vmmp-Microsoft/dom-ms-hybridcloud', primaryEncap=u'unknown', classPref=u'encap', delimiter=u'', instrImedcy=u'lazy', encap=u'unknown', resImedcy=u'immediate')
    #####vmmSecP = cobra.model.vmm.SecP(fvRsDomAtt, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'reject', ownerTag=u'', allowPromiscuous=u'reject', macChanges=u'reject')
    fvRsDomAtt2 = cobra.model.fv.RsDomAtt(fvAEPg, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', primaryEncap=u'unknown', classPref=u'encap', delimiter=u'', instrImedcy=u'lazy', encap=u'unknown', resImedcy=u'immediate')
    fvRsBd = cobra.model.fv.RsBd(fvAEPg, tnFvBDName=u'useg-inbound')
    fvRsCustQosPol = cobra.model.fv.RsCustQosPol(fvAEPg, tnQosCustomPolName=u'')
    fvRsProv0 = cobra.model.fv.RsProv(fvAEPg, tnVzBrCPName='l3out_lb_'+tenantName, matchT=u'AtleastOne', prio=u'unspecified')


    fvAEPg2 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'db', prio=u'unspecified', descr=u'', pcEnfPref=u'unenforced')
    #####fvRsDomAtt3 = cobra.model.fv.RsDomAtt(fvAEPg2, tDn=u'uni/vmmp-Microsoft/dom-ms-hybridcloud', primaryEncap=u'unknown', classPref=u'encap', delimiter=u'', instrImedcy=u'lazy', encap=u'unknown', resImedcy=u'immediate')
    #####vmmSecP2 = cobra.model.vmm.SecP(fvRsDomAtt3, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'reject', ownerTag=u'', allowPromiscuous=u'reject', macChanges=u'reject')
    fvRsDomAtt4 = cobra.model.fv.RsDomAtt(fvAEPg2, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', primaryEncap=u'unknown', classPref=u'useg', delimiter=u'', instrImedcy=u'lazy', encap=u'unknown', resImedcy=u'immediate')
    fvRsBd2 = cobra.model.fv.RsBd(fvAEPg2, tnFvBDName=u'opencart_bd')
    fvRsCustQosPol2 = cobra.model.fv.RsCustQosPol(fvAEPg2, tnQosCustomPolName=u'')
    fvRsProv = cobra.model.fv.RsProv(fvAEPg2, tnVzBrCPName=u'web_db', matchT=u'AtleastOne', prio=u'unspecified')

    fvAEPg3 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'web', prio=u'unspecified', descr=u'', pcEnfPref=u'unenforced')
    fvRsCons2 = cobra.model.fv.RsCons(fvAEPg3, tnVzBrCPName=u'web_db', prio=u'unspecified')
    fvRsDomAtt5 = cobra.model.fv.RsDomAtt(fvAEPg3, tDn=u'uni/vmmp-Microsoft/dom-ms-hybridcloud', primaryEncap=u'unknown', classPref=u'encap', delimiter=u'', instrImedcy=u'lazy', encap=u'unknown', resImedcy=u'immediate')
    vmmSecP3 = cobra.model.vmm.SecP(fvRsDomAtt5, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'reject', ownerTag=u'', allowPromiscuous=u'reject', macChanges=u'reject')
    fvRsDomAtt6 = cobra.model.fv.RsDomAtt(fvAEPg3, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', primaryEncap=u'unknown', classPref=u'useg', delimiter=u'', instrImedcy=u'lazy', encap=u'unknown', resImedcy=u'immediate')
    fvRsBd3 = cobra.model.fv.RsBd(fvAEPg3, tnFvBDName=u'opencart_bd')
    fvRsCustQosPol3 = cobra.model.fv.RsCustQosPol(fvAEPg3, tnQosCustomPolName=u'')
    fvRsProv2 = cobra.model.fv.RsProv(fvAEPg3, tnVzBrCPName=u'lb_web', matchT=u'AtleastOne', prio=u'unspecified')

    fvRsTenantMonPol = cobra.model.fv.RsTenantMonPol(fvTenant, tnMonEPGPolName=u'')

    #vzFilter = cobra.model.vz.Filter(fvTenant, ownerKey=u'', name=u'lb_web_Filter', descr=u'', ownerTag=u'')
    #vzFilter2 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', name=u'port_80', descr=u'', ownerTag=u'')
    #vzFilter3 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', name=u'icmp', descr=u'', ownerTag=u'')
    #vzEntry = cobra.model.vz.Entry(vzFilter3, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'unspecified', descr=u'', matchDscp=u'unspecified', prot=u'icmp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'unspecified', name=u'icmp')
    #vzFilter4 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', name=u'allow_all_Filter', descr=u'', ownerTag=u'')
    #vzFilter5 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', name=u'port_3306', descr=u'', ownerTag=u'')
    #vzEntry2 = cobra.model.vz.Entry(vzFilter5, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'3306', descr=u'', matchDscp=u'unspecified', prot=u'tcp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'3306', name=u'web_db')

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)



def sdcFtdv2Device(md, tenantName, csrIp, ftdDeviceName='ftdv-ngips'):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, ''+str(tenantName)+r'')

    vnsLDevVip = cobra.model.vns.LDevVip(fvTenant, isCopy=u'no', managed=u'yes', name=ftdDeviceName, svcType=u'FW', funcType=u'GoThrough', promMode=u'no', devtype=u'VIRTUAL', packageModel=u'VIRTUAL', contextAware=u'single-Context', nameAlias=u'', trunking=u'no', mode=u'legacy-Mode')
    vnsLIf = cobra.model.vns.LIf(vnsLDevVip, name=u'db', encap=u'unknown', nameAlias=u'')
    vnsRsMetaIf = cobra.model.vns.RsMetaIf(vnsLIf, isConAndProv=u'no', tDn=u'uni/infra/mDev-CISCO-FTD_FI-1.0/mIfLbl-internal')
    vnsRsCIfAttN = cobra.model.vns.RsCIfAttN(vnsLIf, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-'+str(ftdDeviceName)+r'/cDev-Device1/cIf-[GigabitEthernet0/1]')
    vnsLIf2 = cobra.model.vns.LIf(vnsLDevVip, name=u'web', encap=u'unknown', nameAlias=u'')
    vnsRsMetaIf2 = cobra.model.vns.RsMetaIf(vnsLIf2, isConAndProv=u'no', tDn=u'uni/infra/mDev-CISCO-FTD_FI-1.0/mIfLbl-external')
    vnsRsCIfAttN2 = cobra.model.vns.RsCIfAttN(vnsLIf2, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-'+str(ftdDeviceName)+r'/cDev-Device1/cIf-[GigabitEthernet0/0]')
    vnsRsALDevToDomP = cobra.model.vns.RsALDevToDomP(vnsLDevVip, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud')
    vnsCMgmt = cobra.model.vns.CMgmt(vnsLDevVip, host=csrIp, name=u'', nameAlias=u'', port=u'10443')
    vnsRsALDevToDevMgr = cobra.model.vns.RsALDevToDevMgr(vnsLDevVip, tnVnsDevMgrName=u'fmc')
    vnsCDev = cobra.model.vns.CDev(vnsLDevVip, vcenterName=u'vc60-aci-1', vmName=u''+str(tenantName)+r'-'+str(ftdDeviceName)+r'', name=u'Device1', nameAlias=u'', devCtxLbl=u'')
    vnsCMgmt2 = cobra.model.vns.CMgmt(vnsCDev, host=u'10.150.0.23', name=u'', nameAlias=u'', port=u'443')
    vnsCIf = cobra.model.vns.CIf(vnsCDev, name=u'GigabitEthernet0/1', nameAlias=u'', vnicName=u'Network adapter 3')
    vnsCIf2 = cobra.model.vns.CIf(vnsCDev, name=u'GigabitEthernet0/0', nameAlias=u'', vnicName=u'Network adapter 2')
    vnsCCredSecret = cobra.model.vns.CCredSecret(vnsCDev, name=u'password', nameAlias=u'' , value='C1sco12345')
    vnsCCred = cobra.model.vns.CCred(vnsCDev, value=u'admin', name=u'username', nameAlias=u'')
    vnsCCredSecret2 = cobra.model.vns.CCredSecret(vnsLDevVip, name=u'password', nameAlias=u'', value='C1sco12345')
    vnsRsMDevAtt = cobra.model.vns.RsMDevAtt(vnsLDevVip, tDn=u'uni/infra/mDev-CISCO-FTD_FI-1.0')
    vnsCCred2 = cobra.model.vns.CCred(vnsLDevVip, value=u'admin', name=u'username', nameAlias=u'')

    # commit the generated code to APIC
    #print toXMLStr(fvTenant)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)


def sdcFtdv1Device(md, tenant_name,csr_ip):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, '%s' % tenant_name)

    # build the request using cobra syntax
    vnsLDevVip = cobra.model.vns.LDevVip(fvTenant, isCopy=u'no', managed=u'yes', name=u'FTDv', svcType=u'FW', funcType=u'GoTo', devtype=u'VIRTUAL', packageModel=u'VIRTUAL', contextAware=u'single-Context', nameAlias=u'', trunking=u'no', mode=u'legacy-Mode')


    #Interface attacker
    vnsLIf = cobra.model.vns.LIf(vnsLDevVip, name=u'attacker', encap=u'unknown', nameAlias=u'')
    vnsRsMetaIf = cobra.model.vns.RsMetaIf(vnsLIf, isConAndProv=u'no', tDn=u'uni/infra/mDev-CISCO-FTD_FI-1.0/mIfLbl-external')
    vnsRsCIfAttN2 = cobra.model.vns.RsCIfAttN(vnsLIf, tDn=u'uni/tn-%s/lDevVip-FTDv/cDev-Device1/cIf-[GigabitEthernet0/2]' % tenant_name)


    #Interface webout
    vnsLIf2 = cobra.model.vns.LIf(vnsLDevVip, name=u'webout', encap=u'unknown', nameAlias=u'')
    vnsRsMetaIf2 = cobra.model.vns.RsMetaIf(vnsLIf2, isConAndProv=u'no', tDn=u'uni/infra/mDev-CISCO-FTD_FI-1.0/mIfLbl-internal')
    vnsRsCIfAttN4 = cobra.model.vns.RsCIfAttN(vnsLIf2, tDn=u'uni/tn-%s/lDevVip-FTDv/cDev-Device1/cIf-[GigabitEthernet0/1]' % tenant_name)

    #VMM Domain
    vnsRsALDevToDomP = cobra.model.vns.RsALDevToDomP(vnsLDevVip, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud')

    #Device Manager
    vnsCMgmt = cobra.model.vns.CMgmt(vnsLDevVip, host=u'%s' % csr_ip, name=u'ftd', nameAlias=u'', port=u'10443')
    vnsRsALDevToDevMgr = cobra.model.vns.RsALDevToDevMgr(vnsLDevVip, tnVnsDevMgrName=u'fmc')
    vnsCDev = cobra.model.vns.CDev(vnsLDevVip, vcenterName=u'vc60-aci-1', vmName='%s-ftdv' % (tenant_name), name=u'Device1', nameAlias=u'', devCtxLbl=u'' )
    vnsRsCDevToCtrlrP = cobra.model.vns.RsCDevToCtrlrP(vnsCDev, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud/ctrlr-vc60-aci-1')

    #Device 
    vnsCMgmt2 = cobra.model.vns.CMgmt(vnsCDev, host=u'10.150.0.22', name=u'', nameAlias=u'', port=u'443')
    vnsCIf = cobra.model.vns.CIf(vnsCDev, name=u'GigabitEthernet0/1', nameAlias=u'', vnicName=u'Network adapter 3')
    vnsCIf2 = cobra.model.vns.CIf(vnsCDev, name=u'GigabitEthernet0/2', nameAlias=u'', vnicName=u'Network adapter 4')

    #Credentials Device
    vnsCCredSecret = cobra.model.vns.CCredSecret(vnsCDev, name=u'password', nameAlias=u'',value='C1sco12345')
    vnsCCred = cobra.model.vns.CCred(vnsCDev, value=u'admin', name=u'username', nameAlias=u'')

    #Credentials Cluster
    vnsCCredSecret3 = cobra.model.vns.CCredSecret(vnsLDevVip, name=u'password', nameAlias=u'',value='C1sco12345')
    vnsRsMDevAtt = cobra.model.vns.RsMDevAtt(vnsLDevVip, tDn=u'uni/infra/mDev-CISCO-FTD_FI-1.0')
    vnsCCred3 = cobra.model.vns.CCred(vnsLDevVip, value=u'admin', name=u'username', nameAlias=u'')

    # commit the generated code to APIC
    #print toXMLStr(fvTenant)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)


def sdcFmcDeviceManager(md,tenant_name,csr_ip):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, '%s' % tenant_name)

    # build the request using cobra syntax
    vnsDevMgr = cobra.model.vns.DevMgr(fvTenant, ownerKey=u'', name=u'fmc', descr=u'', ownerTag=u'')
    vnsCCred = cobra.model.vns.CCred(vnsDevMgr, name=u'username', value=u'aciadmin')
    vnsCCredSecret = cobra.model.vns.CCredSecret(vnsDevMgr, name=u'password', value=u'cisco')
    vnsCMgmts = cobra.model.vns.CMgmts(vnsDevMgr, host=u'%s' % csr_ip, name=u'', port=u'10443')
    vnsRsDevMgrToMDevMgr = cobra.model.vns.RsDevMgrToMDevMgr(vnsDevMgr, tDn=u'uni/infra/mDevMgr-CISCO-FTDmgr_FI-1.0')

    # commit the generated code to APIC
    #print toXMLStr(fvTenant)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)
    



def sdcFtdv1FunctionProfile(md,tenant_name):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, '%s' % tenant_name)
    vnsAbsFuncProfContr = cobra.model.vns.AbsFuncProfContr(fvTenant)

    # build the request using cobra syntax
    vnsAbsFuncProfGrp = cobra.model.vns.AbsFuncProfGrp(vnsAbsFuncProfContr, ownerKey=u'', name=u'FTDv', descr=u'', ownerTag=u'')
    vnsAbsFuncProf = cobra.model.vns.AbsFuncProf(vnsAbsFuncProfGrp, ownerKey=u'', name=u'l3fw-cfg-ftd1', descr=u'', ownerTag=u'')
    vnsRsProfToMFunc = cobra.model.vns.RsProfToMFunc(vnsAbsFuncProf, tDn=u'uni/infra/mDev-CISCO-FTD_FI-1.0/mFunc-FTD')

    #Device Config
    vnsAbsDevCfg = cobra.model.vns.AbsDevCfg(vnsAbsFuncProf, ownerKey=u'', name=u'devConfig', descr=u'', ownerTag=u'')

    #Device Config --- Internal Interface 
    vnsAbsFolder = cobra.model.vns.AbsFolder(vnsAbsDevCfg, locked=u'no', name=u'internalInterface', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'InterfaceConfig', cardinality=u'unspecified')

    #Device Config --- Internal Interface --- ENABLED
    vnsAbsParam = cobra.model.vns.AbsParam(vnsAbsFolder, validation=u'', mandatory=u'no', name=u'enabled', value=u'true', key=u'enabled', locked=u'no', cardinality=u'unspecified')

    #Device Config --- Internal Interface --- Interface Name --- webout
    vnsAbsParam2 = cobra.model.vns.AbsParam(vnsAbsFolder, validation=u'', mandatory=u'no', name=u'ifname', value=u'webout', key=u'ifname', locked=u'no', cardinality=u'unspecified')

    #Device Config --- Internal Interface --- IPV4
    vnsAbsFolder2 = cobra.model.vns.AbsFolder(vnsAbsFolder, locked=u'no', name=u'IPv4Config', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'IPv4Config', cardinality=u'unspecified')

    #Device Config --- Internal Interface --- IPV4 --- Static 
    vnsAbsFolder3 = cobra.model.vns.AbsFolder(vnsAbsFolder2, locked=u'no', name=u'static', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'static', cardinality=u'unspecified')

    #Device Config --- Internal Interface --- IPV4 --- Static --- 10.150.2.1 
    vnsAbsParam3 = cobra.model.vns.AbsParam(vnsAbsFolder3, validation=u'', mandatory=u'no', name=u'address', value=u'10.150.2.1/24', key=u'address', locked=u'no', cardinality=u'unspecified')

    #Device Config --- Internal Interface --- Internal Security Zone
    vnsAbsFolder4 = cobra.model.vns.AbsFolder(vnsAbsFolder, locked=u'no', name=u'int_security_zone', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'int_security_zone', cardinality=u'unspecified')

    #Device Config --- Internal Interface --- Internal Security Zone --- Security Zone  WEBOUT-ZONE
    vnsAbsCfgRel = cobra.model.vns.AbsCfgRel(vnsAbsFolder4, mandatory=u'no', name=u'security_zone', key=u'security_zone', locked=u'no', cardinality=u'unspecified', targetName=u'webout-zone')

    #Device Config --- Security Zone --- Attacker
    vnsAbsFolder5 = cobra.model.vns.AbsFolder(vnsAbsDevCfg, locked=u'no', name=u'attacker-zone', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'SecurityZone', cardinality=u'unspecified')

    #Device Config --- Security Zone --- Attacker --- Type ROUTED
    vnsAbsParam4 = cobra.model.vns.AbsParam(vnsAbsFolder5, validation=u'', mandatory=u'no', name=u'type', value=u'ROUTED', key=u'type', locked=u'no', cardinality=u'unspecified')

    #Device Config --- FTD-POLICY
    vnsAbsFolder6 = cobra.model.vns.AbsFolder(vnsAbsDevCfg, locked=u'no', name=u'ftd-policy', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'yes', key=u'AccessPolicy', cardinality=u'unspecified')

    #Device Config --- FTD-POLICY --- FTD-RULE2
    vnsAbsFolder7 = cobra.model.vns.AbsFolder(vnsAbsFolder6, locked=u'no', name=u'ftd-rule2', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'AccessRule', cardinality=u'unspecified')
    #Device Config --- FTD-POLICY --- FTD-RULE2 --- BIDIRECTIONAL
    vnsAbsParam5 = cobra.model.vns.AbsParam(vnsAbsFolder7, validation=u'', mandatory=u'no', name=u'bidirectional', value=u'true', key=u'bidirectional', locked=u'no', cardinality=u'unspecified')

    #Device Config --- FTD-POLICY --- FTD-RULE2 --- AccDestionationZones
    vnsAbsFolder8 = cobra.model.vns.AbsFolder(vnsAbsFolder7, locked=u'no', name=u'AccDestinationZones', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'AccDestinationZones', cardinality=u'unspecified')

    #Device Config --- FTD-POLICY --- FTD-RULE2 --- Destination Zone --- internalInterface
    vnsAbsCfgRel2 = cobra.model.vns.AbsCfgRel(vnsAbsFolder8, mandatory=u'no', name=u'DestinationZones', key=u'DestinationZones', locked=u'no', cardinality=u'unspecified', targetName=u'internalInterface/int_security_zone')

    #Device Config --- FTD-POLICY --- FTD-RULE2 --- AccSourceZones
    vnsAbsFolder9 = cobra.model.vns.AbsFolder(vnsAbsFolder7, locked=u'no', name=u'AccSourceZones', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'AccSourceZones', cardinality=u'unspecified')
    #Device Config --- FTD-POLICY --- FTD-RULE2 --- AccSourceZones --- Source Zones --- externalInterface
    vnsAbsCfgRel3 = cobra.model.vns.AbsCfgRel(vnsAbsFolder9, mandatory=u'no', name=u'SourceZones', key=u'SourceZones', locked=u'no', cardinality=u'unspecified', targetName=u'externalInterface/int_security_zone')

    #Device Config --- FTD-POLICY --- FTD-RULE1
    vnsAbsFolder10 = cobra.model.vns.AbsFolder(vnsAbsFolder6, locked=u'no', name=u'ftd-rule1', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'AccessRule', cardinality=u'unspecified')

    #Device Config --- FTD-POLICY --- FTD-RULE1 --- BIDIRECTIONAL
    vnsAbsParam6 = cobra.model.vns.AbsParam(vnsAbsFolder10, validation=u'', mandatory=u'no', name=u'Bi-Directional', value=u'true', key=u'bidirectional', locked=u'no', cardinality=u'unspecified')

    #Device Config --- FTD-POLICY --- FTD-RULE1 --- AccDestionationZones
    vnsAbsFolder11 = cobra.model.vns.AbsFolder(vnsAbsFolder10, locked=u'no', name=u'AccDestinationZones', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'AccDestinationZones', cardinality=u'unspecified')

    #Device Config --- FTD-POLICY --- FTD-RULE1 --- Destination Zone --- internalInterface
    vnsAbsCfgRel4 = cobra.model.vns.AbsCfgRel(vnsAbsFolder11, mandatory=u'no', name=u'DestinationZones', key=u'DestinationZones', locked=u'no', cardinality=u'unspecified', targetName=u'internalInterface/int_security_zone')

    #Device Config --- FTD-POLICY --- FTD-RULE1 --- AccSourceZones
    vnsAbsFolder12 = cobra.model.vns.AbsFolder(vnsAbsFolder10, locked=u'no', name=u'AccSourceZones', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'AccSourceZones', cardinality=u'unspecified')

    #Device Config --- FTD-POLICY --- FTD-RULE1 --- AccSourceZones --- Source Zones --- externalInterface
    vnsAbsCfgRel5 = cobra.model.vns.AbsCfgRel(vnsAbsFolder12, mandatory=u'no', name=u'SourceZone', key=u'SourceZones', locked=u'no', cardinality=u'unspecified', targetName=u'externalInterface/int_security_zone')

    #Device Config --- External Interface
    vnsAbsFolder13 = cobra.model.vns.AbsFolder(vnsAbsDevCfg, locked=u'no', name=u'externalInterface', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'InterfaceConfig', cardinality=u'unspecified')

    #Device Config --- External Interface --- ENABLED
    vnsAbsParam7 = cobra.model.vns.AbsParam(vnsAbsFolder13, validation=u'', mandatory=u'no', name=u'enabled', value=u'true', key=u'enabled', locked=u'no', cardinality=u'unspecified')

    #Device Config --- External Interface --- NAME ATTACKER
    vnsAbsParam8 = cobra.model.vns.AbsParam(vnsAbsFolder13, validation=u'', mandatory=u'no', name=u'ifname', value=u'attacker', key=u'ifname', locked=u'no', cardinality=u'unspecified')

    #Device Config --- External Interface --- IPV4CONFIG
    vnsAbsFolder14 = cobra.model.vns.AbsFolder(vnsAbsFolder13, locked=u'no', name=u'IPv4Config', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'IPv4Config', cardinality=u'unspecified')

    #Device Config --- External Interface --- IPV4CONFIG --- STATIC
    vnsAbsFolder15 = cobra.model.vns.AbsFolder(vnsAbsFolder14, locked=u'no', name=u'static', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'static', cardinality=u'unspecified')

    #Device Config --- External Interface --- IPV4CONFIG --- STATIC --- 10.150.0.1
    vnsAbsParam9 = cobra.model.vns.AbsParam(vnsAbsFolder15, validation=u'', mandatory=u'no', name=u'address', value=u'10.150.1.1/24', key=u'address', locked=u'no', cardinality=u'unspecified')

    #Device Config --- External Interface --- int_security_zone 
    vnsAbsFolder16 = cobra.model.vns.AbsFolder(vnsAbsFolder13, locked=u'no', name=u'int_security_zone', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'int_security_zone', cardinality=u'unspecified')

    #Device Config --- External Interface --- int_security_zone --- ATTACKER-ZONE 
    vnsAbsCfgRel6 = cobra.model.vns.AbsCfgRel(vnsAbsFolder16, mandatory=u'no', name=u'security_zone', key=u'security_zone', locked=u'no', cardinality=u'unspecified', targetName=u'attacker-zone')


    #Device Config --- Security Zone --- WEBOUT-ZONE
    vnsAbsFolder17 = cobra.model.vns.AbsFolder(vnsAbsDevCfg, locked=u'no', name=u'webout-zone', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'SecurityZone', cardinality=u'unspecified')
    #Device Config --- Security Zone --- type ROUTED
    vnsAbsParam10 = cobra.model.vns.AbsParam(vnsAbsFolder17, validation=u'', mandatory=u'no', name=u'type', value=u'ROUTED', key=u'type', locked=u'no', cardinality=u'unspecified')

    #Function Profile Configuration 
    vnsAbsFuncCfg = cobra.model.vns.AbsFuncCfg(vnsAbsFuncProf, ownerKey=u'', name=u'funcConfig', descr=u'', ownerTag=u'')

    #Function Profile Configuration --- IntConfig
    vnsAbsFolder18 = cobra.model.vns.AbsFolder(vnsAbsFuncCfg, locked=u'no', name=u'IntConfig', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'InIntfConfigRelFolder', cardinality=u'unspecified')

    #Function Profile Configuration --- IntConfig  --- internalInterface
    vnsAbsCfgRel7 = cobra.model.vns.AbsCfgRel(vnsAbsFolder18, mandatory=u'no', name=u'InConfigrel', key=u'InIntfConfigRel', locked=u'no', cardinality=u'unspecified', targetName=u'internalInterface')

    #Function Profile Configuration --- Access Policy Folder
    vnsAbsFolder19 = cobra.model.vns.AbsFolder(vnsAbsFuncCfg, locked=u'no', name=u'AccessPolicyFolder', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'yes', key=u'AccessPolicyFolder', cardinality=u'unspecified')

    #Function Profile Configuration --- Access Policy Folder --- InAccessPolicyRel : ftd-policy
    vnsAbsCfgRel8 = cobra.model.vns.AbsCfgRel(vnsAbsFolder19, mandatory=u'no', name=u'InAccessPolicyRel', key=u'InAccessPolicyRel', locked=u'no', cardinality=u'unspecified', targetName=u'ftd-policy')

    #Function Profile Configuration --- ExtConfig
    vnsAbsFolder20 = cobra.model.vns.AbsFolder(vnsAbsFuncCfg, locked=u'no', name=u'ExtConfig', devCtxLbl=u'', scopedBy=u'epg', profileBehaviorShared=u'no', key=u'ExIntfConfigRelFolder', cardinality=u'unspecified')

    #Function Profile Configuration --- ExtConfig --- externalInterface
    vnsAbsCfgRel9 = cobra.model.vns.AbsCfgRel(vnsAbsFolder20, mandatory=u'no', name=u'ExtConfigrel', key=u'ExIntfConfigRel', locked=u'no', cardinality=u'unspecified', targetName=u'externalInterface')

    # commit the generated code to APIC
    #print toXMLStr(vnsAbsFuncProfContr)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(vnsAbsFuncProfContr)
    md.commit(c)



def sdcFtdv2FunctionProfile(md, tenantName, funcProfName='l2fw-cfg-ftdv-ngips', funcProfGroupName='FTDv'):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)
    vnsAbsFuncProfContr = cobra.model.vns.AbsFuncProfContr(fvTenant)

    # build the request using cobra syntax
    vnsAbsFuncProfGrp = cobra.model.vns.AbsFuncProfGrp(vnsAbsFuncProfContr, ownerKey=u'', name=funcProfGroupName, descr=u'', nameAlias=u'', ownerTag=u'')

    vnsAbsFuncProf = cobra.model.vns.AbsFuncProf(vnsAbsFuncProfGrp, ownerKey=u'', name=funcProfName, descr=u'', srcMode=u'', nameAlias=u'', ownerTag=u'')

    vnsRsProfToMFunc = cobra.model.vns.RsProfToMFunc(vnsAbsFuncProf, tDn=u'uni/infra/mDev-CISCO-FTD_FI-1.0/mFunc-FTD')
    vnsAbsFuncCfg = cobra.model.vns.AbsFuncCfg(vnsAbsFuncProf, ownerKey=u'', ownerTag=u'', name=u'funcConfig', descr=u'', nameAlias=u'')
    vnsAbsFolder = cobra.model.vns.AbsFolder(vnsAbsFuncCfg, srcRef=u'', locked=u'no', name=u'IntConfig', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'InIntfConfigRelFolder', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsCfgRel = cobra.model.vns.AbsCfgRel(vnsAbsFolder, mandatory=u'no', name=u'InConfigrel', nameAlias=u'', key=u'InIntfConfigRel', locked=u'no', cardinality=u'unspecified', targetName=u'internalInterface')
    vnsAbsFolder2 = cobra.model.vns.AbsFolder(vnsAbsFuncCfg, srcRef=u'', locked=u'no', name=u'ExtConfig', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'ExIntfConfigRelFolder', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsCfgRel2 = cobra.model.vns.AbsCfgRel(vnsAbsFolder2, mandatory=u'no', name=u'ExtConfigrel', nameAlias=u'', key=u'ExIntfConfigRel', locked=u'no', cardinality=u'unspecified', targetName=u'externalInterface')
    vnsAbsFolder3 = cobra.model.vns.AbsFolder(vnsAbsFuncCfg, srcRef=u'', locked=u'no', name=u'BridgeInterfaceFolder', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'BridgeInterfaceFolder', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsCfgRel3 = cobra.model.vns.AbsCfgRel(vnsAbsFolder3, mandatory=u'no', name=u'InBridgeGroupInterfaceRel', nameAlias=u'', key=u'InBridgeGroupInterfaceRel', locked=u'no', cardinality=u'unspecified', targetName=u'BVI1')
    vnsAbsFolder4 = cobra.model.vns.AbsFolder(vnsAbsFuncCfg, srcRef=u'', locked=u'no', name=u'AccessPolicyFolder', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'AccessPolicyFolder', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsCfgRel4 = cobra.model.vns.AbsCfgRel(vnsAbsFolder4, mandatory=u'no', name=u'InAccessPolicyRel', nameAlias=u'', key=u'InAccessPolicyRel', locked=u'no', cardinality=u'unspecified', targetName=u'ftd-ngips-policy')
    vnsAbsDevCfg = cobra.model.vns.AbsDevCfg(vnsAbsFuncProf, ownerKey=u'', ownerTag=u'', name=u'devConfig', descr=u'', nameAlias=u'')
    vnsAbsFolder5 = cobra.model.vns.AbsFolder(vnsAbsDevCfg, srcRef=u'', locked=u'no', name=u'internalInterface', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsParam = cobra.model.vns.AbsParam(vnsAbsFolder5, validation=u'', mandatory=u'no', name=u'enabled', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'true', key=u'enabled', locked=u'no', cardinality=u'unspecified')
    vnsAbsParam2 = cobra.model.vns.AbsParam(vnsAbsFolder5, validation=u'', mandatory=u'no', name=u'ifname', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'Provider', key=u'ifname', locked=u'no', cardinality=u'unspecified')
    vnsAbsFolder6 = cobra.model.vns.AbsFolder(vnsAbsFolder5, srcRef=u'', locked=u'no', name=u'int_security_zone', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'int_security_zone', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsCfgRel5 = cobra.model.vns.AbsCfgRel(vnsAbsFolder6, mandatory=u'no', name=u'security_zone', nameAlias=u'', key=u'security_zone', locked=u'no', cardinality=u'unspecified', targetName=u'db')
    vnsAbsFolder7 = cobra.model.vns.AbsFolder(vnsAbsDevCfg, srcRef=u'', locked=u'no', name=u'BVI1', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'BridgeGroupInterface', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsParam3 = cobra.model.vns.AbsParam(vnsAbsFolder7, validation=u'', mandatory=u'no', name=u'BridgeGroupId', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'1', key=u'bridge_group_id', locked=u'no', cardinality=u'unspecified')
    vnsAbsFolder8 = cobra.model.vns.AbsFolder(vnsAbsFolder7, srcRef=u'', locked=u'no', name=u'Interfaces', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'Interfaces', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsFolder9 = cobra.model.vns.AbsFolder(vnsAbsFolder8, srcRef=u'', locked=u'no', name=u'InternalInterface', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'Interface', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsCfgRel6 = cobra.model.vns.AbsCfgRel(vnsAbsFolder9, mandatory=u'no', name=u'InterfaceHolder', nameAlias=u'', key=u'InterfaceHolder', locked=u'no', cardinality=u'unspecified', targetName=u'internalInterface')
    vnsAbsFolder10 = cobra.model.vns.AbsFolder(vnsAbsFolder8, srcRef=u'', locked=u'no', name=u'ExternalInterface', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'Interface', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsCfgRel7 = cobra.model.vns.AbsCfgRel(vnsAbsFolder10, mandatory=u'no', name=u'InterfaceHolder', nameAlias=u'', key=u'InterfaceHolder', locked=u'no', cardinality=u'unspecified', targetName=u'externalInterface')
    vnsAbsFolder11 = cobra.model.vns.AbsFolder(vnsAbsFolder7, srcRef=u'', locked=u'no', name=u'IPv4Config', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'IPv4Config', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsFolder12 = cobra.model.vns.AbsFolder(vnsAbsFolder11, srcRef=u'', locked=u'no', name=u'static', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'static', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsParam4 = cobra.model.vns.AbsParam(vnsAbsFolder12, validation=u'', mandatory=u'yes', name=u'address', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'', key=u'address', locked=u'no', cardinality=u'unspecified')
    vnsAbsFolder13 = cobra.model.vns.AbsFolder(vnsAbsDevCfg, srcRef=u'', locked=u'no', name=u'db', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'SecurityZone', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsParam5 = cobra.model.vns.AbsParam(vnsAbsFolder13, validation=u'', mandatory=u'no', name=u'type', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'SWITCHED', key=u'type', locked=u'no', cardinality=u'unspecified')
    vnsAbsFolder14 = cobra.model.vns.AbsFolder(vnsAbsDevCfg, srcRef=u'', locked=u'no', name=u'web', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'SecurityZone', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsParam6 = cobra.model.vns.AbsParam(vnsAbsFolder14, validation=u'', mandatory=u'no', name=u'type', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'SWITCHED', key=u'type', locked=u'no', cardinality=u'unspecified')
    vnsAbsFolder15 = cobra.model.vns.AbsFolder(vnsAbsDevCfg, srcRef=u'', locked=u'no', name=u'ftd-ngips-policy', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'AccessPolicy', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsFolder16 = cobra.model.vns.AbsFolder(vnsAbsFolder15, srcRef=u'', locked=u'no', name=u'ftd-rule1', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'AccessRule', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsParam7 = cobra.model.vns.AbsParam(vnsAbsFolder16, validation=u'', mandatory=u'no', name=u'Bi-Directional', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'true', key=u'bidirectional', locked=u'no', cardinality=u'unspecified')
    vnsAbsFolder17 = cobra.model.vns.AbsFolder(vnsAbsFolder16, srcRef=u'', locked=u'no', name=u'AccSourceZones', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'AccSourceZones', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsCfgRel8 = cobra.model.vns.AbsCfgRel(vnsAbsFolder17, mandatory=u'no', name=u'SourceZone', nameAlias=u'', key=u'SourceZones', locked=u'no', cardinality=u'unspecified', targetName=u'externalInterface/int_security_zone')
    vnsAbsFolder18 = cobra.model.vns.AbsFolder(vnsAbsFolder16, srcRef=u'', locked=u'no', name=u'AccDestinationZones', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'AccDestinationZones', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsCfgRel9 = cobra.model.vns.AbsCfgRel(vnsAbsFolder18, mandatory=u'no', name=u'DestinationZone', nameAlias=u'', key=u'DestinationZones', locked=u'no', cardinality=u'unspecified', targetName=u'internalInterface/int_security_zone')
    vnsAbsFolder19 = cobra.model.vns.AbsFolder(vnsAbsDevCfg, srcRef=u'', locked=u'no', name=u'externalInterface', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsParam8 = cobra.model.vns.AbsParam(vnsAbsFolder19, validation=u'', mandatory=u'no', name=u'enabled', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'true', key=u'enabled', locked=u'no', cardinality=u'unspecified')
    vnsAbsParam9 = cobra.model.vns.AbsParam(vnsAbsFolder19, validation=u'', mandatory=u'no', name=u'ifname', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'Consumer', key=u'ifname', locked=u'no', cardinality=u'unspecified')
    vnsAbsFolder20 = cobra.model.vns.AbsFolder(vnsAbsFolder19, srcRef=u'', locked=u'no', name=u'int_security_zone', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'int_security_zone', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsCfgRel10 = cobra.model.vns.AbsCfgRel(vnsAbsFolder20, mandatory=u'no', name=u'security_zone', nameAlias=u'', key=u'security_zone', locked=u'no', cardinality=u'unspecified', targetName=u'web')

    c = cobra.mit.request.ConfigRequest()
    c.addMo(vnsAbsFuncProfContr)
    md.commit(c)



def sdcFtdv1ServiceGraphTemplate(md,tenant_name):
    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, '%s' % tenant_name)

    # build the request using cobra syntax
    vnsAbsGraph = cobra.model.vns.AbsGraph(fvTenant, ownerKey=u'', name=u'l3fw-ftdv-graph', descr=u'', ownerTag=u'', uiTemplateType=u'UNSPECIFIED')
    #Consumer
    vnsAbsTermNodeCon = cobra.model.vns.AbsTermNodeCon(vnsAbsGraph, ownerKey=u'', name=u'T1', descr=u'', ownerTag=u'')
    vnsAbsTermConn = cobra.model.vns.AbsTermConn(vnsAbsTermNodeCon, ownerKey=u'', attNotify=u'no', name=u'1', descr=u'', ownerTag=u'')
    vnsInTerm = cobra.model.vns.InTerm(vnsAbsTermNodeCon, name=u'', descr=u'')
    vnsOutTerm = cobra.model.vns.OutTerm(vnsAbsTermNodeCon, name=u'', descr=u'')
    #Provider
    vnsAbsTermNodeProv = cobra.model.vns.AbsTermNodeProv(vnsAbsGraph, ownerKey=u'', name=u'T2', descr=u'', ownerTag=u'')
    vnsAbsTermConn2 = cobra.model.vns.AbsTermConn(vnsAbsTermNodeProv, ownerKey=u'', attNotify=u'no', name=u'1', descr=u'', ownerTag=u'')
    vnsInTerm2 = cobra.model.vns.InTerm(vnsAbsTermNodeProv, name=u'', descr=u'')
    vnsOutTerm2 = cobra.model.vns.OutTerm(vnsAbsTermNodeProv, name=u'', descr=u'')
    #
    vnsAbsConnection = cobra.model.vns.AbsConnection(vnsAbsGraph, adjType=u'L2', ownerKey=u'', name=u'C1', descr=u'', connDir=u'provider', connType=u'external', unicastRoute=u'yes', ownerTag=u'', directConnect=u'no')
    vnsRsAbsConnectionConns = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection, tDn=u'uni/tn-%s/AbsGraph-l3fw-ftdv-graph/AbsNode-l3fw/AbsFConn-consumer' % tenant_name)
    vnsRsAbsConnectionConns2 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection, tDn=u'uni/tn-%s/AbsGraph-l3fw-ftdv-graph/AbsTermNodeCon-T1/AbsTConn' % tenant_name)
    #
    vnsAbsConnection2 = cobra.model.vns.AbsConnection(vnsAbsGraph, adjType=u'L2', ownerKey=u'', name=u'C2', descr=u'', connDir=u'provider', connType=u'external', unicastRoute=u'yes', ownerTag=u'', directConnect=u'no')
    vnsRsAbsConnectionConns3 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection2, tDn=u'uni/tn-%s/AbsGraph-l3fw-ftdv-graph/AbsNode-l3fw/AbsFConn-provider'  % tenant_name)
    vnsRsAbsConnectionConns4 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection2, tDn=u'uni/tn-%s/AbsGraph-l3fw-ftdv-graph/AbsTermNodeProv-T2/AbsTConn'  % tenant_name)
    #
    vnsAbsNode = cobra.model.vns.AbsNode(vnsAbsGraph, funcTemplateType=u'FW_ROUTED', isCopy=u'no', ownerKey=u'', managed=u'yes', name=u'l3fw', descr=u'', funcType=u'GoTo', shareEncap=u'no', sequenceNumber=u'0', routingMode=u'unspecified', ownerTag=u'')

    #Consumer
    vnsAbsFuncConn = cobra.model.vns.AbsFuncConn(vnsAbsNode, ownerKey=u'', attNotify=u'yes', name=u'consumer', descr=u'', ownerTag=u'')
    vnsRsMConnAtt = cobra.model.vns.RsMConnAtt(vnsAbsFuncConn, tDn=u'uni/infra/mDev-CISCO-FTD_FI-1.0/mFunc-FTD/mConn-external')
    #Provider
    vnsAbsFuncConn2 = cobra.model.vns.AbsFuncConn(vnsAbsNode, ownerKey=u'', attNotify=u'yes', name=u'provider', descr=u'', ownerTag=u'')
    vnsRsMConnAtt2 = cobra.model.vns.RsMConnAtt(vnsAbsFuncConn2, tDn=u'uni/infra/mDev-CISCO-FTD_FI-1.0/mFunc-FTD/mConn-internal')

    #Function Profile
    vnsRsNodeToAbsFuncProf = cobra.model.vns.RsNodeToAbsFuncProf(vnsAbsNode, tDn=u'uni/tn-%s/absFuncProfContr/absFuncProfGrp-FTDv/absFuncProf-l3fw-cfg-ftd1'  % tenant_name)
    #Device Package Function
    vnsRsNodeToMFunc = cobra.model.vns.RsNodeToMFunc(vnsAbsNode, tDn=u'uni/infra/mDev-CISCO-FTD_FI-1.0/mFunc-FTD')
    #Device
    vnsRsNodeToLDev = cobra.model.vns.RsNodeToLDev(vnsAbsNode, tDn=u'uni/tn-%s/lDevVip-FTDv' % tenant_name)

    # commit the generated code to APIC
    #print toXMLStr(fvTenant)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)



def sdcAsavDevice(md, tenantName, csrIp, asaDeviceName='ASAv'):

    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni,tenantName)

    vnsLDevVip = cobra.model.vns.LDevVip(fvTenant, isCopy=u'no', managed=u'yes', name=asaDeviceName, svcType=u'FW', funcType=u'GoThrough', promMode=u'no', devtype=u'VIRTUAL', packageModel=u'ASAv', contextAware=u'single-Context', nameAlias=u'', trunking=u'no', mode=u'legacy-Mode')
    vnsLIf = cobra.model.vns.LIf(vnsLDevVip, name=u'web', encap=u'unknown', nameAlias=u'')
    vnsRsMetaIf = cobra.model.vns.RsMetaIf(vnsLIf, isConAndProv=u'no', tDn=u'uni/infra/mDev-CISCO-ASA-1.3/mIfLbl-external')
    vnsRsCIfAttN = cobra.model.vns.RsCIfAttN(vnsLIf, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-'+str(asaDeviceName)+r'/cDev-Device1/cIf-[GigabitEthernet0/0]')
    vnsLIf2 = cobra.model.vns.LIf(vnsLDevVip, name=u'db', encap=u'unknown', nameAlias=u'')
    vnsRsMetaIf2 = cobra.model.vns.RsMetaIf(vnsLIf2, isConAndProv=u'no', tDn=u'uni/infra/mDev-CISCO-ASA-1.3/mIfLbl-internal')
    vnsRsCIfAttN2 = cobra.model.vns.RsCIfAttN(vnsLIf2, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-'+str(asaDeviceName)+r'/cDev-Device1/cIf-[GigabitEthernet0/1]')
    vnsRsALDevToDomP = cobra.model.vns.RsALDevToDomP(vnsLDevVip, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud')
    vnsCMgmt = cobra.model.vns.CMgmt(vnsLDevVip, host=str(csrIp), name=u'', nameAlias=u'', port=u'21443')
    vnsCDev = cobra.model.vns.CDev(vnsLDevVip, vcenterName=u'vc60-aci-1', vmName=u''+str(tenantName)+r'-asav', name=u'Device1', nameAlias=u'', devCtxLbl=u'')
    vnsCMgmt2 = cobra.model.vns.CMgmt(vnsCDev, host=str(csrIp), name=u'', nameAlias=u'', port=u'21443')
    vnsCIf = cobra.model.vns.CIf(vnsCDev, name=u'GigabitEthernet0/1', nameAlias=u'', vnicName=u'Network adapter 3')
    vnsCIf2 = cobra.model.vns.CIf(vnsCDev, name=u'GigabitEthernet0/0', nameAlias=u'', vnicName=u'Network adapter 2')
    vnsCCredSecret = cobra.model.vns.CCredSecret(vnsCDev, name=u'password', nameAlias=u'' ,value='C1sco12345')
    vnsCCred = cobra.model.vns.CCred(vnsCDev, value=u'admin', name=u'username', nameAlias=u'')
    vnsCCredSecret2 = cobra.model.vns.CCredSecret(vnsLDevVip, name=u'password', nameAlias=u'', value='C1sco12345')
    vnsRsMDevAtt = cobra.model.vns.RsMDevAtt(vnsLDevVip, tDn=u'uni/infra/mDev-CISCO-ASA-1.3')
    vnsCCred2 = cobra.model.vns.CCred(vnsLDevVip, value=u'admin', name=u'username', nameAlias=u'')

    # commit the generated code to APIC
    #print toXMLStr(fvTenant)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)



def sdcAsavFunctionProfile(md, tenantName, funcProfName='l2fw-cfg-asav', funcProfGroupName='ASAv'):

    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)
    vnsAbsFuncProfContr = cobra.model.vns.AbsFuncProfContr(fvTenant)
    vnsAbsFuncProfGrp = cobra.model.vns.AbsFuncProfGrp(vnsAbsFuncProfContr, ownerKey=u'', name=funcProfGroupName, descr=u'', nameAlias=u'', ownerTag=u'')
    vnsAbsFuncProf = cobra.model.vns.AbsFuncProf(vnsAbsFuncProfGrp, ownerKey=u'', name=funcProfName, descr=u'', srcMode=u'', nameAlias=u'', ownerTag=u'')
    vnsRsProfToMFunc = cobra.model.vns.RsProfToMFunc(vnsAbsFuncProf, tDn=u'uni/infra/mDev-CISCO-ASA-1.3/mFunc-Firewall')
    vnsAbsFuncCfg = cobra.model.vns.AbsFuncCfg(vnsAbsFuncProf, ownerKey=u'', ownerTag=u'', name=u'funcConfig', descr=u'', nameAlias=u'')
    vnsAbsFolder = cobra.model.vns.AbsFolder(vnsAbsFuncCfg, srcRef=u'', locked=u'no', name=u'IntConfig', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'InIntfConfigRelFolder', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsCfgRel = cobra.model.vns.AbsCfgRel(vnsAbsFolder, mandatory=u'no', name=u'InConfigrel', nameAlias=u'', key=u'InIntfConfigRel', locked=u'no', cardinality=u'unspecified', targetName=u'internalIf')
    vnsAbsFolder2 = cobra.model.vns.AbsFolder(vnsAbsFuncCfg, srcRef=u'', locked=u'no', name=u'ExtConfig', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'ExIntfConfigRelFolder', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsCfgRel2 = cobra.model.vns.AbsCfgRel(vnsAbsFolder2, mandatory=u'no', name=u'ExtConfigrel', nameAlias=u'', key=u'ExIntfConfigRel', locked=u'no', cardinality=u'unspecified', targetName=u'externalIf')
    vnsAbsDevCfg = cobra.model.vns.AbsDevCfg(vnsAbsFuncProf, ownerKey=u'', ownerTag=u'', name=u'devConfig', descr=u'', nameAlias=u'')

    #Internal Interface
    vnsAbsFolder3 = cobra.model.vns.AbsFolder(vnsAbsDevCfg, srcRef=u'', locked=u'no', name=u'internalIf', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'Interface', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsFolder4 = cobra.model.vns.AbsFolder(vnsAbsFolder3, srcRef=u'', locked=u'no', name=u'internalIfCfg', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsParam = cobra.model.vns.AbsParam(vnsAbsFolder4, validation=u'', mandatory=u'no', name=u'internal_security_level', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'100', key=u'security_level', locked=u'no', cardinality=u'unspecified')
    vnsAbsCfgRel3 = cobra.model.vns.AbsCfgRel(vnsAbsFolder4, mandatory=u'no', name=u'intbridge', nameAlias=u'', key=u'bridge_group', locked=u'no', cardinality=u'unspecified', targetName=u'1')
    vnsAbsFolder16 = cobra.model.vns.AbsFolder(vnsAbsFolder3, srcRef=u'', locked=u'no', name=u'IntAccessGroup', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'AccessGroup', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsCfgRel6 = cobra.model.vns.AbsCfgRel(vnsAbsFolder16, mandatory=u'no', name=u'name', nameAlias=u'', key=u'inbound_access_list_name', locked=u'no', cardinality=u'unspecified', targetName=u'access-list-inbound')


    vnsAbsFolder5 = cobra.model.vns.AbsFolder(vnsAbsDevCfg, srcRef=u'', locked=u'no', name=u'access-list-inbound', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'AccessList', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsFolder6 = cobra.model.vns.AbsFolder(vnsAbsFolder5, srcRef=u'', locked=u'no', name=u'permit-any', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'AccessControlEntry', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsParam2 = cobra.model.vns.AbsParam(vnsAbsFolder6, validation=u'', mandatory=u'yes', name=u'order1', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'10', key=u'order', locked=u'no', cardinality=u'unspecified')
    vnsAbsParam3 = cobra.model.vns.AbsParam(vnsAbsFolder6, validation=u'', mandatory=u'yes', name=u'action-permit', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'permit', key=u'action', locked=u'no', cardinality=u'unspecified')
    vnsAbsFolder7 = cobra.model.vns.AbsFolder(vnsAbsFolder6, srcRef=u'', locked=u'no', name=u'dest-address', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'destination_address', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsParam4 = cobra.model.vns.AbsParam(vnsAbsFolder7, validation=u'', mandatory=u'no', name=u'any', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'any', key=u'any', locked=u'no', cardinality=u'unspecified')
    vnsAbsFolder8 = cobra.model.vns.AbsFolder(vnsAbsFolder6, srcRef=u'', locked=u'no', name=u'ip', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'protocol', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsParam5 = cobra.model.vns.AbsParam(vnsAbsFolder8, validation=u'', mandatory=u'no', name=u'ip', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'ip', key=u'name_number', locked=u'no', cardinality=u'unspecified')
    vnsAbsFolder9 = cobra.model.vns.AbsFolder(vnsAbsFolder6, srcRef=u'', locked=u'no', name=u'src-address', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'source_address', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsParam6 = cobra.model.vns.AbsParam(vnsAbsFolder9, validation=u'', mandatory=u'no', name=u'any', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'any', key=u'any', locked=u'no', cardinality=u'unspecified')
    vnsAbsFolder10 = cobra.model.vns.AbsFolder(vnsAbsDevCfg, srcRef=u'', locked=u'no', name=u'1', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'BridgeGroupIntf', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsFolder11 = cobra.model.vns.AbsFolder(vnsAbsFolder10, srcRef=u'', locked=u'no', name=u'BviIP', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'IPv4Address', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsParam7 = cobra.model.vns.AbsParam(vnsAbsFolder11, validation=u'', mandatory=u'yes', name=u'IP', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'', key=u'ipv4_address', locked=u'no', cardinality=u'unspecified')

    #External Interface
    vnsAbsFolder12 = cobra.model.vns.AbsFolder(vnsAbsDevCfg, srcRef=u'', locked=u'no', name=u'externalIf', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'Interface', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsFolder13 = cobra.model.vns.AbsFolder(vnsAbsFolder12, srcRef=u'', locked=u'no', name=u'externalIfCfg', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsParam8 = cobra.model.vns.AbsParam(vnsAbsFolder13, validation=u'', mandatory=u'no', name=u'external_security_level', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'50', key=u'security_level', locked=u'no', cardinality=u'unspecified')
    vnsAbsCfgRel4 = cobra.model.vns.AbsCfgRel(vnsAbsFolder13, mandatory=u'no', name=u'extbridge', nameAlias=u'', key=u'bridge_group', locked=u'no', cardinality=u'unspecified', targetName=u'1')
    vnsAbsFolder14 = cobra.model.vns.AbsFolder(vnsAbsFolder12, srcRef=u'', locked=u'no', name=u'ExtAccessGroup', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'AccessGroup', cardinality=u'unspecified', nameAlias=u'')
    vnsAbsCfgRel5 = cobra.model.vns.AbsCfgRel(vnsAbsFolder14, mandatory=u'no', name=u'name', nameAlias=u'', key=u'inbound_access_list_name', locked=u'no', cardinality=u'unspecified', targetName=u'access-list-inbound')

    vnsAbsFolder15 = cobra.model.vns.AbsFolder(vnsAbsDevCfg, srcRef=u'', locked=u'no', name=u'web_server', devCtxLbl=u'', scopedBy=u'epg', auxInfo=u'', profileBehaviorShared=u'no', key=u'NetworkObject', cardinality=u'unspecified', nameAlias=u'')

    vnsAbsParam9 = cobra.model.vns.AbsParam(vnsAbsFolder15, validation=u'', mandatory=u'yes', name=u'network_ip_address', nameAlias=u'', srcRef=u'', auxInfo=u'', value=u'', key=u'network_ip_address', locked=u'no', cardinality=u'unspecified')

    # commit the generated code to APIC
    #print toXMLStr(vnsAbsFuncProfContr)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(vnsAbsFuncProfContr)
    md.commit(c)


def sdcFtdv2AsavServiceGraphTemplate(md, tenantName, graphTemplateName='asav-ftd-graph' , asavFuncProfileName='l2fw-cfg-asav', asaDeviceName='ASAv',  ftdFuncProfileName='l2fw-cfg-ftdv-ngips', ftdDeviceName='ftdv-ngips' ):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)

    # build the request using cobra syntax
    vnsAbsGraph = cobra.model.vns.AbsGraph(fvTenant, ownerKey=u'', name=graphTemplateName, descr=u'', nameAlias=u'', ownerTag=u'', uiTemplateType=u'UNSPECIFIED')
    vnsAbsTermNodeProv = cobra.model.vns.AbsTermNodeProv(vnsAbsGraph, ownerKey=u'', ownerTag=u'', name=u'T2', descr=u'', nameAlias=u'')
    vnsOutTerm = cobra.model.vns.OutTerm(vnsAbsTermNodeProv, name=u'', descr=u'', nameAlias=u'')
    vnsInTerm = cobra.model.vns.InTerm(vnsAbsTermNodeProv, name=u'', descr=u'', nameAlias=u'')
    vnsAbsTermConn = cobra.model.vns.AbsTermConn(vnsAbsTermNodeProv, ownerKey=u'', attNotify=u'no', name=u'1', descr=u'', nameAlias=u'', ownerTag=u'')
    vnsAbsTermNodeCon = cobra.model.vns.AbsTermNodeCon(vnsAbsGraph, ownerKey=u'', ownerTag=u'', name=u'T1', descr=u'', nameAlias=u'')
    vnsOutTerm2 = cobra.model.vns.OutTerm(vnsAbsTermNodeCon, name=u'', descr=u'', nameAlias=u'')
    vnsInTerm2 = cobra.model.vns.InTerm(vnsAbsTermNodeCon, name=u'', descr=u'', nameAlias=u'')
    vnsAbsTermConn2 = cobra.model.vns.AbsTermConn(vnsAbsTermNodeCon, ownerKey=u'', attNotify=u'no', name=u'1', descr=u'', nameAlias=u'', ownerTag=u'')
    vnsAbsNode = cobra.model.vns.AbsNode(vnsAbsGraph, funcTemplateType=u'FW_TRANS', isCopy=u'no', ownerKey=u'', managed=u'yes', name=u'N2', descr=u'', funcType=u'GoThrough', shareEncap=u'no', sequenceNumber=u'0', routingMode=u'unspecified', nameAlias=u'', ownerTag=u'')
    vnsRsNodeToMFunc = cobra.model.vns.RsNodeToMFunc(vnsAbsNode, tDn=u'uni/infra/mDev-CISCO-FTD_FI-1.0/mFunc-FTD')
    vnsRsNodeToLDev = cobra.model.vns.RsNodeToLDev(vnsAbsNode, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-'+str(ftdDeviceName))
    vnsRsNodeToAbsFuncProf = cobra.model.vns.RsNodeToAbsFuncProf(vnsAbsNode, tDn=u'uni/tn-'+str(tenantName)+r'/absFuncProfContr/absFuncProfGrp-FTDv/absFuncProf-'+str(ftdFuncProfileName))
    vnsAbsFuncConn = cobra.model.vns.AbsFuncConn(vnsAbsNode, ownerKey=u'', attNotify=u'no', name=u'provider', descr=u'', nameAlias=u'', ownerTag=u'')
    vnsRsMConnAtt = cobra.model.vns.RsMConnAtt(vnsAbsFuncConn, tDn=u'uni/infra/mDev-CISCO-FTD_FI-1.0/mFunc-FTD/mConn-internal')
    vnsAbsFuncConn2 = cobra.model.vns.AbsFuncConn(vnsAbsNode, ownerKey=u'', attNotify=u'no', name=u'consumer', descr=u'', nameAlias=u'', ownerTag=u'')
    vnsRsMConnAtt2 = cobra.model.vns.RsMConnAtt(vnsAbsFuncConn2, tDn=u'uni/infra/mDev-CISCO-FTD_FI-1.0/mFunc-FTD/mConn-external')
    vnsAbsNode2 = cobra.model.vns.AbsNode(vnsAbsGraph, funcTemplateType=u'FW_TRANS', isCopy=u'no', ownerKey=u'', managed=u'yes', name=u'N1', descr=u'', funcType=u'GoThrough', shareEncap=u'no', sequenceNumber=u'0', routingMode=u'unspecified', nameAlias=u'', ownerTag=u'')
    vnsRsNodeToMFunc2 = cobra.model.vns.RsNodeToMFunc(vnsAbsNode2, tDn=u'uni/infra/mDev-CISCO-ASA-1.3/mFunc-Firewall')
    vnsRsNodeToLDev2 = cobra.model.vns.RsNodeToLDev(vnsAbsNode2, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-'+str(asaDeviceName))
    vnsRsNodeToAbsFuncProf2 = cobra.model.vns.RsNodeToAbsFuncProf(vnsAbsNode2, tDn=u'uni/tn-'+str(tenantName)+r'/absFuncProfContr/absFuncProfGrp-ASAv/absFuncProf-'+str(asavFuncProfileName))
    vnsAbsFuncConn3 = cobra.model.vns.AbsFuncConn(vnsAbsNode2, ownerKey=u'', attNotify=u'no', name=u'provider', descr=u'', nameAlias=u'', ownerTag=u'')
    vnsRsMConnAtt3 = cobra.model.vns.RsMConnAtt(vnsAbsFuncConn3, tDn=u'uni/infra/mDev-CISCO-ASA-1.3/mFunc-Firewall/mConn-internal')
    vnsAbsFuncConn4 = cobra.model.vns.AbsFuncConn(vnsAbsNode2, ownerKey=u'', attNotify=u'no', name=u'consumer', descr=u'', nameAlias=u'', ownerTag=u'')
    vnsRsMConnAtt4 = cobra.model.vns.RsMConnAtt(vnsAbsFuncConn4, tDn=u'uni/infra/mDev-CISCO-ASA-1.3/mFunc-Firewall/mConn-external')
    vnsAbsConnection = cobra.model.vns.AbsConnection(vnsAbsGraph, adjType=u'L2', ownerKey=u'', name=u'C1', descr=u'', connDir=u'provider', connType=u'external', nameAlias=u'', ownerTag=u'', directConnect=u'no', unicastRoute=u'yes')
    vnsRsAbsConnectionConns = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection, tDn=u'uni/tn-'+str(tenantName)+r'/AbsGraph-'+str(graphTemplateName)+r'/AbsNode-N1/AbsFConn-consumer')
    vnsRsAbsConnectionConns2 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection, tDn=u'uni/tn-'+str(tenantName)+r'/AbsGraph-'+str(graphTemplateName)+r'/AbsTermNodeCon-T1/AbsTConn')
    vnsAbsConnection2 = cobra.model.vns.AbsConnection(vnsAbsGraph, adjType=u'L2', ownerKey=u'', name=u'C3', descr=u'', connDir=u'provider', connType=u'external', nameAlias=u'', ownerTag=u'', directConnect=u'no', unicastRoute=u'yes')
    vnsRsAbsConnectionConns3 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection2, tDn=u'uni/tn-'+str(tenantName)+r'/AbsGraph-'+str(graphTemplateName)+r'/AbsTermNodeProv-T2/AbsTConn')
    vnsRsAbsConnectionConns4 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection2, tDn=u'uni/tn-'+str(tenantName)+r'/AbsGraph-'+str(graphTemplateName)+r'/AbsNode-N2/AbsFConn-provider')
    vnsAbsConnection3 = cobra.model.vns.AbsConnection(vnsAbsGraph, adjType=u'L2', ownerKey=u'', name=u'C2', descr=u'', connDir=u'provider', connType=u'external', nameAlias=u'', ownerTag=u'', directConnect=u'no', unicastRoute=u'yes')
    vnsRsAbsConnectionConns5 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection3, tDn=u'uni/tn-'+str(tenantName)+r'/AbsGraph-'+str(graphTemplateName)+r'/AbsNode-N2/AbsFConn-consumer')
    vnsRsAbsConnectionConns6 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection3, tDn=u'uni/tn-'+str(tenantName)+r'/AbsGraph-'+str(graphTemplateName)+r'/AbsNode-N1/AbsFConn-provider')


    # commit the generated code to APIC
    #print toXMLStr(fvTenant)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)





def sdcFtdv1ServiceGraphApply(md,tenant_name):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    #topMo = cobra.model.pol.Uni('')

    # build the request using cobra syntax
    fvTenant = cobra.model.fv.Tenant(polUni, '%s' % tenant_name)

    #fvAp = cobra.model.fv.Ap(fvTenant)
    #fvAEPg = cobra.model.fv.AEPg(fvAp)
    #fvRsCons = cobra.model.fv.RsCons(fvAEPg, tnVzBrCPName=u'attacker-to-webout')
    #fvAEPg2 = cobra.model.fv.AEPg(fvAp)

    fvAp = cobra.model.fv.Ap(fvTenant, prio=u'unspecified', name=u'CloudCenter-Apps')
    fvAEPg = cobra.model.fv.AEPg(fvAp, matchT=u'AtleastOne', name=u'Attacker')
    fvRsCons = cobra.model.fv.RsCons(fvAEPg, tnVzBrCPName=u'attacker-to-webout')
    fvAEPg2 = cobra.model.fv.AEPg(fvAp, matchT=u'AtleastOne', name=u'Web-Out')

    #External Config
    vnsFolderInst = cobra.model.vns.FolderInst(fvAEPg2, graphNameOrLbl=u'l3fw-ftdv-graph', ctrctNameOrLbl=u'attacker-to-webout', key=u'ExIntfConfigRelFolder', nodeNameOrLbl=u'l3fw', name=u'ExtConfig')
    vnsCfgRelInst = cobra.model.vns.CfgRelInst(vnsFolderInst, targetName=u'externalInterface', name=u'ExtConfigrel', key=u'ExIntfConfigRel')

    #Internal Config
    vnsFolderInst2 = cobra.model.vns.FolderInst(fvAEPg2, graphNameOrLbl=u'l3fw-ftdv-graph', ctrctNameOrLbl=u'attacker-to-webout', key=u'InIntfConfigRelFolder', nodeNameOrLbl=u'l3fw', name=u'IntConfig')
    vnsCfgRelInst2 = cobra.model.vns.CfgRelInst(vnsFolderInst2, targetName=u'internalInterface', name=u'InConfigrel', key=u'InIntfConfigRel')

    #Security Zone attacker-zone
    vnsFolderInst3 = cobra.model.vns.FolderInst(fvAEPg2, graphNameOrLbl=u'l3fw-ftdv-graph', ctrctNameOrLbl=u'attacker-to-webout', key=u'SecurityZone', nodeNameOrLbl=u'l3fw', name=u'attacker-zone')
    vnsParamInst = cobra.model.vns.ParamInst(vnsFolderInst3, value=u'ROUTED', name=u'type', key=u'type')

    #Security Zone webout-zone
    vnsFolderInst4 = cobra.model.vns.FolderInst(fvAEPg2, graphNameOrLbl=u'l3fw-ftdv-graph', ctrctNameOrLbl=u'attacker-to-webout', key=u'SecurityZone', nodeNameOrLbl=u'l3fw', name=u'webout-zone')
    vnsParamInst2 = cobra.model.vns.ParamInst(vnsFolderInst4, value=u'ROUTED', name=u'type', key=u'type')


    #Apply Graph External Interface
    vnsFolderInst5 = cobra.model.vns.FolderInst(fvAEPg2, graphNameOrLbl=u'l3fw-ftdv-graph', ctrctNameOrLbl=u'attacker-to-webout', key=u'InterfaceConfig', nodeNameOrLbl=u'l3fw', name=u'externalInterface')
    vnsFolderInst6 = cobra.model.vns.FolderInst(vnsFolderInst5, graphNameOrLbl=u'l3fw-ftdv-graph', ctrctNameOrLbl=u'attacker-to-webout', key=u'IPv4Config', nodeNameOrLbl=u'l3fw', name=u'IPv4Config')
    vnsFolderInst7 = cobra.model.vns.FolderInst(vnsFolderInst6, graphNameOrLbl=u'l3fw-ftdv-graph', ctrctNameOrLbl=u'attacker-to-webout', key=u'static', nodeNameOrLbl=u'l3fw', name=u'static')
    vnsParamInst3 = cobra.model.vns.ParamInst(vnsFolderInst7, value=u'10.150.1.1/24', name=u'address', key=u'address')

    #Apply Graph Interface Secutity Zone
    vnsFolderInst8 = cobra.model.vns.FolderInst(vnsFolderInst5, graphNameOrLbl=u'l3fw-ftdv-graph', ctrctNameOrLbl=u'attacker-to-webout', key=u'int_security_zone', nodeNameOrLbl=u'l3fw', name=u'int_security_zone')
    vnsCfgRelInst3 = cobra.model.vns.CfgRelInst(vnsFolderInst8, targetName=u'attacker-zone', name=u'security_zone', key=u'security_zone')
    vnsParamInst4 = cobra.model.vns.ParamInst(vnsFolderInst5, value=u'true', name=u'enabled', key=u'enabled')
    vnsParamInst5 = cobra.model.vns.ParamInst(vnsFolderInst5, value=u'attacker', name=u'ifname', key=u'ifname')


    #Appply Graph Internal Interface
    vnsFolderInst9 = cobra.model.vns.FolderInst(fvAEPg2, graphNameOrLbl=u'l3fw-ftdv-graph', ctrctNameOrLbl=u'attacker-to-webout', key=u'InterfaceConfig', nodeNameOrLbl=u'l3fw', name=u'internalInterface')
    vnsFolderInst10 = cobra.model.vns.FolderInst(vnsFolderInst9, graphNameOrLbl=u'l3fw-ftdv-graph', ctrctNameOrLbl=u'attacker-to-webout', key=u'IPv4Config', nodeNameOrLbl=u'l3fw', name=u'IPv4Config')
    vnsFolderInst11 = cobra.model.vns.FolderInst(vnsFolderInst10, graphNameOrLbl=u'l3fw-ftdv-graph', ctrctNameOrLbl=u'attacker-to-webout', key=u'static', nodeNameOrLbl=u'l3fw', name=u'static')
    vnsParamInst6 = cobra.model.vns.ParamInst(vnsFolderInst11, value=u'10.150.2.1/24', name=u'address', key=u'address')

    #Apply Graph Interface Security ZOne
    vnsFolderInst12 = cobra.model.vns.FolderInst(vnsFolderInst9, graphNameOrLbl=u'l3fw-ftdv-graph', ctrctNameOrLbl=u'attacker-to-webout', key=u'int_security_zone', nodeNameOrLbl=u'l3fw', name=u'int_security_zone')
    vnsCfgRelInst4 = cobra.model.vns.CfgRelInst(vnsFolderInst12, targetName=u'webout-zone', name=u'security_zone', key=u'security_zone')
    vnsParamInst7 = cobra.model.vns.ParamInst(vnsFolderInst9, value=u'true', name=u'enabled', key=u'enabled')
    vnsParamInst8 = cobra.model.vns.ParamInst(vnsFolderInst9, value=u'webout', name=u'ifname', key=u'ifname')

 
    #Contract
    fvRsProv = cobra.model.fv.RsProv(fvAEPg2, tnVzBrCPName=u'attacker-to-webout')
    vzBrCP = cobra.model.vz.BrCP(fvTenant, name=u'attacker-to-webout')
    vzSubj = cobra.model.vz.Subj(vzBrCP, name=u'Subject')
    vzRsSubjFiltAtt = cobra.model.vz.RsSubjFiltAtt(vzSubj, tnVzFilterName=u'default')
    vzRsSubjGraphAtt = cobra.model.vz.RsSubjGraphAtt(vzSubj, tnVnsAbsGraphName=u'l3fw-ftdv-graph')

    #
    vnsLDevCtx = cobra.model.vns.LDevCtx(fvTenant, ctrctNameOrLbl=u'attacker-to-webout', graphNameOrLbl=u'l3fw-ftdv-graph', nodeNameOrLbl=u'l3fw')

    #Consumer Interface
    vnsLIfCtx = cobra.model.vns.LIfCtx(vnsLDevCtx, connNameOrLbl=u'consumer')
    vnsRsLIfCtxToBD = cobra.model.vns.RsLIfCtxToBD(vnsLIfCtx, tDn=u'uni/tn-%s/BD-Attacker-BD' % tenant_name)
    vnsRsLIfCtxToLIf = cobra.model.vns.RsLIfCtxToLIf(vnsLIfCtx, tDn=u'uni/tn-%s/lDevVip-FTDv/lIf-attacker' % tenant_name)

    #Provider Interface
    vnsLIfCtx2 = cobra.model.vns.LIfCtx(vnsLDevCtx, connNameOrLbl=u'provider')
    vnsRsLIfCtxToBD2 = cobra.model.vns.RsLIfCtxToBD(vnsLIfCtx2, tDn=u'uni/tn-%s/BD-App-BD' % tenant_name)
    vnsRsLIfCtxToLIf2 = cobra.model.vns.RsLIfCtxToLIf(vnsLIfCtx2, tDn=u'uni/tn-%s/lDevVip-FTDv/lIf-webout' % tenant_name)

    #DevCtx to Device
    vnsRsLDevCtxToLDev = cobra.model.vns.RsLDevCtxToLDev(vnsLDevCtx, tDn=u'uni/tn-%s/lDevVip-FTDv' % tenant_name)


    # commit the generated code to APIC
    #print toXMLStr(fvTenant)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)





def sdcFtdv2AsavServiceGraphApply(md, tenantName, ipPoolIndex, networkObjectIp='1.1.1.1/32'):

    #This is a table listing the IP avaialble for BVI
    #This implimentation of table can be imprved in terms of resources
    #Mapping for hybridcloud-inbound BD in common
    #subnet='198.19.192.'
    #mask=r'/25'
    #poolSize=40
    #bviIpPool=[]
    #for i in range (86,86+poolSize):
    #    bviIpPool.append(subnet+str(i)+mask)
    #n1BviIp=bviIpPool[2*(int(ipPoolIndex)-1)]
    #n2BviIp=bviIpPool[2*(int(ipPoolIndex)-1)+1]

    #Mapping for securedc-inbound BD in common
    subnet='198.19.197.'
    mask=r'/24'
    poolSize=40
    bviIpPool=[]
    for i in range (200,200+poolSize):
        bviIpPool.append(subnet+str(i)+mask)
    n1BviIp=bviIpPool[2*(int(ipPoolIndex)-1)]
    n2BviIp=bviIpPool[2*(int(ipPoolIndex)-1)+1]



    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)
    fvAp = cobra.model.fv.Ap(fvTenant, 'CloudCenter-Apps')


    fvAEPgWeb = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Web', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    fvRsConsWeb = cobra.model.fv.RsCons(fvAEPgWeb, tnVzBrCPName=u'web-to-db', prio=u'unspecified')

    fvAEPg = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Db', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')

    vnsFolderInst = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'Interface', cardinality=u'unspecified', nameAlias=u'', name=u'internalIf')
    vnsFolderInst2 = cobra.model.vns.FolderInst(vnsFolderInst, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'', name=u'internalIfCfg')
    vnsParamInst = cobra.model.vns.ParamInst(vnsFolderInst2, validation=u'', mandatory=u'no', name=u'internal_security_level', nameAlias=u'', value=u'100', key=u'security_level', locked=u'no', cardinality=u'unspecified')
    vnsCfgRelInst = cobra.model.vns.CfgRelInst(vnsFolderInst2, mandatory=u'no', name=u'intbridge', nameAlias=u'', key=u'bridge_group', locked=u'no', cardinality=u'unspecified', targetName=u'1')
    vnsFolderInst3 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'', name=u'externalInterface')
    vnsParamInst2 = cobra.model.vns.ParamInst(vnsFolderInst3, validation=u'', mandatory=u'no', name=u'ifname', nameAlias=u'', value=u'Consumer', key=u'ifname', locked=u'no', cardinality=u'unspecified')
    vnsParamInst3 = cobra.model.vns.ParamInst(vnsFolderInst3, validation=u'', mandatory=u'no', name=u'enabled', nameAlias=u'', value=u'true', key=u'enabled', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst4 = cobra.model.vns.FolderInst(vnsFolderInst3, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'int_security_zone', cardinality=u'unspecified', nameAlias=u'', name=u'int_security_zone')
    vnsCfgRelInst2 = cobra.model.vns.CfgRelInst(vnsFolderInst4, mandatory=u'no', name=u'security_zone', nameAlias=u'', key=u'security_zone', locked=u'no', cardinality=u'unspecified', targetName=u'web')
    vnsFolderInst5 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'', name=u'internalInterface')
    vnsParamInst4 = cobra.model.vns.ParamInst(vnsFolderInst5, validation=u'', mandatory=u'no', name=u'ifname', nameAlias=u'', value=u'Provider', key=u'ifname', locked=u'no', cardinality=u'unspecified')
    vnsParamInst5 = cobra.model.vns.ParamInst(vnsFolderInst5, validation=u'', mandatory=u'no', name=u'enabled', nameAlias=u'', value=u'true', key=u'enabled', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst6 = cobra.model.vns.FolderInst(vnsFolderInst5, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'int_security_zone', cardinality=u'unspecified', nameAlias=u'', name=u'int_security_zone')
    vnsCfgRelInst3 = cobra.model.vns.CfgRelInst(vnsFolderInst6, mandatory=u'no', name=u'security_zone', nameAlias=u'', key=u'security_zone', locked=u'no', cardinality=u'unspecified', targetName=u'db')
    vnsFolderInst7 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'NetworkObject', cardinality=u'unspecified', nameAlias=u'', name=u'web_server')
    vnsParamInst6 = cobra.model.vns.ParamInst(vnsFolderInst7, validation=u'', mandatory=u'no', name=u'network_ip_address', nameAlias=u'', value=str(networkObjectIp), key=u'network_ip_address', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst8 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'AccessPolicy', cardinality=u'unspecified', nameAlias=u'', name=u'ftd-ngips-policy')
    vnsFolderInst9 = cobra.model.vns.FolderInst(vnsFolderInst8, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'AccessRule', cardinality=u'unspecified', nameAlias=u'', name=u'ftd-rule1')
    vnsParamInst7 = cobra.model.vns.ParamInst(vnsFolderInst9, validation=u'', mandatory=u'no', name=u'Bi-Directional', nameAlias=u'', value=u'true', key=u'bidirectional', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst10 = cobra.model.vns.FolderInst(vnsFolderInst9, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'AccDestinationZones', cardinality=u'unspecified', nameAlias=u'', name=u'AccDestinationZones')
    vnsCfgRelInst4 = cobra.model.vns.CfgRelInst(vnsFolderInst10, mandatory=u'no', name=u'DestinationZone', nameAlias=u'', key=u'DestinationZones', locked=u'no', cardinality=u'unspecified', targetName=u'internalInterface/int_security_zone')
    vnsFolderInst11 = cobra.model.vns.FolderInst(vnsFolderInst9, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'AccSourceZones', cardinality=u'unspecified', nameAlias=u'', name=u'AccSourceZones')
    vnsCfgRelInst5 = cobra.model.vns.CfgRelInst(vnsFolderInst11, mandatory=u'no', name=u'SourceZone', nameAlias=u'', key=u'SourceZones', locked=u'no', cardinality=u'unspecified', targetName=u'externalInterface/int_security_zone')
    vnsFolderInst12 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'AccessList', cardinality=u'unspecified', nameAlias=u'', name=u'access-list-inbound')
    vnsFolderInst13 = cobra.model.vns.FolderInst(vnsFolderInst12, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'AccessControlEntry', cardinality=u'unspecified', nameAlias=u'', name=u'permit-any')
    vnsParamInst8 = cobra.model.vns.ParamInst(vnsFolderInst13, validation=u'', mandatory=u'no', name=u'action-permit', nameAlias=u'', value=u'permit', key=u'action', locked=u'no', cardinality=u'unspecified')
    vnsParamInst9 = cobra.model.vns.ParamInst(vnsFolderInst13, validation=u'', mandatory=u'no', name=u'order1', nameAlias=u'', value=u'10', key=u'order', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst14 = cobra.model.vns.FolderInst(vnsFolderInst13, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'source_address', cardinality=u'unspecified', nameAlias=u'', name=u'src-address')
    vnsParamInst10 = cobra.model.vns.ParamInst(vnsFolderInst14, validation=u'', mandatory=u'no', name=u'any', nameAlias=u'', value=u'any', key=u'any', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst15 = cobra.model.vns.FolderInst(vnsFolderInst13, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'destination_address', cardinality=u'unspecified', nameAlias=u'', name=u'dest-address')
    vnsParamInst11 = cobra.model.vns.ParamInst(vnsFolderInst15, validation=u'', mandatory=u'no', name=u'any', nameAlias=u'', value=u'any', key=u'any', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst16 = cobra.model.vns.FolderInst(vnsFolderInst13, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'protocol', cardinality=u'unspecified', nameAlias=u'', name=u'ip')
    vnsParamInst12 = cobra.model.vns.ParamInst(vnsFolderInst16, validation=u'', mandatory=u'no', name=u'ip', nameAlias=u'', value=u'ip', key=u'name_number', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst17 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'SecurityZone', cardinality=u'unspecified', nameAlias=u'', name=u'web')
    vnsParamInst13 = cobra.model.vns.ParamInst(vnsFolderInst17, validation=u'', mandatory=u'no', name=u'type', nameAlias=u'', value=u'SWITCHED', key=u'type', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst18 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'ExIntfConfigRelFolder', cardinality=u'unspecified', nameAlias=u'', name=u'ExtConfig')
    vnsCfgRelInst6 = cobra.model.vns.CfgRelInst(vnsFolderInst18, mandatory=u'no', name=u'ExtConfigrel', nameAlias=u'', key=u'ExIntfConfigRel', locked=u'no', cardinality=u'unspecified', targetName=u'externalInterface')
    vnsFolderInst19 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'InIntfConfigRelFolder', cardinality=u'unspecified', nameAlias=u'', name=u'IntConfig')
    vnsCfgRelInst7 = cobra.model.vns.CfgRelInst(vnsFolderInst19, mandatory=u'no', name=u'InConfigrel', nameAlias=u'', key=u'InIntfConfigRel', locked=u'no', cardinality=u'unspecified', targetName=u'internalIf')
    vnsFolderInst20 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'AccessPolicyFolder', cardinality=u'unspecified', nameAlias=u'', name=u'AccessPolicyFolder')
    vnsCfgRelInst8 = cobra.model.vns.CfgRelInst(vnsFolderInst20, mandatory=u'no', name=u'InAccessPolicyRel', nameAlias=u'', key=u'InAccessPolicyRel', locked=u'no', cardinality=u'unspecified', targetName=u'ftd-ngips-policy')
    vnsFolderInst21 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'BridgeGroupInterface', cardinality=u'unspecified', nameAlias=u'', name=u'BVI1')
    vnsParamInst14 = cobra.model.vns.ParamInst(vnsFolderInst21, validation=u'', mandatory=u'no', name=u'BridgeGroupId', nameAlias=u'', value=u'1', key=u'bridge_group_id', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst22 = cobra.model.vns.FolderInst(vnsFolderInst21, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'IPv4Config', cardinality=u'unspecified', nameAlias=u'', name=u'IPv4Config')
    vnsFolderInst23 = cobra.model.vns.FolderInst(vnsFolderInst22, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'static', cardinality=u'unspecified', nameAlias=u'', name=u'static')
    vnsParamInst15 = cobra.model.vns.ParamInst(vnsFolderInst23, validation=u'', mandatory=u'no', name=u'address', nameAlias=u'', value=str(n2BviIp), key=u'address', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst24 = cobra.model.vns.FolderInst(vnsFolderInst21, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'Interfaces', cardinality=u'unspecified', nameAlias=u'', name=u'Interfaces')
    vnsFolderInst25 = cobra.model.vns.FolderInst(vnsFolderInst24, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'Interface', cardinality=u'unspecified', nameAlias=u'', name=u'ExternalInterface')
    vnsCfgRelInst9 = cobra.model.vns.CfgRelInst(vnsFolderInst25, mandatory=u'no', name=u'InterfaceHolder', nameAlias=u'', key=u'InterfaceHolder', locked=u'no', cardinality=u'unspecified', targetName=u'externalInterface')
    vnsFolderInst26 = cobra.model.vns.FolderInst(vnsFolderInst24, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'Interface', cardinality=u'unspecified', nameAlias=u'', name=u'InternalInterface')
    vnsCfgRelInst10 = cobra.model.vns.CfgRelInst(vnsFolderInst26, mandatory=u'no', name=u'InterfaceHolder', nameAlias=u'', key=u'InterfaceHolder', locked=u'no', cardinality=u'unspecified', targetName=u'internalInterface')
    vnsFolderInst27 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'InIntfConfigRelFolder', cardinality=u'unspecified', nameAlias=u'', name=u'IntConfig')
    vnsCfgRelInst11 = cobra.model.vns.CfgRelInst(vnsFolderInst27, mandatory=u'no', name=u'InConfigrel', nameAlias=u'', key=u'InIntfConfigRel', locked=u'no', cardinality=u'unspecified', targetName=u'internalInterface')
    vnsFolderInst28 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'ExIntfConfigRelFolder', cardinality=u'unspecified', nameAlias=u'', name=u'ExtConfig')
    vnsCfgRelInst12 = cobra.model.vns.CfgRelInst(vnsFolderInst28, mandatory=u'no', name=u'ExtConfigrel', nameAlias=u'', key=u'ExIntfConfigRel', locked=u'no', cardinality=u'unspecified', targetName=u'externalIf')
    vnsFolderInst29 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'SecurityZone', cardinality=u'unspecified', nameAlias=u'', name=u'db')
    vnsParamInst16 = cobra.model.vns.ParamInst(vnsFolderInst29, validation=u'', mandatory=u'no', name=u'type', nameAlias=u'', value=u'SWITCHED', key=u'type', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst30 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'BridgeGroupIntf', cardinality=u'unspecified', nameAlias=u'', name=u'1')
    vnsFolderInst31 = cobra.model.vns.FolderInst(vnsFolderInst30, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'IPv4Address', cardinality=u'unspecified', nameAlias=u'', name=u'BviIP')
    vnsParamInst17 = cobra.model.vns.ParamInst(vnsFolderInst31, validation=u'', mandatory=u'no', name=u'IP', nameAlias=u'', value=str(n1BviIp), key=u'ipv4_address', locked=u'no', cardinality=u'unspecified')

    #External Interface
    vnsFolderInst32 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'Interface', cardinality=u'unspecified', nameAlias=u'', name=u'externalIf')
    vnsFolderInst33 = cobra.model.vns.FolderInst(vnsFolderInst32, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'', name=u'externalIfCfg')
    vnsParamInst18 = cobra.model.vns.ParamInst(vnsFolderInst33, validation=u'', mandatory=u'no', name=u'external_security_level', nameAlias=u'', value=u'50', key=u'security_level', locked=u'no', cardinality=u'unspecified')
    vnsCfgRelInst13 = cobra.model.vns.CfgRelInst(vnsFolderInst33, mandatory=u'no', name=u'extbridge', nameAlias=u'', key=u'bridge_group', locked=u'no', cardinality=u'unspecified', targetName=u'1')
    vnsFolderInst34 = cobra.model.vns.FolderInst(vnsFolderInst32, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'AccessGroup', cardinality=u'unspecified', nameAlias=u'', name=u'ExtAccessGroup')
    vnsCfgRelInst14 = cobra.model.vns.CfgRelInst(vnsFolderInst34, mandatory=u'no', name=u'name', nameAlias=u'', key=u'inbound_access_list_name', locked=u'no', cardinality=u'unspecified', targetName=u'access-list-inbound')
    vnsFolderInst35 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'BridgeInterfaceFolder', cardinality=u'unspecified', nameAlias=u'', name=u'BridgeInterfaceFolder')
    vnsCfgRelInst15 = cobra.model.vns.CfgRelInst(vnsFolderInst35, mandatory=u'no', name=u'InBridgeGroupInterfaceRel', nameAlias=u'', key=u'InBridgeGroupInterfaceRel', locked=u'no', cardinality=u'unspecified', targetName=u'BVI1')

    #Internal Interface
    vnsFolderInst36 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'Interface', cardinality=u'unspecified', nameAlias=u'', name=u'internalIf')
    vnsFolderInst37 = cobra.model.vns.FolderInst(vnsFolderInst36, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'', name=u'internalIfCfg')
    vnsParamInst19 = cobra.model.vns.ParamInst(vnsFolderInst37, validation=u'', mandatory=u'no', name=u'internal_security_level', nameAlias=u'', value=u'100', key=u'security_level', locked=u'no', cardinality=u'unspecified')
    vnsCfgRelInst16 = cobra.model.vns.CfgRelInst(vnsFolderInst37, mandatory=u'no', name=u'extbridge', nameAlias=u'', key=u'bridge_group', locked=u'no', cardinality=u'unspecified', targetName=u'1')
    vnsFolderInst38 = cobra.model.vns.FolderInst(vnsFolderInst36, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'AccessGroup', cardinality=u'unspecified', nameAlias=u'', name=u'IntAccessGroup')
    vnsCfgRelInst17 = cobra.model.vns.CfgRelInst(vnsFolderInst38, mandatory=u'no', name=u'name', nameAlias=u'', key=u'inbound_access_list_name', locked=u'no', cardinality=u'unspecified', targetName=u'access-list-inbound')

    fvRsProv = cobra.model.fv.RsProv(fvAEPg, tnVzBrCPName=u'web-to-db', matchT=u'AtleastOne', prio=u'unspecified')

    vzBrCP = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'web-to-db', descr=u'', targetDscp=u'unspecified', nameAlias=u'', ownerTag=u'', prio=u'unspecified')
    vzSubj = cobra.model.vz.Subj(vzBrCP, revFltPorts=u'yes', name=u'Subject', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjGraphAtt = cobra.model.vz.RsSubjGraphAtt(vzSubj, directives=u'', tnVnsAbsGraphName=u'asav-ftd-graph')
    vzRsSubjFiltAtt = cobra.model.vz.RsSubjFiltAtt(vzSubj, directives=u'', tnVzFilterName=u'default')

    #Create a Logical Device Context (a.k.a device selection policy) for N2
    vnsLDevCtx = cobra.model.vns.LDevCtx(fvTenant, context=u'', name=u'', descr=u'', ctrctNameOrLbl=u'web-to-db', nameAlias=u'', nodeNameOrLbl=u'N2', graphNameOrLbl=u'asav-ftd-graph')
    #Point the LDEvCtx to the Logical Device ftdv-ngips ??
    vnsRsLDevCtxToLDev = cobra.model.vns.RsLDevCtxToLDev(vnsLDevCtx, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-ftdv-ngips')
    #Get Logical Interface Context consumer ??
    vnsLIfCtx = cobra.model.vns.LIfCtx(vnsLDevCtx, permitLog=u'no', nameAlias=u'', name=u'', descr=u'', connNameOrLbl=u'consumer')
    #Map Logical interface Context to Logical Interface ??
    vnsRsLIfCtxToLIf = cobra.model.vns.RsLIfCtxToLIf(vnsLIfCtx, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-ftdv-ngips/lIf-web')
    #Map Logical Interface Context to BD ??
    vnsRsLIfCtxToBD = cobra.model.vns.RsLIfCtxToBD(vnsLIfCtx, tDn=u'uni/tn-'+str(tenantName)+r'/BD-asa-to-ftdv-BD')
    #Get Logical Interface Context provider
    vnsLIfCtx2 = cobra.model.vns.LIfCtx(vnsLDevCtx, permitLog=u'no', nameAlias=u'', name=u'', descr=u'', connNameOrLbl=u'provider')
    #Map Logical Interface Conect to Logical Interface ??
    vnsRsLIfCtxToLIf2 = cobra.model.vns.RsLIfCtxToLIf(vnsLIfCtx2, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-ftdv-ngips/lIf-db')
    #Map Logical Interface to BD
    vnsRsLIfCtxToBD2 = cobra.model.vns.RsLIfCtxToBD(vnsLIfCtx2, tDn=u'uni/tn-'+str(tenantName)+r'/BD-db-BD')

    #Create Logical Device Context for N1
    vnsLDevCtx2 = cobra.model.vns.LDevCtx(fvTenant, context=u'', name=u'', descr=u'', ctrctNameOrLbl=u'web-to-db', nameAlias=u'', nodeNameOrLbl=u'N1', graphNameOrLbl=u'asav-ftd-graph')
    #Point LDC to ASAv
    vnsRsLDevCtxToLDev2 = cobra.model.vns.RsLDevCtxToLDev(vnsLDevCtx2, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-ASAv')
    #Get Logical Interface consumer
    vnsLIfCtx3 = cobra.model.vns.LIfCtx(vnsLDevCtx2, permitLog=u'no', nameAlias=u'', name=u'', descr=u'', connNameOrLbl=u'consumer')
    #Map Logical Interface Context to Logical Interface ??
    vnsRsLIfCtxToLIf3 = cobra.model.vns.RsLIfCtxToLIf(vnsLIfCtx3, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-ASAv/lIf-web')
    #Map Logical Interface to BD ??
    #vnsRsLIfCtxToBD3 = cobra.model.vns.RsLIfCtxToBD(vnsLIfCtx3, tDn=u'uni/tn-common/BD-hybridcloud-inbound')
    vnsRsLIfCtxToBD3 = cobra.model.vns.RsLIfCtxToBD(vnsLIfCtx3, tDn=u'uni/tn-common/BD-securedc-inbound')
    #Get Logical Interface provider
    vnsLIfCtx4 = cobra.model.vns.LIfCtx(vnsLDevCtx2, permitLog=u'no', nameAlias=u'', name=u'', descr=u'', connNameOrLbl=u'provider')
    #Map Logical Interface Context to Logical Interface
    vnsRsLIfCtxToLIf4 = cobra.model.vns.RsLIfCtxToLIf(vnsLIfCtx4, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-ASAv/lIf-db')
    #Map Logical Interface Conext to BD
    vnsRsLIfCtxToBD4 = cobra.model.vns.RsLIfCtxToBD(vnsLIfCtx4, tDn=u'uni/tn-'+str(tenantName)+r'/BD-asa-to-ftdv-BD')

    # commit the generated code to APIC
    #print toXMLStr(fvAp)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)



def sdcFtdv2AsavServiceGraphApplyV2(md, tenantName, networkObjectIp='1.1.1.1/32'):

    n1BviIp=r"10.150.3.200/24"
    n2BviIp=r"10.150.3.201/24"

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)
    fvAp = cobra.model.fv.Ap(fvTenant, 'CloudCenter-Apps')


    fvAEPgWeb = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Web', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    fvRsConsWeb = cobra.model.fv.RsCons(fvAEPgWeb, tnVzBrCPName=u'web-to-db', prio=u'unspecified')

    fvAEPg = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Db', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')

    vnsFolderInst = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'Interface', cardinality=u'unspecified', nameAlias=u'', name=u'internalIf')
    vnsFolderInst2 = cobra.model.vns.FolderInst(vnsFolderInst, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'', name=u'internalIfCfg')
    vnsParamInst = cobra.model.vns.ParamInst(vnsFolderInst2, validation=u'', mandatory=u'no', name=u'internal_security_level', nameAlias=u'', value=u'100', key=u'security_level', locked=u'no', cardinality=u'unspecified')
    vnsCfgRelInst = cobra.model.vns.CfgRelInst(vnsFolderInst2, mandatory=u'no', name=u'intbridge', nameAlias=u'', key=u'bridge_group', locked=u'no', cardinality=u'unspecified', targetName=u'1')
    vnsFolderInst3 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'', name=u'externalInterface')
    vnsParamInst2 = cobra.model.vns.ParamInst(vnsFolderInst3, validation=u'', mandatory=u'no', name=u'ifname', nameAlias=u'', value=u'Consumer', key=u'ifname', locked=u'no', cardinality=u'unspecified')
    vnsParamInst3 = cobra.model.vns.ParamInst(vnsFolderInst3, validation=u'', mandatory=u'no', name=u'enabled', nameAlias=u'', value=u'true', key=u'enabled', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst4 = cobra.model.vns.FolderInst(vnsFolderInst3, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'int_security_zone', cardinality=u'unspecified', nameAlias=u'', name=u'int_security_zone')
    vnsCfgRelInst2 = cobra.model.vns.CfgRelInst(vnsFolderInst4, mandatory=u'no', name=u'security_zone', nameAlias=u'', key=u'security_zone', locked=u'no', cardinality=u'unspecified', targetName=u'web')
    vnsFolderInst5 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'', name=u'internalInterface')
    vnsParamInst4 = cobra.model.vns.ParamInst(vnsFolderInst5, validation=u'', mandatory=u'no', name=u'ifname', nameAlias=u'', value=u'Provider', key=u'ifname', locked=u'no', cardinality=u'unspecified')
    vnsParamInst5 = cobra.model.vns.ParamInst(vnsFolderInst5, validation=u'', mandatory=u'no', name=u'enabled', nameAlias=u'', value=u'true', key=u'enabled', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst6 = cobra.model.vns.FolderInst(vnsFolderInst5, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'int_security_zone', cardinality=u'unspecified', nameAlias=u'', name=u'int_security_zone')
    vnsCfgRelInst3 = cobra.model.vns.CfgRelInst(vnsFolderInst6, mandatory=u'no', name=u'security_zone', nameAlias=u'', key=u'security_zone', locked=u'no', cardinality=u'unspecified', targetName=u'db')
    vnsFolderInst7 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'NetworkObject', cardinality=u'unspecified', nameAlias=u'', name=u'web_server')
    vnsParamInst6 = cobra.model.vns.ParamInst(vnsFolderInst7, validation=u'', mandatory=u'no', name=u'network_ip_address', nameAlias=u'', value=str(networkObjectIp), key=u'network_ip_address', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst8 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'AccessPolicy', cardinality=u'unspecified', nameAlias=u'', name=u'ftd-ngips-policy')
    vnsFolderInst9 = cobra.model.vns.FolderInst(vnsFolderInst8, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'AccessRule', cardinality=u'unspecified', nameAlias=u'', name=u'ftd-rule1')
    vnsParamInst7 = cobra.model.vns.ParamInst(vnsFolderInst9, validation=u'', mandatory=u'no', name=u'Bi-Directional', nameAlias=u'', value=u'true', key=u'bidirectional', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst10 = cobra.model.vns.FolderInst(vnsFolderInst9, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'AccDestinationZones', cardinality=u'unspecified', nameAlias=u'', name=u'AccDestinationZones')
    vnsCfgRelInst4 = cobra.model.vns.CfgRelInst(vnsFolderInst10, mandatory=u'no', name=u'DestinationZone', nameAlias=u'', key=u'DestinationZones', locked=u'no', cardinality=u'unspecified', targetName=u'internalInterface/int_security_zone')
    vnsFolderInst11 = cobra.model.vns.FolderInst(vnsFolderInst9, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'AccSourceZones', cardinality=u'unspecified', nameAlias=u'', name=u'AccSourceZones')
    vnsCfgRelInst5 = cobra.model.vns.CfgRelInst(vnsFolderInst11, mandatory=u'no', name=u'SourceZone', nameAlias=u'', key=u'SourceZones', locked=u'no', cardinality=u'unspecified', targetName=u'externalInterface/int_security_zone')
    vnsFolderInst12 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'AccessList', cardinality=u'unspecified', nameAlias=u'', name=u'access-list-inbound')
    vnsFolderInst13 = cobra.model.vns.FolderInst(vnsFolderInst12, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'AccessControlEntry', cardinality=u'unspecified', nameAlias=u'', name=u'permit-any')
    vnsParamInst8 = cobra.model.vns.ParamInst(vnsFolderInst13, validation=u'', mandatory=u'no', name=u'action-permit', nameAlias=u'', value=u'permit', key=u'action', locked=u'no', cardinality=u'unspecified')
    vnsParamInst9 = cobra.model.vns.ParamInst(vnsFolderInst13, validation=u'', mandatory=u'no', name=u'order1', nameAlias=u'', value=u'10', key=u'order', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst14 = cobra.model.vns.FolderInst(vnsFolderInst13, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'source_address', cardinality=u'unspecified', nameAlias=u'', name=u'src-address')
    vnsParamInst10 = cobra.model.vns.ParamInst(vnsFolderInst14, validation=u'', mandatory=u'no', name=u'any', nameAlias=u'', value=u'any', key=u'any', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst15 = cobra.model.vns.FolderInst(vnsFolderInst13, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'destination_address', cardinality=u'unspecified', nameAlias=u'', name=u'dest-address')
    vnsParamInst11 = cobra.model.vns.ParamInst(vnsFolderInst15, validation=u'', mandatory=u'no', name=u'any', nameAlias=u'', value=u'any', key=u'any', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst16 = cobra.model.vns.FolderInst(vnsFolderInst13, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'protocol', cardinality=u'unspecified', nameAlias=u'', name=u'ip')
    vnsParamInst12 = cobra.model.vns.ParamInst(vnsFolderInst16, validation=u'', mandatory=u'no', name=u'ip', nameAlias=u'', value=u'ip', key=u'name_number', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst17 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'SecurityZone', cardinality=u'unspecified', nameAlias=u'', name=u'web')
    vnsParamInst13 = cobra.model.vns.ParamInst(vnsFolderInst17, validation=u'', mandatory=u'no', name=u'type', nameAlias=u'', value=u'SWITCHED', key=u'type', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst18 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'ExIntfConfigRelFolder', cardinality=u'unspecified', nameAlias=u'', name=u'ExtConfig')
    vnsCfgRelInst6 = cobra.model.vns.CfgRelInst(vnsFolderInst18, mandatory=u'no', name=u'ExtConfigrel', nameAlias=u'', key=u'ExIntfConfigRel', locked=u'no', cardinality=u'unspecified', targetName=u'externalInterface')
    vnsFolderInst19 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'InIntfConfigRelFolder', cardinality=u'unspecified', nameAlias=u'', name=u'IntConfig')
    vnsCfgRelInst7 = cobra.model.vns.CfgRelInst(vnsFolderInst19, mandatory=u'no', name=u'InConfigrel', nameAlias=u'', key=u'InIntfConfigRel', locked=u'no', cardinality=u'unspecified', targetName=u'internalIf')
    vnsFolderInst20 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'AccessPolicyFolder', cardinality=u'unspecified', nameAlias=u'', name=u'AccessPolicyFolder')
    vnsCfgRelInst8 = cobra.model.vns.CfgRelInst(vnsFolderInst20, mandatory=u'no', name=u'InAccessPolicyRel', nameAlias=u'', key=u'InAccessPolicyRel', locked=u'no', cardinality=u'unspecified', targetName=u'ftd-ngips-policy')
    vnsFolderInst21 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'BridgeGroupInterface', cardinality=u'unspecified', nameAlias=u'', name=u'BVI1')
    vnsParamInst14 = cobra.model.vns.ParamInst(vnsFolderInst21, validation=u'', mandatory=u'no', name=u'BridgeGroupId', nameAlias=u'', value=u'1', key=u'bridge_group_id', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst22 = cobra.model.vns.FolderInst(vnsFolderInst21, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'IPv4Config', cardinality=u'unspecified', nameAlias=u'', name=u'IPv4Config')
    vnsFolderInst23 = cobra.model.vns.FolderInst(vnsFolderInst22, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'static', cardinality=u'unspecified', nameAlias=u'', name=u'static')
    vnsParamInst15 = cobra.model.vns.ParamInst(vnsFolderInst23, validation=u'', mandatory=u'no', name=u'address', nameAlias=u'', value=str(n2BviIp), key=u'address', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst24 = cobra.model.vns.FolderInst(vnsFolderInst21, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'Interfaces', cardinality=u'unspecified', nameAlias=u'', name=u'Interfaces')
    vnsFolderInst25 = cobra.model.vns.FolderInst(vnsFolderInst24, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'Interface', cardinality=u'unspecified', nameAlias=u'', name=u'ExternalInterface')
    vnsCfgRelInst9 = cobra.model.vns.CfgRelInst(vnsFolderInst25, mandatory=u'no', name=u'InterfaceHolder', nameAlias=u'', key=u'InterfaceHolder', locked=u'no', cardinality=u'unspecified', targetName=u'externalInterface')
    vnsFolderInst26 = cobra.model.vns.FolderInst(vnsFolderInst24, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'Interface', cardinality=u'unspecified', nameAlias=u'', name=u'InternalInterface')
    vnsCfgRelInst10 = cobra.model.vns.CfgRelInst(vnsFolderInst26, mandatory=u'no', name=u'InterfaceHolder', nameAlias=u'', key=u'InterfaceHolder', locked=u'no', cardinality=u'unspecified', targetName=u'internalInterface')
    vnsFolderInst27 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'InIntfConfigRelFolder', cardinality=u'unspecified', nameAlias=u'', name=u'IntConfig')
    vnsCfgRelInst11 = cobra.model.vns.CfgRelInst(vnsFolderInst27, mandatory=u'no', name=u'InConfigrel', nameAlias=u'', key=u'InIntfConfigRel', locked=u'no', cardinality=u'unspecified', targetName=u'internalInterface')
    vnsFolderInst28 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'ExIntfConfigRelFolder', cardinality=u'unspecified', nameAlias=u'', name=u'ExtConfig')
    vnsCfgRelInst12 = cobra.model.vns.CfgRelInst(vnsFolderInst28, mandatory=u'no', name=u'ExtConfigrel', nameAlias=u'', key=u'ExIntfConfigRel', locked=u'no', cardinality=u'unspecified', targetName=u'externalIf')
    vnsFolderInst29 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'SecurityZone', cardinality=u'unspecified', nameAlias=u'', name=u'db')
    vnsParamInst16 = cobra.model.vns.ParamInst(vnsFolderInst29, validation=u'', mandatory=u'no', name=u'type', nameAlias=u'', value=u'SWITCHED', key=u'type', locked=u'no', cardinality=u'unspecified')
    vnsFolderInst30 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'BridgeGroupIntf', cardinality=u'unspecified', nameAlias=u'', name=u'1')
    vnsFolderInst31 = cobra.model.vns.FolderInst(vnsFolderInst30, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'IPv4Address', cardinality=u'unspecified', nameAlias=u'', name=u'BviIP')
    vnsParamInst17 = cobra.model.vns.ParamInst(vnsFolderInst31, validation=u'', mandatory=u'no', name=u'IP', nameAlias=u'', value=str(n1BviIp), key=u'ipv4_address', locked=u'no', cardinality=u'unspecified')

    #External Interface
    vnsFolderInst32 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'Interface', cardinality=u'unspecified', nameAlias=u'', name=u'externalIf')
    vnsFolderInst33 = cobra.model.vns.FolderInst(vnsFolderInst32, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'', name=u'externalIfCfg')
    vnsParamInst18 = cobra.model.vns.ParamInst(vnsFolderInst33, validation=u'', mandatory=u'no', name=u'external_security_level', nameAlias=u'', value=u'50', key=u'security_level', locked=u'no', cardinality=u'unspecified')
    vnsCfgRelInst13 = cobra.model.vns.CfgRelInst(vnsFolderInst33, mandatory=u'no', name=u'extbridge', nameAlias=u'', key=u'bridge_group', locked=u'no', cardinality=u'unspecified', targetName=u'1')
    vnsFolderInst34 = cobra.model.vns.FolderInst(vnsFolderInst32, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'AccessGroup', cardinality=u'unspecified', nameAlias=u'', name=u'ExtAccessGroup')
    vnsCfgRelInst14 = cobra.model.vns.CfgRelInst(vnsFolderInst34, mandatory=u'no', name=u'name', nameAlias=u'', key=u'inbound_access_list_name', locked=u'no', cardinality=u'unspecified', targetName=u'access-list-inbound')
    vnsFolderInst35 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N2', key=u'BridgeInterfaceFolder', cardinality=u'unspecified', nameAlias=u'', name=u'BridgeInterfaceFolder')
    vnsCfgRelInst15 = cobra.model.vns.CfgRelInst(vnsFolderInst35, mandatory=u'no', name=u'InBridgeGroupInterfaceRel', nameAlias=u'', key=u'InBridgeGroupInterfaceRel', locked=u'no', cardinality=u'unspecified', targetName=u'BVI1')

    #Internal Interface
    vnsFolderInst36 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'Interface', cardinality=u'unspecified', nameAlias=u'', name=u'internalIf')
    vnsFolderInst37 = cobra.model.vns.FolderInst(vnsFolderInst36, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'', name=u'internalIfCfg')
    vnsParamInst19 = cobra.model.vns.ParamInst(vnsFolderInst37, validation=u'', mandatory=u'no', name=u'internal_security_level', nameAlias=u'', value=u'100', key=u'security_level', locked=u'no', cardinality=u'unspecified')
    vnsCfgRelInst16 = cobra.model.vns.CfgRelInst(vnsFolderInst37, mandatory=u'no', name=u'extbridge', nameAlias=u'', key=u'bridge_group', locked=u'no', cardinality=u'unspecified', targetName=u'1')
    vnsFolderInst38 = cobra.model.vns.FolderInst(vnsFolderInst36, locked=u'no', graphNameOrLbl=u'asav-ftd-graph', devCtxLbl=u'', ctrctNameOrLbl=u'web-to-db', scopedBy=u'epg', nodeNameOrLbl=u'N1', key=u'AccessGroup', cardinality=u'unspecified', nameAlias=u'', name=u'IntAccessGroup')
    vnsCfgRelInst17 = cobra.model.vns.CfgRelInst(vnsFolderInst38, mandatory=u'no', name=u'name', nameAlias=u'', key=u'inbound_access_list_name', locked=u'no', cardinality=u'unspecified', targetName=u'access-list-inbound')

    fvRsProv = cobra.model.fv.RsProv(fvAEPg, tnVzBrCPName=u'web-to-db', matchT=u'AtleastOne', prio=u'unspecified')

    vzBrCP = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'web-to-db', descr=u'', targetDscp=u'unspecified', nameAlias=u'', ownerTag=u'', prio=u'unspecified')
    vzSubj = cobra.model.vz.Subj(vzBrCP, revFltPorts=u'yes', name=u'Subject', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjGraphAtt = cobra.model.vz.RsSubjGraphAtt(vzSubj, directives=u'', tnVnsAbsGraphName=u'asav-ftd-graph')
    vzRsSubjFiltAtt = cobra.model.vz.RsSubjFiltAtt(vzSubj, directives=u'', tnVzFilterName=u'default')

    #Create a Logical Device Context (a.k.a device selection policy) for N2
    vnsLDevCtx = cobra.model.vns.LDevCtx(fvTenant, context=u'', name=u'', descr=u'', ctrctNameOrLbl=u'web-to-db', nameAlias=u'', nodeNameOrLbl=u'N2', graphNameOrLbl=u'asav-ftd-graph')
    #Point the LDEvCtx to the Logical Device ftdv-ngips ??
    vnsRsLDevCtxToLDev = cobra.model.vns.RsLDevCtxToLDev(vnsLDevCtx, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-ftdv-ngips')
    #Get Logical Interface Context consumer ??
    vnsLIfCtx = cobra.model.vns.LIfCtx(vnsLDevCtx, permitLog=u'no', nameAlias=u'', name=u'', descr=u'', connNameOrLbl=u'consumer')
    #Map Logical interface Context to Logical Interface ??
    vnsRsLIfCtxToLIf = cobra.model.vns.RsLIfCtxToLIf(vnsLIfCtx, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-ftdv-ngips/lIf-web')
    #Map Logical Interface Context to BD ??
    vnsRsLIfCtxToBD = cobra.model.vns.RsLIfCtxToBD(vnsLIfCtx, tDn=u'uni/tn-'+str(tenantName)+r'/BD-asa-to-ftdv-BD')
    #Get Logical Interface Context provider
    vnsLIfCtx2 = cobra.model.vns.LIfCtx(vnsLDevCtx, permitLog=u'no', nameAlias=u'', name=u'', descr=u'', connNameOrLbl=u'provider')
    #Map Logical Interface Conect to Logical Interface ??
    vnsRsLIfCtxToLIf2 = cobra.model.vns.RsLIfCtxToLIf(vnsLIfCtx2, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-ftdv-ngips/lIf-db')
    #Map Logical Interface to BD
    vnsRsLIfCtxToBD2 = cobra.model.vns.RsLIfCtxToBD(vnsLIfCtx2, tDn=u'uni/tn-'+str(tenantName)+r'/BD-db-BD')

    #Create Logical Device Context for N1
    vnsLDevCtx2 = cobra.model.vns.LDevCtx(fvTenant, context=u'', name=u'', descr=u'', ctrctNameOrLbl=u'web-to-db', nameAlias=u'', nodeNameOrLbl=u'N1', graphNameOrLbl=u'asav-ftd-graph')
    #Point LDC to ASAv
    vnsRsLDevCtxToLDev2 = cobra.model.vns.RsLDevCtxToLDev(vnsLDevCtx2, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-ASAv')
    #Get Logical Interface consumer
    vnsLIfCtx3 = cobra.model.vns.LIfCtx(vnsLDevCtx2, permitLog=u'no', nameAlias=u'', name=u'', descr=u'', connNameOrLbl=u'consumer')
    #Map Logical Interface Context to Logical Interface ??
    vnsRsLIfCtxToLIf3 = cobra.model.vns.RsLIfCtxToLIf(vnsLIfCtx3, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-ASAv/lIf-web')
    #Map Logical Interface to BD ??
    vnsRsLIfCtxToBD3 = cobra.model.vns.RsLIfCtxToBD(vnsLIfCtx3, tDn=u'uni/tn-'+str(tenantName)+r'/BD-web-BD')
    #Get Logical Interface provider
    vnsLIfCtx4 = cobra.model.vns.LIfCtx(vnsLDevCtx2, permitLog=u'no', nameAlias=u'', name=u'', descr=u'', connNameOrLbl=u'provider')
    #Map Logical Interface Context to Logical Interface
    vnsRsLIfCtxToLIf4 = cobra.model.vns.RsLIfCtxToLIf(vnsLIfCtx4, tDn=u'uni/tn-'+str(tenantName)+r'/lDevVip-ASAv/lIf-db')
    #Map Logical Interface Conext to BD
    vnsRsLIfCtxToBD4 = cobra.model.vns.RsLIfCtxToBD(vnsLIfCtx4, tDn=u'uni/tn-'+str(tenantName)+r'/BD-asa-to-ftdv-BD')

    # commit the generated code to APIC
    #print toXMLStr(fvAp)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)



def sdcAddStaticRoutesToWeboutEpg(md, tenantName, attackerIp=u'10.150.1.100', internetSite=u'222.29.197.232/32', webToDbSubnet=u'10.150.3.0/24' , webOutIp=u'10.150.2.44' ):

    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)
    fvAp = cobra.model.fv.Ap(fvTenant, 'CloudCenter-Apps')

    # build the request using cobra syntax
    fvAEPg = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Web-Out', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')

    vnsFolderInst9 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'l3fw-ftdv-graph', devCtxLbl=u'', ctrctNameOrLbl=u'attacker-to-webout', scopedBy=u'epg', nodeNameOrLbl=u'l3fw', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'', name=u'internalInterface')
    vnsFolderInst13 = cobra.model.vns.FolderInst(vnsFolderInst9, locked=u'no', graphNameOrLbl=u'l3fw-ftdv-graph', devCtxLbl=u'', ctrctNameOrLbl=u'attacker-to-webout', scopedBy=u'epg', nodeNameOrLbl=u'l3fw', key=u'StaticRoute', cardinality=u'unspecified', nameAlias=u'', name=u'StaticRoute')
    vnsFolderInst14 = cobra.model.vns.FolderInst(vnsFolderInst13, locked=u'no', graphNameOrLbl=u'l3fw-ftdv-graph', devCtxLbl=u'', ctrctNameOrLbl=u'attacker-to-webout', scopedBy=u'epg', nodeNameOrLbl=u'l3fw', key=u'IPv4StaticRoute', cardinality=u'unspecified', nameAlias=u'', name=u'IPv4StaticRoute')
    vnsParamInst8 = cobra.model.vns.ParamInst(vnsFolderInst14, validation=u'', mandatory=u'no', name=u'gateway', nameAlias=u'', value=webOutIp, key=u'gateway', locked=u'no', cardinality=u'unspecified')
    vnsParamInst9 = cobra.model.vns.ParamInst(vnsFolderInst14, validation=u'', mandatory=u'no', name=u'network', nameAlias=u'', value=webToDbSubnet, key=u'network', locked=u'no', cardinality=u'unspecified')
    vnsParamInst10 = cobra.model.vns.ParamInst(vnsFolderInst14, validation=u'', mandatory=u'no', name=u'isTunneled', nameAlias=u'', value=u'false', key=u'isTunneled', locked=u'no', cardinality=u'unspecified')
    vnsParamInst11 = cobra.model.vns.ParamInst(vnsFolderInst14, validation=u'', mandatory=u'no', name=u'metric', nameAlias=u'', value=u'1', key=u'metric', locked=u'no', cardinality=u'unspecified')
    vnsParamInst12 = cobra.model.vns.ParamInst(vnsFolderInst9, validation=u'', mandatory=u'no', name=u'enabled', nameAlias=u'', value=u'true', key=u'enabled', locked=u'no', cardinality=u'unspecified')
    vnsParamInst13 = cobra.model.vns.ParamInst(vnsFolderInst9, validation=u'', mandatory=u'no', name=u'ifname', nameAlias=u'', value=u'webout', key=u'ifname', locked=u'no', cardinality=u'unspecified')

    vnsFolderInst46 = cobra.model.vns.FolderInst(fvAEPg, locked=u'no', graphNameOrLbl=u'l3fw-ftdv-graph', devCtxLbl=u'', ctrctNameOrLbl=u'attacker-to-webout', scopedBy=u'epg', nodeNameOrLbl=u'l3fw', key=u'InterfaceConfig', cardinality=u'unspecified', nameAlias=u'', name=u'externalInterface')
    vnsFolderInst50 = cobra.model.vns.FolderInst(vnsFolderInst46, locked=u'no', graphNameOrLbl=u'l3fw-ftdv-graph', devCtxLbl=u'', ctrctNameOrLbl=u'attacker-to-webout', scopedBy=u'epg', nodeNameOrLbl=u'l3fw', key=u'StaticRoute', cardinality=u'unspecified', nameAlias=u'', name=u'StaticRoute')
    vnsFolderInst51 = cobra.model.vns.FolderInst(vnsFolderInst50, locked=u'no', graphNameOrLbl=u'l3fw-ftdv-graph', devCtxLbl=u'', ctrctNameOrLbl=u'attacker-to-webout', scopedBy=u'epg', nodeNameOrLbl=u'l3fw', key=u'IPv4StaticRoute', cardinality=u'unspecified', nameAlias=u'', name=u'IPv4StaticRoute')
    vnsParamInst29 = cobra.model.vns.ParamInst(vnsFolderInst51, validation=u'', mandatory=u'no', name=u'isTunneled', nameAlias=u'', value=u'false', key=u'isTunneled', locked=u'no', cardinality=u'unspecified')
    vnsParamInst30 = cobra.model.vns.ParamInst(vnsFolderInst51, validation=u'', mandatory=u'no', name=u'network', nameAlias=u'', value=internetSite, key=u'network', locked=u'no', cardinality=u'unspecified')
    vnsParamInst31 = cobra.model.vns.ParamInst(vnsFolderInst51, validation=u'', mandatory=u'no', name=u'gateway', nameAlias=u'', value=attackerIp, key=u'gateway', locked=u'no', cardinality=u'unspecified')
    vnsParamInst32 = cobra.model.vns.ParamInst(vnsFolderInst51, validation=u'', mandatory=u'no', name=u'metric', nameAlias=u'', value=u'1', key=u'metric', locked=u'no', cardinality=u'unspecified')
    vnsParamInst33 = cobra.model.vns.ParamInst(vnsFolderInst46, validation=u'', mandatory=u'no', name=u'enabled', nameAlias=u'', value=u'true', key=u'enabled', locked=u'no', cardinality=u'unspecified')
    vnsParamInst34 = cobra.model.vns.ParamInst(vnsFolderInst46, validation=u'', mandatory=u'no', name=u'ifname', nameAlias=u'', value=u'attacker', key=u'ifname', locked=u'no', cardinality=u'unspecified')

    # commit the generated code to APIC
    #print toXMLStr(fvAp)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)




def sdcCreateTenant(md, tenantName='tenantName'):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')


    # build the request using cobra syntax
    fvTenant = cobra.model.fv.Tenant(polUni, ownerKey=u'', name=tenantName, descr=u'', nameAlias=u'', ownerTag=u'')

    aaaDomainRef = cobra.model.aaa.DomainRef(fvTenant, ownerKey=u'', ownerTag=u'', name=u'SecDo-'+tenantName, descr=u'', nameAlias=u'')

    vnsAbsFuncProfContr = cobra.model.vns.AbsFuncProfContr(fvTenant, ownerKey=u'', ownerTag=u'', name=u'', descr=u'', nameAlias=u'')

    fvAp = cobra.model.fv.Ap(fvTenant, ownerKey=u'', name=u'CloudCenter-Apps', prio=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')

    fvAEPg = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Web', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    #fvRsBd = cobra.model.fv.RsBd(fvAEPg, tnFvBDName=u'hybridcloud-inbound')
    fvRsBd = cobra.model.fv.RsBd(fvAEPg, tnFvBDName=u'securedc-inbound')

    fvRsCustQosPol = cobra.model.fv.RsCustQosPol(fvAEPg, tnQosCustomPolName=u'')
    fvRsCons = cobra.model.fv.RsCons(fvAEPg, tnVzBrCPName=u'L3_Out_default', prio=u'unspecified')
    #fvRsCons2 = cobra.model.fv.RsCons(fvAEPg, tnVzBrCPName=u'db-web', prio=u'unspecified')
    fvRsDomAtt = cobra.model.fv.RsDomAtt(fvAEPg, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')
    vmmSecP = cobra.model.vmm.SecP(fvRsDomAtt, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'accept', nameAlias=u'', ownerTag=u'', allowPromiscuous=u'accept', macChanges=u'accept')

    fvAEPg2 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Web-Out', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    fvRsBd2 = cobra.model.fv.RsBd(fvAEPg2, tnFvBDName=u'App-BD')
    fvRsCustQosPol2 = cobra.model.fv.RsCustQosPol(fvAEPg2, tnQosCustomPolName=u'')
    fvRsDomAtt2 = cobra.model.fv.RsDomAtt(fvAEPg2, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')
    vmmSecP2 = cobra.model.vmm.SecP(fvRsDomAtt2, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'accept', nameAlias=u'', ownerTag=u'', allowPromiscuous=u'accept', macChanges=u'accept')


    fvAEPg3 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Db', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    #fvRsBd3 = cobra.model.fv.RsBd(fvAEPg3, tnFvBDName=u'hybridcloud-inbound')
    #fvRsBd3 = cobra.model.fv.RsBd(fvAEPg3, tnFvBDName=u'securedc-db')
    #fvRsBd3 = cobra.model.fv.RsBd(fvAEPg3, tnFvBDName=tenantName+'-db')
    fvRsBd3 = cobra.model.fv.RsBd(fvAEPg3, tnFvBDName=r'db-BD')
    fvRsCustQosPol3 = cobra.model.fv.RsCustQosPol(fvAEPg3, tnQosCustomPolName=u'')
    fvRsCons3 = cobra.model.fv.RsCons(fvAEPg3, tnVzBrCPName=u'L3_Out_default', prio=u'unspecified')
    fvRsDomAtt3 = cobra.model.fv.RsDomAtt(fvAEPg3, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')
    vmmSecP3 = cobra.model.vmm.SecP(fvRsDomAtt3, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'accept', nameAlias=u'', ownerTag=u'', allowPromiscuous=u'accept', macChanges=u'accept')
    #fvRsProv = cobra.model.fv.RsProv(fvAEPg3, tnVzBrCPName=u'db-web', matchT=u'AtleastOne', prio=u'unspecified')

    fvAEPg4 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Attacker', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    fvRsBd4 = cobra.model.fv.RsBd(fvAEPg4, tnFvBDName=u'Attacker-BD')
    fvRsCustQosPol4 = cobra.model.fv.RsCustQosPol(fvAEPg4, tnQosCustomPolName=u'')
    fvRsDomAtt4 = cobra.model.fv.RsDomAtt(fvAEPg4, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')
    vmmSecP4 = cobra.model.vmm.SecP(fvRsDomAtt4, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'accept', nameAlias=u'', ownerTag=u'', allowPromiscuous=u'accept', macChanges=u'accept')


    fvAp2 = cobra.model.fv.Ap(fvTenant, ownerKey=u'', name=u'secureDC', prio=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')

    fvAEPg5 = cobra.model.fv.AEPg(fvAp2, isAttrBasedEPg=u'yes', matchT=u'AtleastOne', name=u'securedc-useg', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    fvCrtrn = cobra.model.fv.Crtrn(fvAEPg5, ownerKey=u'', name=u'default', descr=u'', prec=u'0', nameAlias=u'', ownerTag=u'', match=u'any')
    fvIpAttr = cobra.model.fv.IpAttr(fvCrtrn, ownerKey=u'', name=u'0', descr=u'', ip=u'10.150.1.0/24', nameAlias=u'', ownerTag=u'', usefvSubnet=u'no')
    fvRsBd5 = cobra.model.fv.RsBd(fvAEPg5, tnFvBDName=u'App-BD')
    fvRsCustQosPol5 = cobra.model.fv.RsCustQosPol(fvAEPg5, tnQosCustomPolName=u'')
    fvRsDomAtt5 = cobra.model.fv.RsDomAtt(fvAEPg5, tDn=u'uni/phys-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')


    fvAEPg6 = cobra.model.fv.AEPg(fvAp2, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'device-mgmt', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    fvRsBd6 = cobra.model.fv.RsBd(fvAEPg6, tnFvBDName=u'secureDc-BD')
    fvRsCustQosPol6 = cobra.model.fv.RsCustQosPol(fvAEPg6, tnQosCustomPolName=u'')
    fvRsDomAtt6 = cobra.model.fv.RsDomAtt(fvAEPg6, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')
    vmmSecP5 = cobra.model.vmm.SecP(fvRsDomAtt6, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'accept', nameAlias=u'', ownerTag=u'', allowPromiscuous=u'accept', macChanges=u'accept')
    fvRsProv2 = cobra.model.fv.RsProv(fvAEPg6, tnVzBrCPName=u'all', matchT=u'AtleastOne', prio=u'unspecified')
    vnsAddrInst = cobra.model.vns.AddrInst(fvAEPg6, ownerKey=u'', addr=u'10.150.0.2/24', descr=u'', nameAlias=u'', ownerTag=u'', name=u'inbandMgmtForL4L7Devices')
    fvnsUcastAddrBlk = cobra.model.fvns.UcastAddrBlk(vnsAddrInst, to=u'10.150.0.243', from_=u'10.150.0.241', name=u'', descr=u'', nameAlias=u'')
    fvRsTenantMonPol = cobra.model.fv.RsTenantMonPol(fvTenant, tnMonEPGPolName=u'')
    vzFilter = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'cliqr-firewall_sdc-opencart-test-app_Apache_3004_5', descr=u'', nameAlias=u'')
    vzEntry = cobra.model.vz.Entry(vzFilter, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'http', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'tcp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'http', name=u'TCP_80_80')
    vzFilter2 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'icmp', descr=u'', nameAlias=u'')
    vzEntry2 = cobra.model.vz.Entry(vzFilter2, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'unspecified', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'icmp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'unspecified', name=u'icmp')
    vzFilter3 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'allow_all', descr=u'', nameAlias=u'')
    vzEntry3 = cobra.model.vz.Entry(vzFilter3, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'unspecified', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'unspecified', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'unspecified', dFromPort=u'unspecified', name=u'allow_all')
    vzFilter4 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'cliqr-firewall_sdc-opencart-test-app_Database_3004_5', descr=u'', nameAlias=u'')
    vzEntry4 = cobra.model.vz.Entry(vzFilter4, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'3306', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'tcp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'3306', name=u'TCP_3306_3306')
    vzFilter5 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'mysql_icmp_ssh', descr=u'', nameAlias=u'')
    vzEntry5 = cobra.model.vz.Entry(vzFilter5, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'3306', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'tcp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'3306', name=u'mysql')
    vzEntry6 = cobra.model.vz.Entry(vzFilter5, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'22', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'tcp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'22', name=u'ssh')
    vzEntry7 = cobra.model.vz.Entry(vzFilter5, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'unspecified', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'icmp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'unspecified', name=u'icmp')
    vzFilter6 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'port_80', descr=u'', nameAlias=u'')
    vzFilter7 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'web-load_Filter', descr=u'', nameAlias=u'')
    vzFilter8 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'allow_all_Filter', descr=u'', nameAlias=u'')
    vzFilter9 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'port_3306', descr=u'', nameAlias=u'')
    vzEntry8 = cobra.model.vz.Entry(vzFilter9, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'3306', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'tcp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'3306', name=u'web_db')





    fvBD = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'secureDc-BD', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'no', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
    fvSubnet = cobra.model.fv.Subnet(fvBD, name=u'', descr=u'', ctrl=u'', ip=u'10.150.0.2/24', preferred=u'no', virtual=u'no', nameAlias=u'')
    fvRsIgmpsn = cobra.model.fv.RsIgmpsn(fvBD, tnIgmpSnoopPolName=u'')
    fvRsCtx = cobra.model.fv.RsCtx(fvBD, tnFvCtxName=u'secureDc-VRF')
    fvRsBdToEpRet = cobra.model.fv.RsBdToEpRet(fvBD, resolveAct=u'resolve', tnFvEpRetPolName=u'') 
    fvRsBDToNdP = cobra.model.fv.RsBDToNdP(fvBD, tnNdIfPolName=u'')


    fvBD2 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'garp', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'App-BD', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'yes', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
    igmpIfP = cobra.model.igmp.IfP(fvBD2, name=u'', descr=u'', nameAlias=u'')
    fvSubnet2 = cobra.model.fv.Subnet(fvBD2, name=u'', descr=u'', ctrl=u'', ip=u'10.150.2.254/24', preferred=u'no', virtual=u'no', nameAlias=u'')
    fvRsIgmpsn2 = cobra.model.fv.RsIgmpsn(fvBD2, tnIgmpSnoopPolName=u'')
    fvRsCtx2 = cobra.model.fv.RsCtx(fvBD2, tnFvCtxName=u'App-VRF')
    fvRsBdToEpRet2 = cobra.model.fv.RsBdToEpRet(fvBD2, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvRsBDToNdP2 = cobra.model.fv.RsBDToNdP(fvBD2, tnNdIfPolName=u'')


    fvBD3 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'garp', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'Attacker-BD', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'yes', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
    igmpIfP2 = cobra.model.igmp.IfP(fvBD3, name=u'', descr=u'', nameAlias=u'')
    fvSubnet3 = cobra.model.fv.Subnet(fvBD3, name=u'', descr=u'', ctrl=u'', ip=u'10.150.1.254/24', preferred=u'no', virtual=u'no', nameAlias=u'')
    fvRsIgmpsn3 = cobra.model.fv.RsIgmpsn(fvBD3, tnIgmpSnoopPolName=u'')
    fvRsCtx3 = cobra.model.fv.RsCtx(fvBD3, tnFvCtxName=u'App-VRF')
    fvRsBdToEpRet3 = cobra.model.fv.RsBdToEpRet(fvBD3, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvRsBDToNdP3 = cobra.model.fv.RsBDToNdP(fvBD3, tnNdIfPolName=u'')



    fvBD4 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'no', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'garp', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'db-BD', epClear=u'no', unkMacUcastAct=u'flood', arpFlood=u'yes', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
    igmpIfP3 = cobra.model.igmp.IfP(fvBD4, name=u'', descr=u'', nameAlias=u'')
    fvRsIgmpsn4 = cobra.model.fv.RsIgmpsn(fvBD4, tnIgmpSnoopPolName=u'')
    fvRsCtx4 = cobra.model.fv.RsCtx(fvBD4, tnFvCtxName=u'VRF_Common')
    fvRsBdToEpRet4 = cobra.model.fv.RsBdToEpRet(fvBD4, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvRsBDToNdP4 = cobra.model.fv.RsBDToNdP(fvBD4, tnNdIfPolName=u'')

    fvBD5 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'no', unicastRoute=u'no', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'garp', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'asa-to-ftdv-BD', epClear=u'no', unkMacUcastAct=u'flood', arpFlood=u'yes', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
    igmpIfP4 = cobra.model.igmp.IfP(fvBD5, name=u'', descr=u'', nameAlias=u'')
    fvRsIgmpsn5 = cobra.model.fv.RsIgmpsn(fvBD5, tnIgmpSnoopPolName=u'')
    fvRsCtx5 = cobra.model.fv.RsCtx(fvBD5, tnFvCtxName=u'VRF_Common')
    fvRsBdToEpRet5 = cobra.model.fv.RsBdToEpRet(fvBD5, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvRsBDToNdP5 = cobra.model.fv.RsBDToNdP(fvBD5, tnNdIfPolName=u'')



#    fvBD = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'garp', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'App-BD', epClear=u'no', unkMacUcastAct=u'flood', arpFlood=u'yes', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
#    fvRsBDToNdP = cobra.model.fv.RsBDToNdP(fvBD, tnNdIfPolName=u'')
#    fvRsBdToEpRet = cobra.model.fv.RsBdToEpRet(fvBD, resolveAct=u'resolve', tnFvEpRetPolName=u'')
#    fvRsCtx = cobra.model.fv.RsCtx(fvBD, tnFvCtxName=u'App-VRF')
#    fvRsIgmpsn = cobra.model.fv.RsIgmpsn(fvBD, tnIgmpSnoopPolName=u'')
#    igmpIfP = cobra.model.igmp.IfP(fvBD, name=u'', descr=u'', nameAlias=u'')


#    fvBD2 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'secureDc-BD', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'no', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
#    fvRsBDToNdP2 = cobra.model.fv.RsBDToNdP(fvBD2, tnNdIfPolName=u'')
#    fvRsBdToEpRet2 = cobra.model.fv.RsBdToEpRet(fvBD2, resolveAct=u'resolve', tnFvEpRetPolName=u'')
#    fvRsCtx2 = cobra.model.fv.RsCtx(fvBD2, tnFvCtxName=u'secureDc-VRF')
#    fvRsIgmpsn2 = cobra.model.fv.RsIgmpsn(fvBD2, tnIgmpSnoopPolName=u'')
#    fvSubnet = cobra.model.fv.Subnet(fvBD2, name=u'', descr=u'', ctrl=u'', ip=u'10.150.0.2/24', preferred=u'no', virtual=u'no', nameAlias=u'')

#    fvBD3 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'garp', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'Attacker-BD', epClear=u'no', unkMacUcastAct=u'flood', arpFlood=u'yes', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
#    fvRsBDToNdP3 = cobra.model.fv.RsBDToNdP(fvBD3, tnNdIfPolName=u'')
#    fvRsBdToEpRet3 = cobra.model.fv.RsBdToEpRet(fvBD3, resolveAct=u'resolve', tnFvEpRetPolName=u'')
#    fvRsCtx3 = cobra.model.fv.RsCtx(fvBD3, tnFvCtxName=u'App-VRF')
#    fvRsIgmpsn3 = cobra.model.fv.RsIgmpsn(fvBD3, tnIgmpSnoopPolName=u'')
#    igmpIfP2 = cobra.model.igmp.IfP(fvBD3, name=u'', descr=u'', nameAlias=u'')



#    fvBD = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'App-BD', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'no', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
#    fvRsBDToNdP = cobra.model.fv.RsBDToNdP(fvBD, tnNdIfPolName=u'')
#    fvRsBdToEpRet = cobra.model.fv.RsBdToEpRet(fvBD, resolveAct=u'resolve', tnFvEpRetPolName=u'')
#    fvRsCtx = cobra.model.fv.RsCtx(fvBD, tnFvCtxName=u'App-VRF')
#    fvRsIgmpsn = cobra.model.fv.RsIgmpsn(fvBD, tnIgmpSnoopPolName=u'')
#    fvSubnet = cobra.model.fv.Subnet(fvBD, name=u'', descr=u'', ctrl=u'', ip=u'10.150.2.1/24', preferred=u'no', virtual=u'no', nameAlias=u'')


#    fvBD2 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'Attacker-BD', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'no', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
#    fvRsBDToNdP2 = cobra.model.fv.RsBDToNdP(fvBD2, tnNdIfPolName=u'')
#    fvRsBdToEpRet2 = cobra.model.fv.RsBdToEpRet(fvBD2, resolveAct=u'resolve', tnFvEpRetPolName=u'')
#    fvRsCtx2 = cobra.model.fv.RsCtx(fvBD2, tnFvCtxName=u'App-VRF')
#    fvRsIgmpsn2 = cobra.model.fv.RsIgmpsn(fvBD2, tnIgmpSnoopPolName=u'')
#    fvSubnet2 = cobra.model.fv.Subnet(fvBD2, name=u'', descr=u'', ctrl=u'', ip=u'10.150.1.1/24', preferred=u'no', virtual=u'no', nameAlias=u'')


#    fvBD3 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'secureDc-BD', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'no', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
#    fvRsBDToNdP3 = cobra.model.fv.RsBDToNdP(fvBD3, tnNdIfPolName=u'')
#    fvRsBdToEpRet3 = cobra.model.fv.RsBdToEpRet(fvBD3, resolveAct=u'resolve', tnFvEpRetPolName=u'')
#    fvRsCtx3 = cobra.model.fv.RsCtx(fvBD3, tnFvCtxName=u'secureDc-VRF')
#    fvRsIgmpsn3 = cobra.model.fv.RsIgmpsn(fvBD3, tnIgmpSnoopPolName=u'')
#    fvSubnet3 = cobra.model.fv.Subnet(fvBD3, name=u'', descr=u'', ctrl=u'', ip=u'10.150.0.2/24', preferred=u'no', virtual=u'no', nameAlias=u'')


    fvCtx = cobra.model.fv.Ctx(fvTenant, ownerKey=u'', bdEnforcedEnable=u'no', descr=u'', knwMcastAct=u'permit', pcEnfDir=u'ingress', nameAlias=u'', ownerTag=u'', pcEnfPref=u'enforced', name=u'secureDc-VRF')
    fvRsBgpCtxPol = cobra.model.fv.RsBgpCtxPol(fvCtx, tnBgpCtxPolName=u'')
    fvRsCtxToExtRouteTagPol = cobra.model.fv.RsCtxToExtRouteTagPol(fvCtx, tnL3extRouteTagPolName=u'')
    fvRsCtxToEpRet = cobra.model.fv.RsCtxToEpRet(fvCtx, tnFvEpRetPolName=u'')
    fvRsOspfCtxPol = cobra.model.fv.RsOspfCtxPol(fvCtx, tnOspfCtxPolName=u'')
    vzAny = cobra.model.vz.Any(fvCtx, prefGrMemb=u'disabled', matchT=u'AtleastOne', name=u'', descr=u'', nameAlias=u'')
    fvRsVrfValidationPol = cobra.model.fv.RsVrfValidationPol(fvCtx, tnL3extVrfValidationPolName=u'')
    fvCtx2 = cobra.model.fv.Ctx(fvTenant, ownerKey=u'', bdEnforcedEnable=u'no', descr=u'', knwMcastAct=u'permit', pcEnfDir=u'ingress', nameAlias=u'', ownerTag=u'', pcEnfPref=u'enforced', name=u'App-VRF')
    fvRsBgpCtxPol2 = cobra.model.fv.RsBgpCtxPol(fvCtx2, tnBgpCtxPolName=u'')
    fvRsCtxToExtRouteTagPol2 = cobra.model.fv.RsCtxToExtRouteTagPol(fvCtx2, tnL3extRouteTagPolName=u'')
    fvRsCtxToEpRet2 = cobra.model.fv.RsCtxToEpRet(fvCtx2, tnFvEpRetPolName=u'')
    fvRsOspfCtxPol2 = cobra.model.fv.RsOspfCtxPol(fvCtx2, tnOspfCtxPolName=u'')
    vzAny2 = cobra.model.vz.Any(fvCtx2, prefGrMemb=u'disabled', matchT=u'AtleastOne', name=u'', descr=u'', nameAlias=u'')
    fvRsVrfValidationPol2 = cobra.model.fv.RsVrfValidationPol(fvCtx2, tnL3extVrfValidationPolName=u'')


    l3extOut = cobra.model.l3ext.Out(fvTenant, ownerKey=u'', name=u'DC-Edge', descr=u'', targetDscp=u'unspecified', enforceRtctrl=u'export', nameAlias=u'', ownerTag=u'')
    l3extInstP = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'PCI_Servers_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol7 = cobra.model.fv.RsCustQosPol(l3extInstP, tnQosCustomPolName=u'')
    l3extInstP2 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Test_Servers_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol8 = cobra.model.fv.RsCustQosPol(l3extInstP2, tnQosCustomPolName=u'')
    l3extInstP3 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Contractors_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol9 = cobra.model.fv.RsCustQosPol(l3extInstP3, tnQosCustomPolName=u'')
    l3extInstP4 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Development_Servers_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol10 = cobra.model.fv.RsCustQosPol(l3extInstP4, tnQosCustomPolName=u'')
    l3extInstP5 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Network_Services_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol11 = cobra.model.fv.RsCustQosPol(l3extInstP5, tnQosCustomPolName=u'')
    l3extInstP6 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'BYOD_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol12 = cobra.model.fv.RsCustQosPol(l3extInstP6, tnQosCustomPolName=u'')
    l3extInstP7 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Production_Servers_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol13 = cobra.model.fv.RsCustQosPol(l3extInstP7, tnQosCustomPolName=u'')
    l3extInstP8 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Point_of_Sale_Systems_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol14 = cobra.model.fv.RsCustQosPol(l3extInstP8, tnQosCustomPolName=u'')

    l3extInstP9 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Auditors_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol15 = cobra.model.fv.RsCustQosPol(l3extInstP9, tnQosCustomPolName=u'')

    l3extInstP10 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'TestingSGT_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol16 = cobra.model.fv.RsCustQosPol(l3extInstP10, tnQosCustomPolName=u'')

    l3extInstP11 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Production_Users_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol17 = cobra.model.fv.RsCustQosPol(l3extInstP11, tnQosCustomPolName=u'')
    l3extInstP12 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Employees_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol18 = cobra.model.fv.RsCustQosPol(l3extInstP12, tnQosCustomPolName=u'')
    l3extInstP13 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Developers_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol19 = cobra.model.fv.RsCustQosPol(l3extInstP13, tnQosCustomPolName=u'')

    l3extInstP14 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Guests_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol20 = cobra.model.fv.RsCustQosPol(l3extInstP14, tnQosCustomPolName=u'')

    l3extInstP15 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'TrustSec_Devices_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol21 = cobra.model.fv.RsCustQosPol(l3extInstP15, tnQosCustomPolName=u'')

    l3extInstP16 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Quarantined_Systems_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol22 = cobra.model.fv.RsCustQosPol(l3extInstP16, tnQosCustomPolName=u'')
    l3extRsEctx = cobra.model.l3ext.RsEctx(l3extOut, tnFvCtxName=u'')
    vnsSvcCont = cobra.model.vns.SvcCont(fvTenant)

    vzBrCP = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'db-web', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj = cobra.model.vz.Subj(vzBrCP, revFltPorts=u'yes', name=u'Subject', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt = cobra.model.vz.RsSubjFiltAtt(vzSubj, directives=u'', tnVzFilterName=u'mysql_icmp_ssh')
    vzBrCP2 = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'all', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj2 = cobra.model.vz.Subj(vzBrCP2, revFltPorts=u'yes', name=u'all', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt2 = cobra.model.vz.RsSubjFiltAtt(vzSubj2, directives=u'', tnVzFilterName=u'allow_all')
    vzBrCP3 = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'cliqr-firewall_sdc-opencart-test-app_Apache_3004_5', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj3 = cobra.model.vz.Subj(vzBrCP3, revFltPorts=u'yes', name=u'cliqr-subject', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt3 = cobra.model.vz.RsSubjFiltAtt(vzSubj3, directives=u'', tnVzFilterName=u'cliqr-firewall_sdc-opencart-test-app_Apache_3004_5')

    vzBrCP4 = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'web-load', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj4 = cobra.model.vz.Subj(vzBrCP4, revFltPorts=u'yes', name=u'Subject', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt4 = cobra.model.vz.RsSubjFiltAtt(vzSubj4, directives=u'', tnVzFilterName=u'default')
    vzBrCP5 = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'cliqr-firewall_sdc-opencart-test-app_Database_3004_5', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj5 = cobra.model.vz.Subj(vzBrCP5, revFltPorts=u'yes', name=u'cliqr-subject', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')

    vzRsSubjFiltAtt5 = cobra.model.vz.RsSubjFiltAtt(vzSubj5, directives=u'', tnVzFilterName=u'cliqr-firewall_sdc-opencart-test-app_Database_3004_5')
    vzBrCP6 = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'allow_all', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj6 = cobra.model.vz.Subj(vzBrCP6, revFltPorts=u'yes', name=u'Subject', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt6 = cobra.model.vz.RsSubjFiltAtt(vzSubj6, directives=u'', tnVzFilterName=u'port_3306')
    vzRsSubjFiltAtt7 = cobra.model.vz.RsSubjFiltAtt(vzSubj6, directives=u'', tnVzFilterName=u'icmp')
    vzBrCP7 = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'allow_all_global', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj7 = cobra.model.vz.Subj(vzBrCP7, revFltPorts=u'yes', name=u'allow_all', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt8 = cobra.model.vz.RsSubjFiltAtt(vzSubj7, directives=u'', tnVzFilterName=u'allow_all_Filter')


    # commit the generated code to APIC
    #print toXMLStr(polUni)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(polUni)
    md.commit(c)





def sdcCreateTenantV2(md, tenantName='tenantName'):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')


    # build the request using cobra syntax
    fvTenant = cobra.model.fv.Tenant(polUni, ownerKey=u'', name=tenantName, descr=u'', nameAlias=u'', ownerTag=u'')

    aaaDomainRef = cobra.model.aaa.DomainRef(fvTenant, ownerKey=u'', ownerTag=u'', name=u'SecDo-'+tenantName, descr=u'', nameAlias=u'')

    vnsAbsFuncProfContr = cobra.model.vns.AbsFuncProfContr(fvTenant, ownerKey=u'', ownerTag=u'', name=u'', descr=u'', nameAlias=u'')

    fvAp = cobra.model.fv.Ap(fvTenant, ownerKey=u'', name=u'CloudCenter-Apps', prio=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')

    fvAEPg = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Web', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    #fvRsBd = cobra.model.fv.RsBd(fvAEPg, tnFvBDName=u'hybridcloud-inbound')
    #fvRsBd = cobra.model.fv.RsBd(fvAEPg, tnFvBDName=u'securedc-inbound')
    fvRsBd = cobra.model.fv.RsBd(fvAEPg, tnFvBDName=u'web-BD')


    fvRsCustQosPol = cobra.model.fv.RsCustQosPol(fvAEPg, tnQosCustomPolName=u'')
    #fvRsCons = cobra.model.fv.RsCons(fvAEPg, tnVzBrCPName=u'L3_Out_default', prio=u'unspecified')
    #fvRsCons2 = cobra.model.fv.RsCons(fvAEPg, tnVzBrCPName=u'db-web', prio=u'unspecified')
    fvRsDomAtt = cobra.model.fv.RsDomAtt(fvAEPg, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')
    vmmSecP = cobra.model.vmm.SecP(fvRsDomAtt, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'accept', nameAlias=u'', ownerTag=u'', allowPromiscuous=u'accept', macChanges=u'accept')

    fvAEPg2 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Web-Out', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    fvRsBd2 = cobra.model.fv.RsBd(fvAEPg2, tnFvBDName=u'App-BD')
    fvRsCustQosPol2 = cobra.model.fv.RsCustQosPol(fvAEPg2, tnQosCustomPolName=u'')
    fvRsDomAtt2 = cobra.model.fv.RsDomAtt(fvAEPg2, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'useg', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')
    vmmSecP2 = cobra.model.vmm.SecP(fvRsDomAtt2, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'accept', nameAlias=u'', ownerTag=u'', allowPromiscuous=u'accept', macChanges=u'accept')


    fvAEPg3 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Db', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    #fvRsBd3 = cobra.model.fv.RsBd(fvAEPg3, tnFvBDName=u'hybridcloud-inbound')
    #fvRsBd3 = cobra.model.fv.RsBd(fvAEPg3, tnFvBDName=u'securedc-db')
    #fvRsBd3 = cobra.model.fv.RsBd(fvAEPg3, tnFvBDName=tenantName+'-db')
    fvRsBd3 = cobra.model.fv.RsBd(fvAEPg3, tnFvBDName=r'db-BD')
    fvRsCustQosPol3 = cobra.model.fv.RsCustQosPol(fvAEPg3, tnQosCustomPolName=u'')
    #fvRsCons3 = cobra.model.fv.RsCons(fvAEPg3, tnVzBrCPName=u'L3_Out_default', prio=u'unspecified')
    fvRsDomAtt3 = cobra.model.fv.RsDomAtt(fvAEPg3, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')
    vmmSecP3 = cobra.model.vmm.SecP(fvRsDomAtt3, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'accept', nameAlias=u'', ownerTag=u'', allowPromiscuous=u'accept', macChanges=u'accept')
    #fvRsProv = cobra.model.fv.RsProv(fvAEPg3, tnVzBrCPName=u'db-web', matchT=u'AtleastOne', prio=u'unspecified')

    fvAEPg4 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Attacker', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    fvRsBd4 = cobra.model.fv.RsBd(fvAEPg4, tnFvBDName=u'Attacker-BD')
    fvRsCustQosPol4 = cobra.model.fv.RsCustQosPol(fvAEPg4, tnQosCustomPolName=u'')
    fvRsDomAtt4 = cobra.model.fv.RsDomAtt(fvAEPg4, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')
    vmmSecP4 = cobra.model.vmm.SecP(fvRsDomAtt4, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'accept', nameAlias=u'', ownerTag=u'', allowPromiscuous=u'accept', macChanges=u'accept')


    fvAp2 = cobra.model.fv.Ap(fvTenant, ownerKey=u'', name=u'secureDC', prio=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')

    fvAEPg5 = cobra.model.fv.AEPg(fvAp2, isAttrBasedEPg=u'yes', matchT=u'AtleastOne', name=u'securedc-useg', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    fvCrtrn = cobra.model.fv.Crtrn(fvAEPg5, ownerKey=u'', name=u'default', descr=u'', prec=u'0', nameAlias=u'', ownerTag=u'', match=u'any')
    fvIpAttr = cobra.model.fv.IpAttr(fvCrtrn, ownerKey=u'', name=u'0', descr=u'', ip=u'10.150.1.0/24', nameAlias=u'', ownerTag=u'', usefvSubnet=u'no')
    fvRsBd5 = cobra.model.fv.RsBd(fvAEPg5, tnFvBDName=u'App-BD')
    fvRsCustQosPol5 = cobra.model.fv.RsCustQosPol(fvAEPg5, tnQosCustomPolName=u'')
    fvRsDomAtt5 = cobra.model.fv.RsDomAtt(fvAEPg5, tDn=u'uni/phys-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')


    fvAEPg6 = cobra.model.fv.AEPg(fvAp2, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'device-mgmt', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    fvRsBd6 = cobra.model.fv.RsBd(fvAEPg6, tnFvBDName=u'secureDc-BD')
    fvRsCustQosPol6 = cobra.model.fv.RsCustQosPol(fvAEPg6, tnQosCustomPolName=u'')
    fvRsDomAtt6 = cobra.model.fv.RsDomAtt(fvAEPg6, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')
    vmmSecP5 = cobra.model.vmm.SecP(fvRsDomAtt6, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'accept', nameAlias=u'', ownerTag=u'', allowPromiscuous=u'accept', macChanges=u'accept')
    fvRsProv2 = cobra.model.fv.RsProv(fvAEPg6, tnVzBrCPName=u'all', matchT=u'AtleastOne', prio=u'unspecified')
    vnsAddrInst = cobra.model.vns.AddrInst(fvAEPg6, ownerKey=u'', addr=u'10.150.0.2/24', descr=u'', nameAlias=u'', ownerTag=u'', name=u'inbandMgmtForL4L7Devices')
    fvnsUcastAddrBlk = cobra.model.fvns.UcastAddrBlk(vnsAddrInst, to=u'10.150.0.243', from_=u'10.150.0.241', name=u'', descr=u'', nameAlias=u'')
    fvRsTenantMonPol = cobra.model.fv.RsTenantMonPol(fvTenant, tnMonEPGPolName=u'')
    vzFilter = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'cliqr-firewall_sdc-opencart-test-app_Apache_3004_5', descr=u'', nameAlias=u'')
    vzEntry = cobra.model.vz.Entry(vzFilter, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'http', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'tcp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'http', name=u'TCP_80_80')
    vzFilter2 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'icmp', descr=u'', nameAlias=u'')
    vzEntry2 = cobra.model.vz.Entry(vzFilter2, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'unspecified', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'icmp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'unspecified', name=u'icmp')
    vzFilter3 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'allow_all', descr=u'', nameAlias=u'')
    vzEntry3 = cobra.model.vz.Entry(vzFilter3, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'unspecified', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'unspecified', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'unspecified', dFromPort=u'unspecified', name=u'allow_all')
    vzFilter4 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'cliqr-firewall_sdc-opencart-test-app_Database_3004_5', descr=u'', nameAlias=u'')
    vzEntry4 = cobra.model.vz.Entry(vzFilter4, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'3306', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'tcp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'3306', name=u'TCP_3306_3306')
    vzFilter5 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'mysql_icmp_ssh', descr=u'', nameAlias=u'')
    vzEntry5 = cobra.model.vz.Entry(vzFilter5, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'3306', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'tcp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'3306', name=u'mysql')
    vzEntry6 = cobra.model.vz.Entry(vzFilter5, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'22', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'tcp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'22', name=u'ssh')
    vzEntry7 = cobra.model.vz.Entry(vzFilter5, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'unspecified', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'icmp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'unspecified', name=u'icmp')
    vzFilter6 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'port_80', descr=u'', nameAlias=u'')
    vzFilter7 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'web-load_Filter', descr=u'', nameAlias=u'')
    vzFilter8 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'allow_all_Filter', descr=u'', nameAlias=u'')
    vzFilter9 = cobra.model.vz.Filter(fvTenant, ownerKey=u'', ownerTag=u'', name=u'port_3306', descr=u'', nameAlias=u'')
    vzEntry8 = cobra.model.vz.Entry(vzFilter9, tcpRules=u'', arpOpc=u'unspecified', applyToFrag=u'no', dToPort=u'3306', descr=u'', nameAlias=u'', matchDscp=u'unspecified', prot=u'tcp', icmpv4T=u'unspecified', sFromPort=u'unspecified', stateful=u'no', icmpv6T=u'unspecified', sToPort=u'unspecified', etherT=u'ip', dFromPort=u'3306', name=u'web_db')




    #securedc-BD
    fvBD = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'secureDc-BD', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'no', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
    fvSubnet = cobra.model.fv.Subnet(fvBD, name=u'', descr=u'', ctrl=u'', ip=u'10.150.0.2/24', preferred=u'no', virtual=u'no', nameAlias=u'')
    fvRsIgmpsn = cobra.model.fv.RsIgmpsn(fvBD, tnIgmpSnoopPolName=u'')
    fvRsCtx = cobra.model.fv.RsCtx(fvBD, tnFvCtxName=u'secureDc-VRF')
    fvRsBdToEpRet = cobra.model.fv.RsBdToEpRet(fvBD, resolveAct=u'resolve', tnFvEpRetPolName=u'') 
    fvRsBDToNdP = cobra.model.fv.RsBDToNdP(fvBD, tnNdIfPolName=u'')

    #app-BD
    fvBD2 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'garp', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'App-BD', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'yes', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
    igmpIfP = cobra.model.igmp.IfP(fvBD2, name=u'', descr=u'', nameAlias=u'')
    fvSubnet2 = cobra.model.fv.Subnet(fvBD2, name=u'', descr=u'', ctrl=u'', ip=u'10.150.2.254/24', preferred=u'no', virtual=u'no', nameAlias=u'')
    fvRsIgmpsn2 = cobra.model.fv.RsIgmpsn(fvBD2, tnIgmpSnoopPolName=u'')
    fvRsCtx2 = cobra.model.fv.RsCtx(fvBD2, tnFvCtxName=u'App-VRF')
    fvRsBdToEpRet2 = cobra.model.fv.RsBdToEpRet(fvBD2, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvRsBDToNdP2 = cobra.model.fv.RsBDToNdP(fvBD2, tnNdIfPolName=u'')

    #Attacker-bd
    fvBD3 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'garp', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'Attacker-BD', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'yes', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
    igmpIfP2 = cobra.model.igmp.IfP(fvBD3, name=u'', descr=u'', nameAlias=u'')
    fvSubnet3 = cobra.model.fv.Subnet(fvBD3, name=u'', descr=u'', ctrl=u'', ip=u'10.150.1.254/24', preferred=u'no', virtual=u'no', nameAlias=u'')
    fvRsIgmpsn3 = cobra.model.fv.RsIgmpsn(fvBD3, tnIgmpSnoopPolName=u'')
    fvRsCtx3 = cobra.model.fv.RsCtx(fvBD3, tnFvCtxName=u'App-VRF')
    fvRsBdToEpRet3 = cobra.model.fv.RsBdToEpRet(fvBD3, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvRsBDToNdP3 = cobra.model.fv.RsBDToNdP(fvBD3, tnNdIfPolName=u'')

    #db-BD
    fvBD4 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'no', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'garp', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'db-BD', epClear=u'no', unkMacUcastAct=u'flood', arpFlood=u'yes', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
    igmpIfP3 = cobra.model.igmp.IfP(fvBD4, name=u'', descr=u'', nameAlias=u'')
    fvRsIgmpsn4 = cobra.model.fv.RsIgmpsn(fvBD4, tnIgmpSnoopPolName=u'')
    fvRsCtx4 = cobra.model.fv.RsCtx(fvBD4, tnFvCtxName=u'App-VRF')
    fvRsBdToEpRet4 = cobra.model.fv.RsBdToEpRet(fvBD4, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvRsBDToNdP4 = cobra.model.fv.RsBDToNdP(fvBD4, tnNdIfPolName=u'')

    #asav-to-ftdv-DB
    fvBD5 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'no', unicastRoute=u'no', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'garp', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'asa-to-ftdv-BD', epClear=u'no', unkMacUcastAct=u'flood', arpFlood=u'yes', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
    igmpIfP4 = cobra.model.igmp.IfP(fvBD5, name=u'', descr=u'', nameAlias=u'')
    fvRsIgmpsn5 = cobra.model.fv.RsIgmpsn(fvBD5, tnIgmpSnoopPolName=u'')
    fvRsCtx5 = cobra.model.fv.RsCtx(fvBD5, tnFvCtxName=u'App-VRF')
    fvRsBdToEpRet5 = cobra.model.fv.RsBdToEpRet(fvBD5, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvRsBDToNdP5 = cobra.model.fv.RsBDToNdP(fvBD5, tnNdIfPolName=u'')

    #web-BD
    fvBD6 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'no', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'garp', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'web-BD', epClear=u'no', unkMacUcastAct=u'flood', arpFlood=u'yes', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
    igmpIfP5 = cobra.model.igmp.IfP(fvBD6, name=u'', descr=u'', nameAlias=u'')
    fvRsIgmpsn6 = cobra.model.fv.RsIgmpsn(fvBD6, tnIgmpSnoopPolName=u'')
    fvRsCtx6 = cobra.model.fv.RsCtx(fvBD6, tnFvCtxName=u'App-VRF')
    fvRsBdToEpRet6 = cobra.model.fv.RsBdToEpRet(fvBD6, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvRsBDToNdP6 = cobra.model.fv.RsBDToNdP(fvBD6, tnNdIfPolName=u'')



#    fvBD = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'garp', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'App-BD', epClear=u'no', unkMacUcastAct=u'flood', arpFlood=u'yes', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
#    fvRsBDToNdP = cobra.model.fv.RsBDToNdP(fvBD, tnNdIfPolName=u'')
#    fvRsBdToEpRet = cobra.model.fv.RsBdToEpRet(fvBD, resolveAct=u'resolve', tnFvEpRetPolName=u'')
#    fvRsCtx = cobra.model.fv.RsCtx(fvBD, tnFvCtxName=u'App-VRF')
#    fvRsIgmpsn = cobra.model.fv.RsIgmpsn(fvBD, tnIgmpSnoopPolName=u'')
#    igmpIfP = cobra.model.igmp.IfP(fvBD, name=u'', descr=u'', nameAlias=u'')


#    fvBD2 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'secureDc-BD', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'no', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
#    fvRsBDToNdP2 = cobra.model.fv.RsBDToNdP(fvBD2, tnNdIfPolName=u'')
#    fvRsBdToEpRet2 = cobra.model.fv.RsBdToEpRet(fvBD2, resolveAct=u'resolve', tnFvEpRetPolName=u'')
#    fvRsCtx2 = cobra.model.fv.RsCtx(fvBD2, tnFvCtxName=u'secureDc-VRF')
#    fvRsIgmpsn2 = cobra.model.fv.RsIgmpsn(fvBD2, tnIgmpSnoopPolName=u'')
#    fvSubnet = cobra.model.fv.Subnet(fvBD2, name=u'', descr=u'', ctrl=u'', ip=u'10.150.0.2/24', preferred=u'no', virtual=u'no', nameAlias=u'')

#    fvBD3 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'garp', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'Attacker-BD', epClear=u'no', unkMacUcastAct=u'flood', arpFlood=u'yes', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
#    fvRsBDToNdP3 = cobra.model.fv.RsBDToNdP(fvBD3, tnNdIfPolName=u'')
#    fvRsBdToEpRet3 = cobra.model.fv.RsBdToEpRet(fvBD3, resolveAct=u'resolve', tnFvEpRetPolName=u'')
#    fvRsCtx3 = cobra.model.fv.RsCtx(fvBD3, tnFvCtxName=u'App-VRF')
#    fvRsIgmpsn3 = cobra.model.fv.RsIgmpsn(fvBD3, tnIgmpSnoopPolName=u'')
#    igmpIfP2 = cobra.model.igmp.IfP(fvBD3, name=u'', descr=u'', nameAlias=u'')



#    fvBD = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'App-BD', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'no', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
#    fvRsBDToNdP = cobra.model.fv.RsBDToNdP(fvBD, tnNdIfPolName=u'')
#    fvRsBdToEpRet = cobra.model.fv.RsBdToEpRet(fvBD, resolveAct=u'resolve', tnFvEpRetPolName=u'')
#    fvRsCtx = cobra.model.fv.RsCtx(fvBD, tnFvCtxName=u'App-VRF')
#    fvRsIgmpsn = cobra.model.fv.RsIgmpsn(fvBD, tnIgmpSnoopPolName=u'')
#    fvSubnet = cobra.model.fv.Subnet(fvBD, name=u'', descr=u'', ctrl=u'', ip=u'10.150.2.1/24', preferred=u'no', virtual=u'no', nameAlias=u'')


#    fvBD2 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'Attacker-BD', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'no', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
#    fvRsBDToNdP2 = cobra.model.fv.RsBDToNdP(fvBD2, tnNdIfPolName=u'')
#    fvRsBdToEpRet2 = cobra.model.fv.RsBdToEpRet(fvBD2, resolveAct=u'resolve', tnFvEpRetPolName=u'')
#    fvRsCtx2 = cobra.model.fv.RsCtx(fvBD2, tnFvCtxName=u'App-VRF')
#    fvRsIgmpsn2 = cobra.model.fv.RsIgmpsn(fvBD2, tnIgmpSnoopPolName=u'')
#    fvSubnet2 = cobra.model.fv.Subnet(fvBD2, name=u'', descr=u'', ctrl=u'', ip=u'10.150.1.1/24', preferred=u'no', virtual=u'no', nameAlias=u'')


#    fvBD3 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'secureDc-BD', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'no', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
#    fvRsBDToNdP3 = cobra.model.fv.RsBDToNdP(fvBD3, tnNdIfPolName=u'')
#    fvRsBdToEpRet3 = cobra.model.fv.RsBdToEpRet(fvBD3, resolveAct=u'resolve', tnFvEpRetPolName=u'')
#    fvRsCtx3 = cobra.model.fv.RsCtx(fvBD3, tnFvCtxName=u'secureDc-VRF')
#    fvRsIgmpsn3 = cobra.model.fv.RsIgmpsn(fvBD3, tnIgmpSnoopPolName=u'')
#    fvSubnet3 = cobra.model.fv.Subnet(fvBD3, name=u'', descr=u'', ctrl=u'', ip=u'10.150.0.2/24', preferred=u'no', virtual=u'no', nameAlias=u'')


    fvCtx = cobra.model.fv.Ctx(fvTenant, ownerKey=u'', bdEnforcedEnable=u'no', descr=u'', knwMcastAct=u'permit', pcEnfDir=u'ingress', nameAlias=u'', ownerTag=u'', pcEnfPref=u'enforced', name=u'secureDc-VRF')
    fvRsBgpCtxPol = cobra.model.fv.RsBgpCtxPol(fvCtx, tnBgpCtxPolName=u'')
    fvRsCtxToExtRouteTagPol = cobra.model.fv.RsCtxToExtRouteTagPol(fvCtx, tnL3extRouteTagPolName=u'')
    fvRsCtxToEpRet = cobra.model.fv.RsCtxToEpRet(fvCtx, tnFvEpRetPolName=u'')
    fvRsOspfCtxPol = cobra.model.fv.RsOspfCtxPol(fvCtx, tnOspfCtxPolName=u'')
    vzAny = cobra.model.vz.Any(fvCtx, prefGrMemb=u'disabled', matchT=u'AtleastOne', name=u'', descr=u'', nameAlias=u'')
    fvRsVrfValidationPol = cobra.model.fv.RsVrfValidationPol(fvCtx, tnL3extVrfValidationPolName=u'')
    fvCtx2 = cobra.model.fv.Ctx(fvTenant, ownerKey=u'', bdEnforcedEnable=u'no', descr=u'', knwMcastAct=u'permit', pcEnfDir=u'ingress', nameAlias=u'', ownerTag=u'', pcEnfPref=u'enforced', name=u'App-VRF')
    fvRsBgpCtxPol2 = cobra.model.fv.RsBgpCtxPol(fvCtx2, tnBgpCtxPolName=u'')
    fvRsCtxToExtRouteTagPol2 = cobra.model.fv.RsCtxToExtRouteTagPol(fvCtx2, tnL3extRouteTagPolName=u'')
    fvRsCtxToEpRet2 = cobra.model.fv.RsCtxToEpRet(fvCtx2, tnFvEpRetPolName=u'')
    fvRsOspfCtxPol2 = cobra.model.fv.RsOspfCtxPol(fvCtx2, tnOspfCtxPolName=u'')
    vzAny2 = cobra.model.vz.Any(fvCtx2, prefGrMemb=u'disabled', matchT=u'AtleastOne', name=u'', descr=u'', nameAlias=u'')
    fvRsVrfValidationPol2 = cobra.model.fv.RsVrfValidationPol(fvCtx2, tnL3extVrfValidationPolName=u'')


    l3extOut = cobra.model.l3ext.Out(fvTenant, ownerKey=u'', name=u'DC-Edge', descr=u'', targetDscp=u'unspecified', enforceRtctrl=u'export', nameAlias=u'', ownerTag=u'')
    l3extInstP = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'PCI_Servers_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol7 = cobra.model.fv.RsCustQosPol(l3extInstP, tnQosCustomPolName=u'')
    l3extInstP2 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Test_Servers_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol8 = cobra.model.fv.RsCustQosPol(l3extInstP2, tnQosCustomPolName=u'')
    l3extInstP3 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Contractors_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol9 = cobra.model.fv.RsCustQosPol(l3extInstP3, tnQosCustomPolName=u'')
    l3extInstP4 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Development_Servers_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol10 = cobra.model.fv.RsCustQosPol(l3extInstP4, tnQosCustomPolName=u'')
    l3extInstP5 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Network_Services_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol11 = cobra.model.fv.RsCustQosPol(l3extInstP5, tnQosCustomPolName=u'')
    l3extInstP6 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'BYOD_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol12 = cobra.model.fv.RsCustQosPol(l3extInstP6, tnQosCustomPolName=u'')
    l3extInstP7 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Production_Servers_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol13 = cobra.model.fv.RsCustQosPol(l3extInstP7, tnQosCustomPolName=u'')
    l3extInstP8 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Point_of_Sale_Systems_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol14 = cobra.model.fv.RsCustQosPol(l3extInstP8, tnQosCustomPolName=u'')

    l3extInstP9 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Auditors_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol15 = cobra.model.fv.RsCustQosPol(l3extInstP9, tnQosCustomPolName=u'')

    l3extInstP10 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'TestingSGT_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol16 = cobra.model.fv.RsCustQosPol(l3extInstP10, tnQosCustomPolName=u'')

    l3extInstP11 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Production_Users_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol17 = cobra.model.fv.RsCustQosPol(l3extInstP11, tnQosCustomPolName=u'')
    l3extInstP12 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Employees_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol18 = cobra.model.fv.RsCustQosPol(l3extInstP12, tnQosCustomPolName=u'')
    l3extInstP13 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Developers_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol19 = cobra.model.fv.RsCustQosPol(l3extInstP13, tnQosCustomPolName=u'')

    l3extInstP14 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Guests_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol20 = cobra.model.fv.RsCustQosPol(l3extInstP14, tnQosCustomPolName=u'')

    l3extInstP15 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'TrustSec_Devices_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol21 = cobra.model.fv.RsCustQosPol(l3extInstP15, tnQosCustomPolName=u'')

    l3extInstP16 = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'Quarantined_Systems_SGT', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsCustQosPol22 = cobra.model.fv.RsCustQosPol(l3extInstP16, tnQosCustomPolName=u'')
    l3extRsEctx = cobra.model.l3ext.RsEctx(l3extOut, tnFvCtxName=u'')
    vnsSvcCont = cobra.model.vns.SvcCont(fvTenant)

    vzBrCP = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'db-web', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj = cobra.model.vz.Subj(vzBrCP, revFltPorts=u'yes', name=u'Subject', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt = cobra.model.vz.RsSubjFiltAtt(vzSubj, directives=u'', tnVzFilterName=u'mysql_icmp_ssh')
    vzBrCP2 = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'all', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj2 = cobra.model.vz.Subj(vzBrCP2, revFltPorts=u'yes', name=u'all', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt2 = cobra.model.vz.RsSubjFiltAtt(vzSubj2, directives=u'', tnVzFilterName=u'allow_all')
    vzBrCP3 = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'cliqr-firewall_sdc-opencart-test-app_Apache_3004_5', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj3 = cobra.model.vz.Subj(vzBrCP3, revFltPorts=u'yes', name=u'cliqr-subject', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt3 = cobra.model.vz.RsSubjFiltAtt(vzSubj3, directives=u'', tnVzFilterName=u'cliqr-firewall_sdc-opencart-test-app_Apache_3004_5')

    vzBrCP4 = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'web-load', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj4 = cobra.model.vz.Subj(vzBrCP4, revFltPorts=u'yes', name=u'Subject', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt4 = cobra.model.vz.RsSubjFiltAtt(vzSubj4, directives=u'', tnVzFilterName=u'default')
    vzBrCP5 = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'cliqr-firewall_sdc-opencart-test-app_Database_3004_5', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj5 = cobra.model.vz.Subj(vzBrCP5, revFltPorts=u'yes', name=u'cliqr-subject', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')

    vzRsSubjFiltAtt5 = cobra.model.vz.RsSubjFiltAtt(vzSubj5, directives=u'', tnVzFilterName=u'cliqr-firewall_sdc-opencart-test-app_Database_3004_5')
    vzBrCP6 = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'allow_all', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj6 = cobra.model.vz.Subj(vzBrCP6, revFltPorts=u'yes', name=u'Subject', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt6 = cobra.model.vz.RsSubjFiltAtt(vzSubj6, directives=u'', tnVzFilterName=u'port_3306')
    vzRsSubjFiltAtt7 = cobra.model.vz.RsSubjFiltAtt(vzSubj6, directives=u'', tnVzFilterName=u'icmp')
    vzBrCP7 = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=u'allow_all_global', prio=u'unspecified', targetDscp=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')
    vzSubj7 = cobra.model.vz.Subj(vzBrCP7, revFltPorts=u'yes', name=u'allow_all', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt8 = cobra.model.vz.RsSubjFiltAtt(vzSubj7, directives=u'', tnVzFilterName=u'allow_all_Filter')


    # commit the generated code to APIC
    #print toXMLStr(polUni)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(polUni)
    md.commit(c)






def createAciSecLabTenant(md,l3outVlan="2052", tenantName="ACI-Sec-Lab-Tenant"):

    polUni = cobra.model.pol.Uni('')

    fvTenant = cobra.model.fv.Tenant(polUni, ownerKey=u'', name=str(tenantName), descr=u'', nameAlias=u'', ownerTag=u'')

    #Security Domain
    aaaDomainRef = cobra.model.aaa.DomainRef(fvTenant, ownerKey=u'', ownerTag=u'', name=u'SecDo-'+tenantName, descr=u'', nameAlias=u'')

    #Application Profile
    fvAp = cobra.model.fv.Ap(fvTenant, ownerKey=u'', name=u'aProf', prio=u'unspecified', descr=u'', nameAlias=u'', ownerTag=u'')

    #EPG: App
    fvAEPg = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'app', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    fvRsBd = cobra.model.fv.RsBd(fvAEPg, tnFvBDName=u'BD1')
    fvRsCustQosPol = cobra.model.fv.RsCustQosPol(fvAEPg, tnQosCustomPolName=u'')
    fvRsDomAtt = cobra.model.fv.RsDomAtt(fvAEPg, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'lazy', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')

    #EPG: Db
    fvAEPg2 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'db', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    fvRsBd2 = cobra.model.fv.RsBd(fvAEPg2, tnFvBDName=u'BD2')
    fvRsCustQosPol2 = cobra.model.fv.RsCustQosPol(fvAEPg2, tnQosCustomPolName=u'')
    fvRsDomAtt2 = cobra.model.fv.RsDomAtt(fvAEPg2, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'lazy', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')

    #EPG: Web
    fvAEPg3 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'web', descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    fvRsBd3 = cobra.model.fv.RsBd(fvAEPg3, tnFvBDName=u'BD1')
    fvRsCustQosPol3 = cobra.model.fv.RsCustQosPol(fvAEPg3, tnQosCustomPolName=u'')
    fvRsDomAtt3 = cobra.model.fv.RsDomAtt(fvAEPg3, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'lazy', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')

    #EPG: failover
    fvAEPg4 = cobra.model.fv.AEPg(fvAp, prio=u'unspecified', matchT=u'AtleastOne', name=u'failover', descr=u'')
    fvRsDomAtt4 = cobra.model.fv.RsDomAtt(fvAEPg4, instrImedcy=u'lazy', resImedcy=u'lazy', encap=u'unknown', tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud')
    fvRsCustQosPol4 = cobra.model.fv.RsCustQosPol(fvAEPg4, tnQosCustomPolName=u'')
    fvRsBd4 = cobra.model.fv.RsBd(fvAEPg4, tnFvBDName=u'BD3')


    #
    fvRsTenantMonPol = cobra.model.fv.RsTenantMonPol(fvTenant, tnMonEPGPolName=u'')

    #BD: BD1
    fvBD = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'BD1', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'no', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
    fvRsBDToNdP = cobra.model.fv.RsBDToNdP(fvBD, tnNdIfPolName=u'')
    fvRsBdToEpRet = cobra.model.fv.RsBdToEpRet(fvBD, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvRsCtx = cobra.model.fv.RsCtx(fvBD, tnFvCtxName=u'internalVRF')
    fvRsIgmpsn = cobra.model.fv.RsIgmpsn(fvBD, tnIgmpSnoopPolName=u'')

    #BD: BD2
    fvBD2 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'BD2', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'no', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
    fvRsBDToNdP2 = cobra.model.fv.RsBDToNdP(fvBD2, tnNdIfPolName=u'')
    fvRsBdToEpRet2 = cobra.model.fv.RsBdToEpRet(fvBD2, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvRsCtx2 = cobra.model.fv.RsCtx(fvBD2, tnFvCtxName=u'internalVRF')
    fvRsIgmpsn2 = cobra.model.fv.RsIgmpsn(fvBD2, tnIgmpSnoopPolName=u'')

    #BD: BD3

    fvBD3 = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', nameAlias=u'', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=u'BD3', epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'yes', intersiteL2Stretch=u'no', OptimizeWanBandwidth=u'no')
    fvRsBDToNdP3 = cobra.model.fv.RsBDToNdP(fvBD3, tnNdIfPolName=u'')
    fvRsBdToEpRet3 = cobra.model.fv.RsBdToEpRet(fvBD3, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvRsCtx3 = cobra.model.fv.RsCtx(fvBD3, tnFvCtxName=u'internalVRF')
    fvRsIgmpsn3 = cobra.model.fv.RsIgmpsn(fvBD3, tnIgmpSnoopPolName=u'')

    #VRF: Internal
    fvCtx = cobra.model.fv.Ctx(fvTenant, ownerKey=u'', bdEnforcedEnable=u'no', descr=u'', knwMcastAct=u'permit', pcEnfDir=u'ingress', nameAlias=u'', ownerTag=u'', pcEnfPref=u'enforced', name=u'internalVRF')
    fvRsBgpCtxPol = cobra.model.fv.RsBgpCtxPol(fvCtx, tnBgpCtxPolName=u'')
    fvRsCtxToExtRouteTagPol = cobra.model.fv.RsCtxToExtRouteTagPol(fvCtx, tnL3extRouteTagPolName=u'')
    fvRsCtxToEpRet = cobra.model.fv.RsCtxToEpRet(fvCtx, tnFvEpRetPolName=u'')
    fvRsOspfCtxPol = cobra.model.fv.RsOspfCtxPol(fvCtx, tnOspfCtxPolName=u'')
    vzAny = cobra.model.vz.Any(fvCtx, prefGrMemb=u'disabled', matchT=u'AtleastOne', name=u'', descr=u'', nameAlias=u'')
    fvRsVrfValidationPol = cobra.model.fv.RsVrfValidationPol(fvCtx, tnL3extVrfValidationPolName=u'')

    #VRF: External
    fvCtx2 = cobra.model.fv.Ctx(fvTenant, ownerKey=u'', bdEnforcedEnable=u'no', descr=u'', knwMcastAct=u'permit', pcEnfDir=u'ingress', nameAlias=u'', ownerTag=u'', pcEnfPref=u'enforced', name=u'externalVRF')
    fvRsBgpCtxPol2 = cobra.model.fv.RsBgpCtxPol(fvCtx2, tnBgpCtxPolName=u'')
    fvRsCtxToExtRouteTagPol2 = cobra.model.fv.RsCtxToExtRouteTagPol(fvCtx2, tnL3extRouteTagPolName=u'')
    fvRsCtxToEpRet2 = cobra.model.fv.RsCtxToEpRet(fvCtx2, tnFvEpRetPolName=u'')
    fvRsOspfCtxPol2 = cobra.model.fv.RsOspfCtxPol(fvCtx2, tnOspfCtxPolName=u'')
    vzAny2 = cobra.model.vz.Any(fvCtx2, prefGrMemb=u'disabled', matchT=u'AtleastOne', name=u'', descr=u'', nameAlias=u'')
    fvRsVrfValidationPol2 = cobra.model.fv.RsVrfValidationPol(fvCtx2, tnL3extVrfValidationPolName=u'')


    #L3Out
    #l3extOut = cobra.model.l3ext.Out(fvTenant, ownerKey=u'', name=u'outside-L3Out3', descr=u'', targetDscp=u'unspecified', enforceRtctrl=u'export', nameAlias=u'', ownerTag=u'')
    #l3extInstP = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=u'outside', descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    #fvRsCustQosPol4 = cobra.model.fv.RsCustQosPol(l3extInstP, tnQosCustomPolName=u'')
    #l3extSubnet = cobra.model.l3ext.Subnet(l3extInstP, name=u'', descr=u'', ip=u'10.70.0.0/24', nameAlias=u'', aggregate=u'')
    #l3extLNodeP = cobra.model.l3ext.LNodeP(l3extOut, ownerKey=u'', name=u'outside-L3Out3', descr=u'', targetDscp=u'unspecified', tag=u'yellow-green', nameAlias=u'', ownerTag=u'')
    #l3extLIfP = cobra.model.l3ext.LIfP(l3extLNodeP, ownerKey=u'', name=u'outside-L3Out3', descr=u'', tag=u'yellow-green', nameAlias=u'', ownerTag=u'')
    #l3extRsEgressQosDppPol = cobra.model.l3ext.RsEgressQosDppPol(l3extLIfP, tnQosDppPolName=u'')
    #l3extRsIngressQosDppPol = cobra.model.l3ext.RsIngressQosDppPol(l3extLIfP, tnQosDppPolName=u'')
    #l3extRsNdIfPol = cobra.model.l3ext.RsNdIfPol(l3extLIfP, tnNdIfPolName=u'')
    #l3extRsPathL3OutAtt = cobra.model.l3ext.RsPathL3OutAtt(l3extLIfP, addr=u'10.60.0.1/24', descr=u'', encapScope=u'local', targetDscp=u'unspecified', llAddr=u'0.0.0.0', autostate=u'disabled', mac=u'00:22:BD:F8:19:FF', mode=u'regular', encap='vlan-'+str(l3outVlan), ifInstT=u'ext-svi', mtu=u'inherit', tDn=u'topology/pod-1/paths-102/pathep-[eth1/46]')
    #l3extRsNodeL3OutAtt = cobra.model.l3ext.RsNodeL3OutAtt(l3extLNodeP, rtrIdLoopBack=u'yes', rtrId=u'5.5.5.5', tDn=u'topology/pod-1/node-102')
    #l3extInfraNodeP = cobra.model.l3ext.InfraNodeP(l3extRsNodeL3OutAtt, fabricExtCtrlPeering=u'no', descr=u'', nameAlias=u'', spineRole=u'', fabricExtIntersiteCtrlPeering=u'no', name=u'')
    #l3extRsEctx = cobra.model.l3ext.RsEctx(l3extOut, tnFvCtxName=u'externalVRF')
    #l3extRsL3DomAtt = cobra.model.l3ext.RsL3DomAtt(l3extOut, tDn=u'uni/l3dom-ACI-Sec-Lab-L3Out3')


    vnsSvcCont = cobra.model.vns.SvcCont(fvTenant)


    # commit the generated code to APIC
    # print toXMLStr(polUni)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)




def addServiceGraphsAsaNgipsSecureDc(md, tenantName='tenantName'):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')

    # build the request using cobra syntax
    fvTenant = cobra.model.fv.Tenant(polUni, ownerKey='', name=tenantName, descr='', nameAlias='', ownerTag='')

    vnsAbsGraph = cobra.model.vns.AbsGraph(fvTenant, ownerKey='', name='ASAv_NGIPSv_SGT', descr='', nameAlias='', ownerTag='', uiTemplateType='UNSPECIFIED')
    vnsAbsTermNodeProv = cobra.model.vns.AbsTermNodeProv(vnsAbsGraph, ownerKey='', ownerTag='', name='T2', descr='', nameAlias='')
    vnsOutTerm = cobra.model.vns.OutTerm(vnsAbsTermNodeProv, name='', descr='', nameAlias='')
    vnsInTerm = cobra.model.vns.InTerm(vnsAbsTermNodeProv, name='', descr='', nameAlias='')
    vnsAbsTermConn = cobra.model.vns.AbsTermConn(vnsAbsTermNodeProv, ownerKey='', attNotify='no', name='1', descr='', nameAlias='', ownerTag='')
    vnsAbsTermNodeCon = cobra.model.vns.AbsTermNodeCon(vnsAbsGraph, ownerKey='', ownerTag='', name='T1', descr='', nameAlias='')
    vnsOutTerm2 = cobra.model.vns.OutTerm(vnsAbsTermNodeCon, name='', descr='', nameAlias='')
    vnsInTerm2 = cobra.model.vns.InTerm(vnsAbsTermNodeCon, name='', descr='', nameAlias='')
    vnsAbsTermConn2 = cobra.model.vns.AbsTermConn(vnsAbsTermNodeCon, ownerKey='', attNotify='no', name='1', descr='', nameAlias='', ownerTag='')
    vnsAbsNode = cobra.model.vns.AbsNode(vnsAbsGraph, funcTemplateType='FW_TRANS', isCopy='no', ownerKey='', managed='yes', name='NGIPS', descr='', funcType='GoTo', shareEncap='no', sequenceNumber='0', routingMode='unspecified', nameAlias='', ownerTag='')
    vnsRsNodeToMFunc = cobra.model.vns.RsNodeToMFunc(vnsAbsNode, tDn='uni/infra/mDev-CISCO-FTD_FI-1.0/mFunc-FTD')
    vnsRsNodeToLDev = cobra.model.vns.RsNodeToLDev(vnsAbsNode, tDn='uni/tn-'+tenantName+'/lDevVip-device-ngipsv')
    vnsRsNodeToAbsFuncProf = cobra.model.vns.RsNodeToAbsFuncProf(vnsAbsNode, tDn='uni/infra/mDev-CISCO-FTD_FI-1.0/absFuncProfContr/absFuncProfGrp-FTDProfiles/absFuncProf-TransparentModeForFTD')
    vnsAbsFuncConn = cobra.model.vns.AbsFuncConn(vnsAbsNode, ownerKey='', attNotify='no', name='provider', descr='', nameAlias='', ownerTag='')
    vnsRsMConnAtt = cobra.model.vns.RsMConnAtt(vnsAbsFuncConn, tDn='uni/infra/mDev-CISCO-FTD_FI-1.0/mFunc-FTD/mConn-internal')
    vnsAbsFuncConn2 = cobra.model.vns.AbsFuncConn(vnsAbsNode, ownerKey='', attNotify='no', name='consumer', descr='', nameAlias='', ownerTag='')
    vnsRsMConnAtt2 = cobra.model.vns.RsMConnAtt(vnsAbsFuncConn2, tDn='uni/infra/mDev-CISCO-FTD_FI-1.0/mFunc-FTD/mConn-external')
    vnsAbsNode2 = cobra.model.vns.AbsNode(vnsAbsGraph, funcTemplateType='FW_TRANS', isCopy='no', ownerKey='', managed='yes', name='ASAv', descr='', funcType='GoTo', shareEncap='no', sequenceNumber='0', routingMode='unspecified', nameAlias='', ownerTag='')
    vnsRsNodeToMFunc2 = cobra.model.vns.RsNodeToMFunc(vnsAbsNode2, tDn='uni/infra/mDev-CISCO-ASA-1.2/mFunc-Firewall')
    vnsRsNodeToLDev2 = cobra.model.vns.RsNodeToLDev(vnsAbsNode2, tDn='uni/tn-'+tenantName+'/lDevVip-dev4-asav')
    vnsRsNodeToAbsFuncProf2 = cobra.model.vns.RsNodeToAbsFuncProf(vnsAbsNode2, tDn='uni/infra/mDev-CISCO-ASA-1.2/absFuncProfContr/absFuncProfGrp-WebServiceProfileGroup/absFuncProf-WebPolicyForTransparentModeIPv4')
    vnsAbsFuncConn3 = cobra.model.vns.AbsFuncConn(vnsAbsNode2, ownerKey='', attNotify='no', name='provider', descr='', nameAlias='', ownerTag='')
    vnsRsMConnAtt3 = cobra.model.vns.RsMConnAtt(vnsAbsFuncConn3, tDn='uni/infra/mDev-CISCO-ASA-1.2/mFunc-Firewall/mConn-internal')
    vnsAbsFuncConn4 = cobra.model.vns.AbsFuncConn(vnsAbsNode2, ownerKey='', attNotify='no', name='consumer', descr='', nameAlias='', ownerTag='')
    vnsRsMConnAtt4 = cobra.model.vns.RsMConnAtt(vnsAbsFuncConn4, tDn='uni/infra/mDev-CISCO-ASA-1.2/mFunc-Firewall/mConn-external')
    vnsAbsConnection = cobra.model.vns.AbsConnection(vnsAbsGraph, adjType='L2', ownerKey='', name='C1', descr='', connDir='provider', connType='external', nameAlias='', ownerTag='', directConnect='no', unicastRoute='yes')
    vnsRsAbsConnectionConns = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection, tDn='uni/tn-'+tenantName+'/AbsGraph-ASAv_NGIPSv_SGT/AbsNode-ASAv/AbsFConn-consumer')
    vnsRsAbsConnectionConns2 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection, tDn='uni/tn-'+tenantName+'/AbsGraph-ASAv_NGIPSv_SGT/AbsTermNodeCon-T1/AbsTConn')
    vnsAbsConnection2 = cobra.model.vns.AbsConnection(vnsAbsGraph, adjType='L2', ownerKey='', name='C3', descr='', connDir='provider', connType='external', nameAlias='', ownerTag='', directConnect='no', unicastRoute='yes')
    vnsRsAbsConnectionConns3 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection2, tDn='uni/tn-'+tenantName+'/AbsGraph-ASAv_NGIPSv_SGT/AbsTermNodeProv-T2/AbsTConn')
    vnsRsAbsConnectionConns4 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection2, tDn='uni/tn-'+tenantName+'/AbsGraph-ASAv_NGIPSv_SGT/AbsNode-NGIPS/AbsFConn-provider')
    vnsAbsConnection3 = cobra.model.vns.AbsConnection(vnsAbsGraph, adjType='L2', ownerKey='', name='C2', descr='', connDir='provider', connType='external', nameAlias='', ownerTag='', directConnect='no', unicastRoute='yes')
    vnsRsAbsConnectionConns5 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection3, tDn='uni/tn-'+tenantName+'/AbsGraph-ASAv_NGIPSv_SGT/AbsNode-NGIPS/AbsFConn-consumer')
    vnsRsAbsConnectionConns6 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection3, tDn='uni/tn-'+tenantName+'/AbsGraph-ASAv_NGIPSv_SGT/AbsNode-ASAv/AbsFConn-provider')

    # commit the generated code to APIC
    # print toXMLStr(polUni)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)


def addServiceGraphsNgipsSecureDc(md, tenantName='tenantName'):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')

    # build the request using cobra syntax
    fvTenant = cobra.model.fv.Tenant(polUni, ownerKey='', name=tenantName, descr='', nameAlias='', ownerTag='')

    vnsAbsGraph2 = cobra.model.vns.AbsGraph(fvTenant, ownerKey='', name='NGIPSv_SGT', descr='', nameAlias='', ownerTag='', uiTemplateType='UNSPECIFIED')
    vnsAbsTermNodeProv2 = cobra.model.vns.AbsTermNodeProv(vnsAbsGraph2, ownerKey='', ownerTag='', name='T2', descr='', nameAlias='')
    vnsOutTerm3 = cobra.model.vns.OutTerm(vnsAbsTermNodeProv2, name='', descr='', nameAlias='')
    vnsInTerm3 = cobra.model.vns.InTerm(vnsAbsTermNodeProv2, name='', descr='', nameAlias='')
    vnsAbsTermConn3 = cobra.model.vns.AbsTermConn(vnsAbsTermNodeProv2, ownerKey='', attNotify='no', name='1', descr='', nameAlias='', ownerTag='')
    vnsAbsTermNodeCon2 = cobra.model.vns.AbsTermNodeCon(vnsAbsGraph2, ownerKey='', ownerTag='', name='T1', descr='', nameAlias='')
    vnsOutTerm4 = cobra.model.vns.OutTerm(vnsAbsTermNodeCon2, name='', descr='', nameAlias='')
    vnsInTerm4 = cobra.model.vns.InTerm(vnsAbsTermNodeCon2, name='', descr='', nameAlias='')
    vnsAbsTermConn4 = cobra.model.vns.AbsTermConn(vnsAbsTermNodeCon2, ownerKey='', attNotify='no', name='1', descr='', nameAlias='', ownerTag='')
    vnsAbsNode3 = cobra.model.vns.AbsNode(vnsAbsGraph2, funcTemplateType='FW_TRANS', isCopy='no', ownerKey='', managed='yes', name='NGIPS', descr='', funcType='GoTo', shareEncap='no', sequenceNumber='0', routingMode='unspecified', nameAlias='', ownerTag='')
    vnsRsNodeToMFunc3 = cobra.model.vns.RsNodeToMFunc(vnsAbsNode3, tDn='uni/infra/mDev-CISCO-FTD_FI-1.0/mFunc-FTD')
    vnsRsNodeToLDev3 = cobra.model.vns.RsNodeToLDev(vnsAbsNode3, tDn='uni/tn-'+tenantName+'/lDevVip-device-ngipsv')
    vnsRsNodeToAbsFuncProf3 = cobra.model.vns.RsNodeToAbsFuncProf(vnsAbsNode3, tDn='uni/infra/mDev-CISCO-FTD_FI-1.0/absFuncProfContr/absFuncProfGrp-FTDProfiles/absFuncProf-TransparentModeForFTD')
    vnsAbsFuncConn5 = cobra.model.vns.AbsFuncConn(vnsAbsNode3, ownerKey='', attNotify='no', name='consumer', descr='', nameAlias='', ownerTag='')
    vnsRsMConnAtt5 = cobra.model.vns.RsMConnAtt(vnsAbsFuncConn5, tDn='uni/infra/mDev-CISCO-FTD_FI-1.0/mFunc-FTD/mConn-external')
    vnsAbsFuncConn6 = cobra.model.vns.AbsFuncConn(vnsAbsNode3, ownerKey='', attNotify='no', name='provider', descr='', nameAlias='', ownerTag='')
    vnsRsMConnAtt6 = cobra.model.vns.RsMConnAtt(vnsAbsFuncConn6, tDn='uni/infra/mDev-CISCO-FTD_FI-1.0/mFunc-FTD/mConn-internal')
    vnsAbsConnection4 = cobra.model.vns.AbsConnection(vnsAbsGraph2, adjType='L2', ownerKey='', name='C2', descr='', connDir='provider', connType='external', nameAlias='', ownerTag='', directConnect='no', unicastRoute='yes')
    vnsRsAbsConnectionConns7 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection4, tDn='uni/tn-'+tenantName+'/AbsGraph-NGIPSv_SGT/AbsTermNodeProv-T2/AbsTConn')
    vnsRsAbsConnectionConns8 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection4, tDn='uni/tn-'+tenantName+'/AbsGraph-NGIPSv_SGT/AbsNode-NGIPS/AbsFConn-provider')
    vnsAbsConnection5 = cobra.model.vns.AbsConnection(vnsAbsGraph2, adjType='L2', ownerKey='', name='C1', descr='', connDir='provider', connType='external', nameAlias='', ownerTag='', directConnect='no', unicastRoute='yes')
    vnsRsAbsConnectionConns9 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection5, tDn='uni/tn-'+tenantName+'/AbsGraph-NGIPSv_SGT/AbsNode-NGIPS/AbsFConn-consumer')
    vnsRsAbsConnectionConns10 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection5, tDn='uni/tn-'+tenantName+'/AbsGraph-NGIPSv_SGT/AbsTermNodeCon-T1/AbsTConn')

    # commit the generated code to APIC
    # print toXMLStr(polUni)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)



def addServiceGraphsAsaSecureDc(md, tenantName='tenantName'):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')

    # build the request using cobra syntax
    fvTenant = cobra.model.fv.Tenant(polUni, ownerKey='', name=tenantName, descr='', nameAlias='', ownerTag='')

    vnsAbsGraph3 = cobra.model.vns.AbsGraph(fvTenant, ownerKey='', name='ASAv_SGT', descr='', nameAlias='', ownerTag='', uiTemplateType='UNSPECIFIED')
    vnsAbsTermNodeProv3 = cobra.model.vns.AbsTermNodeProv(vnsAbsGraph3, ownerKey='', ownerTag='', name='T2', descr='', nameAlias='')
    vnsOutTerm5 = cobra.model.vns.OutTerm(vnsAbsTermNodeProv3, name='', descr='', nameAlias='')
    vnsInTerm5 = cobra.model.vns.InTerm(vnsAbsTermNodeProv3, name='', descr='', nameAlias='')
    vnsAbsTermConn5 = cobra.model.vns.AbsTermConn(vnsAbsTermNodeProv3, ownerKey='', attNotify='no', name='1', descr='', nameAlias='', ownerTag='')
    vnsAbsTermNodeCon3 = cobra.model.vns.AbsTermNodeCon(vnsAbsGraph3, ownerKey='', ownerTag='', name='T1', descr='', nameAlias='')
    vnsOutTerm6 = cobra.model.vns.OutTerm(vnsAbsTermNodeCon3, name='', descr='', nameAlias='')
    vnsInTerm6 = cobra.model.vns.InTerm(vnsAbsTermNodeCon3, name='', descr='', nameAlias='')
    vnsAbsTermConn6 = cobra.model.vns.AbsTermConn(vnsAbsTermNodeCon3, ownerKey='', attNotify='no', name='1', descr='', nameAlias='', ownerTag='')
    vnsAbsNode4 = cobra.model.vns.AbsNode(vnsAbsGraph3, funcTemplateType='FW_TRANS', isCopy='no', ownerKey='', managed='yes', name='ASAv', descr='', funcType='GoTo', shareEncap='no', sequenceNumber='0', routingMode='unspecified', nameAlias='', ownerTag='')
    vnsRsNodeToMFunc4 = cobra.model.vns.RsNodeToMFunc(vnsAbsNode4, tDn='uni/infra/mDev-CISCO-ASA-1.2/mFunc-Firewall')
    vnsRsNodeToLDev4 = cobra.model.vns.RsNodeToLDev(vnsAbsNode4, tDn='uni/tn-'+tenantName+'/lDevVip-dev4-asav')
    vnsRsNodeToAbsFuncProf4 = cobra.model.vns.RsNodeToAbsFuncProf(vnsAbsNode4, tDn='uni/infra/mDev-CISCO-ASA-1.2/absFuncProfContr/absFuncProfGrp-WebServiceProfileGroup/absFuncProf-WebPolicyForTransparentModeIPv4')
    vnsAbsFuncConn7 = cobra.model.vns.AbsFuncConn(vnsAbsNode4, ownerKey='', attNotify='no', name='consumer', descr='', nameAlias='', ownerTag='')
    vnsRsMConnAtt7 = cobra.model.vns.RsMConnAtt(vnsAbsFuncConn7, tDn='uni/infra/mDev-CISCO-ASA-1.2/mFunc-Firewall/mConn-external')
    vnsAbsFuncConn8 = cobra.model.vns.AbsFuncConn(vnsAbsNode4, ownerKey='', attNotify='no', name='provider', descr='', nameAlias='', ownerTag='')
    vnsRsMConnAtt8 = cobra.model.vns.RsMConnAtt(vnsAbsFuncConn8, tDn='uni/infra/mDev-CISCO-ASA-1.2/mFunc-Firewall/mConn-internal')
    vnsAbsConnection6 = cobra.model.vns.AbsConnection(vnsAbsGraph3, adjType='L2', ownerKey='', name='C2', descr='', connDir='provider', connType='external', nameAlias='', ownerTag='', directConnect='no', unicastRoute='yes')
    vnsRsAbsConnectionConns11 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection6, tDn='uni/tn-'+tenantName+'/AbsGraph-ASAv_SGT/AbsTermNodeProv-T2/AbsTConn')
    vnsRsAbsConnectionConns12 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection6, tDn='uni/tn-'+tenantName+'/AbsGraph-ASAv_SGT/AbsNode-ASAv/AbsFConn-provider')
    vnsAbsConnection7 = cobra.model.vns.AbsConnection(vnsAbsGraph3, adjType='L2', ownerKey='', name='C1', descr='', connDir='provider', connType='external', nameAlias='', ownerTag='', directConnect='no', unicastRoute='yes')
    vnsRsAbsConnectionConns13 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection7, tDn='uni/tn-'+tenantName+'/AbsGraph-ASAv_SGT/AbsNode-ASAv/AbsFConn-consumer')
    vnsRsAbsConnectionConns14 = cobra.model.vns.RsAbsConnectionConns(vnsAbsConnection7, tDn='uni/tn-'+tenantName+'/AbsGraph-ASAv_SGT/AbsTermNodeCon-T1/AbsTConn')

    # commit the generated code to APIC
    # print toXMLStr(polUni)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)


    #fvBD = cobra.model.fv.BD(fvTenant, multiDstPktAct='bd-flood', mcastAllow='no', limitIpLearnToSubnets='no', unicastRoute='yes', unkMcastAct='flood', descr='', llAddr='::', nameAlias='', type='regular', ipLearning='yes', vmac='not-applicable', mac='00:22:BD:F8:19:FF', epMoveDetectMode='', ownerTag='', intersiteBumTrafficAllow='no', ownerKey='', name='opencart_bd', epClear='no', unkMacUcastAct='proxy', arpFlood='no', intersiteL2Stretch='no', OptimizeWanBandwidth='no')
    #fvSubnet = cobra.model.fv.Subnet(fvBD, name='', descr='', ctrl='', ip='10.105.0.1/24', preferred='no', virtual='no', nameAlias='')
    #fvRsIgmpsn = cobra.model.fv.RsIgmpsn(fvBD, tnIgmpSnoopPolName='')
    #fvRsCtx = cobra.model.fv.RsCtx(fvBD, tnFvCtxName='opencart_vrf')
    #fvRsBdToEpRet = cobra.model.fv.RsBdToEpRet(fvBD, resolveAct='resolve', tnFvEpRetPolName='')
    #fvRsBDToNdP = cobra.model.fv.RsBDToNdP(fvBD, tnNdIfPolName='')


    #fvCtx = cobra.model.fv.Ctx(fvTenant, ownerKey='', bdEnforcedEnable='no', descr='', knwMcastAct='permit', pcEnfDir='ingress', nameAlias='', ownerTag='', pcEnfPref='enforced', name='opencart_vrf')
    #vzAny = cobra.model.vz.Any(fvCtx, prefGrMemb='disabled', matchT='AtleastOne', name='', descr='', nameAlias='')
    #fvRsOspfCtxPol = cobra.model.fv.RsOspfCtxPol(fvCtx, tnOspfCtxPolName='')
    #fvRsCtxToEpRet = cobra.model.fv.RsCtxToEpRet(fvCtx, tnFvEpRetPolName='')
    #fvRsCtxToExtRouteTagPol = cobra.model.fv.RsCtxToExtRouteTagPol(fvCtx, tnL3extRouteTagPolName='')
    #fvRsBgpCtxPol = cobra.model.fv.RsBgpCtxPol(fvCtx, tnBgpCtxPolName='')


def addDeviceNgipsSecureDc(md, tenantName='tenantName',ngipsIp='198.19.193.110'):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')

    # build the request using cobra syntax
    fvTenant = cobra.model.fv.Tenant(polUni, ownerKey='', name=tenantName, descr='', nameAlias='', ownerTag='')

    vnsLDevVip = cobra.model.vns.LDevVip(fvTenant, isCopy='no', managed='yes', name='device-ngipsv', svcType='FW', funcType='GoTo', devtype='VIRTUAL', packageModel='VIRTUAL', contextAware='single-Context', nameAlias='', trunking='no', mode='legacy-Mode')
    vnsLIf = cobra.model.vns.LIf(vnsLDevVip, name='provider', encap='unknown', nameAlias='')
    vnsRsMetaIf = cobra.model.vns.RsMetaIf(vnsLIf, isConAndProv='no', tDn='uni/infra/mDev-CISCO-FTD_FI-1.0/mIfLbl-internal')
    vnsRsCIfAttN = cobra.model.vns.RsCIfAttN(vnsLIf, tDn='uni/tn-'+tenantName+'/lDevVip-device-ngipsv/cDev-Device1/cIf-[GigabitEthernet0/1]')
    vnsLIf2 = cobra.model.vns.LIf(vnsLDevVip, name='consumer', encap='unknown', nameAlias='')
    vnsRsMetaIf2 = cobra.model.vns.RsMetaIf(vnsLIf2, isConAndProv='no', tDn='uni/infra/mDev-CISCO-FTD_FI-1.0/mIfLbl-external')
    vnsRsCIfAttN2 = cobra.model.vns.RsCIfAttN(vnsLIf2, tDn='uni/tn-'+tenantName+'/lDevVip-device-ngipsv/cDev-Device1/cIf-[GigabitEthernet0/0]')
    vnsRsALDevToDomP = cobra.model.vns.RsALDevToDomP(vnsLDevVip, tDn='uni/vmmp-VMware/dom-vmware60-hybridcloud')
    vnsCMgmt = cobra.model.vns.CMgmt(vnsLDevVip, host=ngipsIp, name='', nameAlias='', port='443')
    vnsCDev = cobra.model.vns.CDev(vnsLDevVip, vcenterName='vc60-aci-1', vmName=tenantName+'-ngipsv', name='Device1', nameAlias='', devCtxLbl='')
    vnsCMgmt2 = cobra.model.vns.CMgmt(vnsCDev, host=ngipsIp, name='', nameAlias='', port='443')
    vnsCIf = cobra.model.vns.CIf(vnsCDev, name='GigabitEthernet0/1', nameAlias='', vnicName='Network adapter 3')
    vnsCIf2 = cobra.model.vns.CIf(vnsCDev, name='GigabitEthernet0/0', nameAlias='', vnicName='Network adapter 2')
    vnsCCredSecret = cobra.model.vns.CCredSecret(vnsCDev,value='C1sco12345', name='password', nameAlias='')
    vnsCCred = cobra.model.vns.CCred(vnsCDev, value='admin', name='username', nameAlias='')
    vnsCCredSecret2 = cobra.model.vns.CCredSecret(vnsLDevVip,value='C1sco12345', name='password', nameAlias='')
    vnsRsMDevAtt = cobra.model.vns.RsMDevAtt(vnsLDevVip, tDn='uni/infra/mDev-CISCO-FTD_FI-1.0')
    vnsCCred2 = cobra.model.vns.CCred(vnsLDevVip, value='admin', name='username', nameAlias='')

    # commit the generated code to APIC
    # print toXMLStr(polUni)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)



def addDeviceAsaSecureDc(md, tenantName='tenantName',asaIp='198.19.193.111'):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')

    # build the request using cobra syntax
    fvTenant = cobra.model.fv.Tenant(polUni, ownerKey='', name=tenantName, descr='', nameAlias='', ownerTag='')
    vnsLDevVip2 = cobra.model.vns.LDevVip(fvTenant, isCopy='no', managed='yes', name='device-asav', svcType='FW', funcType='GoTo', devtype='VIRTUAL', packageModel='ASAv', contextAware='single-Context', nameAlias='', trunking='no', mode='legacy-Mode')
    vnsLIf3 = cobra.model.vns.LIf(vnsLDevVip2, name='provider', encap='unknown', nameAlias='')
    vnsRsMetaIf3 = cobra.model.vns.RsMetaIf(vnsLIf3, isConAndProv='no', tDn='uni/infra/mDev-CISCO-ASA-1.2/mIfLbl-internal')
    vnsRsCIfAttN3 = cobra.model.vns.RsCIfAttN(vnsLIf3, tDn='uni/tn-'+tenantName+'/lDevVip-device-asav/cDev-Device1/cIf-[GigabitEthernet0/1]')
    vnsLIf4 = cobra.model.vns.LIf(vnsLDevVip2, name='consumer', encap='unknown', nameAlias='')
    vnsRsMetaIf4 = cobra.model.vns.RsMetaIf(vnsLIf4, isConAndProv='no', tDn='uni/infra/mDev-CISCO-ASA-1.2/mIfLbl-external')
    vnsRsCIfAttN4 = cobra.model.vns.RsCIfAttN(vnsLIf4, tDn='uni/tn-'+tenantName+'/lDevVip-device-asav/cDev-Device1/cIf-[GigabitEthernet0/0]')
    vnsRsALDevToDomP2 = cobra.model.vns.RsALDevToDomP(vnsLDevVip2, tDn='uni/vmmp-VMware/dom-vmware60-hybridcloud')
    vnsCMgmt3 = cobra.model.vns.CMgmt(vnsLDevVip2, host=asaIp, name='', nameAlias='', port='443')
    vnsCDev2 = cobra.model.vns.CDev(vnsLDevVip2, vcenterName='vc60-aci-1', vmName=tenantName+'-asav', name='Device1', nameAlias='', devCtxLbl='')
    vnsCMgmt4 = cobra.model.vns.CMgmt(vnsCDev2, host=asaIp, name='', nameAlias='', port='443')
    vnsCIf3 = cobra.model.vns.CIf(vnsCDev2, name='GigabitEthernet0/1', nameAlias='', vnicName='Network adapter 3')
    vnsCIf4 = cobra.model.vns.CIf(vnsCDev2, name='GigabitEthernet0/0', nameAlias='', vnicName='Network adapter 2')
    vnsCCredSecret3 = cobra.model.vns.CCredSecret(vnsCDev2,value='C1sco12345', name='password', nameAlias='')
    vnsCCred3 = cobra.model.vns.CCred(vnsCDev2, value='admin', name='username', nameAlias='')
    vnsCCredSecret4 = cobra.model.vns.CCredSecret(vnsLDevVip2,value='C1sco12345', name='password', nameAlias='')
    vnsRsMDevAtt2 = cobra.model.vns.RsMDevAtt(vnsLDevVip2, tDn='uni/infra/mDev-CISCO-ASA-1.2')
    vnsCCred4 = cobra.model.vns.CCred(vnsLDevVip2, value='admin', name='username', nameAlias='')

    # commit the generated code to APIC
    # print toXMLStr(polUni)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)





def createTenantContiv(md, tenantName='tenantName'):

    # the top level object on which operations will be made
    topMo = cobra.model.pol.Uni('')

    fvTenant = cobra.model.fv.Tenant(topMo, ownerKey=u'', name=tenantName, descr=u'', ownerTag=u'')
    aaaDomainRef = cobra.model.aaa.DomainRef(fvTenant, ownerKey=u'', name=u'SecDo-'+tenantName, descr=u'', ownerTag=u'')
    #vnsSvcCont = cobra.model.vns.SvcCont(fvTenant)
    #fvRsTenantMonPol = cobra.model.fv.RsTenantMonPol(fvTenant, tRn=u'monepg-default', tDn=u'uni/tn-common/monepg-default', rType=u'mo', tCl=u'monEPGPol', tContextDn=u'', forceResolve=u'yes', tType=u'name', tnMonEPGPolName=u'')

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)




def createTenantAsapdc(md, tenantName='tenantName'):
    # the top level object on which operations will be made
    topMo = cobra.model.pol.Uni('')


    # build the request using cobra syntax
    fvTenant = Tenant(topMo,tenantName)
    aaaDomainRef = cobra.model.aaa.DomainRef(fvTenant, ownerKey=u'', name=u'SecDo-'+tenantName, descr=u'', ownerTag=u'')
    fvBD = cobra.model.fv.BD(fvTenant, ownerKey=u'', vmac=u'not-applicable', unkMcastAct=u'flood', name=u'myBD', descr=u'', unkMacUcastAct=u'proxy', arpFlood=u'no', limitIpLearnToSubnets=u'no', llAddr=u'::', mcastAllow=u'no', mac=u'00:22:BD:F8:19:FF', epMoveDetectMode=u'', unicastRoute=u'yes', ownerTag=u'', multiDstPktAct=u'bd-flood', type=u'regular', ipLearning=u'yes')
    fvRsBDToNdP = cobra.model.fv.RsBDToNdP(fvBD, tnNdIfPolName=u'')
    fvRsCtx = cobra.model.fv.RsCtx(fvBD, tnFvCtxName=u'myVRF')
    fvRsIgmpsn = cobra.model.fv.RsIgmpsn(fvBD, tnIgmpSnoopPolName=u'')
    fvRsBdToEpRet = cobra.model.fv.RsBdToEpRet(fvBD, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvCtx = cobra.model.fv.Ctx(fvTenant, ownerKey=u'', name=u'myVRF', descr=u'', knwMcastAct=u'permit', pcEnfDir=u'ingress', ownerTag=u'', pcEnfPref=u'enforced')
    fvRsBgpCtxPol = cobra.model.fv.RsBgpCtxPol(fvCtx, tnBgpCtxPolName=u'')
    fvRsCtxToExtRouteTagPol = cobra.model.fv.RsCtxToExtRouteTagPol(fvCtx, tnL3extRouteTagPolName=u'')
    fvRsOspfCtxPol = cobra.model.fv.RsOspfCtxPol(fvCtx, tnOspfCtxPolName=u'')
    vzAny = cobra.model.vz.Any(fvCtx, matchT=u'AtleastOne', name=u'', descr=u'')
    fvRsCtxToEpRet = cobra.model.fv.RsCtxToEpRet(fvCtx, tnFvEpRetPolName=u'')
    vnsSvcCont = cobra.model.vns.SvcCont(fvTenant)
    fvAp = cobra.model.fv.Ap(fvTenant, ownerKey=u'', prio=u'unspecified', name=u'myApp', descr=u'', ownerTag=u'')
    fvAEPg = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Web', prio=u'unspecified', descr=u'', pcEnfPref=u'unenforced')
    fvRsBd = cobra.model.fv.RsBd(fvAEPg, tnFvBDName=u'myBD')
    fvRsCustQosPol = cobra.model.fv.RsCustQosPol(fvAEPg, tnQosCustomPolName=u'')
    fvAEPg2 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'Db', prio=u'unspecified', descr=u'', pcEnfPref=u'unenforced')
    fvRsBd2 = cobra.model.fv.RsBd(fvAEPg2, tnFvBDName=u'myBD')
    fvRsCustQosPol2 = cobra.model.fv.RsCustQosPol(fvAEPg2, tnQosCustomPolName=u'')
    fvAEPg3 = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=u'App', prio=u'unspecified', descr=u'', pcEnfPref=u'unenforced')
    fvRsBd3 = cobra.model.fv.RsBd(fvAEPg3, tnFvBDName=u'myBD')
    fvRsCustQosPol3 = cobra.model.fv.RsCustQosPol(fvAEPg3, tnQosCustomPolName=u'')
    fvRsTenantMonPol = cobra.model.fv.RsTenantMonPol(fvTenant, tnMonEPGPolName=u'')

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)



def createUserWithoutDomainAll(md,userName='userName'):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    aaaUserEp = cobra.model.aaa.UserEp(polUni)

    # build the request using cobra syntax
    aaaUser = cobra.model.aaa.User(aaaUserEp, ownerKey=u'', expires=u'no', firstName=userName, descr=u'', lastName=u'', clearPwdHistory=u'no', accountStatus=u'active', phone=u'', pwdLifeTime=u'no-password-expire', expiration=u'never', ownerTag=u'', email=u'', name=userName, pwd=userName)

    aaaUserDomain = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'SecDo-'+userName, descr=u'', ownerTag=u'')
    aaaUserRole = cobra.model.aaa.UserRole(aaaUserDomain, ownerKey=u'', privType=u'writePriv', name=u'admin', descr=u'', ownerTag=u'')
    aaaUserDomain2 = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'ACI-Security-Integration', descr=u'', ownerTag=u'')
    aaaUserRole2 = cobra.model.aaa.UserRole(aaaUserDomain2, ownerKey=u'', privType=u'readPriv', name=u'read-all', descr=u'', ownerTag=u'')
    aaaUserDomain3 = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'vmware60-hybridcloud', descr=u'', ownerTag=u'')
    aaaUserRole3 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'admin', descr=u'', ownerTag=u'')

    aaaUserDomain4 = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'all', descr=u'', ownerTag=u'')
    aaaUserRole4 = cobra.model.aaa.UserRole(aaaUserDomain4, ownerKey=u'', privType=u'readPriv', name=u'read-all', descr=u'', ownerTag=u'')

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(aaaUserEp)
    md.commit(c)


def createUserWithoutDomainAllWithVmware(md,userName='userName'):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    aaaUserEp = cobra.model.aaa.UserEp(polUni)

    # build the request using cobra syntax
    aaaUser = cobra.model.aaa.User(aaaUserEp, ownerKey=u'', expires=u'no', firstName=userName, descr=u'', lastName=u'', clearPwdHistory=u'no', accountStatus=u'active', phone=u'', pwdLifeTime=u'no-password-expire', expiration=u'never', ownerTag=u'', email=u'', name=userName, pwd=userName)

    aaaUserDomain = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'SecDo-'+userName, descr=u'', ownerTag=u'')
    aaaUserRole = cobra.model.aaa.UserRole(aaaUserDomain, ownerKey=u'', privType=u'writePriv', name=u'admin', descr=u'', ownerTag=u'')
    aaaUserDomain3 = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'vmware60-hybridcloud', descr=u'', ownerTag=u'')
    aaaUserRole3 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'admin', descr=u'', ownerTag=u'')
    aaaUserDomain4 = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'all', descr=u'', ownerTag=u'')
    aaaUserRole4 = cobra.model.aaa.UserRole(aaaUserDomain4, ownerKey=u'', privType=u'readPriv', name=u'read-all', descr=u'', ownerTag=u'')

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(aaaUserEp)
    md.commit(c)




def createUser(md,userName='userName'):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    aaaUserEp = cobra.model.aaa.UserEp(polUni)

    # build the request using cobra syntax
    aaaUser = cobra.model.aaa.User(aaaUserEp, ownerKey=u'', expires=u'no', firstName=userName, descr=u'', lastName=u'', clearPwdHistory=u'no', accountStatus=u'active', phone=u'', pwdLifeTime=u'no-password-expire', expiration=u'never', ownerTag=u'', email=u'', name=userName, pwd=userName)

    aaaUserDomain = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'SecDo-'+userName, descr=u'', ownerTag=u'')
    aaaUserRole = cobra.model.aaa.UserRole(aaaUserDomain, ownerKey=u'', privType=u'writePriv', name=u'admin', descr=u'', ownerTag=u'')
    aaaUserDomain2 = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'common', descr=u'', ownerTag=u'')
    aaaUserRole2 = cobra.model.aaa.UserRole(aaaUserDomain2, ownerKey=u'', privType=u'readPriv', name=u'read-all', descr=u'', ownerTag=u'')
    aaaUserDomain3 = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'all', descr=u'', ownerTag=u'')
    aaaUserRole3 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'aaa', descr=u'', ownerTag=u'')
    aaaUserRole4 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'fabric-admin', descr=u'', ownerTag=u'')
    aaaUserRole5 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'vmm-admin', descr=u'', ownerTag=u'')
    aaaUserRole6 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'tenant-ext-admin', descr=u'', ownerTag=u'')
    aaaUserRole7 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'ops', descr=u'', ownerTag=u'')
    aaaUserRole8 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'read-all', descr=u'', ownerTag=u'')
    aaaUserRole9 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'nw-svc-admin', descr=u'', ownerTag=u'')
    aaaUserRole10 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'admin', descr=u'', ownerTag=u'')
    aaaUserRole11 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'access-admin', descr=u'', ownerTag=u'')
    aaaUserRole12 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'tenant-admin', descr=u'', ownerTag=u'')

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(aaaUserEp)
    md.commit(c)




def createContract(md, tenantName="common", contractName="myContract", subjectName="allow_all", filterName="allow_all"):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)

    # build the request using cobra syntax
    vzBrCP = cobra.model.vz.BrCP(fvTenant, ownerKey=u'', name=contractName, descr=u'', targetDscp=u'unspecified', nameAlias=u'', ownerTag=u'', prio=u'unspecified')
    vzSubj = cobra.model.vz.Subj(vzBrCP, revFltPorts=u'yes', name='http_icmp', prio=u'unspecified', targetDscp=u'unspecified', nameAlias=u'', descr=u'', consMatchT=u'AtleastOne', provMatchT=u'AtleastOne')
    vzRsSubjFiltAtt1 = cobra.model.vz.RsSubjFiltAtt(vzSubj, directives=u'', tnVzFilterName=u'icmp')
    vzRsSubjFiltAtt2 = cobra.model.vz.RsSubjFiltAtt(vzSubj, directives=u'', tnVzFilterName=u'port_80')

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)


def deleteContract(md, tenantName="common", contractName="myContract"):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)

    # build the request using cobra syntax
    vzBrCP = cobra.model.vz.BrCP(fvTenant, status='deleted', ownerKey=u'', name=contractName, descr=u'', targetDscp=u'unspecified', nameAlias=u'', ownerTag=u'', prio=u'unspecified')

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)





def createContractL3Out(md, tenantName='', l3outName='', networkName='',contractName=''):
   # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)
    l3extOut = cobra.model.l3ext.Out(fvTenant, l3outName)

    # build the request using cobra syntax

    l3extInstP = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=networkName, descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsProv = cobra.model.fv.RsProv(l3extInstP, tnVzBrCPName=contractName, matchT=u'AtleastOne', prio=u'unspecified')
    fvRsCons = cobra.model.fv.RsCons(l3extInstP, tnVzBrCPName=contractName, prio=u'unspecified')


    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(l3extOut)
    md.commit(c)


def deleteContractL3Out(md, tenantName='', l3outName='', networkName='',contractName=''):
   # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)
    l3extOut = cobra.model.l3ext.Out(fvTenant, l3outName)

    # build the request using cobra syntax

    l3extInstP = cobra.model.l3ext.InstP(l3extOut, matchT=u'AtleastOne', name=networkName, descr=u'', targetDscp=u'unspecified', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified')
    fvRsProv = cobra.model.fv.RsProv(l3extInstP, tnVzBrCPName=contractName, status='deleted')
    fvRsCons = cobra.model.fv.RsCons(l3extInstP, tnVzBrCPName=contractName, status='deleted')


    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(l3extOut)
    md.commit(c)










def removeBdFromTenant(md,bdName='',tenantName='' ):

    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)

    # build the request using cobra syntax
    fvBD = cobra.model.fv.BD(fvTenant, name=bdName, status='deleted')

    # commit the generated code to APIC
    # print toXMLStr(fvTenant)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)



def addBdToTenant(md,bdName='',ctxName='',tenantName='' ):

    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)

    # build the request using cobra syntax

    fvBD = cobra.model.fv.BD(fvTenant, multiDstPktAct=u'bd-flood', mcastAllow=u'no', limitIpLearnToSubnets=u'yes', unicastRoute=u'yes', unkMcastAct=u'flood', descr=u'', llAddr=u'::', type=u'regular', ipLearning=u'yes', vmac=u'not-applicable', nameAlias=u'', epMoveDetectMode=u'', ownerTag=u'', intersiteBumTrafficAllow=u'no', ownerKey=u'', name=bdName, epClear=u'no', unkMacUcastAct=u'proxy', arpFlood=u'no', intersiteL2Stretch=u'no',  OptimizeWanBandwidth=u'no')
    fvRsBDToNdP = cobra.model.fv.RsBDToNdP(fvBD, tnNdIfPolName=u'')
    fvRsBdToEpRet = cobra.model.fv.RsBdToEpRet(fvBD, resolveAct=u'resolve', tnFvEpRetPolName=u'')
    fvRsCtx = cobra.model.fv.RsCtx(fvBD, tnFvCtxName=ctxName)
    fvRsIgmpsn = cobra.model.fv.RsIgmpsn(fvBD, tnIgmpSnoopPolName=u'')
    fvSubnet = cobra.model.fv.Subnet(fvBD, name=u'', descr=u'', ip=u'10.0.0.2/24', ctrl=u'', preferred=u'no', virtual=u'no', nameAlias=u'')
    #igmpIfP = cobra.model.igmp.IfP(fvBD, name=u'', descr=u'', nameAlias=u'')


    # commit the generated code to APIC
    # print toXMLStr(fvTenant)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)

def removeCtxFromTenant(md,ctxName='',tenantName=''):
    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)

    fvCtx = cobra.model.fv.Ctx(fvTenant, name=ctxName, status='deleted')

    # commit the generated code to APIC
    #print toXMLStr(fvTenant)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)


def addCtxToTenant(md,ctxName='',tenantName=''):

    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)

    # build the request using cobra syntax
    fvCtx = cobra.model.fv.Ctx(fvTenant, ownerKey=u'', bdEnforcedEnable=u'no', descr=u'', knwMcastAct=u'permit', pcEnfDir=u'ingress', name=ctxName, ownerTag=u'', nameAlias=u'', pcEnfPref=u'enforced')

    fvRsBgpCtxPol = cobra.model.fv.RsBgpCtxPol(fvCtx, tnBgpCtxPolName=u'')
    fvRsCtxToExtRouteTagPol = cobra.model.fv.RsCtxToExtRouteTagPol(fvCtx, tnL3extRouteTagPolName=u'')
    fvRsCtxToEpRet = cobra.model.fv.RsCtxToEpRet(fvCtx, tnFvEpRetPolName=u'')
    fvRsOspfCtxPol = cobra.model.fv.RsOspfCtxPol(fvCtx, tnOspfCtxPolName=u'')
    #vzAny = cobra.model.vz.Any(fvCtx, matchT=u'AtleastOne', name=u'', descr=u'', nameAlias=u'', prefGrMemb=u'disabled', pcTag=u'any', useAnyDef=u'yes')
    fvRsVrfValidationPol = cobra.model.fv.RsVrfValidationPol(fvCtx, tnL3extVrfValidationPolName=u'')

    # commit the generated code to APIC
    #print toXMLStr(fvTenant)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvTenant)
    md.commit(c)


def removeAppProfFromTenant(md, tenantName='', appProfName='', bdName=''):

    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)
    fvAp = cobra.model.fv.Ap(fvTenant, appProfName, status='deleted')

    # commit the generated code to APIC
    #print toXMLStr(fvAp)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvAp)
    md.commit(c)


def addAppProfToTenant(md, tenantName='ACI-Security-Integration', appProfName='MyApp', bdName='MyBd' , epgName='myEpg',vlanId='14'):

    polUni = cobra.model.pol.Uni('')
    fvTenant = cobra.model.fv.Tenant(polUni, tenantName)

    fvAp = cobra.model.fv.Ap(fvTenant, appProfName)

    # build the request using cobra syntax
    fvAEPg = cobra.model.fv.AEPg(fvAp, isAttrBasedEPg=u'no', matchT=u'AtleastOne', name=epgName, descr=u'', fwdCtrl=u'', prefGrMemb=u'exclude', floodOnEncap=u'disabled', nameAlias=u'', prio=u'unspecified', pcEnfPref=u'unenforced')
    fvRsBd = cobra.model.fv.RsBd(fvAEPg, tnFvBDName=bdName)
    fvRsCustQosPol = cobra.model.fv.RsCustQosPol(fvAEPg, tnQosCustomPolName=u'')

    fvRsDomAtt = cobra.model.fv.RsDomAtt(fvAEPg, tDn=u'uni/vmmp-VMware/dom-vmware60-hybridcloud', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')
    vmmSecP = cobra.model.vmm.SecP(fvRsDomAtt, ownerKey=u'', name=u'', descr=u'', forgedTransmits=u'accept', ownerTag=u'', allowPromiscuous=u'accept', macChanges=u'accept')


    fvRsDomAtt2 = cobra.model.fv.RsDomAtt(fvAEPg, tDn=u'uni/phys-ASA5525-X-1-MGMT', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')
    fvRsDomAtt3 = cobra.model.fv.RsDomAtt(fvAEPg, tDn=u'uni/phys-ASA5525-X-2-MGMT', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')
    fvRsDomAtt4 = cobra.model.fv.RsDomAtt(fvAEPg, tDn=u'uni/phys-ASA5525-X-3-MGMT', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')
    fvRsDomAtt5 = cobra.model.fv.RsDomAtt(fvAEPg, tDn=u'uni/phys-ASA5525-X-4-MGMT', netflowDir=u'both', epgCos=u'Cos0', classPref=u'encap', primaryEncap=u'unknown', secondaryEncapInner=u'unknown', resImedcy=u'immediate', delimiter=u'', instrImedcy=u'lazy', primaryEncapInner=u'unknown', encap=u'unknown', switchingMode=u'native', encapMode=u'auto', netflowPref=u'disabled', epgCosPref=u'disabled')

    #fvRsPathAtt = cobra.model.fv.RsPathAtt(fvAEPg, tDn=u'topology/pod-1/protpaths-101-102/pathep-[ASA5525-X-1-2-DATA]', descr=u'', primaryEncap=u'unknown', instrImedcy=u'lazy', mode=u'regular', encap=u'vlan-'+vlanId)
    fvRsPathAtt2 = cobra.model.fv.RsPathAtt(fvAEPg, tDn=u'topology/pod-1/paths-101/pathep-[ASA5525-X-4-MGMT]', descr=u'', primaryEncap=u'unknown', instrImedcy=u'lazy', mode=u'regular', encap=u'vlan-'+vlanId)
    #fvRsPathAtt3 = cobra.model.fv.RsPathAtt(fvAEPg, tDn=u'topology/pod-1/protpaths-101-102/pathep-[ASA5525-X-3-4-DATA]', descr=u'', primaryEncap=u'unknown', instrImedcy=u'lazy', mode=u'regular', encap=u'vlan-'+vlanId)
    fvRsPathAtt4 = cobra.model.fv.RsPathAtt(fvAEPg, tDn=u'topology/pod-1/paths-101/pathep-[ASA5525-X-1-MGMT]', descr=u'', primaryEncap=u'unknown', instrImedcy=u'lazy', mode=u'regular', encap=u'vlan-'+vlanId)
    fvRsPathAtt5 = cobra.model.fv.RsPathAtt(fvAEPg, tDn=u'topology/pod-1/paths-101/pathep-[ASA5525-X-2-MGMT]', descr=u'', primaryEncap=u'unknown', instrImedcy=u'lazy', mode=u'regular', encap=u'vlan-'+vlanId)
    fvRsPathAtt6 = cobra.model.fv.RsPathAtt(fvAEPg, tDn=u'topology/pod-1/paths-101/pathep-[ASA5525-X-3-MGMT]', descr=u'', primaryEncap=u'unknown', instrImedcy=u'lazy', mode=u'regular', encap=u'vlan-'+vlanId)




    # commit the generated code to APIC
    #print toXMLStr(fvAp)
    c = cobra.mit.request.ConfigRequest()
    c.addMo(fvAp)
    md.commit(c)





















def createUserAsapdc(md,userName='userName'):

    # the top level object on which operations will be made
    polUni = cobra.model.pol.Uni('')
    aaaUserEp = cobra.model.aaa.UserEp(polUni)

    # build the request using cobra syntax
    aaaUser = cobra.model.aaa.User(aaaUserEp, ownerKey=u'', expires=u'no', firstName=userName, descr=u'', lastName=u'', clearPwdHistory=u'no', accountStatus=u'active', phone=u'', pwdLifeTime=u'no-password-expire', expiration=u'never', ownerTag=u'', email=u'', name=userName, pwd=userName)

    aaaUserDomain = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'SecDo-'+userName, descr=u'', ownerTag=u'')
    aaaUserRole = cobra.model.aaa.UserRole(aaaUserDomain, ownerKey=u'', privType=u'writePriv', name=u'admin', descr=u'', ownerTag=u'')
    aaaUserDomain2 = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'common', descr=u'', ownerTag=u'')
    aaaUserRole2 = cobra.model.aaa.UserRole(aaaUserDomain2, ownerKey=u'', privType=u'readPriv', name=u'read-all', descr=u'', ownerTag=u'')
    aaaUserDomain3 = cobra.model.aaa.UserDomain(aaaUser, ownerKey=u'', name=u'all', descr=u'', ownerTag=u'')
    aaaUserRole3 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'aaa', descr=u'', ownerTag=u'')
    aaaUserRole4 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'fabric-admin', descr=u'', ownerTag=u'')
    aaaUserRole5 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'vmm-admin', descr=u'', ownerTag=u'')
    aaaUserRole6 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'tenant-ext-admin', descr=u'', ownerTag=u'')
    aaaUserRole7 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'ops', descr=u'', ownerTag=u'')
    aaaUserRole8 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'read-all', descr=u'', ownerTag=u'')
    aaaUserRole9 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'nw-svc-admin', descr=u'', ownerTag=u'')
    aaaUserRole10 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'admin', descr=u'', ownerTag=u'')
    aaaUserRole11 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'access-admin', descr=u'', ownerTag=u'')
    aaaUserRole12 = cobra.model.aaa.UserRole(aaaUserDomain3, ownerKey=u'', privType=u'readPriv', name=u'tenant-admin', descr=u'', ownerTag=u'')

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(aaaUserEp)
    md.commit(c)


def createSecDo(md, secDoName='securityDomainName'):
    polUni = cobra.model.pol.Uni('')
    aaaUserEp = cobra.model.aaa.UserEp(polUni)

    # build the request using cobra syntax
    aaaDomain = cobra.model.aaa.Domain(aaaUserEp, name=u'SecDo-'+secDoName)

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(aaaUserEp)
    md.commit(c)



def createSecDoAsapdc(md, secDoName='securityDomainName'):
    polUni = cobra.model.pol.Uni('')
    aaaUserEp = cobra.model.aaa.UserEp(polUni)

    # build the request using cobra syntax
    aaaDomain = cobra.model.aaa.Domain(aaaUserEp, name=u'SecDo-'+secDoName)

    # commit the generated code to APIC
    c = cobra.mit.request.ConfigRequest()
    c.addMo(aaaUserEp)
    md.commit(c)




def createTenantFromTemplate(moDir,TENANT_INFO):
#TENANT_INFO = [{'name': 'ExampleCorp',
#                'pvn': 'pvn1',
#                'bd': 'bd1',
#                'ap': [{'name': 'OnlineStore',
#                        'epgs': [{'name': 'app'},
#                                 {'name': 'web'},
#                                 {'name': 'db'},
#                                ]
#                       },
#                      ]
#                }
#              ]



    uniMo = moDir.lookupByDn('uni')

    for tenant in TENANT_INFO:
        logger.autolog(level=1,message="Creating tenant " + tenant['name'])
        fvTenantMo = Tenant(uniMo, tenant['name'])
        
        # Create Private Network
        Ctx(fvTenantMo, tenant['pvn'])
        
        # Create Bridge Domain
        fvBDMo = BD(fvTenantMo, name=tenant['bd'])
        
        # Create association to private network
        RsCtx(fvBDMo, tnFvCtxName=tenant['pvn'])
        
        # Create Application Profile
        for app in tenant['ap']:
            logger.autolog(level=1,message='Creating Application Profile: ' + app['name'])
            fvApMo = Ap(fvTenantMo, app['name'])
            
            # Create EPGs 
            for epg in app['epgs']:
                logger.autolog(level=1,message="Creating EPG: " + epg['name']) 
                fvAEPgMo = AEPg(fvApMo, epg['name'])
                
                # Associate EPG to Bridge Domain 
                RsBd(fvAEPgMo, tnFvBDName=tenant['bd'])
                # Associate EPG to VMM Domain
                # RsDomAtt(fvAEPgMo, vmmDomPMo.dn)

        # Commit each tenant seperately
        tenantCfg = ConfigRequest()
        tenantCfg.addMo(fvTenantMo)
        logger.autolog(message=str(fvTenantMo))
        moDir.commit(tenantCfg)



