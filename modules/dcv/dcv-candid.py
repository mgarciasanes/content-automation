import urllib2
import json
import os
import requests
import sys
import time
import platform

import ssl
import logging
requests.packages.urllib3.disable_warnings()

modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger
import http as http


####################################
#  Global Variables
####################################



def createCandidUser(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            username=gen.sanitizeString(otherInputs["username"])
            password=gen.sanitizeString(otherInputs["password"])
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'
#
#    session=getOtpToken(creds['host'])
#    if not session:
#        return {},'Failed: Unable to get One Time Token'
#
#    loginResult=login(creds['username'],creds['password'],creds['host'],session['cookie'],session['token'])
#    if not loginResult:
#        return {},'Failed: Unable to login'
#
#    result=createUser(username,password,creds['host'],session['cookie'],loginResult['token'])
#
#    if(result):
#        return {'username':username,'password':password},'Success'
#    else:
#        return {},'Failed: We could login but failed to create user.'
#


    session=getOtpToken(creds['host'])
    if not session:
        return {},'Failed: Unable to get One Time Token'

    loginResult=login(creds['username'],creds['password'],creds['host'],session['cookie'],session['token'])
    if not loginResult:
        return {},'Failed: Unable to login'


    #Check if user already exists...
    users=getUsers(creds['host'],loginResult['cookie'],loginResult['token'])
    userid=''
    if users:
        for user in users:
            if user['username']==username:
                userid=user['uuid']
                break
    else:
        return {},'Failed: Unable to get user list.'
    if userid != '':
         #Users already exists, delete
         logger.autolog(level=1,message='User already exists with username ' + str(username) + ', deleting...',format=logFormat)
         removed=deleteUser(userid,creds['host'],loginResult['cookie'],loginResult['token'])
         if not removed:
             return {},'Failed: User already existed. Failed to recreate (delete)'

    result=createUser(username,password,creds['host'],loginResult['cookie'],loginResult['token'])

    if(result):
        return {'username':username,'password':password},'Success'
    else:
        return {},'Failed: We could login but failed to create user.'




def createCandidUserWithRandomPassword(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            username=gen.sanitizeString(otherInputs["username"])
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    password=gen.generateRandomString(int(8))

    session=getOtpToken(creds['host'])
    if not session:
        return {},'Failed: Unable to get One Time Token'

    loginResult=login(creds['username'],creds['password'],creds['host'],session['cookie'],session['token'])
    if not loginResult:
        return {},'Failed: Unable to login'


    #Check if user already exists...
    users=getUsers(creds['host'],loginResult['cookie'],loginResult['token'])
    userid=''
    if users:
        for user in users:
            if user['username']==username:
                userid=user['uuid']
                break
    else:
        return {},'Failed: Unable to get user list.'
    if userid != '':
         #Users already exists, delete
         logger.autolog(level=1,message='User already exists with username ' + str(username) + ', deleting...',format=logFormat)
         removed=deleteUser(userid,creds['host'],loginResult['cookie'],loginResult['token'])
         if not removed:
             return {},'Failed: User already existed. Failed to recreate (delete)'


    result=createUser(username,password,creds['host'],loginResult['cookie'],loginResult['token'])

    if(result):
        return {'username':username,'password':password},'Success'
    else:
        return {},'Failed: We could login but failed to create user.'



def removeCandidUser(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            username=gen.sanitizeString(otherInputs["username"])
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    session=getOtpToken(creds['host'])
    if not session:
        return {},'Failed: Unable to get One Time Token'

    loginResult=login(creds['username'],creds['password'],creds['host'],session['cookie'],session['token'])
    if not loginResult:
        return {},'Failed: Unable to login'

    users=getUsers(creds['host'],loginResult['cookie'],loginResult['token'])
    userid=''
    if users:
        for user in users:
            if user['username']==username:
                userid=user['uuid']
                break
    else:
        return {},'Failed: Unable to get user list.'
    if userid =='':
         logger.autolog(level=1,message='No user found with username ' + str(username),format=logFormat)
         return {},'Success'


    result=deleteUser(userid,creds['host'],loginResult['cookie'],loginResult['token'])
    if(result):
        return {},'Success'
    else:
        return {},'Failed: We could login but failed to delete user.'








def getOtpToken(host,protocol='https',port='443',path=r'/api/v1/whoami',logFormat=''):
    headers={}
    data= ''
    url  = '%s://%s:%s%s' % (protocol,host,port,path)
    logger.autolog(level=1, message='Lets get the OTP TOKEN. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method='get',url=path,data=data, headers=headers)
    if result:
        logger.autolog(level=1, message='We got OTP TOKEN!! ' + str(result.headers['X-CANDID-LOGIN-OTP'])  + ' Cookie is '  + str(result.cookies), format=logFormat)
        return {'token': result.headers['X-CANDID-LOGIN-OTP'] , 'cookie': result.cookies} 
    else:
        logger.autolog(level=1, message='Something went wrong while getting token, returning False', format=logFormat)
        return False

def login(username,password,host,cookies,token,protocol='https',port='443',path=r'/api/v1/login',logFormat=''):
    headers={'Content-Type':r'application/json','Accept':r'application/json','X-CANDID-LOGIN-OTP':token ,'Accept':r'application/json'}
    #cookies={'SESSION':cookies}
    data=json.dumps({"username" : username,"password" : password})
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Lets LOGIN. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,method='post',host=host,url=path,data=data, headers=headers, cookies=cookies)
    if result:
        try:
            logger.autolog(level=1, message='We got result: ' + str(result.text) + '  trying to convert to dict', format=logFormat)
            r=json.loads(result.text)
            #logger.autolog(level=1, message='Success value is: ' + str(r['success']), format=logFormat)
            if str(r['success'])=='True':
                logger.autolog(level=1, message='We got CSRF TOKEN!! ' + str(result.headers['X-CANDID-CSRF-TOKEN'])  + ' Cookie is '  + str(result.cookies), format=logFormat)
                return {'token': result.headers['X-CANDID-CSRF-TOKEN'] , 'cookie': result.cookies}
            else:
                logger.autolog(level=1,message='Got success value of false',format=logFormat)
                return False
        except Exception as e:
            logger.autolog(level=1,message='Error while reading result on user creation',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return False

         
    else:
        logger.autolog(level=1, message='Something went wrong while trying to login, returning False', format=logFormat)
        return False

def createUser(username,password,host,cookies,token,protocol='https',port='443',path=r'/api/v1/users',logFormat=''):
    headers={'Content-Type':'application/json','Accept':'application/json','X-CANDID-CSRF-TOKEN':token ,'Accept':'application/json'}
    data=json.dumps({"system_object" : "false","email" : username+"@dcloud.cisco.com","username" : username,"password" : password,"confirm_password" : password})
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Lets CREATE USER. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,method='post',host=host,url=path,data=data, headers=headers, cookies=cookies)
    if result:
        try:
            logger.autolog(level=1, message='We got result: ' + str(result.text) + '  trying to convert to dict', format=logFormat)
            r=json.loads(result.text)
            if str(r['success'])=='True':
                return True
            else:
                return False
        except Exception as e:
            logger.autolog(level=1,message='Error while reading result on user creation',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return False
    else:
        logger.autolog(level=1, message='Something went wrong while trying to create user, returning False', format=logFormat)
        return False


def getUsers(host,cookies,token,protocol='https',port='443',path=r'/api/v1/users',logFormat=''):
    headers={'Content-Type':'application/json','Accept':'application/json','X-CANDID-CSRF-TOKEN':token ,'Accept':'application/json'}
    data=''
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Lets GET USERS. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,method='get',host=host,url=path,data=data, headers=headers, cookies=cookies)
    if result:
        try:
            logger.autolog(level=1, message='We got result: ' + str(result.text) + '  trying to convert to dict', format=logFormat)
            r=json.loads(result.text)
            if str(r['success'])=='True':
                return r['value']['data']
            else:
                return False
        except Exception as e:
            logger.autolog(level=1,message='Error while reading result on user list',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return False
    else:
        logger.autolog(level=1, message='Something went wrong while trying to get users, returning False', format=logFormat)
        return False


def deleteUser(userid,host,cookies,token,protocol='https',port='443',path=r'/api/v1/users/',logFormat=''):
    headers={'Content-Type':'application/json','Accept':'application/json','X-CANDID-CSRF-TOKEN':token ,'Accept':'application/json'}
    data=''
    url  = '%s://%s:%s%s%s' % (protocol,host,port,path,userid)

    logger.autolog(level=1, message='Lets GET USERS. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,method='delete',host=host,url=path+userid,data=data, headers=headers, cookies=cookies)
    if result:
        try:
            logger.autolog(level=1, message='We got result: ' + str(result.text) + '  trying to convert to dict', format=logFormat)
            r=json.loads(result.text)
            if str(r['success'])=='True':
                return True
            else:
                return False

        except Exception as e:
            logger.autolog(level=1,message='Error while reading result on user list',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return False
    else:
        logger.autolog(level=1, message='Something went wrong while trying to get users, returning False', format=logFormat)
        return False






