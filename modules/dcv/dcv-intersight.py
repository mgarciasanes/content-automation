import urllib2
import os
import requests
import time
import platform
import sys
import json
from intersight.intersight_api_client import IntersightApiClient
from intersight.apis import iam_account_api
from intersight.apis import iam_user_api
from intersight.apis import iam_role_api
from intersight.apis import iam_end_point_role_api
from intersight.apis import iam_permission_api

import ssl
import logging
requests.packages.urllib3.disable_warnings()

modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger
import http as http


####################################
#  Global Variables
####################################

#This is a horrible workaround for users who use uppercase characters in their usernames. We only get usernames
#in lowercase, regardless the original string. So we ammend them here, one by one... 
#If there is an entry for the user in the following dictionary, use it. Otherwise, use the one provided as argument. 
usersWithUppercase={"koenvdeynde":"koenVdEynde","willkuz68":"WillKuz68", "aaron_fas@hotmail.com":"Aaron_Fas@hotmail.com","chrismialon":"ChrisMialon","csco11685929":"CSCO11685929"}


####################################
#  Front-end  Functions
####################################

def createIntersightUser(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            #username=gen.sanitizeString(otherInputs["username"])
            username=otherInputs["username"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    result=createUser(username,host=creds['host'],privateKeyFile=creds['secretKey'],apiKeyId=creds['apiKeyId'])

    if(result):
        return {'username':username},'Success'
    else:
        return {},'Failed: Unable to create user.'



def removeIntersightUser(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            #username=gen.sanitizeString(otherInputs["username"])
            username=otherInputs["username"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    result=deleteUser(username,host=creds['host'],privateKeyFile=creds['secretKey'],apiKeyId=creds['apiKeyId'])


    if(result):
        return {},'Success'
    else:
        return {},'Failed: Unable to delete user.'


def createUser(userId,userRole="Read-Only",host=r'',privateKeyFile='',apiKeyId=r'',logFormat=''):

    global usersWithUppercase
    try:
        userId=usersWithUppercase[userId]
    except Exception as e:
        pass


    if "@" not in userId:
        userId=userId+"@cisco.com"


    result=False
    try:

        logger.autolog(level=1,message='About to create user in intersight: ' + str(userId),format=logFormat)

        # Create Intersight API instance
        # ----------------------
        api_instance = IntersightApiClient(host=host, private_key=privateKeyFile, api_key_id=apiKeyId)

        # GET Accounts
        accounts_handle = iam_account_api.IamAccountApi(api_instance)
        accounts_result = accounts_handle.iam_accounts_get()

        # USER HANDLE
        users_handle = iam_user_api.IamUserApi(api_instance)

        # Users EXISTS?
        #kwargs = dict(filter="Name eq '%s'" % userId)
        kwargs = dict(filter="Email eq '%s'" % userId)
        users_result = users_handle.iam_users_get(**kwargs)
        if "'results': None" not in  str(users_result):
            logger.autolog(level=1,message='User already exsts in intersight, no need to create!: ' + str(users_result),format=logFormat)
            return True

        logger.autolog(level=1,message='User does not exsts in intersight, will create!: ' + str(users_result),format=logFormat)



        # GET Permission "Read Only"
        permissions_handle = iam_permission_api.IamPermissionApi(api_instance)
        kwargs = dict(filter="Name eq '%s'" % userRole)
        permissions_result = permissions_handle.iam_permissions_get(**kwargs )
        permId=permissions_result.results[0].moid
        logger.autolog(level=1,message="Permission ID is: " + str(permId),format=logFormat)



        # POST Users with Idpreference
        #users_body = {
        #    'Name': userId,
        #    'Idpreference': accounts_result.results[0].idpreferences[0],
        #}


        #logger.autolog(level=1,message="accounts_result.results[0].idpreferences[0]",format=logFormat)
        #logger.autolog(level=1,message=str(type(accounts_result.results[0].idpreferences[0])),format=logFormat)
        #logger.autolog(level=1,message=json.loads(str(accounts_result.results[0].idpreferences[0]).replace("'",'"')),format=logFormat)


        # POST Users with Idpreference 
        users_body = {
            'Email': userId,
            'Idpreference': {"ObjectType": json.loads(str(accounts_result.results[0].idpreferences[0]).replace("'",'"'))["object_type"],"Moid": json.loads(str(accounts_result.results[0].idpreferences[0]).replace("'",'"'))["moid"]},
            'Permissions': [{"ObjectType":"iam.Permission","Moid":permId}],
        }


        logger.autolog(level=1,message='Payload for user creationg in intersight: ' + str(users_body),format=logFormat)
        users_result = users_handle.iam_users_post(users_body)
        
        logger.autolog(level=1,message='User created in intersight: ' + str(users_result),format=logFormat)

        result=True

        # GET Users
        #kwargs = dict(filter="Name eq '%s'" % userId)
        kwargs = dict(filter="Email eq '%s'" % userId)
        users_result = users_handle.iam_users_get(**kwargs)
        logger.autolog(level=1,message='User created in intersight: ' + str(users_result),format=logFormat)

        return result



        # GET Roles
        roles_handle = iam_role_api.IamRoleApi(api_instance)
        roles_result = roles_handle.iam_roles_get()
        for role in roles_result.results:
            if role.name == userRole:
                break

        # GET EndPointRoles
        end_point_roles_handle = iam_end_point_role_api.IamEndPointRoleApi(api_instance)
        endpoint_roles = {}
        endpoint_roles['Read-Only'] = 'endpoint-readonly'
        endpoint_roles['Account Administrator'] = 'endpoint-admin'
        kwargs = dict(filter="RoleType eq '%s'" % endpoint_roles[userRole])
        end_point_roles_result = end_point_roles_handle.iam_end_point_roles_get(**kwargs)

        # POST Permissions with EndPointRoles
        permissions_handle = iam_permission_api.IamPermissionApi(api_instance)
        permissions_body = {
            'Subject': users_result.results[0].moid,
            'Type': 'User',
            'Account': accounts_result.results[0].account_moid,
            'EndPointRoles': end_point_roles_result.results,
            'Roles': [role],
        }
        permissions_result = permissions_handle.iam_permissions_post(permissions_body)
        logger.autolog(level=1,message='Permissions configured for user in intersight: ' + str(userId),format=logFormat)

    except Exception, err:
        logger.autolog(level=1,message='Error while creating user in intersight: ' + str(err),format=logFormat)
        print "Exception:", str(err)
        import traceback
        print '-' * 60
        traceback.print_exc(file=sys.stdout)
        print '-' * 60
    return result




def deleteUser(userId,host=r'',privateKeyFile='',apiKeyId=r'',logFormat=''):

    global usersWithUppercase
    try:
        userId=usersWithUppercase[userId]
    except Exception as e:
        pass

    if "@" not in userId:
        userId=userId+"@cisco.com"



    result=False
    try:
        logger.autolog(level=1,message='About to delete user in intersight: ' + str(userId),format=logFormat)
        # Create Intersight API instance
        # ----------------------
        #api_instance = IntersightApiClient(host=settings['api_host'], private_key=settings['api_private_key'], api_key_id=settings['api_key_id'])
        api_instance = IntersightApiClient(host=host, private_key=privateKeyFile, api_key_id=apiKeyId)

        # GET Users
        users_handle = iam_user_api.IamUserApi(api_instance)


        #NOTE:  For some reason, the filter is not working anymore... 
        #
        ##Create Filter, so we only get the userid we are looking for...
        #kwargs = dict(filter="Name eq '%s'" % userId)
        #users_result = users_handle.iam_users_get(**kwargs)
        ## Users EXISTS?
        #if "'results': None" in  str(users_result):
        #    logger.autolog(level=1,message='User does not exsts in intersight, no need to delete!: ' + str(users_result),format=logFormat)
        #    return True


        kwargs = dict(filter="Email eq '%s'" % userId)


        users_result = users_handle.iam_users_get(**kwargs)
        #logger.autolog(level=1,message='List of users is !: ' + str(users_result),format=logFormat)
        #logger.autolog(level=1,message='List of users is !: ' + str(dir(users_result)),format=logFormat)
        #logger.autolog(level=1,message='List of users is !: ' + str(type(users_result.to_dict()['results'][0])),format=logFormat)


        if "'results': None" in  str(users_result):
            logger.autolog(level=1,message='User doess not exist in intersight, no need to delete!: ' + str(users_result),format=logFormat)
            return True




        logger.autolog(level=1,message='User result: ' + str(users_result),format=logFormat)
        userMoid=users_result.results[0].moid
        logger.autolog(level=1,message='User moid: ' + str(userMoid),format=logFormat)


        ##userMoid=''
        # Users EXISTS?
        #for user in users_result.to_dict()['results']:
        #    #logger.autolog(level=1,message='User in list:' + str(user['name']),format=logFormat)
        #    if user['name']==str(userId):
        #        userMoid=user['moid']
        #        logger.autolog(level=1,message='Found User ' + str(userId)  + ' with moid ' + str(userMoid),format=logFormat)
        #        #return True
        #if userMoid=='':
        #    logger.autolog(level=1,message='User does not exsts in intersight, no need to delete!',format=logFormat)
        #    return True



        # GET Permissions
        #permissions_handle = iam_permission_api.IamPermissionApi(api_instance)
        #kwargs = dict(filter="Subject eq '%s'" % users_result.results[0].moid)
        #kwargs = dict(filter="Subject eq '%s'" % userMoid)
        #permissions_result = permissions_handle.iam_permissions_get(**kwargs)

        # DELETE Permissions
        #permissions_delete_result = permissions_handle.iam_permissions_moid_delete(moid=permissions_result.results[0].moid)
        #result = True
        #logger.autolog(level=1,message='Permissions deleted for user in intersight: ' + str(userId),format=logFormat)

        # DELETE Users
        users_delete_result = users_handle.iam_users_moid_delete(moid=userMoid)
        logger.autolog(level=1,message='User deleted in intersight: ' + str(userId),format=logFormat)
        result = True

    except Exception, err:
        logger.autolog(level=1,message='Error while deleting user in intersight: ' + str(err),format=logFormat)
        print "Exception:", str(err)
        import traceback
        print '-' * 60
        traceback.print_exc(file=sys.stdout)
        print '-' * 60

    return result







