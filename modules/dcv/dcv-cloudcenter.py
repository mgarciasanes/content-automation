import urllib2
import json
import os
import requests
import sys
import time
import platform

import ssl
import logging
requests.packages.urllib3.disable_warnings()

modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger
import http as http


####################################
#  Global Variables
####################################

sdcTetrationUserId={"rtp":[u'5b36597d497d4f0bd3c8a753', u'5b365af9497d4f0bd3c8a757', u'5b3a8bf5755f020b65093f6b', u'5b3a8e2b497d4f1242c8a749', u'5b3a8e9d755f023d13093f47', u'5b3a8f77497d4f13ecc8a759', u'5b3a901c497d4f0733c8a756', u'5b3a915b497d4f2664c8a750', u'5b3a91c7497d4f1242c8a753', u'5b3a929c755f020b65093f75', u'5b3a9323497d4f2664c8a756', u'5b3aa318755f026e87093f4f', u'5b3aa37f497d4f2664c8a75a', u'5b3aa3ff497d4f13ecc8a76b', u'5b3aa46c497d4f13ecc8a76e', u'5b55beaf755f02389c093f48', u'5b3aa4da497d4f1242c8a75a', u'5b3aa538497d4f2664c8a761', u'5b3aa59b755f025c45093f2f', u'5b3aa607755f026e87093f5e'],"lon":[],"sng":[],"sjc":[]}



####################################
#  Front-end  Functions
####################################


def createCloudCenterUser(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            username=gen.sanitizeString(otherInputs["username"])
            password=gen.sanitizeString(otherInputs["password"])
            email=otherInputs["email"]
         
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'

        try:
            tenantId=otherInputs["tenantId"]         
        except Exception as e:
            tenantId='1'

        try:
            regionId=otherInputs["regionId"]         
        except Exception as e:
            regionId='3'

        try:
            credsUser=otherInputs["credsUser"]         
        except Exception as e:
            credsUser=''

        try:
            credsPass=otherInputs["credsPass"]         
        except Exception as e:
            credsPass=''

    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'


    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    if credsUser != '':
        creds["user"]=credsUser 
    if credsPass != '':
        creds["password"]=credsPass

    

    userId,userName =createUser(creds['protocol'],creds['host'],creds['port'],creds['user'],creds['password'],username,password,email,'Cisco',tenantId,regionId,logFormat=logFormat)

    if userId:
        return {"userId":userId,"userName":userName},'Success'
    else:
        return {},'Failed: Error while sending request to create user'





def deleteCloudCenterUser(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            email=otherInputs["email"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    if deleteUser(creds['protocol'],creds['host'],creds['port'],creds['user'],creds['password'],email,logFormat=logFormat):
        return {},'Success'
    else:
        return {},'Failed: Error while deleting user'






def createCloudCenterApiKey(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            userId=otherInputs["userId"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'


    key=createUserKey(creds['protocol'],creds['host'],creds['port'],creds['user'],creds['password'],userId,logFormat=logFormat)

    if key:
        return {'apiKey':key},'Success'
    else:
        return {},'Failed: Error while sending request to create users api key'



def createCloudCenterTenant(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            userId=otherInputs["userId"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'


    tenantId=createTenant(creds['protocol'],creds['host'],creds['port'],creds['user'],creds['password'],tenantName,userId,tenantName+"@cisco.com",logFormat=logFormat)

    if tenantId:
        return {"tenantId":tenantId},'Success'
    else:
        return {},'Failed: Error while creating tenant'




def createCloudCenterTenantPlan(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            tenantId=otherInputs["tenantId"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'

        try:
            credsUser=otherInputs["credsUser"]         
        except Exception as e:
            credsUser=''

        try:
            credsPass=otherInputs["credsPass"]         
        except Exception as e:
            credsPass=''


    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    if credsUser != '':
        creds["user"]=credsUser 
    if credsPass != '':
        creds["password"]=credsPass


    contract=createTenantContract(creds['protocol'],creds['host'],creds['port'],creds['user'],creds['password'],tenantId,tenantName,logFormat=logFormat)
    plan=createTenantManagePlan(creds['protocol'],creds['host'],creds['port'],creds['user'],creds['password'],tenantId,tenantName,logFormat=logFormat)

    if contract and plan:
        return {},'Success'
    else:
        return {},'Failed: Error while creating tenant'



def deleteCloudCenterTenant(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'


    result=deleteTenant(creds['protocol'],creds['host'],creds['port'],creds['user'],creds['password'],tenantName,logFormat=logFormat)

    if result:
        return {},'Success'
    else:
        return {},'Failed: Error while deleting tenant'






def addManagePlanToTenant(sessionId, target, otherInputs=[], logFormat=''):

    if (type(otherInputs) is dict):
        try:
            userId=otherInputs["userId"]
            contractId=otherInputs["contractId"]
            planId=otherInputs["planId"]

        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'

        try:
            credsUser=otherInputs["credsUser"]         
        except Exception as e:
            credsUser=''

        try:
            credsPass=otherInputs["credsPass"]         
        except Exception as e:
            credsPass=''
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    if credsUser != '':
        creds["user"]=credsUser 
    if credsPass != '':
        creds["password"]=credsPass

    result=addManagePlanTenant(creds['protocol'],creds['host'],creds['port'],creds['user'],creds['password'],planId,contractId,userId,logFormat=logFormat)

    if result:
        return {},'Success'
    else:
        return {},'Failed: Error while sending request to add Manage Plan to Tenant'





def deployOpencart(sessionId, target, otherInputs=[], logFormat=''):

    if (type(otherInputs) is dict):
        try:
            appName=otherInputs["appName"]
            environmentId=otherInputs["environmentId"]
            cloudRegionId=otherInputs["cloudRegionId"]
            accountId=otherInputs["accountId"]
            deploymentFolder=otherInputs["deploymentFolder"]
            apicTenantName=otherInputs["apicTenantName"]
            apicEpgDb1=otherInputs["apicEpgDb1"]
            apicEpgWeb1=otherInputs["apicEpgWeb1"]
            apicEpgWeb2=otherInputs["apicEpgWeb2"]    
            tetUserId=otherInputs["tetUserId"]
            dc=otherInputs["datacenter"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
        try:
            credsUser=otherInputs["credsUser"]         
        except Exception as e:
            credsUser=''
        try:
            credsPass=otherInputs["credsPass"]         
        except Exception as e:
            credsPass=''
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    if credsUser != '':
        creds["user"]=credsUser 
    if credsPass != '':
        creds["password"]=credsPass

    try:
        tetUserId=sdcTetrationUserId[dc][int(tetUserId)-1]
    except Exception as e:
        logger.autolog(level=1,message='Unable to get tetUserId: ' + str(e),format=logFormat)
        return {}, 'Failed: Error while getting tetration user ID. ' +str(e)

    result=createOpencartDeployment(creds['protocol'],creds['host'],creds['port'],creds['user'],creds['password'],'375',appName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,tetUserId,logFormat=logFormat)

    if result:
        return {},'Success'
    else:
        return {},'Failed: Error while deplying opencart application to Tenant'






def deployOpencartV2(sessionId, target, otherInputs=[], logFormat=''):

    if (type(otherInputs) is dict):
        try:
            appName=otherInputs["appName"]
            environmentId=otherInputs["environmentId"]
            cloudRegionId=otherInputs["cloudRegionId"]
            accountId=otherInputs["accountId"]
            deploymentFolder=otherInputs["deploymentFolder"]
            apicTenantName=otherInputs["apicTenantName"]
            apicEpgDb1=otherInputs["apicEpgDb1"]
            apicEpgWeb1=otherInputs["apicEpgWeb1"]
            apicEpgWeb2=otherInputs["apicEpgWeb2"]    
            tetUserId=otherInputs["tetUserId"]
            dc=otherInputs["datacenter"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
        try:
            credsUser=otherInputs["credsUser"]         
        except Exception as e:
            credsUser=''
        try:
            credsPass=otherInputs["credsPass"]         
        except Exception as e:
            credsPass=''
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    if credsUser != '':
        creds["user"]=credsUser 
    if credsPass != '':
        creds["password"]=credsPass

    try:
        tetUserId=sdcTetrationUserId[dc][int(tetUserId)-1]
    except Exception as e:
        logger.autolog(level=1,message='Unable to get tetUserId: ' + str(e),format=logFormat)
        return {}, 'Failed: Error while getting tetration user ID. ' +str(e)

    result=createOpencartDeploymentV2(creds['protocol'],creds['host'],creds['port'],creds['user'],creds['password'],'375',appName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,tetUserId,logFormat=logFormat)

    if result:
        return result,'Success'
    else:
        return {},'Failed: Error while deplying opencart application to Tenant'



def deployOpencartV3(sessionId, target, otherInputs=[], logFormat=''):

    if (type(otherInputs) is dict):
        try:
            appName=otherInputs["appName"]
            environmentId=otherInputs["environmentId"]
            cloudRegionId=otherInputs["cloudRegionId"]
            accountId=otherInputs["accountId"]
            deploymentFolder=otherInputs["deploymentFolder"]
            apicTenantName=otherInputs["apicTenantName"]
            apicEpgDb1=otherInputs["apicEpgDb1"]
            apicEpgWeb1=otherInputs["apicEpgWeb1"]
            apicEpgWeb2=otherInputs["apicEpgWeb2"]    
            dc=otherInputs["datacenter"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
        try:
            credsUser=otherInputs["credsUser"]         
        except Exception as e:
            credsUser=''
        try:
            credsPass=otherInputs["credsPass"]         
        except Exception as e:
            credsPass=''
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    if credsUser != '':
        creds["user"]=credsUser 
    if credsPass != '':
        creds["password"]=credsPass


    result=createOpencartDeploymentV3(creds['protocol'],creds['host'],creds['port'],creds['user'],creds['password'],'375',appName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,logFormat=logFormat)

    if result:
        return result,'Success'
    else:
        return {},'Failed: Error while deplying opencart application to Tenant'






def deleteOpencart(sessionId, target, otherInputs=[], logFormat=''):

    if (type(otherInputs) is dict):
        try:
            appName=otherInputs["appName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
        try:
            credsUser=otherInputs["credsUser"]         
        except Exception as e:
            credsUser=''

        try:
            credsPass=otherInputs["credsPass"]         
        except Exception as e:
            credsPass=''
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    if credsUser != '':
        creds["user"]=credsUser 
    if credsPass != '':
        creds["password"]=credsPass

    result=deleteOpencartDeployment(creds['protocol'],creds['host'],creds['port'],creds['user'],creds['password'],appName,logFormat=logFormat)

    if result:
        return {},'Success'
    else:
        return {},'Failed: Error while sending request to delete opencart app'



####################################
#  JSON PAYLOAD
####################################



def createUserBody(username,password,email,company,tenantId,regionId,output='json'):
    createUserBodyDict={"firstName": username,
    "lastName": username,
    "password": password,
    "enabled": True,
    "emailAddr": email,
    "companyName": company,
    "phoneNumber": "",
    "externalId": "",
    "tenantId": int(tenantId),
    "activationData": {
        "activateRegions": [
            {
                "regionId": str(regionId)
            }
        ],
        "agreeToContract": True,
        "sendActivationEmail": False,
        "importApps": []
    }
    }
    if output=='json':
        return json.dumps(createUserBodyDict)
    elif output=='dict':
        return createUserBodyDict
    elif output=='txt':
        return str(createUserBodyDict)



def createKeyBody(output='json'):
    createKeyBodyDict={
    "apiKey": {
        "generate": True
    }
    }
    if output=='json':
        return json.dumps(createKeyBodyDict)
    elif output=='dict':
        return createKeyBodyDict
    elif output=='txt':
        return str(createKeyBodyDict)


def createTenantBody(tenantName,userId,email,output='json'):
    createTenantBodyDict={"userId": str(userId),
    "name": tenantName,
    "shortName": tenantName,
    "domainName": tenantName+"."+gen.generateRandomString(5)+".com",
    "phone": "0000000000",
    "externalId": "",
    "url": "http://www.cisco.com",
    "contactEmail": email,
    "loginLogo": "",
    "homePageLogo": "",
    "about": "",
    "termsOfService": "",
    "privacyPolicy": "",
    "enablePurchaseOrder": True,
    "enableEmailNotificationsToUsers": False,
    "enableMonthlyBilling": False,
    "defaultChargeType": "Hourly",
    "preferences": [
        {
            "name": "PASSWORD_MIN_LENGTH",
            "value": "5"
        },
        {
            "name": "PASSWORD_EXPIRATION_DAYS",
            "value": "365"
        },
        {
            "name": "PASSWORD_DISABLE_SELF_RESET",
            "value": "true"
        },
        {
            "name": "PASSWORD_REQUIRE_LOWERCASE",
            "value": "false"
        }
    ]
    }    
    if output=='json':
        return json.dumps(createTenantBodyDict)
    elif output=='dict':
        return createTenantBodyDict
    elif output=='txt':
        return str(createTenantBodyDict)




def tenantManagePlansBody(planId,contractId,userId,renewContract=False,output='json'):

    tenantManagePlansBodyDict={"action": "MANAGE_PLANS",
    "userManagePlansData": {
        "planId": str(planId) ,
        "contractId": str(contractId),
        "type": "CHANGE_PRORATE",
        "renewContract": renewContract,
        "userId": str(userId)
    }
    }
    if output=='json':
        return json.dumps(tenantManagePlansBodyDict)
    elif output=='dict':
        return tenantManagePlansBodyDict
    elif output=='txt':
        return str(tenantManagePlansBodyDict)



def createTenantManagePlansBody(managePlanName,tenantId,output='json'):

    createTenantManagePlansBodyDict={"name":managePlanName ,
    "description": "dcloud",
    "type": "UNLIMITED_PLAN",
    "showOnlyToAdmin": false,
    "price": "5",
    "onetimeFee": "5",
    "billToVendor": false,
    "paymentProfileRequired": false,
    "tenantId": tenantId
    }
    if output=='json':
        return json.dumps(createTenantManagePlansBodyDict)
    elif output=='dict':
        return createTenantManagePlansBodyDict
    elif output=='txt':
        return str(createTenantManagePlansBodyDict)


def createTenantContractBody(contractName, output='json'):

    createTenantContractBodyDict={"name": contractName,
    "length": 12,
    "terms": "Training terms",
    "discountRate": 50
    } 
    if output=='json':
        return json.dumps(createTenantContractBodyDict)
    elif output=='dict':
        return createTenantContractBodyDict
    elif output=='txt':
        return str(createTenantContractBodyDict)





def deployOpencartWithTetrationInjetorBody(appId,deploymentName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,tetUserId,output='json'):
    deployOpencartWithTetrationInjetorBodyDict={"appId": str(appId),
        "appVersion": "3.0",
        "name": str(deploymentName),
        "metadatas": [
                {
                        "namespace": "",
                        "name": "Job_Name",
                        "value": "%JOB_NAME%",
                        "editable": False,
                        "required": True
                },
                {
                        "namespace": "",
                        "name": "App_Name",
                        "value": "OpenCart",
                        "editable": False,
                        "required": True
                }
        ],
        "environmentId": str(environmentId),
        "tagIds": [
                1
        ],
        "securityProfileIds": [],
        "agingPolicyId": None,
        "suspensionPolicyId": None,
        "policyIds": None,
        "preventTermination": False,
        "parameters": {
                "appParams": [
                        {
                                "name": "TET_SENSOR_TYPE",
                                "value": "Deep Visibility with Enforcement"
                        },
                        {
                                "name": "TET_USER_ID",
                                "value": str(tetUserId)
                        }
                ],
                "cloudParams": {
                        "cloudRegionId": str(cloudRegionId),
                        "accountId": str(accountId)
                }
        },
        "jobs": [
                {
                        "tierId": "376",
                        "policyIds": None,
                        "tagIds": [],
                        "securityProfileIds": [],
                        "parameters": {
                                "appParams": [
                                        {
                                                "name": "referredJob",
                                                "value": ""
                                        }
                                ],
                                "envParams": [
                                        {
                                                "name": "HOSTNAME",
                                                "value": "WEB-%JOB_NAME%"
                                        },
                                        {
                                                "name": "JOBNAME",
                                                "value": "%JOB_NAME%"
                                        },
                                        {
                                                "name": "TET_SENSOR_TYPE",
                                                "value": "%TET_SENSOR_TYPE%"
                                        },
                                        {
                                                "name": "TET_USER_ID",
                                                "value": "%TET_USER_ID%"
                                        }
                                ],
                                "cloudParams": {
                                        "cloudRegionId": str(cloudRegionId),
                                        "accountId": str(accountId),
                                        "volumes": [
                                                {
                                                        "name": "RootVolume",
                                                        "bootable": True,
                                                        "size": 0,
                                                        "type": None,
                                                        "iops": None
                                                }
                                        ],
                                        "rootVolumeSize": "0",
                                        "cloudProperties": [
                                                {
                                                        "name": "FullClone",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "RootDiskResizeConfig",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "UserDataCenterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "UserDatastoreCluster",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "UserTargetDeploymentFolder",
                                                        "value": str(deploymentFolder)
                                                },
                                                {
                                                        "name": "UserClusterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "CC_AUTO_SELECT_NETWORK",
                                                        "value": False
                                                },
                                                {
                                                        "name": "numNICs",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "ExtensionId",
                                                        "value": "1"
                                                },
                                                {
                                                        "name": "AciTenantName",
                                                        "value": str(apicTenantName)
                                                },
                                                {
                                                        "name": "AciVmmDomainName",
                                                        "value": "vmware60-hybridcloud"
                                                },
                                                {
                                                        "name": "externalNetwork",
                                                        "value": "common:L3_Out:L3_Out"
                                                },
                                                {
                                                        "name": "serviceGraphTemplate",
                                                        "value": ""
                                                }
                                        ],
                                        "nics": [
                                                {
                                                        "order": 1,
                                                        "id": str(apicEpgWeb1),
                                                        "type": "ACI_EPG"
                                                },
                                                {
                                                        "order": 2,
                                                        "id": str(apicEpgWeb2),
                                                        "type": "ACI_EPG"
                                                }
                                        ],
                                        "instance": "Micro"
                                }
                        }
                },
                {
                        "tierId": "377",
                        "policyIds": None,
                        "tagIds": [],
                        "securityProfileIds": [],
                        "parameters": {
                                "appParams": [
                                        {
                                                "name": "referredJob",
                                                "value": ""
                                        }
                                ],
                                "envParams": [
                                        {
                                                "name": "JOBNAME",
                                                "value": "%JOB_NAME%"
                                        },
                                        {
                                                "name": "HOSTNAME",
                                                "value": "DB-%JOB_NAME%"
                                        },
                                        {
                                                "name": "TET_SENSOR_TYPE",
                                                "value": "%TET_SENSOR_TYPE%"
                                        },
                                        {
                                                "name": "TET_USER_ID",
                                                "value": "%TET_USER_ID%"
                                        }
                                ],
                                "cloudParams": {
                                        "cloudRegionId": str(cloudRegionId),
                                        "accountId": str(accountId),
                                        "volumes": [
                                                {
                                                        "name": "RootVolume",
                                                        "bootable": True,
                                                        "size": 0,
                                                        "type": None,
                                                        "iops": None
                                                }
                                        ],
                                        "rootVolumeSize": "0",
                                        "cloudProperties": [
                                                {
                                                        "name": "FullClone",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "RootDiskResizeConfig",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "UserDataCenterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "UserDatastoreCluster",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "UserTargetDeploymentFolder",
                                                        "value": str(deploymentFolder)
                                                },
                                                {
                                                        "name": "UserClusterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "CC_AUTO_SELECT_NETWORK",
                                                        "value": False
                                                },
                                                {
                                                        "name": "numNICs",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "ExtensionId",
                                                        "value": "1"
                                                },
                                                {
                                                        "name": "AciTenantName",
                                                        "value": str(apicTenantName)
                                                },
                                                {
                                                        "name": "AciVmmDomainName",
                                                        "value": "vmware60-hybridcloud"
                                                },
                                                {
                                                        "name": "externalNetwork",
                                                        "value": "common:L3_Out:L3_Out"
                                                },
                                                {
                                                        "name": "serviceGraphTemplate",
                                                        "value": ""
                                                }
                                        ],
                                        "nics": [
                                                {
                                                        "order": 1,
                                                        "id": str(apicEpgDb1),
                                                        "type": "ACI_EPG"
                                                }
                                        ],
                                        "instance": "Micro"
                                }
                        }
                },
                {
                        "tierId": "378",
                        "policyIds": None,
                        "tagIds": [],
                        "securityProfileIds": [],
                        "parameters": {
                                "appParams": [
                                        {
                                                "name": "referredJob",
                                                "value": ""
                                        }
                                ],
                                "envParams": [],
                                "cloudParams": {
                                        "cloudRegionId": str(cloudRegionId),
                                        "accountId": str(accountId),
                                        "volumes": [],
                                        "rootVolumeSize": "0",
                                        "cloudProperties": [
                                                {
                                                        "name": "FullClone",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "RootDiskResizeConfig",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "UserDataCenterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "UserDatastoreCluster",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "UserTargetDeploymentFolder",
                                                        "value": str(deploymentFolder)
                                                },
                                                {
                                                        "name": "UserClusterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "CC_AUTO_SELECT_NETWORK",
                                                        "value": False
                                                },
                                                {
                                                        "name": "numNICs",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "ExtensionId",
                                                        "value": "1"
                                                },
                                                {
                                                        "name": "AciTenantName",
                                                        "value": str(apicTenantName)
                                                },
                                                {
                                                        "name": "AciVmmDomainName",
                                                        "value": "vmware60-hybridcloud"
                                                },
                                                {
                                                        "name": "externalNetwork",
                                                        "value": "common:L3_Out:L3_Out"
                                                },
                                                {
                                                        "name": "serviceGraphTemplate",
                                                        "value": ""
                                                }
                                        ],
                                        "nics": [
                                                {}
                                        ]
                                }
                        }
                }
        ],
        "timeZone": "UTC"
    }
    if output=='json':
        return json.dumps(deployOpencartWithTetrationInjetorBodyDict)
    elif output=='dict':
        return deployOpencartWithTetrationInjetorBodyDict
    elif output=='txt':
        return str(deployOpencartWithTetrationInjetorBodyDict)




def deployOpencartBodyV3(appId,deploymentName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,output='json'):
    deployOpencartBodyDict={"appId": str(appId),
        "appVersion": "3.0",
        "name": str(deploymentName),
        "metadatas": [
                {
                        "namespace": "",
                        "name": "Job_Name",
                        "value": "%JOB_NAME%",
                        "editable": False,
                        "required": True
                },
                {
                        "namespace": "",
                        "name": "App_Name",
                        "value": "OpenCart",
                        "editable": False,
                        "required": True
                }
        ],
        "environmentId": str(environmentId),
        "tagIds": [
                1
        ],
        "securityProfileIds": [],
        "agingPolicyId": None,
        "suspensionPolicyId": None,
        "policyIds": None,
        "preventTermination": False,
        "parameters": {
                "appParams": [
                        {
                                "name": "TET_SENSOR_TYPE",
                                "value": "Deep Visibility with Enforcement"
                        }
                ],
                "cloudParams": {
                        "cloudRegionId": str(cloudRegionId),
                        "accountId": str(accountId)
                }
        },
        "jobs": [
                {
                        "tierId": "376",
                        "policyIds": None,
                        "tagIds": [],
                        "securityProfileIds": [],
                        "parameters": {
                                "appParams": [
                                        {
                                                "name": "referredJob",
                                                "value": ""
                                        }
                                ],
                                "envParams": [
                                        {
                                                "name": "HOSTNAME",
                                                "value": "WEB-%JOB_NAME%"
                                        },
                                        {
                                                "name": "JOBNAME",
                                                "value": "%JOB_NAME%"
                                        },
                                        {
                                                "name": "TET_SENSOR_TYPE",
                                                "value": "%TET_SENSOR_TYPE%"
                                        }
                                ],
                                "cloudParams": {
                                        "cloudRegionId": str(cloudRegionId),
                                        "accountId": str(accountId),
                                        "volumes": [
                                                {
                                                        "name": "RootVolume",
                                                        "bootable": True,
                                                        "size": 0,
                                                        "type": None,
                                                        "iops": None
                                                }
                                        ],
                                        "rootVolumeSize": "0",
                                        "cloudProperties": [
                                                {
                                                        "name": "FullClone",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "RootDiskResizeConfig",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "UserDataCenterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "UserDatastoreCluster",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "UserTargetDeploymentFolder",
                                                        "value": str(deploymentFolder)
                                                },
                                                {
                                                        "name": "UserClusterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "CC_AUTO_SELECT_NETWORK",
                                                        "value": False
                                                },
                                                {
                                                        "name": "numNICs",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "ExtensionId",
                                                        "value": "1"
                                                },
                                                {
                                                        "name": "AciTenantName",
                                                        "value": str(apicTenantName)
                                                },
                                                {
                                                        "name": "AciVmmDomainName",
                                                        "value": "vmware60-hybridcloud"
                                                },
                                                {
                                                        "name": "externalNetwork",
                                                        "value": "common:L3_Out:L3_Out"
                                                },
                                                {
                                                        "name": "serviceGraphTemplate",
                                                        "value": ""
                                                }
                                        ],
                                        "nics": [
                                                {
                                                        "order": 1,
                                                        "id": "common|securedc-inbound|securedc-inbound",
                                                        "type": "ACI_EPG"
                                                },
                                                {
                                                        "order": 2,
                                                        "id": str(apicEpgWeb1),
                                                        "type": "ACI_EPG"
                                                },
                                                {
                                                        "order": 3,
                                                        "id": str(apicEpgWeb2),
                                                        "type": "ACI_EPG"
                                                }
                                        ],
                                        "instance": "Micro"
                                }
                        }
                },
                {
                        "tierId": "377",
                        "policyIds": None,
                        "tagIds": [],
                        "securityProfileIds": [],
                        "parameters": {
                                "appParams": [
                                        {
                                                "name": "referredJob",
                                                "value": ""
                                        }
                                ],
                                "envParams": [
                                        {
                                                "name": "JOBNAME",
                                                "value": "%JOB_NAME%"
                                        },
                                        {
                                                "name": "HOSTNAME",
                                                "value": "DB-%JOB_NAME%"
                                        },
                                        {
                                                "name": "TET_SENSOR_TYPE",
                                                "value": "%TET_SENSOR_TYPE%"
                                        }
                                ],
                                "cloudParams": {
                                        "cloudRegionId": str(cloudRegionId),
                                        "accountId": str(accountId),
                                        "volumes": [
                                                {
                                                        "name": "RootVolume",
                                                        "bootable": True,
                                                        "size": 0,
                                                        "type": None,
                                                        "iops": None
                                                }
                                        ],
                                        "rootVolumeSize": "0",
                                        "cloudProperties": [
                                                {
                                                        "name": "FullClone",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "RootDiskResizeConfig",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "UserDataCenterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "UserDatastoreCluster",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "UserTargetDeploymentFolder",
                                                        "value": str(deploymentFolder)
                                                },
                                                {
                                                        "name": "UserClusterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "CC_AUTO_SELECT_NETWORK",
                                                        "value": False
                                                },
                                                {
                                                        "name": "numNICs",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "ExtensionId",
                                                        "value": "1"
                                                },
                                                {
                                                        "name": "AciTenantName",
                                                        "value": str(apicTenantName)
                                                },
                                                {
                                                        "name": "AciVmmDomainName",
                                                        "value": "vmware60-hybridcloud"
                                                },
                                                {
                                                        "name": "externalNetwork",
                                                        "value": "common:L3_Out:L3_Out"
                                                },
                                                {
                                                        "name": "serviceGraphTemplate",
                                                        "value": ""
                                                }
                                        ],
                                        "nics": [
                                                {
                                                        "order": 1,
                                                        "id": "common|securedc-inbound|securedc-inbound",
                                                        "type": "ACI_EPG"
                                                },
                                                {
                                                        "order": 2,
                                                        "id": str(apicEpgDb1),
                                                        "type": "ACI_EPG"
                                                }
                                        ],
                                        "instance": "Micro"
                                }
                        }
                }
        ],
        "timeZone": "UTC"
    }
    if output=='json':
        return json.dumps(deployOpencartBodyDict)
    elif output=='dict':
        return deployOpencartBodyDict
    elif output=='txt':
        return str(deployOpencartBodyDict)




def deployOpencartWithTetrationInjetorBodyV2(appId,deploymentName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,tetUserId,output='json'):
    deployOpencartWithTetrationInjetorBodyDict={"appId": str(appId),
        "appVersion": "3.0",
        "name": str(deploymentName),
        "metadatas": [
                {
                        "namespace": "",
                        "name": "Job_Name",
                        "value": "%JOB_NAME%",
                        "editable": False,
                        "required": True
                },
                {
                        "namespace": "",
                        "name": "App_Name",
                        "value": "OpenCart",
                        "editable": False,
                        "required": True
                }
        ],
        "environmentId": str(environmentId),
        "tagIds": [
                1
        ],
        "securityProfileIds": [],
        "agingPolicyId": None,
        "suspensionPolicyId": None,
        "policyIds": None,
        "preventTermination": False,
        "parameters": {
                "appParams": [
                        {
                                "name": "TET_SENSOR_TYPE",
                                "value": "Deep Visibility with Enforcement"
                        },
                        {
                                "name": "TET_USER_ID",
                                "value": str(tetUserId)
                        }
                ],
                "cloudParams": {
                        "cloudRegionId": str(cloudRegionId),
                        "accountId": str(accountId)
                }
        },
        "jobs": [
                {
                        "tierId": "376",
                        "policyIds": None,
                        "tagIds": [],
                        "securityProfileIds": [],
                        "parameters": {
                                "appParams": [
                                        {
                                                "name": "referredJob",
                                                "value": ""
                                        }
                                ],
                                "envParams": [
                                        {
                                                "name": "HOSTNAME",
                                                "value": "WEB-%JOB_NAME%"
                                        },
                                        {
                                                "name": "JOBNAME",
                                                "value": "%JOB_NAME%"
                                        },
                                        {
                                                "name": "TET_SENSOR_TYPE",
                                                "value": "%TET_SENSOR_TYPE%"
                                        },
                                        {
                                                "name": "TET_USER_ID",
                                                "value": "%TET_USER_ID%"
                                        }
                                ],
                                "cloudParams": {
                                        "cloudRegionId": str(cloudRegionId),
                                        "accountId": str(accountId),
                                        "volumes": [
                                                {
                                                        "name": "RootVolume",
                                                        "bootable": True,
                                                        "size": 0,
                                                        "type": None,
                                                        "iops": None
                                                }
                                        ],
                                        "rootVolumeSize": "0",
                                        "cloudProperties": [
                                                {
                                                        "name": "FullClone",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "RootDiskResizeConfig",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "UserDataCenterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "UserDatastoreCluster",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "UserTargetDeploymentFolder",
                                                        "value": str(deploymentFolder)
                                                },
                                                {
                                                        "name": "UserClusterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "CC_AUTO_SELECT_NETWORK",
                                                        "value": False
                                                },
                                                {
                                                        "name": "numNICs",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "ExtensionId",
                                                        "value": "1"
                                                },
                                                {
                                                        "name": "AciTenantName",
                                                        "value": str(apicTenantName)
                                                },
                                                {
                                                        "name": "AciVmmDomainName",
                                                        "value": "vmware60-hybridcloud"
                                                },
                                                {
                                                        "name": "externalNetwork",
                                                        "value": "common:L3_Out:L3_Out"
                                                },
                                                {
                                                        "name": "serviceGraphTemplate",
                                                        "value": ""
                                                }
                                        ],
                                        "nics": [
                                                {
                                                        "order": 1,
                                                        "id": "common|securedc-inbound|securedc-inbound",
                                                        "type": "ACI_EPG"
                                                },
                                                {
                                                        "order": 2,
                                                        "id": str(apicEpgWeb1),
                                                        "type": "ACI_EPG"
                                                },
                                                {
                                                        "order": 3,
                                                        "id": str(apicEpgWeb2),
                                                        "type": "ACI_EPG"
                                                }
                                        ],
                                        "instance": "Micro"
                                }
                        }
                },
                {
                        "tierId": "377",
                        "policyIds": None,
                        "tagIds": [],
                        "securityProfileIds": [],
                        "parameters": {
                                "appParams": [
                                        {
                                                "name": "referredJob",
                                                "value": ""
                                        }
                                ],
                                "envParams": [
                                        {
                                                "name": "JOBNAME",
                                                "value": "%JOB_NAME%"
                                        },
                                        {
                                                "name": "HOSTNAME",
                                                "value": "DB-%JOB_NAME%"
                                        },
                                        {
                                                "name": "TET_SENSOR_TYPE",
                                                "value": "%TET_SENSOR_TYPE%"
                                        },
                                        {
                                                "name": "TET_USER_ID",
                                                "value": "%TET_USER_ID%"
                                        }
                                ],
                                "cloudParams": {
                                        "cloudRegionId": str(cloudRegionId),
                                        "accountId": str(accountId),
                                        "volumes": [
                                                {
                                                        "name": "RootVolume",
                                                        "bootable": True,
                                                        "size": 0,
                                                        "type": None,
                                                        "iops": None
                                                }
                                        ],
                                        "rootVolumeSize": "0",
                                        "cloudProperties": [
                                                {
                                                        "name": "FullClone",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "RootDiskResizeConfig",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "UserDataCenterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "UserDatastoreCluster",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "UserTargetDeploymentFolder",
                                                        "value": str(deploymentFolder)
                                                },
                                                {
                                                        "name": "UserClusterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "CC_AUTO_SELECT_NETWORK",
                                                        "value": False
                                                },
                                                {
                                                        "name": "numNICs",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "ExtensionId",
                                                        "value": "1"
                                                },
                                                {
                                                        "name": "AciTenantName",
                                                        "value": str(apicTenantName)
                                                },
                                                {
                                                        "name": "AciVmmDomainName",
                                                        "value": "vmware60-hybridcloud"
                                                },
                                                {
                                                        "name": "externalNetwork",
                                                        "value": "common:L3_Out:L3_Out"
                                                },
                                                {
                                                        "name": "serviceGraphTemplate",
                                                        "value": ""
                                                }
                                        ],
                                        "nics": [
                                                {
                                                        "order": 1,
                                                        "id": "common|securedc-inbound|securedc-inbound",
                                                        "type": "ACI_EPG"
                                                },
                                                {
                                                        "order": 2,
                                                        "id": str(apicEpgDb1),
                                                        "type": "ACI_EPG"
                                                }
                                        ],
                                        "instance": "Micro"
                                }
                        }
                },
                {
                        "tierId": "378",
                        "policyIds": None,
                        "tagIds": [],
                        "securityProfileIds": [],
                        "parameters": {
                                "appParams": [
                                        {
                                                "name": "referredJob",
                                                "value": ""
                                        }
                                ],
                                "envParams": [],
                                "cloudParams": {
                                        "cloudRegionId": str(cloudRegionId),
                                        "accountId": str(accountId),
                                        "volumes": [],
                                        "rootVolumeSize": "0",
                                        "cloudProperties": [
                                                {
                                                        "name": "FullClone",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "RootDiskResizeConfig",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "UserDataCenterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "UserDatastoreCluster",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "UserTargetDeploymentFolder",
                                                        "value": str(deploymentFolder)
                                                },
                                                {
                                                        "name": "UserClusterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "CC_AUTO_SELECT_NETWORK",
                                                        "value": False
                                                },
                                                {
                                                        "name": "numNICs",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "ExtensionId",
                                                        "value": "1"
                                                },
                                                {
                                                        "name": "AciTenantName",
                                                        "value": str(apicTenantName)
                                                },
                                                {
                                                        "name": "AciVmmDomainName",
                                                        "value": "vmware60-hybridcloud"
                                                },
                                                {
                                                        "name": "externalNetwork",
                                                        "value": "common:L3_Out:L3_Out"
                                                },
                                                {
                                                        "name": "serviceGraphTemplate",
                                                        "value": ""
                                                }
                                        ],
                                        "nics": [
                                                {}
                                        ]
                                }
                        }
                }
        ],
        "timeZone": "UTC"
    }
    if output=='json':
        return json.dumps(deployOpencartWithTetrationInjetorBodyDict)
    elif output=='dict':
        return deployOpencartWithTetrationInjetorBodyDict
    elif output=='txt':
        return str(deployOpencartWithTetrationInjetorBodyDict)








def deployOpencartBody(appId,deploymentName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,output='json'):
    deployOpencartBodyDict={"appId": str(appId),
        "appVersion": "3.0",
        "name": deploymentName,
        "metadatas": [
                {
                        "namespace": "",
                        "name": "Job_Name",
                        "value": "%JOB_NAME%",
                        "editable": False,
                        "required": True
                },
                {
                        "namespace": "",
                        "name": "App_Name",
                        "value": "OpenCart",
                        "editable": False,
                        "required": True
                }
        ],
        "environmentId": environmentId,
        "securityProfileIds": [],
        "agingPolicyId": None,
        "suspensionPolicyId": None,
        "policyIds": None,
        "preventTermination": False,
        "parameters": {
                "appParams": [],
                "cloudParams": {
                        "cloudRegionId": str(cloudRegionId),
                        "accountId": str(accountId)
                }
        },
        "jobs": [
                {
                        "tierId": "340",
                        "policyIds": None,
                        "securityProfileIds": [],
                        "parameters": {
                                "appParams": [
                                        {
                                                "name": "referredJob",
                                                "value": ""
                                        }
                                ],
                                "envParams": [],
                                "cloudParams": {
                                        "cloudRegionId":str(cloudRegionId),
                                        "accountId":str(accountId),
                                        "volumes": [
                                                {
                                                        "name": "RootVolume",
                                                        "bootable": True,
                                                        "size": 0,
                                                        "type": None,
                                                        "iops": None
                                                }
                                        ],
                                        "rootVolumeSize": "0",
                                        "cloudProperties": [
                                                {
                                                        "name": "FullClone",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "RootDiskResizeConfig",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "UserDataCenterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "UserDatastoreCluster",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "UserTargetDeploymentFolder",
                                                        "value": deploymentFolder
                                                },
                                                {
                                                        "name": "UserClusterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "CC_AUTO_SELECT_NETWORK",
                                                        "value": False
                                                },
                                                {
                                                        "name": "numNICs",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "ExtensionId",
                                                        "value": "1"
                                                },
                                                {
                                                        "name": "AciTenantName",
                                                        "value": apicTenantName
                                                },
                                                {
                                                        "name": "AciVmmDomainName",
                                                        "value": "vmware60-hybridcloud"
                                                },
                                                {
                                                        "name": "externalNetwork",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "serviceGraphTemplate",
                                                        "value": ""
                                                }
                                        ],
                                        "nics": [
                                                {
                                                        "order": 1,
                                                        "id": apicEpgWeb1,
                                                        "type": "ACI_EPG"
                                                },
                                                {
                                                        "order": 2,
                                                        "id": apicEpgWeb2,
                                                        "type": "ACI_EPG"
                                                }
                                        ],
                                        "instance": "Micro"
                                }
                        }
                },
                {
                        "tierId": "341",
                        "policyIds": None,
                        "securityProfileIds": [],
                        "parameters": {
                                "appParams": [
                                        {
                                                "name": "referredJob",
                                                "value": ""
                                        }
                                ],
                                "envParams": [],
                                "cloudParams": {
                                        "cloudRegionId": str(cloudRegionId),
                                        "accountId": str(accountId),
                                        "volumes": [
                                                {
                                                        "name": "RootVolume",
                                                        "bootable": True,
                                                        "size": 0,
                                                        "type": None,
                                                        "iops": None
                                                }
                                        ],
                                        "rootVolumeSize": "0",
                                        "cloudProperties": [
                                                {
                                                        "name": "FullClone",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "RootDiskResizeConfig",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "UserDataCenterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "UserDatastoreCluster",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "UserTargetDeploymentFolder",
                                                        "value": deploymentFolder
                                                },
                                                {
                                                        "name": "UserClusterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "CC_AUTO_SELECT_NETWORK",
                                                        "value": False
                                                },
                                                {
                                                        "name": "numNICs",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "ExtensionId",
                                                        "value": "1"
                                                },
                                                {
                                                        "name": "AciTenantName",
                                                        "value": apicTenantName
                                                },
                                                {
                                                        "name": "AciVmmDomainName",
                                                        "value": "vmware60-hybridcloud"
                                                },
                                                {
                                                        "name": "externalNetwork",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "serviceGraphTemplate",
                                                        "value": ""
                                                }
                                        ],
                                        "nics": [
                                                {
                                                        "order": 1,
                                                        "id": apicEpgDb1,
                                                        "type": "ACI_EPG"
                                                }
                                        ],
                                        "instance": "Micro"
                                }
                        }
                }
        ],
        "timeZone": "UTC"
    }
    if output=='json':
        return json.dumps(deployOpencartBodyDict)
    elif output=='dict':
        return deployOpencartBodyDict
    elif output=='txt':
        return str(deployOpencartBodyDict)







def deployOpencartWithTaggsBody(appId,deploymentName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,output='json'):
    deployOpencartBodyDict={"appId": str(appId),
        "appVersion": "3.0",
        "name": deploymentName,
        "metadatas": [
                {
                        "namespace": "",
                        "name": "Job_Name",
                        "value": "%JOB_NAME%",
                        "editable": False,
                        "required": True
                },
                {
                        "namespace": "",
                        "name": "App_Name",
                        "value": "OpenCart",
                        "editable": False,
                        "required": True
                }
        ],
        "environmentId": environmentId,
        "tagIds": [
                1
        ],
        "securityProfileIds": [],
        "agingPolicyId": None,
        "suspensionPolicyId": None,
        "policyIds": None,
        "preventTermination": False,
        "parameters": {
                "appParams": [],
                "cloudParams": {
                        "cloudRegionId": str(cloudRegionId),
                        "accountId": str(accountId)
                }
        },
        "jobs": [
                {
                        "tierId": "340",
                        "policyIds": None,
                        "tagIds": [
                                1
                        ],
                        "securityProfileIds": [],
                        "parameters": {
                                "appParams": [
                                        {
                                                "name": "referredJob",
                                                "value": ""
                                        }
                                ],
                                "envParams": [],
                                "cloudParams": {
                                        "cloudRegionId":str(cloudRegionId),
                                        "accountId":str(accountId),
                                        "volumes": [
                                                {
                                                        "name": "RootVolume",
                                                        "bootable": True,
                                                        "size": 0,
                                                        "type": None,
                                                        "iops": None
                                                }
                                        ],
                                        "rootVolumeSize": "0",
                                        "cloudProperties": [
                                                {
                                                        "name": "FullClone",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "RootDiskResizeConfig",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "UserDataCenterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "UserDatastoreCluster",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "UserTargetDeploymentFolder",
                                                        "value": deploymentFolder
                                                },
                                                {
                                                        "name": "UserClusterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "CC_AUTO_SELECT_NETWORK",
                                                        "value": False
                                                },
                                                {
                                                        "name": "numNICs",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "ExtensionId",
                                                        "value": "1"
                                                },
                                                {
                                                        "name": "AciTenantName",
                                                        "value": apicTenantName
                                                },
                                                {
                                                        "name": "AciVmmDomainName",
                                                        "value": "vmware60-hybridcloud"
                                                },
                                                {
                                                        "name": "externalNetwork",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "serviceGraphTemplate",
                                                        "value": ""
                                                }
                                        ],
                                        "nics": [
                                                {
                                                        "order": 1,
                                                        "id": apicEpgWeb1,
                                                        "type": "ACI_EPG"
                                                },
                                                {
                                                        "order": 2,
                                                        "id": apicEpgWeb2,
                                                        "type": "ACI_EPG"
                                                }
                                        ],
                                        "instance": "Micro"
                                }
                        }
                },
                {
                        "tierId": "341",
                        "policyIds": None,
                        "tagIds": [
                                1
                        ],
                        "securityProfileIds": [],
                        "parameters": {
                                "appParams": [
                                        {
                                                "name": "referredJob",
                                                "value": ""
                                        }
                                ],
                                "envParams": [],
                                "cloudParams": {
                                        "cloudRegionId": str(cloudRegionId),
                                        "accountId": str(accountId),
                                        "volumes": [
                                                {
                                                        "name": "RootVolume",
                                                        "bootable": True,
                                                        "size": 0,
                                                        "type": None,
                                                        "iops": None
                                                }
                                        ],
                                        "rootVolumeSize": "0",
                                        "cloudProperties": [
                                                {
                                                        "name": "FullClone",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "RootDiskResizeConfig",
                                                        "value": "False"
                                                },
                                                {
                                                        "name": "UserDataCenterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "UserDatastoreCluster",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "UserTargetDeploymentFolder",
                                                        "value": deploymentFolder
                                                },
                                                {
                                                        "name": "UserClusterName",
                                                        "value": "HybridCloud"
                                                },
                                                {
                                                        "name": "CC_AUTO_SELECT_NETWORK",
                                                        "value": False
                                                },
                                                {
                                                        "name": "numNICs",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "ExtensionId",
                                                        "value": "1"
                                                },
                                                {
                                                        "name": "AciTenantName",
                                                        "value": apicTenantName
                                                },
                                                {
                                                        "name": "AciVmmDomainName",
                                                        "value": "vmware60-hybridcloud"
                                                },
                                                {
                                                        "name": "externalNetwork",
                                                        "value": ""
                                                },
                                                {
                                                        "name": "serviceGraphTemplate",
                                                        "value": ""
                                                }
                                        ],
                                        "nics": [
                                                {
                                                        "order": 1,
                                                        "id": apicEpgDb1,
                                                        "type": "ACI_EPG"
                                                }
                                        ],
                                        "instance": "Micro"
                                }
                        }
                }
        ],
        "timeZone": "UTC"
    }
    if output=='json':
        return json.dumps(deployOpencartBodyDict)
    elif output=='dict':
        return deployOpencartBodyDict
    elif output=='txt':
        return str(deployOpencartBodyDict)









####################################
#  Generic Functions
####################################




def createUser(protocol,host,port,authUser,authPassword,username,password,email,company,tenantId,regionId,logFormat=''):

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    data=createUserBody(username,password,email,company,tenantId,regionId,output="json")

    #path
    path=r'/v1/users'

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will create user in cloudcenter. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method='post',url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )

    if result:
        logger.autolog(level=1, message='We received result... ', format=logFormat)
        logger.autolog(level=1, message="Let's verify if user has been created" + str(email), format=logFormat)

        user=getUser(protocol,host,port,authUser,authPassword,email,logFormat=logFormat)
        if user== False:
            logger.autolog(level=1, message='Something went wrong while getting user details, returning False', format=logFormat)
            return False
        if len(user)==0:
            logger.autolog(level=1, message='Specified user was not found. Nothign to delete...', format=logFormat)
            return True
        if len(user)>1:
            logger.autolog(level=1, message='Something went wrong while getting user details, we got more than one!!!  returning False', format=logFormat)
            return False
        #Get the tenant from the list...
        user=user[0]


        logger.autolog(level=1, message="Got user, all good. " + str(user), format=logFormat)
        return user["id"],user["username"]
    else:
        if "RESOURCE_EXISTS" in result.text:
            logger.autolog(level=1, message='User already exists, no need to create...', format=logFormat)
            user=getUser(protocol,host,port,authUser,authPassword,email=email,logFormat=logFormat)
            if user== False:
                logger.autolog(level=1, message='Something went wrong while getting user details, returning False', format=logFormat)
                return False
            if len(user)==0:
                logger.autolog(level=1, message='Specified user was not found. Nothign to delete...', format=logFormat)
                return True
            if len(user)>1:
                logger.autolog(level=1, message='Something went wrong while getting user details, we got more than one!!!  returning False', format=logFormat)
                return False
            #Get the tenant from the list...
            user=user[0]
            return user["id"],user["username"]
        logger.autolog(level=1, message='Something went wrong while creating user, returning False', format=logFormat)
        return False



def getUser(protocol,host,port,authUser,authPassword,email='',logFormat=''):

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    data=''

    #path
    path=r'/v1/users?page=0&size=100'

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will get users in cloudcenter. Sending request to ' + str(url),format=logFormat)

    result=http.sendHttpRequest(protocol=protocol,host=host,method="get",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )

    if result:
        return getMatchedItemByValueName(result,itemValueToMatch="emailAddr",itemsToMatch="users",valueToMatch=email,logFormat='')

#        if email !='':
#            users=json.loads(result.text)['users']
#            for user in users:
#                if user["emailAddr"]==email:
#                    logger.autolog(level=1, message='We found matching user, ID is: ' + str(user["id"]), format=logFormat)
#                    return user
#            return False
#        else:
#            logger.autolog(level=1, message='We received list of users... ', format=logFormat)
#            return json.loads(result.text)['users'] 
    else:
        logger.autolog(level=1, message='Something went wrong while obtainig users list, returning False', format=logFormat)
        return False


def disableUser(protocol,host,port,authUser,authPassword,user,logFormat=''):


    logger.autolog(level=1, message="Disabling User: " + str(user["emailAddr"]) , format=logFormat)
    user['enabled']=False

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    data=json.dumps(user)

    #path
    path=r'/v1/users/'+str(user["id"])

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will disable  users in cloudcenter. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method="put",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )
    if result:
        logger.autolog(level=1, message='We received result ' + str(result), format=logFormat)
        return True
    else:
        logger.autolog(level=1, message='Something went wrong while obtainig users list, returning False', format=logFormat)
        return False




def createUserKey(protocol,host,port,authUser,authPassword,userId,logFormat=''):

    logger.autolog(level=1, message="Creating User API KEY: " + str(userId) , format=logFormat)

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    data=createKeyBody(output='json')

    #path
    path=r'/v1/users/'+str(userId)+r'/keys'

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will create  user API KEY. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method="post",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )
    if result:
        logger.autolog(level=1, message='We received result ' + str(result), format=logFormat)
        try:
            return json.loads(result.text)['apiKey']['key']
        except Exception as e:
            logger.autolog(level=1,message='Error while obtaining user API KEY ',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return False
    else:
        logger.autolog(level=1, message='Something went wrong while obtainig users list, returning False', format=logFormat)
        return False






def deleteUser(protocol,host,port,authUser,authPassword,email,logFormat=''):


    logger.autolog(level=1, message='About to delete user with email ' + str(email), format=logFormat)

    logger.autolog(level=1, message="Let's get the user's details..." + str(email), format=logFormat)
    user=getUser(protocol,host,port,authUser,authPassword,email,logFormat=logFormat)
    if user== False:
        logger.autolog(level=1, message='Something went wrong while getting user details, returning False', format=logFormat)
        return False
    if len(user)==0:
        logger.autolog(level=1, message='Specified user was not found. Nothign to delete...', format=logFormat)
        return True
    if len(user)>1:
        logger.autolog(level=1, message='Something went wrong while getting user details, we got more than one!!!  returning False', format=logFormat)
        return False

    #Get the tenant from the list...
    user=user[0]


    if not disableUser(protocol,host,port,authUser,authPassword,user,logFormat=logFormat):
        logger.autolog(level=1, message='Something went wrong while disabling user details, returning False', format=logFormat)
        return False

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    data=''

    #path
    path=r'/v1/users/'+str(user["id"])

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will delete user in cloudcenter. Sending request to ' + str(url),format=logFormat)

    result=http.sendHttpRequest(protocol=protocol,host=host,method="delete",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )

    if result:
        logger.autolog(level=1, message='We received result ' + str(result), format=logFormat)
        return True 
    else:
        logger.autolog(level=1, message='Something went wrong while deleting user, returning False', format=logFormat)
        return False





def createTenant(protocol,host,port,authUser,authPassword,tenantName,userId,email,logFormat=''):

    logger.autolog(level=1, message="Creating TENANT: " + str(tenantName) , format=logFormat)

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    data=createTenantBody(tenantName,userId,email,output='json')

    #path
    path=r'/v1/tenants/'

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will create TENANT. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method="post",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )
    if result:
        logger.autolog(level=1, message='We received result ' + str(result), format=logFormat)
        try:
            return json.loads(result.text)["id"]
        except Exception as e:
            logger.autolog(level=1,message='Error while creating TENANT ',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return False
    else:
        if "TENANT_WITH_SAME_SHORT_NAME_ALREADY_EXISTS" in result.text:
            logger.autolog(level=1, message='Tenant already exists, no need to create...', format=logFormat)
            return True
        logger.autolog(level=1, message='Something went wrong while creating tenant, returning False', format=logFormat)
        return False





def addManagePlanTenant(protocol,host,port,authUser,authPassword,planId,contractId,userId,logFormat=''):

    logger.autolog(level=1, message="Adding Manage plan to  TENANT: " + str(userId) , format=logFormat)

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    data=tenantManagePlansBody(planId,contractId,userId,output='json')

    #path
    path=r'/v1/users/'+str(userId)

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will add MANAGE PLAN to TENANT. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method="post",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )
    if result:
        return True
    else:
        if "Same plan and contract selected" in result.text:
            logger.autolog(level=1, message='Plan is already selected,nothing to do...', format=logFormat)
            return True
        logger.autolog(level=1, message='Something went wrong while adding Manage Plan tenant, returning False', format=logFormat)
        return False


def createTenantManagePlan(protocol,host,port,authUser,authPassword,tenantId,managePlanName,logFormat=''):

    logger.autolog(level=1, message="Adding Manage plan to  TENANT: " + str(userId) , format=logFormat)

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    data=createTenantManagePlansBody(managePlanName,tenantId,output='json')

    #path
    path=r'/v1/tenants/'+str(tenantId)+r'/plans'

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will create MANAGE PLAN to TENANT. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method="post",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )
    if result:
        return True
    else:
        logger.autolog(level=1, message='Something went wrong while create Manage Plan tenant, returning False', format=logFormat)
        return False


def createTenantContract(protocol,host,port,authUser,authPassword,tenantId,contractName,logFormat=''):

    logger.autolog(level=1, message="Adding Manage plan to  TENANT: " + str(userId) , format=logFormat)

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    data=createTenantContractBody(contractName)

    #path
    path=r'/v1/tenants/'+str(tenantId)+r'/contracts'

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will create contract in TENANT. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method="post",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )
    if result:
        return True
    else:
        logger.autolog(level=1, message='Something went wrong while creating contract in tenant, returning False', format=logFormat)
        return False


def getTenant(protocol,host,port,authUser,authPassword,tenantName,logFormat=''):

    logger.autolog(level=1, message="Geting TENANT: " + str(tenantName) , format=logFormat)

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    data=''

    #path
    path=r'/v1/tenants/'

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will get  TENANT. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method="get",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )
    if result:
        return getMatchedItemByValueName(result,itemValueToMatch="name",itemsToMatch="tenants",valueToMatch=tenantName,logFormat=logFormat)
    else:
        logger.autolog(level=1, message='Something went wrong while creating tenant, returning False', format=logFormat)
        return False







def deleteTenant(protocol,host,port,authUser,authPassword,tenantName,logFormat=''):

    logger.autolog(level=1, message="Deleting TENANT: " + str(tenantName) , format=logFormat)

    logger.autolog(level=1, message="Let's get the tenants details..." + str(tenantName), format=logFormat)
    tenant=getTenant(protocol,host,port,authUser,authPassword,tenantName,logFormat=logFormat)
    if tenant == False:
        logger.autolog(level=1, message='Something went wrong while getting tenant details, returning False', format=logFormat)
        return False
    if len(tenant)==0:
        logger.autolog(level=1, message='Specified tenant was not found. Nothign to delete...', format=logFormat)
        return True
    if len(tenant)>1:
        logger.autolog(level=1, message='Something went wrong while getting tenant details, we got more than one!!!  returning False', format=logFormat)
        return False

    #Get the tenant from the list...
    tenant=tenant[0]

    logger.autolog(level=1, message="Got tenant details... " , format=logFormat)

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    data=''

    #path
    path=r'/v1/tenants/'+str(tenant["id"])

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will delete TENANT. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method="delete",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )
    if result:
        return True
    else:
        logger.autolog(level=1, message='Something went wrong while deleting tenant, returning False', format=logFormat)
        return False



def createOpencartDeployment(protocol,host,port,authUser,authPassword,appId,deploymentName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,tetUserId,logFormat=''):

    logger.autolog(level=1, message="Deploying application : " + str(deploymentName) , format=logFormat)

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    data=deployOpencartBody(appId,deploymentName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,output='json')
    data=deployOpencartWithTetrationInjetorBody(appId,deploymentName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,tetUserId,output='json')

    #path
    path=r'/v2/jobs'

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will deploy application OPencart. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method="post",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )
    if result:
        return True
    else:
        if "DUPLICATE_JOB_NAME_IN_THE_ENVIRONMENT" in result.text:
            logger.autolog(level=1, message='Opencart app with this name already exists, no need to deploy...', format=logFormat)
            return True
        logger.autolog(level=1, message='Something went wrong deploying opencart, returning False', format=logFormat)
        return False



def createOpencartDeploymentV2(protocol,host,port,authUser,authPassword,appId,deploymentName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,tetUserId,logFormat=''):

    logger.autolog(level=1, message="Deploying application : " + str(deploymentName) , format=logFormat)

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    #data=deployOpencartBody(appId,deploymentName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,output='json')
    data=deployOpencartWithTetrationInjetorBodyV2(appId,deploymentName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,tetUserId,output='json')

    #path
    path=r'/v2/jobs'

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will deploy application OPencart. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method="post",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )
    if result:
        jobId=json.loads(result.text)["id"]
        nodeIpAddresses={}
        waitCount=0
        logger.autolog(level=1, message='Deployment with id ' + str(jobId) + ' in progress, will wait for 60 econds before checkign status... ',format=logFormat)
        time.sleep(60)
        while(waitCount<10):
            logger.autolog(level=1, message='Will request status of deployment with id ' + str(jobId),format=logFormat)
            job=getDeploymentDetails(protocol,host,port,authUser,authPassword,jobId,logFormat=logFormat)
            job=json.loads(job.text)
            if job["status"]=="JobInProgress":
                logger.autolog(level=1, message='Deployment in progress..., id:  ' + str(jobId),format=logFormat)
                waitCount=waitCount+1
            elif job["status"]=="JobRunning":
                    childJobs=job["childJobs"]
                    for childJob in childJobs:
                        childJobDetails=getDeploymentDetails(protocol,host,port,authUser,authPassword,childJob["id"],logFormat=logFormat)
                        childJobDetails=json.loads(childJobDetails.text)
                        if childJobDetails["name"] in ["Apache", "Database"]:
                            nodeIpAddresses[childJobDetails["name"]]=childJobDetails["virtualMachines"][0]["publicIpAddr"]
                    return nodeIpAddresses
            elif job["status"]=="JobError":
                    logger.autolog(level=1, message='Deployment failed: Job Status is... ' + str(job["status"]),format=logFormat)
                    return False
            logger.autolog(level=1, message='Job Status is... ' + str(job["status"]),format=logFormat)
            time.sleep(60)
        return False
    else:
        if "DUPLICATE_JOB_NAME_IN_THE_ENVIRONMENT" in result.text:
            logger.autolog(level=1, message='Opencart app with this name already exists, no need to deploy...', format=logFormat)
            return True
        logger.autolog(level=1, message='Something went wrong deploying opencart, returning False', format=logFormat)
        return False



def createOpencartDeploymentV3(protocol,host,port,authUser,authPassword,appId,deploymentName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,logFormat=''):

    logger.autolog(level=1, message="Deploying application : " + str(deploymentName) , format=logFormat)

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    #data=deployOpencartBody(appId,deploymentName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,output='json')
    data=deployOpencartBodyV3(appId,deploymentName,environmentId,cloudRegionId,accountId,deploymentFolder,apicTenantName,apicEpgDb1,apicEpgWeb1,apicEpgWeb2,output='json')

    #path
    path=r'/v2/jobs'

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will deploy application OPencart. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method="post",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )
    if result:
        jobId=json.loads(result.text)["id"]
        nodeIpAddresses={}
        waitCount=0
        logger.autolog(level=1, message='Deployment with id ' + str(jobId) + ' in progress, will wait for 60 econds before checkign status... ',format=logFormat)
        time.sleep(60)
        while(waitCount<90):
            logger.autolog(level=1, message='Will request status of deployment with id ' + str(jobId),format=logFormat)
            job=getDeploymentDetails(protocol,host,port,authUser,authPassword,jobId,logFormat=logFormat)
            job=json.loads(job.text)
            if job["status"]=="JobInProgress":
                logger.autolog(level=1, message='Deployment in progress..., id:  ' + str(jobId),format=logFormat)
                waitCount=waitCount+1
            elif job["status"]=="JobRunning":
                    childJobs=job["childJobs"]
                    for childJob in childJobs:
                        childJobDetails=getDeploymentDetails(protocol,host,port,authUser,authPassword,childJob["id"],logFormat=logFormat)
                        childJobDetails=json.loads(childJobDetails.text)
                        if childJobDetails["name"] in ["Apache", "Database"]:
                            nodeIpAddresses[childJobDetails["name"]]=childJobDetails["virtualMachines"][0]["publicIpAddr"]
                    return nodeIpAddresses
            elif job["status"]=="JobError":
                    logger.autolog(level=1, message='Deployment failed: Job Status is... ' + str(job["status"]),format=logFormat)
                    return False
            logger.autolog(level=1, message='Job Status is... ' + str(job["status"]),format=logFormat)
            time.sleep(60)
        logger.autolog(level=1, message='Opencart Deployment in Cloudcenter is taking too long, marking as failed... ' + str(job["status"]),format=logFormat)
        return False
    else:
        if "DUPLICATE_JOB_NAME_IN_THE_ENVIRONMENT" in result.text:
            logger.autolog(level=1, message='Opencart app with this name already exists, no need to deploy...', format=logFormat)
            return True
        logger.autolog(level=1, message='Something went wrong deploying opencart, returning False', format=logFormat)
        return False



def deleteOpencartDeployment(protocol,host,port,authUser,authPassword,appName,logFormat=''):

    logger.autolog(level=1, message="Deleting deployment  opencart : " + str(appName) , format=logFormat)


    logger.autolog(level=1, message="Let's get the job details..." + str(appName), format=logFormat)
    job=getDeployment(protocol,host,port,authUser,authPassword,appName,logFormat=logFormat)
    if job== False:
        logger.autolog(level=1, message='Something went wrong while getting job  details, returning False', format=logFormat)
        return False
    if len(job)==0:
        logger.autolog(level=1, message='Specified job was not found. Nothign to delete...', format=logFormat)
        return True
    if len(job)>1:
        logger.autolog(level=1, message='Something went wrong while getting job details, we got more than one!!!  returning False', format=logFormat)
        return False

    #Get the job from the list...
    job=job[0]

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    data=''

    #path
    path=r'/v2/jobs/'+str(job["id"])

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will delete application OPencart. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method="delete",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )
    if result:
        return True
    else:
        logger.autolog(level=1, message='Something went wrong deleting  opencart, returning False', format=logFormat)
        return False



def getDeployment(protocol,host,port,authUser,authPassword,appName,logFormat=''):

    logger.autolog(level=1, message="Geting DEPLOYMENTS: " + str(appName) , format=logFormat)

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    data=''

    #path
    path=r'/v2/jobs'

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will get list of  jobs. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method="get",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )
    if result:
        return getMatchedItemByValueName(result,itemValueToMatch="name",itemsToMatch="jobs",valueToMatch=appName,logFormat=logFormat)
    else:
        logger.autolog(level=1, message='Something went wrong while geting job, returning False', format=logFormat)
        return False


def getDeploymentDetails(protocol,host,port,authUser,authPassword,jobId,logFormat=''):

    logger.autolog(level=1, message="Geting DEPLOYMENTS details of job id: " + str(jobId) , format=logFormat)

    #Setup Headers
    headers={'Content-Type':r'application/json','Accept':r'application/json'}

    #Get payload/body
    data=''

    #path
    path=r'/v2/jobs/'+str(jobId)+r'?includeNodeDetails=true'

    #Setup URL
    url  = '%s://%s:%s%s' % (protocol,host,port,path)

    logger.autolog(level=1, message='Will get list of  jobs. Sending request to ' + str(url),format=logFormat)
    result=http.sendHttpRequest(protocol=protocol,host=host,method="get",url=path,data=data, headers=headers, basicAuth={"user":authUser,"password":authPassword} )
    if result:
        return result
    else:
        logger.autolog(level=1, message='Something went wrong while geting job, returning False', format=logFormat)
        return False



def getMatchedItemByValueName(httpResult,itemValueToMatch="name",itemsToMatch="tenants",valueToMatch='tenantName',logFormat=''):
    #If itemsToMatch is specified, it will return the item that contains property "itemValueToMatch" matching "valueToMatch"
    #If there are no matches, it returns empty list
    #If no itemsToMatch  is specified, it returns the whole list of items

    try:
        items=json.loads(httpResult.text)[itemsToMatch]
        if valueToMatch !='':            
            for item in items:
                if item[itemValueToMatch]== valueToMatch:
                    logger.autolog(level=1, message='We found matching ' +str(itemValueToMatch) +' to ' + str(valueToMatch), format=logFormat)
                    return [item]
            return []
        return items
    except Exception as e:
        logger.autolog(level=1, message='Error while getting item by name: ' + str(e), format=logFormat)
        return False 


