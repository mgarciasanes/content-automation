import urllib2
import json
import os
import requests
import sys
import time
import platform

import ssl
import logging
requests.packages.urllib3.disable_warnings()

modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger
import http as http


####################################
#  Global Variables
####################################
credsFile=gen.credsFile



   

def createUcsdUser(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            username=gen.sanitizeString(otherInputs["username"])
            password=gen.sanitizeString(otherInputs["password"])
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'


    api='/app/api/rest'
    headers={'X-Cloupia-Request-Key': creds['token']}
    workflowName="Create User and Group in UCSD"
    data= '?formatType=json&opName=userAPISubmitWorkflowServiceRequest&opData={param0:"'+workflowName+'",param1:{"list":[{"name":"username","value":"'+username+'"},{"name":"password","value":"'+password+'"}]},param2:-1}'
    logger.autolog(level=1, message=' data is : ' + str(data))

    result=http.sendHttpInLineRequest(protocol='http',host=creds['host'],url=api, data=data, headers=headers)

    if(result):
        return {},'Success'
    else:
        return {},'Failed:'+str(result)

def deleteUcsdUser(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            username=gen.sanitizeString(otherInputs["username"])
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    api='/app/api/rest'
    headers={'X-Cloupia-Request-Key': creds['token']}
    workflowName="Delete User and Group in UCSD"
    data= '?formatType=json&opName=userAPISubmitWorkflowServiceRequest&opData={param0:"'+workflowName+'",param1:{"list":[{"name":"username","value":"'+username+'"}]},param2:-1}'
    logger.autolog(level=1, message=' data is : ' + str(data))

    result=http.sendHttpInLineRequest(protocol='http',host=creds['host'],url=api, data=data, headers=headers)

    if(result):
        return {},'Success'
    else:
        return {},'Failed:'+str(result)





if __name__ == '__main__':

    logger.setLogFiles(newLogFile='ucsd.log',newLogDebugFile='ucsd_DEBUG.log')
    logger.setLogger()
    logging.getLogger('werkzeug').setLevel(logging.ERROR)

    #createUcsdUser(1000, 'ucsd', otherInputs={'username': 'test' , 'password' : 'password'})
    deleteUcsdUser(1000, 'ucsd', otherInputs={'username': 'test'})





