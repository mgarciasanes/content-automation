import logging
import os
import os.path
import requests
import sys
import time
import xml.dom.minidom
import yaml
import platform
from xml.dom import minidom
import datetime


modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger


####################################
#  Global Variables
####################################
credsFile=gen.credsFile


def createVm(sessionId, target, otherInputs=[], logFormat=''):
    global scriptPath
    if (type(otherInputs) is dict):
        try:
            vmName=otherInputs["vmName"]
            portProfile=otherInputs["portProfile"]
            username=otherInputs["username"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    scriptPath = r"C:\\scripts\\scvmm\\"
    scriptName = r"cloneVm.ps1"
    psArgs={'username': str(username) ,'vmName': str(vmName) , 'portProfile': '"'+str(portProfile)+'"', 'sessionId': '"'+str(sessionId)+'"'}
    stdOut, stdErr = gen.sshExecPsScriptV2(creds, scriptPath, scriptName, psArgs, timeout=60)

    if stdErr=='[]':
        return {'result':stdOut},'Success'
    else:
        return {'result':stdErr}, 'Failed: ' + str(stdErr)
    

def createWAPUser(sessionId, target, otherInputs=[], logFormat=''):
    global scriptPath
    if (type(otherInputs) is dict):
        try:
            username=otherInputs["username"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    scriptPath = r"C:\\scripts\\wap\\"
    scriptName = r"createWAPUser.ps1"
    psArgs={'username': str(username) , 'sessionId': '"'+str(sessionId)+'"'}
    stdOut, stdErr = gen.sshExecPsScriptV2(creds, scriptPath, scriptName, psArgs, timeout=60)

    if stdErr=='[]' or "Resource already exists" in stdErr:
        return {'result':stdOut},'Success'
    else:
        return {'result':stdErr}, 'Failed: ' + str(stdErr)





def deleteWAPUser(sessionId, target, otherInputs=[], logFormat=''):
    global scriptPath
    if (type(otherInputs) is dict):
        try:
            username=otherInputs["username"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    scriptPath = r"C:\\scripts\\wap\\"
    scriptName = r"deleteWAPUser.ps1"
    psArgs={'username': str(username) , 'sessionId': '"'+str(sessionId)+'"'}
    stdOut, stdErr = gen.sshExecPsScriptV2(creds, scriptPath, scriptName, psArgs, timeout=60)

    if stdErr=='[]' or "Preparing modules for first use" in stdErr:
        return {'result':stdOut},'Success'
    else:
        return {'result':stdErr}, 'Failed: ' + str(stdErr)





def removeVm(sessionId, target,  otherInputs=[], logFormat=''):
    global scriptPath
    logger.autolog(level=1,message='Starting removeADUser',format=logFormat)

    if (type(otherInputs) is dict):
        try:
            vmName=otherInputs["vmName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    scriptPath = r"C:\\scripts\\scvmm\\"
    scriptName = r"deleteVm.ps1"
    psArgs={'vmName': str(vmName), 'sessionId': str(sessionId)}
    stdOut, stdErr = gen.sshExecPsScriptV2(creds, scriptPath, scriptName, psArgs, timeout=60)

    if stdErr=='[]':
        return {'result':stdOut},'Success'
    else:
        return {'result':stdErr}, 'Failed: ' + str(stdErr)
    


