import urllib2
import os
import requests
import time
import platform
import sys
import json


from tetpyclient import RestClient

import ssl
import logging
requests.packages.urllib3.disable_warnings()

modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger
import http as http


####################################
#  Global Variables
####################################

tetrationUserId={"lon":["","","","",
"","","","5b0c6aad755f02093b08c093","5b0c6adb497d4f46240bbd70",
"5b0c6afe755f02360208c095","","","","",
"","","","",""]}


sdcTetrationUserId={"rtp":[u'5b36597d497d4f0bd3c8a753', u'5b365af9497d4f0bd3c8a757', u'5b3a8bf5755f020b65093f6b', u'5b3a8e2b497d4f1242c8a749', u'5b3a8e9d755f023d13093f47', u'5b3a8f77497d4f13ecc8a759', u'5b3a901c497d4f0733c8a756', u'5b3a915b497d4f2664c8a750', u'5b3a91c7497d4f1242c8a753', u'5b3a929c755f020b65093f75', u'5b3a9323497d4f2664c8a756', u'5b3aa318755f026e87093f4f', u'5b3aa37f497d4f2664c8a75a', u'5b3aa3ff497d4f13ecc8a76b', u'5b3aa46c497d4f13ecc8a76e', u'5b55beaf755f02389c093f48', u'5b3aa4da497d4f1242c8a75a', u'5b3aa538497d4f2664c8a761', u'5b3aa59b755f025c45093f2f', u'5b3aa607755f026e87093f5e', u'5b6c56f0755f023f3e093f2b',u'5b733fd9497d4f3d9ec8a7e9',u'5b73407a497d4f3d9ec8a7ec',u'5b7340df497d4f4e3dc8aa18',u'5b73412c755f0226580945bf',u'5b7341ba755f023de4094060',u'5b73420b497d4f7c2ec8a746',u'5b73453c755f02102c093f38',u'5b7345a5497d4f364ec8aa75',u'5b7345ef497d4f3d9ec8a7fc'],"lon":[],"sng":[],"sjc":[]}



tetrationCredsTargetName={'lon':"tetration-lon", "rtp":"tetration-rtp", "sng":"tetration-sng"}



####################################
#  Front-end  Functions
####################################

def updateUserEmailAddress(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            tetUserId=otherInputs["tetUserId"]
            dc=otherInputs["dc"]
            owner=otherInputs["owner"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'


    #creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    creds = gen.getDemoCredentialsByName(target['file'],tetrationCredsTargetName[dc])

    if not creds:
        return {},'Failed: Unable to get credentials for target'

    rc=tetrationLogin(creds,logFormat=logFormat)

    if rc:
        result=updateUser(rc,tetUserId,dc,owner, logFormat=logFormat)
    else:
        return {},'Failed: Unable to login to Tetration...'

    if(result):
        return {},'Success'
    else:
        return {},'Failed: Unable to update user, please check the logs...'



def deleteTetrationAdm(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            scope=otherInputs["scope"]
            dc=otherInputs["dc"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    #creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    creds = gen.getDemoCredentialsByName(target['file'],tetrationCredsTargetName[dc])

    if not creds:
        return {},'Failed: Unable to get credentials for target'

    rc=tetrationLogin(creds,logFormat=logFormat)

    if rc:
        scopeName="pod_"+str(int(scope)+100)
        scopeIds=[]

        parentScope=getScopeByName(rc,scopeName,logFormat=logFormat)
        scopeIds.append(parentScope['id'])
        scopeIds=scopeIds+parentScope['child_app_scope_ids']

        for scopeId in scopeIds:
            deleteAdm(rc,scopeId,logFormat=logFormat)
        return {},'Success'
    else:
        return {},'Failed: Unable to login to Tetration...'
    return {},'Failed: Unable to delete applications. Please check logs...'




def createSecureDcTetration(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            tetUserId=otherInputs["tetUserId"]
            dc=otherInputs["dc"]
            scopeName=otherInputs["scopeName"]
            parentScopeId=otherInputs["parentScopeId"]
            inventoryFilterName=otherInputs["inventoryFilterName"]
            agentConfigProfileName=otherInputs["agentConfigProfileName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'


    #creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    creds = gen.getDemoCredentialsByName(target['file'],tetrationCredsTargetName[dc])

    if not creds:
        return {},'Failed: Unable to get credentials for target'


    try:
        tetUserId=sdcTetrationUserId[dc][int(tetUserId)-1]
    except Exception as e:
        logger.autolog(level=1,message='Unable to get tetUserId: ' + str(e),format=logFormat)
        return {}, 'Failed: Error while getting tetration user ID. ' +str(e)


    scopeNameNoDash=scopeName.replace("-","")

    rc=tetrationLogin(creds,logFormat=logFormat)
    time.sleep(5)

    if rc:

        result=False
      
        scopeQuery={"type": "contains", "field": "host_name", "value": scopeName}

        scope=createScope(rc,scopeNameNoDash,parentScopeId,scopeQuery=scopeQuery,logFormat=logFormat)      
        time.sleep(5)

        role=createRole(rc,scopeName,parentScopeId,abilities=[{"scopeId":scope["id"],"ability":"SCOPE_OWNER"},{"scopeId":parentScopeId,"ability":"SCOPE_READ"}],logFormat=logFormat)
        time.sleep(5)

        user=updateUserEmail(rc,tetUserId,scopeName+'@cisco.com' , logFormat=logFormat)
        time.sleep(5)

        logger.autolog(level=1,message=str(role),format=logFormat)

        user=updateUserRole(rc,tetUserId,role["id"],logFormat=logFormat)
        time.sleep(5)

        invFilter=createInventoryFilter(rc,scope["id"],inventoryFilterName,scopeName,logFormat=logFormat)
        time.sleep(5)

        profile=getSensorConfigProfile(rc,agentConfigProfileName,logFormat=logFormat)
        time.sleep(5)

        createSoftwareSensorIntent(rc,profile["id"],invFilter["id"],logFormat=logFormat)
        time.sleep(5)


        #Get inventory filters (by name) to be used on the absolute policies
        filter={}
        inventories = rc.get('/filters/inventories')
        for inv in json.loads(inventories.content):
            if inv["name"] in  ["quarantine-fmc", "attacker"]:
                filter[inv["name"]]=inv["id"]
        absPolicies=[{"provider_filter_id": filter["attacker"],"action":"DENY","priority":"100","consumer_filter_id": filter["quarantine-fmc"],"l4_params":[{"port":[0,65535],"proto":1}] }]
        time.sleep(5)

        createAdm(rc,scope["id"],scopeName, enforcementEnabled=True,absPolicies=absPolicies, logFormat=logFormat)
        time.sleep(5)

        result=True

    else:
        return {},'Failed: Unable to login to Tetration...'

    if(result):
        return {},'Success'
    else:
        return {},'Failed: Unable to update user, please check the logs...'




def deleteSecureDcTetration(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            tetUserId=otherInputs["tetUserId"]
            dc=otherInputs["dc"]
            scopeName=otherInputs["scopeName"]
            parentScopeId=otherInputs["parentScopeId"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'


    #creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    creds = gen.getDemoCredentialsByName(target['file'],tetrationCredsTargetName[dc])

    if not creds:
        return {},'Failed: Unable to get credentials for target'

    tetUserIdNumber=tetUserId
    try:
        tetUserId=sdcTetrationUserId[dc][int(tetUserId)-1]
    except Exception as e:
        logger.autolog(level=1,message='Unable to get tetUserId: ' + str(e),format=logFormat)
        return {}, 'Failed: Error while getting tetration user ID. ' +str(e)


    scopeNameNoDash=scopeName.replace("-","")

    rc=tetrationLogin(creds,logFormat=logFormat)

    if rc:

        result=False

        user=updateUserEmail(rc,tetUserId,'securedc-tet-'+str(tetUserIdNumber)+'@dcv.svpod' , logFormat=logFormat)
        time.sleep(5)

        role=getRoleByName(rc,scopeName,logFormat=logFormat)
        time.sleep(5)
        if role:
            deleteRole(rc,role["id"],logFormat=logFormat)
        time.sleep(5)


        scope=getScopeByName(rc,"securedctet:"+str(scopeNameNoDash),logFormat=logFormat)
        time.sleep(5)

        deleteScopeRecursive(rc,scope["id"],parentScopeId,logFormat=logFormat)
        result=True

    else:
        return {},'Failed: Unable to login to Tetration...'

    if(result):
        return {},'Success'
    else:
        return {},'Failed: Unable to update user, please check the logs...'




####################################
#  Generic  Functions
####################################

def tetrationLogin(creds,logFormat=''):
    try:
        logger.autolog(level=1,message='About to login to Tetration : ' + str(creds['host']) + " with file : " +str(creds['filename']) ,format=logFormat)
        rc = RestClient(creds['host'],credentials_file=creds['path']+creds['filename'], verify=False)
        return rc
    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while loggin into tetration: ' + str(e),format=logFormat)
        return False



def updateUser(rc,tetUserId,dc,owner,logFormat=''):
    try:
        tetUserId=tetrationUserId[dc][int(tetUserId)-1]
        user_email_update_payload={"email":owner+"@dcloud.cisco.com"}
        response = rc.put('/openapi/v1/users/'+str(tetUserId),json_body=json.dumps(user_email_update_payload))
        # Parse the response only if 200OK has been received
        if response.status_code == 200:
            return response
        else:
            logger.autolog(level=1,message='Something went worng while updating the user: ' + str(json.loads(response.content)) ,format=logFormat)
            return False
    except Exception as e:
        logger.autolog(level=1,message='Something went worng while updating the user: ' + str(e),format=logFormat)
        return False




def updateUserEmail(rc,tetUserId,email,logFormat=''):
    try:

        #tetUserId=tetrationUserId[dc][int(tetUserId)-1]

        user_email_update_payload={"email": str(email)}
        response = rc.put('/openapi/v1/users/'+str(tetUserId),json_body=json.dumps(user_email_update_payload))
        # Parse the response only if 200OK has been received
        if response.status_code == 200:
            logger.autolog(level=1,message='User email updated... ' + str(email) ,format=logFormat)
            return json.loads(response.content)
        else:
            logger.autolog(level=1,message='Something went worng while updating the user: ' + str(json.loads(response.content)) ,format=logFormat)
            return False
    except Exception as e:
        logger.autolog(level=1,message='Something went worng while updating the user: ' + str(e),format=logFormat)
        return False


def updateUserRole(rc,tetUserId,roleId,logFormat=''):
    try:

        #tetUserId=tetrationUserId[dc][int(tetUserId)-1]

        user_role_update_payload={"role_id": str(roleId)}
        response=rc.put('/openapi/v1/users/'+str(tetUserId)+r'/add_role',json_body=json.dumps(user_role_update_payload))

        # Parse the response only if 200OK has been received
        if response.status_code == 200:
            logger.autolog(level=1,message='User role  updated... ' + str(roleId) ,format=logFormat)
            return response

        else:
            logger.autolog(level=1,message='Something went worng while updating the user role: ' + str(json.loads(response.content)) ,format=logFormat)
            return False
    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while updating the user role: ' + str(e),format=logFormat)
        return False


def getScopeByName(rc,scopeName, logFormat=''):
    resp = rc.get('/openapi/v1/app_scopes/')
    if resp.status_code == 200:
        resp_data = resp.json()
    else:
        logger.autolog(level=1,message="[ERROR] reading application scopes.",format=logFormat)
        logger.autolog(level=1,message=str(resp.text),format=logFormat)
        resp_data = {}
        return False
    try:
        for scope in resp_data:
            if  scope["name"] == scopeName:
                logger.autolog(level=1,message='We got a matching SCOPE! ' + str(scopeName),format=logFormat)
                logger.autolog(level=1,message=str(scope),format=logFormat)
                return scope
        logger.autolog(level=1,message='No matching SCOPE!!! ' + str(scopeName),format=logFormat)
        return {}
    except Exception as e:
        logger.autolog(level=1,message='Something went worng while getting scopes: ' + str(e),format=logFormat)
        return False


def createScope(rc,scopeName,parentScopeId, scopeQuery="",logFormat=''):

    if scopeQuery=="":
        scopeQuery={"type":"contains","field": "host_name","value": "{}".format(scopeName)}

    try:
        # Define payload for the creation of the Application Scope on Tetration
        scope_req_payload = {"short_name": "{}".format(scopeName),"short_query": scopeQuery ,"parent_app_scope_id": "{}".format(parentScopeId)}
        scope_resp = rc.post('/openapi/v1/app_scopes', json_body=json.dumps(scope_req_payload))

        # Parse the response only if 200OK has been received 
        if scope_resp.status_code == 200:
            parsed_resp = json.loads(scope_resp.content)
            # Commit dirty scopes
            commit_json = {'root_app_scope_id': parentScopeId}
            commit = rc.post('/openapi/v1/app_scopes/commit_dirty', json_body=json.dumps(commit_json))
            logger.autolog(level=1,message='Scope created ' + str(scopeName),format=logFormat)
            return parsed_resp
        else:
            if "name: already taken, must be case insensitive unique" in json.loads(scope_resp.content)["error"]:
                  parentScopeName=""
                  logger.autolog(level=1,message='Scope already exists...',format=logFormat)
                  scope_resp = rc.get('/openapi/v1/app_scopes/')
                  for scope in json.loads(scope_resp.content):
                      if scope["id"]==parentScopeId:
                          parentScopeName=scope["name"]
                  for scope in json.loads(scope_resp.content):
                      if scope["name"]==parentScopeName+":"+scopeName:
                          logger.autolog(level=1,message='Found existing ssope with name ' + parentScopeName + ":" + scopeName ,format=logFormat)
                          logger.autolog(level=1,message=str(scope) ,format=logFormat)
                          return scope
                  logger.autolog(level=1,message='Unable to find scope when searching for it...',format=logFormat)
                  return False
            logger.autolog(level=1,message='Error while creating scope: ' + str(scope_resp.content),format=logFormat)
            return False
    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while creating scopes: ' + str(scopeName)+ ': '  + str(e),format=logFormat)
        return False


def deleteScope(rc,scopeId,parentScopeId,logFormat=''):

    try:
        resp = rc.delete('/openapi/v1/app_scopes/{}'.format(scopeId))
        if resp.status_code == 200:
             logger.autolog(level=1,message="Removed Application Scope with ID {}".format(scopeId),format=logFormat)
        else:
             logger.autolog(level=1,message="Failed with Resp status code: " + str(resp.status_code),format=logFormat)

        #Commit dirty scopes
        commit_json = {'root_app_scope_id': parentScopeId}
        commit = rc.post('/openapi/v1/app_scopes/commit_dirty',json_body=json.dumps(commit_json))
        logger.autolog(level=1,message="Committed dirty scopes",format=logFormat)
        return True
    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while deleting scope: ' + str(scopeId)+ ': '  + str(e),format=logFormat)
        return False



def deleteScopeRecursive(rc,scopeId,parentScopeId,logFormat=''):

    try:
        resp = rc.get('/openapi/v1/app_scopes/'+scopeId)
        time.sleep(5)
        logger.autolog(level=1,message="Starting deleteScopeRecursive for scope ID " + str(scopeId),format=logFormat)        

        if resp.status_code == 200:
             for childScopeId in json.loads(resp.content)["child_app_scope_ids"]:
                 logger.autolog(level=1,message="Got childScopeId " + childScopeId  + " for parent scope ID " + str(scopeId),format=logFormat)
                 deleteScopeRecursive(rc,childScopeId ,scopeId,logFormat=logFormat)
             
             deleteAdmsInScope(rc,scopeId,logFormat=logFormat)

             invFilters=getInventoryFiltersByScopeId(rc,scopeId,logFormat=logFormat)
             time.sleep(5)
             for invFilter in invFilters:
                 deleteInventoryFilter(rc,invFilter["id"],deleteSoftwareSensorIntentExec=True,logFormat=logFormat)
                 
            
             if deleteScope(rc,scopeId,parentScopeId,logFormat=logFormat):
                 logger.autolog(level=1,message="Removed Scope  recursively with ID {}".format(scopeId),format=logFormat)
             else:
                 logger.autolog(level=1,message="Unable to remove scope... ID: " + str(scopeId),format=logFormat)
                 return False

        else:
             logger.autolog(level=1,message="Failed to remove scope with ID : " + str(scopeId) + " with status: " + str(resp.status_code),format=logFormat)

    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while deleting scope: ' + str(scopeId)+ ': '  + str(e),format=logFormat)
        return False



def createRole(rc,roleName,parentScopeId,abilities=[],logFormat=''):

    try:
        role_req_payload = {"name": roleName,"description": "Created by dCloud automation","app_scope_id": parentScopeId}
        role_resp = rc.post('/roles', json_body=json.dumps(role_req_payload))
        if role_resp.status_code == 200:
            parsed_resp = json.loads(role_resp.content)
            roleId=str(json.loads(role_resp.content)["id"])
            role=json.loads(role_resp.content)
            logger.autolog(level=1,message="Role created: " + str(roleName),format=logFormat)
        else:
            if "role names must be unique" in json.loads(role_resp.content)["error"]:
                logger.autolog(level=1,message="Role already exists...",format=logFormat)
                role_resp = rc.get('/roles')
                for role in json.loads(role_resp.content):
                    if role["name"]==roleName:
                        roleId=role["id"]
                        break
            else:
                logger.autolog(level=1,message="Unable to create Role : " + str(role_resp.content),format=logFormat)
                return False

        for ability in abilities:
            role_update_payload={"app_scope_id": ability["scopeId"] ,"ability": ability["ability"]}
            role_update_resp = rc.post('/roles/'+str(roleId)+'/capabilities', json_body=json.dumps(role_update_payload))
            if role_update_resp.status_code == 200:
                parsed_resp = json.loads(role_update_resp.content)
                logger.autolog(level=1,message="Role added abilities: " + str(ability),format=logFormat)
            else:
                if "duplicate capability" in json.loads(role_update_resp.content)["error"]:
                    logger.autolog(level=1,message="Abiity already exists..",format=logFormat)
                else:
                    logger.autolog(level=1,message="Unable to add role ability: " + str(role_update_resp.content),format=logFormat)
                    return False

        return role

    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while creating role: ' + str(roleName)+ ': '  + str(e),format=logFormat)
        return False


def getRoleByName(rc,roleName,logFormat=''):
    try:
        resp = rc.get('/openapi/v1/roles/')
        if resp.status_code == 200:
            for role in json.loads(resp.content):
                if role["name"]==roleName:
                    return role
            logger.autolog(level=1,message="No role matched name: " + str(roleName),format=logFormat)
            return False
        else:
            logger.autolog(level=1,message="Failed with Resp status code:" + str(resp.status_code),format=logFormat)
    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while getting  role: ' + str(roleName)+ ': '  + str(e),format=logFormat)
        return False


def deleteRole(rc,roleId,logFormat=''):
    try:
        resp = rc.delete('/openapi/v1/roles/{}'.format(roleId))
        if resp.status_code == 200:
            logger.autolog(level=1,message="Removed Role  with ID {}".format(roleId),format=logFormat)
            
        else:
            logger.autolog(level=1,message="Failed with Resp status code:" + str(resp.status_code),format=logFormat)
    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while deleting role: ' + str(roleId)+ ': '  + str(e),format=logFormat)
        return False


def getSensorConfigProfile(rc,profileName,logFormat=''):
    try:
        profiles_resp=rc.get('/openapi/v1/inventory_config/profiles')
        if profiles_resp.status_code == 200:
            logger.autolog(level=1,message="Got a list of profiles...",format=logFormat)
            for profile in json.loads(profiles_resp.content):
                if profile["name"]==profileName:
                    logger.autolog(level=1,message="Found a profile with name: " + profileName ,format=logFormat)
                    return profile
            logger.autolog(level=1,message="Unable to find profile with name " + profileName ,format=logFormat)
            return False
        else:
            logger.autolog(level=1,message="Failed with Resp status code:" + str(profiles_resp.status_code),format=logFormat)
    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while getting list of agent config profiles : ' + str(profileName)+ ': '  + str(e),format=logFormat)
        return False


def createInventoryFilter(rc,scopeId,inventoryFilterName,queryValue,logFormat=''):
    # Create an Inventory Filter
    try:
        inventory_req_payload = {"app_scope_id": scopeId,"name": inventoryFilterName ,"query": {"field": "host_name","type": "contains","value": queryValue}}
        response = rc.post('/filters/inventories',json_body=json.dumps(inventory_req_payload))


        # Parse the response only if 200OK has been received
        if response.status_code == 200:
            logger.autolog(level=1,message='Inventory Filter created:',format=logFormat)
            return json.loads(response.content)
        else:
            if 'name: must be unique within each ownership scope' in json.loads(response.content)["error"]:
                logger.autolog(level=1,message='Inventory filter already exists ',format=logFormat)
                invs=rc.get('/filters/inventories')
                for inv in json.loads(invs.content):
                    if inv["name"]==inventoryFilterName:
                        return inv
                logger.autolog(level=1,message='Inventory Filter indicated as already existing but unable to find it' + str(inventoryFilterName),format=logFormat) 
                return False
            logger.autolog(level=1,message='Something went worng while creating inventory filter : ' + str(json.loads(response.content)) ,format=logFormat)
            return False

    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while creating inventory filter : ' + str(e),format=logFormat)
        return False


def deleteInventoryFilter(rc,inventoryFilterId,deleteSoftwareSensorIntentExec=False,logFormat=''):

    # Delete an Inventory Filter
    try:

        if deleteSoftwareSensorIntentExec:
            # Get software sensor intent ID that matches the inventory filter 
            soft_intent_resp = rc.get('/openapi/v1/inventory_config/intents')
            if soft_intent_resp.status_code == 200:
                parsed_resp = json.loads(soft_intent_resp.content)
                for d in parsed_resp:
                    if d["inventory_filter_id"] == inventoryFilterId:
                        logger.autolog(level=1,message='Got software sensor intent with ID ' + str(inventoryFilterId) ,format=logFormat)
                        deleteSoftwareSensorIntent(rc,d["id"],logFormat=logFormat)
            else:
                logger.autolog(level=1,message='Unable to get list of software sensor intent : ' + str(json.loads(soft_intent_resp.content)) ,format=logFormat)


        response=rc.delete('/openapi/v1/filters/inventories/' + inventoryFilterId)
        # Parse the response only if 200OK has been received
        if response.status_code == 200:
            return response
        else:
            logger.autolog(level=1,message='Something went worng while deleting inventory filter : ' + str(json.loads(response.content)) ,format=logFormat)
            return False

    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while deleting inventory filter : ' + str(e),format=logFormat)
        return False



def getInventoryFiltersByScopeId(rc,scopeId,logFormat=''):
    # Get an Inventory Filters
    try:

        inventoryFilters=[]
        response= rc.get('/openapi/v1/filters/inventories/')

        # Parse the response only if 200OK has been received
        if response.status_code == 200:
             for invFilter in json.loads(response.content):
                 if invFilter["app_scope_id"]==scopeId:
                     inventoryFilters.append(invFilter)
             return inventoryFilters

        else:
            logger.autolog(level=1,message='Something went worng while getting inventory filter : ' + str(json.loads(response.content)) ,format=logFormat)
            return False

    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while getting inventory filter : ' + str(e),format=logFormat)
        return False




def createSoftwareSensorIntent(rc,sensorConfigProfileId,inventoryFilterId,logFormat=''):

    # Create a software sensor intent
    try:
        soft_sens_intent_req_payload = {"inventory_config_profile_id": sensorConfigProfileId,"inventory_filter_id": inventoryFilterId ,"version": 1}
        response = rc.post('/openapi/v1/inventory_config/intents', json_body=json.dumps(soft_sens_intent_req_payload))

        if response.status_code == 200:
            parsed_resp = json.loads(response.content)
        else:
            if "inventory_filter_id: Filter and config profile combination must be unique" in json.loads(response.content)["error"]:
                logger.autolog(level=1,message='Sensor Intent already exists...' ,format=logFormat)
            else:    
                logger.autolog(level=1,message='Something went wrong while POST for creating sensor intent : ' + str(response.content),format=logFormat)
                return False

        intentId=''
        # Get software sensor intent ID. As we don't get a response from the intent creation, we look for the intent ID that matches the inventory filter we've just created
        soft_intent_resp = rc.get('/openapi/v1/inventory_config/intents')
        if soft_intent_resp.status_code == 200:
             parsed_resp = json.loads(soft_intent_resp.content)
             for d in parsed_resp:
                 if d["inventory_filter_id"] == inventoryFilterId:
                     intentId = d["id"]
                     break

        # Put the intent on top of the list
        # Fetch intent list
        ilist_resp = rc.get('/inventory_config/orders')
        order_result_json = json.loads(ilist_resp.content)

        # Create the payload
        order_result_json['intent_ids'].insert(0,"{}".format(intentId))

        # Post the new list to the server
        reorder_resp = rc.post('/inventory_config/orders',json_body=json.dumps(order_result_json))
  
        return d

    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while creating sensor intent : ' + str(e),format=logFormat)
        return False


def deleteSoftwareSensorIntent(rc,softwareSensorIntentId,logFormat=''):

    # Remove Agent Config Intent
    try:
        logger.autolog(level=1,message='Deleting Software Sensor Intent id : ' + str(softwareSensorIntentId),format=logFormat)
        resp = rc.delete('/openapi/v1/inventory_config/intents/'+softwareSensorIntentId)
        if resp.status_code == 200:
            logger.autolog(level=1,message='Removed Agent Config Intent with ID : ' + str(softwareSensorIntentId),format=logFormat)
            return True
        else:
            logger.autolog(level=1,message='Removing Agent Config Intent failed with Resp status code:'+ str(resp.status_code),format=logFormat)
            return False
    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while deleting software sensor intent : ' + str(e),format=logFormat)
        return False




def createAdm(rc,scopeId,appName, enforcementEnabled=True,primary=True,absPolicies=[], logFormat='' ):
    #Create ADM
    try:
        #Create Request Payload and send request
        apps_payload = {"app_scope_id": scopeId,"name": appName+"_workspace","description": "Created by dCloud Automation","primary": primary,"catch_all_action": "ALLOW","absolute_policies":absPolicies}
        appws_resp = rc.post('/openapi/v1/applications',json_body=json.dumps(apps_payload))


        #Enable Policy Enforcement in created App
        if enforcementEnabled:
            appId= json.loads(appws_resp.content)["id"]
            apps_enforcement_resp = rc.post(r'/openapi/v1/applications/'+appId+r'/enable_enforce')


        # Parse the response only if 200OK has been received
        if appws_resp.status_code == 200:
            parsed_resp = json.loads(appws_resp.content)
            logger.autolog(level=1,message='ADM created with ID : ' + str(parsed_resp["id"]),format=logFormat)
            return parsed_resp
        else:
            logger.autolog(level=1,message='Something went wrong while creating application  : ' + str(parsed_resp),format=logFormat)
            return False

    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while creating application  : ' + str(e),format=logFormat)
        return False

def deleteAdm(rc,appId,logFormat=''):

    try:
        app= rc.get('/openapi/v1/filters/applications/'+str(appId)+r'/details')
        if app.status_code == 200:
            app = resp.json(app.content)
        else:
            logger.autolog(level=1,message="[ERROR] reading application workspaces.",format=logFormat)
            logger.autolog(level=1,message=str(app.text),format=logFormat)
            return False

        # first we turn off enforcement
        if app["enforcement_enabled"]:
            r = rc.post('/openapi/v1/applications/' + app_id + '/disable_enforce')
            if r.status_code == 200:
                logger.autolog(level=1,message="[CHANGED] app {} ({}) to not enforcing.".format(app_id, appName),format=logFormat)
        # make the application secondary if it is primary
        if app["primary"]:
            req_payload = {"primary": "false"}
            r = rc.put('/openapi/v1/applications/' + app_id, json_body=json.dumps(req_payload))
            if r.status_code == 200:
                logger.autolog(level=1,message="[CHANGED] app {} ({}) to secondary.".format(app_id, appName),format=logFormat)
        # now delete the app
        r = rc.delete('/openapi/v1/applications/' + app_id)
        if r.status_code == 200:
           logger.autolog(level=1,message="[REMOVED] app {} ({}) successfully.".format(app_id, appName),format=logFormat)

        return True
 
    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while deleting application  : ' + str(e),format=logFormat)
        return False



def deleteAdmsInScope(rc,scopeId,logFormat=''):

    logger.autolog(level=1,message="  Deleting ADM in Scope ID : " + str(scopeId),format=logFormat)
    resp = rc.get('/openapi/v1/applications/')
    if resp.status_code == 200:
        resp_data = json.loads(resp.content)
    else:
        logger.autolog(level=1,message="[ERROR] reading application workspaces to determine which ones should be deleted.",format=logFormat)
        logger.autolog(level=1,message=str(resp.text),format=logFormat)
        resp_data = {}
        return False

    for app in resp_data:
        appName = app["name"]
        if  app["app_scope_id"] == scopeId:
            app_id = app["id"]
            # first we turn off enforcement
            if app["enforcement_enabled"]:
                r = rc.post('/openapi/v1/applications/' + app_id + '/disable_enforce')
                if r.status_code == 200:
                    logger.autolog(level=1,message="[CHANGED] app {} ({}) to not enforcing.".format(app_id, appName),format=logFormat)
            # make the application secondary if it is primary
            if app["primary"]:
                req_payload = {"primary": "false"}
                r = rc.put('/openapi/v1/applications/' + app_id, json_body=json.dumps(req_payload))
                if r.status_code == 200:
                    logger.autolog(level=1,message="[CHANGED] app {} ({}) to secondary.".format(app_id, appName),format=logFormat)
            # now delete the app
            r = rc.delete('/openapi/v1/applications/' + app_id)
            if r.status_code == 200:
               logger.autolog(level=1,message="[REMOVED] app {} ({}) successfully.".format(app_id, appName),format=logFormat)
    return True


def getAdmByScopeId(rc,scopeId,logFormat=''):
    # Get an ADMs
    try:
        apps=[]
        response= rc.get('/openapi/v1/filters/applications/')
        # Parse the response only if 200OK has been received
        if response.status_code == 200:
            for app in json.loads(response.content):
                if app["app_scope_id"]==scopeId:
                    apps.append(invFilter)
            return apps
        else:
            logger.autolog(level=1,message='Something went worng while getting apps: ' + str(json.loads(response.content)) ,format=logFormat)
            return False
    except Exception as e:
        logger.autolog(level=1,message='Something went wrong while getting apps : ' + str(e),format=logFormat)
        return False






