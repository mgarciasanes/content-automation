import logging
import threading
import urllib2
import glob
import json
import os
import os.path
import requests
import sys
import time
import xml.dom.minidom
import yaml
import platform
from xml.dom import minidom
import datetime


modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import mainAutomation as main
import genFunctions as gen

def createAdUser(sessionFile , otherInputs=[]):
    main.autolog('Starting createADUser')

    creds = gen.getDemoCredentialsByName('/root/hyperflex/hxStartup.cfg','ad')

    session = gen.readSessionDic(r'/home/demouser/hyperflex/hyperflex_new/' + sessionFile)
    #session = gen.readSessionDic(r'/root/hyperflex/testsessions/' + sessionFile)

    #userId= session['session']['owner']
    userId= r'v' + session['session']['vpod'] + 'user1'
    passwd = session['session']['anycpwd']

    scriptPath = r'C:\\Scripts\\hyperflex\\'
    scriptName = r"createAdUser.ps1"
    psArgs=[{'name': 'sessionFile' , 'value': sessionFile},{'name': 'userId' , 'value': userId},{'name': 'password' , 'value': passwd}]

    output= gen.sshExecPsScript(creds, scriptPath, scriptName, psArgs, timeout=60)

    if output == "True":
        output= True
        status = True
        main.autolog('Output Value is True')
    else:
        output= False
        status = False
        main.autolog('Output Value is False')
    return gen.formatOutput(output),status



def createDatastoreWithPermissions(sessionFile , otherInputs=[]):
    import hxcontroller as hx
    import vcenter as vc

    hxCreds = gen.getDemoCredentialsByName('/root/hyperflex/hxStartup.cfg','hx-controller')
    vcCreds = gen.getDemoCredentialsByName('/root/hyperflex/hxCleanup.cfg','vc')

    session = gen.readSessionDic(r'/home/demouser/hyperflex/hyperflex_new/' + sessionFile)
    #session = gen.readSessionDic(r'/root/hyperflex/testsessions/' + sessionFile)
    userId= r'v' + session['session']['vpod'] + 'user1'

    myDs=hx.createDatastore(hxCreds, dsName=userId)
    if not myDs:
        main.autolog('Unable to create DS')
        return gen.formatOutput(False),False

    result=vc.setPermissionsToUserDs(host = vcCreds['host'],user = vcCreds['username'], password = vcCreds['password'], dcName='dCloud-HX-DC', dsName=userId, userName=userId)
    
    return gen.formatOutput(result),result
    

def createUserVm(sessionFile, otherInputs=[]):

    import vcenter as vc
    vcCreds = gen.getDemoCredentialsByName('/root/hyperflex/hxCleanup.cfg','vc')

    session = gen.readSessionDic(r'/home/demouser/hyperflex/hyperflex_new/' + sessionFile)
    #session = gen.readSessionDic(r'/root/hyperflex/testsessions/' + sessionFile)
    userId= r'v' + session['session']['vpod'] + 'user1'

    vc.createUserVm(host = vcCreds['host'],user = vcCreds['username'], password = vcCreds['password'],vmName=userId, dsName=userId, destFolderName=userId)

    return gen.formatOutput(True), True


def createVmFolderWithPermissions(sessionFile, otherInputs=[]):
    import vcenter as vc
    creds = gen.getDemoCredentialsByName('/root/hyperflex/hxStartup.cfg','vc')

    session = gen.readSessionDic(r'/home/demouser/hyperflex/hyperflex_new/' + sessionFile)
    #session = gen.readSessionDic(r'/root/hyperflex/testsessions/' + sessionFile)
    folderName= r'v' + session['session']['vpod'] + 'user1'
    userName= 'DCV\\'+folderName

    #result= vc.destroyVmFolder(host = creds['host'],user = creds['username'], password = creds['password'], folderName=folderName, dcName='dCloud-HX-DC')

    result= vc.createVmFolderWithPermissions(host = creds['host'],user = creds['username'], password = creds['password'],  folderName=folderName,userName = userName, dcName='dCloud-HX-DC')
    return gen.formatOutput(result), True


def removeAdUser(sessionFile , otherInputs=[]):
    main.autolog('Starting removeADUser')

    main.autolog('Checking Input: isSessionReadyToDelete ')
    if(gen.getInputByName(otherInputs, 'isSessionReadyToDelete')==False):
        main.autolog('Session is not ready for deletion. Aborting...')
        return gen.formatOutput(None), True
    main.autolog('Session is ready for deletion. Executing...')
        
    creds = gen.getDemoCredentialsByName('/root/hyperflex/hxStartup.cfg','ad')

    session = gen.readSessionDic(r'/home/demouser/hyperflex/hyperflex_active/' + sessionFile)
    #session = gen.readSessionDic(r'/root/hyperflex/testsessions/' + sessionFile)
    userId= r'v' + session['session']['vpod'] + 'user1'
    passwd = session['session']['anycpwd']

    scriptPath = r'C:\\Scripts\\hyperflex\\'
    scriptName = r"removeAdUser.ps1"
    psArgs=[{'name': 'sessionFile' , 'value': sessionFile},{'name': 'userId' , 'value': userId}]

    output= gen.sshExecPsScript(creds, scriptPath, scriptName, psArgs, timeout=60)

    if output == "True":
        output= True
        status = True
        main.autolog('Output Value is True')
    else:
        output= False
        status = False
        main.autolog('Output Value is False')

    return gen.formatOutput(output),status


def deleteUserDatastore(sessionFile, otherInputs=[]):
    main.autolog('Starting deleteUserDatastore')

    main.autolog('Checking Input: isSessionReadyToDelete ')
    if(gen.getInputByName(otherInputs, 'isSessionReadyToDelete')==False):
        main.autolog('Session is not ready for deletion. Aborting...')
        return gen.formatOutput(None), True
    main.autolog('Session is ready for deletion. Executing...')
        

    import hxcontroller as hx
    hxCreds = gen.getDemoCredentialsByName('/root/hyperflex/hxCleanup.cfg','hx-controller')

    session = gen.readSessionDic(r'/home/demouser/hyperflex/hyperflex_active/' + sessionFile)
    #session = gen.readSessionDic(r'/root/hyperflex/testsessions/' + sessionFile)
    userId= r'v' + session['session']['vpod'] + 'user1'

    result = hx.deleteUserDatastore(hxCreds, userId)

    return gen.formatOutput(result),result


def cleanupVmSandbox(sessionFile, otherInputs=[]):
    import vcenter as vc
    creds = gen.getDemoCredentialsByName('/root/hyperflex/hxStartup.cfg','vc')

    result= vc.cleanupVmSandbox(host = creds['host'],user = creds['username'], password = creds['password'], dcName='dCloud-HX-DC')
    return gen.formatOutput(result), True




def cleanupDatastores(sessionFile, otherInputs=[]):
    import hxcontroller as hx
    import vcenter as vc

    hxCreds = gen.getDemoCredentialsByName('/root/hyperflex/hxCleanup.cfg','hx-controller')
    vcCreds = gen.getDemoCredentialsByName('/root/hyperflex/hxCleanup.cfg','vc')

    dsToDelete= hx.getDatastoresToBeDeleted(hxCreds)

    emptyDatastores = vc.deleteVmsInDatastores(datastores=dsToDelete, host = vcCreds['host'],user = vcCreds['username'], password = vcCreds['password'], dcName='dCloud-HX-DC')
    
    hx.deleteDatastores(hxCreds, datastores=emptyDatastores)

    return gen.formatOutput(True), True



def deleteVmFolderWithPermissions(sessionFile, otherInputs=[]):
    main.autolog('Starting deleteVmFolderWithPermissions')

    main.autolog('Checking Input: isSessionReadyToDelete ')
    if(gen.getInputByName(otherInputs, 'isSessionReadyToDelete')==False):
        main.autolog('Session is not ready for deletion. Aborting...')
        return gen.formatOutput(None), True

    main.autolog('Session is ready for deletion. Executing...')

    import vcenter as vc
    creds = gen.getDemoCredentialsByName('/root/hyperflex/hxStartup.cfg','vc')

    session = gen.readSessionDic(r'/home/demouser/hyperflex/hyperflex_active/' + sessionFile)
    #session = gen.readSessionDic(r'/root/hyperflex/testsessions/' + sessionFile)

    folderName= r'v' + session['session']['vpod'] + 'user1'
    result= vc.deleteVmFolderWithPermissions(host = creds['host'],user = creds['username'], password = creds['password'], folderName=folderName, dcName='dCloud-HX-DC')

    return gen.formatOutput(result), result


def bootStorm(sessionFile, otherInputs=[]):
    import vcenter as vc
    creds = gen.getDemoCredentialsByName('/root/hyperflex/hxStartup.cfg','vc')

    folderName= r'Boot-Storm'
    result= vc.toggleVmPowerStateInFolder(host = creds['host'],user = creds['username'], password = creds['password'], folderName=folderName, dcName='dCloud-HX-DC')
    return gen.formatOutput(result), True





