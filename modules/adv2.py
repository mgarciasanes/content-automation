
####################################
#  Module Details
#
# 	Name:
#	Description:
#	Author:
#	Version:
#	Date:
#
#
####################################



####################################
#  Imports
####################################
import sys
modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger

import logging
import os
import os.path
import requests
import time
import xml.dom.minidom
import yaml
import platform
from xml.dom import minidom
import datetime



####################################
#  Global Variables
####################################
credsFile=gen.credsFile


####################################
#  Front-End  Functions
####################################


def createAdUser(sessionId, target, otherInputs=[], logFormat=''):
    global scriptPath
    if (type(otherInputs) is dict):
        try:
            username=gen.sanitizePassword(otherInputs["username"])
            password=gen.sanitizePassword(otherInputs["password"])
            groupName=otherInputs['groupName']
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'

        try:
            usernameMaxLen=int(gen.sanitizePassword(otherInputs["usernameMaxLen"]))
        except Exception as e:
            logger.autolog(level=1,message='No username Max Len found, using empty default 16 ',format=logFormat)
            usernameMaxLen=16

        try:
            prefix=gen.sanitizePassword(otherInputs["prefix"])
        except Exception as e:
            logger.autolog(level=1,message='No prefix found, using empty string...',format=logFormat)
            prefix=''

        try:
            prefixMaxLen=int(gen.sanitizePassword(otherInputs["prefixMaxLen"]))
        except Exception as e:
            logger.autolog(level=1,message='No prefix Max len, using empty default 8 ',format=logFormat)
            prefixMaxLen=8

        try:
            sufix=gen.sanitizePassword(otherInputs["sufix"])
        except Exception as e:
            logger.autolog(level=1,message='No sufix found, using empty string...',format=logFormat)
            sufix=''

        try:
            sufixMaxLen=int(gen.sanitizePassword(otherInputs["sufixMaxLen"]))
        except Exception as e:
            logger.autolog(level=1,message='No sufix Max Len found, using default 8 ',format=logFormat)
            sufixMaxLen=8
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    if username=="USER-ID":
        #trailingString=gen.generateRandomString(4)
        username=username+'-'+str(sessionId)

    if username=="":
        #trailingString=gen.generateRandomString(6)
        username="noUser-"+str(sessionId)

    #add prefix and sufix, if any...
    username=str(prefix)[0:prefixMaxLen]+str(username)[0:usernameMaxLen]+str(sufix)[0:sufixMaxLen]

    if password=="username":
        password=username

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    scriptPath = r"C:\\scripts\\ad\\"
    scriptName = r"createAdUser.ps1"
    psArgs={'userId': str(username) , 'password': str(password), 'sessionId': '"'+str(sessionId)+'"','groupName': '"'+str(groupName)+'"'}
    stdOut, stdErr = gen.sshExecPsScriptV2(creds, scriptPath, scriptName, psArgs, timeout=60)

    if stdErr=='[]' or "Loading Active Directory module for Windows PowerShell with default drive" in stdErr:
        return {'username':str(username),'password':str(password)},'Success'
    else:
        return {'result':stdErr}, 'Failed: ' + str(stdErr)



#This function is similiar to the one above but can handle usernames longer thatn 20 characters...
def createAdUserV2(sessionId, target, otherInputs=[], logFormat=''):
    global scriptPath
    if (type(otherInputs) is dict):
        try:
            username=gen.sanitizePassword(otherInputs["username"])
            password=gen.sanitizePassword(otherInputs["password"])
            groupName=otherInputs['groupName']
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'

        try:
            usernameMaxLen=int(gen.sanitizePassword(otherInputs["usernameMaxLen"]))
        except Exception as e:
            logger.autolog(level=1,message='No username Max Len found, using empty default 16 ',format=logFormat)
            usernameMaxLen=16

        try:
            prefix=gen.sanitizePassword(otherInputs["prefix"])
        except Exception as e:
            logger.autolog(level=1,message='No prefix found, using empty string...',format=logFormat)
            prefix=''

        try:
            prefixMaxLen=int(gen.sanitizePassword(otherInputs["prefixMaxLen"]))
        except Exception as e:
            logger.autolog(level=1,message='No prefix Max len, using empty default 8 ',format=logFormat)
            prefixMaxLen=8

        try:
            sufix=gen.sanitizePassword(otherInputs["sufix"])
        except Exception as e:
            logger.autolog(level=1,message='No sufix found, using empty string...',format=logFormat)
            sufix=''

        try:
            sufixMaxLen=int(gen.sanitizePassword(otherInputs["sufixMaxLen"]))
        except Exception as e:
            logger.autolog(level=1,message='No sufix Max Len found, using default 8 ',format=logFormat)
            sufixMaxLen=8
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    if username=="USER-ID":
        #trailingString=gen.generateRandomString(4)
        username=username+'-'+str(sessionId)

    if username=="":
        #trailingString=gen.generateRandomString(6)
        username="noUser-"+str(sessionId)

    #add prefix and sufix, if any...
    username=str(prefix)[0:prefixMaxLen]+str(username)[0:usernameMaxLen]+str(sufix)[0:sufixMaxLen]

    if password=="username":
        password=username

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    scriptPath = r"C:\\scripts\\ad\\"
    scriptName = r"createAdUserV2.ps1"
    psArgs={'userId': str(username) , 'password': str(password), 'sessionId': '"'+str(sessionId)+'"','groupName': '"'+str(groupName)+'"'}
    stdOut, stdErr = gen.sshExecPsScriptV2(creds, scriptPath, scriptName, psArgs, timeout=60)

    if stdErr=='[]' or "Loading Active Directory module for Windows PowerShell with default drive" in stdErr:
        return {'username':str(username),'password':str(password)},'Success'
    else:
        return {'result':stdErr}, 'Failed: ' + str(stdErr)


def changeUserPassword(sessionId, target, otherInputs=[], logFormat=''):
    global scriptPath
    if (type(otherInputs) is dict):
        try:
            username=otherInputs["username"]
            password=otherInputs["password"]
            groupName=otherInputs['groupName']
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'

        try:
            usernameMaxLen=int(gen.sanitizePassword(otherInputs["usernameMaxLen"]))
        except Exception as e:
            logger.autolog(level=1,message='No username Max Len found, using empty default 16 ',format=logFormat)
            usernameMaxLen=16

        try:
            prefix=gen.sanitizePassword(otherInputs["prefix"])
        except Exception as e:
            logger.autolog(level=1,message='No prefix found, using empty string...',format=logFormat)
            prefix=''

        try:
            prefixMaxLen=int(gen.sanitizePassword(otherInputs["prefixMaxLen"]))
        except Exception as e:
            logger.autolog(level=1,message='No prefix Max len, using empty default 8 ',format=logFormat)
            prefixMaxLen=8

        try:
            sufix=gen.sanitizePassword(otherInputs["sufix"])
        except Exception as e:
            logger.autolog(level=1,message='No sufix found, using empty string...',format=logFormat)
            sufix=''

        try:
            sufixMaxLen=int(gen.sanitizePassword(otherInputs["sufixMaxLen"]))
        except Exception as e:
            logger.autolog(level=1,message='No sufix Max Len found, using default 8 ',format=logFormat)
            sufixMaxLen=8
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    if username=="USER-ID":
        #trailingString=gen.generateRandomString(4)
        username=username+'-'+str(sessionId)

    if username=="":
        #trailingString=gen.generateRandomString(6)
        username="noUser-"+str(sessionId)

    #add prefix and sufix, if any...
    username=str(prefix)[0:prefixMaxLen]+str(username)[0:usernameMaxLen]+str(sufix)[0:sufixMaxLen]

    if password=="username":
        password=username

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    scriptPath = r"C:\\scripts\\ad\\"
    scriptName = r"changeUserPassword.ps1"
    psArgs={'userId': str(username) , 'password': str(password), 'sessionId': '"'+str(sessionId)+'"','groupName': '"'+str(groupName)+'"'}
    stdOut, stdErr = gen.sshExecPsScriptV2(creds, scriptPath, scriptName, psArgs, timeout=60)

    if stdErr=='[]' or "Loading Active Directory module for Windows PowerShell with default drive" in stdErr:
        return {'username':str(username),'password':str(password)},'Success'
    else:
        return {'result':stdErr}, 'Failed: ' + str(stdErr)



def createAdUserWithRandomPassword(sessionId, target, otherInputs=[], logFormat=''):
    global scriptPath
    if (type(otherInputs) is dict):

        try:
            username=gen.sanitizePassword(otherInputs["username"])
            passwordLength=otherInputs["passwordLength"]
            groupName=otherInputs['groupName']
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'

        try:
            prefix=gen.sanitizePassword(otherInputs["prefix"])
        except Exception as e:
            logger.autolog(level=1,message='No prefix found, using empty string...',format=logFormat)
            prefix=''

        try:
            sufix=gen.sanitizePassword(otherInputs["sufix"])
        except Exception as e:
            logger.autolog(level=1,message='No sufix found, using empty string...',format=logFormat)
            sufix=''


    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'


    if username=="USER-ID":
        #trailingString=gen.generateRandomString(4)
        username=username+'-'+str(sessionId)

    if username=="":
        #trailingString=gen.generateRandomString(6)
        username="noUser-"+str(sessionId)

    password=gen.generateRandomString(int(passwordLength))


    #add prefix and sufix, if any...
    username=prefix+username+sufix

    scriptPath = r"C:\\scripts\\ad\\"
    scriptName = r"createAdUser.ps1"
    psArgs={'userId': str(username) , 'password': str(password), 'sessionId': '"'+str(sessionId)+'"','groupName': '"'+str(groupName)+'"'}
    stdOut, stdErr = gen.sshExecPsScriptV2(creds, scriptPath, scriptName, psArgs, timeout=60)

    if stdErr=='[]' or "Loading Active Directory module for Windows PowerShell with default drive" in stdErr:
        return {'username':str(username),'password':str(password)},'Success'
    else:
        return {'result':stdErr}, 'Failed: ' + str(stdErr)






def removeAdUser(sessionId, target,  otherInputs=[], logFormat=''):
    global scriptPath
    logger.autolog(level=1,message='Starting removeADUser',format=logFormat)

    if (type(otherInputs) is dict):
        try:
            username=gen.sanitizePassword(otherInputs["username"])
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'

        try:
            prefix=gen.sanitizePassword(otherInputs["prefix"])
        except Exception as e:
            logger.autolog(level=1,message='No prefix found, using empty string...',format=logFormat)
            prefix=''

        try:
            sufix=gen.sanitizePassword(otherInputs["sufix"])
        except Exception as e:
            logger.autolog(level=1,message='No sufix found, using empty string...',format=logFormat)
            sufix=''

        try:
            usernameMaxLen=int(gen.sanitizePassword(otherInputs["usernameMaxLen"]))
        except Exception as e:
            logger.autolog(level=1,message='No username Max Len found, using empty default 16 ',format=logFormat)
            usernameMaxLen=16

        try:
            prefixMaxLen=int(gen.sanitizePassword(otherInputs["prefixMaxLen"]))
        except Exception as e:
            logger.autolog(level=1,message='No prefix Max len, using empty default 8 ',format=logFormat)
            prefixMaxLen=8

        try:
            sufixMaxLen=int(gen.sanitizePassword(otherInputs["sufixMaxLen"]))
        except Exception as e:
            logger.autolog(level=1,message='No sufix Max Len found, using default 8 ',format=logFormat)
            sufixMaxLen=8

    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    if username=="USER-ID":
        #trailingString=gen.generateRandomString(4)
        username=username+'-'+str(sessionId)

    if username=="":
        #trailingString=gen.generateRandomString(6)
        username="noUser-"+str(sessionId)

    #add prefix and sufix, if any...
    username=str(prefix)[0:prefixMaxLen]+str(username)[0:usernameMaxLen]+str(sufix)[0:sufixMaxLen]


    scriptPath = r"C:\\scripts\\ad\\"
    scriptName = r"removeAdUser.ps1"
    psArgs={'userId': str(username) , 'sessionId': '"'+str(sessionId)+'"'}
    stdOut, stdErr = gen.sshExecPsScriptV2(creds, scriptPath, scriptName, psArgs, timeout=60)

    if stdErr=='[]' or "Loading Active Directory module for Windows PowerShell with default drive" in stdErr:
        return {'username':str(username),'result':stdOut},'Success'
    else:
        return {'username':str(username),'result':stdErr}, 'Failed: ' + str(stdErr)


def addUserToGroup(sessionId, target, otherInputs=[], logFormat=''):
    global scriptPath
    if (type(otherInputs) is dict):
        try:
            username=gen.sanitizePassword(otherInputs["username"])
            groupName=otherInputs['groupName']
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    scriptPath = r"C:\\scripts\\ad\\"
    scriptName = r"addUserToGroup.ps1"
    psArgs={'userId': str(username) ,'sessionId': '"'+str(sessionId)+'"','groupName': '"'+str(groupName)+'"'}
    stdOut, stdErr = gen.sshExecPsScriptV2(creds, scriptPath, scriptName, psArgs, timeout=60)

    if stdErr=='[]' or "Loading Active Directory module for Windows PowerShell with default drive" in stdErr:
        return {'username':str(username)},'Success'
    else:
        return {'result':stdErr}, 'Failed: ' + str(stdErr)


def pureCleanupPS1(sessionId, target, otherInputs=[], logFormat=''):
    global scriptPath
    if (type(otherInputs) is dict):
        try:
            pureid=otherInputs["pureid"]
            hgrp=otherInputs['hgrp']
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    scriptPath = r"C:\\scripts\\pure\\"
    scriptName = r"pure_demo_cleanup.ps1"
    psArgs={'pureid': str(pureid) ,'sessionId': '"'+str(sessionId)+'"','hgrp': '"'+str(hgrp)+'"'}
    stdOut, stdErr = gen.sshExecPsScriptV2(creds, scriptPath, scriptName, psArgs, timeout=120)

    if stdErr=='[]' :#or "Loading Active Directory module for Windows PowerShell with default drive" in stdErr:
        return {},'Success'
    else:
        return {'result':stdErr}, 'Failed: ' + str(stdErr)




def createUserCitrixCloud(sessionId, target, otherInputs=[], logFormat=''):
    global scriptPath
    if (type(otherInputs) is dict):
        try:
            email=otherInputs["email"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'

    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'


    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    scriptPath = r"C:\\scripts\\citrix\\"
    scriptName = r"cloud_user_create.ps1"
    psArgs={'email': '"'+str(email)+'"', 'sessionId': '"'+str(sessionId)+'"'}
    stdOut, stdErr = gen.sshExecPsScriptV2(creds, scriptPath, scriptName, psArgs, timeout=60)

    if stdErr=='[]' or 'Error' not in str(stdErr):
        return {'email':str(email),'password':''},'Success'
    else:
        return {'result':stdErr}, 'Failed: ' + str(stdErr)



def deleteUserCitrixCloud(sessionId, target, otherInputs=[], logFormat=''):
    global scriptPath
    if (type(otherInputs) is dict):
        try:
            email=otherInputs["email"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'

    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'


    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    scriptPath = r"C:\\scripts\\citrix\\"
    scriptName = r"cloud_user_delete.ps1"
    psArgs={'email': '"'+str(email)+'"', 'sessionId': '"'+str(sessionId)+'"'}
    stdOut, stdErr = gen.sshExecPsScriptV2(creds, scriptPath, scriptName, psArgs, timeout=60)

    if stdErr=='[]' or 'Error' not in str(stdErr):
        return {'email':str(email),'password':''},'Success'
    else:
        return {'result':stdErr}, 'Failed: ' + str(stdErr)



####################################
#  Generic  Functions
####################################
