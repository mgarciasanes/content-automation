import os
import time
import logging
import inspect
import sys


modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import logger
import genFunctions as gen

#os.system("wget http://pexpect.sourceforge.net/pexpect-2.3.tar.gz")
#os.system("tar xzf pexpect-2.3.tar.gz")
#os.chdir("/root/pexpect-2.3")
#os.system("python ./setup.py install")



import pexpect
logging.getLogger('pexpect').setLevel(logging.ERROR)


############################################
#  Global Variables
############################################
pexpectRecipesPath=r'/root/scripts/otherFiles/pexpect/'


############################################
#  Front-end Functions
############################################

def executeFromRecipe(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            recipeName=otherInputs["recipeName"]
            recipePath=otherInputs["recipePath"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    #creds = gen.getDemoCredentialsByName(credsFile,target)
    #if not creds:
    #    return {},'Failed: Unable to get credentials for target'

    wholeStatus,errorReport,hostCapturedOutput=pexpectExecuteYaml(pexpectRecipesPath+recipePath+recipeName)

    if wholeStatus:
        return {},'Success'
    else:
        return {},'Failed:'+str(errorReport)



def executeFromRecipeWithSessionDetails(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            recipeName=otherInputs["recipeName"]
            recipePath=otherInputs["recipePath"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    wholeStatus,errorReport,hostCapturedOutput=pexpectExecuteYaml(pexpectRecipesPath+recipePath+recipeName, sessionVariables=otherInputs['sessionDetails'])

    if wholeStatus:
        return {},'Success'
    else:
        return {},'Failed:'+str(errorReport)




#################################################################################



def isArgEmpty(frame):
    logger.autolog(level=1,message='Checking arguments for function ' + inspect.getframeinfo(frame)[2])
    args, _,_, values = inspect.getargvalues(frame)
    empty=False
    for arg in args:
        if values[arg]=='':
            empty=True
            logger.autolog(level=1,message='Argument ' + arg + ' is empty')
    return empty

def pexpectLogin(ipAddress='', username='', password='', prompt='', addStartToPrompt=True):

    if(isArgEmpty(inspect.currentframe())):
        logger.autolog(level=1,message='At least one argument is empty for pexpectLogin. Aborting pexpectLogin')
        return False

    logger.autolog(level=1,message='About to start SSH session to ' + 'ssh ' + username + '@' + ipAddress )
    try:
        child=pexpect.spawn('ssh ' + username + '@' + ipAddress)
        i=child.expect(["The authenticity.*","Password:.*","password:.*"])
        if i==0:
            logger.autolog(level=1,message='Got security Banner, will send a YESS' )
            child.sendline('yes')
            child.expect(["password:.*","Password:.*"])
            logger.autolog(level=1,message='Got asked for password, will send it')
            child.sendline(password)
        if i==1:
            logger.autolog(level=1,message='Got asked for Password, will send it')
            child.sendline(password)
        if i==2:
            logger.autolog(level=1,message='Got asked for password, will send it')
            child.sendline(password)


        logger.autolog(level=1,message='Expecting for prompt ' + str(prompt))
        if type(prompt) is str: 
            if addStartToPrompt:
                child.expect(prompt+".*")
            else:
                child.expect(prompt)
        if type(prompt) is list:
            i=child.expect(prompt)
            logger.autolog(level=1,message='Got Prompt ' + str(prompt[i]))

    except Exception as e:
        logger.autolog(level=1,message='Failed to login')
        logger.autolog(level=1,message=e)

        try:
            logger.autolog(level=1,message=str(child.before))
        except Exception as ee:
            logger.autolog(level=1,message=ee)    
        return False

    logger.autolog(level=1,message='Loging worked')
    return child



def pexpectLoginNoKnownHost(ipAddress='', username='', password='', prompt='', addStartToPrompt=True, port='22'):

    if(isArgEmpty(inspect.currentframe())):
        logger.autolog(level=1,message='At least one argument is empty for pexpectLogin. Aborting pexpectLogin')
        return False

    sshCmd='ssh ' + username + '@' + ipAddress +  ' -p ' + str(port) + '  -o "UserKnownHostsFile /dev/null"'
    logger.autolog(level=1,message='About to start SSH session with command ' + sshCmd )
    try:
        child=pexpect.spawn(sshCmd)
        i=child.expect(["The authenticity.*","Password:.*","password:.*"])
        if i==0:
            logger.autolog(level=1,message='Got security Banner, will send a YESS' )
            child.sendline('yes')
            child.expect(["password:.*","Password:.*"])
            logger.autolog(level=1,message='Got asked for password, will send it')
            child.sendline(password)
        if i==1:
            logger.autolog(level=1,message='Got asked for Password, will send it')
            child.sendline(password)
        if i==2:
            logger.autolog(level=1,message='Got asked for password, will send it')
            child.sendline(password)


        logger.autolog(level=1,message='Expecting for prompt ' + str(prompt))
        if type(prompt) is str: 
            if addStartToPrompt:
                child.expect(prompt+".*")
            else:
                child.expect(prompt)
        if type(prompt) is list:
            i=child.expect(prompt)
            logger.autolog(level=1,message='Got Prompt ' + str(prompt[i]))

    except Exception as e:
        logger.autolog(level=1,message='Failed to login')
        logger.autolog(level=1,message=e)

        try:
            logger.autolog(level=1,message=str(child.before))
        except Exception as ee:
            logger.autolog(level=1,message=ee)    
        return False

    logger.autolog(level=1,message='Loging worked')
    return child


def pexpectScp(ipAddress='', username='', password='', prompt='', scope=' ', source='', dest='', localFile=True, timeout=20):

    logging.info('About to start SCP')
    if(isArgEmpty(inspect.currentframe())):
        logging.info('At least one argument is empty for pexpectScp. Aborting pexpectScp')
        return False


    if localFile:
        scpCmd='scp ' + scope + ' ' + username + '@' + ipAddress + ':' + source + ' ' + dest
    else:
        scpCmd='scp ' + scope + ' ' + source + ' ' + username + '@' + ipAddress + ':' + dest

    logging.info('About to start SCP session to ' + ipAddress)
    logging.info('SCP command is: ' + scpCmd)
    try:
        child=pexpect.spawn(scpCmd )
        i=child.expect(["The authenticity.*","Password:.*","password:.*"])
        if i==0:
            logging.info('Got security Banner, will send a YESS' )
            child.sendline('yes')
            child.expect(["password:.*","Password:.*"],timeout=timeout)
            logging.info('Got asked for password, will send it')
            child.sendline(password)
        if i==1:
            logging.info('Got asked for Password, will send it')
            child.sendline(password)
        if i==2:
            logging.info('Got asked for password, will send it')
            child.sendline(password)

        logging.info('Expecting for prompt')
        i=child.expect([pexpect.TIMEOUT,pexpect.EOF,prompt+".*"],timeout=timeout)
        if i==0:
            logging.info('Got Timeout' )
        if i==1:
            logging.info('Got EOF')
        if i==2:
            logging.info('Got prompt')
        logging.info(str(child.before))

    except Exception as e:
        logging.info('Failed while SCP')
        logging.info(e)
        return False
    return True


   

def pexpectCommand(command, child):
    now=str(command['cmd'])
    cases=command['cases']
    next=command['next']
    timeout=command['timeout']
    


    try:
        logger.autolog(level=1,message='Will send command: ' + str(now))
        child.sendline(str(now))
        logger.autolog(level=1,message='Expecting for ' + str(cases))
        i=child.expect(cases,timeout=timeout)
        logger.autolog(level=1,message='Got case ' + str(cases[i]))
        logger.autolog(level=1,message='Next cmd is ' + str(next[i]))
        capturedB=str(child.before)
        capturedA=str(child.after)
        logger.autolog(level=1,message=capturedB)
        logger.autolog(level=1,message=capturedA)


    except Exception as e:
        logger.autolog(level=1,message='Error while attempting to execute ' + str(now))
        logger.autolog(level=1,message=e)
        return -1
    return next[i],'  before: ' + capturedB +'  after: '+ capturedA

def pexpectExecuteArray(expectArray, logonArray):
    cmd=[]
    for data in expectArray:
        command=data['cmd']
        cases=[pexpect.TIMEOUT,pexpect.EOF,data['expect']]
        next=['timeout','eof',data['next']]
        cmd.append({'oder': data['order'], 'cmd': data['cmd'], 'cases': cases, 'next' : next , 'timeout' : data['timeout']}) 

    #node=[]
    node=logonArray
    return pexpectExecute(username=node['nodeUsername'], password=node['nodePassword'], loginPrompt=node['nodeLoginPrompt'],ipAddress=node['nodeIP'],cmd=cmd)


def pexpectExecute(username='',password='',loginPrompt='',ipAddress='',cmd=''):

    if(isArgEmpty(inspect.currentframe())):
        logger.autolog(level=1,message='At least one argument is empty. Aborting pexpectExecute')
        return False

    logger.autolog(level=1,message='pexpectExecute on node: ' + ipAddress)

    child=pexpectLogin(username=username,password=password,ipAddress=ipAddress, prompt=loginPrompt)
    next=0
    capturedOutput=[]

    if child:
        while next not in ['timeout', 'eof','end','fail']:
            logger.autolog(level=1,message='In 2 seconds we will execute cmd: ' + str(cmd[next]['cmd']))
            time.sleep(2)
            next,captured = pexpectCommand(cmd[next], child)
            capturedOutput.append(captured)
            if next==-1:
                break
            logger.autolog(level=1,message='Next comand index is: ' + str(next))
        logger.autolog(level=1,message=str(child.before))
        #print (str(child.before))
        logger.autolog(level=1,message='Last command exited with ' + str(next))
        if str(next) not in ['end']:
            child.close()
            return False
        else:
            child.close()
            return capturedOutput

        
def pexpectExecuteYaml(yamlFile, sessionVariables=False, logFormat=''):

    if not yamlFile:
        return False

    #Read YAML file containing the commands to be executed and the hosts where they will be executed
    content=gen.readYamlFile(yamlFile)
    hosts=content['Hosts']
    commands=content['Commands']


    #Each command has an unique index number (order). Here we find out what is the highest order value.
    higherCommandOrder=0
    for command in commands:
        if (int(command['order']) > higherCommandOrder):
            higherCommandOrder=int(command['order']) 
    logger.autolog(level=1,message='higher command order is ' + str(higherCommandOrder),format=logFormat)

    #Create a new list of dictionaries. The amount of dictionaries is the highest order value +1
    commandList=[{}]*(higherCommandOrder+1)

    #Sort the commands by index number (order)
    for command in commands:
        next=[]
        cases=[]
        for output in command['outputs']:
            next.append(output['next'])
            cases.append(output['pattern'])
        commandList[command['order']]={'cmd':command['command'],'next':next,'cases': cases,'timeout':command['timeout']}

    #define initial variables
    hostCapturedOutput={}
    wholeStatus=True
    errorReport=''

    #Execute the comands in all the hosts contained in list "hosts"
    for host in hosts:
        try:
            if sessionVariables:
                host['host']=gen.procHashStringV2(host['host'], sessionVariables)
                host['username']=gen.procHashStringV2(host['username'], sessionVariables)
                host['password']=gen.procHashStringV2(host['password'], sessionVariables)
                #host['loginPrompt']=gen.procHashStringV2(host['loginPrompt'], sessionVariables)
                try:
                    host['port']=gen.procHashStringV2(host['port'], sessionVariables)
                except Exception as e:
                    host['port']='22'
                
            #login to host and get handler (child)
            child=pexpectLoginNoKnownHost(ipAddress=host['host'],username=host['username'],password=host['password'], prompt=host['loginPrompt'],addStartToPrompt=False, port=host['port'])
            #start by the first command, order 0
            next=0
            #create list to store captured output of executed commands
            capturedOutput=[]

            if child:
                while next not in ['timeout', 'eof','end','fail']:
                    #Get dictionary with order specified by "next"
                    cmd=commandList[next]
                
                    if sessionVariables:
                        logger.autolog(level=1,message='Command to execute is:  ' + str(cmd['cmd']))
                        #cmd['cmd']=gen.procHashString(cmd['cmd'], sessionVariables, {})
                        cmd['cmd']=gen.procHashStringV2(cmd['cmd'], sessionVariables)


                    next,captured = pexpectCommand(cmd, child)
                    capturedOutput.append({'command':str(cmd['cmd']),'output':captured})
                    if next==-1:
                        break
                    logger.autolog(level=1,message='Next comand index is: ' + str(next),format=logFormat)
                logger.autolog(level=1,message=str(child.before),format=logFormat)
                #print (str(child.before))
                logger.autolog(level=1,message='Last command exited with ' + str(next),format=logFormat)
                if str(next) not in ['end']:
                    child.close()
                    hostCapturedOutput[host['host']]={'status':False,'output':capturedOutput}
                    wholeStatus=False
                    errorReport=errorReport+str(capturedOutput)+'    '
                else:
                    child.close()
                    hostCapturedOutput[host['host']]={'status':True,'output':capturedOutput}
        except Exception as e:
            logger.autolog(level=1,message='Error while attempting to execute commands in host  ' + str(host['host']),format=logFormat)
            logger.autolog(level=1,message=e,format=logFormat)
            wholeStatus=False
            errorReport=errorReport+str(e)+'    '
            

    #print(str(hostCapturedOutput))
    return wholeStatus,errorReport,hostCapturedOutput
