
import logging
import threading
import urllib2
import glob
import json
import os
import os.path
import requests
import sys
import time
import xml.dom.minidom
import yaml
import platform
from xml.dom import minidom
import datetime
import xmltodict
import paramiko
import select 
import random
import string


modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import logger as logger
import genFunctions as gen

#####################################
# Global Variables
#####################################

credsFile=gen.credsFile

dcloudController={'LON':'https://dcloud2-lon.cisco.com/api/public/',
                  'RTP':'https://dcloud2-rtp.cisco.com/api/public/',
                  'SNG':'https://dcloud2-sng.cisco.com/api/public/',
                  'CHI':'https://dcloud2-chi.cisco.com/api/public/'}


#####################################
# GENERIC FUNCTIONS
#####################################

def doNothing(sessionFile):
    return True



#####################################
# Front-end Functions 
#####################################

def readyToDelete(sessionId, target, otherInputs=[], logFormat=''):
    global activeSessionFolderPath

    if (type(otherInputs) is dict):
        try:
            dc=otherInputs["dc"]
            id=otherInputs["id"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'


    sessionStatus= getSessionStatus(id,dc)
    if sessionStatus == 'deleted':
        logger.autolog(level=1,message='session is deleted',format=logFormat)
        return {'readyToDelete': True}, 'Success'

    if sessionStatus == 'active':
        logger.autolog(level=1,message='session is active',format=logFormat)
        return {'readyToDelete': False}, 'Success'

    return {'readyToDelete': False}, 'Failed: ' + str(sessionStatus)


#####################################
# API Functions 
#####################################

def dcloudControllerApi(dc, api):
    global dcloudController

    url=dcloudController[dc]+api
    for tries in [1,2]:
        try:
            content=urllib2.urlopen(url).read()
            logger.autolog (level=1,message='Session status obtained at the '+ str(tries) +' try, in ' + str(url) + ' is: "' + str(content) +'"')
            return content
        except Exception as e:
            logger.autolog(level=1,message='Failed to call dcloud API ' + api + ' in dc ' + dc)
            logger.autolog(level=1,message=e)
        time.sleep(10)

    logger.autolog(level=1,message='WARNING: UNable to reach dCloud Controller.API:  ' + api + ' in dc ' + dc)
    return '{"status":"ACTIVE"}'
    #return 'Failed:Unable to call dcloud API ' + api + ' in dc ' + dc


def getSessionStatus(sessionId,sessionDc=''):
    import ssl
    ssl._create_default_https_context = ssl._create_unverified_context

    content=dcloudControllerApi(sessionDc, 'checkSession?sessionId='+sessionId)
    if 'Failed:Unable to call dcloud API' not in content: 
        if (content == '{"status":"ACTIVE"}'):
            return 'active'
        if (content == '{"status":"STARTING_UP"}'):
            return 'active'
        if (content == '{"status":"SCHEDULED"}'):
            return 'active'
        if (content == '{"status":"STARTING_UP"}'):
            return 'active'
        if (content == '{"status":"AVAILABLE"}'):
            return 'active'
        if (content == '{"status":"UNKNOWN"}'):
            return 'deleted'
        if (content == '{"status":"SHUTTING_DOWN"}'):
            return 'deleted'
        if (content == '{"status":"COMPLETE"}'):
            return 'active'
        if (content == '{"status":"CANCELLED"}'):
            return 'deleted'
        if (content == '{"status":"SESSIONDELETE"}'):
            return 'active'
        if (content == '{"status":"DELETED"}'):
            return 'deleted'
        if (content == '{"status":"PRESERVE"}'):
            return 'active'
        if (content == '{"status":"PRESERVE_AS_FULL"}'):
            return 'active'
        if (content == '{"status":"SAVING"}'):
            return 'active'
        if (content == '{"status":"SAVED"}'):
            return 'deleted'
        if (content == 'Invalid Session Id'):
           return 'deleted'
        return 'active'
    else:
        return content


