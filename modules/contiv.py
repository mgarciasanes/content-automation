import urllib2
import json
import os
import requests
import sys
import time
import platform
import sys

import ssl
import logging
requests.packages.urllib3.disable_warnings()

modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger
import http as http
import worker as worker
import xmltodict 
####################################
#  Global Variables
####################################
credsFile=gen.credsFile
globalLogFormat=''

   
def createContivUser(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            username=gen.sanitizeString(otherInputs["username"])
            password=gen.sanitizeString(otherInputs["password"])
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'


    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    data={"username": username ,"password": username ,"first_name" : username, "last_name": username }

    if contivCreateUser(target, token, data):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while creating User in CONTIV'



def createContivTenant(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            defaultNetworkName=otherInputs["defaultNetworkName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    data={"tenantName": tenantName ,"defaultNetwork": defaultNetworkName}

    if contivCreateTenant(target, token, tenantName, data):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while creating Tenant in CONTIV'



def createContivAuthorization(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            userName=otherInputs["userName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    data={"tenantName": tenantName ,"principalName": userName, 'local': True, 'role' : 'ops' }

    if contivCreateAuthorization(target, token, data):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while creating User Authorizaiton  in CONTIV'


def createContivExtContract(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            contractName=otherInputs["contractName"]
            contractUri=otherInputs['contractUri']
            contractType=otherInputs['contractType']
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    gata={"tenantName": tenantName ,"contractsGroupName": contractName, 'contractsType': contractType, 'contracts' : [contractUri] }
    gata={"tenantName": tenantName ,"contractsGroupName": contractName, 'contractsType': contractType, 'contracts' : [contractUri] }
    data={"tenantName": tenantName ,"contractsGroupName": contractName, 'contractsType': contractType, 'contracts' : [contractUri] }

    if contivCreateExtContract(target, token,tenantName, contractName, data):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while creating External Contracts  in CONTIV'




def createContivNetwork(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            networkName=otherInputs["networkName"]
            subnet=otherInputs['subnet']
            gateway=otherInputs['gateway']
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    data={"pktTag":0 ,"encap": 'vlan', 'ipv6Gateway': '', 'tenantName' : tenantName, 'ipv6Subnet':'', 'gateway': gateway, 'networkName':networkName, 'subnet':subnet, 'nwType': 'data'}

    if contivCreateNetwork(target, token,tenantName, networkName, data):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while creating Network  in CONTIV'




def createContivGroup(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            groupName=otherInputs["groupName"]
            networkName=otherInputs['networkName']
            extContractsGrps=otherInputs['extContractsGrps']
            policies=otherInputs['policies']
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    if extContractsGrps is None or extContractsGrps=='':
        extContractsGrps=[]
    if policies is None or policies=='':
        policies=[]



    data={"groupName":groupName , 'tenantName' : tenantName, 'networkName': networkName, 'extContractsGrps':extContractsGrps, 'policies':policies}
    if contivCreateGroup(target, token,tenantName, groupName, data):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while creating GROUP  in CONTIV'







def createContivAppProfile(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            appProfileName=otherInputs["appProfileName"]
            endpointGroups=otherInputs['endpointGroups']
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'


    data={"endpointGroups":endpointGroups , 'tenantName' : tenantName, 'appProfileName':appProfileName}
    if contivCreateAppProfile(target, token,tenantName, appProfileName, data):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while creating Application Profile  in CONTIV'



def createContivPolicy(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            policyName=otherInputs["policyName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    data={"policyName":policyName , 'tenantName' : tenantName}
    if contivCreatePolicy(target, token,tenantName,policyName, data):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while creating Policy  in CONTIV'


def createContivRule(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            policyName=otherInputs["policyName"]
            ruleId=otherInputs['ruleId']
            protocol=otherInputs['protocol']
            fromEndpointGroup=otherInputs['fromEndpointGroup']
            action=otherInputs['action']
            port=otherInputs['port']
            direction=otherInputs['direction']
             
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    if port is None or port =='':
        port=int()

    data={'tenantName':tenantName,'policyName':policyName, 'ruleId': str(ruleId), "protocol":protocol, 'fromEndpointGroup': fromEndpointGroup , 'action' : action, 'port': port, 'direction' : direction}
    if contivCreateRule(target, token,tenantName, policyName, ruleId, data):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while creating Rule in CONTIV'





def deleteContivUser(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            username=gen.sanitizeString(otherInputs["username"])
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    if contivDeleteUser(target, token, username):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while deleting  User in CONTIV'



def deleteContivTenant(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    if contivDeleteTenant(target, token, tenantName):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while deleting Tenant in CONTIV'



def deleteContivAuthorization(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            userName=otherInputs["userName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    if contivDeleteUserAuthorization(target, token, userName):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while deleting  User Permissions in CONTIV'


def deleteContivExtContract(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            contractName=otherInputs["contractName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    if contivDeleteExtContract(target, token,tenantName, contractName):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while deleting  External Contracts  in CONTIV'



def deleteContivNetwork(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            networkName=otherInputs["networkName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    if contivDeleteNetwork(target, token,tenantName, networkName):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while deleting  Network  in CONTIV'


def deleteContivGroup(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            groupName=otherInputs["groupName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    logger.autolog(level=1, message='Waiting 5 seconds for endports tobe removes before deleting Endpoint Group')
    time.sleep(5)

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    if contivDeleteGroup(target, token,tenantName, groupName):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while deleting  GROUP  in CONTIV'



def deleteContivAppProfile(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            appProfileName=otherInputs["appProfileName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'
    if contivDeleteAppProfile(target, token,tenantName, appProfileName):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while deleting  Application Profile  in CONTIV'




def deleteContivPolicy(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            policyName=otherInputs["policyName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'
    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'
    if contivDeletePolicy(target, token,tenantName,policyName):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while deleting Policy  in CONTIV'


def deleteContivRule(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            policyName=otherInputs["policyName"]
            ruleId=otherInputs['ruleId']
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    if contivDeleteRule(target, token,tenantName, policyName, ruleId):
        return {'result':True},'Success'
    else:
        return {'result':False},'Failed: Error while deleting  Rule in CONTIV'



def deleteUsersDeployment(sessionId, target, otherInputs=[], logFormat=''):
    global globalLogFormat
    globalLogFormat=logFormat

    if (type(otherInputs) is dict):
        try:
            tenantName=otherInputs["tenantName"]
            policyName=otherInputs["policyName"]
            appProfileName=otherInputs["appProfileName"]
            consContractName=otherInputs["consContractName"]
            provContractName=otherInputs["provContractName"]
            policyName=otherInputs["policyName"]
            networkName=otherInputs["networkName"]
            appGroupName=otherInputs["appGroupName"]
            dbGroupName=otherInputs["dbGroupName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    token=contivLogin(target)
    if not token:
        return {},'Failed: Unable to login to CONTIV'

    result=''

    logger.autolog(level=1,message='Removing Users Contiv Deployment: App Profile')
    if contivDeleteAppProfile(target, token,tenantName, appProfileName):
        result = result + "appProfile:OK  "
    else:
        result = result + "AppProfile:NOK  "

    logger.autolog(level=1,message='Removing Users Contiv Deployment: rule1')
    if contivDeleteRule(target, token,tenantName, policyName, 1):
        result = result + "rule1:OK  "
    else:
        result = result + "rule1:NOK  "

    logger.autolog(level=1,message='Removing Users Contiv Deployment: rule2')
    if contivDeleteRule(target, token,tenantName, policyName, 2):
        result = result + "rule2:OK  "
    else:
        result = result + "rule2:NOK  "

    logger.autolog(level=1,message='Removing Users Contiv Deployment: appgroup')
    if contivDeleteGroup(target, token,tenantName, appGroupName):
        result = result + "appgroup:OK  "
    else:
        result = result + "appgroup:NOK  "

    logger.autolog(level=1,message='Removing Users Contiv Deployment: dbgroup')
    if contivDeleteGroup(target, token,tenantName, dbGroupName):
        result = result + "dbgroup:OK  "
    else:
        result = result + "dbgroup:NOK  "

    logger.autolog(level=1,message='Removing Users Contiv Deployment: Network')
    if contivDeleteNetwork(target, token,tenantName, networkName):
        result = result + "Network:OK  "
    else:
        result = result + "Network:NOK  "

    logger.autolog(level=1,message='Removing Users Contiv Deployment: netPolicy')
    if contivDeletePolicy(target, token,tenantName, policyName):
        result = result + "netPolicy:OK  "
    else:
        result = result + "netPolicy:NOK  "

    logger.autolog(level=1,message='Removing Users Contiv Deployment: extContract Consumed')
    if contivDeleteExtContract(target, token,tenantName, consContractName):
        result = result + "consExtContract:OK  "
    else:
        result = result + "consExtContract:NOK  "

    logger.autolog(level=1,message='Removing Users Contiv Deployment: extContract Provided ')
    if contivDeleteExtContract(target, token,tenantName, provContractName):
        result = result + "provExtContract:OK  "
    else:
        result = result + "provExtContract:NOK  "

    if "NOT" in result:
        return {'result':False},'Failed: Error while removing users contiv deployment' + result
    else:
        return {'result':True},'Success'









###############################
# CONTIV Functions
##############################

def contivLogin(target):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/auth_proxy/login/'
    data={"username": creds['username'] ,"password": creds['password']}
    data=json.dumps(data)
    method='post'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    headers={'Content-Type':'application/json'}
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result:
        resultDict=eval(result.text)
        return resultDict['token']
    else:
        return result

def contivCreateUser(target, token, data):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/auth_proxy/local_users/'
    headers={'X-Auth-Token': token}
    data=json.dumps(data)
    method='post'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [201, 400]:    #400 User already exists,  201 Success
        return True
    else:
        return False


def contivDeleteUser(target, token, userName):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/auth_proxy/local_users/'+userName+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps({})
    method='delete'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [204,404]:    #204 Deleted / 404 Not found 
        return True
    else:
        return False


def contivCreateTenant(target, token, tenantName,data):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/tenants/'+tenantName+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps(data)
    method='post'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 201]:    #500 Tenant already exists or error,  200 Success
        return True
    elif result.status_code in [500]:
        if 'error' in result.text:
            return False
        else:
            return True
    else:
        return False


def contivDeleteTenant(target, token, tenantName):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/tenants/'+tenantName+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps({})
    method='delete'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 404]:   # 200 Success 404 Not Found
        return True
    if result.status_code in [500]:
        if 'not found' in result.text:
            return True
    else:
        return False






def contivCreateAuthorization(target, token,data):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/auth_proxy/authorizations/'
    headers={'X-Auth-Token': token}
    data=json.dumps(data)
    method='post'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 201]:    #500 Tenant already exists or error,  200 Success
        return True
    elif result.status_code in [500]:
        if 'error' in result.text:
            return False
        else:
            return True
    else:
        return False



def contivGetAuthorizationUuid(target, token, userName):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/auth_proxy/authorizations/'
    headers={'X-Auth-Token': token}
    data=json.dumps({})
    method='get'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    logger.autolog(format=globalLogFormat, level=1, message=str(result.json))
    authorizations=json.loads(result.text)
    logger.autolog(format=globalLogFormat, level=1, message='Searching for authorization of user ' + userName)
    if result.status_code in [200]:    #500 Tenant already exists or error,  200 Success
        for authorization  in  authorizations:
            if authorization['PrincipalName']==userName:
                logger.autolog(format=globalLogFormat, level=1, message='Authorization for User ' + userName + ' found. UUID: ' + str(authorization["AuthzUUID"]))
                return authorization["AuthzUUID"]
        logger.autolog(format=globalLogFormat, level=1, message='Authorization for user ' +userName + ' not found') 
        return 'Not Found'
    else:
        return False


def contivDeleteAuthorization(target, token, Uuid):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/auth_proxy/authorizations/'+Uuid+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps({})
    method='delete'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    #logger.autolog(format=globalLogFormat, level=1, message=str(result.json))
    if result.status_code in [204]:    #204 Success
        return True
    else:
        return False


def contivDeleteUserAuthorization(target, token, userName):
    uuid=contivGetAuthorizationUuid(target, token, userName)
    if uuid == 'Not Found':
        return True
    if uuid:
        return contivDeleteAuthorization(target, token, uuid)
    else:
        return False

 

def contivCreateExtContract(target, token,tenantName, contractName,data):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/extContractsGroups/'+tenantName+':'+contractName+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps(data)
    method='post'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 201]:    #500 Tenant already exists or error,  200 Success
        return True
    elif result.status_code in [500]:
        if 'error' in result.text:
            return False
        else:
            return True
    else:
        return False



def contivDeleteExtContract(target, token,tenantName, contractName):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/extContractsGroups/'+tenantName+':'+contractName+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps({})
    method='delete'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 404]:    #200 Success 404 NOt found
        return True
    if result.status_code in [500]:
        if 'not found' in result.text:
            return True 
    else:
        return False


def contivCreatePolicy(target, token,tenantName, policyName, data):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/policys/'+tenantName+':'+policyName+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps(data)
    method='post'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 201]:    # 200 Success
        return True
    elif result.status_code in [500]:
        if 'error' in result.text:
            if "Failed to perform duplicate request" in result.text:
                return True
            return False
        else:
            return True
    else:
        return False



def contivDeletePolicy(target, token,tenantName, policyName):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/policys/'+tenantName+':'+policyName+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps({})
    method='delete'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 404]:    #200 Success 404 NOt found
        return True
    elif result.status_code in [500]:
        if 'not found' in result.text:
            return True
    else:
        return False



def contivCreateRule(target, token,tenantName, policyName, ruleId, data):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/rules/'+tenantName+':'+policyName+':'+str(ruleId)+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps(data)
    method='post'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 201]:    # 200 Success
        return True
    elif result.status_code in [500]:
        if 'error' in result.text:
            if "Failed to perform duplicate request" in result.text:
                return True
            return False
        else:
            return True
    else:
        return False



def contivDeleteRule(target, token,tenantName, policyName, ruleId):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/rules/'+tenantName+':'+policyName+':'+str(ruleId)+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps({})
    method='delete'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 404]:    #200 Success 404 Not found
        return True
    elif result.status_code in [500]:
        if 'not found' in result.text:
            return True
    else:
        return False




def contivCreateNetwork(target, token,tenantName, networkName,data):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/networks/'+tenantName+':'+networkName+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps(data)
    method='post'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 201]:    #500 Tenant already exists or error,  200 Success
        return True
    elif result.status_code in [500]:
        if 'error' in result.text:
            return False
        else:
            return True
    else:
        return False


def contivDeleteNetwork(target, token,tenantName, networkName):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/networks/'+tenantName+':'+networkName+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps({})
    method='delete'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 404]:    #200 Success  404 Not Found
        return True
    elif result.status_code in [500]:
        if 'not found' in result.text:
            return True
    else:
        return False







def contivCreateGroup(target, token,tenantName, groupName,data):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/endpointGroups/'+tenantName+':'+groupName+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps(data)
    method='post'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 201]:    #500 Tenant already exists or error,  200 Success
        return True
    elif result.status_code in [500]:
        if 'error' in result.text:
            if "Failed to perform duplicate request" in result.text:
                return True
            return False
        else:
            return True
    else:
        return False


def contivDeleteGroup(target, token,tenantName, groupName):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/endpointGroups/'+tenantName+':'+groupName+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps({})
    method='delete'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 404]:    # 200 Success 404 Not Founfd
        return True
    elif result.status_code in [500]:
        if 'not found' in result.text:
            return True
    else:
        return False




def contivCreateAppProfile(target, token,tenantName, appProfileName,data):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/appProfiles/'+tenantName+':'+appProfileName+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps(data)
    method='post'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 201]:    #500 Tenant already exists or error,  200 Success
        return True
    elif result.status_code in [500]:
        if 'error' in result.text:
            if "Failed to perform duplicate request" in result.text:
                return True
            return False
        else:
            return True
    else:
        return False

def contivDeleteAppProfile(target, token,tenantName, appProfileName):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/appProfiles/'+tenantName+':'+appProfileName+'/'
    headers={'X-Auth-Token': token}
    data=json.dumps({})
    method='delete'
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 404]:    # 200 Success  404 Not Found
        return True
    elif result.status_code in [500]:
        if 'not found' in result.text:
            return True
    else:
        return False




def contivGetGroup(target, token,tenantName, groupName):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/endpointGroups/'+tenantName+':'+groupName+'/'
    headers={'X-Auth-Token': token}
    method='get'
    data=json.dumps({})
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 201]:    #500 Tenant already exists or error,  200 Success
        return True
    elif result.status_code in [500]:
        if 'error' in result.text:
            return False
        else:
            return True
    else:
        return False


def contivGetAllGroups(target, token):
    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'
    api='/endpointGroups/'
    headers={'X-Auth-Token': token}
    method='get'
    data=json.dumps({})
    protocol='https'
    logger.autolog(format=globalLogFormat, level=1, message="URL is " + protocol + creds['api'] + api + " on port " + str(creds['port']))
    logger.autolog(format=globalLogFormat, level=1, message=' data is : ' + str(data))
    result=http.sendHttpRequest(protocol=protocol,host=creds['host'],url=creds['api']+api, data=data, method=method,port=creds['port'], headers=headers)
    logger.autolog(format=globalLogFormat, level=1, message= "Result is " + str(result))
    logger.autolog(format=globalLogFormat, level=1, message=result.text)
    if result.status_code in [200, 201]:    #500 Tenant already exists or error,  200 Success
        return True
    elif result.status_code in [500]:
        if 'error' in result.text:
            return False
        else:
            return True
    else:
        return False

def execFromRecipe(sessionDetails, action='startup'):

    logFormat=sessionDetails['demo']+'|'+ sessionDetails['id'] +'|' + sessionDetails['owner']
    logger.autolog(level=1,message=str(sessionDetails), format=logFormat)
    logger.autolog(message='Got Request. Executing...', format=logFormat)


    if action == 'startup':
        tasks=gen.getDemoTasks(sessionDetails['demo'])
    if action == 'cleanup':
        tasks=gen.getDemoCleanup(sessionDetails['demo'])

    if not tasks:
        logger.autolog(message='Unable to get tasks for demo ' + sessionDetails['demo'], format=logFormat)
        return False
    else:
        failureFree,allTasksExecuted=worker.execTasks(sessionDetails,tasks)
        if (failureFree and allTasksExecuted):
            logger.autolog(message='Test  request executed without errors', format=logFormat)
        else:
            logger.autolog(message='Test request executed with errors or some tasks were not executed', format=logFormat)






if __name__ == '__main__':

    logger.setLogFiles(newLogFile='contiv.log',newLogDebugFile='contiv_DEBUG.log')
    logger.setLogger()
    logging.getLogger('werkzeug').setLevel(logging.ERROR)


    xml='<?xml version="1.0" encoding="UTF-8"?><session><demo>contiv-test</demo><datacenter>RTP</datacenter><id>182630</id><owner>mgarcias</owner><vpod>010</vpod><anycpwd>95f76f</anycpwd><type>session</type><devices></devices><translations></translations><dids></dids></session>'

    sessionDetails=xmltodict.parse(xml)['session']
    #execFromRecipe(sessionDetails)
    execFromRecipe(sessionDetails, action='cleanup')


    #createUcsdUser(1000, 'ucsd', otherInputs={'username': 'test' , 'password' : 'password'})
    #deleteUcsdUser(1000, 'ucsd', otherInputs={'username': 'test'})
    token=contivLogin("contiv")
    #createContivUser(1000, 'contiv', otherInputs={'username':'test','password': 'C1sco12345'})   
    #createContivTenant(1000, 'contiv', otherInputs={'tenantName':'test','defaultNetworkName': 'defNet'})   
    #createContivPermissions(1000, 'contiv', otherInputs={'tenantName':'test','userName': 'test'})
    #createContivExtContract(1000, 'contiv', otherInputs={'tenantName':'TENANTNAME','contractName': 'provided', 'contractUri':'uni/tn-common/brc-contiv-external-contract', 'contractType':'provided'})
    #createContivExtContract(1000, 'contiv', otherInputs={'tenantName':'TENANTNAME','contractName': 'consumed', 'contractUri':'uni/tn-common/brc-contiv-external-contract', 'contractType':'consumed'})

    #createContivNetwork(1000, 'contiv', otherInputs={'tenantName':'TENANTNAME','networkName': 'mytestNet', 'subnet':'192.168.1.0/24', 'gateway':'192.168.1.254'})

    #createContivGroup(1000, 'contiv', otherInputs={'tenantName':'TENANTNAME','networkName': 'mytestNet', 'groupName':'myEPG1', 'extContractsGrps':['consumed','provided']})
    #createContivGroup(1000, 'contiv', otherInputs={'tenantName':'TENANTNAME','networkName': 'mytestNet', 'groupName':'myEPG2', 'extContractsGrps':['consumed','provided']})
    #contivGetGroup('contiv',token, 'TENANTNAME', 'contiv-group-app')
    #contivGetAllGroups('contiv',token)
    #createContivAppProfile(1000, 'contiv', otherInputs={'tenantName':'TENANTNAME','appProfileName': 'myAPP','endpointGroups':['myEPG1','myEPG2']})
    
    contivDeleteUser('contiv', token, 'mgarcias-003')
    contivDeleteUserAuthorization('contiv',token, 'mgarcias-003')
    contivDeleteAppProfile('contiv', token,'mgarcias-003', 'mgarcias-003-contiv-AppProfile')
 
    contivDeleteGroup('contiv', token,'mgarcias-003', 'mgarcias-003-APP')
    contivDeleteGroup('contiv', token,'mgarcias-003', 'mgarcias-003-DB')

    contivDeleteExtContract('contiv', token,'mgarcias-003', 'mgarcias-003-ext-contract-consumed')
    contivDeleteExtContract('contiv', token,'mgarcias-003', 'mgarcias-003-ext-contract-provided')

    contivDeleteNetwork('contiv', token,'mgarcias-003', 'mgarcias-003-net')
    contivDeleteTenant('contiv', token, 'mgarcias-003')

