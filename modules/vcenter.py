#!/usr/bin/env python
# Copyright 2015 Michael Rice <michael@michaelrice.org>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from __future__ import print_function

import atexit

import requests
from pyVim import connect
import sys
from vcentertools import cli
#from vcentertools import tasks
import ssl
import logging
from pyVmomi import vmodl
from pyVmomi import vim
from  datetime import datetime,timedelta
import time
requests.packages.urllib3.disable_warnings()
import threading
import sys
import re
import socket



modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger


####################################
#  Global Variables
####################################
#credsFile=r'/root/indemo_automation/creds.cfg'
credsFile=gen.credsFile

##############################
# Fronend Functions (Called externally)
##############################

def createVmFolderWithPermissions(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile

    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            folderName=otherInputs["folderName"]
            dcName=otherInputs["dcName"]
            userName=otherInputs["userName"]
            groupName=otherInputs['groupName']
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat )
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()

    #Create Folder
    folder = createVmFolder(content, dcName, folderName)
    if not folder:
        logger.autolog(level=1,message='Not able to create folder, Aborting...',format=logFormat)
        return  {},'Failed: Not able to create folder in vc'

    #Create Permissions
    if setFolderPermissions(content,folder,userName,groupName):
        return {},'Success'
    else:
        return {},'Failed: Not able to assign permissions to folder'


def createVmFolderWithPermissionsV2(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile

    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            folderPath=otherInputs["folderPath"]
            folderName=otherInputs["folderName"]
            dcName=otherInputs["dcName"]
            userName=otherInputs["userName"]
            groupName=otherInputs['groupName']
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat )
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()

    #Create Folder
    folder = createVmSubfolder(content, dcName, folderName, folderPath=folderPath)
    if not folder:
        logger.autolog(level=1,message='Not able to create folder, Aborting...',format=logFormat)
        return  {},'Failed: Not able to create folder in vc'

    #Create Permissions
    if setFolderPermissions(content,folder,userName,groupName):
        return {},'Success'
    else:
        return {},'Failed: Not able to assign permissions to folder'




def setDatastorePermissions(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            dcName=otherInputs["dcName"]
            dsName=otherInputs["dsName"]
            userName=otherInputs["userName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()

    if not setPermissionsToUserDs(host = creds['host'],user = creds['username'], password = creds['password'], dcName=dcName, dsName=dsName, userName=userName):
        return {},'Failed: Unable to set permissions on DS'
    else:
        return {},'Success'


def createUserVm(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            vmName=otherInputs["vmName"]
            dsName=otherInputs["dsName"]
            destFolderName=otherInputs["destFolderName"]
            templateName=otherInputs["templateName"]
            dcName=otherInputs["dcName"]
            networkNameAdapters=otherInputs["networkNameAdapters"]
            clusterName=otherInputs["clusterName"]
            vmConfig=otherInputs["vmConfig"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'

        try:
            host=otherInputs["host"]
        except Exception as e:
            host=""

    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'


    if networkNameAdapters is None or networkNameAdapters=='':
        networkNameAdapters=[]

    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()

    template=getVm(content,templateName)
    if not template:
        return {},'Failed: Unable to get VM template'

    if not cloneVmFromTemplate(si,template=template, destFolderName=destFolderName, dsName=dsName, vmName=vmName, dcName=dcName, clusterName=clusterName, host=host, networkNames=networkNameAdapters, vmConfig=vmConfig, logFormat=logFormat):
        return {},'Failed: Failed while cloning from template'

    #t = threading.Thread(target=cloneVmFromTemplate, args=(si,) , kwargs={'template':template, 'destFolderName':destFolderName, 'dsName':dsName, 'vmName':vmName, 'dcName':dcName, 'clusterName':clusterName, 'host':host, 'networkNames':networkNameAdapters, 'vmConfig':vmConfig, 'logFormat':logFormat})
    #t.setDaemon(True)
    #t.start()

    return {},'Success'




def createUserVmV2(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            vmName=otherInputs["vmName"]
            dsName=otherInputs["dsName"]
            destFolderName=otherInputs["destFolderName"]
            templateName=otherInputs["templateName"]
            dcName=otherInputs["dcName"]
            networkNameAdapters=otherInputs["networkNameAdapters"]
            clusterName=otherInputs["clusterName"]
            vmConfig=otherInputs["vmConfig"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'

        try:
            host=otherInputs["host"]
        except Exception as e:
            host=""

    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'


    if networkNameAdapters is None or networkNameAdapters=='':
        networkNameAdapters=[]

    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()

    template=getVm(content,templateName)
    if not template:
        return {},'Failed: Unable to get VM template'

    if not cloneVmFromTemplate(si,template=template, destFolderName=destFolderName, dsName=dsName, vmName=vmName, dcName=dcName, clusterName=clusterName, host=host, networkNames=networkNameAdapters, vmConfig=vmConfig, logFormat=logFormat):
        return {},'Failed: Failed while cloning from template'

    #t = threading.Thread(target=cloneVmFromTemplate, args=(si,) , kwargs={'template':template, 'destFolderName':destFolderName, 'dsName':dsName, 'vmName':vmName, 'dcName':dcName, 'clusterName':clusterName, 'host':host, 'networkNames':networkNameAdapters, 'vmConfig':vmConfig, 'logFormat':logFormat})
    #t.setDaemon(True)
    #t.start()

    return {},'Success'




def createUserVmNoWait(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            vmName=otherInputs["vmName"]
            dsName=otherInputs["dsName"]
            destFolderName=otherInputs["destFolderName"]
            templateName=otherInputs["templateName"]
            dcName=otherInputs["dcName"]
            networkNameAdapters=otherInputs["networkNameAdapters"]
            clusterName=otherInputs["clusterName"]
            vmConfig=otherInputs["vmConfig"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'

        try:
            host=otherInputs["host"]
        except Exception as e:
            host=""

    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'


    if networkNameAdapters is None or networkNameAdapters=='':
        networkNameAdapters=[]

    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()

    template=getVm(content,templateName)
    if not template:
        return {},'Failed: Unable to get VM template'

    #if not cloneVmFromTemplate(si,template=template, destFolderName=destFolderName, dsName=dsName, vmName=vmName, dcName=dcName, clusterName=clusterName, host=host, networkNames=networkNameAdapters, vmConfig=vmConfig, logFormat=logFormat):
    #    return {},'Failed: Failed while cloning from template'

    t = threading.Thread(target=cloneVmFromTemplate, args=(si,) , kwargs={'template':template, 'destFolderName':destFolderName, 'dsName':dsName, 'vmName':vmName, 'dcName':dcName, 'clusterName':clusterName, 'host':host, 'networkNames':networkNameAdapters, 'vmConfig':vmConfig, 'logFormat':logFormat})
    t.setDaemon(True)
    t.start()

    return {},'Success'



def removeVmFolderWithVms(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            folderName=otherInputs["folderName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'

        try:
            dcName=otherInputs["dcName"]
        except Exception as e:
            logger.autolog(level=1,message='Didnt get dcName, using default HybridCloud',format=logFormat)
            dcName='HybridCloud'

    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'
    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()



    if isinstance(folderName, str):
        folder = getFolder(content, folderName)
    if isinstance(folderName, list):
        folder = getVmSubfolder(content, dcName, folderPath=folderName)

    if not folder:
        logger.autolog(level=1,message='Folder does not exists.',format=logFormat)
        return {}, 'Success'

    vms = getAllVmsInFolder(content,folderName)
    if(destroyVms(si, vms)):
        if destroyVmFolder(content, folderName):
            return {}, 'Success'
        else:
            return {}, 'Failed: Unable to delete Folder'
    else:
        logger.autolog(level=1,message='Failed to delete VMs in folder ' + folderName,format=logFormat)
        return {}, 'Failed: Unable to delete Vms in folder'




def revertVmSnapshotInFolder(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments

    if (type(otherInputs) is dict):
        try:
            dcName=otherInputs["dcName"]
            folderPath=otherInputs["folderPath"]
            snapshotName=otherInputs["snapshotName"]


        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'

    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'
    # Get Credentials and connect to vCenter

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'


    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])

    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()


    #Get folder or subfolder
    folder=getVmSubfolder(content, dcName, folderPath=folderPath)

    logger.autolog(level=1,message='Got folder: ' + str(folder.name) ,format=logFormat)

    #Get all vms in folder
    vmNames=getAllVmsInFolder(content,folder.name)

    if vmSnapshot(si, vmNames,snapshotName,operation='revert'):
        logger.autolog(level=1,message='VMs reverted OK!',format=logFormat)
        return {}, 'Success'
    else:
        logger.autolog(level=1,message='Error while reverting snapshots in ' + str(folderPath),format=logFormat)
        return {}, 'Failed: Error while reverting snapshots'



def getVmSubfolderF(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments

    if (type(otherInputs) is dict):
        try:
            folderName=otherInputs["folderName"]
            dcName=otherInputs["dcName"]
            folderPath=otherInputs["folderPath"]

        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'

    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'
    # Get Credentials and connect to vCenter

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()

    folder = getVmSubfolder(content, dcName, folderPath=folderPath)

    if folder:
        logger.autolog(level=1,message='Found it!',format=logFormat)
        return {}, 'Success'
    else:
        logger.autolog(level=1,message='Folder not found:  ' + folderName,format=logFormat)
        return {}, 'Failed: Unable to find folder'



def createVmSubfolderF(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments

    if (type(otherInputs) is dict):
        try:
            folderName=otherInputs["folderName"]
            dcName=otherInputs["dcName"]
            folderPath=otherInputs["folderPath"]

        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'

    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'
    # Get Credentials and connect to vCenter

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()

    folder = createVmSubfolder(content, dcName,folderName, folderPath=folderPath)

    if folder:
        logger.autolog(level=1,message='Found it!',format=logFormat)
        return {}, 'Success'
    else:
        logger.autolog(level=1,message='Folder not found:  ' + folderName,format=logFormat)
        return {}, 'Failed: Unable to find folder'








def resetUserVm(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            vmName=otherInputs["vmName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'

    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()

    vm=getVm(content,vmName)
    if not vm:
        return {},'Failed: Unable to get VM for reboot'

    if resetVm(vm):
        return {}, 'Success'
    else:
        logger.autolog(level=1,message='Failed to reboot VMs in ' + str(vm),format=logFormat)
        return {}, 'Failed: Unable to reboot Vms '




def vmExists(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            vmName=otherInputs["vmName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()

    try:

        vm=getVm(content,vmName)

        if not vm:
            return {'vmExists': False},'Success'
        return {'vmExists':True}, 'Success'

    except Exception as e:
        eMsg='Error while checking VM existance.' + str(e)
        logger.autolog(level=1,message=eMsg)
        return {'vmExists': False}, 'Success'




def forceConnectNics(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            vmNames=otherInputs["vmNames"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'

    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])

    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()

    eMsg = ''
    errorFlag=False
    for vmName in vmNames:
        logger.autolog(level=1,message='Fetching VM ' + str(vmName) ,format=logFormat)
        count=0
        max=2
        while(count<max):
            vm=getVm(content,vmName)
            if not vm:
                eMsg=eMsg + 'Try ('+str(count)+'): Unable to get VM ' + str(vmName) + ', '
                count=count+1
                if count==max:
                    errorFlag=True
                else:
                    time.sleep(10)
            else:
                if not fixVmNic(vm):
                    eMsg=eMsg +  'Try ('+str(count)+'): Unable to fix  VM  nics on ' + str(vmName) + ', '
                    time.sleep(10)
                    count=count+1
                    if count==max:
                        errorFlag=True
                    else:
                        time.sleep(10)
                else:
                    count=max

    if not errorFlag:
        return {}, 'Success'
    else:
        logger.autolog(level=1,message='Failed to fix VM nics:  ' + str(eMsg),format=logFormat)
        return {}, 'Failed: ' + str(eMsg)




def changeNetworkOnOpencartVm(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            folderName=otherInputs["folderName"]
            networkNames=otherInputs["networkNames"]
            dbNics=otherInputs["dbNics"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'

    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])

    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()

    vms = getAllVmsInFolder(content,folderName)

    for vm in vms:
        logger.autolog(level=1,message='Fetching VM ' + str(vm.config.name) ,format=logFormat)
        if "cqjw" in str(vm.config.name):
            nics=getVmNics(vm)
            if len(nics)==int(dbNics):
                logger.autolog(level=1,message='Found matching VM ' + str(vm.config.name) + " Will change network..." ,format=logFormat)
                vmAdaptersToNetwork(si,vmName=vm.config.name, networkNames=networkNames, logFormat=logFormat)
                return {}, 'Success'

    logger.autolog(level=1,message='Unable to Find matching VM...',format=logFormat)
    return {}, 'Failed: Unable to Find matching VM...'




def getVmIp(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            vmName=otherInputs["vmName"]
            networkName=otherInputs["networkName"]

        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'
    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()

    ips=getVmIps(content,vmName,networkName,logFormat=logFormat)

    logger.autolog(level=1,message='The IPs: ' + str(ips),format=logFormat)

    if(ips):
        return {'ipAddress':ips[0]}, 'Success'
    else:
        return {}, 'Failed: Unable to get IP'



def getGuestMac(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            vmName=otherInputs["vmName"]
            networkAdapter=otherInputs["networkAdapter"]

        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'
    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()

    mac=getVmMac(content,vmName,networkAdapter,logFormat=logFormat)

    logger.autolog(level=1,message='The MAC: ' + str(mac),format=logFormat)

    if(mac):
        return {'macAddress':mac}, 'Success'
    else:
        return {}, 'Failed: Unable to get MAC'


def getDhcpdLease(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            vmName=otherInputs["vmName"]
            macAddress=otherInputs["macAddress"]

        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'
    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()

    result=[]
    count=0
    while True:
        leases=getFileFromVm(content,vmName,r'/var/lib/dhcpd/dhcpd.leases',logFormat=logFormat)
        count=count+1
        for mac in macAddress:
            logger.autolog(level=1,message='searching for ' +mac,format=logFormat)
            match=False
            for line in reversed(leases.splitlines()):
                #print(line)
                if match:
                    if 'lease' in line:
                        #print(line)
                        ip=line.split(' ')[1]
                        result.append({'mac':mac,'ip':ip})
                        macAddress.remove(mac)
                        break
                if mac in line:
                    #print(line)
                    match=True
        if macAddress==[] or count > 20:
            break
        time.sleep(30)

    logger.autolog(level=1,message=str(result),format=logFormat)

    if(result):
        return {'ipAddress':result[0]['ip']}, 'Success'
    else:
        return {}, 'Failed: Unable to get IP'




def addPortGroup(sessionFile, target, otherInputs=[], logFormat=''):
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            vSwitchName=otherInputs["vSwitchName"]
            pgName=otherInputs["pgName"]
            vlan=otherInputs["vlan"]
            hostName=otherInputs["hostName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'
        try:
            vlanOffset=otherInputs["vlanOffset"]
        except Exception as e:
            vlanOffset=0

        vlan=int(vlan)+int(vlanOffset)

    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])

    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'

    content = si.RetrieveContent()

    if not createPortGroup(content, hostName, pgName, vSwitchName,vlan=vlan):
        return {},'Failed: Unable to create port group'
    else:
        return {},'Success'

def removePortGroup(sessionFile, target, otherInputs=[], logFormat=''):
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            pgName=otherInputs["pgName"]
            hostName=otherInputs["hostName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    # Get Credentials and connect to vCenter
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])

    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'

    content = si.RetrieveContent()

    if not deletePortGroup(content, hostName, pgName):
        return {},'Failed: Unable to delete port group'
    else:
        return {},'Success'



def getLicenseStatus(sessionFile, target, otherInputs=[], logFormat=''):
    global credsFile

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'
    content = si.RetrieveContent()
    licenseInfo = getLicense(content, licenseName)


def updateLicenseWithThreshold(sessionFile, target, otherInputs=[], logFormat=''):
    # Get Arguments
    if (type(otherInputs) is dict):
        try:
            hostName=otherInputs["hostName"]
            threshold=otherInputs["threshold"]
            licenseKeyLocation=otherInputs["licenseKeyLocation"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    vmwareLic = gen.getDemoCredentialsByName(licenseKeyLocation['file'],licenseKeyLocation['device'])
    if not vmwareLic:
        return {},'Failed: Unable to get license for vmware host'


    si=connectVcenter(host=creds['host'], user=creds['username'], password=creds['password'])
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..',format=logFormat)
        return {},'Failed: Unable to get Service Instance from vCenter'

    content = si.RetrieveContent()

    hostLicenses=getHostLicenses(content)

    if hostLicenses:

        for hostLicense in hostLicenses:
            logger.autolog(level=1,message='License with name ' + str(hostLicense["name"]) + ' has expiration date of ' + str(hostLicense["expirationDate"]),format=logFormat)
            expirationDate=datetime.strptime(str(hostLicense["expirationDate"]).split(" ")[0],'%Y-%m-%d')
            now=datetime.now()
            logger.autolog(level=1,message='Expiration date is ' + str(expirationDate),format=logFormat)
            logger.autolog(level=1,message='Today is ' + str(now),format=logFormat)


            if expirationDate  > now and expirationDate <= (now + timedelta(days=int(threshold))):
               logger.autolog(level=1,message='License is still valid but will expire soon, will update...',format=logFormat)
               info=updateHostLicense(content, vmwareLic["key"])
               #logger.autolog(level=1,message='New License Key is' + str(info))             
               if info.total==0:
                   return {}, 'Failed: License Update failed'
               else:
                   return {},'Success'
            elif expirationDate <= now:
               logger.autolog(level=1,message='License is already expired. Updating...',format=logFormat)
               info=updateHostLicense(content, vmwareLic["key"])
               #logger.autolog(level=1,message='New License Key is' + str(info))
               if info.total==0:
                   return {}, 'Failed: License Update failed'
               else:
                   return {},'Success'
            else:
                logger.autolog(level=1,message='License is stil valid, nothing to do...',format=logFormat)
                return {},'Success'
    else:
        logger.autolog(level=1,message='Unable to get host license...',format=logFormat)
        return {},'Failed: Unable to get host license...'







#############################
# Generic Functions
#############################

def waitForTasks(service_instance, tasks):
    """Given the service instance si and tasks, it returns after all the
   tasks are complete
   """
    property_collector = service_instance.content.propertyCollector
    task_list = [str(task) for task in tasks]
    # Create filter
    obj_specs = [vmodl.query.PropertyCollector.ObjectSpec(obj=task)
                 for task in tasks]
    property_spec = vmodl.query.PropertyCollector.PropertySpec(type=vim.Task,
                                                               pathSet=[],
                                                               all=True)
    filter_spec = vmodl.query.PropertyCollector.FilterSpec()
    filter_spec.objectSet = obj_specs
    filter_spec.propSet = [property_spec]
    pcfilter = property_collector.CreateFilter(filter_spec, True)
    try:
        version, state = None, None
        # Loop looking for updates till the state moves to a completed state.
        while len(task_list):
            update = property_collector.WaitForUpdates(version)
            for filter_set in update.filterSet:
                for obj_set in filter_set.objectSet:
                    task = obj_set.obj
                    for change in obj_set.changeSet:
                        if change.name == 'info':
                            state = change.val.state
                        elif change.name == 'info.state':
                            state = change.val
                        else:
                            continue

                        if not str(task) in task_list:
                            continue

                        if state == vim.TaskInfo.State.success:
                            # Remove task from taskList
                            task_list.remove(str(task))
                        elif state == vim.TaskInfo.State.error:
                            raise task.info.error
            # Move to next version
            version = update.version
    finally:
        if pcfilter:
            pcfilter.Destroy()




def connectVcenter(host='', user='', password='', port=443):
    context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
    context.verify_mode = ssl.CERT_NONE

    service_instance = connect.SmartConnect(host=host,
                                            user=user,
                                            pwd=password,
                                            port=int(port),
                                            sslContext=context)
    if not service_instance:
        logger.autolog(level=1,message="Unable to connect with the vCenter Server using the provided credentials")
        return False

    atexit.register(connect.Disconnect, service_instance)
    #content = service_instance.RetrieveContent()
    return service_instance


def getObj(content, vimType, name):
    obj = None
    try:
        container = content.viewManager.CreateContainerView(content.rootFolder, vimType, True)
    except Exception as e:
        logger.autolog(level=1,message="Error while retrieving container for  '"+ str(vimType) +"'from vCenter  : " + str(e))
        return False

    for c in container.view:
        #logger.autolog(level=1,message="Checking container view " + str(c.name))
        if c.name == name:
            logger.autolog(level=1,message="Got matching container view " + str(c.name))
            return c
    logger.autolog(level=1,message="Unable to find matching container view with name " + str(name))
    return False

#            obj = c
#            break
#    return obj


def getDatacenter(content, dcName):
    dc = getObj(content, [vim.Datacenter], dcName)
    if dc:
        return dc
    else:
        return False

def getCluster(content, clusterName):
    cluster = getObj(content, [vim.ClusterComputeResource], clusterName)
    if cluster:
        return cluster
    else:
        return False

def getHostLicenses(content):
    try:
        hostLicenses=[]
        hostLicense={}
        for license in content.licenseManager.licenses:
            hostLicense["name"]=license.name
            logger.autolog(level=1,message="License Name is  " + str(license.name))
            hostLicense["licenseKey"]=license.licenseKey
            #logger.autolog(level=1,message="License Key is  " + str(license.licenseKey))
            for property in license.properties:
                if property.key == "expirationDate":
                    hostLicense["expirationDate"]=property.value
                    logger.autolog(level=1,message="License Expiration Date is  " + str(property.value))
            hostLicenses.append(hostLicense)
        return hostLicenses 

    except Exception as e:
        logger.autolog(level=1,message="Error while getting License from host:   " + str(e))
        return False

def updateHostLicense(content, licenseKey):
    try:
        logger.autolog(level=1,message="Updating License...")
        return content.licenseManager.UpdateLicense(licenseKey)
    except Exception as e:
        logger.autolog(level=1,message="Error while updating License " + str(e))
        return False


def getVm(content, vmName):
    try:
        logger.autolog(level=1,message='Fetching VM ' + str(vmName))
        count=0
        max=6
        while(1):
            try:
                vm = getObj(content, [vim.VirtualMachine], vmName)
            except Exception as e:
                logger.autolog(level=1,message="Try ["+ str(count) + "] Error while fetching  VM "+ str(vmName) +" from vCenter  : " + str(e))
                vm=False

            if not vm:
                count=count+1
                if count==max:
                    logger.autolog(level=1, message="Unable to fetch VM  " + str(vmName))
                    return False
                time.sleep(10)
            else:
                break

        #vm = getObj(content, [vim.VirtualMachine], vmName)
        if vm:
            return vm
        else:
            return False
    except Exception as e:
        logger.autolog(level=1,message="Error while getting VM "+ str(vmName) +" from vCenter  : " + str(e))
        return False



def getHost(content, hostName):
    host = getObj(content, [vim.HostSystem], hostName)
    if host :
        return host
    else:
        return False



def getFolder(content, folderName):
    logger.autolog(level=1,message='Getting folder ' + folderName)
    try:
        folder = getObj(content, [vim.Folder], folderName)
    except Exception as e:
        logger.autolog(level=1,message="Error while getting Folder "+ str(folderName) +"   : " + str(e))
        return False

    if folder:
        return folder
    else:
        return False


def getVmIps(content,vmName,networkName,ipVersion='v4',logFormat=''):
    vm=getVm(content,vmName)

    result=[]

    try:
        logger.autolog(level=1,message='VM is !!!!!!!!!!!! : ' + str(vm),format=logFormat)
        #logger.autolog(level=1,message='VM.GUEST is : ' + str(vm.guest),format=logFormat)
        logger.autolog(level=1,message='VM.GUEST.NET is : ' + str(vm.guest.net),format=logFormat)
        for nic in vm.guest.net:
            if str(nic.network)==str(networkName):
                ips = nic.ipConfig.ipAddress
                logger.autolog(level=1,message='Found Interface on Network: ' + str(nic.network),format=logFormat)
                for ip in ips:

                    if ipVersion == 'v4':
                        pattern=re.compile("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$")
                        if pattern.match(str(ip.ipAddress)):
                            logger.autolog(level=1,message='Ip Address: ' + str(ip.ipAddress),format=logFormat)
                            result.append(str(ip.ipAddress))

                    if ipVersion == 'v6':
                        try:
                            socket.inet_pton(socket.AF_INET6,str(ip.ipAddress))
                            result.append(str(ip.ipAddress))
                        except Exception as e:
                            pass
        return result

    except Exception as e:
        logger.autolog(level=1,message="Error while getting IP address "+ str(vmName) +"   : " + str(e),format=logFormat)
        return False


def getVmMac(content,vmName,networkAdapter,logFormat=''):

    result=[]
    try:

        tries=0

        while(tries < 5):

            logger.autolog(level=1,message='Attempting to get MAC address from vm ' + str(vmName) + '. Will start in 10 seconds...' , format=logFormat)
            time.sleep(10)

            vm=getVm(content,vmName)
            logger.autolog(level=1,message='VM is !!!!!!!!!!!! : ' + str(vm),format=logFormat)
            #logger.autolog(level=1,message='VM.GUEST is : ' + str(vm.guest),format=logFormat)
            #logger.autolog(level=1,message='VM.GUEST.NET is : ' + str(vm.guest.net),format=logFormat)
            #logger.autolog(level=1,message='VM.SUMMARY is : ' + str(vm.summary),format=logFormat)
            #logger.autolog(level=1,message='VM.SUMMARY.RUNTIME.DEVICE is : ' + str(vm.summary.runtime.device),format=logFormat)
            #logger.autolog(level=1,message='VM.config is : ' + str(vm.config) , format=logFormat)


            try:
                for device in vm.config.hardware.device:
                    if isinstance(device, vim.vm.device.VirtualEthernetCard):
                        #logger.autolog(level=1,message='device is !!!!!!!!!!!!!!: ' + str(device.deviceInfo.summary), format=logFormat)
                        #logger.autolog(level=1,message='device is !!!!!!!!!!!!!!: ' + str(device), format=logFormat)
                        if str(device.deviceInfo.label)==str(networkAdapter):
                            logger.autolog(level=1,message='Found Adapter ' + str(networkAdapter) + ' in VM ' + str(vmName) + '. MAC address is: ' + str(device.macAddress), format=logFormat)
                            return device.macAddress
            except Exception as e:
                logger.autolog(level=1,message='Error while getting devices from vm.config.hardware.device.', format=logFormat)
                tries=tries +1
                logger.autolog(level=1,message='Try ' + str(tries) + ' of 5', format=logFormat)


            #try:
                #for nic in vm.guest.net:
                #    logger.autolog(level=1,message='Nic: ' + str(nic) , format=logFormat)
                #if str(nic.network)==str(networkName):
                #    mac = nic.macAddress
                #    logger.autolog(level=1,message='Found Interface on Network: ' + str(nic.network),format=logFormat)
                #    result.append(str(mac))
                #    #return result
            #except Exception as e:
            #    logger.autolog(level=1,message='Error while getting net from vm.guest.net.  Giving up... ', format=logFormat)
            #    return False


    except Exception as e:
        logger.autolog(level=1,message="Error while getting MAC address "+ str(vmName) +"   : " + str(e),format=logFormat)
        return False

    return False




def getFileFromVm(content,vmName,filePath,logFormat=''):
    logger.autolog(level=1,message="Tryung to get file " + str(filePath) + " from VM " + str(vmName) ,format=logFormat)


    vm=getVm(content,vmName)

    #vm = content.searchIndex.FindByUuid(None, vmName, True)
    tools_status = vm.guest.toolsStatus
    if (tools_status == 'toolsNotInstalled' or tools_status == 'toolsNotRunning'):
        logger.autolog(level=1,message="VMwareTools is either not running or not installed.",format=logFormat)
        return False

    creds = vim.vm.guest.NamePasswordAuthentication(username='root', password='C^rr0t3965')


    try:

        tries=0
        while(tries < 5):
            logger.autolog(level=1,message='Attempting to get file from vm ' + str(vmName) + '. Will start in 10 seconds...' , format=logFormat)
            time.sleep(10)

            fileInfo = content.guestOperationsManager.fileManager.InitiateFileTransferFromGuest(vm, creds, filePath)
                # When : host argument becomes https://*:443/guestFile?
                # Ref: https://github.com/vmware/pyvmomi/blob/master/docs/ \
                #            vim/vm/guest/FileManager.rst
                # Script fails in that case, saying URL has an invalid label.
                # By having hostname in place will take take care of this.

            logger.autolog(level=1,message="fileInfo: " + str(fileInfo),format=logFormat)
            url=fileInfo.url
            resp = requests.get(url,verify=False)
            txt=resp.text
            #logger.autolog(level=1,message="file: " + str(txt),format=logFormat)
            return txt

                #url = re.sub(r"^https://\*:", "https://"+str(args.host)+":", url)
                #resp = requests.put(url, data=args, verify=False)
                #if not resp.status_code == 200:
                #    print "Error while uploading file"
                #else:
                #    print "Successfully uploaded file"
        #except IOError, e:
        #    print e
        #except vmodl.MethodFault as error:
        #    print "Caught vmodl fault : " + error.msg
        #    return -1
    except Exception as e:
        tries=tries +1
        logger.autolog(level=1,message='Try ' + str(tries) + ' of 5', format=logFormat)

    logger.autolog(level=1,message="Error while getting file from  "+ str(vmName) +"   : " + str(e),format=logFormat)
    return False





#############################
# Functions for Host Managment
#############################

def removeHost(hostName='',host='', user='', password='', port=443):

    service_instance = connectVcenter(host=host, user=user, password=password)
    if not service_instance:
        logger.autolog(level=1,message='Not able to connect to vCenter. Failing...')
        return False

    content = content = service_instance.RetrieveContent()

    hostObjectView = content.viewManager.CreateContainerView(content.rootFolder,[vim.HostSystem],True)
    hosts = hostObjectView.view
    hostObjectView.Destroy()

    hostToRemove=[]
    for host in hosts:
        if host.name == hostName:
            hostToRemove = host
            logger.autolog(level=1,message='Found name matching Host ( '+hostName+'). Compute Resource is ' + str(hostToRemove))

    if not hostToRemove:
        logger.autolog(level=1,message='Could not find a name matching host. Nothing to delete...')
        return True

    compResourceObjectView = content.viewManager.CreateContainerView(content.rootFolder,[vim.ComputeResource],True)
    computeResources = compResourceObjectView.view
    compResourceObjectView.Destroy()

    for computeResource in computeResources:
        if computeResource.host[0] == hostToRemove:
            logger.autolog(level=1,message=" Found Compute Resoruce. About to delete " + str(hostToRemove))
            try:
                computeResource.Destroy()
                logger.autolog(level=1,message="Compute Resoruce deleted.  " + str(hostToRemove))
                return True
            except Exception as e:
                logger.autolog(level=1,message="Error while removing host "+ str(computeResource.host[0]) +" from vCenter  : " + str(e))
                return False
    logger.autolog(level=1,message='Could not find compute resource for deletion. Could not delete')
    return False



##################################
# Functions for Folder Management
##################################

def createVmFolder(content, dcName, folderName):
    logger.autolog(level=1,message='About to create folder ' + str(folderName))
    dc=getDatacenter(content, dcName)
    if not dc:
        logger.autolog(level=1,message='Unable to get Datacenter with name '+ dcName +' from vCenter. Aborting..')
        return False

    folder= getFolder(content, folderName)
    if folder:
       logger.autolog(level=1,message="Folder already exists...")
       return folder
    try:
        dc.vmFolder.CreateFolder(folderName)
        logger.autolog(level=1,message='Folder ' + str(folderName) + ' created...')
        folder=getFolder(content, folderName)
        return folder

    except Exception as e:
        logger.autolog(level=1,message="Error while creating VM Folder "+ str(folderName) +"   : " + str(e))
        return False


def createVmSubfolder(content, dcName,folderName ,folderPath=['root','sessions']):
    logger.autolog(level=1,message='About to create subfolder ' + str(folderName))

    subFolder=getVmSubfolder(content,dcName, folderPath=folderPath)

    if subFolder:
        try:
            subFolder.CreateFolder(folderName)
            logger.autolog(level=1,message='Folder ' + str(folderName) + ' created...')
            folderPath.append(folderName)
            folder=getVmSubfolder(content,dcName, folderPath=folderPath)
            return folder
        except Exception as e:
            if "already exists" in str(e):
                logger.autolog(level=1,message='This folder already exists, no need to create again...')
                return getVmSubfolder(content,dcName, folderPath=[subFolder,folderName])
            else:
                logger.autolog(level=1,message="Error while creating VM Folder "+ str(folderName) +"   : " + str(e))
                return False
    else:
        return False



def getVmSubfolder(content, dcName, folderPath=['root','sessions']):

    logger.autolog(level=1,message='About to get VM subfolder with path ' + str(folderPath))
    #logger.autolog(level=1,message='folderPath[0]:' + str(folderPath[0]))
    #logger.autolog(level=1,message='Type of folderPath[0]:' + str(type(folderPath[0])))

    if str(folderPath[0])=='root':
        logger.autolog(level=1,message='Got root, getting root folder...')
        dc=getDatacenter(content, dcName)
        if not dc:
            logger.autolog(level=1,message='Unable to get Datacenter with name '+ dcName +' from vCenter. Aborting..')
            return False
        root=dc.vmFolder
        folderPath[0]=root
        return getVmSubfolder(content, dcName, folderPath=folderPath)

    if isinstance(folderPath[0], vim.Folder):
        logger.autolog(level=1,message='Got Folder ' + str(folderPath[0].name))
        try:
            nextFolder=folderPath[1]
            logger.autolog(level=1,message='Trying to find  ' + str(folderPath[1]))
        except Exception as e:
            logger.autolog(level=1,message="No more subfolders to go through... ")
            logger.autolog(level=1,message="Returning folder: " + str(folderPath[0]))
            return folderPath[0]

        childEntity=folderPath[0].childEntity
        for child in childEntity:
            if child.name == nextFolder:
                folderPath.pop(0)
                folderPath[0]=child
                return getVmSubfolder(content, dcName, folderPath=folderPath)
        logger.autolog(level=1,message='Unable to find folder ' + str(folderPath[1]) + 'in folder ' + str(folderPath[0].name) )
        return False



def setFolderPermissions(content,folder, userName,groupName,logFormat=''):
        logger.autolog(level=1,message='About to assign permissions to folder ' + str(folder) + 'for user ' + str(userName)  + ' in Group ' + str(groupName) )

        myPermissions=[]

        if userName:
            logger.autolog(level=1,message='Will add user ' + str(userName) + ' to folder ' +  str(folder), format=logFormat)
            myPermission= vim.AuthorizationManager.Permission()
            myPermission.entity=folder
            myPermission.principal = 'DCV\\' +  userName #'DCV\\v561user1'
            myPermission.group= False
            myPermission.roleId= -1
            myPermission.propagate= True
            myPermissions.append(myPermission)

        if groupName:
            logger.autolog(level=1,message='Will add groupname ' + str(groupName) + ' to folder ' +  str(folder), format=logFormat)
            myPermission= vim.AuthorizationManager.Permission()
            myPermission.entity=folder
            myPermission.principal = 'DCV\\'+ groupName   # 'DCV\\Hyperflex Hardware Demo Users'
            myPermission.group= True
            myPermission.roleId= -2
            myPermission.propagate= True
            myPermissions.append(myPermission)

        try:
            content.authorizationManager.SetEntityPermissions(folder,myPermissions)
            logger.autolog(level=1,message='Permissions assgined to folder ' + str(folder))
            return True
        except Exception as e:
            logger.autolog(level=1,message="Error while assigning permissions to  Folder "+ str(folder) +"   : " + str(e))
            return False


def destroyVmFolder(content, folderName , dcName='HybridCloud'):

    if isinstance(folderName , str):
        folder=getFolder(content, folderName)

    if isinstance(folderName , list):
        folder=getVmSubfolder(content, dcName, folderPath=folderName)

    if not folder:
       logger.autolog(level=1,message="Folder does not exist...")
       return True
    logger.autolog(level=1,message='About to delete folder ' + str(folderName) )
    try:
        folder.Destroy()
        return True
    except Exception as e:
        logger.autolog(level=1,message="Error while deleting VM Folder "+ str(folderName) +"   : " + str(e))
        return False


def createHostFolder(content, dcName, folderName):
    dc=getDatacenter(content, dcName)
    if not dc:
        logger.autolog(level=1,message='Unable to get Datacenter with name '+ dcName +' from vCenter. Aborting..')
        return False

    if getFolder(content, folderName):
       logger.autolog(level=1,message="Folder already exists...")
       return True

    try:
        dc.hostFolder.CreateFolder(folderName)
        return True
    except Exception as e:
        logger.autolog(level=1,message="Error while creating VM Folder "+ str(folderName) +"   : " + str(e))
        return False



def toggleVmPowerStateInFolder(folderName='', dcName='', host='', user='', password='', port=443):
    si=connectVcenter(host=host, user=user, password=password)
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..')
        return False

    content = si.RetrieveContent()
    vms = getAllVmsInFolder(content,folderName)
    for vm in vms:
        if vm.runtime.powerState == 'poweredOff':
            powerOnVms([vm])
        if vm.runtime.powerState == 'poweredOn':
            shutdownVms([vm])



def cleanupVmSandbox(folderName='VM-Sandbox', dcName='', host='', user='', password='', port=443):
    import datetime
    import pickle

    si=connectVcenter(host=host, user=user, password=password)
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..')
        return False

    content = si.RetrieveContent()
    vms = getAllVmsInFolder(content,folderName)
    now = datetime.datetime.now()
    fiveDaysAgo = now - datetime.timedelta(days=5)
    fiveHoursAgo = now - datetime.timedelta(hours=5)
    fiveMinutesAgo = now - datetime.timedelta(minutes=5)
    oneDayAgo = now - datetime.timedelta(days=1)
    oneHourAgo = now - datetime.timedelta(hours=1)
    oneMinuteAgo = now - datetime.timedelta(minutes=1)
    thirtyMinutesAgo = now - datetime.timedelta(minutes=30)

    expiryTime = thirtyMinutesAgo

    fiveHoursAgo = now - datetime.timedelta(hours=5)
    vmsInLog=[]
    try:
        with open('/root/hyperflex/vmlog','rb') as f:
            vmsInLog = pickle.load(f)
    except Exception as e:
        logger.autolog(level=1,message="Error while opening vmlog file. Will create a new one  : " + str(e))

    vmsToBeDelteted = []

    for vm in vms:
        found = False
        logger.autolog(level=1,message="Checking Vm " + vm.summary.config.name)
        if vm.summary.config.name not in ['Win10-Template-Sandbox', 'Win8-Template-Sandbox']:
            for vmInLog in vmsInLog:
                if vmInLog['vm']== vm.summary.config.instanceUuid:
                    found = True
                    if (vmInLog['creationDate'] < expiryTime):
                        logger.autolog(level=1,message='VM with name ' + vm.summary.config.name + ' was created more than 5 hours ago, deleting...')
                        #logger.autolog(level=1,message=vmInLog['creationDate'])
                        #logger.autolog(level=1,message=expiryTime)
                        vmsToBeDelteted.append(vm)
                        vmsInLog.remove(vmInLog)
                    else:
                        logger.autolog(level=1,message='VM with name ' + vm.summary.config.name + ' was created less than 5 hours ago, leaving VM be...')
                        #logger.autolog(level=1,message=vmInLog['creationDate'])
                        #logger.autolog(level=1,message=expiryTime)
            if not found:
                logger.autolog(level=1,message='VM with name ' + vm.summary.config.name + ' is new. Adding to VmLogs')
                vmsInLog.append({'vm': vm.summary.config.instanceUuid , 'creationDate' : now })

    logger.autolog(level=1,message=vmsInLog)
    with open('/root/hyperflex/vmlog','wb') as f:
        pickle.dump(vmsInLog,f)

    return destroyVms(si,vmsToBeDelteted)






####################################
# Functions for Snapshot Management
####################################


def listSnapshotsRecursively(snapshots):
    snapshot_data = []
    snap_text = ""
    for snapshot in snapshots:
        snap_text = "Name: %s; Description: %s; CreateTime: %s; State: %s" % (snapshot.name, snapshot.description,snapshot.createTime, snapshot.state)
        snapshot_data.append(snap_text)
        snapshot_data = snapshot_data + listSnapshotsRecursively(snapshot.childSnapshotList)
    return snapshot_data



def getSnapshotsByNameRecursively(snapshots, snapname):
    snap_obj = []
    for snapshot in snapshots:
        if snapshot.name == snapname:
            snap_obj.append(snapshot)
        else:
            snap_obj = snap_obj + getSnapshotsByNameRecursively(snapshot.childSnapshotList, snapname)
    return snap_obj


def getCurrentSnapshot(snapshots, snapob):
    snap_obj = []
    for snapshot in snapshots:
        if snapshot.snapshot == snapob:
            snap_obj.append(snapshot)
        snap_obj = snap_obj + getCurrentSnapshot(snapshot.childSnapshotList, snapob)
    return snap_obj


def vmSnapshot(si, vms,snapshotName,operation='revert'):

    content = si.RetrieveContent()

    #si = None
    #print("Trying to connect to VCENTER SERVER . . .")
    #context = None
    #if inputs['ignore_ssl'] and hasattr(ssl, "_create_unverified_context"):
    #    context = ssl._create_unverified_context()
    #
    #si = connect.Connect(inputs['vcenter_ip'], 443,
    #                     inputs['vcenter_user'], inputs[
    #                         'vcenter_password'],
    #                     sslContext=context)

    #atexit.register(Disconnect, si)
    #print("Connected to VCENTER SERVER !")
    #content = si.RetrieveContent()
    #print content


    #operation = inputs['operation']
    #print operation

    for vm in vms:
        vm_name=vm.name
        #print vm_name
        vm=getVm(content, vm_name)
        #vm = get_obj(content, [vim.VirtualMachine], vm_name)
        #print vm
        if not vm:
            #print("Virtual Machine %s doesn't exists" % vm_name)
            return False
            #sys.exit()

        if operation != 'create' and vm.snapshot is None:
            #print("Virtual Machine %s doesn't have any snapshots" % vm.name)
            return False
            #sys.exit()

        if operation == 'create':
            snapshot_name = snapshotName
            description = "Snapshot created by indemo automation controller"
            dumpMemory = False
            quiesce = False
            #print("Creating snapshot %s for virtual machine %s" % (snapshot_name, vm.name))
            waitForTasks(si,[vm.CreateSnapshot(snapshot_name, description, dumpMemory, quiesce)])

        elif operation in ['remove', 'revert']:
            snapshot_name = snapshotName
            snap_obj = getSnapshotsByNameRecursively(vm.snapshot.rootSnapshotList, snapshot_name)
            # if len(snap_obj) is 0; then no snapshots with specified name
            if len(snap_obj) == 1:
                snap_obj = snap_obj[0].snapshot
                if operation == 'remove':
                    #print("Removing snapshot %s" % snapshot_name)
                    waitForTasks(si,[snap_obj.RemoveSnapshot_Task(True)])
                    #return True
                else:
                    try:
                        logger.autolog(level=1,message="Reverting to snapshot %s" % snapshot_name)
                        waitForTasks(si,[snap_obj.RevertToSnapshot_Task()])
                        powerOnVms([vm])
                        #return True
                    except Exception as e:
                        logger.autolog(level=1,message="Error while reverting snapshot : " + str(e))
                        return False
            else:
                #print("No snapshots found with name: %s on VM: %s" % (snapshot_name, vm.name))
                return False

        elif operation == 'list_all':
            #print("Display list of snapshots on virtual machine %s" % vm.name)
            snapshot_paths = list_snapshots_recursively(vm.snapshot.rootSnapshotList)
            for snapshot in snapshot_paths:
                #print(snapshot)
                pass
            return snapshot_paths

        elif operation == 'list_current':
            current_snapref = vm.snapshot.currentSnapshot
            current_snap_obj = get_current_snap_obj(vm.snapshot.rootSnapshotList, current_snapref)
            current_snapshot = "Name: %s; Description: %s; CreateTime: %s; State: %s" % (current_snap_obj[0].name,current_snap_obj[0].description,current_snap_obj[0].createTime,current_snap_obj[0].state)
            #print("Virtual machine %s current snapshot is:" % vm.name)
            #print(current_snapshot)
            return current_snapshot

        elif operation == 'remove_all':
            #print("Removing all snapshots for virtual machine %s" % vm.name)
            waitForTasks(si,[vm.RemoveAllSnapshots()])
            #return True

        else:
            #print("Specify operation in create/remove/revert/list_all/list_current/remove_all")
            return False





    return True





#############################
# Functions for VM Management
#############################

def powerOnVms(vms):
    status= True
    for vm in vms:
        if vm.runtime.powerState == 'poweredOff':
            logger.autolog(level=1,message='Powering up VM ' + vm.config.name)
            try:
                vm.PowerOn()
            except Exception as e:
                logger.autolog(level=1,message="Error while powering on VM  "+ str(vm.config.name) +"   : " + str(e))
                status = False
    return status



def rebootVms(vms):
    status= True
    for vm in vms:
        logger.autolog(level=1,message='Rebooting VM ' + vm.config.name)
        try:
            vm.RebootGuest()
        except Exception as e:
            logger.autolog(level=1,message="Error while rebooting VM  "+ str(vm.config.name) +"   : " + str(e))
            status = False
    return status




def fixVmNic(vm):

    try:
        logger.autolog(level=1,message="Starting fixVmNic")
        logger.autolog(level=1,message="Trying to force connect nics on   "+ str(vm.config.name) )

        nics=getVmNics(vm)
        if nics:
            for nic in nics:
                logger.autolog(level=1,message="Fetching Nic " + str(nic['name'])+ " in VM " +str(vm.config.name) )
                valid,connectionState = getVmNicConnectionState(vm, nic['name'])
                if valid:
                    if not connectionState :
                        logger.autolog(level=1,message="Nic is disconnected. Trying to force connect nic " + str(nic['name'])+ " in VM " +str(vm.config.name) )
                        setVmNicConnectionState(vm, nic['name'], 'connect')
                    else:
                        logger.autolog(level=1,message="Nic is connected, nothing to do for " + str(nic['name'])+ " in VM " +str(vm.config.name) )
            return True
        else:
            logger.autolog(level=1,message="Unable to get nic for vm :  " + str(vm))
            return False

    except Exception as e:
        logger.autolog(level=1,message="Error while fixing VM nic state : " + str(e))
        return False




def resetVm(vm):
    status= True

    logger.autolog(level=1,message='Rebooting VM ' + str(vm.config.name))
    try:

        if vm.runtime.powerState == vim.VirtualMachinePowerState.poweredOn:
            task = vm.PowerOff()
            while task.info.state not in [vim.TaskInfo.State.success,vim.TaskInfo.State.error]:
                time.sleep(1)
            #print "power is off."
        if vm.runtime.powerState != vim.VirtualMachinePowerState.poweredOn:
        # now we get to work... calling the vSphere API generates a task...
            task = vm.PowerOn()
            while task.info.state not in [vim.TaskInfo.State.success, vim.TaskInfo.State.error]:
                time.sleep(1)
        if task.info.state ==  vim.TaskInfo.State.success:
            return True
        else:
            return False

    except Exception as e:
        logger.autolog(level=1,message="Error while reseting the  VM  "+ str(vm) +"   : " + str(e))
        status = False



def shutdownVms(vms):
    status= True
    for vm in vms:
        if vm.runtime.powerState == 'poweredOn':
            logger.autolog(level=1,message='Shutting down VM ' + vm.config.name)
            try:
                vm.ShutdownGuest()
            except Exception as e:
                logger.autolog(level=1,message="Error while shutting down VM  "+ str(vm.config.name) +"   : " + str(e))
                status = False
    return status




def getVmNics(vm):

    nics=[]

    for dev in vm.config.hardware.device:
        if isinstance(dev, vim.vm.device.VirtualEthernetCard):
            dev_backing = dev.backing
            portGroup = None
            vlanId = None
            vSwitch = None
            if hasattr(dev_backing, 'port'):
                portGroupKey = dev.backing.port.portgroupKey
                dvsUuid = dev.backing.port.switchUuid
                try:
                    dvs = content.dvSwitchManager.QueryDvsByUuid(dvsUuid)
                except:
                    portGroup = "** Error: DVS not found **"
                    vlanId = "NA"
                    vSwitch = "NA"
                else:
                    pgObj = dvs.LookupDvPortGroup(portGroupKey)
                    portGroup = pgObj.config.name
                    vlanId = str(pgObj.config.defaultPortConfig.vlan.vlanId)
                    vSwitch = str(dvs.name)
            #else:
            #    portGroup = dev.backing.network.name
            #    vmHost = vm.runtime.host
            #    # global variable hosts is a list, not a dict
            #    host_pos = hosts.index(vmHost)
            #    viewHost = hosts[host_pos]
            #    # global variable hostPgDict stores portgroups per host
            #    pgs = hostPgDict[viewHost]
            #    for p in pgs:
            #        if portGroup in p.key:
            #            vlanId = str(p.spec.vlanId)
            #            vSwitch = str(p.spec.vswitchName)

            if portGroup is None:
                portGroup = 'NA'
            if vlanId is None:
                vlanId = 'NA'
            if vSwitch is None:
                vSwitch = 'NA'
            nic={'name':dev.deviceInfo.label,'mac':dev.macAddress, 'vSwitch' : vSwitch , 'portGroup': portGroup }
            #print('\t' + dev.deviceInfo.label + '->' + dev.macAddress + ' @ ' + vSwitch + '->' + portGroup + ' (VLAN ' + vlanId + ')')
            logger.autolog(level=1,message="Nic: " + str(nic))
            nics.append(nic)

    return nics





def getVmNicConnectionState(vm_obj, nicName):

    nic_label = nicName

    virtual_nic_device = None

    for dev in vm_obj.config.hardware.device:
        if isinstance(dev, vim.vm.device.VirtualEthernetCard) and dev.deviceInfo.label == nic_label:
            virtual_nic_device = dev
            try:
                return True, virtual_nic_device.connectable.connected
            except Exception as e:
                logger.autolog(level=1, message="Unable to get connection state  for : " + str(nicName) + '---' + str(e), format=logFormat)
                return False, False


    if not virtual_nic_device:
        #raise RuntimeError('Virtual {} could not be found.'.format(nic_label))
        logger.autolog(level=1,message='Virtual {} could not be found.'.format(nic_label))
        return False, False


def setVmNicConnectionState(vm_obj, nicName, new_nic_state):

    try:
        nic_label = nicName
        virtual_nic_device = None

        for dev in vm_obj.config.hardware.device:
            if isinstance(dev, vim.vm.device.VirtualEthernetCard) and dev.deviceInfo.label == nic_label:
                virtual_nic_device = dev
        if not virtual_nic_device:
            #raise RuntimeError('Virtual {} could not be found.'.format(nic_label))
            logger.autolog(level=1,message='Virtual {} could not be found.'.format(nic_label))
            return False

        virtual_nic_spec = vim.vm.device.VirtualDeviceSpec()

        virtual_nic_spec.operation = vim.vm.device.VirtualDeviceSpec.Operation.remove if new_nic_state == 'delete' else vim.vm.device.VirtualDeviceSpec.Operation.edit

        virtual_nic_spec.device = virtual_nic_device
        virtual_nic_spec.device.key = virtual_nic_device.key
        virtual_nic_spec.device.macAddress = virtual_nic_device.macAddress
        virtual_nic_spec.device.backing = virtual_nic_device.backing
        virtual_nic_spec.device.wakeOnLanEnabled = virtual_nic_device.wakeOnLanEnabled

        connectable = vim.vm.device.VirtualDevice.ConnectInfo()

        if new_nic_state == 'connect':
            connectable.connected = True
            connectable.startConnected = True
        elif new_nic_state == 'disconnect':
            connectable.connected = False
            connectable.startConnected = False
        else:
            connectable = virtual_nic_device.connectable

        virtual_nic_spec.device.connectable = connectable
        dev_changes = []
        dev_changes.append(virtual_nic_spec)
        spec = vim.vm.ConfigSpec()
        spec.deviceChange = dev_changes
        task = vm_obj.ReconfigVM_Task(spec=spec)
        #tasks.wait_for_tasks(si, [task])
        return True

    except Exception as e:
        logger.autolog(level=1,message='Error while changing conneciotn state of nic ' + str(nicName) + ' on VM ' + str(vm_obj.config.name), format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return False












def vmAdaptersToNetwork(service_instance,vmName='', networkNames=[], logFormat=''):

    adapterName="Network adapter "
    adapterNumber=1

    try:
        content = service_instance.RetrieveContent()


        logger.autolog(level=1,message='Fetching VM  for function vmAdaptersToNetwork ' + str(vmName) ,format=logFormat)
        count=0
        max=2
        while(1):
            vm = getVm(content,vmName)
            if not vm:
                count=count+1
                if count==max:
                    logger.autolog(level=1, message="Unable to fetch VM for function vmAdaptersToNetwork  " + str(vmName), format=logFormat)
                    return False
                time.sleep(10)
            else:
                break


        deviceChange = []
        for networkName in networkNames:
            adaptName=adapterName + str(adapterNumber)
            for device in vm.config.hardware.device:
                if isinstance(device, vim.vm.device.VirtualEthernetCard):
                    if device.deviceInfo.label == adaptName:
                        logger.autolog(level=1, message="Changing Adapter with name " + str(adaptName) + " on vm with name " + str(vmName) + " connecting to network " + str(networkName), format=logFormat)
                        nicspec = vim.vm.device.VirtualDeviceSpec()
                        nicspec.operation = vim.vm.device.VirtualDeviceSpec.Operation.edit
                        nicspec.device = device
                        nicspec.device.wakeOnLanEnabled = True

                        count=0
                        logger.autolog(level=1,message='Fetching VM  for function vmAdaptersToNetwork ' + str(vmName) ,format=logFormat)
                        max=6
                        while(count<max):
                            try:
                                logger.autolog(level=1, message="Trying DVS", format=logFormat)
                                network = getObj(content,[vim.dvs.DistributedVirtualPortgroup],networkName)
                                dvs_port_connection = vim.dvs.PortConnection()
                                dvs_port_connection.portgroupKey = network.key
                                dvs_port_connection.switchUuid = network.config.distributedVirtualSwitch.uuid
                                nicspec.device.backing = vim.vm.device.VirtualEthernetCard.DistributedVirtualPortBackingInfo()
                                nicspec.device.backing.port = dvs_port_connection
                                logger.autolog(level=1, message="DVS worked!", format=logFormat)
                                count=max
                            except Exception as e:
                                try:
                                    logger.autolog(level=1, message="DVS failed: " + str(e) + " Now Trying VS", format=logFormat)
                                    nicspec.device.backing = vim.vm.device.VirtualEthernetCard.NetworkBackingInfo()
                                    nicspec.device.backing.network = getObj(content, [vim.Network], networkName)
                                    nicspec.device.backing.deviceName = networkName
                                    logger.autolog(level=1, message="VS worked!", format=logFormat)
                                    count=max
                                except Exception as e:
                                   logger.autolog(level=1, message="VS failed: " + str(e) + ".", format=logFormat)
                                   count = count + 1
                                   if count==max:
                                       logger.autolog(level=1, message="Network confguration failed for vm : " + str(vmName) + " , not cloning.", format=logFormat)
                                       return False
                                   logger.autolog(level=1, message="Waiting 10 seconds before trying again...", format=logFormat)
                                   time.sleep(10)
                                   #break

                        nicspec.device.connectable = vim.vm.device.VirtualDevice.ConnectInfo()
                        nicspec.device.connectable.startConnected = True
                        nicspec.device.connectable.connected = True
                        nicspec.device.connectable.allowGuestControl = True
                        deviceChange.append(nicspec)
            adapterNumber=adapterNumber+1

        configSpec = vim.vm.ConfigSpec(deviceChange=deviceChange)
        task = vm.ReconfigVM_Task(configSpec)
        waitForTasks(service_instance, [task])
        logger.autolog(level=1, message="Successfully changed network for vm " + str(vmName), format=logFormat)
        return True

    except vmodl.MethodFault as error:
        logger.autolog(level=1, message="Caught vmodl fault : " + error.msg, format=logFormat)
        return -1

    #return 0



def cloneVmFromTemplate(si, template='', dcName='dCloud-HX-DC',destFolderName='', dsName='', vmName='', clusterName='dCloud-HX-Cluster', host='', networkNames=[],vmConfig=[],logFormat=''):

    content = si.RetrieveContent()
    logger.autolog(level=1,message='About to clone VM with name ' + str(vmName) + ' from template ' + str(template), format=logFormat)


    #Get Folder
    if isinstance(destFolderName,list):
        destFolder = getVmSubfolder(content, dcName, folderPath=destFolderName)
    if isinstance(destFolderName,str):
        destFolder = getFolder(content, destFolderName)


    ds=getDatastoreByName(content, dsName=dsName)
    cluster = getCluster(content,clusterName)
    resourcePool = cluster.resourcePool

    #Set  relospec
    logger.autolog(level=1,message='Preparing reloSpec to clone VM with name ' + str(vmName) + ' from template ' + str(template),format=logFormat)
    try:
        reloSpec = vim.vm.RelocateSpec()
        reloSpec.datastore = ds
        if host !="":
            destHost = getHost(content , host)
            reloSpec.host=destHost
        else:
            reloSpec.pool=resourcePool
    except Exception as e:
        logger.autolog(level=1,message='Error while preparing reloSpec to clone VM with name ' + str(vmName) + ' from template ' + str(template), format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return False


    #VM config spec
    if vmConfig !=[]:
        logger.autolog(level=1,message='Got vmConfig: ' + str(vmConfig),format=logFormat)
        try:
            logger.autolog(level=1,message='Preparing vmConf to clone VM with name ' + str(vmName) + ' from template ' + str(template),format=logFormat)
            vmConf = vim.vm.ConfigSpec()
            try:
                vmConf.numCPUs = vmConfig['numCPUs']
            except:
                pass
            try:
                vmConf.memoryMB = vmConfig['memoryMB']
            except:
                pass
            try:
                vmConf.uuid = vmConfig['uuid']
            except:
                pass
            #vmConf.cpuHotAddEnabled = True
            #vmConf.memoryHotAddEnabled = True
            #vmConf.deviceChange = devices

            try:
                vAppDict=dict([(p.id, p.value) for p in template.config.vAppConfig.property])
                logger.autolog(level=1,message='Template vApp Options: ' + str(vAppDict),format=logFormat)
                logger.autolog(level=1,message='Clonned VM-vApp Options: ' + str(vmConfig['vAppOptions']),format=logFormat)

                vmvApp = template.config.vAppConfig
                configSpec = vim.vApp.VmConfigSpec()
                for prop in vmvApp.property:
                    logger.autolog(level=1,message='Checking if there is value for property: ' + str(prop.id) ,format=logFormat)
                    for vAppOption in vmConfig['vAppOptions']:
                        #logger.autolog(level=1,message='Checking if we have new  value for property: ' + str(vAppOption) ,format=logFormat)
                        if prop.id == vAppOption:
                            propSpec = vim.vApp.PropertySpec()
                            propSpec.operation = 'edit'
                            propSpec.info = prop
                            propSpec.info.value = str(vmConfig['vAppOptions'][prop.id])
                            logger.autolog(level=1,message='vApp Options: ' + str(prop) +' = ' + str(vmConfig['vAppOptions'][prop.id]) ,format=logFormat)
                            configSpec.property.append(propSpec)
                vmConf.vAppConfig = configSpec
            except Exception as e:
                logger.autolog(level=1,message='Something went wrong while checking the vApp options ' + str(e),format=logFormat)
                pass



        except Exception as e:
            logger.autolog(level=1,message='Eror while preparing vmConf to clone VM with name ' + str(vmName) + ' from template ' + str(template), format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return False


   #Clone spec
    try:
        logger.autolog(level=1,message='Preparing cloneSpec to clone VM with name ' + str(vmName) + ' from template ' + str(template),format=logFormat)
        cloneSpec = vim.vm.CloneSpec()
        cloneSpec.location = reloSpec
        cloneSpec.powerOn = False
        cloneSpec.config = vmConf
    except Exception as e:
        logger.autolog(level=1,message='Eror while preparing cloneSpec  to clone VM with name ' + str(vmName) + ' from template ' + str(template), format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return False


    #Start Cloning...
    try:
        logger.autolog(level=1,message='Clone about to start....', format=logFormat)
        task = [template.Clone(folder=destFolder, name=vmName, spec=cloneSpec)]
        logger.autolog(level=1, message= "Clone initiated...", format=logFormat)
        sys.stdout.flush()
        waitForTasks(si,task)
        logger.autolog(level=1, message="Virtual Machine "+ vmName +" has been cloned successfully", format=logFormat)

    except Exception as e:
        if "already exists" in str(e):
            logger.autolog(level=1,message='This VM already exists, no need to clone again...', format=logFormat)
            pass
        else:
            logger.autolog(level=1,message='Error while cloning VM from template', format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return False

    #Configure Network
    try:
        logger.autolog(level=1,message='About to configure VM network interfaces....', format=logFormat)
        if not vmAdaptersToNetwork(si,vmName=vmName, networkNames=networkNames, logFormat=logFormat):
            logger.autolog(level=1,message='Unable to configure network interfaces in VM ', format=logFormat)
            return False
    except Exception as e:
        logger.autolog(level=1,message='Error while configuring netowrk interfaces in VM ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return False

    #PowerOn
    try:
        logger.autolog(level=1,message='About to power on  VM...', format=logFormat)
        vmToPowerOn=getVm(content,vmName)
        if vmToPowerOn:
            if powerOnVms([vmToPowerOn]):
                return True
            else:
                logger.autolog(level=1,message='Unable to POwer On VM  ', format=logFormat)
                return False
        else:
            logger.autolog(level=1,message='Unable to get VM ', format=logFormat)
            return False

    except Exception as e:
        logger.autolog(level=1,message='Error while Powering On VM ', format=logFormat)
        logger.autolog(level=1,message=str(e), format=logFormat)
        return False




def destroyVms(si, vms):

    status = True
    for vm in vms:
        try:
            logger.autolog(level=1,message='Deleting VM ' + str(vm.summary.config.name))
            if vm.runtime.powerState == "poweredOn":
                TASK = vm.PowerOffVM_Task()
                waitForTasks(si, [TASK])
                logger.autolog(level=1,message=(TASK.info.state))
            TASK = vm.Destroy_Task()
            waitForTasks(si, [TASK])
            logger.autolog(level=1,message="vm  deleted")
        except Exception as e:
            logger.autolog(level=1,message="Error while deleting VM  : " + str(e))
            status = False

    return status




def deleteVms(service_instance, vmsToDelete=[]):
    logger.autolog(level=1,message='Deleting VMs in list ' + str(vmsToDelete))
    for vm in vmsToDelete:
        logger.autolog(level=1,message='Searching for VM with name ' + vm['name'] + ' and UUID ' + vm['biosUuid'] )
        VM = service_instance.content.searchIndex.FindByUuid(None, vm['biosUuid'],True)
        if VM is None:
            logger.autolog ("Unable to locate VirtualMachine with the given details")
            return False
        logger.autolog(level=1,message="Found: {0}".format(VM.name))
        logger.autolog(level=1,message="The current powerState is: {0}".format(VM.runtime.powerState))
        try:
            if format(VM.runtime.powerState) == "poweredOn":
                logger.autolog(level=1,message="Attempting to power off {0}".format(VM.name))
                TASK = VM.PowerOffVM_Task()
                waitForTasks(service_instance, [TASK])
                logger.autolog(level=1,message="{0}".format(TASK.info.state))

            logger.autolog(level=1,message="Deleting VM from vSphere.")
            TASK = VM.Destroy_Task()
            waitForTasks(service_instance, [TASK])
            logger.autolog(level=1,message="Deletion Done.")
        except Exception as e:
            logger.autolog(level=1,message="Error while deleting VM  : " + str(e))
            return False

    return True

def putVmInfoInDic(virtualMachine):

    summary = virtualMachine.summary
    vm= {'name': summary.config.name, \
         'template': summary.config.template, \
         'path': summary.config.vmPathName, \
         'guestFullName': summary.config.guestFullName, \
         'instanceUuid' : summary.config.instanceUuid, \
         'biosUuid' : summary.config.uuid, \
         'annotation': summary.config.annotation, \
         'state'     : summary.runtime.powerState, \
         'ipAddress' : summary.guest.ipAddress, \
         'toolsVersion' : summary.guest.toolsStatus}

    return vm

def getAllVms(service_instance):

    content = service_instance.RetrieveContent()

    container = content.rootFolder  # starting point to look into
    viewType = [vim.VirtualMachine]  # object types to look for
    recursive = True  # whether we should look into it recursively
    containerView = content.viewManager.CreateContainerView(container, viewType, recursive)

    vms=[]
    children = containerView.view
    for child in children:
        #print_vm_info(child)
        vms.append(putVmInfoInDic(child))

    return vms

def getAllVmsInFolder(content, folderName, dcName='HybridCloud'):

    if isinstance(folderName, str):
        logger.autolog(level=1,message='About to get list of VMs in folder ' + str(folderName))
        folder = getFolder(content,folderName)

    if isinstance(folderName, list):
        logger.autolog(level=1,message='About to get list of VMs in folder ' + str(folderName))
        folder = getVmSubfolder(content,dcName, folderPath=folderName)

    if not folder:
        logger.autolog(level=1,message='Not able to get folder. Aborting...')
        return False
    logger.autolog(level=1,message='Getting the list of Vms in folder ' + str(folderName))
    #logger.autolog(level=1,message=folder.childEntity)
    return folder.childEntity



def getVmNameWith(service_instance, vmNameWith=''):

    logger.autolog(level=1,message='Searching for Vms that contain ' + vmNameWith + ' on its name')

    vms=getAllVms(service_instance)

    vmsToDelete=[]
    for vm in vms:
        try:
            searchThisString=str(vm['name'])
            if (searchThisString.find(vmNameWith) != -1):
                logger.autolog(level=1,message='Found VM with name ' + searchThisString )
                vmsToDelete.append(vm)
        except Exception as e:
            logger.autolog(level=1,message='Error while comparing Vm names')
            logger.autolog(level=1,message=e)
            return False

    return vmsToDelete


def deleteVmNameWith(vmNameWith='', host='', user='', password='', port=443):
    logger.autolog(level=1,message='Starting VM Deletion Process of VMs with names containing ' + vmNameWith)

    service_instance = connectVcenter(host=host, user=user, password=password)
    if not service_instance:
        logginginfo('Not able to connect to vCenter. Failing...')
        return False

    vmsToDelete = getVmNameWith(service_instance, vmNameWith=vmNameWith)

    if (vmsToDelete == []):
        logger.autolog(level=1,message='Couldnt Find any VM with the provided name. Nothing to delete')
        return True

    if(vmsToDelete != False):
        logger.autolog(level=1,message='Got some VMs to delete. Proceed with deletion.')
        return deleteVms(service_instance, vmsToDelete)
    else:
        logger.autolog(level=1,message='Got some error while getting list of Vms to delete. Failing...')
        return False

    #return vcenter.deleteVms(vmsToDelete,host=creds['host'], user=creds['username'], password=creds['password'])


##############################
# Functions for DS Management
##############################


def setPermissionsToUserDs(dsName='', dcName='', host='', user='', userName='',  password='', port=443):
    logger.autolog(level=1,message='About to set permissions to User DS  ' + dsName)
    si=connectVcenter(host=host, user=user, password=password)
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..')
        return False
    content = si.RetrieveContent()

    ds= getDatastoreByName(content, dsName=dsName)
    if not ds:
        logger.autolog(level=1,message='Not able to find DS')
        return False

    return  setDsPermissions(content, ds, 'DCV\\'+userName)


def getDatastore(content, vim_type=[vim.Datastore], dsNameWith=''):
    obj = None
    container = content.viewManager.CreateContainerView(content.rootFolder, vim_type, True)
    dsObjList=[]
    if dsNameWith:
        for c in container.view:
            #logger.autolog(level=1,message='checking in container. Now doing ' + str(c.name))
            if (str(c.name).find(dsNameWith) != -1):
                obj = c
                dsObjList.append(obj)
    else:
        logger.autolog(level=1,message='No Name provided. Not able to search for datastore')
        return False
    if dsObjList:
        return dsObjList
    else:
        logger.autolog(level=1,message='Could not find Ds with the provided details')

    return dsObjList

def getDatastoreByName(content, vim_type=[vim.Datastore], dsName=''):
    logger.autolog(level=1,message='About to get DS with name ' + dsName)
    container = content.viewManager.CreateContainerView(content.rootFolder, vim_type, True)
    if dsName:
        for c in container.view:
            #logger.autolog(level=1,message='checking in container. Now doing ' + str(c.name))
            if (str(c.name) == dsName):
                return c
    else:
        logger.autolog(level=1,message='No Name provided. Not able to search for datastore')
        return False

    return False





def setDsPermissions(content,ds, userName):
        logger.autolog(level=1,message='About to assign permissions to DS  ' + str(ds))

        myPermissions=[]

        myPermission= vim.AuthorizationManager.Permission()
        myPermission.entity=ds
        myPermission.principal =  userName #'DCV\\v561user1'
        myPermission.group= False
        myPermission.roleId= -1
        myPermission.propagate= True
        myPermissions.append(myPermission)

        myPermission= vim.AuthorizationManager.Permission()
        myPermission.entity=ds
        myPermission.principal = 'DCV\\Hyperflex Hardware Demo Users'
        myPermission.group= True
        myPermission.roleId= -2
        myPermission.propagate= True
        myPermissions.append(myPermission)

        try:
            content.authorizationManager.SetEntityPermissions(ds,myPermissions)
            logger.autolog(level=1,message='Permissions assgined to ds ' + str(ds))
            return True
        except Exception as e:
            logger.autolog(level=1,message="Error while assigning permissions to  DS  "+ str(ds) +"   : " + str(e))
            return False


def isDatastoreUserCreated(content, dsName):
    folder = getFolder(content, dsName)
    if folder:
        return False
    else:
        return True


def deleteVmsInDatastores(datastores=[], dcName='', host='', user='', password='', port=443):
    logger.autolog(level=1,message='About to delete Vms in selected datastores')
    si=connectVcenter(host=host, user=user, password=password)
    content = si.RetrieveContent()
    if not si:
        logger.autolog(level=1,message='Unable to get Service instance from vCenter. Aborting..')
        return False
    vmsInDsDeleted=[]
    for ds in datastores:
        if isDatastoreUserCreated(content, ds['name']):
            if deleteVmsInDatastore(si, ds['name']):
                vmsInDsDeleted.append(ds)
    logger.autolog(level=1,message='Emptied datastores:')
    logger.autolog(level=1,message=vmsInDsDeleted)
    return vmsInDsDeleted


def deleteVmsInDatastore(si,dsName):
    logger.autolog(level=1,message='About to delete VMs in datastore ' + dsName)
    content = si.RetrieveContent()

    vmsToDelete=getVmsInDatastore(content, dsName)
    logger.autolog(level=1,message='Vms to be deleted: ')
    logger.autolog(level=1,message=vmsToDelete)
    return destroyVms(si, vmsToDelete)


def getVmsInDatastore(content, dsName):

    ds= getDatastoreByName(content,dsName=dsName)
    if not ds:
        logger.autolog(level=1,message='Unable to find datastore with name ' + dsName + '. Returning no vms')
        return []
    logger.autolog(level=1,message=ds.vm)
    return ds.vm


def getStorageSystem(content, vim_type=[vim.host.StorageSystem], hostName=''):
    logger.autolog(level=1,message='Looking for Storage system')

    hostObjectView = content.viewManager.CreateContainerView(content.rootFolder,[vim.HostSystem],True)
    hosts = hostObjectView.view
    hostObjectView.Destroy()

    hostWithDatastoreToDelete=[]
    for host in hosts:
        if host.name == hostName:
            hostWithDatastoreToDelete= host
            logger.autolog(level=1,message='Found name matching Host ( '+hostName+'). Compute Resource is ' + str(hostWithDatastoreToDelete))


    if not hostWithDatastoreToDelete:
        logger.autolog(level=1,message='Could not find a name matching host. There must be an error. Failing...')
        return False

    configManager= hostWithDatastoreToDelete.configManager
    storageSystem = configManager.storageSystem

    return storageSystem


def removeDatastoresNamedWith(dsNameWith='' , hostName='', host='', user='', password='', port=443):

    logger.autolog(level=1,message='About to delete Datastores with names that contain ' + str(dsNameWith))

    service_instance = connectVcenter(host=host, user=user, password=password)
    if not service_instance:
        logginginfo('Not able to connect to vCenter. Failing...')
        return False


    content = service_instance.RetrieveContent()


    storageSystem = getStorageSystem(content, hostName=hostName)
    logger.autolog(level=1,message='Refreshing Storage System for Host')
    storageSystem.RefreshStorageSystem()
    time.sleep(2)

    dsObjList = getDatastore(content, dsNameWith=dsNameWith)


    if dsObjList == False:
        logger.autolog(level=1,message='Got Error while getting list of Datastores. Failing...')
        return False

    if dsObjList:
        for dsObj in dsObjList:
            #logger.autolog(level=1,message=str(dsObj.info))
            dsName=dsObj.info.vmfs.name
            dsUuid=dsObj.info.vmfs.uuid
            scsiUuid=dsObj.info.vmfs.extent[0].diskName
            logger.autolog(level=1,message='Removing  DS ' +  str(dsName))
            #logger.autolog(level=1,message=str(scsiUuid))
            try:
                logger.autolog(level=1,message='About to unmount DS ' + str(dsName))
                storageSystem.UnmountVmfsVolume(dsUuid)
                logger.autolog(level=1,message='About to Delete Volume state for  DS ' + str(dsName))
                storageSystem.DeleteVmfsVolumeState(dsUuid)
                logger.autolog(level=1,message='About to Detach SCSI Lun for  DS ' + str(dsName))
                storageSystem.DetachScsiLun(scsiUuid)
                time.sleep(4)
                logger.autolog(level=1,message='Refreshing Storage System for Host')
                storageSystem.RefreshStorageSystem()

            except Exception as e:
                logger.autolog(level=1,message='Error while removing datastore ' + str(dsName))
                logger.autolog(level=1,message=e)
                return False
    else:
        logger.autolog(level=1,message='No DS to delete. Nothing to do')
        return True




####################################
# Functions for Network  Management
####################################


def createPortGroup(content, hostName, pgName, vssName,vlan=0):

    try:
        host=getObj(content, [vim.HostSystem], hostName)
        hostNetworkSystem = host.configManager.networkSystem

        port_group_spec = vim.host.PortGroup.Specification()
        port_group_spec.name = pgName
        port_group_spec.vlanId = vlan
        port_group_spec.vswitchName = vssName

        security_policy = vim.host.NetworkPolicy.SecurityPolicy()
        security_policy.allowPromiscuous = True
        security_policy.forgedTransmits = True
        security_policy.macChanges = True

        port_group_spec.policy = vim.host.NetworkPolicy(security=security_policy)
        hostNetworkSystem.AddPortGroup(portgrp=port_group_spec)
        return True

    except Exception as e:
        if "vim.fault.AlreadyExists" not in str(e):
            logger.autolog(level=1,message='Error while creating  portgroup  ' + str(pgName))
            logger.autolog(level=1,message=e)
            return False
        else:
            logger.autolog(level=1,message='Portgroup  alread exists...' + str(pgName))
            return True


def deletePortGroup(content, hostName, pgName):
    try:
        host=getObj(content, [vim.HostSystem], hostName)
        hostNetworkSystem = host.configManager.networkSystem
        hostNetworkSystem.RemovePortGroup(pgName)
        return True
    except Exception as e:
        if "vim.fault.NotFound" not in str(e):
            logger.autolog(level=1,message='Error while deleting portgroup: ' + str(pgName))
            logger.autolog(level=1,message=e)
            return False
        else:
            logger.autolog(level=1,message='Portgroup not found: ' + str(pgName))
            return True




if __name__ == '__main__':

    logger.setLogFiles(newLogFile='vcenter.log',newLogDebugFile='vcenter_DEBUG.log')
    logger.setLogger()
    logging.getLogger('werkzeug').setLevel(logging.ERROR)

    #createUserVm(1000, 'vc60', otherInputs={'vmName':'myVm','dsName':'iSCSI-vmware60','destFolderName':'admin','templateName':'webserver2','dcName':'HybridCloud','networkNameAdapter1':'mgarcias-999|Opencart|web' ,'networkNameAdapter2':'', 'clusterName':'HybridCloud'})
    #createUserVm(1000, 'vc60', otherInputs={'vmName':'myVm5','dsName':'iSCSI-vmware60','destFolderName':'admin','templateName':'loadbalancer','dcName':'HybridCloud','networkNameAdapter1':'mgarcias-999|Opencart|Loadbalancer', 'networkNameAdapter2':'mgarcias-999|Opencart|web','clusterName':'HybridCloud'})


    removeVmFolderWithVms(10000, 'vc60', otherInputs={'folderName': 'mgarcias-999'})

    time.sleep(320)
