
Function getFile{
param(
[string]$fileSource,
[string]$fileDestination
)
$WebClient = New-Object System.Net.WebClient
$WebClient.DownloadFile($fileSource, $fileDestination)
& 'C:\Program Files\7-zip\7z.exe' x $fileDestination -oc:\sftproot -y
}