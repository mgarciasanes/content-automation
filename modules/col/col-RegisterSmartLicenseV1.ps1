#used for licensing collab products with smart licensing




function getFile{
    param(
    [string]$fileSource,
    [string]$fileDestination
    )
    $WebClient = New-Object System.Net.WebClient
    $WebClient.DownloadFile($fileSource, $fileDestination)
    & 'C:\Program Files\7-zip\7z.exe' x $fileDestination -oc:\sftproot -y
}


function loop_match($text){
    #general loop to match exactly a certain string of characters
    $match = Get-Content $log_path | Where-Object {$_ -match $text} 
    while ($match -ne $text) { 
        Write-Host "Waiting for $text prompt" -ForegroundColor DarkGreen
        $match = Get-Content $log_path | Where-Object {$_ -match $text}
        Start-Sleep 5 
    }
}
function loop_find($text){
    #general loop to find if a string of characters are found in the log file
    $string = Get-Content $log_path | Select-String $text -quiet
    while ($string -ne $True) { 
        Write-Host "Waiting for $text prompt" -ForegroundColor DarkGreen
        $string = Get-Content $log_path | Select-String $text -quiet
        Start-Sleep 5 
    }
}
function unreg_start_install{
    #unregister procduct and start the install process
    param ($com1 = "license smart deregister", $com2 = "yes", $com3 = "utils system upgrade initiate")
    Select-Window putty | Send-Keys $com1
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 5

    Select-Window putty | Send-Keys $com2
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 15
    
    Select-Window putty | Send-Keys $com3
    Select-Window putty | Send-Keys "{ENTER}"
}

function install($path){
    #main install steps
    Select-Window putty | Send-Keys "1"
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 2

    Select-Window putty | Send-Keys $path
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 2

    Select-Window putty | Send-Keys "198.18.135.67"
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 2

    Select-Window putty | Send-Keys "demo"
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 2

    Select-Window putty | Send-Keys "C1sco12345"
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 2

    Select-Window putty | Send-Keys "{ENTER}"
    
    loop_find "Available options and upgrades"

    Select-Window putty | Send-Keys "1"
    Select-Window putty | Send-Keys "{ENTER}"
    
    loop_find "Start installation"

    Select-Window putty | Send-Keys "yes"
    Select-Window putty | Send-Keys "{ENTER}"
    
    loop_find "has been installed successfully"
    
    write-host "Sleeping for 60 sec after install completes"
    Start-Sleep 60

    Select-Window putty | Send-Keys $register
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 5

    $still_registered = Get-Content $log_path | Select-String "Product already registered for Smart Licensing" -quiet
    if ($still_registered -eq $True) { 
        
        Write-Host "Deregistering again. " -ForegroundColor DarkGreen
        Select-Window putty | Send-Keys "license smart deregister"
        Select-Window putty | Send-Keys "{ENTER}"
        Start-Sleep 15

        Write-Host "Trying to register once more. " -ForegroundColor DarkGreen
        Select-Window putty | Send-Keys $register
        Select-Window putty | Send-Keys "{ENTER}"
        Start-Sleep 5
    }

    Select-Window putty | Send-Keys "exit"
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 2


}
function verify_reg($ip, $name, $pass){
    #Used to verify if product has registered with the licensing server
    ./putty.exe $ip -l $name -pw $pass 
    
    loop_match 'admin:'

    Select-Window putty | Send-Keys "show license status"
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 2

    $status = Get-Content $log_path | Select-String "Status: REGISTERED" -quiet
    if ($status -eq $True) {
        Select-Window putty | Send-Keys "exit"
        Select-Window putty | Send-Keys "{ENTER}"
        Start-Sleep 2
        return $status
    } 
    else {
        Select-Window putty | Send-Keys $register
        Select-Window putty | Send-Keys "{ENTER}"
        Start-Sleep 5

        $still_registered = Get-Content $log_path | Select-String "Product already registered for Smart Licensing" -quiet
        if ($still_registered -eq $True) { 
            Write-Host "Deregistering again. " -ForegroundColor DarkGreen
            Select-Window putty | Send-Keys "license smart deregister"
            Select-Window putty | Send-Keys "{ENTER}"
            Start-Sleep 10
        }

    }
    Select-Window putty | Send-Keys "exit"
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 2
    return $status
}


#Scritp Start

[XML]$xml = Get-Content "c:/dcloud/session.xml"
$vpod = $xml.session.vpod
$owner = $xml.session.owner
$acpassword = $xml.session.anycpwd
$acusername = "v" + $vpod + "user1"
$id = $xml.session.id
$dc = $xml.session.datacenter

write-host 'Starting License Update'
Import-Module WASP

cd 'C:\Program Files\PuTTY'
$log_path = 'C:\dcloud\putty.log'
$xml_path = 'c:\dcloud\session.xml'

$products = 0
$registered = 0
$ucm_reg = 'no'
$cuc_reg = 'no'
$cer_reg = 'no'

$ucm_ip = '198.18.133.3'
$cuc_ip = '198.18.133.5'
$cer_ip = '198.18.133.167'

$uname = 'administrator'
$passwd = 'dCloud123!'

#Download COP Files
$fileSource="http://198.19.254.140/collab/scripts/licensing/cop_files.zip" 
$fileDestination="C:\sftproot\cop_files.zip"
getFile $fileSource $fileDestination
Write-Host "COP Files Downloaded"

#Get Token and register command
$token_url = "http://198.19.254.140/collab/licensing/token.txt"
$token_txt = "C:\Users\Administrator\Desktop\token.txt"
$WebClient = New-Object System.Net.WebClient
$WebClient.DownloadFile($token_url, $token_txt)
$token = Get-Content $token_txt
$token = $token.Replace("%","{%}")
$register = "license smart register idtoken " + $token + " force"
Write-Host "Register command is $register"



#Start UCM registration process if available
if (Test-Connection -Computername $ucm_ip -BufferSize 16 -Count 4 -Quiet) {
    write-host "Starting registration process for UCM"
    $ucm_present = 'yes'
    $products ++

    ./putty.exe $ucm_ip -l $uname -pw $passwd      
    loop_match 'admin:'
    unreg_start_install
    loop_match 'Source:'
    install '/UCM'

    write-host "Getting product info to register to license server"
    ./putty.exe $ucm_ip -l $uname -pw $passwd 
    loop_match 'admin:'

    Select-Window putty | Send-Keys "show license tech support"
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 2

    Select-Window putty | Send-Keys "exit"
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 2

    $a = Get-Content $log_path | Where-Object {$_ -match '(?<=regid.)(.+)()'}
    $ucmregid = "regid." + $matches[1]

    $a = Get-Content $log_path | Where-Object {$_ -match '(?<=PID:UCM,SN:)(.+)(?=,UUID:)'}
    $ucmsn = $matches[1]

    $a = Get-Content $log_path | Where-Object {$_ -match '(?<=,UUID:)(.+)()'} 
    $ucmuuid = $matches[1]

    $doc = New-Object System.Xml.XmlDocument
    $doc.Load($xml_path)
    $xmlElt = $doc.CreateElement("ucmregid")
    $xmlText = $doc.CreateTextNode($ucmregid)
    $xmlElt.AppendChild($xmlText) 
    $doc.DocumentElement.AppendChild($xmlElt)

    $xmlElt = $doc.CreateElement("ucmsn")
    $xmlText = $doc.CreateTextNode($ucmsn)
    $xmlElt.AppendChild($xmlText) 
    $doc.DocumentElement.AppendChild($xmlElt)

    $xmlElt = $doc.CreateElement("ucmuuid")
    $xmlText = $doc.CreateTextNode($ucmuuid)
    $xmlElt.AppendChild($xmlText) 
    $doc.DocumentElement.AppendChild($xmlElt)

    $doc.Save($xml_path)

}
#Start CUC registration process if available
if (Test-Connection -Computername $cuc_ip -BufferSize 16 -Count 4 -Quiet) {
    write-host "Starting registration process for CUC"
    $cuc_present = 'yes'
    $products ++
    
    ./putty.exe $cuc_ip -l $uname -pw $passwd 
    loop_match 'admin:'
    unreg_start_install
    loop_match 'Source:'
    install '/CUC'

    write-host "Getting product info to register to license server"
    ./putty.exe $cuc_ip -l $uname -pw $passwd 
    loop_match 'admin:'

    Select-Window putty | Send-Keys "show license tech support"
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 2

    Select-Window putty | Send-Keys "exit"
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 2

    $a = Get-Content $log_path | Where-Object {$_ -match '(?<=regid.)(.+)()'} 
    $cucregid = "regid." + $matches[1]

    $a = Get-Content $log_path | Where-Object {$_ -match '(?<=Connection,SN:)(.+)(?=,UUID:)'} 
    $cucsn = $matches[1]

    $a = Get-Content $log_path | Where-Object {$_ -match '(?<=,UUID:)(.+)()'} 
    $cucuuid = $matches[1]

    $doc = New-Object System.Xml.XmlDocument
    $doc.Load($xml_path)
    $xmlElt = $doc.CreateElement("cucregid")
    $xmlText = $doc.CreateTextNode($cucregid)
    $xmlElt.AppendChild($xmlText) 
    $doc.DocumentElement.AppendChild($xmlElt)

    $xmlElt = $doc.CreateElement("cucsn")
    $xmlText = $doc.CreateTextNode($cucsn)
    $xmlElt.AppendChild($xmlText) 
    $doc.DocumentElement.AppendChild($xmlElt)

    $xmlElt = $doc.CreateElement("cucuuid")
    $xmlText = $doc.CreateTextNode($cucuuid)
    $xmlElt.AppendChild($xmlText) 
    $doc.DocumentElement.AppendChild($xmlElt)

    $doc.Save($xml_path)
}
#Start CER registration process if available
if (Test-Connection -Computername $cer_ip -BufferSize 16 -Count 4 -Quiet) {
    write-host "Starting registration process for CER"
    $cer_present = 'yes'
    $products ++
    
    ./putty.exe $cer_ip -l $uname -pw $passwd
    loop_match 'admin:'
    unreg_start_install
    loop_match 'Source:'
    install '/CER'

    write-host "Getting product info to register to license server"
    ./putty.exe $cer_ip -l $uname -pw $passwd 
    loop_match 'admin:'

    Select-Window putty | Send-Keys "show license tech support"
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 2

    Select-Window putty | Send-Keys "exit"
    Select-Window putty | Send-Keys "{ENTER}"
    Start-Sleep 2
    
    $a = Get-Content $log_path | Where-Object {$_ -match '(?<=regid.)(.+)()'} 
    $cerregid = "regid." + $matches[1]

    $a = Get-Content $log_path | Where-Object {$_ -match '(?<=PID:CER,SN:)(.+)(?=,UUID:)'} 
    $cersn = $matches[1]

    $a = Get-Content $log_path | Where-Object {$_ -match '(?<=,UUID:)(.+)()'} 
    $ceruuid = $matches[1]

    $doc = New-Object System.Xml.XmlDocument
    $doc.Load($xml_path)
    $xmlElt = $doc.CreateElement("cerregid")
    $xmlText = $doc.CreateTextNode($cerregid)
    $xmlElt.AppendChild($xmlText) 
    $doc.DocumentElement.AppendChild($xmlElt)

    $xmlElt = $doc.CreateElement("cersn")
    $xmlText = $doc.CreateTextNode($cersn)
    $xmlElt.AppendChild($xmlText) 
    $doc.DocumentElement.AppendChild($xmlElt)

    $xmlElt = $doc.CreateElement("ceruuid")
    $xmlText = $doc.CreateTextNode($ceruuid)
    $xmlElt.AppendChild($xmlText) 
    $doc.DocumentElement.AppendChild($xmlElt)

    $doc.Save($xml_path)
}


#Add two elements for license cleanup tasks
$recipe_name = 'smart-license-v1'
$recipe_path = 'col/'

$doc = New-Object System.Xml.XmlDocument
$doc.Load($xml_path)
$xmlElt = $doc.CreateElement("recipeName")
$xmlText = $doc.CreateTextNode($recipe_name)
$xmlElt.AppendChild($xmlText) 
$doc.DocumentElement.AppendChild($xmlElt)

$xmlElt = $doc.CreateElement("recipePath")
$xmlText = $doc.CreateTextNode($recipe_path)
$xmlElt.AppendChild($xmlText) 
$doc.DocumentElement.AppendChild($xmlElt)

$doc.Save($xml_path)

#send to automation server 
write-host 'Sending session data to automation server.'
$url = "http://198.19.254.51:31080/api/v1.0/task"
$postData = Get-Content $xml_path
Invoke-WebRequest -Uri $url -Method POST -Body $postData -ContentType "text/xml"


#Confirm all products are registered
write-host 'Confirming all products are registered'
while ($registered -ne $products) {    
  
    if ($ucm_present -eq 'yes'-And $ucm_reg -eq 'no') {
        if (verify_reg '198.18.133.3' 'administrator' 'dCloud123!') {
            write-host "UCM registered"
            $registered++
            $ucm_reg = 'yes'
        } 
        else {
            write-host "UCM not registered"
        }
    }
    if ($cuc_present -eq 'yes'-And $cuc_reg -eq 'no') {
        if (verify_reg '198.18.133.5' 'administrator' 'dCloud123!') {
            write-host "CUC registered"
            $registered++
            $cuc_reg = 'yes'
        } 
        else {
            write-host "CUC not registered"
        }
    }
    if ($cer_present -eq 'yes'-And $cer_reg -eq 'no') {
        if (verify_reg '198.18.133.167' 'administrator' 'dCloud123!') {
            write-host "CER registered"
            $registered++
            $cer_reg = 'yes'
        } 
        else {
            write-host "CER not registered"
        }
    }
    Start-Sleep 10
}


write-host 'Licensing update complete'
write-host 'Cleaning up files...'
Start-Sleep 5

Remove-Item $token_txt
Remove-Item "c:\dcloud\setup_scripts.zip"
Remove-Item "c:\dcloud\putty.log"
Get-ChildItem -Path C:\sftproot -Include *.* -File -Recurse | foreach { $_.Delete()}
Remove-Item "c:\dcloud\licensing.ps1" -Force

write-host 'All done...goodbye.'
Start-sleep 3