
####################################
#  Imports
####################################
import sys
modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
modulesPath=r'/root/scripts/modules/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger
import os
import sys
import urllib2
import time
import requests
import json
import http as http

####################################
#  Global Variables
####################################

cleanLicRetry=0
maxTries=4


####################################
#  FRONT-END FUNCTIONS
####################################
def getSmartLicenseToken(sessionId, target, otherInputs=[], logFormat=''):

    #status,token=getToken(otherInputs['clientId'],otherInputs['clientSecret'],otherInputs['userName'],otherInputs['password'],logFormat)
    status,token=getToken(target,logFormat)

    if status == False:
        return {},'Failed: ' + str(token) 
    else:
        return {'token':token}, 'Success'


def removeSmartLicense(sessionId, target, otherInputs=[], logFormat=''):
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            token=otherInputs['token']
            sn=otherInputs['sn']
            regid=otherInputs['regid']
            uuid=otherInputs['uuid']
            pid=otherInputs['pid']
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            #return {},'Failed: Error while reading input arguments'
            return {},'Success: Not executed, required details not found'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    #result,eMsg=removeLicense(otherInputs['token'],otherInputs['sn'],otherInputs['regid'],otherInputs['uuid'],otherInputs['pid'],target,logFormat)
    result,eMsg=removeLicense(token,sn,regid,uuid,pid,target,logFormat)


    if result:
        return {},'Success'
    else:
        return {},'Failed: ' + str(eMsg)    


def spartTestFunction(sessionId, target, otherInputs=[], logFormat=''):
    msg=otherInputs['message']

    result=sendSparkPost(msg,target)

    if result:
        return {},'Success'
    else:
        return {},'Failed: ' + 'Failed...'  

####################################
#  GENERIC FUNCTIONS
####################################


#def getToken(clientId,clientSecret,userName,password,logFormat):
def getToken(target,logFormat):

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return False,'Error: Unable to get credentials for target'

    host=creds['host']
    protocol=creds['protocol']
    url=creds['url']+'&client_id='+creds['clientId']+'&client_secret='+creds['clientSecret']+'&username='+creds['userName']+'&password='+creds['password']

    count=0
    while True:
        try:
            count += 1
            r=http.sendRequestGetDict(protocol=protocol,host=host, url=url,data='', method='post',logFormat=logFormat)
            if r == False:
                logger.autolog(level=1, message='Error while getting Token'  ,format=logFormat)
                raise ValueError("Failed to get response from HTTP request for TOKEN")
            token=r['access_token']
            msg=' Got Smart License Token:  ' + str(token) 
            logger.autolog(level=1, message=msg ,format=logFormat)
            #sendSparkPost('From Content Automation:  '+logFormat+msg)
            return True,token
                        
        except Exception as e:
            logger.autolog(level=1, message=str(e) ,format=logFormat )
            time.sleep(2)
            if (count == 5):
                msg = 'Cleanup script was unable to get token after 5 tries.'
                logger.autolog(level=1, message=msg  ,format=logFormat)
                #send_spark_post(message_api, {"roomId": space_id, "markdown": msg})
                sendSparkPost('From Content Automation:  '+logFormat+msg)
                return False, msg 
            pass    

def removeLicense(token,sn,regid,uuid,pid,target,logFormat):

    global cleanLicRetry
    global maxTries

    if cleanLicRetry>=maxTries:
        msg='Maximun tries reached while trying to delete licenses for: ' +str(pid) + " with sn: " + str(sn) 
        logger.autolog(level=1, message=msg ,format=logFormat)
        sendSparkPost('From Content Automation:  '+logFormat+msg, {'file':r'/root/repo/col/creds.cfg','device':'sparkNotifications'})
        return False, "Maximun tries reached while trying to delete licenses for " +str(pid)

    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return False,'Error: Unable to get credentials for target'

    removeHost=creds['host']
    removeUrl=creds['removeUrl']
    protocol=creds['protocol']

    headers={'Authorization': 'Bearer ' + token,'Content-Type': 'application/json'}
    data = '{"sudi":{"uuid":"'+uuid+'","udiPid":"'+pid+'","udiSerialNumber":"'+sn+'"}, "productTagName":"'+regid+'"}'

    r=http.sendRequestGetDict(protocol=protocol,host=removeHost, url=removeUrl,data=data, method='post',headers=headers,logFormat=logFormat)
    if r == False:
        eMsg='Error while removing license of type: ' + str(pid) + ' Got unexpected response, check logs for more info.'
        logger.autolog(level=1, message=eMsg  ,format=logFormat)
        return False, eMsg

    if 'noJson' in r.keys():
        if 'censored' in r['noJson']:
            eMsg=' Product '+pid+' with SN '+sn+' and UUID '+uuid+' returned --censored-- while cleanup. Asumming deleted'
            logger.autolog(level=1, message=eMsg ,format=logFormat)
            return True, eMsg

        if 'Access Denied' in r['noJson']:
            eMsg=' Product '+pid+' with SN '+sn+' and UUID '+uuid+' returned --Access Denied-- while cleanup. Will try again in 5 seconds'
            logger.autolog(level=1, message=eMsg ,format=logFormat)
            time.sleep(5)
            cleanLicRetry=cleanLicRetry+1
            return removeLicense(token,sn,regid,uuid,pid,target,logFormat)

    if 'status' in r.keys():
        if r['status'] != 'SUCCESS':
            message=r['statusMessage']
            if 'instance does not exist'  in message:
                msg=' Product '+pid+' with SN '+sn+' and UUID '+uuid+' not found. Nothing to remove'
                #sendSparkPost('From Content Automation:  '+logFormat+msg, {'file':r'/root/repo/col/creds.cfg','device':'sparkNotifications'})
                return True, 'Not found, nothing to do'
            else:
                eMsg=' Product '+pid+' with SN '+sn+' and UUID '+uuid+' failed on remove with message "'+message+'". '
                logger.autolog(level=1, message=eMsg ,format=logFormat)
                #sendSparkPost('From Content Automation:  '+logFormat+msg, {'file':r'/root/repo/col/creds.cfg','device':'sparkNotifications'})
                return False, eMsg

        if r['status'] == 'SUCCESS':
            msg=' Product '+pid+' with SN '+sn+' and UUID '+uuid+'  removed'
            logger.autolog(level=1, message=msg ,format=logFormat)
            #sendSparkPost('From Content Automation:  '+logFormat+msg, {'file':r'/root/repo/col/creds.cfg','device':'sparkNotifications'})
            return True,msg

    logger.autolog(level=1, message="Either response is not in JSON format or status key was not found",format=logFormat)
    return False, "Either response is not in JSON format or status key was not found"



def sendSparkPost(msg,target):
    """
    This method is used for:
        -posting a message to the Spark room to confirm that a command was received and processed
    """
    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return False,'Error: Unable to get credentials for target'


    data={"roomId": creds['spaceId'], "markdown": str(msg)}
 
    bot_bearer = creds['bot_bearer']
    url=creds['url']

    #message_api  = "https://api.ciscospark.com/v1/messages"

    request = urllib2.Request(url, json.dumps(data),
                            headers={"Accept" : "application/json",
                                     "Content-Type":"application/json"})
    request.add_header("Authorization", "Bearer "+bot_bearer)
    contents = urllib2.urlopen(request).read()
    return contents




####################################
#  OLD FUNCTIONS
####################################


def  lic_cleanup(session,logFormat):

    #get token
    tokenUrl = 'https://cloudsso.cisco.com/as/token.oauth2?grant_type=password&client_id=MASKED&client_secret=MASKED&username=MASKED&password=MASKED'
    remove_url = 'https://apx.cisco.com/services/api/smart-accounts-and-licensing/v1/accounts/dcloud.cisco.com/virtual-accounts/dCloud%20Collab/devices/remove'
    removeHost='apx.cisco.com'
    removeUrl=r'/services/api/smart-accounts-and-licensing/v1/accounts/dcloud.cisco.com/virtual-accounts/dCloud%20Collab/devices/remove'
    url='/as/token.oauth2?grant_type=password&client_id=MASKED&client_secret=MASKED&username=MASKED&password=MASKED'
    token=''

    #url = tokenUrl
    count = 0
    while True:
        try:
            count += 1

            r=http.sendRequestGetDict(protocol="https",host='cloudsso.cisco.com', url=url,data='', method='post',logFormat=logFormat)

            if r == False:
                logger.autolog(level=1, message='Error while getting Token'  ,format=logFormat)
                raise ValueError("Failed to get response from HTTP request for TOKEN")

            token=r['access_token']
            logger.autolog(level=1, message=' GOT TOKEN ' + str(token)  ,format=logFormat)
            break

            #r = requests.post(url)
            ##print(r.content)

            result = str(r.text)
            ##print(result)
            result = result.split( )
            result = '[ "result",' + ' '.join(result) + ']'
            result = json.loads(result)
            token=result[1]['access_token']
            ##print(token)
            break
                        
        except Exception as e:
            #logging.info("Error getting token")
            logger.autolog(level=1, message=str(e) ,format=logFormat )
            time.sleep(2)
            if (count == 5):
                msg = 'Cleanup script was unable to get token after 5 tries. XML left in place to try again next time.'
                logger.autolog(level=1, message=msg  ,format=logFormat)
                #send_spark_post(message_api, {"roomId": space_id, "markdown": msg})
                return False, msg 
            pass    

    #delete products
    
    headers={'Authorization': 'Bearer ' + token,
    'Content-Type': 'application/json'}
    
    if (session['ucmregid'] != ''):
        sn = session['ucmsn']
        regid = session['ucmregid']
        uuid = session['ucmuuid']
        pid = 'UCM'
        data = '{"sudi":{"uuid":"'+uuid+'","udiPid":"UCM","udiSerialNumber":"'+sn+'"}, "productTagName":"'+regid+'"}'

        r=http.sendRequestGetDict(protocol="https",host=removeHost, url=removeUrl,data=data, method='post',headers=headers,logFormat=logFormat)
        if r == False:
            logger.autolog(level=1, message='Error while removing UCM license'  ,format=logFormat)
            #return False, 'Error while removing UCM license'


        #r = requests.post(remove_url, data=data, headers=headers)
        ##print (r.content)
        try:

            if r['status'] != 'SUCCESS':
                message=r['statusMessage']
                logger.autolog(level=1, message=' Product '+pid+' with SN '+sn+' and UUID '+uuid+' failed on remove with message "'+message+'". '   ,format=logFormat)
                #return False, ' Product '+pid+' with SN '+sn+' and UUID '+uuid+' failed on remove with message "'+message+'". '
            else:
                logger.autolog(level=1, message=' Product '+pid+' with SN '+sn+' and UUID '+uuid+'  removed' ,format=logFormat)

            #result = str(r.text)
            ##print(result)
            #result = result.split( )
            #result = '[ "result",' + ' '.join(result) + ']'
            #result = json.loads(result)
            #if (result[1]['status'] != 'SUCCESS'):
            #    status = result[1]['status']
            #    message = result[1]['statusMessage']
            #    msg = 'Product '+pid+' with SN '+sn+' and UUID '+uuid+' failed on remove with message "'+message+'".'
            #    #send_spark_post(message_api, {"roomId": space_id, "markdown": msg})
        except Exception as e:
            #logging.info("Error while extracting info from JSON format.")
            #logging.info(e)
            logger.autolog(level=1, message="Error while extracting info from JSON format." ,format=logFormat )
            logger.autolog(level=1, message=str(e) ,format=logFormat )

    
    if (session['cucregid'] != ''):
        sn = session['cucsn']
        regid = session['cucregid']
        uuid = session['cucuuid']
        pid = 'Cisco Unity Connection'
        data = '{"sudi":{"uuid":"'+uuid+'","udiPid":"Cisco Unity Connection","udiSerialNumber":"'+sn+'"}, "productTagName":"'+regid+'"}'

        r=http.sendRequestGetDict(protocol="https",host=removeHost, url=removeUrl,data=data, method='post',headers=headers,logFormat=logFormat)
        if r == False:
            logger.autolog(level=1, message='Error while removing CUC license'  ,format=logFormat)
            #return False, 'Error while removing CUC license'

        #r = requests.post(remove_url, data=data, headers=headers)
        ##print (r.content)
        try:

            if r['status'] != 'SUCCESS':
                message=r['statusMessage']
                logger.autolog(level=1, message=' Product '+pid+' with SN '+sn+' and UUID '+uuid+' failed on remove with message "'+message+'". '   ,format=logFormat)
                #return False, ' Product '+pid+' with SN '+sn+' and UUID '+uuid+' failed on remove with message "'+message+'". '
            else:
                logger.autolog(level=1, message=' Product '+pid+' with SN '+sn+' and UUID '+uuid+'  removed' ,format=logFormat)

            #result = str(r.text)
            ###print(result)
            #result = result.split( )
            #result = '[ "result",' + ' '.join(result) + ']'
            #result = json.loads(result)
            #if (result[1]['status'] != 'SUCCESS'):
            #    status = result[1]['status']
            #    message = result[1]['statusMessage']
            #    msg = 'Product '+pid+' with SN '+sn+' and UUID '+uuid+' failed on remove with message "'+message+'".'
            #    #send_spark_post(message_api, {"roomId": space_id, "markdown": msg})
        except Exception as e:
            #logging.info("Error while extracting info from JSON format.")
            #logging.info(e)
            logger.autolog(level=1, message="Error while extracting info from JSON format." ,format=logFormat )
            logger.autolog(level=1, message=str(e) ,format=logFormat )

    if (session['cerregid'] != ''):
        sn = session['cersn']
        regid = session['cerregid']
        uuid = session['ceruuid']
        pid = 'CER'
        data = '{"sudi":{"uuid":"'+uuid+'","udiPid":"CER","udiSerialNumber":"'+sn+'"}, "productTagName":"'+regid+'"}'
        r=http.sendRequestGetDict(protocol="https",host=removeHost, url=removeUrl,data=data, method='post',headers=headers,logFormat=logFormat)
        if r == False:
            logger.autolog(level=1, message='Error while removing CER license',format=logFormat)
            #return False, 'Error while removing CER license'

        #r = requests.post(remove_url, data=data, headers=headers)
        ##print (r.content)
        try:


            if r['status'] != 'SUCCESS':
                message=r['statusMessage']
                logger.autolog(level=1, message=' Product '+pid+' with SN '+sn+' and UUID '+uuid+' failed on remove with message "'+message+'". '   ,format=logFormat)
                #return False, ' Product '+pid+' with SN '+sn+' and UUID '+uuid+' failed on remove with message "'+message+'". '
            else:
                logger.autolog(level=1, message=' Product '+pid+' with SN '+sn+' and UUID '+uuid+'  removed' ,format=logFormat)


            #result = str(r.text)
            ###print(result)
            #result = result.split( )
            #result = '[ "result",' + ' '.join(result) + ']'
            #result = json.loads(result)
            #if (result[1]['status'] != 'SUCCESS'):
            #    status = result[1]['status']
            #    message = result[1]['statusMessage']
            #    msg = 'Product '+pid+' with SN '+sn+' and UUID '+uuid+' failed on remove with message "'+message+'".'
            #    #send_spark_post(message_api, {"roomId": space_id, "markdown": msg})
        except Exception as e:
            #logging.info("Error while extracting info from JSON format.")
            #logging.info(e)
            logger.autolog(level=1, message="Error while extracting info from JSON format." ,format=logFormat )
            logger.autolog(level=1, message=str(e) ,format=logFormat )

    
    return True,'Successful'