import logging
import os
import os.path
import requests
import sys
import time
import xml.dom.minidom
import yaml
import platform
from xml.dom import minidom
import datetime

modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)
import genFunctions as gen
import logger as logger




####################################
#  Global Variables
####################################
scriptPath = r'C:\\Scripts\\hyperflex\\'
#credsFile=r'/root/indemo_automation/creds.cfg'
credsFile=gen.credsFile


def createAdUser(sessionId, target, otherInputs=[], logFormat=''):
    global scriptPath
    if (type(otherInputs) is dict):
        try:
            username=gen.sanitizeString(otherInputs["username"])
            password=gen.sanitizeString(otherInputs["password"])
            groupName=otherInputs['groupName']
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'


    scriptName = r"createAdUser.ps1"
    psArgs=[{'name': 'sessionFile' , 'value': sessionId},{'name': 'userId' , 'value': username},{'name': 'password' , 'value': password},{'name':'groupName' , 'value': groupName}]
    output= gen.sshExecPsScript(creds, scriptPath, scriptName, psArgs, timeout=60)
    if(output=="True" or "Microsoft.ActiveDirectory.Management.ADIdentityAlreadyExistsException" in output):
        return {'result':output},'Success'
    else:
        return {'result':output},'Failed:'+str(output)



def removeAdUser(sessionId, target,  otherInputs=[], logFormat=''):
    global scriptPath
    logger.autolog(level=1,message='Starting removeADUser',format=logFormat)

    if (type(otherInputs) is dict):
        try:
            username=gen.sanitizeString(otherInputs["username"])
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    creds = gen.getDemoCredentialsByName(credsFile,target)
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    scriptName = r"removeAdUser.ps1"
    psArgs=[{'name': 'sessionFile' , 'value': sessionId},{'name': 'userId' , 'value': username}]
    output= gen.sshExecPsScript(creds, scriptPath, scriptName, psArgs, timeout=60)

    if (output=="True" or "Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException" in output):
        return {},'Success'
    else:
        return {},'Failed: '+str(output)


