#Generic
logFormat=''


#Controller
location=''
dcNumber=''
nodePortApi=''
nodePortDb=''
rabbitServer=''
dcIndex={'01':'rtp','02':'sng','03':'lon','05':'sjc','08':'chi'}
thisDc=''


#Worker
worker=''
workerLocation=''
workerRabbitServer=''
workerRabbitPort=''


#Register
register=''
registerLocation=''
registerRabbitServer=''
registerRabbitPort=''
