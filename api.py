####################################
# Global Variabled
####################################

INFLUXDB='influxKube'
global NODEPORT
global NODEPORTAPI
global NODEPORTDB
global RABBITSERVER
global LOCATION
global DCNUMBER
global CONTROLLERURL
#NODEPORT=''
#CONTROLLERURL='dcv-automation-api'
dcNumberToDcMapping={"01":"rtp","02":"sng","03":"lon","05":"sjc","08":"chi"}





#####################################
# Import Modules
#####################################
import sys
import os
import socket
import signal
import datetime
import string
import random
from flask import Flask, url_for, redirect
from flask import abort
from flask import make_response, current_app
from flask import request
from flask import jsonify
from flask import send_from_directory
from flask_cors import CORS

import xmltodict
import json
import threading
import logging
import subprocess
from flask.ext.httpauth import HTTPBasicAuth
import time
import pika

auth = HTTPBasicAuth()
app = Flask(__name__)
CORS(app)

modulesPath=r'/root/scripts/modules/'
sys.path.append(modulesPath)
modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)



import settings
import rabbitQueue as rabbit
import influx as influx
import inMemoryStorage
import genFunctions as gen
import logger as logger
import worker as worker
import worker1 as worker1
import checkQueuedRequests as start


def refreshModules():
    logger.autolog(level=1, message='Refreshing Modules, reloading...')
    reload(influx)
    reload(gen)
    reload(logger)
    reload(worker)



##################################################################
# General Settings
# 
##################################################################

@auth.get_password
def get_password(username):
    if username == 'mgarcias':
        return 'C1sco12345'
    return None

@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 401)

@app.route('/')
def api_root():
    return 'Welcome to DCV Automation...Looking for the Dashboard? Go to  http://dcv-automation-api:PORT/api/v1.0/local/dashboard     '

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)






##################################################################
# APIs
# 
##################################################################




##################################################################
##################################################################
#REQUESTS HANDLING
##################################################################
##################################################################

##################################################################
# Queue Request in DB
##################################################################
@app.route('/api/v1.0/task', methods=['POST'])
def api_task():
    refreshModules()
    #print "HEADERS", request.headers
    #print "REQ_path", request.path
    #print "ARGS",request.args
    #print "DATA",request.data
    #print "FORM",request.form

    #Remove any change of line from request
    data=request.data.replace('\n','')

    #Convert to DICT
    sessionDetails=xmltodict.parse(data)['session']

    #Log the request
    logFormat=sessionDetails['recipeName']+'|'+ sessionDetails['id'] +'|' + sessionDetails['owner'] + '|url: /api/v1.0/task '
    logger.autolog(level=1,message=str(data),format=logFormat)

    #Validate the request 
    sessionDetails, eMsg = gen.validateRequest(sessionDetails,'task',logFormat)
    if sessionDetails == False:
        return 'REQUEST IGNORED: ' + str(eMsg)

    #Cache the request
    sessionDetails["request"]["api"]=r'/api/v1.0/task'
    inMemoryStorage.cacheWriteRequest(sessionDetails,logFormat=logFormat)

    #Add json and xml format of the request to sessionDetails
    #sessionDetails["json"]=str(json.dumps(sessionDetails))
    #sessionDetails["xml"]=str(data)

    #Write the request in DB
    logger.autolog(message='Got request. Queuing it...',format=logFormat)
    influx.markSessionQueued(sessionDetails["request"],influxDb=INFLUXDB)

    #Queue the request in RABBIT
    sessionDetails["action"]='Start'
    rabbit.rabbitSendToWorkersQueue(RABBITSERVER,sessionDetails)

    return 'OK'

##################################################################
# Priority 0 Request. It executes the requests without queue
# [INSTANT DEMOS]
# Request details are received as [COOKIES]
# Returns [HTML] response
##################################################################
@app.route('/api/v1.0/task/q0', methods=['GET'])
def api_task_q0():
    refreshModules()

    sessionDetails={}
    try:
        logger.autolog(level=1,message=str(request.cookies))
        for cookie in request.cookies.keys():
            sessionDetails[cookie]=request.cookies[cookie]

    except Exception as e:
        logger.autolog(level=1,message='Unable to get required values from cookies... Ignoring request...  ' + str(e))
        return "REQUEST IGNORED:Unable to get required values from cookies... Ignoring request..." + str(e)


    try:    
        logFormat=sessionDetails['recipeName']+'|'+ sessionDetails['id'] +'|' + sessionDetails['owner'] + '|url: /api/v1.0/task/q0 '
    except Exception as e:
        logFormat=sessionDetails['recipeName']+'|NO REQUEST ID|' + sessionDetails['owner'] + '|url: /api/v1.0/task/q0 '

    logger.autolog(level=1,message="Request received at q0, processing...",format=logFormat)

    sessionDetails, eMsg = gen.validateRequest(sessionDetails,'q0',logFormat)
    if sessionDetails == False:
        return 'REQUEST IGNORED: ' + str(eMsg)

    #Cache the request
    sessionDetails['api']=r'/api/v1.0/task/q0'
    inMemoryStorage.cacheWriteRequest(sessionDetails,logFormat=logFormat)

    #Add json and xml format of the request to sessionDetails
    sessionDetails["json"]=str(json.dumps(sessionDetails))
    sessionDetails["xml"]=""

    sessionDetails["action"]="Start"

    executedTasks=execP0(sessionDetails)
    if executedTasks == False:
        return "Error while executing the request... Please contact dCloud Support"

    content=""
    for executedTask in executedTasks:
        if executedTask['taskName']=="htmlOutput":
            return executedTask['taskOutput']["htmlOutput"]
        if executedTask['taskName']=="htmlRedirect":
            return redirect(str(executedTask['taskOutput']["htmlRedirect"]), code=302)

    return 'HTML Output not found!!'




##################################################################
# SCHEDULING
##################################################################
@app.route('/api/v1.0/scheduler/create', methods=['POST'])
def api_schedule_create():
    refreshModules()

    #Remove any change of line from request
    data=request.data.replace('\n','')

    #Convert to DICT
    scheduleDetails=xmltodict.parse(data)['cron']

    #Log the request
    logFormat=scheduleDetails['recipeName']+'|'+ scheduleDetails['schedule'] +'|' + '|url: /api/v1.0/schedulre/create '
    logger.autolog(level=1,message=str(data),format=logFormat)

    #Validate the request 
    scheduleDetails, eMsg = gen.validateRequest(scheduleDetails,'schedule-create',logFormat)
    if scheduleDetails== False:
        return 'REQUEST IGNORED: ' + str(eMsg)

    #Cache the request
    scheduleDetails['api']=r'/api/v1.0/schedule/create'
    #inMemoryStorage.cacheWriteRequest(scheduleDetails,logFormat=logFormat)

    #Add json and xml format of the request to scheduleDetails
    scheduleDetails["json"]=str(json.dumps(scheduleDetails))
    scheduleDetails["xml"]=str(data)
 
    #Queue the request
    logger.autolog(message='Got schedule request.Processing it...',format=logFormat)
    #influx.markSessionScheduled(sessionDetails,influxDb=INFLUXDB)

    rabbit.rabbitSend(RABBITSERVER, 'scheduler', scheduleDetails)

    return 'OK'


@app.route('/api/v1.0/scheduler/delete', methods=['POST'])
def api_schedule_delete():
    refreshModules()

    #Remove any change of line from request
    data=request.data.replace('\n','')

    #Convert to DICT
    scheduleDetails=xmltodict.parse(data)['cron']

    #Log the request
    logFormat=scheduleDetails['recipeName']+'|'+ scheduleDetails['schedule'] +'|' + '|url: /api/v1.0/scheduler/delete '
    logger.autolog(level=1,message=str(data),format=logFormat)

    #Validate the request 
    scheduleDetails, eMsg = gen.validateRequest(scheduleDetails,'schedule-delete',logFormat)
    if scheduleDetails== False:
        return 'REQUEST IGNORED: ' + str(eMsg)

    #Cache the request
    scheduleDetails['api']=r'/api/v1.0/schedule/create'
    #inMemoryStorage.cacheWriteRequest(scheduleDetails,logFormat=logFormat)

    #Add json and xml format of the request to scheduleDetails
    scheduleDetails["json"]=str(json.dumps(scheduleDetails))
    scheduleDetails["xml"]=str(data)
 
    #Queue the request
    logger.autolog(message='Got schedule request.Processing it...',format=logFormat)
    #influx.markSessionScheduled(sessionDetails,influxDb=INFLUXDB)

    rabbit.rabbitSend(RABBITSERVER, 'scheduler', scheduleDetails)

    return 'OK'




##################################################################
##################################################################
# API VERSION 2.0 
##################################################################
##################################################################



###################################################################
#  REQUESTS  
###################################################################



##############    ##########    #######
#  REQUESTS  ###### CREATE ###### GET ###
##############    ##########    #######
@app.route('/api/v2.0/request/create/<id>', methods=['GET'])
def api_request_create_get(id):

    initLoggerData(request)
    refreshModules()

    logger.autolog(level=1,message='Received new request: ' + str(request))

    #Get Payload
    payload=getPayload(request)
    if not payload['status']:
        return payload['message']
    sessionDetails=payload['sessionDetails']

    logFormat=''

    #Get or generate request ID
    try:
        sessionDetails['idInUrl']=str(id)
    except Exception as e:
        sessionDetails['idInUrl']='0'

#        if sessionDetails['id']:
#            logger.autolog(level=1,message='Session ID Found! ' + str(sessionDetails['id']) ,format=logFormat) 
#            pass          
#    except Exception as e:
#        logger.autolog(level=1,message='Session ID Not Found in payload. Checking ID in API URL' ,format=logFormat)
#        try:
#            if sessionDetails['idInUrl']!='0':
#                logger.autolog(level=1,message='Session ID Found in API URL! Using it!' ,format=logFormat)
#                sessionDetails['id']=sessionDetails['idInUrl']
#            else:
#                logger.autolog(level=1,message='Session ID in API URL is 0! Using timestamp!' ,format=logFormat)
#                sessionDetails['id']=str(int(time.time()))
#        except Exception as e:
#            logger.autolog(level=1,message='Session ID not found in API URL! Using timestamp!' ,format=logFormat)
#            sessionDetails['id']=str(int(time.time()))


    #Log the request
    #logFormat=sessionDetails['recipeName']+'|'+ sessionDetails['owner'] + '|url: /api/v2.0/request/create/ [GET] '
    #logger.autolog(level=1,message=str(data),format=logFormat)


    #REQUEST VALIDATION FAILED. SHOW ERROR PAGE  
    sessionFullDetails=onboardRequest(sessionDetails)
    if isinstance(sessionFullDetails,str):
        if "REQUEST IGNORED" in sessionFullDetails:
            logger.autolog(message='Redirecting HTTP request to ' + str(settings.controllerWebUrl+r'/errorPage.php?msg='+sessionFullDetails) + ' [APACHE]' ,format=logFormat)
            httpRedirect=redirect(settings.controllerWebUrl+r'/errorPage.php?msg='+sessionFullDetails,code=307)
            response=current_app.make_response(httpRedirect)
            initLoggerData('',mode='stop')
            return response


    #REQUEST VALIDATION OK...
    initLoggerData(str(sessionFullDetails['request']['id']),mode='id')

    #logFormat=sessionFullDetails['request']['recipeName']+'|'+ sessionFullDetails['request']['id'] +'|'+ sessionFullDetails['request']['owner'] + '|url: /api/v2.0/request/create/[GET]'
    #t=threading.Thread(target=onboardRequest, args=([sessionDetails]))
    #t.setDaemon(True)
    #t.start()
    

    ##Cache the request
    #logger.autolog(level=1,message='Will cache the request [REDIS]',format=logFormat)
    #sessionDetails['api']=r'/api/v2.0/request [POST]'
    #cachedRequest=inMemoryStorage.cacheWriteRequest(sessionDetails,logFormat=logFormat)

    ##Add serialized payload to sessionDetails
    #sessionDetails["payload"]=str(json.dumps(sessionDetails))

    ##Write the request in DB
    #logger.autolog(message='Storing request [INFLUXDB]',format=logFormat)
    #influx.markSessionQueued(sessionDetails,influxDb=INFLUXDB)

    ##Queue the request in RABBIT
    #logger.autolog(message='Queuing request [RABBIT]',format=logFormat)
    #sessionDetails["action"]='Start'
    #rabbit.rabbitSendToWorkersQueue(RABBITSERVER,sessionDetails)

    ##Return redirect to waiting page
    logger.autolog(message='Redirecting HTTP request to ' + str(settings.controllerWebUrl+r'/loader-instant.php?id='+sessionDetails['id']) + ' [APACHE]' ,format=logFormat)
    httpRedirect=redirect(settings.controllerWebUrl+r'/loader-instant.php?id='+sessionDetails['id'],code=307)
    response=current_app.make_response(httpRedirect)

    #Insert SESSIONID cookie
    expire_date = datetime.datetime.now()
    expire_date = expire_date + datetime.timedelta(hours=2)
    response.set_cookie('dcloudSessionId', value=sessionDetails['id'],expires=expire_date)

    #Insert RECIPE cookie
    expire_date = datetime.datetime.now()
    expire_date = expire_date + datetime.timedelta(hours=2)
    response.set_cookie('recipeName', value=sessionDetails['recipeName'],expires=expire_date)

    #Insert USERID cookie
    expire_date = datetime.datetime.now()
    expire_date = expire_date + datetime.timedelta(days=365)
    cookieUserId=str(sessionDetails['owner']).replace("@","-at-")
    response.set_cookie('dcloudUserId', value=cookieUserId,expires=expire_date, domain='.dc-01.com')
    response.set_cookie('dcloudUserId', value=cookieUserId,expires=expire_date, domain='.dc-02.com')
    response.set_cookie('dcloudUserId', value=cookieUserId,expires=expire_date, domain='.dc-03.com')
    response.set_cookie('dcloudUserId', value=cookieUserId,expires=expire_date, domain='.dc-05.com')
    response.set_cookie('dcloudUserId', value=cookieUserId,expires=expire_date, domain='.dc-08.com')
    response.set_cookie('dcloudUserId', value=cookieUserId,expires=expire_date, domain='.cisco.com')

    #Insert DCLOUD cookie
    expire_date = datetime.datetime.now()
    expire_date = expire_date + datetime.timedelta(hours=2)
    response.set_cookie('dcloud', value=sessionDetails['id'],expires=expire_date)

    return response

    #htmlFile=open('/root/scripts/http/loader.html','r')
    #htmlContent=htmlFile.read()
    #htmlFile.close()
    #htmlContent=htmlContent.replace('REQUESTSTATUSURL',settings.controllerApiUrl+r'/api/v2.0/request/'+str(sessionDetails['id']))
    #return htmlContent





    #logger.autolog(level=1,message='Response.content is :' +str(response.__dict__),format=logFormat)
    #return '<body><h1>YEAH</h1><body>'




##############    ##########
#  REQUESTS  ###### CREATE #
##############    ##########
@app.route('/api/v2.0/request/<id>', methods=['POST'])
def api_request_create(id):


    refreshModules()
    #print "HEADERS", request.headers
    #print "REQ_path", request.path
    #print "ARGS",request.args
    #print "DATA",request.data
    #print "FORM",request.form

    logger.autolog(level=1,message='Received new request: ' + str(request))

    #Get Payload
    payload=getPayload(request)
    if not payload['status']:
        return payload['message']
    sessionDetails=payload['sessionDetails']


    #Log the request
    logFormat=sessionDetails['recipeName']+'|'+ sessionDetails['owner'] + '|url: /api/v2.0/request [POST] '
    #logger.autolog(level=1,message=str(data),format=logFormat)

    #Validate the request 
    sessionDetails['idInUrl']=str(id)
    sessionFullDetails, eMsg = gen.validateRequestV2(sessionDetails,'request-create',logFormat)
    if sessionFullDetails == False:
        return 'REQUEST IGNORED: ' + str(eMsg)
    logFormat=sessionFullDetails["request"]['recipeName']+'|'+ sessionFullDetails["request"]['id'] +'|'+ sessionFullDetails["request"]['owner'] + '|url: /api/v2.0/request [POST]'


    #Set the request details for status "queued" and action "start"
    sessionFullDetails["action"]='Start'
    sessionFullDetails["status"]="queued"

    #Queue the request in RABBIT for registration
    logger.autolog(message='Queuing request [RABBIT]',format=logFormat)
    rabbit.rabbitSendToRegisterExchange(RABBITSERVER,sessionFullDetails)

    #Queue the request in RABBIT for execution
    logger.autolog(message='Queuing request [RABBIT]',format=logFormat)
    rabbit.rabbitSendToWorkersQueue(RABBITSERVER,sessionFullDetails)


    #t=threading.Thread(target=onboardRequest, args=([sessionFullDetails]))
    #t.setDaemon(True)
    #t.start()
    ##return 'Executing Request'

    #Return redirect to waiting page
    #logger.autolog(message='Redirecting HTTP request to ' + str(cachedRequest['loaderUrl']) + ' [APACHE]' ,format=logFormat)
    #response=redirect(sessionFullDetails['loaderUrl'],code=307)
    #response.response=json.dumps(sessionFullDetails)
    #response.headers['Content-Length']=len(response.response)
    #logger.autolog(level=1,message='Response.content is :' +str(response.__dict__),format=logFormat)
    #return response
    return "ok"
    






##############    ##########    ##########
#  REQUESTS  ###### CREATE ###### BUFFER #
##############    ##########    ##########
 
@app.route('/api/v2.0/request/buffer/<bufferName>', methods=['POST'])
def api_request_buffer_create(bufferName):

    refreshModules()

    logger.autolog(level=1,message='Received new buffer request: ' + str(request))

    #Get Payload
    payload=getPayload(request)
    if not payload['status']:
        return payload['message']
    sessionDetails=payload['sessionDetails']

    #Log the request
    logFormat=sessionDetails['recipeName']+'|'+ sessionDetails['owner'] + '|url: /api/v2.0/request/buffer [POST] '

    #Validate the request 
    sessionDetails['bufferName']=str(bufferName)
    sessionFullDetails, eMsg = gen.validateRequestV2(sessionDetails,'request-create',logFormat)
    if sessionFullDetails == False:
        return 'REQUEST IGNORED: ' + str(eMsg)
    logFormat=sessionFullDetails["request"]['recipeName']+'|'+ sessionFullDetails["request"]['id'] +'|'+ sessionFullDetails["request"]['owner'] + '|url: /api/v2.0/request/buffer [POST]'



    #Set the request details for status "queued" and action "start"
    sessionFullDetails["action"]='Start'
    sessionFullDetails["status"]="queued"

    #Queue the request in RABBIT for registration
    logger.autolog(message='Queuing request [RABBIT]',format=logFormat)
    rabbit.rabbitSendToRegisterExchange(RABBITSERVER,sessionFullDetails)

    #Queue the request in RABBIT for execution
    logger.autolog(message='Queuing request [RABBIT]',format=logFormat)
    rabbit.rabbitSendToBufferQueue(RABBITSERVER,sessionFullDetails)


    ##Cache the request
    #logger.autolog(level=1,message='Will cache the request [REDIS]',format=logFormat)
    #sessionFullDetails["request"]['api']=r'/api/v2.0/request/buffer [POST]'
    #cachedRequest=inMemoryStorage.cacheWriteRequest(sessionFullDetails,logFormat=logFormat)

    ##Write the request in DB
    #logger.autolog(message='Storing request [INFLUXDB]',format=logFormat)
    #influx.markSessionQueued(sessionFullDetails["request"],influxDb=INFLUXDB)

    ##Queue the request in RABBIT
    #logger.autolog(message='Queuing request [RABBIT]',format=logFormat)
    #sessionFullDetails["action"]='Start'
    #rabbit.rabbitSendToBufferQueue(RABBITSERVER,sessionFullDetails)


    #Return redirect to waiting page
    #logger.autolog(message='Redirecting HTTP request to ' + str(cachedRequest['loaderUrl']) + ' [APACHE]' ,format=logFormat)
    #response=redirect(cachedRequest['loaderUrl'],code=307)
    #response.response=json.dumps(cachedRequest)
    #response.headers['Content-Length']=len(response.response)
    #logger.autolog(level=1,message='Response.content is :' +str(response.__dict__),format=logFormat)
    #return response
    return "ok"
    


##############    ##########
#  REQUESTS  ###### STATUS #
##############    ##########
@app.route('/api/v2.0/request/<id>', methods=['GET'])
def api_request_status(id):

    refreshModules()

    logger.autolog(level=1,message='Received new STATUS request: ' + str(request))

    #Get Payload
    #payload=getPayload(request)
    #if not payload['status']:
    #    return payload['message']
    #sessionDetails=payload['sessionDetails']
    #sessionDetails={}
    #Log the request
    #logFormat='Request Status | url: /api/v2.0/request [GET] '
    #logger.autolog(level=1,message=str(data),format=logFormat)

    #Validate the request 
#    sessionDetails['idInUrl']=str(id)
#    sessionDetails, eMsg = gen.validateRequestV2(sessionDetails,'request-status',logFormat)
#    if sessionDetails == False:
#        return 'REQUEST IGNORED: ' + str(eMsg)
#    logFormat='Request Status|'+ sessionDetails['id'] + '|url: /api/v2.0/request [GET] '


    sessionDetails={}
    sessionDetails["registerAction"]="requestStatus"
    sessionDetails["request"]={}
    sessionDetails["request"]["id"]=str(id)

    try:
        logger.autolog(level=1,message='Will send rabbit RPC request for session id : ' + str(id))
        cachedRequest=rabbit.rabbitSendRPC(RABBITSERVER, socket.gethostname() , socket.gethostname(), sessionDetails,exchangeName='REGISTERS')
        #logger.autolog(level=1,message='RPC call response: ' + str(cachedRequest))
    except Exception as e:
        logger.autolog(level=1,message='Error sending RPC call: ' + str(e))
        return oops

    return cachedRequest




##############    ###########
#  REQUESTS  ###### RESTART #
##############    ###########
@app.route('/api/v2.0/request/restart/<id>', methods=['POST'])
def api_request_restart(id):
    refreshModules()

    logFormat='Request Restart | url: /api/v2.0/request/restart [POST] |' +str(id) +'|'

    logger.autolog(message='Received restart action for request with id:' +str(id),format=logFormat)

    #Get request details from INFLUX
    sessionDetails=influx.getSessionDetails(str(id),influxDb=INFLUXDB)
    if sessionDetails:
        #Update Request status in REDIS
        result,requestStatus=inMemoryStorage.cacheUpdateRequestMainStatus(sessionDetails,'queued','',logFormat=logFormat)
        #Update INFLUXDB
        influx.markSessionQueued(sessionDetails,serialStatus=json.dumps(requestStatus),influxDb=INFLUXDB)
        #Queue the request in RABBIT
        sessionDetails={"request":sessionDetails}
        sessionDetails['action']='Start'
        if rabbit.rabbitSendToWorkersQueue(RABBITSERVER,sessionDetails):
        #if True:
            #If request was queued ok, return updated status
            sessionDetails=influx.getSessionById(str(id),influxDb=INFLUXDB)
            #session=prepareSessionforPresentationV2(sessionDetails)
            jsonResponse=jsonify({'requests': sessionDetails})
            logger.autolog(level=1,message=str(sessionDetails),format=logFormat) 
            return jsonResponse
        else:
            return "Error while queueing request"
    else:
        return "Unable to find request"


##############    ###########
#  REQUESTS  ###### CLEANUP #
##############    ###########
@app.route('/api/v2.0/request/cleanup/<id>', methods=['POST'])
def api_request_cleanup(id):
    refreshModules()

    logFormat='Request Cleanup | url: /api/v2.0/request/cleanup [POST] |' +str(id) +'|'

    logger.autolog(message='Received cleanup action for request with id:' +str(id),format=logFormat)

    #Get request details from INFLUX
    sessionDetails=influx.getSessionDetails(str(id),influxDb=INFLUXDB)
    if sessionDetails:
        #Update Request status in REDIS
        sessionDetails=inMemoryStorage.cacheReadRequest(sessionDetails,logFormat=logFormat)
        sessionDetails["status"]="toBeDeleted" 
        inMemoryStorage.cacheWriteRequest(sessionDetails,logFormat=logFormat)
        #result,requestStatus=inMemoryStorage.cacheUpdateRequestMainStatus(sessionDetails,'toBeDeleted','',logFormat=logFormat)

        #Update INFLUXDB
        influx.markSessionForDeletion(sessionDetails["request"],serialStatus=json.dumps(sessionDetails["request"]),influxDb=INFLUXDB)
        #Queue the request in RABBIT
        sessionDetails['action']='Stop'
        if rabbit.rabbitSendToWorkersQueue(RABBITSERVER,sessionDetails):
            #If request was queued ok, return updated status
            sessionDetails=influx.getSessionById(str(id),influxDb=INFLUXDB)
            #session=prepareSessionforPresentationV2(sessionDetails)
            jsonResponse=jsonify({'requests': sessionDetails})
            logger.autolog(level=1,message=str(sessionDetails),format=logFormat) 
            return jsonResponse
        else:
            return "Error while cleanup request"
    else:
        return "Unable to find request"





##############    ###########
#  REQUESTS  ######  LIST   #
##############    ###########
@app.route('/api/v2.0/request/list', methods=['GET'])
def api_request_list():
    refreshModules()

    logFormat='Request List | url: /api/v2.0/request/list [GET] |'
    logger.autolog(level=1,message="Received list request at /api/v2.0/request/list",format=logFormat)

    #Get filters for each key, if any on the headers...
    filterLocation=getHeaderIfExists(request,"queryLocation", default=".*",logFormat=logFormat)
    filterId=getHeaderIfExists(request,"queryId", default=".*",logFormat=logFormat)
    filterStatus=getHeaderIfExists(request,"queryStatus", default=".*",logFormat=logFormat)
    filterOwner=getHeaderIfExists(request,"queryUser", default=".*",logFormat=logFormat)
    filterDemo=getHeaderIfExists(request,"queryDemo", default=".*",logFormat=logFormat)
    filterDc=getHeaderIfExists(request,"queryDc", default=".*",logFormat=logFormat)
    filterTimeRangeStart=getHeaderIfExists(request,"timeRangeStart", default="",logFormat=logFormat)
    filterTimeRangeEnd=getHeaderIfExists(request,"timeRangeEnd", default="",logFormat=logFormat)


    requestDetails={}
    requestDetails["registerAction"]="requestList"
    requestDetails["request"]={}
    requestDetails["request"]["id"]="na"
    requestDetails["requestListFilters"]={}
    requestDetails["requestListFilters"]["filterLocation"]=filterLocation
    requestDetails["requestListFilters"]["filterId"]=filterId
    requestDetails["requestListFilters"]["filterStatus"]=filterStatus
    requestDetails["requestListFilters"]["filterOwner"]=filterOwner
    requestDetails["requestListFilters"]["filterDemo"]=filterDemo
    requestDetails["requestListFilters"]["filterDc"]=filterDc
    requestDetails["requestListFilters"]["filterTimeRangeStart"]=filterTimeRangeStart
    requestDetails["requestListFilters"]["filterTimeRangeEnd"]=filterTimeRangeEnd


    try:
        logger.autolog(level=1,message='Will send rabbit RPC request for request list')
        requestList=rabbit.rabbitSendRPC(RABBITSERVER, socket.gethostname() , socket.gethostname(), requestDetails,exchangeName='REGISTERS')
        #logger.autolog(level=1,message='RPC call response: ' + str(requestList))
    except Exception as e:
        logger.autolog(level=1,message='Error sending RPC call: ' + str(e))
        return oops

    return jsonify({'requests': json.loads(requestList)})












##################################################################
# General Functions
##################################################################

def onboardRequest(sessionDetails):

    #Validate the request 
    sessionFullDetails, eMsg = gen.validateRequestV2(sessionDetails,'request-create','')
    if sessionFullDetails== False:
        return 'REQUEST IGNORED: ' + str(eMsg)

    logFormat=sessionFullDetails['request']['recipeName']+'|'+ sessionFullDetails['request']['id'] +'|'+ sessionFullDetails['request']['owner'] + '|url: /api/v2.0/request/create/[GET]'

    #Set the request details for status "queued" and action "start"
    sessionFullDetails["action"]='Start'
    sessionFullDetails["status"]="queued"

    #Queue the request in RABBIT for registration
    logger.autolog(message='Queuing request [RABBIT]',format=logFormat)
    rabbit.rabbitSendToRegisterExchange(RABBITSERVER,sessionFullDetails)

    #Queue the request in RABBIT for execution
    logger.autolog(message='Queuing request [RABBIT]',format=logFormat)
    rabbit.rabbitSendToWorkersQueue(RABBITSERVER,sessionFullDetails)

    return sessionFullDetails

    logFormat=sessionFullDetails["request"]['recipeName']+'|'+ sessionFullDetails["request"]['owner'] + '|url: /api/v2.0/request [POST] '

    #Cache the request
    logger.autolog(level=1,message='Will cache the request [REDIS]',format=logFormat)
    sessionFullDetails["request"]['api']=r'/api/v2.0/request [POST]'
    cachedRequest=inMemoryStorage.cacheWriteRequest(sessionFullDetails,logFormat=logFormat)

    #Write the request in DB
    logger.autolog(message='Storing request [INFLUXDB]',format=logFormat)
    influx.markSessionQueued(sessionFullDetails["request"],influxDb=INFLUXDB)

    #Queue the request in RABBIT
    logger.autolog(message='Queuing request [RABBIT]',format=logFormat)
    sessionFullDetails["action"]='Start'
    rabbit.rabbitSendToWorkersQueue(RABBITSERVER,sessionFullDetails)


def getPayload(request):

    sessionDetails={}

    #Sanitize Payload:  Remove any change of line from request
    try:
        data=request.data.replace('\n','')
    except Exception as e:
        logger.autolog(level=1,message='Couldnt get payload data', format='api_request_api|')
        return  {"status":False,"message":'REQUEST IGNORED: Couldnt get payload data','sessionDetails':{}}

    #Get PAYLOAD
    try:
        if request.headers['Content-Type']:

            #XML
            if request.headers['Content-Type']==r'application/xml':
                try: 
                    sessionDetails=xmltodict.parse(data)['session']
                except Exception as e:
                    logger.autolog(level=1,message='Error while extracting XML payload ' + str(e), format='api_request_api|')
                    return  {"status":False,"message":'REQUEST IGNORED: Couldnt extract XML payload data: ' + str(e),'sessionDetails':{}}


            #JSON
            if request.headers['Content-Type']==r'application/json':
                try:
                    sessionDetails=json.loads(data)['session']      
                except Exception as e:  
                    logger.autolog(level=1,message='Error while extracting JSON payload ' + str(e), format='api_request_api|')
                    return  {"status":False,"message":'REQUEST IGNORED: Couldnt extract JSON  payload data: ' + str(e),'sessionDetails':{}}


            #COOKIES
            if request.headers['Content-Type']==r'application/cookies':
                try:
                    logger.autolog(level=1,message=str(request.cookies))
                    for cookie in request.cookies.keys():
                        sessionDetails[cookie]=request.cookies[cookie]
                except Exception as e:
                    logger.autolog(level=1,message='Unable to get required values from cookies... Ignoring request...  ' + str(e))
                    return {"status":False,"message":"REQUEST IGNORED:Unable to get required values from cookies. Ignoring request..." + str(e),'sessionDetails':{}}


    except Exception as e:
        logger.autolog(level=1,message='Couldnt find Content-Type header, assuming XML...', format='api_request_api|')
        try:
            #sessionDetails=json.loads(data)['session']    
            sessionDetails=xmltodict.parse(data)['session']    
        except Exception as e:
            logger.autolog(level=1,message='Error while extracting XML payload ' + str(e), format='api_request_api|')
            return  {"status":False,"message":'REQUEST IGNORED: Couldnt extract XML payload data: ' + str(e),'sessionDetails':{}}


    return {"status":True,"message":'Success','sessionDetails':sessionDetails}




def getHeaderIfExists(request, headerName, default=False, logFormat=''):

    try:
        if request.headers[headerName]:  
            #logger.autolog(level=1,message='Found header with name ' + headerName + " Value is " + str(request.headers[headerName]) ,format=logFormat)
            return request.headers[headerName]
    except Exception as e:  
        #logger.autolog(level=1,message="Header with name " + headerName + " not found! Using default valiue of " + str(default) ,format=logFormat)
        return default



def initLoggerData(request,mode="start"):

    if mode=='start':
        settings.loggerDetails["api"]=str(request.path)
        settings.loggerDetails["method"]=str(request.method)
        settings.loggerDetails["apiLogToken"]=''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(25))
        settings.loggerDetails["requestId"]=''


    if mode=='id':
        settings.loggerDetails["requestId"]=str(request)

    if mode=='stop':
        settings.loggerDetails["api"]=''
        settings.loggerDetails["method"]=''
        settings.loggerDetails["apiLogToken"]=''
        settings.loggerDetails["requestId"]=''


def execP0(sessionDetails):

    logFormat=sessionDetails["request"]['recipeName']+'|'+ sessionDetails["request"]['id'] +'|' + sessionDetails["request"]['owner'] + '|url: /api/v1.0/task/p0'

    #sessionDetails=sessionDetails[0]
    logger.autolog(level=1,message=str(sessionDetails["request"]), format=logFormat)
    logger.autolog(message='Got Request. Executing...', format=logFormat)
 

    recipePath=''
    try:
        if sessionDetails["request"]['recipePath']:
            logger.autolog(level=1,message='Recipe Path Found! ' + str(sessionDetails["request"]['recipePath']), format=logFormat)
            recipePath=sessionDetails["request"]['recipePath']  
    except Exception as e:
        logger.autolog(level=1,message='Recipe Path  Not Found! Using default', format=logFormat)
        recipePath=''

    return start.executeRecipe(sessionDetails)




if __name__ == '__main__':

    ##################################
    ## Check Arguments
    ##################################

    from argparse import ArgumentParser
    parser = ArgumentParser("dCloud DCV Automation Script: API")
    parser.add_argument('-n', '--nodeport', help='Port to be used by Proxy or Kube Service Nodeport ', default=False)
    parser.add_argument('-api', '--apinodeport', help='Port to be used by Proxy or Kube Service Nodeport ', default=False)
    parser.add_argument('-db', '--dbnodeport', help='Port to be used by Proxy or Kube Service DB Nodeport ', default=False)

    parser.add_argument('-r', '--rabbitserver', help='Specify rabbitmq server to use. Default is content-automation-rabbitmq', default='content-automation-rabbitmq')
    parser.add_argument('-rp', '--rabbitport', help='Specify rabbitmq port to use. Default is 5672 ', default="5672")
#    parser.add_argument('-c', '--credfile', help='Specify Credentials File  ', default=r'/root/repo/dcv/creds.cfg')

    parser.add_argument('-loc', '--location', help='Specify Location where this script is running. Default is False ', default=False)
    parser.add_argument('-dcn', '--dcnumber', help='Number Specific to dCloud DC. RTP=1,SNG=2,LON=3... Default is False ', default=False)
    parser.add_argument('-aps', '--apiservicename', help='DB Service Name in kubernetes  ', default='content-automation-counter')
    parser.add_argument('-dbs', '--dbservicename', help='API Service Name in kubernetes  ', default='content-automation-fridge')
    parser.add_argument('-a', '--controllerapiurl', help='URL to be used by Controller API', default='https://dcv-automation-api')
    parser.add_argument('-w', '--controllerweburl', help='URL to be used by Controller WEB ', default='http://dcv-automation-web')


    args = parser.parse_args()

    

    #global NODEPORT
    #NODEPORT=str(args.nodeport)
    #CONTROLLERURL=str(args.controllerapiurl)

    #####################################
    ## Start SSH Service
    #####################################
    #os.system(r'/usr/sbin/sshd -D &')

    #####################################
    ## Configure Logger
    #####################################

    hostname = socket.gethostname()
    logger.setLogFiles(newLogFile=str(hostname )+'.log',newLogDebugFile=str(hostname )+'_DEBUG.log')
    logger.setLogger()
    logging.getLogger('werkzeug').setLevel(logging.ERROR)
    logging.getLogger("pika").setLevel(logging.WARNING)

    settings.hostname=str(hostname)
    settings.loggerDetails={}
    settings.loggerDetails["hostname"]=settings.hostname


    #####################################################################################
    ## Get Port settings for API and DB. Get Location and Controller URL
    ##
    ## Note:
    ## If no arguments are provided, it will be assumed that we are running on kubernetes
    ##
    #####################################################################################


    CONTROLLERURL=str(args.controllerapiurl)

    #Get Cluster Location
    if not args.location:
        cmd=r'kubectl get services kubernetes --namespace=default -o jsonpath={.metadata.labels.location} > /root/location'
        logger.autolog(level=1,message=str(cmd))
        os.system(cmd)
        locationFile=open(r'/root/location','r')
        location=locationFile.read().strip('\n')
        locationFile.close()
        logger.autolog(level=1,message="We are working in location " + str(location))
        LOCATION=location
    else:
        LOCATION=str(args.location)
         
    #inMemoryStorage.redisSet("LOCATION", LOCATION)
    settings.location=LOCATION
    settings.loggerDetails["location"]=settings.location

    #Get DC Number
    if not args.location:
        cmd=r'kubectl get services kubernetes --namespace=default -o jsonpath={.metadata.labels.dcnumber} > /root/dcnumber'
        logger.autolog(level=1,message=str(cmd))
        os.system(cmd)
        dcnumberFile=open(r'/root/dcnumber','r')
        dcnumber=dcnumberFile.read().strip('\n')
        dcnumberFile.close()
        logger.autolog(level=1,message="We are working with dcnumber " + str(dcnumber))
        DCNUMBER=dcnumber
    else:
        DCNUMBER=str(args.dcnumber)

    #inMemoryStorage.redisSet("DCNUMBER", str(DCNUMBER))
    settings.dcNumber=DCNUMBER
    settings.thisDc=settings.dcIndex[DCNUMBER]
    settings.controllerApiUrl=str(args.controllerapiurl)+'.svpod.dc-'+DCNUMBER+'.com'
    settings.controllerWebUrl=str(args.controllerweburl)+'.svpod.dc-'+DCNUMBER+'.com'




    #Get API NodePort
    if not args.apinodeport:
        cmd=r"kubectl get services  "+str(args.apiservicename)+"  -o=jsonpath='{.spec.ports[?(@.name=="+'"api"'+r")].nodePort}' > /root/nodePortApi"
        logger.autolog(level=1,message=str(cmd))
        os.system(cmd)
        nodePortApiFile=open(r'/root/nodePortApi','r')
        nodePortApi=nodePortApiFile.read().strip('\n')
        nodePortApiFile.close()
        logger.autolog(level=1,message="We are working with API nodePort " + str(nodePortApi))
        if str(nodePortApi)=='31080':
            NODEPORTAPI=''
            CONTROLLERURL='dcv-automation-api.svpod.dc-'+DCNUMBER+'.com'
        else:
            NODEPORTAPI=nodePortApi
    else:
        NODEPORTAPI=str(args.apinodeport)
    #inMemoryStorage.redisSet("NODEPORTAPI", NODEPORTAPI)
    settings.nodePortApi=NODEPORTAPI

    #Get DB NodePort
    if not args.dbnodeport:
        cmd=r"kubectl get services  "+str(args.dbservicename)+"  -o=jsonpath='{.spec.ports[?(@.name=="+'"api"'+r")].nodePort}' > /root/nodePortDb"
        logger.autolog(level=1,message=str(cmd))
        os.system(cmd)
        nodePortDbFile=open(r'/root/nodePortDb','r')
        nodePortDb=nodePortDbFile.read().strip('\n')
        nodePortDbFile.close()
        logger.autolog(level=1,message="We are working with DB nodePort " + str(nodePortDb))
        NODEPORTDB=nodePortDb
    else:
        NODEPORTDB=str(args.dbnodeport)
    #inMemoryStorage.redisSet("NODEPORTDB", NODEPORTDB)
    settings.nodePortDb=NODEPORTDB

    #Get Rabbitmq Server
    #creds = gen.getDemoCredentialsByName(args.credfile,args.rabbitserver)
    RABBITSERVER=args.rabbitserver
    settings.rabbitServer=args.rabbitserver
    settings.rabbitPort=args.rabbitport


    #logger.autolog(level=1,message="Sending restart order to all workers!...")
    #rabbit.rabbitSendToWorkersQueue(RABBITSERVER,{"action":"workerRestart","datacenter":dcNumberToDcMapping[str(DCNUMBER)]})
    logger.autolog(level=1,message="Starting the API Server...")



    #####################################
    ## Start 
    #####################################

    #app.run(host='0.0.0.0',port='80',debug=True)
    app.run(debug=True,host='0.0.0.0',port=80,threaded=True)

