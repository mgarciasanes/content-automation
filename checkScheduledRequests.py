#!/usr/bin/env python2.7

#####################################
# Global Variables
#####################################
#demoRecipesPath=r'/root/scripts/demoRecipes/'
modulesPath=r'/root/scripts/modules/'
INFLUXDB='influxKube'
global LOCATION
global INFLUXDBPORT

#####################################
# Import Modules
#####################################
import sys
import logging
import threading
import os
import os.path
import time
import datetime
import socket
import psutil
import json


from crontab import CronTab
from croniter import croniter
from crontab import CronSlices
import xmltodict
import pika

modulesPath=r'/root/scripts/modules/'
sys.path.append(modulesPath)
corePath=r'/root/scripts/core/'
sys.path.append(corePath)


import genFunctions as gen
import logger as logger
import worker as worker
import signalMonitor
import influx as influx
import rabbitQueue

host = socket.gethostname()


#####################################
# General Functions
#####################################

def refreshModules():
    logger.autolog(level=1, message='Refreshing Modules, importing...')
    reload(influx)
    reload(gen)
    reload(logger)
    reload(worker)
    #reload(signalMonitor)


#####################################
#  FUNCTIONS IN LOOP
#####################################


def executeQueuedRequests():
    logger.autolog(level=1,message='Execute queued requests...')

    #queuedSessions=influx.readFromInfluxDb('select "datacenter","host","recipePath","recipeName","type","demo","id","vpod","anycpwd","owner",demoStatus from (select "datacenter","recipePath","recipeName","type","demo","host","id","vpod","anycpwd","owner",last(demoStatus) as demoStatus  from inDemoAutomation_sessions  group by id )  where demoStatus='+"'queued'" +' and host='+"'"+host+"'" +   ' group by id',influxDb=INFLUXDB)
    queuedSessions=influx.readFromInfluxDb('select *,id from (select *,last(demoStatus) from '+ str(influx.measurementSessions)+'  group by id )  where demoStatus='+"'queued'" +' and host='+"'"+host+"'" +   ' group by id',influxDb=INFLUXDB)

    if queuedSessions == False:
        logger.autolog(message='Unable to get queued requests')
        return False

    if queuedSessions == []:
        logger.autolog(message='No queued requests found')
        return True

    for queuedSession in queuedSessions:
        logger.autolog(level=1,message='Found queued request for recipe ' +  queuedSession['recipeName']+ ' with id ' + queuedSession['id'] + '. It will be executed now')
        influx.markSessionStarting(queuedSession,influxDb=INFLUXDB)
        tasks,errorMessage=gen.getRecipeStartTasks(queuedSession['recipeName'],specificFolder=queuedSession['recipePath'])
        if tasks == False: 
            logger.autolog(message=errorMessage +' '+ str(queuedSession['recipeName']))
            influx.markSessionError(queuedSession ,comments=errorMessage,influxDb=INFLUXDB)
        else: 
            failureFree,allTasksExecuted,executedTasks=worker.execTasks(queuedSession,tasks,action='Start') 
            influx.recordExecutedTasks(executedTasks,influxDb=INFLUXDB)
            if (failureFree and allTasksExecuted):
                logger.autolog(message='queued session executed without errors')
                influx.markSessionActive(queuedSession,influxDb=INFLUXDB)
            else:
                logger.autolog(message='queued session executed with errors or some tasks were not executed')
                influx.markSessionError(queuedSession,comments='Failed to startup',influxDb=INFLUXDB)


def getRequestFromQueue():
    logger.autolog(level=1,message='WIll connect to DB and get the oldest request in queue...')

    query='select *,id from (select *,last(demoStatus) from '+ str(influx.measurementSessions)+'  group by id )  where demoStatus='+"'queued'" +' and host='+"'"+host+"'" +   ' group by id'
    queuedSession=influx.readFromInfluxDb(query,influxDb=INFLUXDB)

    if queuedSession == False:
        logger.autolog(message='WARNING: Unable to get queued requests')
        return False

    if queuedSession == []:
        logger.autolog(message='No queued requests found')
        return False

    logger.autolog(level=1,message='Found queued request for recipe ' +  queuedSession['recipeName']+ ' with id ' + queuedSession['id'] + '. It will be executed now')
    return queuedSession


def executeRecipe(queuedSession):
    influx.markSessionStarting(queuedSession,influxDb=INFLUXDB)
    tasks,errorMessage=gen.getRecipeStartTasks(queuedSession['recipeName'],specificFolder=queuedSession['recipePath'])
    if tasks == False: 
        logger.autolog(message=errorMessage +' '+ str(queuedSession['recipeName']))
        influx.markSessionError(queuedSession ,comments=errorMessage,influxDb=INFLUXDB)
    else: 
        failureFree,allTasksExecuted,executedTasks=worker.execTasks(queuedSession,tasks,action='Start') 
        influx.recordExecutedTasks(executedTasks,influxDb=INFLUXDB)
        if (failureFree and allTasksExecuted):
            logger.autolog(message='queued session executed without errors')
            influx.markSessionActive(queuedSession,influxDb=INFLUXDB)
            return executedTasks
        else:
            logger.autolog(message='queued session executed with errors or some tasks were not executed')
            influx.markSessionError(queuedSession,comments='Failed to startup',influxDb=INFLUXDB)
            return False
                

#####################################
#  RABBITMQ
#####################################

'''

XML payload for schduling...

<?xml version="1.0" encoding="UTF-8"?>
<cron>
  <id>0000001</id>
  <datacenter>RTP</datacenter>
  <owner>mgarcias</owner>
  <cloud>dcloud</cloud>
  <geo>rtp</geo>
  <pod>sharedservices</pod>
  <recipeName>contiv_v1</recipeName>
  <recipePath>dcv/</reciepPath>
  <action>create</action>
  <schedule>* * * * *</schedule>
  <cmdcomment>* * * * *</cmdcomment>
</cron>


id: To be assigned by API
action: To be assigned by API:create/delete/update/enable/disable
schedule: Time slice for execution of the task. It follows CRON format and supports the following:

	Case		Meaning
	@reboot	Every boot
	@hourly	0 * * * *
	@daily	0 0 * * *
	@weekly	0 0 * * 0
	@monthly	0 0 1 * *
	@yearly	0 0 1 1 *
	@annually	0 0 1 1 *
	@midnight	0 0 * * *

'''


def callback(ch, method, properties, body):
    logger.autolog(level=1, message="Starting the Rabbit Callback Function", format="|RABBITCALLBACK|")
    logger.autolog(level=1, message="Message received: " + str(body), format="|RABBITCALLBACK|")

    try:
        bodyDict=json.loads(body)

        if bodyDict['action']=='create':
            try:
                if not CronSlices.is_valid(str(bodyDict['schedule'])):
                    logger.autolog(level=1, message="Time Slice is not valid : " + str(bodyDict['schedule']), format="|RABBITCALLBACK|CREATE|")
                    ch.basic_ack(delivery_tag = method.delivery_tag)
                    return

                system_cron = CronTab(tabfile='/etc/crontab', user=False)
                job=system_cron.new(command='/usr/bin/python /root/scripts/queueRequest.py -id ' + str(bodyDict['id']) , user='root',comment=str(bodyDict['id']) + ' ' + str(bodyDict['cmdcomment']))
                #job.set_comment=str(bodyDict['id']) + ': ' + str(bodyDict['cmdcomment'])
                job.setall(str(bodyDict['schedule']))
                job.enable()
                schedule = job.schedule(date_from=datetime.datetime.now())
                logger.autolog(level=1, message="Generated Job will be executed  : " + str(schedule.get_next()), format="|RABBITCALLBACK|CREATE|")
                if job.is_valid():
                    system_cron.write()
                    influx.markSessionScheduled(bodyDict,comments='To be executed: '+str(schedule.get_next()),influxDb=INFLUXDB)
                else:
                    logger.autolog(level=1, message="Generated Job is not valid : " + str(job), format="|RABBITCALLBACK|CREATE|")               
            except Exception as e:
                logger.autolog(level=1, message="Error while creating CRON job...", format="|RABBITCALLBACK|CREATE|")
                logger.autolog(level=1, message=str(e), format="|RABBITCALLBACK|CREATE|")

        if bodyDict['action']=='delete':
            try:
                system_cron = CronTab(tabfile='/etc/crontab', user=False)
                iter=system_cron.find_comment(str(bodyDict['id'])+ ' ' + str(bodyDict['cmdcomment']))
                flag=False
                for job in iter:
                    flag=True  
                    logger.autolog(level=1, message="Found jobs to delete: " + str(job), format="|RABBITCALLBACK|DELETE|")
                    system_cron.remove(job)
                if not flag:
                    logger.autolog(level=1, message="No jobs found with the provided comment: " + str(bodyDict['id'])+ ' ' + str(bodyDict['cmdcomment']) , format="|RABBITCALLBACK|DELETE|")
                system_cron.write()
                influx.markSessionCancelled(bodyDict,comments='SCHEDULE DELETED',influxDb=INFLUXDB)
            except Exception as e:
                logger.autolog(level=1, message="Error while deleting CRON job...", format="|RABBITCALLBACK|DELETE|")
                logger.autolog(level=1, message=str(e), format="|RABBITCALLBACK|DELETE|")

        #Send ACK
        ch.basic_ack(delivery_tag = method.delivery_tag)

    except Exception as e:
        logger.autolog(level=1, message="Error while in CALLBACK funciton...", format="|RABBITCALLBACK|DELETE|")
        logger.autolog(level=1, message=str(e), format="|RABBITCALLBACK|DELETE|")
        ch.basic_ack(delivery_tag = method.delivery_tag)


if __name__ == '__main__':

    ##################################
    ## Check Arguments
    ##################################

    from argparse import ArgumentParser
    parser = ArgumentParser("dCloud DCV Automation Script: checkScheduledRequests")
    parser.add_argument('-l', '--loop', help='Run Once or Loop. Values are  loop or once', default=True)
    parser.add_argument('-t', '--timer', help='Run Once or Loop. Values are  loop or once', default=5)
    parser.add_argument('-d', '--database', help='Specify database to use. Default is influxKube ', default='influxKube')
    parser.add_argument('-dp', '--databaseport', help='Specify database port to use. Default is noPort  ', default="noPort")
    parser.add_argument('-r', '--rabbitserver', help='Specify rabbitmq server to use. Default is rabbitKube ', default='rabbitKube')
    parser.add_argument('-rp', '--rabbitport', help='Specify rabbitmq port to use. Default is noPort  ', default="noPort")
    parser.add_argument('-c', '--credfile', help='Specify Credentials File  ', default=r'/root/repo/dcv/creds.cfg')


    parser.add_argument('-loc', '--location', help='Specify Location where this script is running. Default is False ', default="noLocation")
    parser.add_argument('-x', '--multithread', help='Run tasks for all sessions in concurrent or sequential. Values are true or false. default is false.', default=False)
    args = parser.parse_args()
    
    
    #####################################
    ## Configure Logger
    #####################################

    logger.setLogFiles(newLogFile='checkScheduledRequests.log',newLogDebugFile='checkScheduledRequests_DEBUG.log')
    logger.setLogPath(newLogPath=r'/var/log/automation/')
    logger.setLogger()


    #####################################
    ## Configure Signal Check
    #####################################

    signalCheck = signalMonitor.SignalMonitor()


    #####################################
    ## Start CRONIE Service
    #####################################
    os.system(r'/usr/sbin/cron')


    #####################################
    ## Start Loop
    #####################################
    #global INFLUXDB

    #DB Port to be used. If not specified as argument, use the one specified in Creds file, section "database"
    if args.databaseport=="noPort":
        logger.autolog(level=1,message="No database port specified, using port from Cred file")
        INFLUXDB=args.database
    else:
        logger.autolog(level=1,message="Database port specified, using it. " +str(args.databaseport))
        INFLUXDB={'database':args.database,'port':args.databaseport}

    #Get Cluster Location. If not specified as argument, use the valued obtained directly from Kubernetes' label
    if args.location=="noLocation":
        cmd=r'kubectl get services kubernetes --namespace=default -o jsonpath={.metadata.labels.location} > /root/location'
        logger.autolog(level=1,message=str(cmd))
        os.system(cmd)
        locationFile=open(r'/root/location','r')
        location=locationFile.read().strip('\n')
        locationFile.close()
        logger.autolog(level=1,message="We are working with location  " + str(location))
        LOCATION=location
    else:
        LOCATION=str(args.location)
    
    #loop(args.loop,args.multithread,int(args.timer))


    creds = gen.getDemoCredentialsByName(args.credfile,args.rabbitserver)
    rabbitServer=creds['host']
    queueName='scheduler'
    rabbitQueue.rabbitListenBlocking(rabbitServer,queueName,callback)