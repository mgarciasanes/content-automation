# dCloud content automation
Controller for content automation in dcloud 

This project aims to provide a standardised way to automatically setup and cleanup the required environment for diferent types of dCloud content (scheduled sessions, instant demos,etc...)

Each piece of content has unique requirements. Given their specific nature, they might require to dynamically create and delete  accounts, tenants, licenses or any type of configurations into local and remote, internal and external appliances, services or clouds.




## Access to Dashboard

    http://dcv-automation-api:<API_PORT>/api/v1.0/dashboard 

## API to Schedule new Requests

    http://198.19.254.51:<API_PORT>/api/v1.0/task 

## Access to InfluxDB Web Interface

    http://dcv-automation-api:31883

## Access to InfluxDB API

    http://dcv-automation-api:31886


## Recipes and Modules

## [Example: In-demo automation configuration Step-By-Step](https://github.com/dcloud-automation/content-automation/blob/master/help_001.md)
  - [bootstrap.sh description](https://github.com/dcloud-automation/content-automation/blob/master/indemo/common/bootstrap.md)
  - [auto.sh description](https://github.com/dcloud-automation/content-automation/blob/master/indemo/common/auto.md)

## [Master and Development Controller Instances](https://github.com/dcloud-automation/content-automation/blob/master/help_002.md)

