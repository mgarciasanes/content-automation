
usage ()
{
  echo 'Usage : getConfig  -ip | --ip-address   IP address of Kubernetes Master hosting the config file'
  echo '                   -d  | --destination  local path where to store the configuration file'
  echo '                   -s  | --source       sourche path on Kubernetes Master where configuration file is'
  exit
}



destination='.'
source='/etc/kubernetes/admin.conf'
#ip='198.19.254.63'
ip='kubernetes'

while [ "$1" != "" ]; do
    case $1 in
        -ip | --ip-address )    shift
                                ip=$1
                                ;;
        -d | --destination )    shift
                                destination=$1
                                ;;
        -s | --source )         shift
                                source=$1
                                ;;

        * )                     usage
                                exit 1
    esac
    shift
done



#echo scp root@$ip:/etc/kubernetes/admin.conf .

scp root@$ip:$source $destination


#Hosts File Configuration
cat <<EOF >> /etc/hosts
$ip  kubernetes
EOF
