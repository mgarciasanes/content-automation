##############################################################################################
#  Parsing Arguments
##############################################################################################

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -i|--indemo-recipe)
    inDemoRecipeName="$2"
    shift # past argument
    ;;
    -ip|--indemo-recipe-path)
    inDemoRecipePath="$2"
    shift # past argument
    ;;
    -d|--demo-recipe)
    demoRecipeName="$2"
    shift # past argument
    ;;
    -id|--demo-recipe-path)
    demoRecipePath="$2"
    shift # past argument
    ;;
    --default)
    helpMe="YES"
    ;;
    *)
           # unknown option
    ;;
esac
shift # past argument or value
done




##############################################################################################
#  Print Usage Example....
##############################################################################################


if [ -z "$inDemoRecipeName" ] &&  [ -z "$demoRecipeName" ] ; then
    echo
    echo "Automation Bootstrap. Examples of use:"
    echo
    echo " Request both, indemo and demo automation: "
    echo " ./auto.sh --indemo-recipe contiv-v1-indemo  --indemo-recipe-path dcv/ --demo-recipe contiv-v1 --demo-recipe-path dcv/ "
    echo " ./auto.sh  -i contiv-v1-indemo -ip dcv/ -d contiv-v1  -dp dcv/ "
    echo
    echo " Request indemo automation only: "
    echo " ./auto.sh  --demo-name 'ACI Contiv v1' --indemo-recipe contiv-v1-indemo  --indemo-recipe-path dcv/ "
    echo " ./auto.sh  -i contiv-v1-indemo -ip dcv/  "
    echo
    echo " Request demo automation only: "
    echo " ./auto.sh  --demo-recipe contiv-v1 --demo-recipe-path dcv/ "
    echo " ./auto.sh  -d contiv-v1  -dp dcv/   "
    echo
    exit
fi




##############################################################################################
#  (If specified by arguments) Proceed to send request for demo automation in shared services
##############################################################################################

if [ -n "$demoRecipeName" ]; then
    echo "Working on Demo Automation Request"

    #Prepare XML for Demo Automation Request
    #Copy Original file with session details
    cp /dcloud/session.xml /dcloud/session-sharedservices.xml

    #Add Type tag to the session file (if not present already)
    grep '<type>' /dcloud/session-sharedservices.xml 2>&1 1>/dev/null || sed -i '/<\/session>/i <type>queued<\/type>' /dcloud/session-sharedservices.xml

    #Add Recipe Name tag to the session file (if not present already)
    grep '<recipeName>'  /dcloud/session-sharedservices.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <recipeName>$demoRecipeName<\/recipeName>" /dcloud/session-sharedservices.xml

    #Add recipePath tag to the session file (if argument is present)
    if [ -n "$demoRecipePath" ]; then
        echo "Adding recipe Path for demo request"
        grep '<recipePath>' /dcloud/session-sharedservices.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <recipePath>$demoRecipePath<\/recipePath>" /dcloud/session-sharedservices.xml
    fi

    #Get XML on a single line
    sharedServicesXML=$(cat /dcloud/session-sharedservices.xml  | tr -d '\n')
    sharedServicesXML=$(echo "$sharedServicesXML" | tr -d '\r')
    #sharedServicesXML=$(echo "$sharedServicesXML" | tr -d '\r' | tr -d ' ')

    #Send  Automation Request to Central Automation Controller
    #This places the demo automation request that will be executed in shared services
    command='curl -i -H "Content-Type: text/xml" -X POST -d '$"'"$sharedServicesXML$"'"'  http://198.19.254.51:31080/api/v1.0/task'
    echo $command
    eval $command
    #curl -i -H "Content-Type: text/xml" -X POST -d '$sharedServicesXML'  http://198.19.254.51:31080/api/v1.0/task

fi

##############################################################################################
#  (If specified by arguments) Proceed to send remote request for in-demo automation
##############################################################################################

if [ -n "$inDemoRecipeName" ]; then

    echo "Preparing environment in order to execute in-demo automation"

    #Install Docker
    yum -y install docker
    systemctl enable docker
    systemctl start docker

    #Mount Code
    mkdir /var/nfs 2>/dev/null
    mkdir /var/nfs/dcv-automation-code 2>/dev/null
    mount 198.19.254.51:/var/nfs/dcv-automation-code /var/nfs/dcv-automation-code

    #Mount Logs
    mkdir /var/nfs/dcv-automation-logs 2>/dev/null
    mount 198.19.254.51:/var/nfs/dcv-automation-logs /var/nfs/dcv-automation-logs

    echo "Preparing inDemo Automation Request"

    #Prepare XML for Demo Automation Request
    #Duplicate  file with session details
    cp /dcloud/session.xml /dcloud/session-indemo.xml

    #Add Recipe Name tag to the session file (if not present already)
    grep '<recipeName>' /dcloud/session-indemo.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <recipeName>$inDemoRecipeName<\/recipeName>" /dcloud/session-indemo.xml

    #Add Type tag to the session file (if not present already)
    grep '<type>' /dcloud/session-indemo.xml 2>&1 1>/dev/null || sed -i '/<\/session>/i <type>remote<\/type>' /dcloud/session-indemo.xml

    #Add recipePath tag to the session file (if argument is present)
    if [ -n "$inDemoRecipePath" ]; then
        echo "Adding recipe Path for demo request"
        grep '<recipePath>' /dcloud/session-indemo.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <recipePath>$inDemoRecipePath<\/recipePath>" /dcloud/session-indemo.xml
    fi


    #Get Session ID from session file  (and remove the change of line characters)
    sessionId=$(sed  -n  -e 's/.*<id>\(.*\)<\/id>/\1/p' /dcloud/session-indemo.xml)
    sessionId=$(echo "$sessionId" | tr -d '\r')
    #Create hostname for the container. To be used as "Host Name on the controller"
    containerName=$sessionId$'-remote'

    #Modify the Session ID. (Append a 'r' to it)
    newRequestId=$sessionId$'r'
    sed -i "s/$sessionId/$newRequestId/" /dcloud/session-indemo.xml

    #Add Host tag to the session file (if not present already)
    hostLine=$'<host>'$containerName$'<\/host>'
    grep host /dcloud/session-indemo.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i $hostLine" /dcloud/session-indemo.xml

    #Get XML on a single line
    inDemoXML=$(cat /dcloud/session-indemo.xml  | tr -d '\n')
    inDemoXML=$(echo "$inDemoXML" | tr -d '\r')
 
    #Send Remote Request to Central Automation Controller
    #This places the in-demo automation request that will be executed in the demo environement by the local Startup Container
    command='curl -i -H "Content-Type: text/xml" -X POST -d '$"'"$inDemoXML$"'"'  http://198.19.254.51:31080/api/v1.0/task/r0'
    echo $command
    eval $command

    #Start StartUp  Container
    /bin/docker rm -f checkQueuedRequests  2>&1 1>/dev/null 
    /bin/docker run  -d --name checkQueuedRequests --hostname $containerName -it -v /var/nfs/dcv-automation-code:/root/scripts  -v /var/nfs/dcv-automation-logs:/var/log  mgarcias/dcv-automation-base   /usr/bin/python /root/scripts/checkQueuedRequests.py -d influxIndemo

fi

