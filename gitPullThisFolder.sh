##############################################################################################
#  Parsing Arguments
##############################################################################################
while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -u |--useremail)
    GITHUBUSER="$2"
    shift # past argument
    ;;
esac
shift # past argument or value
done

if [ -z $GITHUBUSER ]; then
    echo "You must provide github user's email address"
    echo " ./gitPushThisFolder.sh --useremail <EMAILADDRESS>"
    echo
   exit
fi 

#BRANCHNAME=$(pwd | awk  -F/ '{print $NF}')
BRANCHNAME=$(git branch | grep ^[*] | awk -F'* ' '{print $2}')

echo
echo
echo "!!!!!!!!!IMPORTANT!!!!!!!!!"
echo
echo
echo "This will pull  content from  Github."
echo "Branch $BRANCHNAME"
echo "Github User is : $GITHUBUSER"
echo
echo
read -p "Are you sure to continue? [N]? " -n 1 -r
echo
echo

if [[ $REPLY =~ ^[Yy]$ ]]
then
    git config --global user.email ""$GITHUBUSER""
    git pull -u origin "$BRANCHNAME"
    exit
fi

echo "Github Push operation cancelled"
exit



