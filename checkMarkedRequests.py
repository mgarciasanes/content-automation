#!/usr/bin/env python2.7
#####################################
# Import Modules
#####################################
import sys
import logging
import threading
import os
import os.path
import time
import datetime
import socket
import psutil


modulesPath=r'/root/scripts/modules/'
sys.path.append(modulesPath)
corePath=r'/root/scripts/core/'
sys.path.append(corePath)


import influx as influx
import genFunctions as gen
import logger as logger
import worker as worker
import signalMonitor


#####################################
# Global Variables
#####################################
#Hostname
host=socket.gethostname()
INFLUXDB='influxKube'

#####################################
# General Functions
#####################################


def refreshModules():
    logger.autolog(level=1, message='Refreshing Modules, reloading...')
    reload(influx)
    reload(gen)
    reload(logger)
    reload(worker)
    #reload(signalMonitor)


#####################################
#  FUNCTIONS IN LOOP
#####################################

def checkMarkedRequests():

    global host

    #markedSessions=influx.readFromInfluxDb('select "datacenter","host","recipePath","recipeName","type","demo","id","vpod","anycpwd","owner",demoStatus from (select "datacenter","host","demo","recipePath","recipeName","type","id","vpod","anycpwd","owner",last(demoStatus) as demoStatus  from inDemoAutomation_sessions  group by id )  where demoStatus='+"'toBeDeleted'" +' and host='+"'"+host+"'" +'  group by id',influxDb=INFLUXDB)
    markedSessions=influx.readFromInfluxDb('select *,id from (select *,last(demoStatus) from '+ str(influx.measurementSessions)+'  group by id )  where demoStatus='+"'toBeDeleted'" +' and host='+"'"+host+"'" +   ' group by id',influxDb=INFLUXDB)
    logger.autolog(level=1,message=markedSessions)


    if markedSessions == False:
        logger.autolog(message='Unable to get marked  requests')
        return False

    if markedSessions == []:
        logger.autolog(message='No marked requests found')
        return True

    for markedSession in markedSessions:
        tasks,errorMessage=gen.getRecipeStopTasks(markedSession['recipeName'],specificFolder=markedSession['recipePath'])
        influx.markSessionDeleting(markedSession,influxDb=INFLUXDB)
        if tasks == False:
            logger.autolog(message=errorMessage +' '+ str(markedSession['recipeName'])+ ' with ID ' + markedSession['id'])
            influx.markSessionError(markedSession,comments=errorMessage,influxDb=INFLUXDB)
        else:   
            failureFree,allTasksExecuted,executedTasks=worker.execTasks(markedSession,tasks,action='Stop')   
            influx.recordExecutedTasks(executedTasks,influxDb=INFLUXDB)
            if (failureFree and allTasksExecuted):
                logger.autolog(message='All tasks executed without errors while checking marked Session. Marking deleted')
                influx.markSessionDeleted(markedSession,influxDb=INFLUXDB)
            if (not failureFree):
                logger.autolog(message='Some tasks got errors while checking marked Sessions. Marking error')
                influx.markSessionError(markedSession,comments='Error while cleanup',influxDb=INFLUXDB)
            if (failureFree and not allTasksExecuted):
                logger.autolog(message='No Errors, leaving Session as marked')
                #influx.markSessionActive(activeSession,influxDb=INFLUXDB)



def getRequestFromQueue():

    global host

    query='select *,id from (select *,last(demoStatus) from '+ str(influx.measurementSessions)+'  group by id )  where demoStatus='+"'toBeDeleted'" +' and host='+"'"+host+"'" +   ' group by id'
    markedSession=influx.readFromInfluxDb(query,influxDb=INFLUXDB)

    logger.autolog(level=1,message=str(markedSession))

    if markedSession == False:
        logger.autolog(message='Unable to get marked  requests')
        return False

    if markedSession == []:
        logger.autolog(message='No marked requests found')
        return True

    logger.autolog(level=1,message='Found marked request for recipe ' +  markedSession['recipeName']+ ' with id ' + markedSession['id'] + '. It will be deleted now')
    return markedSession 


def executeRecipe(markedSession):
    tasks,errorMessage=gen.getRecipeStopTasks(markedSession['recipeName'],specificFolder=markedSession['recipePath'])
    influx.markSessionDeleting(markedSession,influxDb=INFLUXDB)
    if tasks == False:
        logger.autolog(message=errorMessage +' '+ str(markedSession['recipeName'])+ ' with ID ' + markedSession['id'])
        influx.markSessionError(markedSession,comments=errorMessage,influxDb=INFLUXDB)
    else:   
        failureFree,allTasksExecuted,executedTasks=worker.execTasks(markedSession,tasks,action='Stop')   
        influx.recordExecutedTasks(executedTasks,influxDb=INFLUXDB)
        if (failureFree and allTasksExecuted):
            logger.autolog(message='All tasks executed without errors while checking marked Session. Marking deleted')
            influx.markSessionDeleted(markedSession,influxDb=INFLUXDB)
        if (not failureFree):
            logger.autolog(message='Some tasks got errors while checking marked Sessions. Marking error')
            influx.markSessionError(markedSession,comments='Error while cleanup',influxDb=INFLUXDB)
        if (failureFree and not allTasksExecuted):
            logger.autolog(message='No Errors, leaving Session as marked')
            #influx.markSessionActive(activeSession,influxDb=INFLUXDB)




#####################################
#  LOOP
#####################################

def loop(loopMode,multithread,timer):

    refreshModules()
    
    if signalCheck.kill_now:
        logger.autolog(message="End of loop. I was killed gracefully.")
        quit()

    checkMarkedRequests()
    #markedSession=getRequestFromQueue()
    #if markedSession:
    #    executeRecipe(markedSession)
    #else:
    #    logger.autolog(message='No session details for Recipe execution...')


     
    if loopMode:
        logger.autolog(message='checkQueuedRequests running loop mode. Will excute in ' + str(timer)+' seconds...')

        #Loop Option 1
        #logger.autolog(level=1, message='Looping with threading.Timer')
        #A thread will call the loop function in time described in "timer"
        #threading.Timer(timer, loop,[loopMode, multithread, timer]).start() 


        #Loop Option 2
        #The code below is not actually a loop, but rather it will finish the script and restart itself automatially 
        #
        logger.autolog(level=1, message='Looping with execl. Restarting Script.')
        time.sleep(timer)
        try:
            p = psutil.Process(os.getpid())
            for handler in p.get_open_files() + p.connections():
                print("Restarting Script: Closing handler  " + str(handler))
                os.close(handler.fd)        
        except Exception as e:
            logger.autolog(level=1, message="Error Restarting the Script")
            logger.autolog(level=1, message=str(e))

        print("Restarting Script NOW!")
        python = sys.executable
        os.execl(python, python, *sys.argv) 


if __name__ == '__main__':

    ##################################
    ## Check Arguments
    ##################################

    from argparse import ArgumentParser
    parser = ArgumentParser("dCloud DCV Automation Script: checkMarkedRequests")
    parser.add_argument('-l', '--loop', help='Run Once or Loop. Values are  loop or once', default=True)
    parser.add_argument('-t', '--timer', help='Run Once or Loop. Values are  loop or once', default=60)
    parser.add_argument('-d', '--database', help='Specify database to use. Default is influxKube ', default='influxKube')
    parser.add_argument('-dp', '--databaseport', help='Specify database port to use. Default is False  ', default="noPort")
    parser.add_argument('-loc', '--location', help='Specify Location where this script is running. Default is False ', default="noLocation")
    parser.add_argument('-x', '--multithread', help='Run tasks for all sessions in concurrent or sequential. Values are true or false. default is false.', default=False)
    args = parser.parse_args()

    ################################
    #Start SSH Service
    ################################
    
    #os.system(r'/usr/sbin/sshd -D &')
    
    #####################################
    ## Configure Logger
    #####################################

    logger.setLogFiles(newLogFile='checkMarkedRequests.log',newLogDebugFile='checkMarkedRequests_DEBUG.log')
    logger.setLogger()

    #####################################
    ## Configure Signal Check 
    #####################################

    signalCheck = signalMonitor.SignalMonitor()

    #####################################
    ## Start Loop
    #####################################
    #global INFLUXDB
    #INFLUXDB=args.database

    #DB Port to be used. If not specified as argument, use the one specified in Creds file, section "database"
    if args.databaseport=="noPort":
        logger.autolog(level=1,message="No database port specified, using port from Cred file")
        INFLUXDB=args.database
    else:
        logger.autolog(level=1,message="Database port specified, using it. " +str(args.databaseport))
        INFLUXDB={'database':args.database,'port':args.databaseport}


    #Get Cluster Location. If not specified as argument, use the valued obtained directly from Kubernetes' label
    if args.location=="noLocation":
        cmd=r'kubectl get services kubernetes --namespace=default -o jsonpath={.metadata.labels.location} > /root/location'
        logger.autolog(level=1,message=str(cmd))
        os.system(cmd)
        locationFile=open(r'/root/location','r')
        location=locationFile.read().strip('\n')
        locationFile.close()
        logger.autolog(level=1,message="We are working with location  " + str(location))
        LOCATION=location
    else:
        LOCATION=str(args.location)


    loop(args.loop,args.multithread,int(args.timer))


