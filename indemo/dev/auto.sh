##############################################################################################
#  Parsing Arguments
##############################################################################################

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -i|--indemo-recipe)
    inDemoRecipeName="$2"
    shift # past argument
    ;;
    -ip|--indemo-recipe-path)
    inDemoRecipePath="$2"
    shift # past argument
    ;;
    -iwc|--indemo-worker-cloud)
    inDemoCloud="$2"
    shift # past argument
    ;;
    -iwg|--indemo-worker-geo)
    inDemoGeo="$2"
    shift # past argument
    ;;
    -iwl|--indemo-worker-location)
    inDemoLocation="$2"
    shift # past argument
    ;;
    -d|--demo-recipe)
    demoRecipeName="$2"
    shift # past argument
    ;;
    -dp|--demo-recipe-path)
    demoRecipePath="$2"
    shift # past argument
    ;;
    -dwc|--demo-worker-cloud)
    demoCloud="$2"
    shift # past argument
    ;;
    -dwg|--demo-worker-geo)
    demoGeo="$2"
    shift # past argument
    ;;
    -dwl|--demo-worker-location)
    demoLocation="$2"
    shift # past argument
    ;;
    -b|--branch-name)
    BRANCHNAME="$2"
    shift # past argument
    ;;
    -api|--api-port)
    APIPORT="$2"
    shift # past argument
    ;;
    -db|--db-api-port)
    DBAPIPORT="$2"
    shift # past argument
    ;;
    -r|--rabbitserver)
    rabbitserver="$2"
    shift # past argument
    ;;
    -rep|--redisport)
    redisport="$2"
    shift # past argument
    ;;
    -re|--redisserver)
    redisserver="$2"
    shift # past argument
    ;;
    -rp|--rabbitport)
    rabbitport="$2"
    shift # past argument
    ;;
    -d|--database)
    DATABASE="$2"
    shift # past argument
    ;;
    --default)
    helpMe="YES"
    ;;
    *)
           # unknown option
    ;;
esac
shift # past argument or value
done


##############################################################################################
#  Set Defaults
##############################################################################################


if [ -z "$APIPORT" ]; then
    APIPORT="31080"
fi

if [ -z "$DATABASE" ]; then
    DATABASE="influxExternal"
fi

if [ -z "$DBAPIPORT" ]; then
    DBAPIPORT="0"
fi

if [ -z "$BRANCHNAME" ]; then
    BRANCHNAME="master"
fi

if [ -z "$rabbitserver" ]; then
    rabbitserver="rabbitExternal"
fi 

if [ -z "$rabbitport" ]; then
    rabbitport="0"
fi 

if [ -z "$redisserver" ]; then
    redisserver="redisExternal"
fi 

if [ -z "$redisport" ]; then
    redisport="0"
fi 


##############################################################################################
# Development AUTOMATION PROCESSING
##############################################################################################


    echo
    echo "###################################################################"
    echo
    echo "Preparing DEV environment "
    echo
    echo "RABBIT SERVER is $rabbitserver "
    echo "RABBIT PORT is $rabbitport"
    echo "REDIS SERVER is $redisserver "
    echo "REDIS PORT is $redisport"
    echo "DB SERVER is $DATABASE "
    echo "DB PORT is $DBAPIPORT"
    echo "Branchname is $BRANCHNAME"
    echo "###################################################################"
    echo
    

    #################################################
    # Install required packages 
    #################################################
    #
    # This is not needed if the Centos7 VM is installed in Centos7 vm
    #
    #Install Docker
    #echo
    #echo "If not installed, install docker now..."
    #echo
    #yum -y install docker
    #systemctl enable docker
    #systemctl start docker

   
    #########################################
    #Prepare XML for Development Automation 
    #########################################    

    #Duplicate  file with session details
    rm -f /dcloud/session-dev.xml
    cp /dcloud/session.xml /dcloud/session-dev.xml

    #Add Recipe Name tag to the session file (if not present already)
    grep '<recipeName>' /dcloud/session-dev.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <recipeName>$inDemoRecipeName<\/recipeName>" /dcloud/session-dev.xml

    #Add recipePath tag to the session file (if not present already)
    grep '<recipePath>' /dcloud/session-dev.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <recipePath>$inDemoRecipePath<\/recipePath>" /dcloud/session-dev.xml

    #Add pod tag to the session file (if not present already)
    grep '<pod>' /dcloud/session-dev.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <pod>dpod<\/pod>" /dcloud/session-dev.xml


    #Get Session ID from session file  (and remove the change of line characters)
    #Note: The following command will take the value from xml node <id>
    #       If more than one <id> exists, it will take the first one.  
    sessionId=$(sed '/<id>/!d;s/.*<id>\(.*\)<\/id>/\1/;/./q' /dcloud/session-dev.xml)
    #sessionId=$(sed  -n  -e 's/.*<id>\(.*\)<\/id>/\1/p' /dcloud/session-dev.xml)
    sessionId=$(echo "$sessionId" | tr -d '\r')

    #Get Session DATACENTER from session file  (and remove the change of line characters)
    #Note: The following command will take the value from xml node <datacenter>
    #       If more than one <datacenter> exists, it will take the first one.
    dc=$(sed '/<datacenter>/!d;s/.*<datacenter>\(.*\)<\/datacenter>/\1/;/./q' /dcloud/session-dev.xml)
    dc=$(echo "$dc" | tr -d '\r')

    location=dcloud."$dc".dpod."$sessionId"

    #Add location tag to the session file (if not present already)
    grep '<location>' /dcloud/session-dev.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <location>$location<\/location>" /dcloud/session-dev.xml


    #Modify the Session ID. (Append a '-inDpod' to it)
    newRequestId=$sessionId$'-dPod'
    sed -i "s/<id>$sessionId<\/id>/<id>$newRequestId<\/id>/" /dcloud/session-dev.xml


    ############################################################
    #Start Container Locally and execute worker1.py
    ############################################################

    #Create hostname for the container. To be used as "Host Name on the controller"
    containerName=$sessionId$'-dPod'

    #Remove any existing instance of the container
    /bin/docker rm -f workerInDpod  2>&1 1>/dev/null 

    #Create command
    cmd="/bin/docker run -d -it  --name workerInDpod --hostname $containerName  -v /var/nfs/repo:/root/repo -v /var/nfs/code/"$BRANCHNAME":/root/scripts  -v /var/nfs/logs/"$BRANCHNAME":/var/log  --restart always mgarcias/dcv-automation-base:latest   /usr/bin/python /root/scripts/worker1.py -d $DATABASE -dp "$DBAPIPORT" --location $location -r $rabbitserver -rp $rabbitport -re $redisserver -rep $redisport"

    #Execute command: Start container, attach volumes...
    echo $cmd
    /bin/docker run -d -it  --name workerInDpod --hostname $containerName  -v /var/nfs/repo:/root/repo -v /var/nfs/code/"$BRANCHNAME":/root/scripts  -v /var/nfs/logs/"$BRANCHNAME":/var/log  --restart always mgarcias/dcv-automation-base:latest   /usr/bin/python /root/scripts/worker1.py -d $DATABASE -dp "$DBAPIPORT" --location $location -r $rabbitserver -rp $rabbitport  -re $redisserver -rep $redisport


##############################################################################################
#  Anything else specific to your content?  Do it here....
##############################################################################################

