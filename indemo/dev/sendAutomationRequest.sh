#rsing Arguments
##############################################################################################

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -ci|--controller-ip)
    CONTROLLERIP="$2"
    shift # past argument
    ;;
    -api|--api-port)
    APIPORT="$2"
    shift # past argument
    ;;
    -au|--api-url)
    APIURL="$2"
    shift # past argument
    ;;
    --default)
    helpMe="YES"
    ;;
    *)
           # unknown option
    ;;
esac
shift # past argument or value
done


##############################################################################################
#  Set Defaults
##############################################################################################

if [ -z "$CONTROLLERIP" ]; then
    CONTROLLERIP="198.19.254.51"
fi 

if [ -z "$APIPORT" ]; then
    APIPORT="31080"
fi


if [ -z "$APIURL" ]; then
    APIURL="/api/v2.0/request/0"
fi 



##############################################################################################
# Development AUTOMATION PROCESSING
##############################################################################################


    #Format XML on a single line
    devXML=$(cat /dcloud/session-dev.xml  | tr -d '\n')
    devXML=$(echo "$devXML" | tr -d '\r')
 
    ##########################################################
    #Send Request API to Controller
    ##########################################################
    #This places the  automation request that will be executed in the dev session 
    #Build Command

    command='curl -v -i -H "Content-Type: application/xml" -X POST -d '$"'"$devXML$"'""  http://$CONTROLLERIP:$APIPORT$APIURL"


    #Log Command
    echo $command
    #Execute Command
    eval $command

