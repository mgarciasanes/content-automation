##############################################################################################
#  Bootstrap
#  This script does the following:
#  Downloads code from remote repository (shared services) to a local folder  
#  Executes script "auto.sh" from the downloaded code,  while passing all the arguments recieved by this script
#
#  It also has a auto-update feature. If new-bootstrap-url and new-bootstrap-file are specified
#  the new file will be downloaded from the provided URL and executed with all the arguments
#  received by this script. 
##############################################################################################


##############################################################################################
#  Parsing Arguments
##############################################################################################

ARGS="$@"

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -ci|--controller-ip)
    CONTROLLERIP="$2"
    shift # past argument
    ;;
    -r|--repo-ip)
    REPOIP="$2"
    shift # past argument
    ;;
    -b|--branch-name)
    BRANCHNAME="$2"
    shift # past argument
    ;;
    -api|--api-port)
    APIPORT="$2"
    shift # past argument
    ;;
    -db|--db-api-port)
    DBAPIPORT="$2"
    shift # past argument
    ;;
    -ap|--auto-path)
    AUTOPATH="$2"
    shift # past argument
    ;;
    -ad|--auto-destination)
    AUTODEST="$2"
    shift # past argument
    ;;
    -ae|--auto-exec)
    AUTOEXEC="$2"
    shift # past argument
    ;;
    -args|--pass-args)
    PASSARGS="$2"
    shift # past argument
    ;;
    -mp|--mount-path)
    MOUNTPATH="$2"
    shift # past argument
    ;;
    -cr|--code-root)
    CODEROOT="$2"
    shift # past argument
    ;;

    -nbu|--new-bootstrap-url)
    BOOTSTRAPURL="$2"
    shift # past argument
    ;;

    -nbn|--new-bootstrap-file)
    BOOTSTRAPFILE="$2"
    shift # past argument
    ;;


    -lf|--log-file)
    LOGFILE="$2"
    shift # past argument
    ;;
    *)
           # unknown option
    ;;
esac
shift # past argument or value
done

##############################################################################################
#  Setting Default values
##############################################################################################

# Default log file location is /root/bootstrap.log
if [ -z "$LOGFILE" ]; then
LOGFILE="/root/bootstrap.log"
fi

# Default Controller IP is currently 198.19.254.159 
if [ -z "$CONTROLLERIP" ]; then
CONTROLLERIP="198.19.254.159"
fi

# Default branch name is master
if [ -z "$BRANCHNAME" ]; then
BRANCHNAME="master"
fi

# Default Script remote location is /indemo/common/auto.sh
# Most likelt you need to create your own script and specify where it is located
if [ -z "$AUTOPATH" ]; then
AUTOPATH="/indemo/common/auto.sh"
fi

# Default folder location of the downloaded script is /root/auto.sh
if [ -z "$AUTODEST" ]; then
AUTODEST="/root/auto.sh"
fi

# Default is the same as AUTODEST
# The intented use for this value is to handle other kind of scripts (python, perl,...) that will need a specific way of calling
# Currently not supported 
if [ -z "$AUTOEXEC" ]; then
AUTOEXEC=$AUTODEST
fi

# By default, the remote NFS folder will be mounted on temp folder
if [ -z "$MOUNTPATH" ]; then
MOUNTPATH="/root/temp/"
fi

# By default, the root code folder on the repo server is  /var/nfs/code 
if [ -z "$CODEROOT" ]; then
CODEROOT="/var/nfs/code/"
fi

# By default, the root repo folder on the repo server is  /var/nfs/code 
if [ -z "$REPOROOT" ]; then
REPOROOT="/var/nfs/repo/"
fi

# By default, the root log folder on the log server is  /var/nfs/logs 
if [ -z "$LOGROOT" ]; then
LOGROOT="/var/nfs/logs/"
fi

# By default, the branch is master and the api port 31080
if [ -z "$APIPORT" ]; then
APIPORT="31080"
fi

# By default, the branch is master and the DB api port 31886
if [ -z "$DBAPIPORT" ]; then
DBAPIPORT="31886"
fi

# By default, all the arguments are passed to the copied script 
if [ -z "$PASSARGS" ]; then
PASSARGS="YES"
fi
if [ "$PASSARGS" = "no" ]; then
ARGS=""
fi


# By default, the repo ip address is 198.19.254.160
if [ -z "$REPOIP" ]; then
REPOIP="198.19.254.160"
fi


# By default, the new bootstrap filename is newbootstrap.sh
if [ -z "$BOOTSTRAPFILE" ]; then
BOOTSTRAPFILE="newbootstrap.sh"
fi




##############################################################################################
#  Usage Example:
##############################################################################################
echo
echo
echo "dCloud Bootstrap Script for Content Automation"
echo
echo "Example of usage for branch named mgarcias-31081 running on API port 31081 and DB port 31281"
echo "./bootstrap.sh --indemo-recipe esa-indemo-v1  --indemo-recipe-path sec/ --branch-name mgarcias-31081 --api-port 31081 --db-api-port 31281"
echo
echo


##############################################################################################
#  BOOTSTRAP UPDATE
##############################################################################################


if [ -n "$BOOTSTRAPURL" ] && [ -n "$BOOTSTRAPFILE" ]; then

    #Delete LOG File
    now=$(date)
    echo "$now: Updating Bootstrap" > "$LOGFILE"

    cmd="yum -y install wget "$LOGFILE" 2>&1 && \
    rm -f /root/"$BOOTSTRAPFILE"  >> "$LOGFILE" 2>&1  && \
    wget -P /root/  "$BOOTSTRAPURL$BOOTSTRAPFILE"  >> "$LOGFILE" 2>&1 && \
    chmod 777 /root/"$BOOTSTRAPFILE"   >>  "$LOGFILE"  2>&1  && \
    /root/"$BOOTSTRAPFILE" "$ARGS" >>  "$LOGFILE"  2>&1 && \
    echo 'BOOTSTRAP UPDATED' >> "$LOGFILE

    #Log command to be executed
    echo "Command to execute" >> "$LOGFILE"
    echo "$cmd" >> "$LOGFILE"
    echo "$cmd"

    #Execute Command
    eval $cmd

    exit
fi

##############################################################################################
#  Executing
##############################################################################################

#Delete LOG File
now=$(date)
echo "$now: Starting Bootstrap" > "$LOGFILE"

#Prepare the command
#cmd="cp "$CODEROOT$BRANCHNAME$AUTOPATH" "$AUTODEST" >> "$LOGFILE" 2>&1 && \
#chmod 777 "$AUTODEST"   >>  "$LOGFILE"  2>&1  && \
#"$AUTODEST" "$ARGS" >>  "$LOGFILE"  2>&1"



cmd="yum -y install wget >> "$LOGFILE" 2>&1 && \
rm -f "$AUTODEST"  >> "$LOGFILE" 2>&1  && \
rm -fr "$CODEROOT$BRANCHNAME" >> "$LOGFILE" 2>&1  &&  \
rm -fr "$REPOROOT" >> "$LOGFILE" 2>&1  &&  \
rm -fr "$LOGROOT$BRANCHNAME" >> "$LOGFILE" 2>&1  &&  \
mkdir -p "$CODEROOT$BRANCHNAME" >> "$LOGFILE" 2>&1  &&  \
mkdir -p "$REPOROOT" >> "$LOGFILE" 2>&1  &&  \
mkdir -p "$LOGROOT$BRANCHNAME" >> "$LOGFILE" 2>&1  &&  \
wget -X /code/master/http -r -np -R 'index.html*,REQ_URL' -nH --cut-dirs=2  -P "$CODEROOT$BRANCHNAME"/  http://"$REPOIP"/code/"$BRANCHNAME"/ >> "$LOGFILE" 2>&1 && \
echo 'Code Downloaded $?' >> "$LOGFILE" 2>&1 && \
wget -r -np -R 'index.html*,REQ_URL' -nH --cut-dirs=1  -P "$REPOROOT"/  http://"$REPOIP"/repo/ >> "$LOGFILE" 2>&1 && \
echo 'Repo Downloaded $?' >> "$LOGFILE" 2>&1 && \
cp "$CODEROOT$BRANCHNAME$AUTOPATH" "$AUTODEST" >> "$LOGFILE" 2>&1 && \
chmod 777 "$AUTODEST"   >>  "$LOGFILE"  2>&1  && \
"$AUTODEST" "$ARGS" >>  "$LOGFILE"  2>&1 && \
echo 'DONE' >> "$LOGFILE


#Log command to be executed
echo "Command to execute" >> "$LOGFILE"
echo "$cmd" >> "$LOGFILE"
echo "$cmd"

#Execute Command
eval $cmd