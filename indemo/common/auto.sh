##############################################################################################
#  AUTO.SH
#
#  This script is used to deploy the automation workers inside a VPOD. 
#  If the arguments are provided, it will also send automation API request to the controller 
#  for both, demo and indemo 
#  
#  It assumes there are three controllers, RTP, LON and SNG. One worker is deployed for 
#  each controller. 
#
##############################################################################################



##############################################################################################
#  Parsing Arguments
##############################################################################################

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -i|--indemo-recipe)
    inDemoRecipeName="$2"
    shift # past argument
    ;;
    -ip|--indemo-recipe-path)
    inDemoRecipePath="$2"
    shift # past argument
    ;;
    -iwc|--indemo-worker-cloud)
    inDemoCloud="$2"
    shift # past argument
    ;;
    -iwg|--indemo-worker-geo)
    inDemoGeo="$2"
    shift # past argument
    ;;
    -iwl|--indemo-worker-location)
    inDemoLocation="$2"
    shift # past argument
    ;;
    -d|--demo-recipe)
    demoRecipeName="$2"
    shift # past argument
    ;;
    -dp|--demo-recipe-path)
    demoRecipePath="$2"
    shift # past argument
    ;;
    -dwc|--demo-worker-cloud)
    demoCloud="$2"
    shift # past argument
    ;;
    -dwg|--demo-worker-geo)
    demoGeo="$2"
    shift # past argument
    ;;
    -dwl|--demo-worker-location)
    demoLocation="$2"
    shift # past argument
    ;;
    -ci|--controller-ip)
    CONTROLLERIP="$2"
    shift # past argument
    ;;
    -r|--repo-ip)
    REPOIP="$2"
    shift # past argument
    ;;
    -b|--branch-name)
    BRANCHNAME="$2"
    shift # past argument
    ;;
    -api|--api-port)
    APIPORT="$2"
    shift # past argument
    ;;
    -db|--db-api-port)
    DBAPIPORT="$2"
    shift # past argument
    ;;
    -r|--rabbitserver)
    rabbitserver="$2"
    shift # past argument
 
    ;;
    -rep|--redisport)
    redisport="$2"
    shift # past argument
 
    ;;
    -re|--redisserver)
    redisserver="$2"
    shift # past argument
 
    ;;
    -rp|--rabbitport)
    rabbitport="$2"
    shift # past argument


    ;;
    -d|--database)
    DATABASE="$2"
    shift # past argument
    ;;
    --default)
    helpMe="YES"
    ;;
    *)
           # unknown option
    ;;
esac
shift # past argument or value
done


##############################################################################################
#  Set Defaults
##############################################################################################

if [ -z "$CONTROLLERIP" ]; then
    CONTROLLERIP="198.19.254.51"
fi 

if [ -z "$APIPORT" ]; then
    APIPORT="31080"
fi

if [ -z "$DATABASE" ]; then
    DATABASE="influxExternal"
fi

if [ -z "$DBAPIPORT" ]; then
    DBAPIPORT="0"
fi

if [ -z "$BRANCHNAME" ]; then
    BRANCHNAME="master"
fi

if [ -z "$REPOIP" ]; then
    REPOIP="198.19.254.160"
fi 

if [ -z "$APIURL" ]; then
    APIURL="/api/v2.0/request/0"
fi 

if [ -z "$rabbitserver" ]; then
    rabbitserver="rabbitExternal"
fi 

if [ -z "$rabbitport" ]; then
    rabbitport="24001"
fi 

if [ -z "$redisserver" ]; then
    redisserver="redisExternal"
fi 

if [ -z "$redisport" ]; then
    redisport="0"
fi 




##############################################################################################
#  Print Usage Example....
##############################################################################################


if [ -z "$inDemoRecipeName" ] &&  [ -z "$demoRecipeName" ] ; then
    echo
    echo "Automation Bootstrap. Examples of use:"
    echo
    echo " Request both, indemo and demo automation: "
    echo " ./auto.sh --indemo-recipe contiv-v1-indemo  --indemo-recipe-path dcv/ --demo-recipe contiv-v1 --demo-recipe-path dcv/ "
    echo " ./auto.sh  -i contiv-v1-indemo -ip dcv/ -d contiv-v1  -dp dcv/ "
    echo
    echo " Request indemo automation only: "
    echo " ./auto.sh  --demo-name 'ACI Contiv v1' --indemo-recipe contiv-v1-indemo  --indemo-recipe-path dcv/ "
    echo " ./auto.sh  -i contiv-v1-indemo -ip dcv/  "
    echo
    echo " Request demo automation only: "
    echo " ./auto.sh  --demo-recipe contiv-v1 --demo-recipe-path dcv/ "
    echo " ./auto.sh  -d contiv-v1  -dp dcv/   "
    echo
    exit
fi


##############################################################################################
#  DEMO AUTOMATION PROCESSING
##############################################################################################

if [ -n "$demoRecipeName" ] && [ -n "$demoRecipePath" ]; then
    echo
    echo "###################################################################"
    echo "Preparing environment in order to execute Demo Automation Request"
    echo "Controller IP is $CONTROLLERIP"
    echo "API port is $APIPORT"
    echo "Branchname is $BRANCHNAME"
    echo "###################################################################"
    echo

    #########################################
    #Prepare XML for Demo Automation Request
    #########################################
    
    #Copy Original file with session details
    rm -f /dcloud/session-sharedservices.xml
    cp /dcloud/session.xml /dcloud/session-sharedservices.xml

    #Add Recipe Name tag to the session file (if not present already)
    grep '<recipeName>'  /dcloud/session-sharedservices.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <recipeName>$demoRecipeName<\/recipeName>" /dcloud/session-sharedservices.xml

    #Add recipePath tag to the session file (if not present already)
    grep '<recipePath>' /dcloud/session-sharedservices.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <recipePath>$demoRecipePath<\/recipePath>" /dcloud/session-sharedservices.xml

    #Add pod tag to the session file (if not present already)
    grep '<pod>' /dcloud/session-sharedservices.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <pod>sharedservices<\/pod>" /dcloud/session-sharedservices.xml


   #Format XML on a single line
    sharedServicesXML=$(cat /dcloud/session-sharedservices.xml  | tr -d '\n')
    sharedServicesXML=$(echo "$sharedServicesXML" | tr -d '\r')

    ##########################################################
    #Send Request API to Controller
    ###########################################################
    
    #Build command

    command='curl -i -H "Content-Type: application/xml" -X POST -d '$"'"$sharedServicesXML$"'""  http://$CONTROLLERIP:$APIPORT$APIURL"

    #Log Command
    echo $command
    #Execute Command
    eval $command
    
else
    echo "No Demo request to be sent..."
fi

##############################################################################################
#  IN-DEMO AUTOMATION PROCESSING
##############################################################################################

if [ -n "$inDemoRecipeName" ] && [ -n "$inDemoRecipePath" ]; then

    echo
    echo "###################################################################"
    echo "Preparing environment in order to execute in-demo automation"
    echo "Controller IP is $CONTROLLERIP"
    echo "API port is $APIPORT"
    echo "Branchname is $BRANCHNAME"
    echo "InfluxDB port to be used is: $DBAPIPORT"
    echo "###################################################################"
    echo
    
    #################################################
    # Install required packages 
    #################################################
    #
    # This is not needed if the Centos7 VM is installed in Centos7 vm
    #
    #Install Docker
    echo
    echo "If not installed, install docker now..."
    echo
    yum -y install docker
    systemctl enable docker
    systemctl start docker

   
    #########################################
    #Prepare XML for Demo Automation Request
    #########################################    

    #Duplicate  file with session details
    rm -f /dcloud/session-indemo.xml
    cp /dcloud/session.xml /dcloud/session-indemo.xml

    #Add Recipe Name tag to the session file (if not present already)
    grep '<recipeName>' /dcloud/session-indemo.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <recipeName>$inDemoRecipeName<\/recipeName>" /dcloud/session-indemo.xml

    #Add recipePath tag to the session file (if not present already)
    grep '<recipePath>' /dcloud/session-indemo.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <recipePath>$inDemoRecipePath<\/recipePath>" /dcloud/session-indemo.xml

    #Add pod tag to the session file (if not present already)
    grep '<pod>' /dcloud/session-indemo.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <pod>vpod<\/pod>" /dcloud/session-indemo.xml


    #Get Session ID from session file  (and remove the change of line characters)
    #Note: The following command will take the value from xml node <id>
    #       If more than one <id> exists, it will take the first one.  
    sessionId=$(sed '/<id>/!d;s/.*<id>\(.*\)<\/id>/\1/;/./q' /dcloud/session-indemo.xml)
    #sessionId=$(sed  -n  -e 's/.*<id>\(.*\)<\/id>/\1/p' /dcloud/session-indemo.xml)
    sessionId=$(echo "$sessionId" | tr -d '\r')

    #Get Session DATACENTER from session file  (and remove the change of line characters)
    #Note: The following command will take the value from xml node <datacenter>
    #       If more than one <datacenter> exists, it will take the first one.
    dc=$(sed '/<datacenter>/!d;s/.*<datacenter>\(.*\)<\/datacenter>/\1/;/./q' /dcloud/session-indemo.xml)
    dc=$(echo "$dc" | tr -d '\r')

    location=dcloud."$dc".vpod."$sessionId"

    #Add location tag to the session file (if not present already)
    grep '<location>' /dcloud/session-indemo.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <location>$location<\/location>" /dcloud/session-indemo.xml


    #Modify the Session ID. (Append a '-inVpod' to it)
    newRequestId=$sessionId$'-inVpod'
    sed -i "s/<id>$sessionId<\/id>/<id>$newRequestId<\/id>/" /dcloud/session-indemo.xml

    #Create hostname for the container. To be used as "Host Name on the controller"
    containerName=$sessionId$'-inVpod'

    #Format XML on a single line
    inDemoXML=$(cat /dcloud/session-indemo.xml  | tr -d '\n')
    inDemoXML=$(echo "$inDemoXML" | tr -d '\r')
 
    ##########################################################
    #Send Request API to Controller
    ##########################################################
    #This places the in-demo automation request that will be executed in the demo environement by the local Startup Container
    #Build Command

    command='curl -v -i -H "Content-Type: application/xml" -X POST -d '$"'"$inDemoXML$"'""  http://$CONTROLLERIP:$APIPORT$APIURL"


    #Log Command
    echo $command
    #Execute Command
    eval $command

    ############################################################
    # Start Container Locally and execute worker1.py
    #
    # The code below will attempt to delete any existing container with the same name. 
    # It will deploy one container for each broker (controller)
    # 
    # Note:
    # On the current environment, the container trying to connect to the local broker using the dc-x.com name will fail. 
    # For that reason, we are adding one more "broker" using the local IP address instead. It is expected then, 
    # that from the 4 containers one will fail (the one connecting the local broker)
    #
    ############################################################

    MAXCOUNT=1
    for broker in dcv-automation-amqp.svpod.dc-01.com dcv-automation-amqp.svpod.dc-02.com dcv-automation-amqp.svpod.dc-03.com 198.19.254.159 ; do
        COUNTER=1
        until [ $COUNTER -gt $MAXCOUNT ];do
            CMD=$'/bin/docker rm -f worker-'$broker$'-'$COUNTER$' ; /bin/docker run -d -it  --name worker-'$broker$'-'$COUNTER$' --hostname dcv-automation-worker-'$broker$'-'$COUNTER$'  -v /var/nfs/repo:/root/repo -v /var/nfs/code/'$BRANCHNAME$':/root/scripts  -v /var/nfs/logs/'$BRANCHNAME$':/var/log  --restart always mgarcias/dcv-automation-base:latest   /usr/bin/python /root/scripts/worker1.py  --location '$location$' -R '$broker$'  -rp '$rabbitport

             #Start container, attach volumes...
             echo $CMD
             echo "Creating worker $COUNTER for Broker  dcv-automation-amqp.svpod.dc-$broker.com on port $rabbitport. Attempting to deploy $MAXCOUNT workers..."
             let COUNTER+=1
             eval $CMD
        done
    done










else
    echo "No in-Demo request to be sent..."
fi



##############################################################################################
#  Anything else specific to your content?  Do it here....
##############################################################################################
#reboot