[Back to Main Page](https://github.com/dcloud-automation/content-automation/blob/master/README.md)

# auto.sh

In normal operation, this script will be downloaded and executed by bootstrap.sh runnig on a VM, normally Centos7-tools, locally inside the session.  The arguments auto.sh require are provided by bootstrap.sh.  Examples below are provided as a reference for testing or development. 

During creation of new content, developers may need to modify this script. In that case, the new script should be stored on a separate folder (i.e indemo/sec/demo1/auto.sh , indemo/dcv/demo2/auto.sh , etc) and the location of it should be provided to the bootstrap.sh script by using argument --auto-path


This script does the following:
1. <h4>When --demo-recipe  and --demo-recipe-path are present </h4>

    a. Read session file located at /dcloud/session.xml

    b. Add xml node TYPE:queued
    
    c. Add xml node RECIPENAME:VALUE  [This value is provided on argument --demo-recipe]
    
    d. Add xml node RECIPEPATH:VALUE  [This value is provided on argument --demo-recipe-path]
    
    e. Send the XML file as payload to API Controller          [API IP and PORT are provided on argument --controller-ip and --api-port]
    
    
2. <h4>When --indemo-recipe and --indemo-recipe-path are present </h4>

    a. Read session file located at /dcloud/session.xml
    
    b. Install required packages (docker, nfs-utils)
    
    c. Mount NFS volumes (code, logs, repo) from REPO server   [REPO IP is obtained from argument --repo-ip]
    
    d. Add xml node TYPE  <type>remote</type>
    
    e. Add xml node RECIPENAME:VALUE  [This value is provided on argument --demo-recipe]
    
    f. Add xml node RECIPEPATH:VALUE  [This value is provided on argument --demo-recipe-path]
    
    g. Add xml node HOST:VALUE        [This value is the concat of sessionId in session file and string "-indemo"]
    
    h. Edit xml node ID:VALUE         [This value is the concat of sessionId in session file and string "-indemo"]
    
    i. Send the XML file as payload to API Controller   [API IP and PORT are provided on argument --controller-ip and --api-port]
    
    j. Run a docker container that hosts the checkQueuedRequests.py script. This will connect to the controller's DB, get the list of tasks and execute it locally inside the session. 
    
    
<h2> Standard Arguments : </h2>

<h4> --demo-recipe </h4>
Name of Recipe for Demo Automation 
 
Default: None

<h4> --demo-recipe-path </h4>
Location path where the recipe file is stored 

Default: None

<h4> --indemo-recipe </h4>
Name of Recipe for In-Demo Automation 

Default: None

<h4> --indemo-recipe-path </h4>
Location path where the recipe file is stored 

Default: None

<h4> --controller-ip </h4>
This is the IP address of the controller. The API requests will be sent to this IP

Default: 198.19.254.51

<h4> --repo-ip </h4>
This is the IP address of the repository (where the code is stored).

Default: 198.19.254.160

<h4> --branch-name </h4>
This is the name of the Github branch, which matches the name of the folder where the code is stored

Default: master

<h4> --api-port </h4>
This is the Port where the controller is listening for API requests

Default: 31080

<h4> --db-api-port </h4>
This is the Port where the controller's database is listening for queries.

Default: 31886

<h2> Non-standard Arguments : </h2>

This script should be taken as a template. When more actions are to be taken, addional arguments should be added as needed by the developer.


<h2> Usage Example 1: In-demo Automation on production controller (master) </h2>

Automation Type:      indemo  (automation execution happens inside of the session)

Controller Instance:  master

Recipe Name:          myRecipeName

Recipe Path:          sec/

Auto.sh execution command: 
<h4>./auto.sh --indemo-recipe myRecipeName  ---indemo-recipe-path sec/ </h4>


<h2> Usage Example 2: In-demo Automation on development controller instance  </h2>

Automation Type:      indemo  (automation execution happens inside of the session)

Controller Instance:  myBranch-31085

Recipe Name:          myRecipeName

Recipe Path:          sec/

Auto.sh execution command
<h4>./auto.sh --indemo-recipe myRecipeName --indemo-recipe-path sec/ --branch-name myBranch-31085 --api-port 31085 --db-api-port 31285 </h4>


<h2> Usage Example 3: Demo Automation on production controller (master) </h2>

Automation Type:      Demo  (automation execution happens outside of the session)

Controller Instance:  master

Recipe Name:          myRecipeName

Recipe Path:          sec/

Auto.sh execution command: 
<h4>./auto.sh --demo-recipe myRecipeName  ---demo-recipe-path sec/ </h4>


<h2> Usage Example 4: Demo Automation on development controller instance </h2>

Automation Type:      Demo  (automation execution happens outside of the session)

Controller Instance:  myBranch-31085

Recipe Name:          myRecipeName

Recipe Path:          sec/

Auto.sh execution command: 
<h4> ./auto.sh --demo-recipe myRecipeName  ---demo-recipe-path sec/ --branch-name myBranch-31085 --api-port 31085 --db-api-port 31285 </h4>


<h2> Usage Example 5: Demo and In-demo Automation on production controller (master) </h2>

Automation Type:      Demo and indemo  (automation execution happens inside and outside of the session)

Controller Instance:  master

Demo Recipe Name:          myDemoRecipeName

Demo Recipe Path:          sec/

In-demo Recipe Name:       myIndemoRecipeName

In-demo Recipe Path:       sec/

Bootstrap execution command: 
<h4>./auto.sh --demo-recipe myRecipeName  ---demo-recipe-path sec/  --indemo-recipe myIndemoRecipeName  --indemo-recipe-path sec/</h4>


<h2> Usage Example 6: Demo and In-demo Automation on development controller instance </h2>

Automation Type:      Demo and indemo  (automation execution happens inside and outside of the session)

Controller Instance:  myBranch-31085

Demo Recipe Name:          myDemoRecipeName

Demo Recipe Path:          sec/

In-demo Recipe Name:       myIndemoRecipeName

In-demo Recipe Path:       sec/

Bootstrap execution command: 
<h4> ./auto.sh --demo-recipe myRecipeName  ---demo-recipe-path sec/  --indemo-recipe myIndemoRecipeName  --indemo-recipe-path sec/ --branch-name myBranch-31085 --api-port 31085 --db-api-port 31285 </h4>



[Back to Main Page](https://github.com/dcloud-automation/content-automation/blob/master/README.md)

