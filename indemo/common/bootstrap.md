[Back to Main Page](https://github.com/dcloud-automation/content-automation/blob/master/README.md)

# bootstrap.sh

This script is hosted on a VM, normally Centos7-tools, inside of the session. It should be located at /root/bootstrap.sh and will be executed by the dCloud controller at session start. The execution of this script can be setup in Topology Builder (See example 7 below)


This script does the following:
1. Mounts a remote NFS folder  
2. Copies a script from the remote location to a local folder (auto.sh)
3. Unmount the remote NFS folder
4. <b>Executes the downloaded script (auto.sh)  while passing all the arguments recieved by this script (even those not handled by this script)</b>

<h2> Controller Arguments : </h2>

<h4> --controller-ip </h4>
This is the IP address of the controller. The API requests will be sent to this IP

Default: 198.19.254.51

<h4> --repo-ip </h4>
This is the IP address of the repository (where the code is stored).

Default: 198.19.254.160



<h2> Development/Troubleshooting Arguments: </h2> (Only use when not using the master controller)

<h4> --branch-name </h4>
This is the name of the github branch. Only specifcy when not using the master controller

Default: master

<h4> --api-port </h4>
This is the API port of the controller. Only specify when not using the master controller.

Default: 31080

<h4> --db-api-port </h4>
This is the DATABASE API port of the controller. Only specify when not using the master controller.

Default: 31886



<h2> Bootstrap Arguments: </h2>

<h4> --auto-path </h4> 
Path on remote NFS folder where the script to be downloaded is located. If you have created an specific auto.sh for this content, you MUST provide the path.

Default: indemo/common/auto.sh

<h4> --auto-destination</h4> 
Path (local on the centos VM)  where the script will be stored

Default: /root/auto.sh

<h4> --auto-exec</h4> 
Future use, in case the script downloaded is not bash and required specific prefix to be executed

Default:none

<h4> --pass-args</h4> 
Should the arguments be passed to the downloaded script? 

Default: yes

<h4> --mount-path</h4> 
Path (local on the centos VM) where the NFS folder will be mounted

Default: /root/temp/

<h4> --code-root</h4> 
Root path of the code stored on the repo server. Together with the --branch-name option, it will be the folder to mount from the repo server.  

Default: /var/nfs/code/


<h4> --log-file</h4> 
Bootstrap log file

Default: /root/bootstrap.log


<h2> Usage Example 1: In-demo Automation on production controller (master) </h2>

Automation Type:      indemo  (automation execution happens inside of the session)

Controller Instance:  master

Recipe Name:          myRecipeName

Recipe Path:          sec/

Auto.sh Path:         indemo/sec/myRecipeName/auto.sh

Bootstrap execution command: 
<h4>/root/bootstrap.sh --indemo-recipe myRecipeName  --indemo-recipe-path sec/ --auto-path indemo/sec/myRecipeName/auto.sh </h4>


<h2> Usage Example 2: In-demo Automation on development controller instance  </h2>

Automation Type:      indemo  (automation execution happens inside of the session)

Controller Instance:  myBranch-31085

Recipe Name:          myRecipeName

Recipe Path:          sec/

Auto.sh Path:         indemo/sec/myRecipeName/auto.sh

Bootstrap execution command
<h4>/root/bootstrap.sh --indemo-recipe myRecipeName  --indemo-recipe-path sec/ --auto-path indemo/sec/myRecipeName/auto.sh  --branch-name myBranch-31085 --api-port 31085 --db-api-port 31285 </h4>


<h2> Usage Example 3: Demo Automation on production controller (master) </h2>

Automation Type:      Demo  (automation execution happens outside of the session)

Controller Instance:  master

Recipe Name:          myRecipeName

Recipe Path:          sec/

Auto.sh Path:         indemo/sec/myRecipeName/auto.sh

Bootstrap execution command: 
<h4>/root/bootstrap.sh --demo-recipe myRecipeName  --demo-recipe-path sec/ --auto-path indemo/sec/myRecipeName/auto.sh </h4>


<h2> Usage Example 4: Demo Automation on development controller instance </h2>

Automation Type:      Demo  (automation execution happens outside of the session)

Controller Instance:  myBranch-31085

Recipe Name:          myRecipeName

Recipe Path:          sec/

Auto.sh Path:         indemo/sec/myRecipeName/auto.sh

Bootstrap execution command: 
<h4>./bootstrap.sh --demo-recipe myRecipeName  --demo-recipe-path sec/ --auto-path indemo/sec/myRecipeName/auto.sh  --branch-name myBranch-31085 --api-port 31085 --db-api-port 31285 </h4>


<h2> Usage Example 5: Demo and In-demo Automation on production controller (master) </h2>

Automation Type:      Demo and indemo  (automation execution happens inside and outside of the session)

Controller Instance:  master

Demo Recipe Name:          myDemoRecipeName

Demo Recipe Path:          sec/

In-demo Recipe Name:       myIndemoRecipeName

In-demo Recipe Path:       sec/

Auto.sh Path:         indemo/sec/myRecipeName/auto.sh

Bootstrap execution command: 
<h4>/root/bootstrap.sh --demo-recipe myDemoRecipeName  --demo-recipe-path sec/ --indemo-recipe myIndemoRecipeName  --indemo-recipe-path sec/ --auto-path indemo/sec/myRecipeName/auto.sh </h4>


<h2> Usage Example 6: Demo and In-demo Automation on development controller instance </h2>

Automation Type:      Demo and indemo  (automation execution happens inside and outside of the session)

Controller Instance:  myBranch-31085

Demo Recipe Name:          myDemoRecipeName

Demo Recipe Path:          sec/

In-demo Recipe Name:       myIndemoRecipeName

In-demo Recipe Path:       sec/

Auto.sh Path:         indemo/sec/myRecipeName/auto.sh

Bootstrap execution command: 
<h4>/root/bootstrap.sh --demo-recipe myDemoRecipeName  --demo-recipe-path sec/ --indemo-recipe myIndemoRecipeName  --indemo-recipe-path sec/ --auto-path indemo/sec/myRecipeName/auto.sh  --branch-name myBranch-31085 --api-port 31085 --db-api-port 31285 </h4>



<h2> Usage Example 7: In-demo  Automation on production controller (master) </h2>

Automation Type:      In-demo  (automation execution happens inside the session)

Controller Instance:  master

In-demo Recipe Name:  esa-indemo-v1

In-demo Recipe Path:  sec/

Auto.sh Path:         indemo/common/auto.sh (default value)

<img src="https://github.com/dcloud-automation/content-automation/blob/master/indemo/common/guest-automation-topology-builder.png">

[Back to Main Page](https://github.com/dcloud-automation/content-automation/blob/master/README.md)
