##############################################################################################
#  Parsing Arguments
##############################################################################################

while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -i|--indemo-recipe)
    inDemoRecipeName="$2"
    shift # past argument
    ;;
    -ip|--indemo-recipe-path)
    inDemoRecipePath="$2"
    shift # past argument
    ;;
    -d|--demo-recipe)
    demoRecipeName="$2"
    shift # past argument
    ;;
    -id|--demo-recipe-path)
    demoRecipePath="$2"
    shift # past argument
    ;;
    -ci|--controller-ip)
    CONTROLLERIP="$2"
    shift # past argument
    ;;
    -pu|--ps-user)
    PSUSER="$2"
    shift # past argument
    ;;
    -ph|--ps-host)
    PSHOST="$2"
    shift # past argument
    ;;
    -r|--repo-ip)
    REPOIP="$2"
    shift # past argument
    ;;
    -b|--branch-name)
    BRANCHNAME="$2"
    shift # past argument
    ;;
    -api|--api-port)
    APIPORT="$2"
    shift # past argument
    ;;
    -db|--db-api-port)
    DBAPIPORT="$2"
    shift # past argument
    ;;
    --default)
    helpMe="YES"
    ;;
    *)
           # unknown option
    ;;
esac
shift # past argument or value
done


##############################################################################################
#  General Parameters
##############################################################################################


if [ -z "$CONTROLLERIP" ]; then
    CONTROLLERIP="198.19.254.51"
fi 

if [ -z "$REPOIP" ]; then
    REPOIP="198.19.254.160"
fi 

if [ -z "$APIPORT" ]; then
    APIPORT="31080"
fi

if [ -z "$DBAPIPORT" ]; then
    DBAPIPORT="influxIndemo"
fi

if [ -z "$BRANCHNAME" ]; then
    BRANCHNAME="master"
fi


if [ -z "$PSHOST" ]; then
    PSHOST="198.18.135.67"
fi

if [ -z "$PSUSER" ]; then
    PSUSER="licensing"
fi


##############################################################################################
#  Print Usage Example....
##############################################################################################


if [ -z "$inDemoRecipeName" ] &&  [ -z "$demoRecipeName" ] ; then
    echo
    echo "Automation Bootstrap. Examples of use:"
    echo
    echo " Request both, indemo and demo automation: "
    echo " ./auto.sh --indemo-recipe contiv-v1-indemo  --indemo-recipe-path dcv/ --demo-recipe contiv-v1 --demo-recipe-path dcv/ "
    echo " ./auto.sh  -i contiv-v1-indemo -ip dcv/ -d contiv-v1  -dp dcv/ "
    echo
    echo " Request indemo automation only: "
    echo " ./auto.sh  --demo-name 'ACI Contiv v1' --indemo-recipe contiv-v1-indemo  --indemo-recipe-path dcv/ "
    echo " ./auto.sh  -i contiv-v1-indemo -ip dcv/  "
    echo
    echo " Request demo automation only: "
    echo " ./auto.sh  --demo-recipe contiv-v1 --demo-recipe-path dcv/ "
    echo " ./auto.sh  -d contiv-v1  -dp dcv/   "
    echo
    exit
fi




##############################################################################################
#  (If specified by arguments) Proceed to send request for demo automation in shared services
##############################################################################################

if [ -n "$demoRecipeName" ]; then
    echo
    echo
    echo "Working on Demo Automation Request"
    echo "Controller IP is $CONTROLLERIP"
    echo "Repo IP is $REPOIP"
    echo "API port is $APIPORT"
    echo "Branchname is $BRANCHNAME"
    echo
    echo


    #Prepare XML for Demo Automation Request
    #Copy Original file with session details
    cp /dcloud/session.xml /dcloud/session-sharedservices.xml

    #Add Type tag to the session file (if not present already)
    grep '<type>' /dcloud/session-sharedservices.xml 2>&1 1>/dev/null || sed -i '/<\/session>/i <type>queued<\/type>' /dcloud/session-sharedservices.xml

    #Add Recipe Name tag to the session file (if not present already)
    grep '<recipeName>'  /dcloud/session-sharedservices.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <recipeName>$demoRecipeName<\/recipeName>" /dcloud/session-sharedservices.xml

    #Add recipePath tag to the session file (if argument is present)
    if [ -n "$demoRecipePath" ]; then
        echo "Adding recipe Path for demo request"
        grep '<recipePath>' /dcloud/session-sharedservices.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <recipePath>$demoRecipePath<\/recipePath>" /dcloud/session-sharedservices.xml
    fi

    #Get XML on a single line
    sharedServicesXML=$(cat /dcloud/session-sharedservices.xml  | tr -d '\n')
    sharedServicesXML=$(echo "$sharedServicesXML" | tr -d '\r')
    #sharedServicesXML=$(echo "$sharedServicesXML" | tr -d '\r' | tr -d ' ')

    #Send  Automation Request to Central Automation Controller
    #This places the demo automation request that will be executed in shared services
    #command='curl -i -H "Content-Type: text/xml" -X POST -d '$"'"$sharedServicesXML$"'"'  http://198.19.254.51:31080/api/v1.0/task'
    command='curl -i -H "Content-Type: text/xml" -X POST -d '$"'"$sharedServicesXML$"'"'  http://$CONTROLLERIP:$APIPORT/api/v1.0/task'
    echo $command
    eval $command
    #curl -i -H "Content-Type: text/xml" -X POST -d '$sharedServicesXML'  http://198.19.254.51:31080/api/v1.0/task

fi

##############################################################################################
#  (If specified by arguments) Proceed to send remote request for in-demo automation
##############################################################################################

if [ -n "$inDemoRecipeName" ]; then

    echo
    echo
    echo "Preparing environment in order to execute in-demo automation"
    echo "Controller IP is $CONTROLLERIP"
    echo "Repo IP is $REPOIP"
    echo "API port is $APIPORT"
    echo "Branchname is $BRANCHNAME"
    echo "InfluxDB port to be used is: $DBAPIPORT"
    echo
    echo

    #Install Docker
    echo
    echo "If not installed, install docker now..."
    echo
    yum -y install docker
    systemctl enable docker
    systemctl start docker

    #Mount Code
    echo
    echo "Mounting code folder  $REPOIP:/var/nfs/code/$BRANCHNAME "
    echo
    mkdir /var/nfs 2>/dev/null
    mkdir /var/nfs/code 2>/dev/null
    mkdir /var/nfs/code/"$BRANCHNAME" 2>/dev/null
    mount "$REPOIP":/var/nfs/code/"$BRANCHNAME" /var/nfs/code/"$BRANCHNAME"

    #Mount Logs
    echo
    echo "Mounting code folder  $REPOIP:/var/nfs/logs/$BRANCHNAME "
    echo
    mkdir /var/nfs/logs 2>/dev/null
    mkdir /var/nfs/logs/"$BRANCHNAME" 2>/dev/null
    mount "$REPORIP":/var/nfs/logs/"$BRANCHNAME" /var/nfs/logs/"$BRANCHNAME"

    #Mount Repo
    echo
    echo "Mounting code folder  $REPOIP:/var/nfs/repo "
    echo
    mkdir /var/nfs 2>/dev/null
    mkdir /var/nfs/repo 2>/dev/null
    mount "$REPOIP":/var/nfs/repo /var/nfs/repo


    if [ -n "$PSHOST" ]; then
        echo 
        echo "PS Support required"
        echo
        ssh "$PSUSER"@"$PSHOST"  "net use p: /delete ; net use p: \\\\$REPOIP\\code\\$BRANCHNAME /P:yes "
        ssh "$PSUSER"@"$PSHOST"  "net use q: /delete ; net use q: \\\\$REPOIP\\logs\\$BRANCHNAME /P:yes "
    fi

    echo
    echo "Preparing inDemo Automation Request"
    echo
    #Prepare XML for Demo Automation Request
    #Duplicate  file with session details
    cp /dcloud/session.xml /dcloud/session-indemo.xml

    #Add Recipe Name tag to the session file (if not present already)
    grep '<recipeName>' /dcloud/session-indemo.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <recipeName>$inDemoRecipeName<\/recipeName>" /dcloud/session-indemo.xml

    #Add Type tag to the session file (if not present already)
    grep '<type>' /dcloud/session-indemo.xml 2>&1 1>/dev/null || sed -i '/<\/session>/i <type>remote<\/type>' /dcloud/session-indemo.xml

    #Add recipePath tag to the session file (if argument is present)
    if [ -n "$inDemoRecipePath" ]; then
        echo "Adding recipe Path for demo request"
        grep '<recipePath>' /dcloud/session-indemo.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i <recipePath>$inDemoRecipePath<\/recipePath>" /dcloud/session-indemo.xml
    fi


    #Get Session ID from session file  (and remove the change of line characters)
    sessionId=$(sed  -n  -e 's/.*<id>\(.*\)<\/id>/\1/p' /dcloud/session-indemo.xml)
    sessionId=$(echo "$sessionId" | tr -d '\r')
    #Create hostname for the container. To be used as "Host Name on the controller"
    containerName=$sessionId$'-remote'

    #Modify the Session ID. (Append a 'r' to it)
    newRequestId=$sessionId$'r'
    sed -i "s/$sessionId/$newRequestId/" /dcloud/session-indemo.xml

    #Add Host tag to the session file (if not present already)
    hostLine=$'<host>'$containerName$'<\/host>'
    grep host /dcloud/session-indemo.xml 2>&1 1>/dev/null || sed -i "/<\/session>/i $hostLine" /dcloud/session-indemo.xml

    #Get XML on a single line
    inDemoXML=$(cat /dcloud/session-indemo.xml  | tr -d '\n')
    inDemoXML=$(echo "$inDemoXML" | tr -d '\r')
 
    #Send Remote Request to Central Automation Controller
    #This places the in-demo automation request that will be executed in the demo environement by the local Startup Container
    #command='curl -i -H "Content-Type: text/xml" -X POST -d '$"'"$inDemoXML$"'"'  http://198.19.254.51:31080/api/v1.0/task/r0'
    command='curl -i -H "Content-Type: text/xml" -X POST -d '$"'"$inDemoXML$"'"'  http://$CONTROLLERIP:$APIPORT/api/v1.0/task/r0'
    echo 
    echo "Request will be send as: "
    echo $command
    eval $command
    echo
    echo

    #Start StartUp  Container
    /bin/docker rm -f checkQueuedRequests  2>&1 1>/dev/null 
    #cmd="/bin/docker run  -d --name checkQueuedRequests --hostname $containerName -it -v /var/nfs/repo:/root/repo -v /var/nfs/code/"$BRANCHNAME":/root/scripts  -v /var/nfs/logs/"$BRANCHNAME":/var/log  mgarcias/dcv-automation-base   /usr/bin/python /root/scripts/checkQueuedRequests.py -d "$DBAPIPORT" "

    /bin/docker run  -d --name checkQueuedRequests --hostname $containerName -it -v /var/nfs/repo:/root/repo -v /var/nfs/code/"$BRANCHNAME":/root/scripts  -v /var/nfs/logs/"$BRANCHNAME":/var/log  mgarcias/dcv-automation-base   /usr/bin/python /root/scripts/checkQueuedRequests.py -d "$DBAPIPORT"



fi



##############################################################################################
#  Anything else specific to your content?  Do it here....
##############################################################################################



