[Back to Main Page](https://github.com/dcloud-automation/content-automation/blob/master/README.md)




## Step by Step Configuration Process **CloudLock V1**

Here we document the steps followed in order to configure the CloudLock demo in dCloud

### <a name="step1"></a> Step 1. Define the main values to be used. We assume we start working with a DEV instance of the controller. (Not production)

The values are specified by the following arguments:

    --indemo-recipe:      cloudlock-indemo-v1
    --indemo-recipe-path: sec/
    --branch-name:        rtp-cloudlock-31098
    --api-port:           31098
    --db-api-port:        31298
    
    
### <a name="step2"></a>Step 2. Create a Dummy Recipe File for indemo automation 

The name in the "Details" section of the file must match --indemo-recipe. In this case the name is **cloudlock-indemo-v1**
The the file should be created on the folder specified on --indemo-recipe-path. In this case file is created in **recipes/sec/**

    Details:
      name: cloudlock-indemo-v1
      demo: Cloudlock 
      
    Start:
        - name: WaitForidp
          description:
          type: python
          import: genFunctions
          target: None
          function: pingHost
          runIf:
          input:
            host: 198.18.134.139
            waitForEcho: "Yes"
            waitMaxSeconds: 10
    End:

    Stop:


THe recipe file above has a single task that only pings the IDP appliance in the session. This is just for testing purposes...


### <a name="step3"></a>Step 3. Update the Bootstrap script 

The latest version of the bootstrap.sh is located in Github [here](https://github.com/dcloud-automation/content-automation/tree/master/indemo/common)
In case an update is needed, download and replace the existing one. 

Spin up a session of your content, SSH to the Centos7-tools1 VM,delete the existing bootstrap.sh file and download the latest version of the script. The following code should do it:

    mkdir /root/tmp
    rm -f /root/bootstrap.sh
    git clone -b master https://github.com/dcloud-automation/content-automation.git /root/tmp
    cp /root/tmp/indemo/common/bootstrap.sh /root/
    chmod 777 /root/bootstrap.sh
    rm -fr /root/tmp

Once the bootstrap.sh file has been downloaded,change the permission of bootstrap.sh file to 777 and proceed to save the session.

**Note1:** For more information about the bootstrap.sh script, please click [here](https://github.com/dcloud-automation/content-automation/blob/master/indemo/common/bootstrap.md)

**Note2:** Make sure the Centos7-tools1 VM is part of the content you are working on. It can be found as a template in topology builder under the name Centos7-tools1

**Note3:** Pre-configure the Centos7-tools VM to mount the NFS shares at boot, so this does not have to be done by auto.sh.  The code to pre-configure the Centos7 VM is below. This will create the required folders and add the mount entries to /etc/fstab. The mounts on this file are mounted at boot time. In order to trigger fstab without rebooting, execute  `mount -a` after executing the code below. 


    REPOIP="198.19.254.160"
    mkdir -p /var/nfs/code
    mkdir -p /var/nfs/logs
    mkdir -p /var/nfs/repo
    echo "$REPOIP:/var/nfs/code /var/nfs/code  nfs  defaults    0   0" >> /etc/fstab
    echo "$REPOIP:/var/nfs/logs /var/nfs/logs  nfs  defaults  0   0" >> /etc/fstab
    echo "$REPOIP:/var/nfs/repo /var/nfs/repo  nfs    defaults  0   0" >> /etc/fstab



### <a name="step4"></a>Step 4. Test API request and Container deployment 

At this point, with the latest bootstrap version on the Centos7 VM, let's test the API request creation by executing the following command from the Centos7 Vm:

    /root/bootstrap.sh --indemo-recipe cloudlock-indemo-v1 --indemo-recipe-path sec/ --branch-name rtp-cloudlock-31098 --api-port 31098 --db-api-port 31298

In order to verify that the API request has been sent, we should check the request on the controller's dashboard  `http://dcv-automation-api:31098/api/v1.0/dashboard` . The new request should be identified on the dashboard by the session ID and the owner, values taken from the session.xml file on the Centos7 Vm.

To verify the deployment of the container on the CentOS7 VM, we need to execute the command `docker ps`.  The output should show the following (Note the container ID will be different in each case) :

    [root@centos7-tools1 ~]# docker ps
    CONTAINER ID IMAGE                          COMMAND                  CREATED          STATUS        PORTS       NAMES
    9510df7af6b6 mgarcias/dcv-automation-base   "/usr/bin/python /roo"   5 seconds ago    Up 4 seconds              checkQueuedRequests

 
### <a name="step5"></a>Step 5. Create a functional Recipe File for indemo automation (automation inside the vpod)

Now we create a Recipe that will actually server the purpose of the automation we need for this demo. The indemo automation is all about what needs to be done inside of the session environment, also called vPod. 


The objective is to edit a configuration file on the IDP server and restart the Apache service. This IDP server is part of the vPod. 


The following recipe has two tasks on the Start section. The first one will check connectivity with the IDP appliance, if this is successfull, the second task will be executed (the conditional execution is indicated by the `runIf` label, in this case task 2  will execute only if tasks 1  was executed successfully)

This recipe is contained in file `cloudlock-indemo-v1.cfg` in folder `recipes/sec/` (Note: when sending the API request to the controller, the recipePath value should be `sec/` only. 

    Details:
          name: cloudlock-indemo-v1
          demo: Cloudlock 
    Start:
        - name: WaitForIdp
          description:
          type: python
          import: genFunctions
          target: None
          function: pingHost
          runIf:
          input:
            host: 198.18.134.139
            waitForEcho: "Yes"
            waitMaxSeconds: 10

        - name: ConfigureIdpApache
          description:
          type: pexpect
          import: pexpectCommands
          target: None
          function: executeFromRecipeWithSessionDetails
          runIf:
              req1: WaitForIdp
          input:
            recipeName: configureIdpApache.yaml
            recipePath: sec/
    End:

    Stop:



### <a name="step6"></a>Step 6. Using pexpect module  

The second task on the recipe above involves the use of the `pexpect` module. This module allows us to execute commands on remote servers via SSH. The module has a function called `executeFromRecipeWithSessionDetails` that requires a recipe file itself. The recipe file we use on this demo is called `configureIdpApache.yaml` and is located in folder `otherFiles/pexpect/sec/` (Note the recipePath on the recipe's task above is only `sec/` )  The pexpect recipe file for this demo is:

    Details:
          name: cofigureIdpApache

    Hosts:

        - name: IDP-Appliance
          host: 198.18.134.139
          username: root
          password: C1sco12345
          loginPrompt: 
            - .*@ubuntu:~#

    Commands:
        - order: 0
          description: Edit authsoruces file
          command: sed -i -e "s/userlab@cloudlocktraining.*.com/#sessionDetails['devices']['device']['name']#/g" /var/simplesamlphp/config/authsources.php
          limit: 10
          timeout: 5
          outputs:
            - pattern: .*@ubuntu:~#
              next: 1

        - order: 1
          description: Restart Apache Service
          command: service apache2 restart
          limit: 10
          timeout: 30
          outputs:
            - pattern: .*@ubuntu:~#
              next: end


The file has three sections, `Details`, `Hosts` and `Commands`. The `Hosts` section includes the connection and credentials details of the host or hosts where the commans will be executed. In this case, the commands will be executed on host `198.18.134.139` with credentials `root/C1sco12345`. 

The `Commands` sections include the commands that will be executed and the order in which they should be executed. In this case, there are two commands, a sed command that looks like:

    sed -i -e "s/userlab@cloudlocktraining.*.com/#sessionDetails['devices']['device']['name']#/g" /var/simplesamlphp/config/authsources.php


Note that the values between the `#` is `sessionDetails['devices']['device']['name']` and will be replaced by the value contained in the XML payload when the request is sent to the controller. This is a feature of the controller that allows us to access information contained in the session file.  If the session.xlm file is as shown below, the value between `#` on the example above will be replaced by `dcloud@cloudlocktraining33.com`


    <?xml version="1.0" encoding="UTF-8"?>
    <session>
      <datacenter>RTP</datacenter>
      <id>451008</id>
      <owner>swadvani</owner>
      <vpod>1041</vpod>
      <anycpwd>ff5851</anycpwd>
      <devices>
        <device>
          <id>1195</id>
          <name>dcloud@cloudlocktraining33.com</name>
        </device>
      </devices>
      <translations>
      </translations>
      <dids></dids>
    </session>


The second command will restart the apache service and then the task will finish. 

    service apache2 restart




### <a name="step7"></a>Step 7. Verify indemo Automation

Execute the bootstrap command again from the Centos7 VM. This time the new indemo recipe will be used and the IDP appliance should be configured as expected. 

    /root/bootstrap.sh --indemo-recipe cloudlock-indemo-v1 --indemo-recipe-path sec/ --branch-name rtp-cloudlock-31098 --api-port 31098 --db-api-port 31298




### <a name="step8"></a>Step 8. Create a Recipe File for demo automation (automation outside the vpod)

Now we create a recipe for the automation required to happen outside of the vPod, in this case shared services. The starting recipe is contained in file `cloudlock-demo-v1.cfg` in folder `recipes/sec/` (Note: when sending the API request to the controller, the recipePath value should be `sec/` only. 


    Details:
          name: cloudlock-demo-v1
          demo: Cloudlock 
    Start:

    End:
        - name: IsSessionReadyForCleanup
          type: python
          target: local
          import: dcloudController
          function: readyToDelete
          runIf:
          input:
            id: "$sessionDetails['id']$"
            dc: "$sessionDetails['datacenter']$"

        - name: CloudLockCleanup
          type: python
          target: local
          import: cloudlock
          function: cloudLockCleanup
          runIf:
              req1: IsSessionReadyForCleanup
              eval1: "outputs['IsSessionReadyForCleanup']['readyToDelete']"
          input:
            id: "$sessionDetails['id']$"
            dc: "$sessionDetails['datacenter']$"

    Stop:


The recipe above only contains tasks in its End section. That is because it is a cleanup script and it only needs to be executed once the session is over. 

The task `IsSessionReadyForCleanup` checks if the session is active or not. In case the session is active, it return False, otherwise True.

Task `CloudLockCleanup` contains two values in the `runIf` section. `req1: IsSessionReadyForCleanup` makes this task executes only if task `IsSessionReadyForCleanup` executed successfully.  `eval1: "outputs['IsSessionReadyForCleanup']['readyToDelete']"` will make sure this task executes only if the output `readyToDelete` of task `IsSessionReadyForCleanup` is True. 

Task `CloudLockCleanup` will execute function `cloudLockCleanup` from module `cloudlock`. This module is located in folder `modules/sec/`. The function will be passed two inputs: `id: "$sessionDetails['id']$"` and `dc: "$sessionDetails['datacenter']$"`. 

Note that the values between `$` will be replaced by the values contained in the session.xml file, `451008` and `RTP` respectively if we use the session.xml file above.  

Note that the inputs at this point are there only for reference. We still need to define what inputs we need for our cleanup function. 

The starting module file looks like this:


    #Imports
    import sys
    modulesPath=r'/root/scripts/core/'
    sys.path.append(modulesPath)
    import genFunctions as gen
    import logger as logger
    import logging

    #Front-End  Functions
    def cleanupCloudlock(sessionId, target, otherInputs=[], logFormat=''):
        if (type(otherInputs) is dict):
            try:
                id=otherInputs["id"]
                dc=otherInputs["dc"]
            except Exception as e:
                logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
                logger.autolog(level=1,message=str(e),format=logFormat)
                return {},'Failed:Error while reading input arguments'
        else:
            logger.autolog(level=1,message='No arguments provided',format=logFormat)  
            return {},'Failed: No arguments provided'

        #creds = gen.getDemoCredentialsByName(target['file'],target['device'])
        #if not creds:
        #    return {},'Failed: Unable to get credentials for target'
        
        result,eMsg=removeSomething(id,dc,target,logFormat)

        if result:
            return {},'Success'
        else:
            return {},'Failed: ' + str(eMsg) 

    #Generic  Functions
    def removeSomething(id,dc,target,logFormat):
        return True,'OK'


In the modules there are two kind of functions, front-end and generic functions. The front-end functions need to comply with certain requirements:

- Inputs: The inputs must always be `(sessionId, target, otherInputs=[], logFormat='')`

- Outputs: The output must always be `return {},'Success'` for good results or `return {},'Failed:<<<Here an error message>>>'`

Generic functions have no specific requirements but it is recomended to use the logFormat as one of the arguments

In our module, we have the front-end function called `cleanupCloudlock` which in turn will call generic function `removeSomething`. **It is in this generic function that we need to implement the function that does the cleanup.** We can create more than one function if required.




### <a name="step4"></a>Step 9. Test API request for demo automation (outside the vPod)

At this point, with the latest bootstrap version on the Centos7 VM, let's test the API request creation for demo automation by executing the following command from the Centos7 Vm:

    /root/bootstrap.sh --demo-recipe cloudlock-demo-v1 --demo-recipe-path sec/ --indemo-recipe cloudlock-indemo-v1 --indemo-recipe-path sec/ --branch-name rtp-cloudlock-31098 --api-port 31098 --db-api-port 31298
    
Note that we still keep the indemo automation parameters, we are just adding the demo automation ones `--demo-recipe cloudlock-demo-v1 --demo-recipe-path sec/ `.

A few seconds after executing the command, we should see two new entries on the dashboard  `http://dcv-automation-api:31098/api/v1.0/dashboard` 



[Back to Main Page](https://github.com/dcloud-automation/content-automation/blob/master/README.md)










