import redis
import sys

#modulesPath=r'/root/scripts/core/'
#sys.path.append(modulesPath)

import logger as logger
import genFunctions as gen
import datetime
import timestring
import socket
import json
import pika


###################################
#  global variables
###################################


####################################
# GENERIC
####################################


def rabbitSendToWorkersQueue(rabbitServer,body,auto_delete=False,durable=True,arguments={'x-max-priority':5},priority=0,heartbeat=1800):
    try:
        queueName=body['cloud'].lower()
    except Exception as e:
        queueName='dcloud'    
    try:
        queueName=queueName+'.'+body['datacenter'].lower()
    except Exception as e:
        logger.autolog(level=1, message="Error while rabbit send. Cannot find DATACENTER. Aborting message...", format="|RABBITSEND|")
        return False
    try:
        queueName=queueName+'.'+body['pod'].lower()
    except Exception as e:
        queueName=queueName+'.sharedservices'    

    #If worker's location is in a vpod, make the queue auto_delete
    try:
        queueName=queueName+'.vpod.'+body['vpod-automation']
        auto_delete=True
    except Exception as e:
        pass

    return rabbitSend(rabbitServer, 'WORKERS.'+queueName,body,auto_delete=auto_delete,durable=durable,arguments=arguments,priority=priority,heartbeat=heartbeat)


def rabbitListenToWorkersQueue(rabbitServer,location,callback,auto_delete=False,durable=True,arguments={'x-max-priority':5},heartbeat=1800):

    #If vpod is part of the location string, it comes from a vpod so we need to enable auto_delete
    if 'vpod' in str(location):
        auto_delete=True

    rabbitListenBlocking(rabbitServer,'WORKERS.'+location,callback,auto_delete=auto_delete,durable=durable,arguments=arguments,heartbeat=heartbeat)


def rabbitSend(rabbitServer, queueName,body,auto_delete=False,durable=True,arguments={'x-max-priority':5},priority=0,heartbeat=1800):
    try:
        body=json.dumps(body)
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitServer,heartbeat=heartbeat,blocked_connection_timeout=1800))
        channel = connection.channel()
        channel.queue_declare(queue=queueName,durable=durable,auto_delete=auto_delete,arguments=arguments)
        channel.basic_publish(exchange='', routing_key=queueName,body=body,properties=pika.BasicProperties(delivery_mode = 2, priority = priority))
        connection.close()
    except Exception as e:
        logger.autolog(level=1, message="Error while rabbit send...", format="|RABBITSEND|")
        logger.autolog(level=1, message=str(e), format="|RABBITSEND|")
        return False
    return True

def rabbitListenBlocking(rabbitServer,queueName,callback,auto_delete=False,durable=True,arguments={'x-max-priority':5},heartbeat=600):
    logger.autolog(level=1, message="Starting the Rabbit", format="|RABBITLISTEN|")
    try:
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitServer,heartbeat=heartbeat,blocked_connection_timeout=1800))
        channel = connection.channel()
        channel.queue_declare(queue=queueName, durable=durable,auto_delete=auto_delete,arguments=arguments)
        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(callback,queue=queueName)
        channel.start_consuming()
    except Exception as e:
        logger.autolog(level=1, message="Error while rabbit listen...", format="|RABBITLISTEN|")
        logger.autolog(level=1, message=str(e), format="|RABBITLISTEN|")