#!/usr/bin/env python2.7

#####################################
# Global Variables
#####################################

#logPath=r'/var/log/'
#logFile=r'checkQueuedRequest.log'
#logDebugFile=r'checkQueuedRequest_DEBUG.log'


#####################################
# Import Modules
#####################################
import sys
recipesPath=r'/root/scripts/recipes/'
sys.path.append(recipesPath)
modulesPath=r'/root/scripts/modules/'
sys.path.append(modulesPath)

modulesPath=r'/root/scripts/modules/sec'
sys.path.append(modulesPath)

modulesPath=r'/root/scripts/modules/dcv'
sys.path.append(modulesPath)

modulesPath=r'/root/scripts/modules/col'
sys.path.append(modulesPath)

import settings
import rabbitQueue as rabbit
import logging
import threading
import urllib2
import glob
import json
import os
import os.path
import requests
import time
import xml.dom.minidom
import yaml
import platform
import signal
from xml.dom import minidom
import datetime
import genFunctions as gen
import logger as logger



#####################################
# Worker's Generic Functions
#####################################

def checkDependencies(taskDependencies,taskName, executedTasks):
    logger.autolog(message='Checking dependencies for task ' + str(taskName))
    failedTasks=[]
    falseDependencies=[]
    if (type(taskDependencies) is dict):
        logger.autolog(message='Found dependencies list for task ' + str(taskName))
        for req in taskDependencies:
            dep=False
            logger.autolog(message='!!!!!!!!!! dependency is !!!!! ' + taskDependencies[req])
            if("req" in req):
                for executedTask in executedTasks:
                    #logger.autolog(message='taskDependencies[req] ' + str(taskDependencies[req]))
                    #logger.autolog(message='executedTask["taskName"] ' + str(executedTask['taskName']))
                    if (taskDependencies[req] == executedTask['taskName']):
                        dep=True
                        logger.autolog(message='Found dependencie for task ' + str(taskName) + " " + str(executedTask['taskName']))
                        logger.autolog(message='executedTask["taskStatus"] ' + str(executedTask['taskStatus']))
                        if("Failed" in executedTask['taskStatus'] or "unavailable" in executedTask['taskStatus'] ):
                            logger.autolog(message='Found task with failed status ' +executedTask['taskName'] + ',' + executedTask['taskStatus'] )
                            failedTasks.append(executedTask['taskName'])
                #logger.autolog(message=dep)
                if not dep :
                    logger.autolog(message='Couldnt find dependencie for task ' + str(taskName) + " " + str(executedTask['taskName']))
                    failedTasks.append(executedTask['taskName'])
            if("eval" in req):
                logger.autolog(message='Found eval dependency for task ' + str(taskName) + ' ' + taskDependencies[req])
                resultDep=gen.procDeps(taskDependencies[req],executedTasks) 
                if(resultDep=="ERROR"):
                    logger.autolog(message='Unable to evaluate ' + taskDependencies[req])
                    falseDependencies.append(taskDependencies[req]+' Error while evaluating expression')
                if(resultDep==True):
                    #logger.autolog(message='Eval dependency '+ req +' is true')
                    pass
                else:                   
                    logger.autolog(message='Eval dependency '+ req +' is false')
                    falseDependencies.append(taskDependencies[req])
    else:
        #logger.autolog(message='No dependencies for task ' + str(taskName))
        pass
    failedDepListString=''
    for failedTask in failedTasks:
        failedDepListString=failedDepListString+'[req]:'+failedTask+','
    for falseDependency in falseDependencies:
        failedDepListString=failedDepListString+'[eval]:'+falseDependency+','
    if ((failedTasks==[]) & (falseDependencies==[])):
        #logger.autolog(message='Dependencies OK for task ' + str(taskName))
        return True,[],[],failedDepListString
    else:
        #logger.autolog(message='At least one dependencie failed for task ' + str(taskName))
        return False,failedTasks,falseDependencies,failedDepListString
    return True    



#####################################
# Signal Functions
#####################################

def timeout(signum, frame):
    logger.autolog(level=1,message='Signal Alert for fuction')
    raise Exception("Function time out!")


#####################################
# Worker Functions
#####################################
def execFunc(task, sessionId, otherInputs=[],logFormat=''):

    try:
        logger.autolog(level=1,message='About to execute task  using module ' + str(task['import']), format=logFormat)

        #Get module name from task and import it
        mod = __import__(task['import'])
        reload(mod)
    except Exception as e:
        logger.autolog(message='Error while importing module  ' + task['import'], format=logFormat)
        logger.autolog(message='' + str(e), format=logFormat)
        return [],'Failed: ' + str(e)

    logger.autolog(level=1, message='task function is '  +str(task['function']), format=logFormat)
    logger.autolog(level=1, message='task target is '  +str(task['target']), format=logFormat)


    try:
        #Get function namefrom task and execute it
        func = getattr(mod,task['function'])
        return func(sessionId, task['target'], otherInputs=otherInputs, logFormat=logFormat)
    except Exception as e:
        logger.autolog(message='Could not execute task ', format=logFormat)
        logger.autolog(message='' + str(e), format=logFormat)
        return {},'Failed: ' + str(e)



def execFuncV2(task, sessionId, otherInputs=[],logFormat=''):

    #Import the module
    try:
        logger.autolog(level=1,message='About to execute task  using module ' + str(task['import']), format=logFormat)

        #Get module name from task and import it
        mod = __import__(task['import'])
        reload(mod)
    except Exception as e:
        logger.autolog(message='Error while importing module  ' + task['import'], format=logFormat)
        logger.autolog(message='' + str(e), format=logFormat)
        return [],'Failed: ' + str(e)

    logger.autolog(level=1, message='task function is '  +str(task['function']), format=logFormat)
    logger.autolog(level=1, message='task target is '  +str(task['target']), format=logFormat)

    #Register the Signal (alarm)
    signal.signal(signal.SIGALRM, timeout)

    #Set the alarm in 10 mins
    signal.alarm(600)



    #Execute the function
    try:
        #Get function namefrom task and execute it
        func = getattr(mod,task['function'])
        result=func(sessionId, task['target'], otherInputs=otherInputs, logFormat=logFormat)
        #Cancel the timer
        signal.alarm(0)
        return result
    except Exception as e:
        if e == "Function time out!":
            logger.autolog(message='Could not execute task, it TIMED OUT!! ', format=logFormat)
            return {},'Failed: ' + str(e)
        else:
            signal.alarm(0)
            logger.autolog(message='Could not execute task ', format=logFormat)
            logger.autolog(message='' + str(e), format=logFormat)
            return {},'Failed: ' + str(e)

def execTasks(sessionDetails, tasks, action='Start', redisDetails={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'},returnDetails=False):
    executedTasks=[]
    failureFree=True
    allTasksExecuted=True

    try:
        #Execute One Task at the time
        for task in tasks:
            logger.autolog(message='These is the task to execute:   ' + str(task))
            execFormat=sessionDetails["request"]['recipeName']+'|'+ sessionDetails["request"]['id'] +'|' + sessionDetails["request"]['owner'] + '|task:' + task['name']

            logger.autolog(level=1,message='Starting Task. Checking dependencies...' ,format=execFormat)

            #Get required dependencies for this task, if any.
            dependenciesOK,failedTasks,falseDependencies,failedDepListString=checkDependencies(task['runIf'],task['name'],executedTasks)
        
            logger.autolog(level=1,message='Dependencies result: ' + str(dependenciesOK) ,format=execFormat)

            if dependenciesOK:
                processedInputs=gen.procInputsV2(sessionDetails,executedTasks,task['input'])
                processedInputs['sessionDetails']=sessionDetails


                try:
                    timeStart=str(datetime.datetime.now())
                    #inMemoryStorage.cacheWriteRequest(gen.updateSessionDetails(sessionDetails,task,action,'Running','unavailable',timeStart=timeStart),redisDetails=redisDetails,logFormat=execFormat)
                    sessionDetails['registerAction']='redisOnly'
                    rabbit.rabbitSendToRegisterExchange(settings.workerRabbitServer,gen.updateSessionDetails(sessionDetails,task,action,'Running','unavailable',timeStart=timeStart),port=settings.workerRabbitPort)
                except Exception as e:
                    logger.autolog(message='Could not update cache (REDIS)', format=execFormat)
                    logger.autolog(message=str(e), format=execFormat)
                    pass 


                taskOutput , taskStatus  = execFunc(task,sessionDetails["request"]['id'], processedInputs,logFormat=execFormat)
                

                try:
                    timeEnd=str(datetime.datetime.now())
                    #inMemoryStorage.cacheUpdateRequestOutput(sessionDetails,action,task,taskOutput,redisDetails=redisDetails,logFormat=execFormat)
                    #inMemoryStorage.cacheUpdateRequestStatus(sessionDetails,action,task,taskStatus,redisDetails=redisDetails,logFormat=execFormat)
                    #inMemoryStorage.cacheWriteRequest(gen.updateSessionDetails(sessionDetails,task,action,taskStatus,taskOutput,timeStart=timeStart,timeEnd=timeEnd),redisDetails=redisDetails,logFormat=execFormat)
                    sessionDetails['registerAction']='redisOnly'
                    rabbit.rabbitSendToRegisterExchange(settings.workerRabbitServer,gen.updateSessionDetails(sessionDetails,task,action,taskStatus,taskOutput,timeStart=timeStart,timeEnd=timeEnd),port=settings.workerRabbitPort)

                except Exception as e:
                    logger.autolog(message='Could not update cache (REDIS)', format=execFormat)
                    logger.autolog(message=str(e), format=execFormat)
                    pass 

                executedTask={'datacenter': sessionDetails["request"]['datacenter'], 'owner': sessionDetails["request"]['owner'], 'demo':sessionDetails["request"]['recipeName'], 'id': sessionDetails["request"]['id'], 'taskName': task['name'], 'taskOutput': taskOutput , 'taskStatus': taskStatus ,'taskAction': task['taskAction'] }
                executedTasks.append(executedTask) 
                #logger.autolog(message=executedTask)
                if("Failed" not in taskStatus):
                    logger.autolog(message='OK',format=execFormat)
                else:
                    logger.autolog(message='Executed with errors: ' +taskStatus , format=execFormat)
                    failureFree=False
            else:
                #logger.autolog(message='At least one requirement is not met for ' + task['name'] + '. Not executing ')
                executedTask={'datacenter': sessionDetails["request"]['datacenter'], 'owner': sessionDetails["request"]['owner'], 'demo':sessionDetails["request"]['recipeName'], 'id': sessionDetails["request"]['id'], 'taskName': task['name'], 'taskOutput': {} , 'taskStatus': 'notExecuted due to missing dependencies: ' + failedDepListString ,'taskAction': task['taskAction']}
                executedTasks.append(executedTask)
                logger.autolog(message='Not Executed due to missing dependencies. ' + failedDepListString, format=execFormat)
                if(len(failedTasks)>0):   
                    failureFree=False
                allTasksExecuted=False



    except Exception as e:
        logger.autolog(message='Error found while executing tasks  --execTasks--- ')
        logger.autolog(message=str(e))
        if returnDetails:
            return False, False, [], sessionDetails
        else:
            return False, False, []


    if returnDetails:
        return failureFree,allTasksExecuted,executedTasks,sessionDetails
    else:
        return failureFree,allTasksExecuted,executedTasks





