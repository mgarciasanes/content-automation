import redis
import sys

#modulesPath=r'/root/scripts/core/'
#sys.path.append(modulesPath)

import logger as logger
import genFunctions as gen
import datetime
import timestring
import socket
import json


###################################
#  global variables
###################################


###################################
#  Front-end functions
###################################

###################################
#
###################################

#redisDetails={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'}


def cacheWriteRequest(sessionFullDetails,redisDetails={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'},requestStatus='queued',logFormat=''):

    cacheKey=str(sessionFullDetails["request"]['key']).lower()
    logger.autolog(level=1,message='Writing REDIS with key: ' + str(cacheKey) ,format=logFormat)

    if redisSetJson(cacheKey,sessionFullDetails,redisDetails=redisDetails,logFormat=logFormat):
        return sessionFullDetails
    return False


def cacheUpdateRequest(sessionDetails,cacheDict,redisDetails={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'},logFormat=''):

    cacheKey=str(sessionDetails['key']).lower()

    logger.autolog(level=1,message='Updating Request in REDIS with key ' + str(cacheKey) ,format=logFormat)
    #logger.autolog(level=1,message='Updating Request in REDIS ' + str(cacheDict) ,format=logFormat)
    return redisSetJson(cacheKey,cacheDict,redisDetails=redisDetails,logFormat=logFormat)



def cacheReadRequest(sessionDetails,output='json',redisDetails={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'},logFormat=''):

    cacheKey=str(sessionDetails['key']).lower()

    logger.autolog(level=1,message='Reading from REDIS with KEY: ' + str(cacheKey),format=logFormat)


    if output=='json':
        return redisGetJson(cacheKey,redisDetails=redisDetails,logFormat=logFormat)
    else:
        return redisGet(cacheKey,redisDetails=redisDetails,logFormat=logFormat)
  
def cacheReadRequestOutputs(sessionDetails,redisDetails={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'},logFormat=''):

    #logger.autolog(level=1,message='cacheReadRequestOutputs  with redisdetails :  ' + str(redisDetails),format=logFormat)
    request=cacheReadRequest(sessionDetails,redisDetails=redisDetails,logFormat=logFormat)
    #logger.autolog(level=1,message='Could read request from REDIS for Outputs build and ' + str(request['tasks']),format=logFormat)

    requestOutputs={}
    if request:
        for action in ['Start','Stop','End']:
            #logger.autolog(level=1,message='Got : ' + str(request['tasks'][action]))
            try:
                for recipeTask in request['tasks'][action]:
                    try:
                        requestOutputs[recipeTask['name']]=recipeTask['output']
                    except Exception as e:
                        requestOutputs[recipeTask['name']]={}
            except Exception as e:
                pass #No tasks in action

    else:
        logger.autolog(level=1,message='Unable to get request from redis. Unable to build output dict!! ',format=logFormat)
        return False
    #logger.autolog(level=1,message='Obtained Outputs from REDIS: ' + str(requestOutputs),format=logFormat)
    return requestOutputs


def cacheUpdateRequestMainStatus(sessionDetails,status,comments='',redisDetails={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'},logFormat=''):

    #logger.autolog(level=1,message='cacheUpdateRequestMainStatus with redisdetails :  ' + str(redisDetails),format=logFormat)
    request=cacheReadRequest(sessionDetails,redisDetails=redisDetails,logFormat=logFormat)
    #logger.autolog(level=1,message='Could read request from REDIS ' + str(request),format=logFormat)
    try:
        if request:
            request['status']=status
            request['comments']=comments
        else:
            logger.autolog(level=1,message='Unable to get request from redis. Wont update main status!! ',format=logFormat)
            return False
    except Exception as e:
        logger.autolog(level=0,message='ERROR in REDIS: Unable to update request main status in REDIS',format=logFormat)
        logger.autolog(level=0,message=e,format=logFormat)
        return False
    return cacheUpdateRequest(sessionDetails,request,redisDetails=redisDetails,logFormat=logFormat),request



def cacheUpdateRequestStatus(sessionDetails,action,task,taskStatus,redisDetails={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'},logFormat=''):
    #logger.autolog(level=1,message='cacheUpdateRequestStatus with redisdetails :  ' + str(redisDetails),format=logFormat)
    request=cacheReadRequest(sessionDetails,redisDetails=redisDetails,logFormat=logFormat)
    #logger.autolog(level=1,message='Could read request from REDIS ' + str(request),format=logFormat)
    try:
        if request:
            for recipeTask in request['tasks'][action]:
                #logger.autolog(level=1,message='Now checking task ' + str(recipeTask), format=logFormat)
                if recipeTask['name']==task['name']:
                    recipeTask['status']=taskStatus


        else:
            logger.autolog(level=1,message='Unable to get request from redis. Wont update task status!! ',format=logFormat)
            return False
    except Exception as e:
        logger.autolog(level=0,message='ERROR in REDIS: Unable to update request in REDIS',format=logFormat)
        logger.autolog(level=0,message=e,format=logFormat)
        return False
    return cacheUpdateRequest(sessionDetails,request,redisDetails=redisDetails,logFormat=logFormat)


def cacheUpdateRequestOutput(sessionDetails,action,task,taskOutput,redisDetails={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'},logFormat=''):
    request=cacheReadRequest(sessionDetails,redisDetails=redisDetails,logFormat=logFormat)
    #logger.autolog(level=1,message='Got request from REDIS with action ' + str(action) +' and ' + str(request['tasks']),format=logFormat)
    try:
        if request:
            for recipeTask in request['tasks'][action]:
                #logger.autolog(level=1,message='Now checking task ' + str(recipeTask), format=logFormat)
                if recipeTask['name']==task['name']:
                    recipeTask['output']=taskOutput
        else:
            logger.autolog(level=1,message='Unable to get request from redis. Wont update task output!! ',format=logFormat)
            return False
    except Exception as e:
        logger.autolog(level=0,message='ERROR in REDIS: Unable to update request in REDIS',format=logFormat)
        logger.autolog(level=0,message=e,format=logFormat)
        return False
    return cacheUpdateRequest(sessionDetails,request,redisDetails=redisDetails,logFormat=logFormat)




####################################
# GENERIC
####################################

def redisSet(redisKey, redisValue,redisDetails={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'}, logFormat=''):
    try:
        creds = gen.getDemoCredentialsByName(redisDetails['filePath'],redisDetails['name'])
        if not creds:
            logger.autolog(level=1,message='Unable to get creds for write function in redis',format=logFormat)
            return False
        #logger.autolog(level=1,message='About to execute  write  operation in redis ' + str(redisDetails) + '. Key: ' + str(redisKey) + ' Value: ' + str(redisValue),format=logFormat)

        #If port is specific in redisDetails, use it, otherwise use from creds file
        try:
            redisPort=creds['port']
            if redisDetails['port']:
                redisPort=redisDetails['port']
        except Exception as e:
            pass

        logger.autolog(level=0,message='About to write to REDIS: ' + str(creds['host']) + ' on port : ' + str(redisPort) ,format=logFormat)
        r = redis.StrictRedis(host=creds['host'], port=redisPort, db=0)

        return r.set(redisKey, redisValue)
    except Exception as e:
        logger.autolog(level=0,message='ERROR in REDIS: Unable to write in REDIS',format=logFormat)
        logger.autolog(level=0,message=e,format=logFormat)
        return False


def redisGet(redisKey,redisDetails={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'}, logFormat=''):
    try:
        logger.autolog(level=1,message='About to read from redis, key: ' + str(redisKey),format=logFormat)
        creds = gen.getDemoCredentialsByName(redisDetails['filePath'],redisDetails['name'])
        if not creds:
            logger.autolog(level=1,message='Unable to get creds for write function in redis',format=logFormat)
            return False
        #logger.autolog(level=1,message='About to execute  read  operation in redis ' + str(redisDetails) + '. Key: ' + str(redisKey),format=logFormat)



        #If port is specific in redisDetails, use it, otherwise use from creds file
        try:
            redisPort=creds['port']
            if redisDetails['port']:
                redisPort=redisDetails['port']
        except Exception as e:
            pass


        logger.autolog(level=0,message='About to read from REDIS: ' + str(creds['host']) + ' on port : ' + str(redisPort) ,format=logFormat)
        r = redis.StrictRedis(host=creds['host'], port=redisPort, db=0)
        if r.get(redisKey):
            value= r.get(redisKey)
            #logger.autolog(level=1,message='Read key ' + str(redisKey) + ' from REDIS : ' + str(value),format=logFormat)
            return value
        else:
            logger.autolog(level=0,message='Warning in REDIS: Unable to find key ' + str(redisKey) + ',returning empty list.',format=logFormat)
            return '[]'
    except Exception as e:
        logger.autolog(level=0,message='ERROR in REDIS: Unable to read in REDIS',format=logFormat)
        logger.autolog(level=0,message=e,format=logFormat)
        return False


def redisSetJson(redisKey, redisValue,redisDetails={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'}, logFormat=''):
    return redisSet(redisKey,json.dumps(redisValue),redisDetails=redisDetails,logFormat=logFormat)
    

def redisGetJson(redisKey,redisDetails={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'}, logFormat=''):
    return json.loads(redisGet(redisKey,redisDetails=redisDetails, logFormat=logFormat))


def redisSetDict(redisKey, redisValue,redisDetails={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'}, logFormat=''):
    creds = gen.getDemoCredentialsByName(redisDetails['filePath'],redisDetails['name'])
    if not creds:
        logger.autolog(level=1,message='Unable to get creds for write function in redis',format=logFormat)
        return False
    logger.autolog(level=1,message='About to execute  write  operation in redis ' + str(redisDetails) + '. Key: ' + str(redisKey) + ' Value: ' + str(redisValue),format=logFormat)
    try:
        r = redis.StrictRedis(host=creds['host'], port=creds['port'], db=0)
        return r.hmset(redisKey, redisValue)
    except Exception as e:
        logger.autolog(level=0,message='ERROR in REDIS: Unable to write in REDIS')
        logger.autolog(level=0,message=e)
        return False


def redisGetDict(redisKey,redisDetails={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'}, logFormat=''):
    creds = gen.getDemoCredentialsByName(redisDetails['filePath'],redisDetails['name'])
    if not creds:
        logger.autolog(level=1,message='Unable to get creds for write function in redis',format=logFormat)
        return False
    logger.autolog(level=1,message='About to execute  read  operation in redis ' + str(redisDetails) + '. Key: ' + str(redisKey),format=logFormat)
    try:
        r = redis.StrictRedis(host=creds['host'], port=creds['port'], db=0)
        return r.hgetall(redisKey)
    except Exception as e:
        logger.autolog(level=0,message='ERROR in REDIS: Unable to read in REDIS')
        logger.autolog(level=0,message=e)
        return False



