#####################################
# Import Modules
#####################################
import logging
import logging.handlers


########################
## Logging Configuration
########################

import socket
import os
import rabbitQueue as rabbit
import settings
import json

#hostname = socket.gethostname()
#os.system('mkdir /var/log/' + str(hostname) + ' 2> /dev/null ')
#logPath=r'/var/log/'+str(hostname)+'/'

logPath=r'/var/log/'
logFile=r'logger.log'
logDebugFile=r'logger_DEBUG.log'


#####################################
# Logger
#####################################
def setLogFiles(newLogFile='',newLogDebugFile=''):
    global logFile,logDebugFile
    if (newLogFile != ''):    
        logFile=newLogFile
    if (newLogDebugFile != ''):
        logDebugFile=newLogDebugFile
    return 

def setLogPath(newLogPath=''):
    global logPath
    if (newLogPath != ''):    
        logPath=newLogPath
    return 



def setLogger():

    global logFile,logDebugFile

 
    #logging.basicConfig(filename=logPath+logFile ,level=logging.INFO,format='%(asctime)s %(message)s')
    global fileLogger 
    fileLogger = logging.getLogger()
    fileLogger.setLevel(logging.DEBUG)

    global fileHandlerInfo
    fileHandlerInfo=logging.handlers.RotatingFileHandler(logPath+logFile, maxBytes= 50000000, backupCount=5)
    fileHandlerInfo.setLevel(logging.INFO)

    formatter=logging.Formatter('%(asctime)s %(message)s')
    fileHandlerInfo.setFormatter(formatter)

    global fileHandlerDebug 
    fileHandlerDebug=logging.handlers.RotatingFileHandler(logPath+logDebugFile, maxBytes= 50000000, backupCount=5)
    fileHandlerDebug.setLevel(logging.DEBUG)
    formatter=logging.Formatter('%(asctime)s %(message)s')
    fileHandlerDebug.setFormatter(formatter)

    fileLogger.addHandler(fileHandlerInfo)
    fileLogger.addHandler(fileHandlerDebug)



def parseLog(input):
    if isinstance(input,dict):
        output="{"
        for key in input:
            output=output+'"'+str(key)+'":'+parseLog(input[key])+','
        if len(output)>1:
            output=output[:-1]+'}'
        else:
            output=output+'}'
        return output
    if isinstance(input,str):
        return '"'+input.replace('"',"'")+'"'
    return output



def autolog(level=0,message='',format=''):
    global fileLogger,fileHandler,fileHandlerInfo,fileHandlerDebug
    import inspect

    func = inspect.currentframe().f_back.f_code
    try:
        logDict={}
        for key in settings.loggerDetails.keys():
            logDict[key]=settings.loggerDetails[key]
        logDict['scriptFileName']=str(func.co_filename)
        logDict['functionName']=str(func.co_name)
        logDict['logMessage']=str(message)
        rabbit.rabbitSendToLoggersExchange(settings.rabbitServer,logDict,port=settings.rabbitPort)
    except Exception as e:
        logging.debug("WARNING: Unable to send log message to rabbitmq")
        logging.debug(str(e))



    if format=='':
        formatter=logging.Formatter('%(asctime)s %(message)s')
        fileHandlerInfo.setFormatter(formatter)
        fileHandlerDebug.setFormatter(formatter)

    else:
        formatter=logging.Formatter('%(asctime)s ['+ format +']%(message)s')
        fileHandlerInfo.setFormatter(formatter)
        fileHandlerDebug.setFormatter(formatter)

    if level == 0:
        # Get the previous frame in the stack, otherwise it would be this function!!!
        #func = inspect.currentframe().f_back.f_code
        #logging.info("[%s][%s]: %s" % (func.co_filename, func.co_name, message))
        logging.info(message)

    else:
        # Get the previous frame in the stack, otherwise it would be this function!!!
        #func = inspect.currentframe().f_back.f_code
        logging.debug("[%s][%s]: %s" % (func.co_filename, func.co_name, message))

    #Clear Format 
    formatter=logging.Formatter('%(asctime)s %(message)s')
    fileHandlerInfo.setFormatter(formatter)
    fileHandlerDebug.setFormatter(formatter)


def getLogFileContent(filter='', logFile='api.log'):
    global logPath
    content='<br><h4>LogFile: ' + str(logFile) + ' <br>  Filter: ' + str(filter) + '</h4><br>'
    autolog(level=1, message='Reading Log file ' + str(logFile))

    for container in os.walk('/var/log/'):
        for fileNumber in ['','.1','.2','.3','.4','.5']:
            try:
                content = content + '<br> <h5> ' + str(container[0])+'/'+logFile+str(fileNumber) + ' </h5> <br>' 
                content1=''
                for line in open(str(container[0])+'/'+logFile+str(fileNumber)):
                    if filter !='':
                        if filter in line:
                            content1 = content1 + '<br>' + line
                    else:
                        content1= content1  + '<br>' + line
                content = content + ' <br> ' + content1[-100000:] 
            except Exception as e:
                autolog(level=1, message='Error while reading log file ' + str(container[0])+'/'+logFile+str(fileNumber) )
                autolog(level=1, message=str(e))

    return content





