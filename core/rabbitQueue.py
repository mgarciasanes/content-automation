#import redis
import sys

#modulesPath=r'/root/scripts/core/'
#sys.path.append(modulesPath)

import logger as logger
import genFunctions as gen
import datetime
import timestring
import socket
import json
import pika
import time
import random

import settings

###################################
#  global variables
###################################
rpcResponse=''


####################################
# FRON-END FUNCTIONS
####################################


def moveMessageFromBuffer(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            bufferNames=otherInputs["bufferNames"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)
        return {},'Failed: No arguments provided'


    creds = gen.getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'


    for bufferName in bufferNames:
        message=rabbitGetOneMessage(creds['host'],bufferName,port=creds['port'])
        if message=='':
            logger.autolog(level=1,message='No messages in buffer, nothing to do.',format=logFormat)
            return {},'Success'
        elif not message:
            logger.autolog(level=1,message='Unable to read buffer....',format=logFormat)
            return {},'Failed: Unable to read buffer'         
        else:
            if rabbitSendToWorkersQueue(creds['host'],json.loads(message)):
                return {},'Success'
            else:
                return {},'Failed: Error while moving message from buffer'

    return {},'Success'


####################################
# GENERIC
####################################




##################
# PUBLISH
##################

def rabbitSendToWorkersQueue(rabbitServer,body,auto_delete=False,durable=True,arguments={'x-max-priority':5},priority=0,heartbeat=1800):

    try:
        location=body["request"]['location'].lower()
    except Exception as e:
        logger.autolog(level=1, message="Unable to find LOCATION in message. Aborting message...", format="|RABBITSEND|")
        logger.autolog(level=1, message=str(e), format="|RABBITSEND|")
        return False    

    #If worker's location is in a vpod or sPod, make the queue auto_delete
    try:
        if 'vpod' in location or 'spod' in location or 'dpod' in location:
            auto_delete=True
    except Exception as e:
        pass

    logger.autolog(level=1, message="Sending message to queue : WORKERS."+location, format="|RABBITSEND|")

    return rabbitSend(rabbitServer, 'WORKERS.'+location,body,auto_delete=auto_delete,durable=durable,arguments=arguments,priority=priority,heartbeat=heartbeat)



def rabbitSendToRegisterExchange(rabbitServer,body,port='5672', durable=False,arguments={'x-max-priority':5},priority=0,heartbeat=1800):
    logger.autolog(level=1, message="Sending message to exchange : REGISTERS", format="|RABBITEXCHANGESEND|")
    return rabbitSendToExchange(rabbitServer, 'REGISTERS', 'fanout' ,body,port=port,durable=durable, arguments=arguments,priority=priority,heartbeat=heartbeat)


def rabbitSendToLoggersExchange(rabbitServer,body,port='5672',arguments={'x-max-priority':5},priority=0,heartbeat=1800):
    #logger.autolog(level=1, message="Sending message to exchange : LOGGERS", format="|RABBITEXCHANGESEND|")
    return rabbitSendToExchange(rabbitServer, 'LOGGERS', 'fanout' ,body,port=port,arguments=arguments,priority=priority,heartbeat=heartbeat)



def rabbitSendToBufferQueue(rabbitServer,body,auto_delete=True,durable=True,arguments={'x-max-priority':5},priority=0,heartbeat=1800):

    try:
        bufferName=body["request"]['bufferName'].lower()
    except Exception as e:
        logger.autolog(level=1, message="Unable to find BUFFER NAME in message. Aborting message...", format="|RABBITSEND|")
        return False    

    logger.autolog(level=1, message="Sending message to queue : BUFFERS."+bufferName, format="|RABBITSEND|")

    return rabbitSend(rabbitServer, 'BUFFERS.'+bufferName,body,auto_delete=auto_delete,durable=durable,arguments=arguments,priority=priority,heartbeat=heartbeat)



def rabbitSend(rabbitServer, queueName,body,auto_delete=False,durable=True,arguments={'x-max-priority':5},priority=0,heartbeat=1800):
    try:
        body=json.dumps(body)
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitServer,heartbeat=heartbeat,blocked_connection_timeout=3600))
        channel = connection.channel()
        channel.queue_declare(queue=queueName,durable=durable,auto_delete=auto_delete,arguments=arguments)
        channel.basic_publish(exchange='', routing_key=queueName,body=body,properties=pika.BasicProperties(delivery_mode = 2, priority = priority))
        connection.close()
    except Exception as e:
        logger.autolog(level=1, message="Error while rabbit send...", format="|RABBITSEND|")
        logger.autolog(level=1, message=str(e), format="|RABBITSEND|")
        return False
    return True



def rabbitSendToExchange(rabbitServer, exchangeName,exchangeType,body,port='5672',durable=True, arguments={'x-max-priority':5},priority=0,heartbeat=1800):
    try:
        body=json.dumps(body)
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitServer,port=port,heartbeat=heartbeat,blocked_connection_timeout=3600))
        channel = connection.channel()
        channel.exchange_declare(exchange=exchangeName,durable=durable,exchange_type=exchangeType,arguments=arguments)
        channel.basic_publish(exchange=exchangeName, routing_key='',body=body,properties=pika.BasicProperties(delivery_mode = 2, priority = priority))
        connection.close()
    except Exception as e:
        logger.autolog(level=1, message="Error while rabbit exchange send...", format="|RABBITEXCHANGESEND|")
        logger.autolog(level=1, message=str(e), format="|RABBITEXCHANGESEND|")
        return False
    return True



##################
# RPC
##################

def rabbitResponseRPC(rabbitServer, corrId,  queueName,body):
    try:
        body=json.dumps(body)
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitServer))
        channel = connection.channel()
        #channel.queue_declare(queue=queueName)
        channel.basic_publish(exchange='', routing_key=queueName,body=body,properties=pika.BasicProperties(correlation_id=corrId))
        connection.close()
    except Exception as e:
        logger.autolog(level=1, message="Error while rabbit RPC response ...", format="|RABBITRPCRESPONSE|")
        logger.autolog(level=1, message=str(e), format="|RABBITRPCRESPONSE|")
        return False
    return True



def rabbitSendRPC(rabbitServer, corrId , rcvQueueName, body,exchangeName='',sendQueueName=''):


    global rpcResponse
    rpcResponse=''

    def on_response(ch, method, props, body):
        logger.autolog(level=1, message="RPC Call Got Response" , format="|RABBITRPCSEND|")
        if corrId == props.correlation_id:
            global rpcResponse
            logger.autolog(level=1, message="Response received on Queue "  + str(rcvQueueName) , format="|RABBITRPCSEND|")
            #logger.autolog(level=1, message=str(body) , format="|RABBITRPCSEND|")
            rpcResponse = body

    try:
        rcvQueueName=rcvQueueName+"-"+str(random.randint(1,1000000000))
        corrId=rcvQueueName
        logger.autolog(level=1, message="Creating queue to receive RPC response. Queue Name is " + str(rcvQueueName), format="|RABBITRPCSEND|")
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitServer))
        channel = connection .channel()
        channel.queue_declare(queue=rcvQueueName,exclusive=True)

        logger.autolog(level=1, message="Sending message to RPC server. Queue is " + str(sendQueueName) + " Exchange is "  + str(exchangeName) , format="|RABBITRPCSEND|")
        body=json.dumps(body)
        channel.basic_publish(exchange=exchangeName, routing_key=sendQueueName,body=body,properties=pika.BasicProperties(correlation_id=corrId,reply_to=rcvQueueName))

        logger.autolog(level=1, message="Start listening for response on Queue "  + str(rcvQueueName) , format="|RABBITRPCSEND|")
        channel.basic_consume(on_response, no_ack=True,queue=rcvQueueName)

        while rpcResponse=='':
            #time.sleep(1)
            connection.process_data_events()
        
        channel.close()
        return rpcResponse

    except Exception as e:
        logger.autolog(level=1, message="Error while rabbit RPC send...", format="|RABBITRPCSEND|")
        logger.autolog(level=1, message=str(e), format="|RABBITRPCSEND|")
        return False




##################
# CONSUME
##################




def rabbitListenToWorkersQueue(rabbitServer,location,callback,port='5672',auto_delete=False,durable=True,arguments={'x-max-priority':5},heartbeat=3600):

    #If vpod is part of the location string, it comes from a vpod so we need to enable auto_delete
    if 'vpod' in str(location) or 'spod' in str(location)or 'dpod' in str(location):
        auto_delete=True

    rabbitListenBlocking(rabbitServer,'WORKERS.'+location.lower(),callback,port=port,auto_delete=auto_delete,durable=durable,arguments=arguments,heartbeat=heartbeat)






def rabbitListenBlocking(rabbitServer,queueName,callback,port='5672',auto_delete=False,durable=True,arguments={'x-max-priority':5},heartbeat=600):
    logger.autolog(level=1, message="Starting the Rabbit, listeing...", format="|RABBITLISTEN|")
    logger.autolog(level=1, message="Rabbit: " + str(rabbitServer), format="|RABBITLISTEN|")
    logger.autolog(level=1, message="Port: " + str(port), format="|RABBITLISTEN|")
    logger.autolog(level=1, message="Queuename: " + str(queueName), format="|RABBITLISTEN|")


    try:
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitServer,port=port,heartbeat=heartbeat,blocked_connection_timeout=3600))
        channel = connection.channel()
        channel.queue_declare(queue=queueName, durable=durable,auto_delete=auto_delete,arguments=arguments)
        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(callback,queue=queueName)
        channel.start_consuming()
    except Exception as e:
        logger.autolog(level=1, message="Error while rabbit listen...", format="|RABBITLISTEN|")
        logger.autolog(level=1, message=str(e), format="|RABBITLISTEN|")




def rabbitListenBlockingExchange(rabbitServer,exchangeName,exchangeType,queueName,callback,port='5672',auto_delete=False,exclusive=False,durable=True,arguments={'x-max-priority':5},heartbeat=600):
    logger.autolog(level=1, message="Starting the Rabbit, exchange listening...", format="|RABBITEXCHANGELISTEN|")
    logger.autolog(level=1, message="Rabbit: " + str(rabbitServer), format="|RABBITEXCHANGELISTEN|")
    logger.autolog(level=1, message="Port: " + str(port), format="|RABBITEXCHANGELISTEN|")
    logger.autolog(level=1, message="ExchangeName: " + str(exchangeName), format="|RABBITEXCHANGELISTEN|")
    logger.autolog(level=1, message="Queuename: " + str(queueName), format="|RABBITEXCHANGELISTEN|")

    try:
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitServer,port=port,socket_timeout=5,heartbeat=heartbeat,blocked_connection_timeout=3600))
        channel = connection.channel()
        channel.exchange_declare(exchange=exchangeName,exchange_type=exchangeType,arguments=arguments)
        channel.queue_declare(queue=queueName, durable=durable,auto_delete=auto_delete,exclusive=exclusive,arguments=arguments)
        channel.queue_bind(exchange=exchangeName,queue=queueName)
        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(callback,queue=queueName)
        channel.start_consuming()
    except Exception as e:
        logger.autolog(level=1, message="Error while rabbit exchange listen...", format="|RABBITEXCHANGELISTEN|")
        logger.autolog(level=1, message=str(e), format="|RABBITEXCHANGELISTEN|")





def rabbitGetOneMessage(rabbitServer,queueName,port='5672',auto_delete=True,durable=True,arguments={'x-max-priority':5},heartbeat=600):
    try:
        parameters = pika.ConnectionParameters(host=rabbitServer,port=port,heartbeat=heartbeat,blocked_connection_timeout=1800)
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue=queueName, durable=durable,auto_delete=auto_delete,arguments=arguments)
        method_frame, header_frame, body = channel.basic_get(queue=queueName)

        if isinstance(method_frame, type(None)):
            connection.close()
            return ''

        try:
            if method_frame.NAME == 'Basic.GetEmpty':
                connection.close()
                return ''
        except Exception as e:
            pass

        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
        connection.close()
        logger.autolog(level=1, message="Got message... " +str(body), format="|RABBITLISTEN|")
        return body

    except Exception as e:
        logger.autolog(level=1, message="Error while rabbit listen ONE message...", format="|RABBITLISTEN|")
        logger.autolog(level=1, message=str(e), format="|RABBITLISTEN|")
        return False
