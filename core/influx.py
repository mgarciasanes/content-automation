from influxdb import InfluxDBClient
import sys

modulesPath=r'/root/scripts/core/'
sys.path.append(modulesPath)

import logger as logger
import genFunctions as gen
import datetime
import timestring
import socket
import json


###################################
#  global variables
###################################


credsFile=r'/root/repo/common/creds.cfg'

#The measurement name where sessions are stored in InfluxDb
#measurementSessions='inDemoAutomation_sessions'
measurementTasks='indemo_automation'

measurementSessions='registeredRequests'


#Name of hostname
host = socket.gethostname() 

###################################
#  Front-end functions
###################################

def getSessionDurationInMins(sessionId,target,otherInputs=[], logFormat=''):

    logger.autolog(level=1,message="Will check duration for session " + str(sessionId), format=logFormat)
    #Get Arguments
    if (type(otherInputs) is dict):
        try:
            id=otherInputs["id"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments' , format=logFormat)
            logger.autolog(level=1,message=str(e), format=logFormat)
            return {},'Failed: Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided' , format=logFormat)
        return {},'Failed: No arguments provided'

    query=r"select last(demoStatus) from  " +str(measurementSessions)+"  where demoStatus='active' and id='" + str(sessionId) + "'"
    lastSessionWithIdWithActive=readFromInfluxDb(query,influxDb=target['device'])
    if not lastSessionWithIdWithActive:
        logger.autolog(level=1,message=' Unable to get duration of session ' + query,format=logFormat)
        return {'sessionDurationInMin': 0}, 'Failed: Unable to get duration of session' + query
    if len(lastSessionWithIdWithActive)>1:
        logger.autolog(level=1,message='Got more than one result while querying session duration. Only one expected.',format=logFormat)
        return {'sessionDurationInMin': 0}, 'Failed: Got more than one result while querying session duration. Only one expected.'
    try:
        sessionStart=timestring.Date(lastSessionWithIdWithActive[0]['time']).date
        logger.autolog(level=1,message='sessionStart: ' + str(sessionStart), format=logFormat)
        logger.autolog(level=1,message='now: ' + str(datetime.datetime.now()) , format=logFormat) 
        sessionDurationInSeconds=(datetime.datetime.now()-sessionStart).total_seconds()
        sessionDurationInMinutes=sessionDurationInSeconds/60
        logger.autolog(level=1,message='sessionStart: ' + str(sessionStart) + ', sessionDurationInMinutes: ' + str(sessionDurationInMinutes) ,format=logFormat)
    except Exception as e:
        logger.autolog(level=1,message='Error while calculating session duration',format=logFormat)
        logger.autolog(level=1,message=str(e),format=logFormat)
        return {'sessionDurationInMin': 0}, 'Failed: Error while calculating session duration'
    return {'sessionDurationInMin': sessionDurationInMinutes }, 'Success'





###################################
#
###################################

def writeToInfluxDb(jsonPayload,influxDb='influxKube' ):

    if isinstance(influxDb, dict):
        creds = gen.getDemoCredentialsByName(credsFile,influxDb['database'])
        if not creds:
            logger.autolog(level=1,message='Unable to get creds for write function in influxdb [dict] ' + str(influxDb['database']))
            return False
        creds['port']=influxDb['port']

    if isinstance(influxDb, str):
        creds = gen.getDemoCredentialsByName(credsFile,influxDb)
        if not creds:
            logger.autolog(level=1,message='Unable to get creds for write function in influxdb [str] ' + str(influxDb))
            return False

    #logger.autolog(level=1,message='About to execute  write  operation in ' + str(influxDb) + ' payload: ' + str(jsonPayload) )

    try:
        client = InfluxDBClient(creds['host'],creds['port'], creds['username'], creds['password'], creds['dbName'])
    except Exception as e:
        logger.autolog(level=0,message='ERROR in InfluxDB: Unable to get influDB Client for write operation')
        logger.autolog(level=0,message=e)
        return False

    if not client.write_points(jsonPayload):
        logger.autolog(level=0,message='ERROR in InfluxDB: Unable to execute write operation in ' + str(influxDb) + ' payload: ' + jsonPayload )
        return False
    else:
        return True

def readFromInfluxDb(query,influxDb='influxKube' ):

    if isinstance(influxDb, dict):
        creds = gen.getDemoCredentialsByName(credsFile,influxDb['database'])
        if not creds:
            logger.autolog(level=1,message='Unable to get creds for read function in influxdb [dict] ' + str(influxDb['database']))
            return False
        creds['port']=influxDb['port']
        #influxDb=influxDb['database']

    if isinstance(influxDb, str):
        creds = gen.getDemoCredentialsByName(credsFile,influxDb)
        if not creds:
            logger.autolog(level=1,message='Unable to get creds for read function in influxdb [str] ' + str(influxDb))
            return False

    try:
        client = InfluxDBClient(creds['host'],creds['port'], creds['username'], creds['password'], creds['dbName'])
    except Exception as e:
        logger.autolog(level=0,message='ERROR in InfluxDB: Unable to get influDB Client or unable to get credentials for influxDb  for ' + str(influxDb) + ' with query: ' + str(query))
        logger.autolog(level=0,message=e)
        return False

    try:
        #logger.autolog(level=1,message='About to execute  read operation in ' + str(influxDb) + ' query: ' + query )
        rs = client.query(query)  
        #logger.autolog(level=1,message='Response of read operation is ' + str(rs) )
        result=list(rs.get_points())
        #logger.autolog(level=1,message='Returning  ' + str(result))
        return result 
    except Exception as e:
        logger.autolog(level=0,message='ERROR in InfluxDB: Unable to execute read operation in ' + str(influxDb) + ' query: ' + query )
        logger.autolog(level=0,message=e)
        return False
        

def recordExecutedTasks(executedTasks,influxDb='influxKube'):
    return writeToInfluxDb(createJsonForInfluxFromExecutedTasks(executedTasks),influxDb=influxDb)



def markSessionCancelled(sessionDetails,influxDb='influxKube',comments='',serialStatus=''):
    return writeToInfluxDb(createJsonForInfluxFromSessionDetails(sessionDetails,measurement=measurementSessions,demoStatus="cancelled",comments=comments,serialStatus=serialStatus),influxDb=influxDb)

def markSessionStarting(sessionDetails,influxDb='influxKube',comments='',serialStatus=''):
    return writeToInfluxDb(createJsonForInfluxFromSessionDetails(sessionDetails,measurement=measurementSessions,demoStatus="starting",comments=comments,serialStatus=serialStatus),influxDb=influxDb)


def markSessionExecuting(sessionDetails,influxDb='influxKube',comments='',serialStatus=''):
    return writeToInfluxDb(createJsonForInfluxFromSessionDetails(sessionDetails,measurement=measurementSessions,demoStatus="executing",comments=comments,serialStatus=serialStatus),influxDb=influxDb)


def markSessionExecuted(sessionDetails,influxDb='influxKube',comments='',serialStatus=''):
    return writeToInfluxDb(createJsonForInfluxFromSessionDetails(sessionDetails,measurement=measurementSessions,demoStatus="executed",comments=comments,serialStatus=serialStatus),influxDb=influxDb)


def markSessionCleaning(sessionDetails,influxDb='influxKube',comments='',serialStatus=''):
    return writeToInfluxDb(createJsonForInfluxFromSessionDetails(sessionDetails,measurement=measurementSessions,demoStatus="cleaning",comments=comments,serialStatus=serialStatus),influxDb=influxDb)


def markSessionScheduled(sessionDetails,influxDb='influxKube',comments='',serialStatus=''):
    return writeToInfluxDb(createJsonForInfluxForScheduler(sessionDetails,measurement=measurementSessions,demoStatus="scheduled",comments=comments,serialStatus=serialStatus),influxDb=influxDb)

def markSessionDeleting(sessionDetails,influxDb='influxKube',comments='',serialStatus=''):
    return writeToInfluxDb(createJsonForInfluxFromSessionDetails(sessionDetails,measurement=measurementSessions,demoStatus="deleting",comments=comments,serialStatus=serialStatus),influxDb=influxDb)

def markSessionQueued(sessionDetails,influxDb='influxKube',comments='',serialStatus=''):
    return writeToInfluxDb(createJsonForInfluxFromSessionDetails(sessionDetails,measurement=measurementSessions,demoStatus="queued",comments=comments,serialStatus=serialStatus),influxDb=influxDb)

def markSessionQueuedV2(sessionDetails,influxDb='influxKube',comments='',serialStatus=''):
    return writeToInfluxDb(createJsonForInfluxForScheduler(sessionDetails,measurement=measurementSessions,demoStatus="queued",comments=comments,serialStatus=serialStatus),influxDb=influxDb)

def markSessionActive(sessionDetails,influxDb='influxKube',comments='',serialStatus=''):
    return writeToInfluxDb(createJsonForInfluxFromSessionDetails(sessionDetails,measurement=measurementSessions,demoStatus="active",comments=comments,serialStatus=serialStatus),influxDb=influxDb)

def markSessionError(sessionDetails,influxDb='influxKube',comments='',serialStatus=''):
    return writeToInfluxDb(createJsonForInfluxFromSessionDetails(sessionDetails,measurement=measurementSessions,demoStatus="error",comments=comments,serialStatus=serialStatus),influxDb=influxDb)

def markSessionStopped(sessionDetails,influxDb='influxKube',comments='',serialStatus=''):
    return writeToInfluxDb(createJsonForInfluxFromSessionDetails(sessionDetails,measurement=measurementSessions,demoStatus="stopped",comments=comments,serialStatus=serialStatus),influxDb=influxDb)

def markSessionDeleted(sessionDetails,influxDb='influxKube',comments='',serialStatus=''):
    return writeToInfluxDb(createJsonForInfluxFromSessionDetails(sessionDetails,measurement=measurementSessions,demoStatus="deleted",comments=comments,serialStatus=serialStatus),influxDb=influxDb)

def markSessionComplete(sessionDetails,influxDb='influxKube',comments='',serialStatus=''):
    return writeToInfluxDb(createJsonForInfluxFromSessionDetails(sessionDetails,measurement=measurementSessions,demoStatus="complete",comments=comments,serialStatus=serialStatus),influxDb=influxDb)

def markSessionForDeletion(sessionDetails, influxDb='influxKube',comments='',serialStatus=''):
    return writeToInfluxDb(createJsonForInfluxFromSessionDetails(sessionDetails,measurement=measurementSessions,demoStatus="toBeDeleted",comments=comments,serialStatus=serialStatus),influxDb=influxDb)

def markSessionQueuedFromId(sessionId,influxDb='influxKube',comments='',serialStatus=''):
    sessionDetails=getSessionDetails(sessionId,influxDb=influxDb)
    return writeToInfluxDb(createJsonForInfluxFromSessionDetails(sessionDetails,measurement=measurementSessions,demoStatus="queued",comments=comments,serialStatus=serialStatus),influxDb=influxDb)

def getSessionStatusHistory(sessionId, limit=100 ,influxDb='influxKube'):
    historyQuery="select datacenter as dc, demo, id, anycpwd, vpod, owner, comments, demoStatus , type, host, priority from "+measurementSessions+" where id='"+sessionId+"'"
    sessionStatusHistory=readFromInfluxDb(historyQuery,influxDb=influxDb)
    if sessionStatusHistory == False:
        return False
    return sessionStatusHistory[-limit:]    

def getStartupTasksStatus(sessionId, limit=100 , influxDb='influxKube'):
    startupTasksQuery="select taskName,taskStatus from (select last(status) as taskStatus,taskName  from "+measurementTasks+" where id='"+sessionId+"' AND taskAction='startup' group by taskNameTag) group by id,taskNameTag"
    sessionStartupTasks=readFromInfluxDb(startupTasksQuery,influxDb=influxDb)
    return sessionStartupTasks[-limit:]    

def getCleanupTasksStatus(sessionId, limit=100 ,influxDb='influxKube'):
    cleanupTasksQuery="select taskName,taskStatus from (select last(status) as taskStatus,taskName  from "+measurementTasks+" where id='"+sessionId+"' AND taskAction='cleanup' group by taskNameTag) group by id,taskNameTag"
    sessionCleanupTasks=readFromInfluxDb(cleanupTasksQuery,influxDb=influxDb)
    return sessionCleanupTasks[-limit:]    



def getAllSessionsWithStatus(sessionStatus='.*',influxDb='influxKube',limit=100):
    query=r'select dc,demo,id,anycpwd,vpod,owner,comments,status as demoStatus,type,priority,host  from (select datacenter as dc,demo,id,anycpwd,vpod,owner,host,comments,last(demoStatus) as status,type,priority   from   '+measurementSessions+r'  group by demo,id)  where "status" =~ /'+sessionStatus+'/ group by demo,id'
    allSessions=readFromInfluxDb(query,influxDb=influxDb)
    return allSessions[-limit:]    


def getAllSessionsWithFilters(sessionStatus='.*',sessionId='.*',sessionOwner='.*',sessionDemo='.*',sessionDc='.*',sessionLocation='.*',timeRangeStart="",timeRangeEnd="",  influxDb='influxKube',limit=300):

    #logger.autolog(level=1, message="Time Range Start: " + str(timeRangeStart))
    #logger.autolog(level=1, message="Time Range End: " + str(timeRangeEnd))

    
    timeRangeQuery=' and  time > now() - 1d '
    if timeRangeStart!="":
        if timeRangeEnd!="":
            timeRangeQuery=" and  time > '" +str(timeRangeStart) + "' and time < '" + str(timeRangeEnd)+"'" 

    query=r'select dc,demo,id,owner,comments,controller,status as demoStatus,location  from (select datacenter as dc,demo,id,owner,location,comments,controller,last(demoStatus) as status from '+measurementSessions+r'  group by demo,id)  where "status" =~ /'+sessionStatus+'/ and "id" =~ /'+sessionId+'/  and  "owner" =~ /'+sessionOwner+'/  and "dc" =~ /'+sessionDc+'/ and "location" =~ /'+sessionLocation+'/  and "demo" =~ /'+sessionDemo+'/  '+timeRangeQuery + ' group by demo,id'

    #logger.autolog(level=1, message=str(query))
    allSessions=readFromInfluxDb(query,influxDb=influxDb)
    return allSessions[-limit:]   


def getSessionById(sessionId,influxDb='influxKube'):
    query=" select datacenter as dc,demo,id,owner,comments,location,last(demoStatus)as demoStatus from "+measurementSessions+"  where id='"+sessionId+"' group by id "
    session=readFromInfluxDb(query,influxDb=influxDb)

    if not session:
        logger.autolog(level=1, message="Failed to get record from influxdb")
        return False
 
    if (len(session)>1):
        logger.autolog(level=1, message="Expected only one record while querying for single session, got more than one. Something went wrong...")
        logger.autolog(level=1, message=str(session))
        return False

    return session


def getSession(sessionId,influxDb='influxKube'):
    #query=" select id,datacenter as dc,*,last(demoStatus) as status from "+measurementSessions+"  where id='"+sessionId+"' group by id "
    query=" select *,id,last(demoStatus) from "+measurementSessions+"  where id='"+sessionId+"' group by id "
    session=readFromInfluxDb(query,influxDb=influxDb)

    if session == False:
        logger.autolog(level=1, message="Failed to get record from influxdb")
        return False
 
    if (len(session)>1):
        logger.autolog(level=1, message="Expected only one record while querying for single session, got more than one. Something went wrong...")
        logger.autolog(level=1, message=str(session))
        return False

    return session

def getSessionType(sessionDetails,influxDb='influxKube'):
    session=getSession(sessionDetails['id'], influxDb=influxDb)
    if session:
        return session[0]['type']
    return False


def getSessionPriority(sessionDetails,influxDb='influxKube'):
    session=getSession(sessionDetails['id'], influxDb=influxDb)
    if session:
        return session[0]['priority']
    return False

def getSessionField(sessionDetails,field='',influxDb='influxKube'):
    session=getSession(sessionDetails['id'], influxDb=influxDb)
    if session:
        try:
            return session[0][field]
        except Exception as e:
            logger.autolog(level=1, message='Error while trying to get value ' + field + ' from influx row')
            logger.autolog(level=1, message=str(e))
            return False
    return False


def getSessionFieldBySessionId(sessionId,field='',influxDb='influxKube'):
    session=getSession(sessionId, influxDb=influxDb)
    if session:
        try:
            return session[0][field]
        except Exception as e:
            logger.autolog(level=1, message='Error while trying to get value ' + field + ' from influx row')
            logger.autolog(level=1, message=str(e))
            return False
    return False




def getSessionDetails(sessionId,influxDb='influxKube'):
    session=getSession(sessionId, influxDb=influxDb)
    if session == False:
        return False
    if session != []:
        return session[0]
    if session == []:
        return session





def createJsonForInfluxFromExecutedTasks(executedTasks,measurement=measurementTasks):
#    json_body = [
#        {
#            "measurement": "cpu_load_short",
#            "tags": {
#                "host": "server01",
#                "region": "us-west"
#            },
#            "time": "2009-11-10T23:00:00Z",
#           "fields": {
#                "Float_value": 0.64,
#                "Int_value": 3,
#                "String_value": "Text",
#                "Bool_value": True
#            }
#        }
#    ]

    jsonForInflux=[]
    for task in executedTasks:
        try:
            jsonForInflux.append({"measurement":measurement,"tags":{"datacenter":task['datacenter'],"demo":task['demo'],"owner":task['owner'],"id":task["id"],"taskNameTag":task["taskName"],"taskAction":task['taskAction']},"fields":{"status": task["taskStatus"],"taskName":task["taskName"],"demoStatus":"active"}})
        except Exception as e:
            logger.autolog(level=1,message='Error while reading executedTasks list...' )
            logger.autolog(level=1,message=str(e))
    return jsonForInflux

def createJsonForInfluxFromSessionDetails(sessionDetails,measurement=measurementSessions,demoStatus="queued",comments="",priority='',type='session',serialStatus=''):

    tags=sessionDetails
    #logger.autolog(level=1,message='sessionDetails is: ' + str(sessionDetails))


    if 'time' in tags.keys():
       tags.pop('time') 
       #logger.autolog(level=1,message='Removing Time key from session Details...' )

    if 'last' in tags.keys():
       tags.pop('last') 

#    for key in tags.keys():
#        if 'last_' in key:
#            tags.pop(key)


    if 'demoStatus' in tags.keys():
       tags.pop('demoStatus') 

    if 'comments' in tags.keys():
       tags.pop('comments') 

    if 'serialStatus' in tags.keys():
       tags.pop('serialStatus') 

    global host
    # The code below will keep the host controller (if found in session details). What we want now is to always update the host controller value with the current controller's name 
    #try:
    #    if tags['host']:
    #        logger.autolog(level=1,message='Controller host Found! ' + str(tags['host']))
    #        #hostController=tags['host']  
    #except Exception as e:
    #    logger.autolog(level=1,message='Controller host or type Not Found! Using local host as controller' )
    #    #logger.autolog(level=1,message=str(e))
    #    tags['host']=host


    # The code below will keep the host controller (if found in session details) when it is not a remote request. If it is a remote requests and host exists, it will not update it. 
    #if 'type' in tags.keys():
    #    if tags['type']=='remote':
    #        if 'host' in tags.keys():
    #            logger.autolog(level=1,message='It is a remote request. Controller host found in session details, will leave it as it is: ' + str(tags['host']))
    #        else:
    #            logger.autolog(level=1,message='Warning:It is a remote request without controller host!! Leaving it blank...')
    #    else:
    #        logger.autolog(level=1,message='It is not a remote request. Will make current controller owner of this request:  ' +  str(host))
    #        tags['host']=host
    #else:
    #    logger.autolog(level=1,message='No request type found in session details. Will make current controller owner of this request:  ' +  str(host))
    #    tags['host']=host





    recipePath=''
    try:
        if tags['recipePath']:
            pass
            #logger.autolog(level=1,message='Recipe Path Found! ' + str(tags['recipePath']))
    except Exception as e:
        #logger.autolog(level=1,message='Recipe Path Not Found! Using empty string.' )
        tags['recipePath']=''


    demo=''
    try:
        if tags['demo']:
            pass
            #logger.autolog(level=1,message='Demo tag Found! ' + str(tags['demo']))
    except Exception as e:
        #logger.autolog(level=1,message='Demo tag  Not Found! Using default' )
        tags['demo']='NO NAME'


    #if priority != '':
    #    tags['priority']==priority
    #else:
    #    try:
    #        if tags['priority']:
    #            logger.autolog(level=1,message='Priority tag Found! ' + str(tags['priority']))
    #    except Exception as e:
    #        logger.autolog(level=1,message='Priority tag  Not Found! Using default' )
    #        tags['priority']='1'


    #for ind in tags.keys():
    #    if isinstance(tags[ind],dict):
    #        tags[ind]=json.dumps(tags[ind])
    #tags["xml"]=json.dumps(tags)

    jsonForInflux=[]
    try:
        #jsonForInflux.append({"measurement":measurement,"tags":{"datacenter":sessionDetails['datacenter'],"anycpwd":sessionDetails['anycpwd'],"vpod":sessionDetails['vpod'],"demo":sessionDetails['demo'],"owner":sessionDetails['owner'],"id":sessionDetails["id"],"priority":priority,"type":type,'host':hostController,'recipePath':sessionDetails["recipePath"],'recipeName':sessionDetails["recipeName"]},"fields":{"demoStatus":demoStatus,"comments":comments}})
        jsonForInflux.append({"measurement":measurement,"tags":tags,"fields":{"demoStatus":demoStatus,"comments":comments,"serialStatus":serialStatus}})

    except Exception as e:
        logger.autolog(level=1,message='Error while reading sessionDetails list...' )
        logger.autolog(level=1,message=str(e))
    return jsonForInflux


def createJsonForInfluxForScheduler(sessionDetails,measurement=measurementSessions,demoStatus="queued",comments="",priority='',type='session'):

    logger.autolog(level=1,message='sessionDetails is: ' + str(sessionDetails))

    jsonForInflux=[]
    try:
        #jsonForInflux.append({"measurement":measurement,"tags":{"datacenter":sessionDetails['datacenter'],"anycpwd":sessionDetails['anycpwd'],"vpod":sessionDetails['vpod'],"demo":sessionDetails['demo'],"owner":sessionDetails['owner'],"id":sessionDetails["id"],"priority":priority,"type":type,'host':hostController,'recipePath':sessionDetails["recipePath"],'recipeName':sessionDetails["recipeName"]},"fields":{"demoStatus":demoStatus,"comments":comments}})
        jsonForInflux.append({"measurement":measurement,"tags":sessionDetails,"fields":{"demoStatus":demoStatus,"comments":comments}})

    except Exception as e:
        logger.autolog(level=1,message='Error while reading sessionDetails list...' )
        logger.autolog(level=1,message=str(e))
    return jsonForInflux

