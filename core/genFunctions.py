
#####################################
# Import Public Modules
#####################################
import logging
import threading
import urllib2
import glob
import json
import os
import os.path
import requests
import sys
import time
import xml.dom.minidom
import yaml
import platform
from xml.dom import minidom
import datetime
import xmltodict
import select 
import random
import string
import logger as logger
import paramiko
import re
import timestring
logging.getLogger('paramiko').setLevel(logging.ERROR)

#####################################
# Import Controller Modules
#####################################
import settings
import inMemoryStorage
import dcloudController
import influx

#####################################
# Global Variables
#####################################

credsFile=r'/root/scripts/modules/creds.cfg'
recipesPath=r'/root/scripts/recipes/'




#####################################
#  FRONT_END FUNCTIONS
#####################################


def remotePowerShellFunction(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            psArgs=otherInputs["psArgs"]
            functionName=otherInputs["psFunctionName"]
            scriptPath=otherInputs["psScriptPath"]
            scriptName=otherInputs["psScriptName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    creds=getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    status,stdOut,stdErr=sshExecPsFunction(creds, scriptPath, scriptName,functionName, psArgs)
    if status==True:
        if stdErr=='[]':
            return {'result':stdOut},'Success'
        else:
            return {'result':stdErr}, 'Failed: ' + str(stdErr)
    else:
        return {'result':status}, 'Failed: ' + str(status)  



def remotePowerShellScript(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            psArgs=otherInputs["psArgs"]
            scriptPath=otherInputs["psScriptPath"]
            scriptName=otherInputs["psScriptName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    creds=getDemoCredentialsByName(target['file'],target['device'])
    if not creds:
        return {},'Failed: Unable to get credentials for target'

    status,stdOut,stdErr=sshExecPsScript(creds, scriptPath, scriptName, str(psArgs))
    if status==True:
        if stdErr=='[]':
            return {'result':stdOut},'Success'
        else:
            return {'result':stdErr}, 'Failed: ' + str(stdErr)
    else:
        return {'result':status}, 'Failed: ' + str(status)





def pingHost(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            host=otherInputs["host"]
            waitForEcho=otherInputs["waitForEcho"]
            waitMaxSeconds=otherInputs["waitMaxSeconds"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    loopCount=0
    response=1
    if waitForEcho == "Yes":
        while(loopCount < waitMaxSeconds) and (response!=0):
            response=os.system('ping -c 1 -W 1 ' + str(host) + ' > /dev/null 2>&1 ' )
            loopCount=loopCount+1
    else:
        response=os.system('ping -c 1 -W 1 ' + str(host) + ' > /dev/null 2>&1 ' )

    if response == 512:
        return {},'Failed:Unable to resolve name'
    if response == 256:
        return {},'Failed:No response'
    if response == 0:
        return {},'Success'


def isRequestReadyForCleanup(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            id=otherInputs["id"]
            dc=otherInputs["dc"]
            sessionMaxDurationMins=otherInputs["sessionMaxDurationMins"]
            expirationMode=otherInputs["expirationMode"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'


    if expirationMode=='session':
        sessionStatus= dcloudController.getSessionStatus(id,dc)
        if sessionStatus == 'deleted':
            logger.autolog(level=1,message='session is deleted',format=logFormat)
            return {'readyToDelete': True}, 'Success'

        if sessionStatus == 'active':
            logger.autolog(level=1,message='session is active',format=logFormat)
            return {'readyToDelete': False}, 'Success'

        return {'readyToDelete': False}, 'Failed: Unable to get valid response from dcloudController'


    if expirationMode=='timeout':
      
        #If Max Duration is set to "never", do not delete...
        if sessionMaxDurationMins=='never':
            return {'readyToDelete': False}, 'Success'
  
        INFLUXMEASSUREMENT='registeredRequests'
        INFLUXDB='influxKube'
        query=r"select last(demoStatus) from  " +str(INFLUXMEASSUREMENT)+"  where demoStatus='active' and id='" + str(id) + "'"
        lastSessionWithIdWithActive=influx.readFromInfluxDb(query,influxDb=INFLUXDB)
        if not lastSessionWithIdWithActive:
            logger.autolog(level=1,message=' Unable to get duration of session ' + query,format=logFormat)
            return {'sessionDurationInMin': 0}, 'Failed: Unable to get duration of session' + query
        if len(lastSessionWithIdWithActive)>1:
            logger.autolog(level=1,message='Got more than one result while querying session duration. Only one expected.',format=logFormat)
            return {'sessionDurationInMin': 0}, 'Failed: Got more than one result while querying session duration. Only one expected.'
        try:
            sessionStart=timestring.Date(lastSessionWithIdWithActive[0]['time']).date
            logger.autolog(level=1,message='sessionStart: ' + str(sessionStart), format=logFormat)
            logger.autolog(level=1,message='now: ' + str(datetime.datetime.now()) , format=logFormat) 
            sessionDurationInSeconds=(datetime.datetime.now()-sessionStart).total_seconds()
            sessionDurationInMinutes=sessionDurationInSeconds/60
            logger.autolog(level=1,message='sessionStart: ' + str(sessionStart) + ', sessionDurationInMinutes: ' + str(sessionDurationInMinutes) ,format=logFormat)
            if sessionDurationInMinutes > sessionMaxDurationMins:
                logger.autolog(level=1,message='session ' + str(id) + ' has reached timeout: ' + str(sessionMaxDurationMins) ,format=logFormat)
                return {'readyToDelete': True}, 'Success'
            else:
                logger.autolog(level=1,message='session ' + str(id) + ' is still active. It hasnt reached timeout: ' + str(sessionMaxDurationMins) ,format=logFormat)
                return {'readyToDelete': False}, 'Success'
        except Exception as e:
            logger.autolog(level=1,message='Error while calculating session duration',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {'readyToDelete': False}, 'Failed: Error while calculating current session duration'


def takeItEasy(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            seconds=otherInputs["seconds"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    if waitForSeconds(seconds):
        return {}, 'Success'
    else:
        return {},'Failed:Unable to resolve name'



def takeItEasyRandom(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            seconds=otherInputs["seconds"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    if waitForSeconds(random.randint(0,seconds)):
    #if waitForSeconds(1):
        return {}, 'Success'
    else:
        return {},'Failed:Unable to resolve name'



def htmlFromTemplate(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            templateFile=otherInputs["templateFile"]
            username=otherInputs["username"]
            password=otherInputs["password"]
            destUrl=otherInputs["destUrl"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    try:
        htmlFile=open(templateFile,'r')
        htmlContent=htmlFile.read()
        htmlFile.close()
        htmlContent=htmlContent.replace('USERNAME',username)
        htmlContent=htmlContent.replace('PASSWORD',password)
        htmlContent=htmlContent.replace('REQ_URL',destUrl)

        return {'htmlOutput': htmlContent}, 'Success'
    except Exception as e:
        logger.autolog(level=1,message='Error while creating HTML output',format=logFormat)
        logger.autolog(level=1,message=str(e),format=logFormat)
        return {},'Failed: Error while creating HTML output ' +str(e)


def htmlRedirect(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            destUrl=otherInputs["destUrl"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'

    return {'htmlRedirect': destUrl}, 'Success'


def selectUrlByDc(sessionId, target, otherInputs=[], logFormat=''):
    if (type(otherInputs) is dict):
        try:
            urls=otherInputs["urls"]
            dc=otherInputs["dc"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments',format=logFormat)
            logger.autolog(level=1,message=str(e),format=logFormat)
            return {},'Failed:Error while reading input arguments'
    else:
        logger.autolog(level=1,message='No arguments provided',format=logFormat)  
        return {},'Failed: No arguments provided'
    try:
        url=urls[dc]
    except Exception as e:
        logger.autolog(level=1,message='Error while getting URL',format=logFormat)
        logger.autolog(level=1,message=str(e),format=logFormat)
        return {}, 'Failed: Error while getting URL ' + str(e)

    return {'url': url}, 'Success'



#####################################
#  GENERIC FUNCTIONS
#####################################

def doNothing(sessionFile):
    return True

def validateRequestV2(sessionDetails,api,logFormat):

    if api=='request-create':
        #Check if Recipe Name is present
        try:
            if sessionDetails['recipeName']:
                logger.autolog(level=1,message='Request Recipe Name Found! ' + str(sessionDetails['recipeName']) ,format=logFormat)
        except Exception as e:
            logger.autolog(level=1,message='Request Recipe Name Not Found! Ignoring request' ,format=logFormat)
            return False,'Error: Recipe Name not Provided. Ignoring request...'

        #Check if Recipe Path is present
        try:
            if sessionDetails['recipePath']:
                logger.autolog(level=1,message='Request Recipe Path Found! ' + str(sessionDetails['recipePath']) ,format=logFormat)
        except Exception as e:
            logger.autolog(level=1,message='Request Recipe Path Not Found! Ignoring request' ,format=logFormat)
            return False,'Error: Recipe Path not Provided. Ignoring request...'


        #Check if Request URLHOST is present
        try:
            if sessionDetails['urlhost']:
                logger.autolog(level=1,message='Request from haproxy with urlhost cookie! ' + str(sessionDetails['urlhost']) ,format=logFormat)
                dcNumber=sessionDetails['urlhost'].split('dc-')[1].split('.com')[0]
                sessionDetails['dcNumber']=dcNumber
        except Exception as e:
            logger.autolog(level=1,message='WARNING!! Request without urlhost!! Using RTPs dcNumber as default ' ,format=logFormat)
            #return False,'Error: Request from haproxy without urlhost. Needed to identity request location!. Ignoring request...'
            sessionDetails['dcNumber']='01'

        #Check if Request haproxyDestUrl is present
        try:
            if sessionDetails["haproxyDestUrl"]:
                logger.autolog(level=1,message='Request from haproxy with haproxyDestUrl! ' + str(sessionDetails["haproxyDestUrl"]) ,format=logFormat)
        except Exception as e:
            logger.autolog(level=1,message='WARNING!! Request without haproxyDestUrl !! Using dCloud  URL  as default ' ,format=logFormat)
            sessionDetails['haproxyDestUrl']='https://dcloud2-rtp.cisco.com'


        #Controller
        try:
            sessionDetails['controller']=settings.thisDc
        except Exception as e: 
            logger.autolog(level=1,message='Error: Unable to assign controller:  ' +str(e) ,format=logFormat)


        #Datacenter
        try:
            if sessionDetails['datacenter']:
                logger.autolog(level=1,message='Request datacenter Found! ' + str(sessionDetails['datacenter']) ,format=logFormat)
                sessionDetails['datacenter']=sessionDetails['datacenter'].lower()
            else:
                logger.autolog(level=1,message='Request datacenter fond but empty!! Using local DC' ,format=logFormat)
                sessionDetails['datacenter']=settings.thisDc                
        except Exception as e: 
            try:
                #dcNumber=str(inMemoryStorage.redisGet("DCNUMBER"))
                #localDc=dcIndex[dcNumber]
                logger.autolog(level=1,message='Request datacenter Not Found! Using local DC: ' + str(settings.thisDc) ,format=logFormat)
                sessionDetails['datacenter']=settings.thisDc
            except Exception as e:
                logger.autolog(level=1,message='Error: Datacenter was not found in request and getting localDc info from REDIS failed. Ignoring request. Error: ' +str(e) ,format=logFormat)
                return False,'Error: Request DC not Provided and REDIS failed to provide localDc info. Ignoring request...'+str(e)

        #Owner
        try:
            if sessionDetails['owner']=='':
                logger.autolog(level=1,message='Request owner empty! Ignoring request' ,format=logFormat)
                return False,'Error: Request owner is empty. Ignoring request...'
            logger.autolog(level=1,message='Request owner Found! ' + str(sessionDetails['owner']) ,format=logFormat)
            sessionDetails['owner']=sessionDetails['owner'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request owner Not Found! Ignoring request' ,format=logFormat)
            return False,'Error: Request owner not Provided. Ignoring request...'

        #Cloud
        try:
            if sessionDetails['cloud']:
                logger.autolog(level=1,message='Request cloud Found! ' + str(sessionDetails['cloud']) ,format=logFormat)
                sessionDetails['cloud']=sessionDetails['cloud'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request cloud Not Found! Using dcloud ' ,format=logFormat)
            sessionDetails['cloud']='dcloud'

        #Pod
        try:
            if sessionDetails['pod']:
                logger.autolog(level=1,message='Request POD Found! ' + str(sessionDetails['pod']) ,format=logFormat)
                sessionDetails['pod']=sessionDetails['pod'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request POD Not Found! Using sharedservices ' ,format=logFormat)
            sessionDetails['pod']='sharedservices'



        #vpod-automation
        #try:
        #    if sessionDetails['pod']:
        #        logger.autolog(level=1,message='Request vPOD Found! ' + str(sessionDetails['vpod-automation']) ,format=logFormat)
        #        sessionDetails['pod']='vpod.'+str(sessionDetails['vpod-automation'])
        #except Exception as e:
        #    logger.autolog(level=1,message='Request vPOD Not Found! Using sharedservices ' ,format=logFormat)


        #ID
        try:
            if sessionDetails['id']:
                logger.autolog(level=1,message='Session ID Found! ' + str(sessionDetails['id']) ,format=logFormat)           
        except Exception as e:
            logger.autolog(level=1,message='Session ID Not Found in payload. Checking ID in API URL' ,format=logFormat)
            try:
                if sessionDetails['idInUrl']!='0':
                    logger.autolog(level=1,message='Session ID Found in API URL! Using it!' ,format=logFormat)
                    sessionDetails['id']=sessionDetails['idInUrl']
                else:
                    logger.autolog(level=1,message='Session ID in API URL is 0! Using timestamp!' ,format=logFormat)
                    sessionDetails['id']=str(int(time.time()))
            except Exception as e:
                logger.autolog(level=1,message='Session ID not found in API URL! Using timestamp!' ,format=logFormat)
                sessionDetails['id']=str(int(time.time()))

        #Location
        try:
            if sessionDetails['location']:
                logger.autolog(level=1,message='Request location Found! ' + str(sessionDetails['location']) ,format=logFormat)
                sessionDetails['location']=sessionDetails['location'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request location Not Found! Using cloud + datacenter + pod  ' ,format=logFormat)
            if sessionDetails['pod']=='sharedservices':
                sessionDetails['location']=sessionDetails['cloud']+'.'+sessionDetails['datacenter']+'.sharedservices'
            if sessionDetails['pod']=='vpod':
                sessionDetails['location']=sessionDetails['cloud']+'.'+sessionDetails['datacenter']+'.vpod.'+sessionDetails['id']
            if sessionDetails['pod']=='spod':
                sessionDetails['location']=sessionDetails['cloud']+'.'+sessionDetails['datacenter']+'.spod.'+sessionDetails['id']
        logger.autolog(level=1,message='Request LOCATION set to:  ' + sessionDetails['location']  ,format=logFormat)


        #unique key
        try:
            if sessionDetails['key']:
                logger.autolog(level=1,message='Request KEY Found! ' + str(sessionDetails['key']) ,format=logFormat)
                sessionDetails['key']=sessionDetails['key'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request KEY Not Found! Using location  + id ' ,format=logFormat)
            if sessionDetails['pod']=='sharedservices':
                sessionDetails['key']=sessionDetails['location']+'.'+sessionDetails['id']
            if sessionDetails['pod']=='vpod':
                sessionDetails['key']=sessionDetails['location']
            if sessionDetails['pod']=='spod':
                sessionDetails['key']=sessionDetails['location']
            if sessionDetails['pod']=='dpod':
                sessionDetails['key']=sessionDetails['location']
        logger.autolog(level=1,message='Request KEY set to:  ' + sessionDetails['key']  ,format=logFormat)


        requestFullDetails={}
        requestFullDetails['request']=sessionDetails
        requestFullDetails['output']=""
        requestFullDetails['status']=""
        requestFullDetails['comments']=""
        requestFullDetails['workerId']=""
        requestFullDetails['action']=""
        requestFullDetails['statusUrl']=settings.controllerWebUrl+r'/requeststatus.php?id='+str(sessionDetails['id'])
        requestFullDetails['loaderUrl']=settings.controllerWebUrl+r'/loader-instant.php?id='+str(sessionDetails['id'])
        requestFullDetails['statusApi']=settings.controllerApiUrl+r'/api/v2.0/request/'+str(sessionDetails['id'])
        requestFullDetails['errorUrl']=settings.controllerWebUrl+r'/errorPage.php?id='+str(sessionDetails['id'])


        #Loading Recipe tasks....
        recipeDict,eMsg=getRecipe(sessionDetails['recipeName'],sessionDetails['recipePath'])

        #If recipe is found, load the tasks into the Request Full Details Dictionary
        if recipeDict:
            for action in ['Start','End','Stop']:
                try:
                    for task in recipeDict[action]:
                        task['status']='unavailable'
                        task['output']='unavailable'
                except Exception as e:
                    pass
            requestFullDetails['tasks']=recipeDict
            #Update OUTPUT URL
            try:
                requestFullDetails['outputUrl']=settings.controllerWebUrl+r'/requestoutput.php?htmlTemplate='+str(recipeDict['Output']['htmlTemplate'])+'&id='+str(sessionDetails['id'])
            except Exception as e:
                requestFullDetails['outputUrl']=settings.controllerWebUrl+r'/requestoutput.php?htmlTemplate=notemplate.php&id='+str(sessionDetails['id'])

            #Update Recipe Name
            try:
                if requestFullDetails['request']['demo']:
                    logger.autolog(level=1,message='Demo Name provided on request payload, using it :  ' + requestFullDetails['request']['demo'] ,format=logFormat)
            except Exception as e:
                try:
                    if recipeDict["Details"]["demo"]:
                        requestFullDetails['request']['demo']=recipeDict["Details"]["demo"]
                        logger.autolog(level=1,message='Demo Name not provided on request payload, using the one on recipe :  ' + recipeDict["Details"]["demo"] ,format=logFormat)
                except Exception as e:
                    requestFullDetails['request']['demo']="NO NAME IN RECIPE"
                    logger.autolog(level=1,message='No demo name provided on request nor in recipe. Marking as NO NAME' ,format=logFormat)


        #If Recipe not found...
        else:
            #If location is sharedservices, ignore request...
            if "sharedservices" in sessionDetails["location"]:
                logger.autolog(level=1,message='Failed to find matching recipe:  ' + sessionDetails['recipeName']+ " in folder: " +sessionDetails['recipePath'] ,format=logFormat)
                return False,'Error: Unable to find recipe in local repo.'

            #Not a sharedservices request, let's create a empty list for tasks
            #Create Empty Tasks 
            requestFullDetails['tasks']={'Start':[],'Stop':[],'End':[]}
            #Update Recipe Name
            try:
                if requestFullDetails['request']['demo']:
                    logger.autolog(level=1,message='Demo Name provided on request payload, using it :  ' + requestFullDetails['request']['demo'] ,format=logFormat)
            except Exception as e:
                requestFullDetails['request']['demo']="NO RECIPE"
                logger.autolog(level=1,message='No demo name provided on request and recipe not found in controller. Marking as NO RECIPE' ,format=logFormat)

        return requestFullDetails,''

    if api=='request-status':

        #Check if Request URLHOST is present
        try:
            if sessionDetails['urlhost']:
                logger.autolog(level=1,message='Request from haproxy with urlhost cookie! ' + str(sessionDetails['urlhost']) ,format=logFormat)
                dcNumber=sessionDetails['urlhost'].split('dc-')[1].split('.com')[0]
                #dcIndex={'01':'rtp','02':'sng','03':'lon','05':'chi','08':'sjc'}
                sessionDetails['dcNumber']=dcNumber
                #sessionDetails['datacenter']=dcIndex[dcNumber]
        except Exception as e:
            logger.autolog(level=1,message='WARNING!! Request without urlhost!! Using RTPs dcNumber as default ' ,format=logFormat)
            #return False,'Error: Request from haproxy without urlhost. Needed to identity request location!. Ignoring request...'
            sessionDetails['dcNumber']='01'


        #Check if Request haproxyDestUrl is present
        try:
            if sessionDetails["haproxyDestUrl"]:
                logger.autolog(level=1,message='Request from haproxy with haproxyDestUrl! ' + str(sessionDetails["haproxyDestUrl"]) ,format=logFormat)
        except Exception as e:
            logger.autolog(level=1,message='WARNING!! Request without haproxyDestUrl !! Using dCloud  URL  as default ' ,format=logFormat)
            sessionDetails['haproxyDestUrl']='https://dcloud2-rtp.cisco.com'

        #Datacenter
        try:
            if sessionDetails['datacenter']:
                logger.autolog(level=1,message='Request datacenter Found! ' + str(sessionDetails['datacenter']) ,format=logFormat)
                sessionDetails['datacenter']=sessionDetails['datacenter'].lower()
        except Exception as e: 
            try:
                #dcNumber=str(inMemoryStorage.redisGet("DCNUMBER"))
                #localDc=dcIndex[dcNumber]
                logger.autolog(level=1,message='Request datacenter Not Found! Using local DC: ' + str(settings.thisDc) ,format=logFormat)
                sessionDetails['datacenter']=settings.thisDc
            except Exception as e:
                logger.autolog(level=1,message='Error: Datacenter was not found in request and getting localDc info from REDIS failed. Ignoring request. Error: ' +str(e) ,format=logFormat)
                return False,'Error: Request DC not Provided and REDIS failed to provide localDc info. Ignoring request...'+str(e)

        #Cloud
        try:
            if sessionDetails['cloud']:
                logger.autolog(level=1,message='Request cloud Found! ' + str(sessionDetails['cloud']) ,format=logFormat)
                sessionDetails['cloud']=sessionDetails['cloud'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request cloud Not Found! Using dcloud ' ,format=logFormat)
            sessionDetails['cloud']='dcloud'

        try:
            if sessionDetails['pod']:
                logger.autolog(level=1,message='Request POD Found! ' + str(sessionDetails['pod']) ,format=logFormat)
                sessionDetails['pod']=sessionDetails['pod'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request POD Not Found! Using sharedservices ' ,format=logFormat)
            sessionDetails['pod']='sharedservices'

        #try:
        #    if sessionDetails['vpod-automation']:
        #        logger.autolog(level=1,message='Request vPOD Found! ' + str(sessionDetails['vpod-automation']) ,format=logFormat)
        #        sessionDetails['pod']='vpod.'+str(sessionDetails['vpod-automation'])
        #except Exception as e:
        #    logger.autolog(level=1,message='Request vPOD Not Found! Using sharedservices ' ,format=logFormat)

        #try:
        #    if sessionDetails['location']:
        #        logger.autolog(level=1,message='Request location Found! ' + str(sessionDetails['location']) ,format=logFormat)
        #except Exception as e:
        #    logger.autolog(level=1,message='Request location Not Found! Using cloud + datacenter + pod  ' ,format=logFormat)
        #    sessionDetails['location']=sessionDetails['cloud']+'.'+sessionDetails['datacenter']+'.'+sessionDetails['pod']

        try:
            if sessionDetails['id']:
                logger.autolog(level=1,message='Session ID Found! ' + str(sessionDetails['id']) ,format=logFormat)           
        except Exception as e:
            logger.autolog(level=1,message='Session ID Not Found in payload. Checking ID in API URL' ,format=logFormat)
            try:
                if sessionDetails['idInUrl']!='0':
                    logger.autolog(level=1,message='Session ID Found in API URL! Using it!' ,format=logFormat)
                    sessionDetails['id']=sessionDetails['idInUrl']
                else:
                    logger.autolog(level=1,message='Session ID not found in API URL! Ignoring request!' ,format=logFormat)
                    return False,'Error: Request ID not Provided. Ignoring request...'
            except Exception as e:
                logger.autolog(level=1,message='Session ID not found in API URL! Ignoring request!' ,format=logFormat)
                return False,'Error: Request ID not Provided. Ignoring request...'

        #Location
        try:
            if sessionDetails['location']:
                logger.autolog(level=1,message='Request location Found! ' + str(sessionDetails['location']) ,format=logFormat)
                sessionDetails['location']=sessionDetails['location'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request location Not Found! Using cloud + datacenter + pod  ' ,format=logFormat)
            if sessionDetails['pod']=='sharedservices':
                sessionDetails['location']=sessionDetails['cloud']+'.'+sessionDetails['datacenter']+'.sharedservices'
            if sessionDetails['pod']=='vpod':
                sessionDetails['location']=sessionDetails['cloud']+'.'+sessionDetails['datacenter']+'.vpod.'+sessionDetails['id']
            if sessionDetails['pod']=='spod':
                sessionDetails['location']=sessionDetails['cloud']+'.'+sessionDetails['datacenter']+'.spod.'+sessionDetails['id']
        logger.autolog(level=1,message='Request LOCATION set to:  ' + sessionDetails['location']  ,format=logFormat)


        #unique key
        try:
            if sessionDetails['key']:
                logger.autolog(level=1,message='Request KEY Found! ' + str(sessionDetails['key']) ,format=logFormat)
                sessionDetails['key']=sessionDetails['key'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request KEY Not Found! Using location  + id ' ,format=logFormat)
            if sessionDetails['pod']=='sharedservices':
                sessionDetails['key']=sessionDetails['location']+'.'+sessionDetails['id']
            if sessionDetails['pod']=='vpod':
                sessionDetails['key']=sessionDetails['location']
            if sessionDetails['pod']=='spod':
                sessionDetails['key']=sessionDetails['location']
            if sessionDetails['pod']=='dpod':
                sessionDetails['key']=sessionDetails['location']
        logger.autolog(level=1,message='Request KEY set to:  ' + sessionDetails['key']  ,format=logFormat)

        return sessionDetails,''



def validateRequest(sessionDetails,api,logFormat):


    #Check if Recipe Name is present
    try:
        if sessionDetails['recipeName']:
            logger.autolog(level=1,message='Request Recipe Name Found! ' + str(sessionDetails['recipeName']) ,format=logFormat)
    except Exception as e:
        logger.autolog(level=1,message='Request Recipe Name Not Found! Ignoring request' ,format=logFormat)
        return False,'Error: Recipe Name not Provided. Ignoring request...'

    #Check if Recipe Path is present
    try:
        if sessionDetails['recipePath']:
            logger.autolog(level=1,message='Request Recipe Path Found! ' + str(sessionDetails['recipePath']) ,format=logFormat)
    except Exception as e:
        logger.autolog(level=1,message='Request Recipe Path Not Found! Ignoring request' ,format=logFormat)
        return False,'Error: Recipe Path not Provided. Ignoring request...'



    if api=='r0':

        #Check if Request ID is present
        try:
            if sessionDetails['id']:
                logger.autolog(level=1,message='Request ID Found! ' + str(sessionDetails['id']) ,format=logFormat)
        except Exception as e:
            logger.autolog(level=1,message='Request ID Not Found! Ignoring request' ,format=logFormat)
            return False,'Error: Request ID not Provided. Ignoring request...'

        #try:
        #    if sessionDetails['type']:
        #        logger.autolog(level=1,message='Session Type Found! ' + str(sessionDetails['type'])  ,format=logFormat)
        #except Exception as e:
        #    logger.autolog(level=1,message='Session Type Not Found! Using type:remote'  ,format=logFormat)
        #    sessionDetails['type']='remote'

        #try:
        #    if sessionDetails['priority']:
        #        logger.autolog(level=1,message='Request Priority Found! ' + str(sessionDetails['priority'])  ,format=logFormat)
        #except Exception as e:
        #    logger.autolog(level=1,message='Request Priority Not Found! Using type: 1'  ,format=logFormat)
        #    sessionDetails['priority']='1'

        #try:
        #    if sessionDetails['host']:
        #        logger.autolog(level=1,message='Controller host Found! ' + str(sessionDetails['host'])  ,format=logFormat)
        #except Exception as e:
        #    logger.autolog(level=1,message='Controller host Not Found! Ignoring request...' ,format=logFormat)
        #    return False,'Error: Host Controller not Provided. Ignoring request...'

        return sessionDetails,''


    if api=='task':

        #Check if Request ID is present
        try:
            if sessionDetails['id']:
                logger.autolog(level=1,message='Request ID Found! ' + str(sessionDetails['id']) ,format=logFormat)
        except Exception as e:
            logger.autolog(level=1,message='Request ID Not Found! Ignoring request' ,format=logFormat)
            return False,'Error: Request ID not Provided. Ignoring request...'

        try:
            if sessionDetails['datacenter']:
                logger.autolog(level=1,message='Request datacenter Found! ' + str(sessionDetails['datacenter']) ,format=logFormat)
                sessionDetails['datacenter']=sessionDetails['datacenter'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request datacenter Not Found! Ignoring request' ,format=logFormat)
            return False,'Error: Request ID not Provided. Ignoring request...'

        try:
            if sessionDetails['cloud']:
                logger.autolog(level=1,message='Request cloud Found! ' + str(sessionDetails['cloud']) ,format=logFormat)
                sessionDetails['cloud']=sessionDetails['cloud'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request cloud Not Found! Using dcloud ' ,format=logFormat)
            sessionDetails['cloud']='dcloud'

        try:
            if sessionDetails['pod']:
                logger.autolog(level=1,message='Request POD Found! ' + str(sessionDetails['pod']) ,format=logFormat)
                sessionDetails['pod']=sessionDetails['pod'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request POD Not Found! Using sharedservices ' ,format=logFormat)
            sessionDetails['pod']='sharedservices'

        #try:
        #    if sessionDetails['vpod-automation']:
        #        logger.autolog(level=1,message='Request vPOD Found! ' + str(sessionDetails['vpod-automation']) ,format=logFormat)
        #        sessionDetails['pod']='vpod.'+str(sessionDetails['vpod-automation'])
        #except Exception as e:
        #    logger.autolog(level=1,message='Request vPOD Not Found! Using sharedservices ' ,format=logFormat)

        try:
            if sessionDetails['location']:
                logger.autolog(level=1,message='Request location Found! ' + str(sessionDetails['location']) ,format=logFormat)
        except Exception as e:
            logger.autolog(level=1,message='Request location Not Found! Using ' +sessionDetails['cloud']+'.'+sessionDetails['datacenter']+'.'+sessionDetails['pod'],format=logFormat)
            sessionDetails['location']=sessionDetails['cloud']+'.'+sessionDetails['datacenter']+'.'+sessionDetails['pod']


        #unique key
        try:
            if sessionDetails['key']:
                logger.autolog(level=1,message='Request KEY Found! ' + str(sessionDetails['key']) ,format=logFormat)
                sessionDetails['key']=sessionDetails['key'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request KEY Not Found! Using location  + id ' ,format=logFormat)
            if sessionDetails['pod']=='sharedservices':
                sessionDetails['key']=sessionDetails['location']+'.'+sessionDetails['id']
            if sessionDetails['pod']=='vpod':
                sessionDetails['key']=sessionDetails['location']
            if sessionDetails['pod']=='spod':
                sessionDetails['key']=sessionDetails['location']
        logger.autolog(level=1,message='Request KEY set to:  ' + sessionDetails['key']  ,format=logFormat)

        
        #Building the Request Full Details Dictionary
        requestFullDetails={}
        #Store the received Request details
        requestFullDetails['request']=sessionDetails
        requestFullDetails['output']=""
        requestFullDetails['status']=""
        requestFullDetails['comments']=""
        requestFullDetails['workerId']=""
        #Specify some general URL references
        requestFullDetails['statusUrl']=settings.controllerWebUrl+r'/requeststatus.php?id='+str(sessionDetails['id'])
        requestFullDetails['loaderUrl']=settings.controllerWebUrl+r'/loader-instant.php?id='+str(sessionDetails['id'])
        requestFullDetails['statusApi']=settings.controllerApiUrl+r'/api/v2.0/request/'+str(sessionDetails['id'])
        requestFullDetails['errorUrl']=settings.controllerWebUrl+r'/errorPage.php?id='+str(sessionDetails['id'])


        #Loading Recipe tasks....
        recipeDict,eMsg=getRecipe(sessionDetails['recipeName'],sessionDetails['recipePath'])
        #If recipe is found, load the tasks into the Request Full Details Dictionary
        if recipeDict:
            for action in ['Start','End','Stop']:
                try:
                    for task in recipeDict[action]:
                        task['status']='unavailable'
                        task['output']='unavailable'
                except Exception as e:
                    pass
            requestFullDetails['tasks']=recipeDict

            #Update OUTPUT URL
            try:
                requestFullDetails['outputUrl']=settings.controllerWebUrl+r'/requestoutput.php?htmlTemplate='+str(recipeDict['Output']['htmlTemplate'])+'&id='+str(sessionDetails['id'])
            except Exception as e:
                requestFullDetails['outputUrl']=settings.controllerWebUrl+r'/requestoutput.php?htmlTemplate=notemplate.php&id='+str(sessionDetails['id'])

        #If Recipe not found...
        else:
            #If location is sharedservices, ignore request...
            if "sharedservices" in sessionDetails["location"]:
                logger.autolog(level=1,message='Failed to find matching recipe:  ' + sessionDetails['recipeName']+ " in folder: " +sessionDetails['recipePath'] ,format=logFormat)
                return False,'Error: Unable to find recipe in local repo.'
            #Not a sharedservices request, let's create a empty list for tasks
            requestFullDetails['tasks']={'Start':[],'Stop':[],'End':[]}
        return requestFullDetails,''


    if api=='q0':

        #Check if Request URLHOST is present
        try:
            if sessionDetails['urlhost']:
                logger.autolog(level=1,message='Request from haproxy with urlhost cookie! ' + str(sessionDetails['urlhost']) ,format=logFormat)
                dcNumber=sessionDetails['urlhost'].split('dc-')[1].split('.com')[0]
                #dcIndex={'01':'rtp','02':'sng','03':'lon','05':'sjc','08':'chi'}
                sessionDetails['dcNumber']=dcNumber
                sessionDetails['datacenter']=settings.dcIndex[dcNumber]
        except Exception as e:
            logger.autolog(level=1,message='Request without urlhost!!  Ignoring request' ,format=logFormat)
            return False,'Error: Request from haproxy without urlhost. Needed to identity request location!. Ignoring request...'

        try:
            if sessionDetails['owner']=='':
                logger.autolog(level=1,message='Request owner empty! Ignoring request' ,format=logFormat)
                return False,'Error: Request owner is empty. Ignoring request...'
            logger.autolog(level=1,message='Request owner Found! ' + str(sessionDetails['owner']) ,format=logFormat)
            sessionDetails['owner']=sessionDetails['owner'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request owner Not Found! Ignoring request' ,format=logFormat)
            return False,'Error: Request owner not Provided. Ignoring request...'


        try:
            if sessionDetails['cloud']:
                logger.autolog(level=1,message='Request cloud Found! ' + str(sessionDetails['cloud']) ,format=logFormat)
                sessionDetails['cloud']=sessionDetails['cloud'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request cloud Not Found! Using dcloud ' ,format=logFormat)
            sessionDetails['cloud']='dcloud'

        try:
            if sessionDetails['pod']:
                logger.autolog(level=1,message='Request POD Found! ' + str(sessionDetails['pod']) ,format=logFormat)
                sessionDetails['pod']=sessionDetails['pod'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request POD Not Found! Using sharedservices ' ,format=logFormat)
            sessionDetails['pod']='sharedservices'

        #try:
        #    if sessionDetails['vpod-automation']:
        #        logger.autolog(level=1,message='Request vPOD Found! ' + str(sessionDetails['vpod-automation']) ,format=logFormat)
        #        sessionDetails['pod']='vpod.'+str(sessionDetails['vpod-automation'])
        #except Exception as e:
        #    logger.autolog(level=1,message='Request vPOD Not Found! Using sharedservices ' ,format=logFormat)

        #try:
        #    if sessionDetails['location']:
        #        logger.autolog(level=1,message='Request location Found! ' + str(sessionDetails['location']) ,format=logFormat)
        #except Exception as e:
        #    logger.autolog(level=1,message='Request location Not Found! Using cloud + datacenter + pod  ' ,format=logFormat)
        #    sessionDetails['location']=sessionDetails['cloud']+'.'+sessionDetails['datacenter']+'.'+sessionDetails['pod']

        try:
            if sessionDetails['id']:
                logger.autolog(level=1,message='Session ID Found! ' + str(sessionDetails['id']) ,format=logFormat)
        except Exception as e:
            logger.autolog(level=1,message='Session ID Not Found! Using ID:timestamp' ,format=logFormat)
            sessionDetails['id']=str(int(time.time()))



        #Location
        try:
            if sessionDetails['location']:
                logger.autolog(level=1,message='Request location Found! ' + str(sessionDetails['location']) ,format=logFormat)
                sessionDetails['location']=sessionDetails['location'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request location Not Found! Using cloud + datacenter + pod  ' ,format=logFormat)
            if sessionDetails['pod']=='sharedservices':
                sessionDetails['location']=sessionDetails['cloud']+'.'+sessionDetails['datacenter']+'.sharedservices'
            if sessionDetails['pod']=='vpod':
                sessionDetails['location']=sessionDetails['cloud']+'.'+sessionDetails['datacenter']+'.vpod.'+sessionDetails['id']
            if sessionDetails['pod']=='spod':
                sessionDetails['location']=sessionDetails['cloud']+'.'+sessionDetails['datacenter']+'.spod.'+sessionDetails['id']
        logger.autolog(level=1,message='Request LOCATION set to:  ' + sessionDetails['location']  ,format=logFormat)


        #unique key
        try:
            if sessionDetails['key']:
                logger.autolog(level=1,message='Request KEY Found! ' + str(sessionDetails['key']) ,format=logFormat)
                sessionDetails['key']=sessionDetails['key'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request KEY Not Found! Using location  + id ' ,format=logFormat)
            if sessionDetails['pod']=='sharedservices':
                sessionDetails['key']=sessionDetails['location']+'.'+sessionDetails['id']
            if sessionDetails['pod']=='vpod':
                sessionDetails['key']=sessionDetails['location']
            if sessionDetails['pod']=='spod':
                sessionDetails['key']=sessionDetails['location']
        logger.autolog(level=1,message='Request KEY set to:  ' + sessionDetails['key']  ,format=logFormat)



        requestFullDetails={}
        requestFullDetails['request']=sessionDetails
        requestFullDetails['output']=""
        requestFullDetails['status']=""
        requestFullDetails['comments']=""
        requestFullDetails['workerId']=""
        requestFullDetails['statusUrl']=settings.controllerWebUrl+r'/requeststatus.php?id='+str(sessionDetails['id'])
        requestFullDetails['loaderUrl']=settings.controllerWebUrl+r'/loader-instant.php?id='+str(sessionDetails['id'])
        requestFullDetails['statusApi']=settings.controllerApiUrl+r'/api/v2.0/request/'+str(sessionDetails['id'])
        requestFullDetails['errorUrl']=settings.controllerWebUrl+r'/errorPage.php?id='+str(sessionDetails['id'])


        #Loading Recipe tasks....
        recipeDict,eMsg=getRecipe(sessionDetails['recipeName'],sessionDetails['recipePath'])
        #If recipe is found, load the tasks into the Request Full Details Dictionary
        if recipeDict:
            for action in ['Start','End','Stop']:
                try:
                    for task in recipeDict[action]:
                        task['status']='unavailable'
                        task['output']='unavailable'
                except Exception as e:
                    pass
            requestFullDetails['tasks']=recipeDict
        #If Recipe not found...
        else:
            #If location is sharedservices, ignore request...
            if "sharedservices" in sessionDetails["location"]:
                logger.autolog(level=1,message='Failed to find matching recipe:  ' + sessionDetails['recipeName']+ " in folder: " +sessionDetails['recipePath'] ,format=logFormat)
                return False,'Error: Unable to find recipe in local repo.'
            #Not a sharedservices request, let's create a empty list for tasks
            requestFullDetails['tasks']={'Start':[],'Stop':[],'End':[]}
        return requestFullDetails,''



    if api=='p0':

        try:
            if sessionDetails['datacenter']:
                logger.autolog(level=1,message='Request datacenter Found! ' + str(sessionDetails['datacenter']) ,format=logFormat)
                sessionDetails['datacenter']=sessionDetails['datacenter'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request datacenter Not Found! Ignoring request' ,format=logFormat)
            return False,'Error: Request DC  not Provided. Ignoring request...'

        try:
            if sessionDetails['owner']=='':
                logger.autolog(level=1,message='Request owner empty! Ignoring request' ,format=logFormat)
                return False,'Error: Request owner is empty. Ignoring request...'
            logger.autolog(level=1,message='Request owner Found! ' + str(sessionDetails['owner']) ,format=logFormat)
            sessionDetails['owner']=sessionDetails['owner'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request owner Not Found! Ignoring request' ,format=logFormat)
            return False,'Error: Request owner not Provided. Ignoring request...'

        try:
            if sessionDetails['cloud']:
                logger.autolog(level=1,message='Request cloud Found! ' + str(sessionDetails['cloud']) ,format=logFormat)
                sessionDetails['cloud']=sessionDetails['cloud'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request cloud Not Found! Using dcloud ' ,format=logFormat)
            sessionDetails['cloud']='dcloud'

        try:
            if sessionDetails['pod']:
                logger.autolog(level=1,message='Request POD Found! ' + str(sessionDetails['pod']) ,format=logFormat)
                sessionDetails['pod']=sessionDetails['pod'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request POD Not Found! Using sharedservices ' ,format=logFormat)
            sessionDetails['pod']='sharedservices'

        try:
            if sessionDetails['vpod-automation']:
                logger.autolog(level=1,message='Request vPOD Found! ' + str(sessionDetails['vpod-automation']) ,format=logFormat)
                sessionDetails['pod']='vpod.'+str(sessionDetails['vpod-automation'])
        except Exception as e:
            logger.autolog(level=1,message='Request vPOD Not Found! Using sharedservices ' ,format=logFormat)

        try:
            if sessionDetails['location']:
                logger.autolog(level=1,message='Request location Found! ' + str(sessionDetails['location']) ,format=logFormat)
        except Exception as e:
            logger.autolog(level=1,message='Request location Not Found! Using cloud + datacenter + pod  ' ,format=logFormat)
            sessionDetails['location']=sessionDetails['cloud']+'.'+sessionDetails['datacenter']+'.'+sessionDetails['pod']


        try:
            if sessionDetails['id']:
                logger.autolog(level=1,message='Session ID Found! ' + str(sessionDetails['id']) ,format=logFormat)
        except Exception as e:
            logger.autolog(level=1,message='Session ID Not Found! Using ID:timestamp' ,format=logFormat)
            sessionDetails['id']=str(int(time.time()))

        return sessionDetails,''





    if api=='schedule-create':


        try:
            if sessionDetails['datacenter']:
                logger.autolog(level=1,message='Request datacenter Found! ' + str(sessionDetails['datacenter']) ,format=logFormat)
                sessionDetails['datacenter']=sessionDetails['datacenter'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request datacenter Not Found! Ignoring request' ,format=logFormat)
            return False,'Error: Request ID not Provided. Ignoring request...'

        try:
            if sessionDetails['cloud']:
                logger.autolog(level=1,message='Request cloud Found! ' + str(sessionDetails['cloud']) ,format=logFormat)
                sessionDetails['cloud']=sessionDetails['cloud'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request cloud Not Found! Using dcloud ' ,format=logFormat)
            sessionDetails['cloud']='dcloud'

        try:
            if sessionDetails['pod']:
                logger.autolog(level=1,message='Request POD Found! ' + str(sessionDetails['pod']) ,format=logFormat)
                sessionDetails['pod']=sessionDetails['pod'].lower()
        except Exception as e:
            logger.autolog(level=1,message='Request POD Not Found! Using sharedservices ' ,format=logFormat)
            sessionDetails['pod']='sharedservices'

        try:
            if sessionDetails['vpod-automation']:
                logger.autolog(level=1,message='Request vPOD Found! ' + str(sessionDetails['vpod-automation']) ,format=logFormat)
                sessionDetails['pod']='vpod.'+str(sessionDetails['vpod-automation'])
        except Exception as e:
            logger.autolog(level=1,message='Request vPOD Not Found! Using sharedservices ' ,format=logFormat)

        try:
            if sessionDetails['location']:
                logger.autolog(level=1,message='Request location Found! ' + str(sessionDetails['location']) ,format=logFormat)
        except Exception as e:
            logger.autolog(level=1,message='Request location Not Found! Using cloud + datacenter + pod  ' ,format=logFormat)
            sessionDetails['location']=sessionDetails['cloud']+'.'+sessionDetails['datacenter']+'.'+sessionDetails['pod']

        try:
            if sessionDetails['schedule']:
                logger.autolog(level=1,message='Schedule Found! ' + str(sessionDetails['schedule']) ,format=logFormat)
        except Exception as e:
            logger.autolog(level=1,message='Schdule Not Found! Ignoring request' ,format=logFormat)
            return False,'Error: Schedule not Provided. Ignoring request...'

        try:
            if sessionDetails['cmdcomment']:
                logger.autolog(level=1,message='Command Comment Found! ' + str(sessionDetails['cmdcomment']) ,format=logFormat)
        except Exception as e:
            logger.autolog(level=1,message='Command Comment Not Found! Adding empty comment' ,format=logFormat)
            sessionDetails['cmdcomment']=''

        try:
            if sessionDetails['id']:
                newId=str(int(time.time()))
                logger.autolog(level=1,message='Session ID Found: ' + str(sessionDetails['id']) + ' IT WILL BE OVERWRITTEN BY : ' + str(newId)  ,format=logFormat)
                sessionDetails['id']=newId
        except Exception as e:
            logger.autolog(level=1,message='Session ID Not Found! Using ID:timestamp' ,format=logFormat)
            sessionDetails['id']=str(int(time.time()))

        try:
            if sessionDetails['action']:
                logger.autolog(level=1,message='Action Found: ' + str(sessionDetails['action']) + ' IT WILL BE OVERWRITTEN BY : create ' ,format=logFormat)
                sessionDetails['action']='create'
        except Exception as e:
            logger.autolog(level=1,message='Action Not Found! Using create'  ,format=logFormat)
            sessionDetails['action']='create'

        return sessionDetails,''


    if api=='schedule-delete':
        try:
            if sessionDetails['id']:
                logger.autolog(level=1,message='ID Found! ' + str(sessionDetails['id']) ,format=logFormat)
        except Exception as e:
            logger.autolog(level=1,message='ID Not Found! Ignoring request' ,format=logFormat)
            return False,'Error: ID not Provided. Ignoring request...'

        try:
            if sessionDetails['cmdcomment']:
                logger.autolog(level=1,message='Command Comment Found! ' + str(sessionDetails['cmdcomment']) ,format=logFormat)
        except Exception as e:
            logger.autolog(level=1,message='Command Comment Not Found! Adding empty comment' ,format=logFormat)
            sessionDetails['cmdcomment']=''

        try:
            if sessionDetails['action']:
                logger.autolog(level=1,message='Action Found: ' + str(sessionDetails['action']) + ' IT WILL BE OVERWRITTEN BY : delete ' ,format=logFormat)
                sessionDetails['action']='delete'
        except Exception as e:
            logger.autolog(level=1,message='Action Not Found! Using create'  ,format=logFormat)
            sessionDetails['action']='delete'

        return sessionDetails,''

    return False,'Unable to match API action'





def updateSessionDetails(requestStatus, task, action,  taskStatus, taskOutput,timeStart=False, timeEnd=False):

    try:
        logger.autolog(level=1,message='Updating session details with task: ' + str(task))
        task["output"]=taskOutput
        task["status"]=taskStatus
        task["timeStart"]=timeStart  if timeStart else 'undefined'
        task["timeEnd"]=timeEnd  if timeEnd  else 'undefined'

        for item in requestStatus["tasks"][action]:
            if item["name"]==task["name"]:
                for key in task.keys():
                    item[key]=task[key]
                return requestStatus
        requestStatus["tasks"][action].append(task)
        return requestStatus
    except Exception as e:
        logger.autolog(level=1,message='unable to update sessiondetails dictionary...: ' + str(e))
        return False



def getTasksFromRecipe(recipeName, taskType,taskAction, sourceFolder=recipesPath):

    if recipeName is None:
        logger.autolog(level=1, message='provided recipeName is not valid (None)')
        return False

    logger.autolog(level=1, message='About to get tasks for ' + str(recipeName) + ' with source folder ' + str(sourceFolder) + 'and action ' + str(taskType))
    files=getFilesInFolder(sourceFolder)
    taskFilesFound=[]
    fileMatch=False

    for file in files:
        try:
            recipeNameFound=getCfgFileValues(sourceFolder+file, 'Details')['name']
            if (recipeNameFound == recipeName):
                fileMatch=True
                branch=getCfgFileValues(sourceFolder+file, taskType)
                if branch:
                    taskFilesFound.append(branch)
                else:
                    logger.autolog(level=1, message='Warning: Unable to read recipe file ' + str(file))
        except Exception as e:
            logger.autolog(level=1, message='Unable to fetch recipe tasks-type or recipe name from file ' + str(file))
            logger.autolog(level=1, message=str(e))
            #return False

    if (len(taskFilesFound) > 1 ):
        errorMessage='WARNING: Found more than one recipe with name: ' + str(recipeName) + " Files found: " + str(taskFilesFound)
        logger.autolog(level=1,message=errorMessage)
        #taskFilesFound=[taskFilesFound[0]]
        return False,errorMessage
    if (len(taskFilesFound) < 1 ):
        if fileMatch:
            errorMessage='Couldnt find tasks for recipe with name ' + str(recipeName) + ' in folder ' + str(sourceFolder) + ' and section ' +str(taskType) 
            logger.autolog(level=1,message=errorMessage)
            return [],''
        else:
            errorMessage='Error: Couldnt find recipe with name ' + str(recipeName) + ' in folder ' + str(sourceFolder)
            logger.autolog(level=1,message=errorMessage)
            return False,errorMessage
    if (len(taskFilesFound) == 1 ):
        logger.autolog(level=1,message='Found recipe file for task type  '+  str(taskType) + ' for recipe ' + str(recipeName))
        #logger.autolog(level=1,message=str(taskFilesFound))
        for i in range(0,len(taskFilesFound[0])):
            taskFilesFound[0][i]['taskAction']=taskAction
        #logger.autolog(level=1,message='Returning ' + str(taskFilesFound[0]))
        return taskFilesFound[0],''



def getTypeFromRecipe(recipeName,sourceFolder=recipesPath):

    if recipeName is None:
        logger.autolog(level=1, message='provided recipeName is not valid (None)')
        return False

    logger.autolog(level=1, message='About to get type for ' + str(recipeName) + ' with source folder ' + str(sourceFolder))
    files=getFilesInFolder(sourceFolder)
    taskFilesFound=[]
    fileMatch=False

    for file in files:
        try:
            recipeNameFound=getCfgFileValues(sourceFolder+file, 'Details')['name']
            if (recipeNameFound == recipeName):
                fileMatch=True
                try:
                    recipeType=getCfgFileValues(sourceFolder+file, 'Details')['type']
                    if recipeType:
                        taskFilesFound.append(recipeType)
                except Exception as e:
                    logger.autolog(level=1, message='Warning: Unable to read recipe type on  ' + str(file) + 'using default start+stop')
                    taskFilesFound.append('start+stop')

        except Exception as e:
            logger.autolog(level=1, message='Unable to fetch recipe type or recipe name from file ' + str(file))
            logger.autolog(level=1, message=str(e))
            #return False

    if (len(taskFilesFound) > 1 ):
        errorMessage='WARNING: Found more than one recipe with name: ' + str(recipeName) + " Files found: " + str(taskFilesFound)
        logger.autolog(level=1,message=errorMessage)
        #taskFilesFound=[taskFilesFound[0]]
        return False,errorMessage
    if (len(taskFilesFound) < 1 ):
        if fileMatch:
            errorMessage='Couldnt find tasks for recipe with name ' + str(recipeName) + ' in folder ' + str(sourceFolder) + ' and section ' +str(taskType) 
            logger.autolog(level=1,message=errorMessage)
            return [],''
        else:
            errorMessage='Error: Couldnt find recipe with name ' + str(recipeName) + ' in folder ' + str(sourceFolder)
            logger.autolog(level=1,message=errorMessage)
            return False,errorMessage
    if (len(taskFilesFound) == 1 ):
        logger.autolog(level=1,message='Found recipe type for recipe ' + str(recipeName))
        return taskFilesFound[0],''




def getRecipe(recipeName,sourceFolder):

    if recipeName is None:
        logger.autolog(level=1, message='provided recipeName is not valid (None)')
        return False
    
    sourceFolder=recipesPath+sourceFolder

    logger.autolog(level=1, message='About to get recipe ' + str(recipeName) + ' with source folder ' + str(sourceFolder))
    files=getFilesInFolder(sourceFolder)
    taskFilesFound=[]
    fileMatch=False
    for file in files:
        try:
            recipeNameFound=getCfgFileValues(sourceFolder+file, 'Details')['name']
            if (recipeNameFound == recipeName):
                fileMatch=True
                recipeDict=readYamlFile(sourceFolder+file)
                if recipeDict:
                    taskFilesFound.append(recipeDict)
                else:
                    logger.autolog(level=1, message='Warning: Unable to read recipe file ' + str(file))
        except Exception as e:
            logger.autolog(level=1, message='Unable to fetch recipe tasks-type or recipe name from file ' + str(file))
            logger.autolog(level=1, message=str(e))
            #return False

    if (len(taskFilesFound) > 1 ):
        errorMessage='Error: Found more than one recipe with name: ' + str(recipeName)
        logger.autolog(level=1,message=errorMessage)
        return False,errorMessage
    if (len(taskFilesFound) < 1 ):
        if fileMatch:
            errorMessage='Couldnt find tasks for recipe with name ' + str(recipeName) + ' in folder ' + str(sourceFolder) 
            logger.autolog(level=1,message=errorMessage)
            return [],''
        else:
            errorMessage='Error: Couldnt find recipe with name ' + str(recipeName) + ' in folder ' + str(sourceFolder)
            logger.autolog(level=1,message=errorMessage)
            return False,errorMessage
    if (len(taskFilesFound) == 1 ):
        logger.autolog(level=1,message='Found recipe file for recipe ' + str(recipeName)  +'. ' + str(taskFilesFound))
        #logger.autolog(level=1,message='Returning ' + str(taskFilesFound[0]))
        return taskFilesFound[0],''


def getRecipeType(recipeName,sourceFolder=recipesPath,specificFolder=str()):
    if specificFolder is None:
        specificFolder=str()
    return getTypeFromRecipe(recipeName, sourceFolder=sourceFolder+str(specificFolder))


def getRecipeStartTasks(recipeName,sourceFolder=recipesPath,specificFolder=str()):
    if specificFolder is None:
        specificFolder=str()
    return getTasksFromRecipe(recipeName, 'Start', 'startup', sourceFolder=sourceFolder+str(specificFolder))

def getRecipeStopTasks(recipeName,sourceFolder=recipesPath,specificFolder=str()):
    if specificFolder is None:
        specificFolder=str()
    return getTasksFromRecipe(recipeName, 'Stop', 'stop', sourceFolder=sourceFolder+str(specificFolder))

def getRecipeEndTasks(recipeName,sourceFolder=recipesPath,specificFolder=str()):
    if specificFolder is None:
        specificFolder=str()
    return getTasksFromRecipe(recipeName, 'End', 'cleanup', sourceFolder=sourceFolder+str(specificFolder))



def getDemoTasks(demoName,sourceFolder=recipesPath,specificFolder=''):
    return getTasksFromRecipe(demoName, 'Tasks', 'startup', sourceFolder=sourceFolder+specificFolder)

def getDemoStop(demoName,sourceFolder=recipesPath,specificFolder=''):
    return getTasksFromRecipe(demoName, 'Stop','stop', sourceFolder=sourceFolder+specificFolder)

def getDemoCleanup(demoName,sourceFolder=recipesPath,specificFolder=''):
    return getTasksFromRecipe(demoName, 'Cleanup','cleanup', sourceFolder=sourceFolder+specificFolder)


def sanitizeString(inputString,charsToRemove=r'!@#$%^&*()_+=`~/?<>,|:;.'):
#Remove special characters
    outputString=re.sub('['+str(charsToRemove)+']','',inputString)
    return outputString


def sanitizePassword(inputString,charsToRemove=r'!@#$%^&*()_+=`~/?<>,|:;.'):
#Remove special characters
    inputString=str(inputString)
    inputString=inputString.split('@')[0]
    outputString=re.sub('['+str(charsToRemove)+']','',inputString)
    return outputString


#def sanitizePassword(inputString,charsToRemove=r'!@#$%^&*()_+=`~/?<>,|:;.'):
#Remove special characters
#    inputString=str(inputString)
#    inputString=inputString.split('@')[0]
#    outputString=re.sub('['+str(charsToRemove)+']','',inputString)
#    return outputString

def generateRandomString(len):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(len))


def procDollarString(dollarString,sessionDetails,outputs):
    switch=False
    endString=""
    toEvaluate=""
    null=''
    #sessionDetails=eval(str(sessionDetails['json']))
    try:
        sessionDetails=json.loads(str(sessionDetails['payload']))
    except Exception as e:
        sessionDetails=eval(str(sessionDetails['json']))


    if dollarString is None:
        return ''
    if type(dollarString) is not str:
        return dollarString

    for c in dollarString:
        if switch:
            if c !="$":
                toEvaluate=toEvaluate+c
            else: 
                switch=False
                try:
                    endString=endString+str(eval(toEvaluate))
                    toEvaluate=""
                except Exception as e:
                    logger.autolog(level=1,message='Got exception while evaluating input ' + toEvaluate)
                    logger.autolog(level=1,message=e)
                    return False
        else:
            if c =="$":
                switch=True
            else:
                endString=endString+c
    return endString



def procDollarStringV2(dollarString,sessionDetails):
    switch=False
    endString=""
    toEvaluate=""
    null=''

    if dollarString is None:
        return ''
    if type(dollarString) is not str:
        return dollarString

    for c in dollarString:
        if switch:
            if c !="$":
                toEvaluate=toEvaluate+c
            else: 
                switch=False
                try:
                    if "sessionDetails" in  toEvaluate:
                        toEvaluate=toEvaluate.replace("sessionDetails","sessionDetails['request']")
                        endString=endString+str(eval(toEvaluate))
                        toEvaluate=""

                    elif "outputs" in  toEvaluate:
                        #Get task name
                        taskNameWithQuote=toEvaluate.split("outputs[")[1].split("][")[0]
                        if "'" in taskNameWithQuote:
                            taskName=taskNameWithQuote.split("'")[1]
                        else:
                            taskName=taskNameWithQuote.split('"')[1]
                        logger.autolog(level=1,message='Got taskName: ' + str(taskName))

                        #Get Output
                        outputName=toEvaluate.split("["+taskNameWithQuote+"]")[1]
                        logger.autolog(level=1,message='Got outputName: ' + str(outputName))

                        #Get task
                        logger.autolog(level=1,message='Got task action : ' + str(sessionDetails["action"]))
                        for task in sessionDetails["tasks"][sessionDetails["action"]]:
                            if task["name"]==taskName:
                                logger.autolog(level=1,message='Found a matching task name, will try to get output:  ' + "task['output']"+str(outputName))
                                endString=endString+str(eval("task['output']"+str(outputName)))
                        toEvaluate=""

                    else:
                        endString=endString+str(eval("sessionDetails"+toEvaluate))
                        toEvaluate=""      
                except Exception as e:
                    logger.autolog(level=1,message='Got exception while evaluating input ' + toEvaluate)
                    logger.autolog(level=1,message=e)
                    return False
        else:
            if c =="$":
                switch=True
            else:
                endString=endString+c
    return endString



def procHashStringV2(hashString,sessionDetails):
    switch=False
    endString=""
    toEvaluate=""
    null=''

    if hashString is None:
        return ''
    if type(hashString) is not str:
        return hashString

    for c in hashString:
        if switch:
            if c !="#":
                toEvaluate=toEvaluate+c
            else: 
                switch=False
                try:

                    if "sessionDetails" in  toEvaluate:
                        toEvaluate=toEvaluate.replace("sessionDetails","sessionDetails['request']")
                        endString=endString+str(eval(toEvaluate))
                        toEvaluate=""
                    elif "outputs" in  toEvaluate:
                        #Get task name
                        taskNameWithQuote=toEvaluate.split("outputs[")[1].split("][")[0]
                        if "'" in taskNameWithQuote:
                            taskName=taskNameWithQuote.split("'")[1]
                        else:
                            taskName=taskNameWithQuote.split('"')[1]
                        logger.autolog(level=1,message='Got taskName: ' + str(taskName))

                        #Get Output
                        outputName=toEvaluate.split("["+taskNameWithQuote+"]")[1]
                        logger.autolog(level=1,message='Got outputName: ' + str(outputName))

                        #Get task
                        logger.autolog(level=1,message='Got task action : ' + str(sessionDetails["action"]))
                        for task in sessionDetails["tasks"][sessionDetails["action"]]:
                            if task["name"]==taskName:
                                logger.autolog(level=1,message='Found a matching task name, will try to get output:  ' + "task['output']"+str(outputName))
                                endString=endString+str(eval("task['output']"+str(outputName)))
                        toEvaluate=""
                    else:
                        endString=endString+str(eval("sessionDetails"+toEvaluate))
                        toEvaluate=""     

                except Exception as e:
                    logger.autolog(level=1,message='Got exception while evaluating input ' + toEvaluate)
                    logger.autolog(level=1,message=e)
                    return False
        else:
            if c =="#":
                switch=True
            else:
                endString=endString+c
    return endString


def procHashString(hashString,sessionDetails,outputs):
    switch=False
    endString=""
    toEvaluate=""
    null=''
    #sessionDetails=eval(str(sessionDetails['json']))

    try:
        sessionDetails=json.loads(str(sessionDetails['payload']))
    except Exception as e:
        sessionDetails=eval(str(sessionDetails['json']))

    if hashString is None:
        return ''
    if type(hashString) is not str:
        return hashString

    for c in hashString:
        if switch:
            if c !="#":
                toEvaluate=toEvaluate+c
            else: 
                switch=False
                try:
                    endString=endString+str(eval(toEvaluate))
                    toEvaluate=""
                except Exception as e:
                    logger.autolog(level=1,message='Got exception while evaluating input ' + toEvaluate)
                    logger.autolog(level=1,message=e)
                    return False
        else:
            if c =="#":
                switch=True
            else:
                endString=endString+c
    return endString



def procHashStringWithRedis(hashString,sessionDetails,logFormat=''):
    logger.autolog(level=1,message='Got hastring to process: ' + str(hashString))
    outputs=inMemoryStorage.cacheReadRequestOutputs(sessionDetails,logFormat=logFormat)
    if outputs:
        return procHashString(hashString,sessionDetails,outputs)
    else:
        return procHashString(hashString,sessionDetails,{})


def procDeps(evalString,executedTasks):
    outputs=getTasksOutputs(executedTasks)
    try:
        return eval(evalString)
    except Exception as e:
        logger.autolog(level=1,message='Error while evaluating dependecy ' + evalString)
        logger.autolog(level=1,message=e)
        return "ERROR"

def procInput(input, sessionDetails, outputs):
    if type(input) is str:
        return procDollarString(input, sessionDetails, outputs)
    if type(input) is dict:
        for key in input.keys():
            input[key]=procInput(input[key], sessionDetails, outputs)
        return input
    if type(input) is list:
        output=[]
        for item in input:
            output.append(procInput(item, sessionDetails, outputs))
        return output
    if input is None:
        return ''
    return input


def procInputV2(input, sessionDetails):
    if type(input) is str:
        return procDollarStringV2(input, sessionDetails)
    if type(input) is dict:
        for key in input.keys():
            input[key]=procInputV2(input[key], sessionDetails)
        return input
    if type(input) is list:
        output=[]
        for item in input:
            output.append(procInputV2(item, sessionDetails))
        return output
    if input is None:
        return ''
    return input




def procInputsV2(sessionDetails,executedTasks,inputs):
    outputs=getTasksOutputs(executedTasks)
    newInputs={}
    if type(inputs) is dict:
        for key in inputs:
            processedInput=procInputV2(inputs[key],sessionDetails)
            if processedInput or processedInput =='':
                newInputs[key]=processedInput
            else:
                logger.autolog(level=1,message='Failed to get processed input for ' + str(inputs[key]))
                pass
    else:
        #logger.autolog(level=1,message='Provided input is not a dictionary')
        pass
    return newInputs



def procInputs(sessionDetails,executedTasks,inputs):
    outputs=getTasksOutputs(executedTasks)
    newInputs={}
    if type(inputs) is dict:
        for key in inputs:
            processedInput=procInput(inputs[key],sessionDetails,outputs)
            if processedInput or processedInput =='':
                newInputs[key]=processedInput
            else:
                logger.autolog(level=1,message='Failed to get processed input for ' + str(inputs[key]))
                pass
    else:
        #logger.autolog(level=1,message='Provided input is not a dictionary')
        pass
    return newInputs


def getTasksOutputs(executedTasks):
    outputs={}
    for executedTask in executedTasks:
        outputs[executedTask['taskName']]=executedTask['taskOutput']
    return outputs




#####################################
# Generic File Functions 
#####################################
def getFilesInFolder(sourceFolder):
    if sourceFolder=='':
        logger.autolog(level=1,message='Missing sourceFolder argument')
        return False

    #logger.autolog(level=1,message='Getting list of Files in folder ' + sourceFolder)
    files=[]
    try:
        for (dirpath, dirname, filenames) in os.walk(sourceFolder):
            #logger.autolog(level=1,message='Found files... ' + str(filenames))
            files.extend(filenames)
            break
    except Exception as e:
        logger.autolog(level=1,message='Could not get list of files of folder  ' + sourceFolder)
        logger.autolog(level=1,message=e)
        return []  
    return files

#####################################
# Session File Functions 
#####################################

def getSessionFiles(sourceFolder=''):
    return getFilesInFolder(sourceFolder)


def readSession(file_path=''):
    return read_session(file_path)

def read_session(file_path=''):
    if file_path=='':
        logger.autolog(level=1,message=' No session file path provided' )
        return False    
    #logger.autolog(level=1,message='About to read session File ' + file_path )
    #This function returns a dictionary with the data extracted from the session.xml dile
    #The values are extracted from the dictionarny with the following indexes:
    # id,owner,passwd, vpod
    #
    #example:  my_session= read_session();
    #          my_session_id=my_session['id']
    
    xmldoc = None;
    if platform.system() == 'Linux':
        try:
            xmldoc = minidom.parse(file_path);
        except IOError:
            logger.autolog(level=1,message='Oops! That was no valid file. Make sure session file is created')
            return False
        except Exception as e :
             logger.autolog(level=1,message='Oops! That was no valid file. Maybe malformed XML')
             logger.autolog(level=1,message=e)
             return False
    
    logger.autolog(level=1,message=' Reading Session file ' + file_path)
    session ={};
    try:
        session['id'] = xmldoc.getElementsByTagName('id')[0].childNodes[0].nodeValue;
    except IndexError:
        logger.autolog(level=1,message='Oops! No session ID found')
        session['id'] = '';
    try:
        session['owner'] = xmldoc.getElementsByTagName('owner')[0].childNodes[0].nodeValue;
    except IndexError:
        logger.autolog(level=1,message='Oops! No session Owner found')
        session['owner'] = '';
    try:
        session['vpod'] = xmldoc.getElementsByTagName('vpod')[0].childNodes[0].nodeValue;
    except IndexError:
        logger.autolog(level=1,message='Oops! No session vpod found')
        session['vpod'] = '';
    try:
        session['anycpwd']= xmldoc.getElementsByTagName('anycpwd')[0].childNodes[0].nodeValue;
    except IndexError:
        logger.autolog(level=1,message='Oops! No session anyconnect password found')
        session['anycpwd'] = '';

    try:
        session['datacenter']= xmldoc.getElementsByTagName('datacenter')[0].childNodes[0].nodeValue;
    except IndexError:
        logger.autolog(level=1,message='Oops! No session datacenter found')
        session['datacenter'] = '';


#    logger.autolog(level=1,message='Obtained Values are:')
#    logger.autolog(level=1,message='Session ID is: %s' % session['id'])
#    logger.autolog(level=1,message='Session Owner is: %s' % session['owner'])  
#    logger.autolog(level=1,message='Session vpod is :%s' % session['vpod'])
#    logger.autolog(level=1,message='Session Anyconnect Password is: %s' % session['anycpwd'])
#    logger.autolog(level=1,message='Session Datacenter is: %s' % session['datacenter'])
    return session



def readSessionDic(sessionFilePath=''):
#
#   This function returns a dictionary 
#   Example:
#   sessionDic = readSessionDic(path_to_session_file)
#   sessionId = sessionDic['session']['id']
#   sessionVpod = sessionDic['session']['vpod']
#   sessionDevices = sessionDic['session']['devices']['device'] # this will return a list of all devices
#


    if sessionFilePath=='':
        logger.autolog(level=1,message='No session file name provided' )
        return False    
    #logger.autolog(level=1,message='About to read XML session File ' + sessionFilePath )
    try:
        with open(sessionFilePath) as sessionFile:
            sessionDic = xmltodict.parse(sessionFile.read())
    except Exception as e:
        logger.autolog(level=1,message='Error while opening or parsing file')
        logger.autolog(level=1,message=e)
        return False
    return sessionDic


def readSessionFileFromActive(sessionFileName, otherInputs=[] ):

    global activeSessionFolderPath
    return readSessionDic(activeSessionFolderPath+sessionFileName)

def readSessionFileFromNew(sessionFileName, otherInputs=[]):

    global newSessionFolderPath
    return readSessionDic(newSessionFolderPath+sessionFileName)


def moveFile(fileName, source, destination):
    #logger.autolog(level=1,message='Moving file ' + fileName + 'from ' + source + ' to ' + destination)
    try:
        cmd='mv ' + source + fileName + ' ' + destination
        #logger.autolog(level=1,message='command to be executed is : ' + cmd)
        os.system(cmd)
        #logger.autolog ("File " + fileName + ' has been moved to ' + destination)
        return True
    except:
        logger.autolog(level=1,message='Error found, not able to move file ' + fileName)
        logger.autolog(level=1,message=sys.exc_info()[0])
        return False


def getSessionValueByKey(sessionFile, key='id'):
    global activeSessionFolderPath
    session=readSession(activeSessionFolderPath + sessionFile)
    return session[key]

#####################################
# Session Status Functions 
#####################################
def isSessionReadyToDelete(sessionFile,otherInputs=[]):
    global activeSessionFolderPath

    session=readSession(activeSessionFolderPath + sessionFile)
    if session == False:
        status= False
        output= None
        return formatOutput(output), status

    sessionStatus= checkSessionStatus(session['id'],session['datacenter'])
    if sessionStatus == 'Error':
        status= False
        output= None
        return formatOutput(output), status

    logger.autolog ('Session Status is: ' + str(sessionStatus))
    status = True
    output =  not sessionStatus
    logger.autolog ('Output Value is: ' + str(output))

    return formatOutput(output), status




#####################################
# Configuration File Functions (YAML)
#####################################

def getDemoCredentials(cfgFile):
    #logger.autolog(level=1,message='About to get Cred  from ' + cfgFile)
    return getCfgFileValues(cfgFile, 'Creds')


def getDemoCredentialsByName(cfgFile, name):
    #logger.autolog(level=1,message='About to get Cred  for device  ' + str(name))
    try:
        creds = getCfgFileValues(cfgFile, 'Creds')
    except Exception as e:
        logger.autolog(level=1,message='Error while obtaining Creds from file  ' + str(cfgFile))
        logger.autolog(level=1,message=e)
        return False

    if not creds:
        logger.autolog(level=1,message='Unable to get cred with matching name ' + str(name))
        return False
    try:
        for cred in creds:
            if cred['name']== name:
                #logger.autolog(level=1,message='Found cred with matching name ' + str(name))
                return cred
    except Exception as e:
        logger.autolog(level=1,message='Error while obtaining creds matching name ' + str(name))
        logger.autolog(level=1,message=e)
        return False

    logger.autolog(level=1,message='Could not find a cred with matching name  ' + str(name))
    return False

#def getDemoTasks(cfgFile):
#    #logger.autolog(level=1,message='About to get Tasks from ' + cfgFile)
#    return getCfgFileValues(cfgFile, 'Tasks')

def getDemoAfterTasks(cfgFile):
    #logger.autolog(level=1,message='About to get After Tasks details from ' + cfgFile)
    return getCfgFileValues(cfgFile, 'AfterTasks')

def getDemoDetails(cfgFile):
    #logger.autolog(level=1,message='About to get Demo details from ' + cfgFile)
    return getCfgFileValues(cfgFile, 'Details')

def getDemoImports(cfgFile):
    #logger.autolog(level=1,message='About to get Demo details from ' + cfgFile)
    return getCfgFileValues(cfgFile, 'Imports')

def getCfgFileValues(cfgFile, branchName):
    #logger.autolog(level=1,message='About to get Demo details from ' + cfgFile)
    try:
        content = readYamlFile(cfgFile)
        if content:
            return content[branchName]
        else:
            return False
    except Exception as e:
        logger.autolog(level=1,message='Could not get Demo Details from file ' + cfgFile)
        logger.autolog(level=1,message=e)
        return False  
 
def readYamlFile(srcFile):
    #logger.autolog(level=1,message='About to read the YAML file: ' + srcFile)
    try:
        #logger.autolog(level=1,message='Extracting info from  file using YAML')
        with open( srcFile, 'r' ) as contentList:
            content = yaml.safe_load( contentList )
    except Exception as e:
        logger.autolog(level=1,message='Could not read YAML  file')
        logger.autolog(level=1,message=e)
        return False  
    return content

#####################################
# Task Status Functions 
#####################################

def checkAllTasksStatus(sessionFile, executedTasks):
    errorFlag = False
    errorTasks = []
    for executedTask in executedTasks:
        if executedTask['taskStatus'] == False:
            errorFlag = True
            errorTasks.append(executedTask['taskName'])
            logger.autolog(level=1,message='Task named ' + str(executedTask['taskName']) + ' failed')
    logger.autolog(level=1,message='errorFlag is set to ' + str(errorFlag))
    return formatOutput(errorFlag), True


#####################################
# Input/Output Functions 
#####################################

def getInputByName(inputs, inputName):
    for input in inputs:
        if input['name'] == inputName:
            return input['value']
    return 'not found'
 

def formatOutput(output):
    import inspect
    # Get the previous frame in the stack, otherwise it would
    # be this function!!!
    func = inspect.currentframe().f_back.f_code
    return {'name': func.co_name , 'value' : output}



#####################################
# SSH FUNCTIONS 
#####################################
def sshExecCommand(host='',username='', password='', command='', timeout=20 ):
    logger.autolog(level=1,message="About to send the following command via SSH: " + command)
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(host, username=username, password=password)
        stdin, stdout, stderr = ssh.exec_command(command, timeout=timeout)
        #Wait for the command to terminate

#        while not ssh_stdout.channel.exit_status_ready():
#            ##Only print data if there is data to read in the channel
#            logger.autolog(level=1,message='Ready!')
#            logger.autolog(level=1,message=ssh_stdout.read())
#            #logger.autolog(level=1,message='Std Err:')
#            #logger.autolog(level=1,message=ssh_stderr.read())

        output=''
        while not stdout.channel.exit_status_ready():
            # Only print data if there is data to read in the channel
            if stdout.channel.recv_ready():
                rl, wl, xl = select.select([stdout.channel], [], [], 0.0)
                if len(rl) > 0:
                    # Print data from stdout
                    output= stdout.channel.recv(1024),

    #except paramiko.ssh_exception.SSHException, SSHException:
    except Exception as e:
        logger.autolog(level=1,message='Error while executing exec_command')
        logger.autolog(level=1,message=str(e))
        return False

    logger.autolog(level=1,message='obtained output for command   ' + str(command) + 'is ' + str(output))
    ssh.close()
    return output


def sshExecPsScript(creds, scriptPath, scriptName, psArgs, timeout=60):

    logger.autolog(level=1,message='Starting sshExecPsScript')

    #Full path to the wrapper batch script on the PS Server
    wrapperBatchFile = r'C:\\Users\\demouser\\Documents\\generic\\execPs.bat'
    wrapperBatchFile = r'C:\\Scripts\\generic\\execPs.bat'

    #Generate name of a temp file to be used to hold the output of the sccript to be executed. Tha name is random, it will be deleted at the end of this function.
    outputFileNameSize = 8
    chars = string.letters + string.digits
    outputFileName = ''.join((random.choice(chars)) for x in range(outputFileNameSize))
    outputFile =  scriptPath + outputFileName + '.txt'
    #logger.autolog(level=1,message='Temp file to be used is ' + outputFile)

    #The arguments to be passed to the PS script are received in this function as a list of dictionaries. 
    #psArgs = [{'name': **Arg1 Name** , 'value': **Arg1 Value**},{'name': **Arg2 Name** , 'value': **Arg2 Value**},...]
    #In order to read them, we get first argument name as psArgs[0]['name'] and its value as psArgs[0]['value']
    #The following code puts all the arguments on a single string
    otherPsArgs=''
    for arg in psArgs:
        otherPsArgs += " -" + arg['name']
        otherPsArgs += " " + arg['value']
    #logger.autolog(level=1,message='Extra Arguments to be passed to PS script: ' +  otherPsArgs)


    #Connect to PS Server via SSH, execute the wrapper batch passing the outputFile, and the PS script file to be executed. If there are more arguments to be passed to the PS script, it shuold be in psArgs
    #logger.autolog(level=1,message='About to execute wrapper batch script for PS script ' + scriptName)
    #command= wrapperBatchFile + ' ' + outputFile + ' ' + scriptPath + scriptName + ' "' + otherPsArgs+ '"'
    #command= wrapperBatchFile + ' ' + outputFile + ' ' + scriptPath + scriptName + ' "' + otherPsArgs+ '" 1>nul 2>nul'
    command= wrapperBatchFile + ' ' + outputFile + ' ' + scriptPath + scriptName + ' "' + otherPsArgs+ '"'

    batOutput= sshExecCommand(host = creds['host'],username = creds['username'], password = creds['password'], command=command, timeout=timeout)
    if not batOutput:
        logger.autolog(level=1,message='Error while trying to execute wrapper batch script on PS Server')
        return False

    #The PS script is expected to print the script output (ie. True or False) to a temp file. The content of this temp file will be returned from this function
    #logger.autolog(level=1,message='About to read temp file that holds output of PS script ' + scriptName + '. Temp File is ' + outputFile )
    command = r'type ' + outputFile
    output=sshExecCommand(host = creds['host'],username = creds['username'], password = creds['password'], command=command, timeout=timeout)
    if not output:
        logger.autolog(level=1,message='Error while reading temp file')
        return False

    #Delete temp file
    #logger.autolog(level=1,message='About to delete temp file ' + outputFile )   
    command = r' cd ' + scriptPath + ' && del ' + outputFileName + '.txt'
    delOutput= sshExecCommand(host = creds['host'],username = creds['username'], password = creds['password'], command=command, timeout=timeout)
    if delOutput == False:
        logger.autolog(level=1,message='Error while deleting temp file ' + outputFile ) 
        return False 

    logger.autolog(level=1,message='Value found on temp file is : '+ str(output[0].split('\r')[0]) )
    return output[0].split('\r')[0]


def sshExecPsScriptV2(creds, scriptPath, scriptName, psArgs, timeout=60):

    logger.autolog(level=1,message='Starting sshExecPsScriptV2')

    #The arguments to be passed to the PS script are received in this function as a  dictionary. 
    #psArgs = {'argName1': argValue1, 'argName2': argValue2, ...}
    #The following code puts all the arguments on a single string
    otherPsArgs=' '
    for argName in psArgs.keys():
        otherPsArgs += " -" + argName + ' ' + psArgs[argName]
    logger.autolog(level=1,message='Arguments to be passed to PS script: ' +  otherPsArgs)

    ssh= paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    logger.autolog(level=1,message='Connecting to ' + str(creds['host']) )

    try:
        ssh.connect(creds['host'], port=creds['port'], username=creds['username'], password=creds['password'])
        logger.autolog(level=1,message='Connected to '  + str(creds['host']))

    except Exception as e:
        logger.autolog(level=1,message='Error while connecting... ')
        logger.autolog(level=1,message=str(e))
        return False,False,False

    cmd = scriptPath + scriptName + otherPsArgs
    logger.autolog(level=1,message='Command to execute is  '  + str(cmd))

    stdIn=''
    stdOut=''
    stdErr=''

    try:
        stdin, stdout, stderr = ssh.exec_command(cmd,timeout=600)
        stdOut=str(stdout.readlines())
        stdErr=str(stderr.readlines())
        logger.autolog(level=1,message='StdOut is  '  + stdOut)
        logger.autolog(level=1,message='StdErr is  '  + stdErr)
        ssh.close()
    except Exception as e:
        logger.autolog(level=1,message='Error while executing exec_command')
        logger.autolog(level=1,message=str(e))
        return False,False,False

    return stdOut,stdErr



def sshExecPsFunction(creds, scriptPath, scriptName,functionName, psArgs, timeout=60):

    logger.autolog(level=1,message='Starting sshExecPsFunction')

    logger.autolog(level=1,message='Arguments to be passed to PS script: ' +  str(psArgs))

    ssh= paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    logger.autolog(level=1,message='Connecting to ' + str(creds['host']) )

    try:
        ssh.connect(creds['host'], port=creds['port'], username=creds['username'], password=creds['password'])
        logger.autolog(level=1,message='Connected to '  + str(creds['host']))

    except Exception as e:
        logger.autolog(level=1,message='Error while connecting... ')
        logger.autolog(level=1,message=str(e))
        return str(e),False,False

    cmd = r"cd p:\modules"+scriptPath +r" ; . .\\" + scriptName +" ; " + functionName + " " +str(psArgs)

    logger.autolog(level=1,message='Command to execute is  '  + str(cmd))

    stdIn=''
    stdOut=''
    stdErr=''

    try:
        stdin, stdout, stderr = ssh.exec_command(cmd,timeout=600)
        stdOut=str(stdout.readlines())
        stdErr=str(stderr.readlines())
        logger.autolog(level=1,message='StdOut is  '  + stdOut)
        logger.autolog(level=1,message='StdErr is  '  + stdErr)
        ssh.close()
    except Exception as e:
        logger.autolog(level=1,message='Error while executing exec_command')
        logger.autolog(level=1,message=str(e))
        return str(e),False,False

    return True,stdOut,stdErr


def sshExecPsScript(creds, scriptPath, scriptName, psArgs, timeout=60):

    logger.autolog(level=1,message='Starting sshExecPsFunction')

    logger.autolog(level=1,message='Arguments to be passed to PS script: ' +  str(psArgs))

    ssh= paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    logger.autolog(level=1,message='Connecting to ' + str(creds['host']) )

    try:
        ssh.connect(creds['host'], port=creds['port'], username=creds['username'], password=creds['password'])
        logger.autolog(level=1,message='Connected to '  + str(creds['host']))

    except Exception as e:
        logger.autolog(level=1,message='Error while connecting... ')
        logger.autolog(level=1,message=str(e))
        return str(e),False,False

    cmd = r"cd p:\modules"+scriptPath +r" ; . .\\" + scriptName +"  " +str(psArgs)

    logger.autolog(level=1,message='Command to execute is  '  + str(cmd))

    stdIn=''
    stdOut=''
    stdErr=''

    try:
        stdin, stdout, stderr = ssh.exec_command(cmd,timeout=600)
        stdOut=str(stdout.readlines())
        stdErr=str(stderr.readlines())
        logger.autolog(level=1,message='StdOut is  '  + stdOut)
        logger.autolog(level=1,message='StdErr is  '  + stdErr)
        ssh.close()
    except Exception as e:
        logger.autolog(level=1,message='Error while executing exec_command')
        logger.autolog(level=1,message=str(e))
        return str(e),False,False

    return True,stdOut,stdErr
#####################################
# POWERSHELL FUNCTIONS 
#####################################






#####################################
# WAIT FUNCTIONS 
#####################################


def waitForSeconds(seconds):
    try:
        logger.autolog(level=1,message='Waiting for ' + str(seconds) + ' seconds')
        time.sleep(seconds)
    except Exception as e:
        logger.autolog(level=1,message='Error while sleeping')
        logger.autolog(level=1,message=str(e))
        return False
    return True


#####################################
# TESTING FUNCTIONS 
#####################################
def createAdUser(sessionFile, otherInputs):
    if (type(otherInputs) is dict):
        try:
            username=otherInputs["username"]
            password=otherInputs["password"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments')
            logger.autolog(level=1,message=str(e))
            return {},'Failed'
    with open('/root/indemo_automation/createAdUserStatus.txt', 'r') as actionStatus:
        status=actionStatus.read().replace('\n', '')
    with open('/root/indemo_automation/createAdUserOutput.txt', 'r') as actionOutput:
        output=actionOutput.read().replace('\n', '')
    return {"status":output},status
    #return {},'Success'

def createVmFolderWithPermissions(sessionFile, otherInputs):
    if (type(otherInputs) is dict):
        try:
            folderName=otherInputs["folderName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments')
            logger.autolog(level=1,message=str(e))
            return {},'Failed'
    return {"test":"testvalue"},'Success'

def createDatastoreWithPermissions(sessionFile, otherInputs):
    if (type(otherInputs) is dict):
        try:
            dsName=otherInputs["dsName"]
        except Exception as e:
            logger.autolog(level=1,message='Error while reading input arguments')
            logger.autolog(level=1,message=str(e))
            return {},'Failed'
    return {},'Success'



def createUserVm(sessionFile, otherInputs):
    return {},'Success'

def finishSessionTask(sessionFile, target, otherInputs):
    return {},'Success'

def getSessionStatus(sessionFile, target, otherInputs):
    with open('/root/indemo_automation/sessionStatus.txt', 'r') as sessionStatus:
        status=sessionStatus.read().replace('\n', '')
    return {"status":status},'Success'
    #return {"status":"Deleted"},'Success'
    #return {"status":"active"},'Success'


def removeAdUser(sessionFile, otherInputs):
    return {},'Success'

def removeVmFolderWithPermissions(sessionFile, otherInputs):
    return {},'Success'

def removeDatastoreWithPermissions(sessionFile, otherInputs):
    return {},'Success'

def removeUserVm(sessionFile, otherInputs):
    return {},'Success'


