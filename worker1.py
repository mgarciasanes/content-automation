#!/usr/bin/env python2.7

#####################################
# Global Variables
#####################################
#demoRecipesPath=r'/root/scripts/demoRecipes/'
modulesPath=r'/root/scripts/modules/'
INFLUXDB='influxKube'
REDISDETAILS={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'}
global LOCATION
global INFLUXDBPORT

#####################################
# Import Public Modules
#####################################
import sys
import logging
import threading
import os
import os.path
import time
import datetime
import socket
import psutil
import json


#####################################
# Import Controller Modules
#####################################
modulesPath=r'/root/scripts/modules/'
sys.path.append(modulesPath)
corePath=r'/root/scripts/core/'
sys.path.append(corePath)


import settings
import rabbitQueue as rabbit
import genFunctions as gen
import logger as logger
import worker as worker
import signalMonitor
#import influx as influx
#import inMemoryStorage

host = socket.gethostname()


#####################################
# General Functions
#####################################

def refreshModules():
    logger.autolog(level=1, message='Refreshing Modules, importing...')
    reload(gen)
    reload(logger)
    reload(worker)
    #reload(signalMonitor)


#####################################
#  FUNCTIONS 
#####################################

def callback(ch, method, properties, body):
    try:

        refreshModules()

        bodyDict=json.loads(body)

        ###########################################         
        #Checking if it is a worker restart action
        ###########################################         

        if bodyDict['action']=='workerRestart':
            try:
                logger.autolog(level=1,message='##########################################################')
                logger.autolog(level=1,message='##########################################################')
                logger.autolog(level=1,message='### Order from RabbitMq: Worker to be restarted    ... ###')
                logger.autolog(level=1,message='##########################################################')
                logger.autolog(level=1,message='##########################################################')


                #Send ACK
                ch.basic_ack(delivery_tag = method.delivery_tag)

                #Stop consuming 
                ch.stop_consuming()

                try:
                    p = psutil.Process(os.getpid())
                    for handler in p.open_files() + p.connections():
                        logger.autolog(level=1,message="Restarting Worker: Closing handler  " + str(handler))
                        os.close(handler.fd)        
                except Exception as e:
                    logger.autolog(level=1, message="Error Restarting Worker")
                    logger.autolog(level=1, message=str(e))

                logger.autolog(level=1,message="Restarting Worker NOW!")
                python = sys.executable
                os.execl(python, python, *sys.argv) 

            except Exception as e:
                logger.autolog(level=1, message="Error while restarting worker. Will need to restart manually!")
                logger.autolog(level=1, message=str(e))



        #################################################         
        #Checking for request related acitons  start/stop
        #################################################         

        logFormat=bodyDict["request"]['id']+'|'+bodyDict["request"]['owner']+'|'+bodyDict["request"]['recipeName']+'|'+bodyDict['action']+'|'
        settings.logFormat=logFormat
        logger.autolog(level=1, message="Starting the Rabbit Callback Function for worker",format=logFormat)
        logger.autolog(level=1, message="Message received: " + str(body),format=logFormat)

        if bodyDict['action']=='Start':
            try:
                logger.autolog(level=1,message='Execute queue requests with ID ' + str(bodyDict["request"]['id']),format=logFormat)

                queuedSession=bodyDict

                logger.autolog(level=1,message='Got queued request for recipe ' +  queuedSession["request"]['recipeName']+ ' with id ' + queuedSession["request"]['id'] + '. It will be executed now',format=logFormat)
                #################result,request=inMemoryStorage.cacheUpdateRequestMainStatus(queuedSession,'starting','',redisDetails=REDISDETAILS,logFormat=logFormat)
                queuedSession["status"]="starting" 
                queuedSession["registerAction"]=""
                #################inMemoryStorage.cacheWriteRequest(queuedSession,redisDetails=REDISDETAILS,logFormat=logFormat)
                #################influx.markSessionStarting(queuedSession["request"],serialStatus=json.dumps(queuedSession["request"]),influxDb=INFLUXDB)
                rabbit.rabbitSendToRegisterExchange(rabbitServer,queuedSession,port=settings.workerRabbitPort)


                tasks,errorMessage=gen.getRecipeStartTasks(queuedSession["request"]['recipeName'],specificFolder=queuedSession["request"]['recipePath'])
                type,eMsg=gen.getRecipeType(queuedSession["request"]['recipeName'],specificFolder=queuedSession["request"]['recipePath'])
                #logger.autolog(message='!!!!!!!!!!!!!!!!!RECIPE TYPE IS !!!!!!!!!!!!!!!!!!: '+ str(type),format=logFormat)


                if tasks == False: 
                    logger.autolog(message=errorMessage +' '+ str(queuedSession["request"]['recipeName']),format=logFormat)
                    #################result,request=inMemoryStorage.cacheUpdateRequestMainStatus(queuedSession,'error',errorMessage,redisDetails=REDISDETAILS,logFormat=logFormat)
                    queuedSession["status"]="error" 
                    queuedSession["comments"]=errorMessage
                    queuedSession["registerAction"]=""
                    #################inMemoryStorage.cacheWriteRequest(queuedSession,redisDetails=REDISDETAILS,logFormat=logFormat)
                    #################influx.markSessionError(queuedSession["request"] ,comments=errorMessage,serialStatus=json.dumps(queuedSession["request"]),influxDb=INFLUXDB)
                    rabbit.rabbitSendToRegisterExchange(rabbitServer,queuedSession,port=settings.workerRabbitPort)


                else: 
                    failureFree,allTasksExecuted,executedTasks,queuedSession=worker.execTasks(queuedSession,tasks,action='Start',redisDetails=REDISDETAILS,returnDetails=True) 

                    if (failureFree and allTasksExecuted):
                        logger.autolog(message='queued session executed without errors',format=logFormat)
                        if type == 'start+stop':
                            #################result,request=inMemoryStorage.cacheUpdateRequestMainStatus(queuedSession,'active','',redisDetails=REDISDETAILS,logFormat=logFormat)
                            queuedSession["status"]="active" 
                            queuedSession["registerAction"]=""
                            #################inMemoryStorage.cacheWriteRequest(queuedSession,redisDetails=REDISDETAILS,logFormat=logFormat)
                            #################influx.markSessionActive(queuedSession["request"],serialStatus=json.dumps(queuedSession["request"]),influxDb=INFLUXDB)
                            rabbit.rabbitSendToRegisterExchange(rabbitServer,queuedSession,port=settings.workerRabbitPort)

                        else:
                            ################result,request=inMemoryStorage.cacheUpdateRequestMainStatus(queuedSession,'executed','',redisDetails=REDISDETAILS,logFormat=logFormat)
                            queuedSession["status"]="executed"
                            queuedSession["registerAction"]=""
                            #################inMemoryStorage.cacheWriteRequest(queuedSession,redisDetails=REDISDETAILS,logFormat=logFormat)
                            #################influx.markSessionExecuted(queuedSession["request"],serialStatus=json.dumps(queuedSession["request"]),influxDb=INFLUXDB)
                            rabbit.rabbitSendToRegisterExchange(rabbitServer,queuedSession,port=settings.workerRabbitPort)

                    else:
                        logger.autolog(message='queued session executed with errors or some tasks were not executed',format=logFormat)
                        ################result,request=inMemoryStorage.cacheUpdateRequestMainStatus(queuedSession,'error','Failed to startup',redisDetails=REDISDETAILS,logFormat=logFormat)
                        queuedSession["status"]="error"
                        queuedSession["registerAction"]=""
                        #################inMemoryStorage.cacheWriteRequest(queuedSession,redisDetails=REDISDETAILS,logFormat=logFormat)
                        #################influx.markSessionError(queuedSession["request"],comments='Failed to startup',serialStatus=json.dumps(queuedSession["request"]),influxDb=INFLUXDB)
                        rabbit.rabbitSendToRegisterExchange(rabbitServer,queuedSession,port=settings.workerRabbitPort)

                        logger.autolog(message='Will proceed to clean up the error request....',format=logFormat)
                        bodyDict['action']='Stop'

                    #influx.recordExecutedTasks(executedTasks,influxDb=INFLUXDB)

            except Exception as e:
                logger.autolog(level=1, message="Error while executing START recipe " + bodyDict["request"]['recipeName'] ,format=logFormat)
                logger.autolog(level=1, message=str(e),format=logFormat)

        if bodyDict['action']=='Stop':
            try:

                markedSession=bodyDict

                logger.autolog(level=1,message='Got cleaning request for recipe ' +  markedSession["request"]['recipeName']+ ' with id ' + markedSession["request"]['id'] + '. It will be cleaned now',format=logFormat)
                ##################result,request=inMemoryStorage.cacheUpdateRequestMainStatus(markedSession,'cleaning','',redisDetails=REDISDETAILS,logFormat=logFormat)
                markedSession["status"]="cleaning"
                markedSession["registerAction"]=""
                ##################inMemoryStorage.cacheWriteRequest(markedSession,redisDetails=REDISDETAILS,logFormat=logFormat)
                ##################influx.markSessionCleaning(markedSession["request"],serialStatus=json.dumps(markedSession["request"]),influxDb=INFLUXDB)
                rabbit.rabbitSendToRegisterExchange(rabbitServer,markedSession,port=settings.workerRabbitPort)


                tasks,errorMessage=gen.getRecipeStopTasks(markedSession["request"]['recipeName'],specificFolder=markedSession["request"]['recipePath'])
                #type,eMsg=gen.getRecipeType(markedSession["request"]['recipeName'],specificFolder=markedSession["request"]['recipePath'])
                
                if tasks == False:
                    logger.autolog(message=errorMessage +' '+ str(markedSession["request"]['recipeName'])+ ' with ID ' + markedSession["request"]['id'])
                    #####################result,request=inMemoryStorage.cacheUpdateRequestMainStatus(markedSession,'error',errorMessage,redisDetails=REDISDETAILS,logFormat=logFormat)
                    markedSession["status"]="error" 
                    markedSession["comments"]=errorMessage
                    markedSession["registerAction"]=""
                    #################inMemoryStorage.cacheWriteRequest(markedSession,redisDetails=REDISDETAILS,logFormat=logFormat)
                    #################influx.markSessionError(markedSession["request"],comments=errorMessage,serialStatus=json.dumps(markedSession["request"]),influxDb=INFLUXDB)
                    rabbit.rabbitSendToRegisterExchange(rabbitServer,markedSession,port=settings.workerRabbitPort)

                else:   
                    failureFree,allTasksExecuted,executedTasks,markedSession=worker.execTasks(markedSession,tasks,action='Stop',redisDetails=REDISDETAILS, returnDetails=True)   
                    
                    if (failureFree and allTasksExecuted):
                        logger.autolog(message='All tasks executed without errors while checking marked Session. Marking deleted')
                        #################result,request=inMemoryStorage.cacheUpdateRequestMainStatus(markedSession,'complete','',redisDetails=REDISDETAILS,logFormat=logFormat)
                        markedSession["status"]="complete" 
                        markedSession["registerAction"]=""
                        #################inMemoryStorage.cacheWriteRequest(markedSession,redisDetails=REDISDETAILS,logFormat=logFormat)
                        #################influx.markSessionComplete(markedSession["request"],serialStatus=json.dumps(markedSession["request"]),influxDb=INFLUXDB)
                        rabbit.rabbitSendToRegisterExchange(rabbitServer,markedSession,port=settings.workerRabbitPort)

                    if (not failureFree):
                        logger.autolog(message='Some tasks got errors while checking marked Session. Marking error')
                        #################result,request=inMemoryStorage.cacheUpdateRequestMainStatus(markedSession,'error','Failed to cleanup',redisDetails=REDISDETAILS,logFormat=logFormat)
                        markedSession["status"]="error"
                        markedSession["registerAction"]=""
                        markedSession["comments"]='Error while cleanup'
                        #################inMemoryStorage.cacheWriteRequest(markedSession,redisDetails=REDISDETAILS,logFormat=logFormat)
                        #################influx.markSessionError(markedSession["request"],comments='Error while cleanup',serialStatus=json.dumps(markedSession["request"]),influxDb=INFLUXDB)
                        rabbit.rabbitSendToRegisterExchange(rabbitServer,markedSession,port=settings.workerRabbitPort)
                        
                    if (failureFree and not allTasksExecuted):
                        logger.autolog(message='All tasks executed without errors while checking marked Session. Marking deleted')
                        #################result,request=inMemoryStorage.cacheUpdateRequestMainStatus(markedSession,'complete','',redisDetails=REDISDETAILS,logFormat=logFormat)
                        markedSession["status"]="complete" 
                        markedSession["registerAction"]=""
                        #################inMemoryStorage.cacheWriteRequest(markedSession,redisDetails=REDISDETAILS,logFormat=logFormat)
                        #################influx.markSessionComplete(markedSession["request"],serialStatus=json.dumps(markedSession["request"]),influxDb=INFLUXDB)
                        rabbit.rabbitSendToRegisterExchange(rabbitServer,markedSession,port=settings.workerRabbitPort)
                    #    logger.autolog(message='No Errors, leaving Session as marked')
                    #    #influx.markSessionActive(activeSession,influxDb=INFLUXDB)

                    #influx.recordExecutedTasks(executedTasks,influxDb=INFLUXDB)

            except Exception as e:
                logger.autolog(level=1, message="Error while executing STOP recipe " + bodyDict["request"]['recipeName'] ,format=logFormat)
                logger.autolog(level=1, message=str(e),format=logFormat)



        #Send ACK
        ch.basic_ack(delivery_tag = method.delivery_tag)

    except Exception as e:
        logger.autolog(level=1, message="Error while in CALLBACK functon...", format="|RABBITCALLBACK|WORKER|")
        logger.autolog(level=1, message=str(e), format="|RABBITCALLBACK|WORKER|")
        ch.basic_ack(delivery_tag = method.delivery_tag)


if __name__ == '__main__':

    ##################################
    ## Check Arguments
    ##################################

    from argparse import ArgumentParser
    parser = ArgumentParser("dCloud DCV Automation Script: checkQueuedRequests")


    #NEED TO REMOVE THESE OPTIONS FROM TOPOLOGY BUILDER. NOT IN USE ANYMORE!
    parser.add_argument('-l', '--loop', help='Run Once or Loop. Values are  loop or once', default=True)
    parser.add_argument('-t', '--timer', help='Run Once or Loop. Values are  loop or once', default=5)
    parser.add_argument('-d', '--database', help='Specify database to use. Default is influxKube ', default='influxKube')
    parser.add_argument('-dp', '--databaseport', help='Specify database port to use. Default is False  ', default="noPort")
    parser.add_argument('-re', '--redisserver', help='Specify database to use. Default is influxKube ', default={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'})
    parser.add_argument('-rep', '--redisport', help='Specify database port to use. Default is False  ', default="noPort")
    parser.add_argument('-x', '--multithread', help='Run tasks for all sessions in concurrent or sequential. Values are true or false. default is false.', default=False)


    #THESE ARGUMENTS ARE STIL VALID
    parser.add_argument('-loc', '--location', help='Specify Location where this script is running. Default is False ', default="noLocation")
    parser.add_argument('-R', '--rabbitservernocreds', help='Specify rabbitmq server to use without creds file. ', default='noRabbit')
    parser.add_argument('-r', '--rabbitserver', help='Specify rabbitmq server to use. Default is rabbitKube ', default='rabbitKube')
    parser.add_argument('-rp', '--rabbitport', help='Specify rabbitmq port to use. Default is noPort  ', default="noPort")
    parser.add_argument('-c', '--credfile', help='Specify Credentials File  ', default=r'/root/repo/dcv/creds.cfg')

    args = parser.parse_args()
    
    
    #####################################
    ## Configure Logger
    #####################################


    hostname = socket.gethostname()
    settings.worker=hostname
    logger.setLogFiles(newLogFile=str(hostname)+'.log',newLogDebugFile=str(hostname )+'_DEBUG.log')

    #logger.setLogFiles(newLogFile='worker1.log',newLogDebugFile='worker1_DEBUG.log')
    logger.setLogger()
    logging.getLogger("pika").setLevel(logging.WARNING)


    #####################################
    ## Configure Signal Check
    #####################################

    #signalCheck = signalMonitor.SignalMonitor()

    #####################################
    ## Start Loop
    #####################################
    #global INFLUXDB




    #####################
    #Get DB Server
    #####################
    ##DB Port to be used. If not specified as argument, use the one specified in Creds file, section "database"
    #if args.databaseport=="noPort" or args.databaseport=="0":
    #    logger.autolog(level=1,message="No database port specified, using port from Cred file")
    #    INFLUXDB=args.database
    #else:
    #    logger.autolog(level=1,message="Database port specified, using it. " +str(args.databaseport))
    #    INFLUXDB={'database':args.database,'port':args.databaseport}


    #####################
    #Get Location
    #####################
    #Get Cluster Location. If not specified as argument, use the valued obtained directly from Kubernetes' label
    if args.location=="noLocation":
        cmd=r'kubectl get services kubernetes --namespace=default -o jsonpath={.metadata.labels.location} > /root/location'
        logger.autolog(level=1,message=str(cmd))
        os.system(cmd)
        locationFile=open(r'/root/location','r')
        location=locationFile.read().strip('\n')
        locationFile.close()
        logger.autolog(level=1,message="We are working with location  " + str(location))
        LOCATION=location
    else:
        LOCATION=str(args.location).lower()
    settings.workerLocation=LOCATION

    #####################
    #Get Rabbitmq Server
    #####################

    if args.rabbitservernocreds != "noRabbit":
        rabbitServer=args.rabbitservernocreds
    else:
        try:
            creds = gen.getDemoCredentialsByName(args.credfile,args.rabbitserver)
            rabbitServer=creds['host']
        except Exception as e:
            logger.autolog(level=1, message="Error while obtainig RABBIT server!!  Unable to continue, aborting worker... ")
            logger.autolog(level=1, message=str(e))
            quit(1)

    #If no port is specified for rabbitserver, use the one from credentials file. Otherwise, use the one provided. 
    if args.rabbitport=="noPort" or args.rabbitport=="0":
        try:
            rabbitPort=creds['port']
        except Exception as e:
            logger.autolog(level=1, message="Error while obtainig RABBIT port!!  Unable to continue, aborting worker... ")
            logger.autolog(level=1, message=str(e))
            quit(1)
    else:
        rabbitPort=str(args.rabbitport)

    settings.workerRabbitServer=rabbitServer
    settings.workerRabbitPort=rabbitPort


    #####################
    #Get Redis Server
    #####################
    #if isinstance(args.redisserver,str):
    #    try:
    #        creds = gen.getDemoCredentialsByName(args.credfile,args.redisserver)
    #        redisServer=creds['host']
    #        if args.redisport=="noPort" or args.redisport=="0":
    #            REDISDETAILS={'filePath':args.credfile,'name':args.redisserver}
    #        else:
    #            REDISDETAILS={'filePath':args.credfile,'name':args.redisserver, 'port': args.redisport}
    #    except Exception as e:
    #        logger.autolog(level=1, message="Error while obtainig REDIS  details!!  Unable to continue, aborting worker... ")
    #        logger.autolog(level=1, message=str(e))
    #        quit(1)
    #else:
    #    REDISDETAILS=args.redisserver


    logger.autolog(level=1,message='##########################################################')
    logger.autolog(level=1,message='##########################################################')
    logger.autolog(level=1,message='### Starting Worker...')
    logger.autolog(level=1,message='### Rabbit Server:  ' + str(rabbitServer))
    logger.autolog(level=1,message='### Rabbit Port:  ' + str(rabbitPort))
    #logger.autolog(level=1,message='### Redis Details:  ' + str(REDISDETAILS))
    #logger.autolog(level=1,message='### Influx  Details:  ' + str(INFLUXDB))
    logger.autolog(level=1,message='### Location:  ' + str(LOCATION))
    logger.autolog(level=1,message='### Will consume Rabbit queue now :  WORKERS.' + str(LOCATION))
    logger.autolog(level=1,message='##########################################################')
    logger.autolog(level=1,message='##########################################################')

#    loop(args.loop,args.multithread,int(args.timer))
    logger.autolog(level=1,message="Rabbit Server to be used is: " + str(rabbitServer))
    rabbit.rabbitListenToWorkersQueue(rabbitServer,LOCATION,callback,port=rabbitPort)


