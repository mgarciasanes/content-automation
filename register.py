#!/usr/bin/env python2.7

#####################################
# Global Variables
#####################################
#demoRecipesPath=r'/root/scripts/demoRecipes/'
modulesPath=r'/root/scripts/modules/'
INFLUXDB='influxKube'
REDISDETAILS={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'}
global LOCATION
global INFLUXDBPORT

#####################################
# Import Public Modules
#####################################
import sys
import logging
import threading
import os
import os.path
import time
import datetime
import socket
import psutil
import json


#####################################
# Import Controller Modules
#####################################
modulesPath=r'/root/scripts/modules/'
sys.path.append(modulesPath)
corePath=r'/root/scripts/core/'
sys.path.append(corePath)


import settings
import rabbitQueue as rabbit
import genFunctions as gen
import logger as logger
import worker as worker
import signalMonitor
import influx as influx
import inMemoryStorage

host = socket.gethostname()


#####################################
# General Functions
#####################################

def refreshModules():
    logger.autolog(level=1, message='Refreshing Modules, importing...')
    reload(influx)
    reload(gen)
    reload(logger)
    reload(worker)
    #reload(signalMonitor)




def callback(ch, method, properties, body):
    try:

        refreshModules()

        bodyDict=json.loads(body)

        ###########################################         
        #Checking if it is a register restart action
        ###########################################         

        if bodyDict["registerAction"]=='registerRestart':
            try:
                logger.autolog(level=1,message='##########################################################')
                logger.autolog(level=1,message='##########################################################')
                logger.autolog(level=1,message='### Order from RabbitMq: Register to be restarted  ... ###')
                logger.autolog(level=1,message='##########################################################')
                logger.autolog(level=1,message='##########################################################')


                #Send ACK
                ch.basic_ack(delivery_tag = method.delivery_tag)

                #Stop consuming 
                ch.stop_consuming()

                try:
                    p = psutil.Process(os.getpid())
                    for handler in p.open_files() + p.connections():
                        logger.autolog(level=1,message="Restarting Register: Closing handler  " + str(handler))
                        os.close(handler.fd)        
                except Exception as e:
                    logger.autolog(level=1, message="Error Restarting Register")
                    logger.autolog(level=1, message=str(e))

                logger.autolog(level=1,message="Restarting Register NOW!")
                python = sys.executable
                os.execl(python, python, *sys.argv) 

            except Exception as e:
                logger.autolog(level=1, message="Error while restarting worker. Will need to restart manually!")
                logger.autolog(level=1, message=str(e))


        #################################################         
        #Checking for READ actions
        ################################################# 


        logFormat=bodyDict["request"]['id']+'|'+bodyDict['registerAction']+'|'
        settings.logFormat=logFormat
        
        if bodyDict["registerAction"]=="requestStatus":

            try:
                if (not isinstance(properties.correlation_id,type(None)) and not isinstance(properties.reply_to,type(None))):

                    logger.autolog(level=1, message="Starting the Rabbit RPC Callback Function for Register with corrId " + str(properties.correlation_id) + " reply_to" + str(properties.reply_to),format=logFormat)

                    sessionDetails=influx.getSessionDetails(bodyDict["request"]["id"],influxDb=INFLUXDB)

                    if sessionDetails == [] or sessionDetails==False:
                        rabbit.rabbitResponseRPC(rabbitServer, properties.correlation_id,  properties.reply_to,[])
                        ch.basic_ack(delivery_tag = method.delivery_tag)
                        return False

                    #get Request History from INFLUXDB
                    requestHistory=influx.getSessionStatusHistory(bodyDict["request"]["id"], limit=100,influxDb=INFLUXDB)
                    if requestHistory== [] or requestHistory==False:
                        rabbit.rabbitResponseRPC(rabbitServer, properties.correlation_id,  properties.reply_to,[])
                        ch.basic_ack(delivery_tag = method.delivery_tag)
                        return False

                    #Get Cached request
                    cachedRequest=inMemoryStorage.cacheReadRequest(sessionDetails,output='txt',logFormat=logFormat)

                    #Insert Request History in cachedRequest before sending it to browser
                    cachedRequest=json.loads(cachedRequest)
                    cachedRequest["history"]=requestHistory
                    #cachedRequest=json.dumps(cachedRequest)


                    data=rabbit.rabbitResponseRPC(rabbitServer, properties.correlation_id,  properties.reply_to,cachedRequest)

                    #Send ACK
                    ch.basic_ack(delivery_tag = method.delivery_tag)

                    return data


            except Exception as e:
                logger.autolog(level=1, message="Error while executing RPC read call.")
                logger.autolog(level=1, message=str(e))
                #Send ACK
                ch.basic_ack(delivery_tag = method.delivery_tag)
                return False


        if bodyDict["registerAction"]=="requestList":

            try:
                if (not isinstance(properties.correlation_id,type(None)) and not isinstance(properties.reply_to,type(None))):

                    logger.autolog(level=1, message="Starting the Rabbit RPC Callback Function for Request List with corrId " + str(properties.correlation_id) + " reply_to" + str(properties.reply_to),format=logFormat)
                    sessions=influx.getAllSessionsWithFilters(sessionStatus=bodyDict["requestListFilters"]["filterStatus"],timeRangeStart=bodyDict["requestListFilters"]["filterTimeRangeStart"],timeRangeEnd=bodyDict["requestListFilters"]["filterTimeRangeEnd"], sessionDc=bodyDict["requestListFilters"]["filterDc"],sessionId=bodyDict["requestListFilters"]["filterId"],sessionOwner=bodyDict["requestListFilters"]["filterOwner"],sessionDemo=bodyDict["requestListFilters"]["filterDemo"],sessionLocation=bodyDict["requestListFilters"]["filterLocation"],influxDb=INFLUXDB,limit=300)
                    #logger.autolog(level=1, message="List of requets is:  " + str(sessions) ,format=logFormat)


                    data=rabbit.rabbitResponseRPC(rabbitServer, properties.correlation_id,  properties.reply_to,list(reversed(sessions)))

                    #Send ACK
                    ch.basic_ack(delivery_tag = method.delivery_tag)

                    return data

            except Exception as e:
                logger.autolog(level=1, message="Error while executing RPC read call.")
                logger.autolog(level=1, message=str(e))
                #Send ACK
                ch.basic_ack(delivery_tag = method.delivery_tag)
                return False


        #################################################         
        #Checking for WRITE actions
        #################################################         

        logFormat=bodyDict["request"]['id']+'|'+bodyDict["request"]['owner']+'|'+bodyDict["request"]['recipeName']+'|'+bodyDict['registerAction']+'|'
        settings.logFormat=logFormat
        logger.autolog(level=1, message="Starting the Rabbit Callback Function for Register",format=logFormat)
        #logger.autolog(level=1, message="Message received: " + str(body),format=logFormat)
        logger.autolog(level=1, message="Message received: " + str(bodyDict["status"]) ,format=logFormat)


        try:
            if bodyDict["registerAction"]=="redisOnly":
                inMemoryStorage.cacheWriteRequest(bodyDict,redisDetails=REDISDETAILS,logFormat=logFormat)
                #Send ACK
                ch.basic_ack(delivery_tag = method.delivery_tag)
                return 
        except Exception as e:
            pass



        inMemoryStorage.cacheWriteRequest(bodyDict,redisDetails=REDISDETAILS,logFormat=logFormat)

        if bodyDict["status"]=="starting":
            influx.markSessionStarting(bodyDict["request"],serialStatus=json.dumps(bodyDict["request"]),influxDb=INFLUXDB)

        if bodyDict["status"]=="queued":
            influx.markSessionQueued(bodyDict["request"],serialStatus=json.dumps(bodyDict["request"]),influxDb=INFLUXDB)

        if bodyDict["status"]=="error":
            influx.markSessionError(bodyDict["request"] ,comments=bodyDict["comments"],serialStatus=json.dumps(bodyDict["request"]),influxDb=INFLUXDB)

        if bodyDict["status"]=="active":
            influx.markSessionActive(bodyDict["request"],serialStatus=json.dumps(bodyDict["request"]),influxDb=INFLUXDB)

        if bodyDict["status"]=="executed":
            influx.markSessionExecuted(bodyDict["request"],serialStatus=json.dumps(bodyDict["request"]),influxDb=INFLUXDB)

        if bodyDict["status"]=="cleaning":
            influx.markSessionCleaning(bodyDict["request"],serialStatus=json.dumps(bodyDict["request"]),influxDb=INFLUXDB)

        if bodyDict["status"]=="complete":
            influx.markSessionComplete(bodyDict["request"],serialStatus=json.dumps(bodyDict["request"]),influxDb=INFLUXDB)
            



        #Send ACK
        ch.basic_ack(delivery_tag = method.delivery_tag)

    except Exception as e:
        logger.autolog(level=1, message="Error while in CALLBACK function...", format="|RABBITCALLBACK|REGISTER|")
        logger.autolog(level=1, message=str(e), format="|RABBITCALLBACK|REGISTER|")
        ch.basic_ack(delivery_tag = method.delivery_tag)


if __name__ == '__main__':

    ##################################
    ## Check Arguments
    ##################################

    from argparse import ArgumentParser
    parser = ArgumentParser("dCloud DCV Automation Script: Register")

    parser.add_argument('-d', '--database', help='Specify database to use. Default is influxKube ', default='influxKube')
    parser.add_argument('-dp', '--databaseport', help='Specify database port to use. Default is False  ', default="noPort")

    parser.add_argument('-re', '--redisserver', help='Specify database to use. Default is influxKube ', default={'filePath':r'/root/repo/dcv/creds.cfg','name':'redisKube'})
    parser.add_argument('-rep', '--redisport', help='Specify database port to use. Default is False  ', default="noPort")

    parser.add_argument('-loc', '--location', help='Specify Location where this script is running. Default is False ', default="noLocation")

    parser.add_argument('-r', '--rabbitserver', help='Specify rabbitmq server to use. Default is rabbitKube ', default='rabbitKube')
    parser.add_argument('-rp', '--rabbitport', help='Specify rabbitmq port to use. Default is noPort  ', default="noPort")
    parser.add_argument('-c', '--credfile', help='Specify Credentials File  ', default=r'/root/repo/dcv/creds.cfg')

    args = parser.parse_args()
    
    
    #####################################
    ## Configure Logger
    #####################################


    hostname = socket.gethostname()
    settings.register=hostname
    logger.setLogFiles(newLogFile=str(hostname)+'.log',newLogDebugFile=str(hostname)+'_DEBUG.log')

    #logger.setLogFiles(newLogFile='worker1.log',newLogDebugFile='worker1_DEBUG.log')
    logger.setLogger()


    #####################################
    ## Configure Signal Check
    #####################################

    #signalCheck = signalMonitor.SignalMonitor()


    #####################
    #Get DB Server
    #####################
    #DB Port to be used. If not specified as argument, use the one specified in Creds file, section "database"
    if args.databaseport=="noPort" or args.databaseport=="0":
        logger.autolog(level=1,message="No database port specified, using port from Cred file")
        INFLUXDB=args.database
    else:
        logger.autolog(level=1,message="Database port specified, using it. " +str(args.databaseport))
        INFLUXDB={'database':args.database,'port':args.databaseport}


    #####################
    #Get Location
    #####################
    #Get Cluster Location. If not specified as argument, use the valued obtained directly from Kubernetes' label
    if args.location=="noLocation":
        cmd=r'kubectl get services kubernetes --namespace=default -o jsonpath={.metadata.labels.location} > /root/location'
        logger.autolog(level=1,message=str(cmd))
        os.system(cmd)
        locationFile=open(r'/root/location','r')
        location=locationFile.read().strip('\n')
        locationFile.close()
        logger.autolog(level=1,message="We are working with location  " + str(location))
        LOCATION=location
    else:
        LOCATION=str(args.location).lower()
    settings.registerLocation=LOCATION

    #####################
    #Get Rabbitmq Server
    #####################
    creds = gen.getDemoCredentialsByName(args.credfile,args.rabbitserver)
    rabbitServer=creds['host']
    #If no port is specified for rabbitserver, use the one from credentials file. Otherwise, use the one provided. 
    if args.rabbitport=="noPort" or args.rabbitport=="0":
        try:
            rabbitPort=creds['port']
        except Exception as e:
            logger.autolog(level=1, message="Error while obtainig RABBIT port!!  Unable to continue, aborting worker... ")
            logger.autolog(level=1, message=str(e))
            quit(1)
    else:
        rabbitPort=str(args.rabbitport)
    settings.registerRabbitServer=rabbitServer
    settings.registerRabbitPort=rabbitPort


    #####################
    #Get Redis Server
    #####################
    if isinstance(args.redisserver,str):
        try:
            creds = gen.getDemoCredentialsByName(args.credfile,args.redisserver)
            redisServer=creds['host']
            if args.redisport=="noPort" or args.redisport=="0":
                REDISDETAILS={'filePath':args.credfile,'name':args.redisserver}
            else:
                REDISDETAILS={'filePath':args.credfile,'name':args.redisserver, 'port': args.redisport}
        except Exception as e:
            logger.autolog(level=1, message="Error while obtainig REDIS  details!!  Unable to continue, aborting worker... ")
            logger.autolog(level=1, message=str(e))
            quit(1)
    else:
        REDISDETAILS=args.redisserver


    logger.autolog(level=1,message='##########################################################')
    logger.autolog(level=1,message='##########################################################')
    logger.autolog(level=1,message='### Starting Register...')
    logger.autolog(level=1,message='### Rabbit Server:  ' + str(rabbitServer))
    logger.autolog(level=1,message='### Rabbit Port:  ' + str(rabbitPort))
    logger.autolog(level=1,message='### Redis Details:  ' + str(REDISDETAILS))
    logger.autolog(level=1,message='### Influx  Details:  ' + str(INFLUXDB))
    logger.autolog(level=1,message='### Location:  ' + str(LOCATION))
    logger.autolog(level=1,message='### Will consume Rabbit Exchange now :  REGISTERS')
    logger.autolog(level=1,message='### Will consume Rabbit Queue now :  REGISTERS.'+str(LOCATION))
    logger.autolog(level=1,message='##########################################################')
    logger.autolog(level=1,message='##########################################################')

    logger.autolog(level=1,message="Rabbit Server to be used is: " + str(rabbitServer))
    rabbit.rabbitListenBlockingExchange(rabbitServer,"REGISTERS",'fanout',"REGISTERS."+str(LOCATION),callback,port=rabbitPort)


